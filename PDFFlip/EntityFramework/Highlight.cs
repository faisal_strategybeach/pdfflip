//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PDFFlip.EntityFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class Highlight
    {
        public int HighlightID { get; set; }
        public int Chapter { get; set; }
        public string Location { get; set; }
        public string ColorCode { get; set; }
        public string BookID { get; set; }
        public string UserName { get; set; }
        public string Src { get; set; }
        public int Hid { get; set; }
    }
}
