﻿function getElementsByAttribute(e, t, n, r) {
    for (var o, i, a = "*" == t && e.all ? e.all : e.getElementsByTagName(t), s = new Array, l = void 0 !== r ? new RegExp("(^|\\s)" + r + "(\\s|$)", "i") : null, c = 0; c < a.length; c++) "string" == typeof (i = (o = a[c]).getAttribute && o.getAttribute(n)) && 0 < i.length && (void 0 === r || l && l.test(i)) && s.push(o);
    return s
}

function toggleClass(e, t) {
    strHasClass(e.className, t) ? removeClass(e, t) : addClass(e, t)
}

function addRemoveClass(e, t, n) {
    var r = e.className,
        o = !0;
    if (t)
        for (var i = 0; i < t.length; i++) strHasClass(r, t[i]) || (o = !1, r += ("" == r ? "" : " ") + t[i]);
    if (n)
        for (i = 0; i < n.length; i++) strHasClass(r, n[i]) && (o = !1, r = strRemoveClass(r, n[i]));
    o || (e.className = r.trim())
}

function addClass(e, t) {
    var n = e.className.trim();
    if (t instanceof Array) {
        for (var r = !0, o = 0; o < t.length; o++) strHasClass(n, t[o]) || (r = !1, n += ("" == n ? "" : " ") + t[o]);
        if (r) return
    } else {
        if (strHasClass(n, t)) return;
        n += ("" == n ? "" : " ") + t
    }
    e.className = n
}

function removeClass(e, t) {
    var n = e.className;
    if (t instanceof Array) {
        for (var r = !0, o = 0; o < t.length; o++) strHasClass(n, t[o]) && (r = !1, n = strRemoveClass(n, t[o]));
        if (r) return
    } else {
        if (!strHasClass(n, t)) return;
        n = strRemoveClass(n, t)
    }
    "" == (n = n.trim()) ? e.removeAttribute("class") : e.className = n
}

function getClassThatStartsWith(e, t) {
    for (var n = e.className.trim().split(" "), r = 0; r < n.length; r++)
        if (-1 < n[r].indexOf(t)) return n[r]
}

function hasClass(e, t) {
    return strHasClass(e.className, t)
}

function strHasClass(e, t) {
    for (var n = (e = e || "").trim().split(" "), r = 0; r < n.length; r++)
        if (n[r] == t) return !0;
    return !1
}

function strRemoveClass(e, t) {
    for (var n = (e = e || "").trim().split(" "), r = 0; r < n.length; r++) n[r] == t && (n[r] = null);
    return n.join(" ")
}

function shouldParse(e) {
    if (!e.match(/^http[s]?:/g) && 0 != e.indexOf("/") && 0 != e.indexOf("#dontparse")) return !0
}

function removeElement(e) {
    e && e.parentNode && e.parentNode.removeChild(e)
} ! function () {
    var e = CryptoJS,
        l = e.lib.WordArray;
    e.enc.Base64 = {
        stringify: function (e) {
            var t = e.words,
                n = e.sigBytes,
                r = this._map;
            e.clamp(), e = [];
            for (var o = 0; o < n; o += 3)
                for (var i = (t[o >>> 2] >>> 24 - o % 4 * 8 & 255) << 16 | (t[o + 1 >>> 2] >>> 24 - (o + 1) % 4 * 8 & 255) << 8 | t[o + 2 >>> 2] >>> 24 - (o + 2) % 4 * 8 & 255, a = 0; a < 4 && o + .75 * a < n; a++) e.push(r.charAt(i >>> 6 * (3 - a) & 63));
            if (t = r.charAt(64))
                for (; e.length % 4;) e.push(t);
            return e.join("")
        },
        parse: function (e) {
            var t = e.length,
                n = this._map;
            !(r = n.charAt(64)) || -1 != (r = e.indexOf(r)) && (t = r);
            for (var r = [], o = 0, i = 0; i < t; i++)
                if (i % 4) {
                    var a = n.indexOf(e.charAt(i - 1)) << i % 4 * 2,
                        s = n.indexOf(e.charAt(i)) >>> 6 - i % 4 * 2;
                    r[o >>> 2] |= (a | s) << 24 - o % 4 * 8, o++
                }
            return l.create(r, o)
        },
        _map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
    }
}(),
    function (i) {
        function C(e, t, n, r, o, i, a) {
            return ((e = e + (t & n | ~t & r) + o + a) << i | e >>> 32 - i) + t
        }

        function I(e, t, n, r, o, i, a) {
            return ((e = e + (t & r | n & ~r) + o + a) << i | e >>> 32 - i) + t
        }

        function E(e, t, n, r, o, i, a) {
            return ((e = e + (t ^ n ^ r) + o + a) << i | e >>> 32 - i) + t
        }

        function T(e, t, n, r, o, i, a) {
            return ((e = e + (n ^ (t | ~r)) + o + a) << i | e >>> 32 - i) + t
        }
        for (var e = CryptoJS, t = (r = e.lib).WordArray, n = r.Hasher, r = e.algo, S = [], o = 0; o < 64; o++) S[o] = 4294967296 * i.abs(i.sin(o + 1)) | 0;
        r = r.MD5 = n.extend({
            _doReset: function () {
                this._hash = new t.init([1732584193, 4023233417, 2562383102, 271733878])
            },
            _doProcessBlock: function (e, t) {
                for (var n = 0; n < 16; n++) {
                    var r = e[a = t + n];
                    e[a] = 16711935 & (r << 8 | r >>> 24) | 4278255360 & (r << 24 | r >>> 8)
                }
                n = this._hash.words;
                var o, i, a = e[t + 0],
                    s = (r = e[t + 1], e[t + 2]),
                    l = e[t + 3],
                    c = e[t + 4],
                    d = e[t + 5],
                    u = e[t + 6],
                    f = e[t + 7],
                    p = e[t + 8],
                    h = e[t + 9],
                    g = e[t + 10],
                    m = e[t + 11],
                    v = e[t + 12],
                    b = e[t + 13],
                    y = e[t + 14],
                    w = e[t + 15],
                    k = n[0],
                    x = T(x = T(x = T(x = T(x = E(x = E(x = E(x = E(x = I(x = I(x = I(x = I(x = C(x = C(x = C(x = C(x = n[1], i = C(i = n[2], o = C(o = n[3], k = C(k, x, i, o, a, 7, S[0]), x, i, r, 12, S[1]), k, x, s, 17, S[2]), o, k, l, 22, S[3]), i = C(i, o = C(o, k = C(k, x, i, o, c, 7, S[4]), x, i, d, 12, S[5]), k, x, u, 17, S[6]), o, k, f, 22, S[7]), i = C(i, o = C(o, k = C(k, x, i, o, p, 7, S[8]), x, i, h, 12, S[9]), k, x, g, 17, S[10]), o, k, m, 22, S[11]), i = C(i, o = C(o, k = C(k, x, i, o, v, 7, S[12]), x, i, b, 12, S[13]), k, x, y, 17, S[14]), o, k, w, 22, S[15]), i = I(i, o = I(o, k = I(k, x, i, o, r, 5, S[16]), x, i, u, 9, S[17]), k, x, m, 14, S[18]), o, k, a, 20, S[19]), i = I(i, o = I(o, k = I(k, x, i, o, d, 5, S[20]), x, i, g, 9, S[21]), k, x, w, 14, S[22]), o, k, c, 20, S[23]), i = I(i, o = I(o, k = I(k, x, i, o, h, 5, S[24]), x, i, y, 9, S[25]), k, x, l, 14, S[26]), o, k, p, 20, S[27]), i = I(i, o = I(o, k = I(k, x, i, o, b, 5, S[28]), x, i, s, 9, S[29]), k, x, f, 14, S[30]), o, k, v, 20, S[31]), i = E(i, o = E(o, k = E(k, x, i, o, d, 4, S[32]), x, i, p, 11, S[33]), k, x, m, 16, S[34]), o, k, y, 23, S[35]), i = E(i, o = E(o, k = E(k, x, i, o, r, 4, S[36]), x, i, c, 11, S[37]), k, x, f, 16, S[38]), o, k, g, 23, S[39]), i = E(i, o = E(o, k = E(k, x, i, o, b, 4, S[40]), x, i, a, 11, S[41]), k, x, l, 16, S[42]), o, k, u, 23, S[43]), i = E(i, o = E(o, k = E(k, x, i, o, h, 4, S[44]), x, i, v, 11, S[45]), k, x, w, 16, S[46]), o, k, s, 23, S[47]), i = T(i, o = T(o, k = T(k, x, i, o, a, 6, S[48]), x, i, f, 10, S[49]), k, x, y, 15, S[50]), o, k, d, 21, S[51]), i = T(i, o = T(o, k = T(k, x, i, o, v, 6, S[52]), x, i, l, 10, S[53]), k, x, g, 15, S[54]), o, k, r, 21, S[55]), i = T(i, o = T(o, k = T(k, x, i, o, p, 6, S[56]), x, i, w, 10, S[57]), k, x, u, 15, S[58]), o, k, b, 21, S[59]), i = T(i, o = T(o, k = T(k, x, i, o, c, 6, S[60]), x, i, m, 10, S[61]), k, x, s, 15, S[62]), o, k, h, 21, S[63]);
                n[0] = n[0] + k | 0, n[1] = n[1] + x | 0, n[2] = n[2] + i | 0, n[3] = n[3] + o | 0
            },
            _doFinalize: function () {
                var e = this._data,
                    t = e.words,
                    n = 8 * this._nDataBytes,
                    r = 8 * e.sigBytes;
                t[r >>> 5] |= 128 << 24 - r % 32;
                var o = i.floor(n / 4294967296);
                for (t[15 + (r + 64 >>> 9 << 4)] = 16711935 & (o << 8 | o >>> 24) | 4278255360 & (o << 24 | o >>> 8), t[14 + (r + 64 >>> 9 << 4)] = 16711935 & (n << 8 | n >>> 24) | 4278255360 & (n << 24 | n >>> 8), e.sigBytes = 4 * (t.length + 1), this._process(), t = (e = this._hash).words, n = 0; n < 4; n++) r = t[n], t[n] = 16711935 & (r << 8 | r >>> 24) | 4278255360 & (r << 24 | r >>> 8);
                return e
            },
            clone: function () {
                var e = n.clone.call(this);
                return e._hash = this._hash.clone(), e
            }
        }), e.MD5 = n._createHelper(r), e.HmacMD5 = n._createHmacHelper(r)
    }(Math),
    function () {
        var e, t = CryptoJS,
            n = (e = t.lib).Base,
            c = e.WordArray,
            r = (e = t.algo).EvpKDF = n.extend({
                cfg: n.extend({
                    keySize: 4,
                    hasher: e.MD5,
                    iterations: 1
                }),
                init: function (e) {
                    this.cfg = this.cfg.extend(e)
                },
                compute: function (e, t) {
                    for (var n = (a = this.cfg).hasher.create(), r = c.create(), o = r.words, i = a.keySize, a = a.iterations; o.length < i;) {
                        s && n.update(s);
                        var s = n.update(e).finalize(t);
                        n.reset();
                        for (var l = 1; l < a; l++) s = n.finalize(s), n.reset();
                        r.concat(s)
                    }
                    return r.sigBytes = 4 * i, r
                }
            });
        t.EvpKDF = function (e, t, n) {
            return r.create(n).compute(e, t)
        }
    }(), CryptoJS.lib.Cipher || function () {
        var e = (f = CryptoJS).lib,
            t = e.Base,
            a = e.WordArray,
            n = e.BufferedBlockAlgorithm,
            r = f.enc.Base64,
            o = f.algo.EvpKDF,
            i = e.Cipher = n.extend({
                cfg: t.extend(),
                createEncryptor: function (e, t) {
                    return this.create(this._ENC_XFORM_MODE, e, t)
                },
                createDecryptor: function (e, t) {
                    return this.create(this._DEC_XFORM_MODE, e, t)
                },
                init: function (e, t, n) {
                    this.cfg = this.cfg.extend(n), this._xformMode = e, this._key = t, this.reset()
                },
                reset: function () {
                    n.reset.call(this), this._doReset()
                },
                process: function (e) {
                    return this._append(e), this._process()
                },
                finalize: function (e) {
                    return e && this._append(e), this._doFinalize()
                },
                keySize: 4,
                ivSize: 4,
                _ENC_XFORM_MODE: 1,
                _DEC_XFORM_MODE: 2,
                _createHelper: function (r) {
                    return {
                        encrypt: function (e, t, n) {
                            return ("string" == typeof t ? p : u).encrypt(r, e, t, n)
                        },
                        decrypt: function (e, t, n) {
                            return ("string" == typeof t ? p : u).decrypt(r, e, t, n)
                        }
                    }
                }
            });
        e.StreamCipher = i.extend({
            _doFinalize: function () {
                return this._process(!0)
            },
            blockSize: 1
        });

        function s(e, t, n) {
            var r = this._iv;
            r ? this._iv = void 0 : r = this._prevBlock;
            for (var o = 0; o < n; o++) e[t + o] ^= r[o]
        }
        var l = f.mode = {},
            c = (e.BlockCipherMode = t.extend({
                createEncryptor: function (e, t) {
                    return this.Encryptor.create(e, t)
                },
                createDecryptor: function (e, t) {
                    return this.Decryptor.create(e, t)
                },
                init: function (e, t) {
                    this._cipher = e, this._iv = t
                }
            })).extend();
        c.Encryptor = c.extend({
            processBlock: function (e, t) {
                var n = this._cipher,
                    r = n.blockSize;
                s.call(this, e, t, r), n.encryptBlock(e, t), this._prevBlock = e.slice(t, t + r)
            }
        }), c.Decryptor = c.extend({
            processBlock: function (e, t) {
                var n = this._cipher,
                    r = n.blockSize,
                    o = e.slice(t, t + r);
                n.decryptBlock(e, t), s.call(this, e, t, r), this._prevBlock = o
            }
        }), l = l.CBC = c, c = (f.pad = {}).Pkcs7 = {
            pad: function (e, t) {
                for (var n, r = (n = (n = 4 * t) - e.sigBytes % n) << 24 | n << 16 | n << 8 | n, o = [], i = 0; i < n; i += 4) o.push(r);
                n = a.create(o, n), e.concat(n)
            },
            unpad: function (e) {
                e.sigBytes -= 255 & e.words[e.sigBytes - 1 >>> 2]
            }
        }, e.BlockCipher = i.extend({
            cfg: i.cfg.extend({
                mode: l,
                padding: c
            }),
            reset: function () {
                i.reset.call(this);
                var e = (t = this.cfg).iv,
                    t = t.mode;
                if (this._xformMode == this._ENC_XFORM_MODE) var n = t.createEncryptor;
                else n = t.createDecryptor, this._minBufferSize = 1;
                this._mode = n.call(t, this, e && e.words)
            },
            _doProcessBlock: function (e, t) {
                this._mode.processBlock(e, t)
            },
            _doFinalize: function () {
                var e = this.cfg.padding;
                if (this._xformMode == this._ENC_XFORM_MODE) {
                    e.pad(this._data, this.blockSize);
                    var t = this._process(!0)
                } else t = this._process(!0), e.unpad(t);
                return t
            },
            blockSize: 4
        });
        var d = e.CipherParams = t.extend({
            init: function (e) {
                this.mixIn(e)
            },
            toString: function (e) {
                return (e || this.formatter).stringify(this)
            }
        }),
            u = (l = (f.format = {}).OpenSSL = {
                stringify: function (e) {
                    var t = e.ciphertext;
                    return ((e = e.salt) ? a.create([1398893684, 1701076831]).concat(e).concat(t) : t).toString(r)
                },
                parse: function (e) {
                    var t = (e = r.parse(e)).words;
                    if (1398893684 == t[0] && 1701076831 == t[1]) {
                        var n = a.create(t.slice(2, 4));
                        t.splice(0, 4), e.sigBytes -= 16
                    }
                    return d.create({
                        ciphertext: e,
                        salt: n
                    })
                }
            }, e.SerializableCipher = t.extend({
                cfg: t.extend({
                    format: l
                }),
                encrypt: function (e, t, n, r) {
                    r = this.cfg.extend(r);
                    var o = e.createEncryptor(n, r);
                    return t = o.finalize(t), o = o.cfg, d.create({
                        ciphertext: t,
                        key: n,
                        iv: o.iv,
                        algorithm: e,
                        mode: o.mode,
                        padding: o.padding,
                        blockSize: e.blockSize,
                        formatter: r.format
                    })
                },
                decrypt: function (e, t, n, r) {
                    return r = this.cfg.extend(r), t = this._parse(t, r.format), e.createDecryptor(n, r).finalize(t.ciphertext)
                },
                _parse: function (e, t) {
                    return "string" == typeof e ? t.parse(e, this) : e
                }
            })),
            f = (f.kdf = {}).OpenSSL = {
                execute: function (e, t, n, r) {
                    return r = r || a.random(8), e = o.create({
                        keySize: t + n
                    }).compute(e, r), n = a.create(e.words.slice(t), 4 * n), e.sigBytes = 4 * t, d.create({
                        key: e,
                        iv: n,
                        salt: r
                    })
                }
            },
            p = e.PasswordBasedCipher = u.extend({
                cfg: u.cfg.extend({
                    kdf: f
                }),
                encrypt: function (e, t, n, r) {
                    return n = (r = this.cfg.extend(r)).kdf.execute(n, e.keySize, e.ivSize), r.iv = n.iv, (e = u.encrypt.call(this, e, t, n.key, r)).mixIn(n), e
                },
                decrypt: function (e, t, n, r) {
                    return r = this.cfg.extend(r), t = this._parse(t, r.format), n = r.kdf.execute(n, e.keySize, e.ivSize, t.salt), r.iv = n.iv, u.decrypt.call(this, e, t, n.key, r)
                }
            })
    }(),
    function () {
        for (var e = CryptoJS, t = e.lib.BlockCipher, n = e.algo, a = [], r = [], o = [], i = [], s = [], l = [], c = [], d = [], u = [], f = [], p = [], h = 0; h < 256; h++) p[h] = h < 128 ? h << 1 : h << 1 ^ 283;
        var g = 0,
            m = 0;
        for (h = 0; h < 256; h++) {
            var v = (v = m ^ m << 1 ^ m << 2 ^ m << 3 ^ m << 4) >>> 8 ^ 255 & v ^ 99;
            a[g] = v;
            var b = p[r[v] = g],
                y = p[b],
                w = p[y],
                k = 257 * p[v] ^ 16843008 * v;
            o[g] = k << 24 | k >>> 8, i[g] = k << 16 | k >>> 16, s[g] = k << 8 | k >>> 24, l[g] = k, k = 16843009 * w ^ 65537 * y ^ 257 * b ^ 16843008 * g, c[v] = k << 24 | k >>> 8, d[v] = k << 16 | k >>> 16, u[v] = k << 8 | k >>> 24, f[v] = k, g ? (g = b ^ p[p[p[w ^ b]]], m ^= p[p[m]]) : g = m = 1
        }
        var x = [0, 1, 2, 4, 8, 16, 32, 64, 128, 27, 54];
        n = n.AES = t.extend({
            _doReset: function () {
                for (var e = (n = this._key).words, t = n.sigBytes / 4, n = 4 * ((this._nRounds = t + 6) + 1), r = this._keySchedule = [], o = 0; o < n; o++)
                    if (o < t) r[o] = e[o];
                    else {
                        var i = r[o - 1];
                        o % t ? 6 < t && 4 == o % t && (i = a[i >>> 24] << 24 | a[i >>> 16 & 255] << 16 | a[i >>> 8 & 255] << 8 | a[255 & i]) : (i = a[(i = i << 8 | i >>> 24) >>> 24] << 24 | a[i >>> 16 & 255] << 16 | a[i >>> 8 & 255] << 8 | a[255 & i], i ^= x[o / t | 0] << 24), r[o] = r[o - t] ^ i
                    }
                for (e = this._invKeySchedule = [], t = 0; t < n; t++) o = n - t, i = t % 4 ? r[o] : r[o - 4], e[t] = t < 4 || o <= 4 ? i : c[a[i >>> 24]] ^ d[a[i >>> 16 & 255]] ^ u[a[i >>> 8 & 255]] ^ f[a[255 & i]]
            },
            encryptBlock: function (e, t) {
                this._doCryptBlock(e, t, this._keySchedule, o, i, s, l, a)
            },
            decryptBlock: function (e, t) {
                var n = e[t + 1];
                e[t + 1] = e[t + 3], e[t + 3] = n, this._doCryptBlock(e, t, this._invKeySchedule, c, d, u, f, r), n = e[t + 1], e[t + 1] = e[t + 3], e[t + 3] = n
            },
            _doCryptBlock: function (e, t, n, r, o, i, a, s) {
                for (var l = this._nRounds, c = e[t] ^ n[0], d = e[t + 1] ^ n[1], u = e[t + 2] ^ n[2], f = e[t + 3] ^ n[3], p = 4, h = 1; h < l; h++) {
                    var g = r[c >>> 24] ^ o[d >>> 16 & 255] ^ i[u >>> 8 & 255] ^ a[255 & f] ^ n[p++],
                        m = r[d >>> 24] ^ o[u >>> 16 & 255] ^ i[f >>> 8 & 255] ^ a[255 & c] ^ n[p++],
                        v = r[u >>> 24] ^ o[f >>> 16 & 255] ^ i[c >>> 8 & 255] ^ a[255 & d] ^ n[p++];
                    f = r[f >>> 24] ^ o[c >>> 16 & 255] ^ i[d >>> 8 & 255] ^ a[255 & u] ^ n[p++], c = g, d = m, u = v
                }
                g = (s[c >>> 24] << 24 | s[d >>> 16 & 255] << 16 | s[u >>> 8 & 255] << 8 | s[255 & f]) ^ n[p++], m = (s[d >>> 24] << 24 | s[u >>> 16 & 255] << 16 | s[f >>> 8 & 255] << 8 | s[255 & c]) ^ n[p++], v = (s[u >>> 24] << 24 | s[f >>> 16 & 255] << 16 | s[c >>> 8 & 255] << 8 | s[255 & d]) ^ n[p++], f = (s[f >>> 24] << 24 | s[c >>> 16 & 255] << 16 | s[d >>> 8 & 255] << 8 | s[255 & u]) ^ n[p++], e[t] = g, e[t + 1] = m, e[t + 2] = v, e[t + 3] = f
            },
            keySize: 8
        });
        e.AES = t._createHelper(n)
    }(),
    function (e, t, n) {
        "use strict";
        "undefined" != typeof module && module.exports ? module.exports = n() : "function" == typeof define && define.amd ? define(n) : t.Fingerprint2 = n()
    }(0, this, function () {
        "use strict";
        Array.prototype.indexOf || (Array.prototype.indexOf = function (e, t) {
            var n;
            if (null == this) throw new TypeError("'this' is null or undefined");
            var r = Object(this),
                o = r.length >>> 0;
            if (0 == o) return -1;
            var i = +t || 0;
            if (Math.abs(i) === 1 / 0 && (i = 0), o <= i) return -1;
            for (n = Math.max(0 <= i ? i : o - Math.abs(i), 0); n < o;) {
                if (n in r && r[n] === e) return n;
                n++
            }
            return -1
        });

        function e(e) {
            this.options = this.extend(e, {
                swfContainerId: "fingerprintjs2",
                swfPath: "flash/compiled/FontList.swf",
                detectScreenOrientation: !0,
                sortPluginsFor: [/palemoon/i]
            }), this.nativeForEach = Array.prototype.forEach, this.nativeMap = Array.prototype.map
        }
        return e.prototype = {
            extend: function (e, t) {
                if (null == e) return t;
                for (var n in e) null != e[n] && t[n] !== e[n] && (t[n] = e[n]);
                return t
            },
            log: function (e) {
                window.console
            },
            get: function (r) {
                var e = [];
                e = this.userAgentKey(e), e = this.languageKey(e), e = this.colorDepthKey(e), e = this.screenResolutionKey(e), e = this.availableScreenResolutionKey(e), e = this.timezoneOffsetKey(e), e = this.sessionStorageKey(e), e = this.localStorageKey(e), e = this.indexedDbKey(e), e = this.addBehaviorKey(e), e = this.openDatabaseKey(e), e = this.cpuClassKey(e), e = this.platformKey(e), e = this.doNotTrackKey(e), e = this.pluginsKey(e), e = this.canvasKey(e), e = this.webglKey(e), e = this.adBlockKey(e), e = this.hasLiedLanguagesKey(e), e = this.hasLiedResolutionKey(e), e = this.hasLiedOsKey(e), e = this.hasLiedBrowserKey(e), e = this.touchSupportKey(e);
                var o = this;
                this.fontsKey(e, function (e) {
                    var n = [];
                    o.each(e, function (e) {
                        var t = e.value;
                        e.value && (void 0 !== e.value.join && (t = e.value.join(";")), n.push(t))
                    });
                    var t = o.x64hash128(n.join("~~~"), 31);
                    return r(t, e)
                })
            },
            userAgentKey: function (e) {
                return this.options.excludeUserAgent || e.push({
                    key: "user_agent",
                    value: this.getUserAgent()
                }), e
            },
            getUserAgent: function () {
                return navigator.userAgent
            },
            languageKey: function (e) {
                return this.options.excludeLanguage || e.push({
                    key: "language",
                    value: navigator.language || navigator.userLanguage || navigator.browserLanguage || navigator.systemLanguage
                }), e
            },
            colorDepthKey: function (e) {
                return this.options.excludeColorDepth || e.push({
                    key: "color_depth",
                    value: screen.colorDepth
                }), e
            },
            screenResolutionKey: function (e) {
                return this.options.excludeScreenResolution ? e : this.getScreenResolution(e)
            },
            getScreenResolution: function (e) {
                var t;
                return void 0 !== (t = this.options.detectScreenOrientation && screen.height > screen.width ? [screen.height, screen.width] : [screen.width, screen.height]) && e.push({
                    key: "resolution",
                    value: t
                }), e
            },
            availableScreenResolutionKey: function (e) {
                return this.options.excludeAvailableScreenResolution ? e : this.getAvailableScreenResolution(e)
            },
            getAvailableScreenResolution: function (e) {
                var t;
                return screen.availWidth && screen.availHeight && (t = this.options.detectScreenOrientation ? screen.availHeight > screen.availWidth ? [screen.availHeight, screen.availWidth] : [screen.availWidth, screen.availHeight] : [screen.availHeight, screen.availWidth]), void 0 !== t && e.push({
                    key: "available_resolution",
                    value: t
                }), e
            },
            timezoneOffsetKey: function (e) {
                return this.options.excludeTimezoneOffset || e.push({
                    key: "timezone_offset",
                    value: (new Date).getTimezoneOffset()
                }), e
            },
            sessionStorageKey: function (e) {
                return !this.options.excludeSessionStorage && this.hasSessionStorage() && e.push({
                    key: "session_storage",
                    value: 1
                }), e
            },
            localStorageKey: function (e) {
                return !this.options.excludeSessionStorage && this.hasLocalStorage() && e.push({
                    key: "local_storage",
                    value: 1
                }), e
            },
            indexedDbKey: function (e) {
                return !this.options.excludeIndexedDB && this.hasIndexedDB() && e.push({
                    key: "indexed_db",
                    value: 1
                }), e
            },
            addBehaviorKey: function (e) {
                return document.body && !this.options.excludeAddBehavior && document.body.addBehavior && e.push({
                    key: "add_behavior",
                    value: 1
                }), e
            },
            openDatabaseKey: function (e) {
                return !this.options.excludeOpenDatabase && window.openDatabase && e.push({
                    key: "open_database",
                    value: 1
                }), e
            },
            cpuClassKey: function (e) {
                return this.options.excludeCpuClass || e.push({
                    key: "cpu_class",
                    value: this.getNavigatorCpuClass()
                }), e
            },
            platformKey: function (e) {
                return this.options.excludePlatform || e.push({
                    key: "navigator_platform",
                    value: this.getNavigatorPlatform()
                }), e
            },
            doNotTrackKey: function (e) {
                return this.options.excludeDoNotTrack || e.push({
                    key: "do_not_track",
                    value: this.getDoNotTrack()
                }), e
            },
            canvasKey: function (e) {
                return !this.options.excludeCanvas && this.isCanvasSupported() && e.push({
                    key: "canvas",
                    value: this.getCanvasFp()
                }), e
            },
            webglKey: function (e) {
                return this.options.excludeWebGL ? "undefined" == typeof NODEBUG && this.log("Skipping WebGL fingerprinting per excludeWebGL configuration option") : this.isWebGlSupported() ? e.push({
                    key: "webgl",
                    value: this.getWebglFp()
                }) : "undefined" == typeof NODEBUG && this.log("Skipping WebGL fingerprinting because it is not supported in this browser"), e
            },
            adBlockKey: function (e) {
                return this.options.excludeAdBlock || e.push({
                    key: "adblock",
                    value: this.getAdBlock()
                }), e
            },
            hasLiedLanguagesKey: function (e) {
                return this.options.excludeHasLiedLanguages || e.push({
                    key: "has_lied_languages",
                    value: this.getHasLiedLanguages()
                }), e
            },
            hasLiedResolutionKey: function (e) {
                return this.options.excludeHasLiedResolution || e.push({
                    key: "has_lied_resolution",
                    value: this.getHasLiedResolution()
                }), e
            },
            hasLiedOsKey: function (e) {
                return this.options.excludeHasLiedOs || e.push({
                    key: "has_lied_os",
                    value: this.getHasLiedOs()
                }), e
            },
            hasLiedBrowserKey: function (e) {
                return this.options.excludeHasLiedBrowser || e.push({
                    key: "has_lied_browser",
                    value: this.getHasLiedBrowser()
                }), e
            },
            fontsKey: function (e, t) {
                return this.options.excludeJsFonts ? this.flashFontsKey(e, t) : this.jsFontsKey(e, t)
            },
            flashFontsKey: function (t, n) {
                return this.options.excludeFlashFonts ? ("undefined" == typeof NODEBUG && this.log("Skipping flash fonts detection per excludeFlashFonts configuration option"), n(t)) : this.hasSwfObjectLoaded() ? this.hasMinFlashInstalled() ? void 0 === this.options.swfPath ? ("undefined" == typeof NODEBUG && this.log("To use Flash fonts detection, you must pass a valid swfPath option, skipping Flash fonts enumeration"), n(t)) : void this.loadSwfAndDetectFonts(function (e) {
                    t.push({
                        key: "swf_fonts",
                        value: e.join(";")
                    }), n(t)
                }) : ("undefined" == typeof NODEBUG && this.log("Flash is not installed, skipping Flash fonts enumeration"), n(t)) : ("undefined" == typeof NODEBUG && this.log("Swfobject is not detected, Flash fonts enumeration is skipped"), n(t))
            },
            jsFontsKey: function (f, p) {
                var h = this;
                return setTimeout(function () {
                    var i = ["monospace", "sans-serif", "serif"],
                        a = document.getElementsByTagName("body")[0],
                        s = document.createElement("span");
                    s.style.fontSize = "72px", s.innerHTML = "mmmmmmmmmmlli";
                    for (var l = {}, c = {}, e = 0, t = i.length; e < t; e++) s.style.fontFamily = i[e], a.appendChild(s), l[i[e]] = s.offsetWidth, c[i[e]] = s.offsetHeight, a.removeChild(s);

                    function n(e) {
                        for (var t = !1, n = 0, r = i.length; n < r; n++) {
                            s.style.fontFamily = e + "," + i[n], a.appendChild(s);
                            var o = s.offsetWidth !== l[i[n]] || s.offsetHeight !== c[i[n]];
                            a.removeChild(s), t = t || o
                        }
                        return t
                    }
                    var r = ["Andale Mono", "Arial", "Arial Black", "Arial Hebrew", "Arial MT", "Arial Narrow", "Arial Rounded MT Bold", "Arial Unicode MS", "Bitstream Vera Sans Mono", "Book Antiqua", "Bookman Old Style", "Calibri", "Cambria", "Cambria Math", "Century", "Century Gothic", "Century Schoolbook", "Comic Sans", "Comic Sans MS", "Consolas", "Courier", "Courier New", "Garamond", "Geneva", "Georgia", "Helvetica", "Helvetica Neue", "Impact", "Lucida Bright", "Lucida Calligraphy", "Lucida Console", "Lucida Fax", "LUCIDA GRANDE", "Lucida Handwriting", "Lucida Sans", "Lucida Sans Typewriter", "Lucida Sans Unicode", "Microsoft Sans Serif", "Monaco", "Monotype Corsiva", "MS Gothic", "MS Outlook", "MS PGothic", "MS Reference Sans Serif", "MS Sans Serif", "MS Serif", "MYRIAD", "MYRIAD PRO", "Palatino", "Palatino Linotype", "Segoe Print", "Segoe Script", "Segoe UI", "Segoe UI Light", "Segoe UI Semibold", "Segoe UI Symbol", "Tahoma", "Times", "Times New Roman", "Times New Roman PS", "Trebuchet MS", "Verdana", "Wingdings", "Wingdings 2", "Wingdings 3"];
                    h.options.extendedJsFonts && (r = r.concat(["Abadi MT Condensed Light", "Academy Engraved LET", "ADOBE CASLON PRO", "Adobe Garamond", "ADOBE GARAMOND PRO", "Agency FB", "Aharoni", "Albertus Extra Bold", "Albertus Medium", "Algerian", "Amazone BT", "American Typewriter", "American Typewriter Condensed", "AmerType Md BT", "Andalus", "Angsana New", "AngsanaUPC", "Antique Olive", "Aparajita", "Apple Chancery", "Apple Color Emoji", "Apple SD Gothic Neo", "Arabic Typesetting", "ARCHER", "ARNO PRO", "Arrus BT", "Aurora Cn BT", "AvantGarde Bk BT", "AvantGarde Md BT", "AVENIR", "Ayuthaya", "Bandy", "Bangla Sangam MN", "Bank Gothic", "BankGothic Md BT", "Baskerville", "Baskerville Old Face", "Batang", "BatangChe", "Bauer Bodoni", "Bauhaus 93", "Bazooka", "Bell MT", "Bembo", "Benguiat Bk BT", "Berlin Sans FB", "Berlin Sans FB Demi", "Bernard MT Condensed", "BernhardFashion BT", "BernhardMod BT", "Big Caslon", "BinnerD", "Blackadder ITC", "BlairMdITC TT", "Bodoni 72", "Bodoni 72 Oldstyle", "Bodoni 72 Smallcaps", "Bodoni MT", "Bodoni MT Black", "Bodoni MT Condensed", "Bodoni MT Poster Compressed", "Bookshelf Symbol 7", "Boulder", "Bradley Hand", "Bradley Hand ITC", "Bremen Bd BT", "Britannic Bold", "Broadway", "Browallia New", "BrowalliaUPC", "Brush Script MT", "Californian FB", "Calisto MT", "Calligrapher", "Candara", "CaslonOpnface BT", "Castellar", "Centaur", "Cezanne", "CG Omega", "CG Times", "Chalkboard", "Chalkboard SE", "Chalkduster", "Charlesworth", "Charter Bd BT", "Charter BT", "Chaucer", "ChelthmITC Bk BT", "Chiller", "Clarendon", "Clarendon Condensed", "CloisterBlack BT", "Cochin", "Colonna MT", "Constantia", "Cooper Black", "Copperplate", "Copperplate Gothic", "Copperplate Gothic Bold", "Copperplate Gothic Light", "CopperplGoth Bd BT", "Corbel", "Cordia New", "CordiaUPC", "Cornerstone", "Coronet", "Cuckoo", "Curlz MT", "DaunPenh", "Dauphin", "David", "DB LCD Temp", "DELICIOUS", "Denmark", "DFKai-SB", "Didot", "DilleniaUPC", "DIN", "DokChampa", "Dotum", "DotumChe", "Ebrima", "Edwardian Script ITC", "Elephant", "English 111 Vivace BT", "Engravers MT", "EngraversGothic BT", "Eras Bold ITC", "Eras Demi ITC", "Eras Light ITC", "Eras Medium ITC", "EucrosiaUPC", "Euphemia", "Euphemia UCAS", "EUROSTILE", "Exotc350 Bd BT", "FangSong", "Felix Titling", "Fixedsys", "FONTIN", "Footlight MT Light", "Forte", "FrankRuehl", "Fransiscan", "Freefrm721 Blk BT", "FreesiaUPC", "Freestyle Script", "French Script MT", "FrnkGothITC Bk BT", "Fruitger", "FRUTIGER", "Futura", "Futura Bk BT", "Futura Lt BT", "Futura Md BT", "Futura ZBlk BT", "FuturaBlack BT", "Gabriola", "Galliard BT", "Gautami", "Geeza Pro", "Geometr231 BT", "Geometr231 Hv BT", "Geometr231 Lt BT", "GeoSlab 703 Lt BT", "GeoSlab 703 XBd BT", "Gigi", "Gill Sans", "Gill Sans MT", "Gill Sans MT Condensed", "Gill Sans MT Ext Condensed Bold", "Gill Sans Ultra Bold", "Gill Sans Ultra Bold Condensed", "Gisha", "Gloucester MT Extra Condensed", "GOTHAM", "GOTHAM BOLD", "Goudy Old Style", "Goudy Stout", "GoudyHandtooled BT", "GoudyOLSt BT", "Gujarati Sangam MN", "Gulim", "GulimChe", "Gungsuh", "GungsuhChe", "Gurmukhi MN", "Haettenschweiler", "Harlow Solid Italic", "Harrington", "Heather", "Heiti SC", "Heiti TC", "HELV", "Herald", "High Tower Text", "Hiragino Kaku Gothic ProN", "Hiragino Mincho ProN", "Hoefler Text", "Humanst 521 Cn BT", "Humanst521 BT", "Humanst521 Lt BT", "Imprint MT Shadow", "Incised901 Bd BT", "Incised901 BT", "Incised901 Lt BT", "INCONSOLATA", "Informal Roman", "Informal011 BT", "INTERSTATE", "IrisUPC", "Iskoola Pota", "JasmineUPC", "Jazz LET", "Jenson", "Jester", "Jokerman", "Juice ITC", "Kabel Bk BT", "Kabel Ult BT", "Kailasa", "KaiTi", "Kalinga", "Kannada Sangam MN", "Kartika", "Kaufmann Bd BT", "Kaufmann BT", "Khmer UI", "KodchiangUPC", "Kokila", "Korinna BT", "Kristen ITC", "Krungthep", "Kunstler Script", "Lao UI", "Latha", "Leelawadee", "Letter Gothic", "Levenim MT", "LilyUPC", "Lithograph", "Lithograph Light", "Long Island", "Lydian BT", "Magneto", "Maiandra GD", "Malayalam Sangam MN", "Malgun Gothic", "Mangal", "Marigold", "Marion", "Marker Felt", "Market", "Marlett", "Matisse ITC", "Matura MT Script Capitals", "Meiryo", "Meiryo UI", "Microsoft Himalaya", "Microsoft JhengHei", "Microsoft New Tai Lue", "Microsoft PhagsPa", "Microsoft Tai Le", "Microsoft Uighur", "Microsoft YaHei", "Microsoft Yi Baiti", "MingLiU", "MingLiU_HKSCS", "MingLiU_HKSCS-ExtB", "MingLiU-ExtB", "Minion", "Minion Pro", "Miriam", "Miriam Fixed", "Mistral", "Modern", "Modern No. 20", "Mona Lisa Solid ITC TT", "Mongolian Baiti", "MONO", "MoolBoran", "Mrs Eaves", "MS LineDraw", "MS Mincho", "MS PMincho", "MS Reference Specialty", "MS UI Gothic", "MT Extra", "MUSEO", "MV Boli", "Nadeem", "Narkisim", "NEVIS", "News Gothic", "News GothicMT", "NewsGoth BT", "Niagara Engraved", "Niagara Solid", "Noteworthy", "NSimSun", "Nyala", "OCR A Extended", "Old Century", "Old English Text MT", "Onyx", "Onyx BT", "OPTIMA", "Oriya Sangam MN", "OSAKA", "OzHandicraft BT", "Palace Script MT", "Papyrus", "Parchment", "Party LET", "Pegasus", "Perpetua", "Perpetua Titling MT", "PetitaBold", "Pickwick", "Plantagenet Cherokee", "Playbill", "PMingLiU", "PMingLiU-ExtB", "Poor Richard", "Poster", "PosterBodoni BT", "PRINCETOWN LET", "Pristina", "PTBarnum BT", "Pythagoras", "Raavi", "Rage Italic", "Ravie", "Ribbon131 Bd BT", "Rockwell", "Rockwell Condensed", "Rockwell Extra Bold", "Rod", "Roman", "Sakkal Majalla", "Santa Fe LET", "Savoye LET", "Sceptre", "Script", "Script MT Bold", "SCRIPTINA", "Serifa", "Serifa BT", "Serifa Th BT", "ShelleyVolante BT", "Sherwood", "Shonar Bangla", "Showcard Gothic", "Shruti", "Signboard", "SILKSCREEN", "SimHei", "Simplified Arabic", "Simplified Arabic Fixed", "SimSun", "SimSun-ExtB", "Sinhala Sangam MN", "Sketch Rockwell", "Skia", "Small Fonts", "Snap ITC", "Snell Roundhand", "Socket", "Souvenir Lt BT", "Staccato222 BT", "Steamer", "Stencil", "Storybook", "Styllo", "Subway", "Swis721 BlkEx BT", "Swiss911 XCm BT", "Sylfaen", "Synchro LET", "System", "Tamil Sangam MN", "Technical", "Teletype", "Telugu Sangam MN", "Tempus Sans ITC", "Terminal", "Thonburi", "Traditional Arabic", "Trajan", "TRAJAN PRO", "Tristan", "Tubular", "Tunga", "Tw Cen MT", "Tw Cen MT Condensed", "Tw Cen MT Condensed Extra Bold", "TypoUpright BT", "Unicorn", "Univers", "Univers CE 55 Medium", "Univers Condensed", "Utsaah", "Vagabond", "Vani", "Vijaya", "Viner Hand ITC", "VisualUI", "Vivaldi", "Vladimir Script", "Vrinda", "Westminster", "WHITNEY", "Wide Latin", "ZapfEllipt BT", "ZapfHumnst BT", "ZapfHumnst Dm BT", "Zapfino", "Zurich BlkEx BT", "Zurich Ex BT", "ZWAdobeF"]));
                    for (var o = [], d = 0, u = r.length; d < u; d++) n(r[d]) && o.push(r[d]);
                    f.push({
                        key: "js_fonts",
                        value: o
                    }), p(f)
                }, 1)
            },
            pluginsKey: function (e) {
                return this.options.excludePlugins || (this.isIE() ? e.push({
                    key: "ie_plugins",
                    value: this.getIEPlugins()
                }) : e.push({
                    key: "regular_plugins",
                    value: this.getRegularPlugins()
                })), e
            },
            getRegularPlugins: function () {
                for (var e = [], t = 0, n = navigator.plugins.length; t < n; t++) e.push(navigator.plugins[t]);
                return this.pluginsShouldBeSorted() && (e = e.sort(function (e, t) {
                    return e.name > t.name ? 1 : e.name < t.name ? -1 : 0
                })), this.map(e, function (e) {
                    var t = this.map(e, function (e) {
                        return [e.type, e.suffixes].join("~")
                    }).join(",");
                    return [e.name, e.description, t].join("::")
                }, this)
            },
            getIEPlugins: function () {
                var e = [];
                if (Object.getOwnPropertyDescriptor && Object.getOwnPropertyDescriptor(window, "ActiveXObject") || "ActiveXObject" in window) {
                    e = this.map(["AcroPDF.PDF", "Adodb.Stream", "AgControl.AgControl", "DevalVRXCtrl.DevalVRXCtrl.1", "MacromediaFlashPaper.MacromediaFlashPaper", "Msxml2.DOMDocument", "Msxml2.XMLHTTP", "PDF.PdfCtrl", "QuickTime.QuickTime", "QuickTimeCheckObject.QuickTimeCheck.1", "RealPlayer", "RealPlayer.RealPlayer(tm) ActiveX Control (32-bit)", "RealVideo.RealVideo(tm) ActiveX Control (32-bit)", "Scripting.Dictionary", "SWCtl.SWCtl", "Shell.UIHelper", "ShockwaveFlash.ShockwaveFlash", "Skype.Detection", "TDCCtl.TDCCtl", "WMPlayer.OCX", "rmocx.RealPlayer G2 Control", "rmocx.RealPlayer G2 Control.1"], function (e) {
                        try {
                            return new ActiveXObject(e), e
                        } catch (e) {
                            return null
                        }
                    })
                }
                return navigator.plugins && (e = e.concat(this.getRegularPlugins())), e
            },
            pluginsShouldBeSorted: function () {
                for (var e = !1, t = 0, n = this.options.sortPluginsFor.length; t < n; t++) {
                    var r = this.options.sortPluginsFor[t];
                    if (navigator.userAgent.match(r)) {
                        e = !0;
                        break
                    }
                }
                return e
            },
            touchSupportKey: function (e) {
                return this.options.excludeTouchSupport || e.push({
                    key: "touch_support",
                    value: this.getTouchSupport()
                }), e
            },
            hasSessionStorage: function () {
                try {
                    return !!window.sessionStorage
                } catch (e) {
                    return !0
                }
            },
            hasLocalStorage: function () {
                try {
                    return !!window.localStorage
                } catch (e) {
                    return !0
                }
            },
            hasIndexedDB: function () {
                return !!window.indexedDB
            },
            getNavigatorCpuClass: function () {
                return navigator.cpuClass ? navigator.cpuClass : "unknown"
            },
            getNavigatorPlatform: function () {
                return navigator.platform ? navigator.platform : "unknown"
            },
            getDoNotTrack: function () {
                return navigator.doNotTrack ? navigator.doNotTrack : "unknown"
            },
            getTouchSupport: function () {
                var e = 0,
                    t = !1;
                void 0 !== navigator.maxTouchPoints ? e = navigator.maxTouchPoints : void 0 !== navigator.msMaxTouchPoints && (e = navigator.msMaxTouchPoints);
                try {
                    document.createEvent("TouchEvent"), t = !0
                } catch (e) { }
                return [e, t, "ontouchstart" in window]
            },
            getCanvasFp: function () {
                var e = [],
                    t = document.createElement("canvas");
                t.width = 2e3, t.height = 200, t.style.display = "inline";
                var n = t.getContext("2d");
                return n.rect(0, 0, 10, 10), n.rect(2, 2, 6, 6), e.push("canvas winding:" + (!1 === n.isPointInPath(5, 5, "evenodd") ? "yes" : "no")), n.textBaseline = "alphabetic", n.fillStyle = "#f60", n.fillRect(125, 1, 62, 20), n.fillStyle = "#069", this.options.dontUseFakeFontInCanvas ? n.font = "11pt Arial" : n.font = "11pt no-real-font-123", n.fillText("Cwm fjordbank glyphs vext quiz, 😃", 2, 15), n.fillStyle = "rgba(102, 204, 0, 0.7)", n.font = "18pt Arial", n.fillText("Cwm fjordbank glyphs vext quiz, 😃", 4, 45), n.globalCompositeOperation = "multiply", n.fillStyle = "rgb(255,0,255)", n.beginPath(), n.arc(50, 50, 50, 0, 2 * Math.PI, !0), n.closePath(), n.fill(), n.fillStyle = "rgb(0,255,255)", n.beginPath(), n.arc(100, 50, 50, 0, 2 * Math.PI, !0), n.closePath(), n.fill(), n.fillStyle = "rgb(255,255,0)", n.beginPath(), n.arc(75, 100, 50, 0, 2 * Math.PI, !0), n.closePath(), n.fill(), n.fillStyle = "rgb(255,0,255)", n.arc(75, 75, 75, 0, 2 * Math.PI, !0), n.arc(75, 75, 25, 0, 2 * Math.PI, !0), n.fill("evenodd"), e.push("canvas fp:" + t.toDataURL()), e.join("~")
            },
            getWebglFp: function () {
                function e(e) {
                    return t.clearColor(0, 0, 0, 1), t.enable(t.DEPTH_TEST), t.depthFunc(t.LEQUAL), t.clear(t.COLOR_BUFFER_BIT | t.DEPTH_BUFFER_BIT), "[" + e[0] + ", " + e[1] + "]"
                }
                var t;
                if (!(t = this.getWebglCanvas())) return null;
                var n = [],
                    r = t.createBuffer();
                t.bindBuffer(t.ARRAY_BUFFER, r);
                var o = new Float32Array([-.2, -.9, 0, .4, -.26, 0, 0, .732134444, 0]);
                t.bufferData(t.ARRAY_BUFFER, o, t.STATIC_DRAW), r.itemSize = 3, r.numItems = 3;
                var i = t.createProgram(),
                    a = t.createShader(t.VERTEX_SHADER);
                t.shaderSource(a, "attribute vec2 attrVertex;varying vec2 varyinTexCoordinate;uniform vec2 uniformOffset;void main(){varyinTexCoordinate=attrVertex+uniformOffset;gl_Position=vec4(attrVertex,0,1);}"), t.compileShader(a);
                var s, l, c, d = t.createShader(t.FRAGMENT_SHADER);
                return t.shaderSource(d, "precision mediump float;varying vec2 varyinTexCoordinate;void main() {gl_FragColor=vec4(varyinTexCoordinate,0,1);}"), t.compileShader(d), t.attachShader(i, a), t.attachShader(i, d), t.linkProgram(i), t.useProgram(i), i.vertexPosAttrib = t.getAttribLocation(i, "attrVertex"), i.offsetUniform = t.getUniformLocation(i, "uniformOffset"), t.enableVertexAttribArray(i.vertexPosArray), t.vertexAttribPointer(i.vertexPosAttrib, r.itemSize, t.FLOAT, !1, 0, 0), t.uniform2f(i.offsetUniform, 1, 1), t.drawArrays(t.TRIANGLE_STRIP, 0, r.numItems), null != t.canvas && n.push(t.canvas.toDataURL()), n.push("extensions:" + t.getSupportedExtensions().join(";")), n.push("webgl aliased line width range:" + e(t.getParameter(t.ALIASED_LINE_WIDTH_RANGE))), n.push("webgl aliased point size range:" + e(t.getParameter(t.ALIASED_POINT_SIZE_RANGE))), n.push("webgl alpha bits:" + t.getParameter(t.ALPHA_BITS)), n.push("webgl antialiasing:" + (t.getContextAttributes().antialias ? "yes" : "no")), n.push("webgl blue bits:" + t.getParameter(t.BLUE_BITS)), n.push("webgl depth bits:" + t.getParameter(t.DEPTH_BITS)), n.push("webgl green bits:" + t.getParameter(t.GREEN_BITS)), n.push("webgl max anisotropy:" + ((c = (s = t).getExtension("EXT_texture_filter_anisotropic") || s.getExtension("WEBKIT_EXT_texture_filter_anisotropic") || s.getExtension("MOZ_EXT_texture_filter_anisotropic")) ? (0 === (l = s.getParameter(c.MAX_TEXTURE_MAX_ANISOTROPY_EXT)) && (l = 2), l) : null)), n.push("webgl max combined texture image units:" + t.getParameter(t.MAX_COMBINED_TEXTURE_IMAGE_UNITS)), n.push("webgl max cube map texture size:" + t.getParameter(t.MAX_CUBE_MAP_TEXTURE_SIZE)), n.push("webgl max fragment uniform vectors:" + t.getParameter(t.MAX_FRAGMENT_UNIFORM_VECTORS)), n.push("webgl max render buffer size:" + t.getParameter(t.MAX_RENDERBUFFER_SIZE)), n.push("webgl max texture image units:" + t.getParameter(t.MAX_TEXTURE_IMAGE_UNITS)), n.push("webgl max texture size:" + t.getParameter(t.MAX_TEXTURE_SIZE)), n.push("webgl max varying vectors:" + t.getParameter(t.MAX_VARYING_VECTORS)), n.push("webgl max vertex attribs:" + t.getParameter(t.MAX_VERTEX_ATTRIBS)), n.push("webgl max vertex texture image units:" + t.getParameter(t.MAX_VERTEX_TEXTURE_IMAGE_UNITS)), n.push("webgl max vertex uniform vectors:" + t.getParameter(t.MAX_VERTEX_UNIFORM_VECTORS)), n.push("webgl max viewport dims:" + e(t.getParameter(t.MAX_VIEWPORT_DIMS))), n.push("webgl red bits:" + t.getParameter(t.RED_BITS)), n.push("webgl renderer:" + t.getParameter(t.RENDERER)), n.push("webgl shading language version:" + t.getParameter(t.SHADING_LANGUAGE_VERSION)), n.push("webgl stencil bits:" + t.getParameter(t.STENCIL_BITS)), n.push("webgl vendor:" + t.getParameter(t.VENDOR)), n.push("webgl version:" + t.getParameter(t.VERSION)), t.getShaderPrecisionFormat ? (n.push("webgl vertex shader high float precision:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.HIGH_FLOAT).precision), n.push("webgl vertex shader high float precision rangeMin:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.HIGH_FLOAT).rangeMin), n.push("webgl vertex shader high float precision rangeMax:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.HIGH_FLOAT).rangeMax), n.push("webgl vertex shader medium float precision:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.MEDIUM_FLOAT).precision), n.push("webgl vertex shader medium float precision rangeMin:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.MEDIUM_FLOAT).rangeMin), n.push("webgl vertex shader medium float precision rangeMax:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.MEDIUM_FLOAT).rangeMax), n.push("webgl vertex shader low float precision:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.LOW_FLOAT).precision), n.push("webgl vertex shader low float precision rangeMin:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.LOW_FLOAT).rangeMin), n.push("webgl vertex shader low float precision rangeMax:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.LOW_FLOAT).rangeMax), n.push("webgl fragment shader high float precision:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.HIGH_FLOAT).precision), n.push("webgl fragment shader high float precision rangeMin:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.HIGH_FLOAT).rangeMin), n.push("webgl fragment shader high float precision rangeMax:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.HIGH_FLOAT).rangeMax), n.push("webgl fragment shader medium float precision:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.MEDIUM_FLOAT).precision), n.push("webgl fragment shader medium float precision rangeMin:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.MEDIUM_FLOAT).rangeMin), n.push("webgl fragment shader medium float precision rangeMax:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.MEDIUM_FLOAT).rangeMax), n.push("webgl fragment shader low float precision:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.LOW_FLOAT).precision), n.push("webgl fragment shader low float precision rangeMin:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.LOW_FLOAT).rangeMin), n.push("webgl fragment shader low float precision rangeMax:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.LOW_FLOAT).rangeMax), n.push("webgl vertex shader high int precision:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.HIGH_INT).precision), n.push("webgl vertex shader high int precision rangeMin:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.HIGH_INT).rangeMin), n.push("webgl vertex shader high int precision rangeMax:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.HIGH_INT).rangeMax), n.push("webgl vertex shader medium int precision:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.MEDIUM_INT).precision), n.push("webgl vertex shader medium int precision rangeMin:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.MEDIUM_INT).rangeMin), n.push("webgl vertex shader medium int precision rangeMax:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.MEDIUM_INT).rangeMax), n.push("webgl vertex shader low int precision:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.LOW_INT).precision), n.push("webgl vertex shader low int precision rangeMin:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.LOW_INT).rangeMin), n.push("webgl vertex shader low int precision rangeMax:" + t.getShaderPrecisionFormat(t.VERTEX_SHADER, t.LOW_INT).rangeMax), n.push("webgl fragment shader high int precision:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.HIGH_INT).precision), n.push("webgl fragment shader high int precision rangeMin:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.HIGH_INT).rangeMin), n.push("webgl fragment shader high int precision rangeMax:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.HIGH_INT).rangeMax), n.push("webgl fragment shader medium int precision:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.MEDIUM_INT).precision), n.push("webgl fragment shader medium int precision rangeMin:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.MEDIUM_INT).rangeMin), n.push("webgl fragment shader medium int precision rangeMax:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.MEDIUM_INT).rangeMax), n.push("webgl fragment shader low int precision:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.LOW_INT).precision), n.push("webgl fragment shader low int precision rangeMin:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.LOW_INT).rangeMin), n.push("webgl fragment shader low int precision rangeMax:" + t.getShaderPrecisionFormat(t.FRAGMENT_SHADER, t.LOW_INT).rangeMax)) : "undefined" == typeof NODEBUG && this.log("WebGL fingerprinting is incomplete, because your browser does not support getShaderPrecisionFormat"), n.join("~")
            },
            getAdBlock: function () {
                var e = document.createElement("div");
                e.setAttribute("id", "ads");
                try {
                    return document.body.appendChild(e), !document.getElementById("ads")
                } catch (e) {
                    return !1
                }
            },
            getHasLiedLanguages: function () {
                if (void 0 !== navigator.languages) try {
                    if (navigator.languages[0].substr(0, 2) !== navigator.language.substr(0, 2)) return !0
                } catch (e) {
                    return !0
                }
                return !1
            },
            getHasLiedResolution: function () {
                return screen.width < screen.availWidth || screen.height < screen.availHeight
            },
            getHasLiedOs: function () {
                var e, t = navigator.userAgent.toLowerCase(),
                    n = navigator.oscpu,
                    r = navigator.platform.toLowerCase();
                if (e = 0 <= t.indexOf("windows phone") ? "Windows Phone" : 0 <= t.indexOf("win") ? "Windows" : 0 <= t.indexOf("android") ? "Android" : 0 <= t.indexOf("linux") ? "Linux" : 0 <= t.indexOf("iphone") || 0 <= t.indexOf("ipad") ? "iOS" : 0 <= t.indexOf("mac") ? "Mac" : "Other", ("ontouchstart" in window || 0 < navigator.maxTouchPoints || 0 < navigator.msMaxTouchPoints) && "Windows Phone" !== e && "Android" !== e && "iOS" !== e && "Other" !== e) return !0;
                if (void 0 !== n) {
                    if (0 <= (n = n.toLowerCase()).indexOf("win") && "Windows" !== e && "Windows Phone" !== e) return !0;
                    if (0 <= n.indexOf("linux") && "Linux" !== e && "Android" !== e) return !0;
                    if (0 <= n.indexOf("mac") && "Mac" !== e && "iOS" !== e) return !0;
                    if (0 === n.indexOf("win") && 0 === n.indexOf("linux") && 0 <= n.indexOf("mac") && "other" !== e) return !0
                }
                return 0 <= r.indexOf("win") && "Windows" !== e && "Windows Phone" !== e || ((0 <= r.indexOf("linux") || 0 <= r.indexOf("android") || 0 <= r.indexOf("pike")) && "Linux" !== e && "Android" !== e || ((0 <= r.indexOf("mac") || 0 <= r.indexOf("ipad") || 0 <= r.indexOf("ipod") || 0 <= r.indexOf("iphone")) && "Mac" !== e && "iOS" !== e || (0 === r.indexOf("win") && 0 === r.indexOf("linux") && 0 <= r.indexOf("mac") && "other" !== e || void 0 === navigator.plugins && "Windows" !== e && "Windows Phone" !== e)))
            },
            getHasLiedBrowser: function () {
                var e, t = navigator.userAgent.toLowerCase(),
                    n = navigator.productSub;
                if (("Chrome" === (e = 0 <= t.indexOf("firefox") ? "Firefox" : 0 <= t.indexOf("opera") || 0 <= t.indexOf("opr") ? "Opera" : 0 <= t.indexOf("chrome") ? "Chrome" : 0 <= t.indexOf("safari") ? "Safari" : 0 <= t.indexOf("trident") ? "Internet Explorer" : "Other") || "Safari" === e || "Opera" === e) && "20030107" !== n) return !0;
                var r, o = eval.toString().length;
                if (37 === o && "Safari" !== e && "Firefox" !== e && "Other" !== e) return !0;
                if (39 === o && "Internet Explorer" !== e && "Other" !== e) return !0;
                if (33 === o && "Chrome" !== e && "Opera" !== e && "Other" !== e) return !0;
                try {
                    throw "a"
                } catch (e) {
                    try {
                        e.toSource(), r = !0
                    } catch (e) {
                        r = !1
                    }
                }
                return !(!r || "Firefox" === e || "Other" === e)
            },
            isCanvasSupported: function () {
                var e = document.createElement("canvas");
                return !(!e.getContext || !e.getContext("2d"))
            },
            isWebGlSupported: function () {
                if (!this.isCanvasSupported()) return !1;
                var t, e = document.createElement("canvas");
                try {
                    t = e.getContext && (e.getContext("webgl") || e.getContext("experimental-webgl"))
                } catch (e) {
                    t = !1
                }
                return !!window.WebGLRenderingContext && !!t
            },
            isIE: function () {
                return "Microsoft Internet Explorer" === navigator.appName || !("Netscape" !== navigator.appName || !/Trident/.test(navigator.userAgent))
            },
            hasSwfObjectLoaded: function () {
                return void 0 !== window.swfobject
            },
            hasMinFlashInstalled: function () {
                return swfobject.hasFlashPlayerVersion("9.0.0")
            },
            addFlashDivNode: function () {
                var e = document.createElement("div");
                e.setAttribute("id", this.options.swfContainerId), document.body.appendChild(e)
            },
            loadSwfAndDetectFonts: function (t) {
                var e = "___fp_swf_loaded";
                window[e] = function (e) {
                    t(e)
                };
                var n = this.options.swfContainerId;
                this.addFlashDivNode();
                var r = {
                    onReady: e
                };
                swfobject.embedSWF(this.options.swfPath, n, "1", "1", "9.0.0", !1, r, {
                    allowScriptAccess: "always",
                    menu: "false"
                }, {})
            },
            getWebglCanvas: function () {
                var e = document.createElement("canvas"),
                    t = null;
                try {
                    t = e.getContext("webgl") || e.getContext("experimental-webgl")
                } catch (e) { }
                return t = t || null
            },
            each: function (e, t, n) {
                if (null !== e)
                    if (this.nativeForEach && e.forEach === this.nativeForEach) e.forEach(t, n);
                    else if (e.length === +e.length) {
                        for (var r = 0, o = e.length; r < o; r++)
                            if (t.call(n, e[r], r, e) === {}) return
                    } else
                        for (var i in e)
                            if (e.hasOwnProperty(i) && t.call(n, e[i], i, e) === {}) return
            },
            map: function (e, r, o) {
                var i = [];
                return null == e ? i : this.nativeMap && e.map === this.nativeMap ? e.map(r, o) : (this.each(e, function (e, t, n) {
                    i[i.length] = r.call(o, e, t, n)
                }), i)
            },
            x64Add: function (e, t) {
                e = [e[0] >>> 16, 65535 & e[0], e[1] >>> 16, 65535 & e[1]], t = [t[0] >>> 16, 65535 & t[0], t[1] >>> 16, 65535 & t[1]];
                var n = [0, 0, 0, 0];
                return n[3] += e[3] + t[3], n[2] += n[3] >>> 16, n[3] &= 65535, n[2] += e[2] + t[2], n[1] += n[2] >>> 16, n[2] &= 65535, n[1] += e[1] + t[1], n[0] += n[1] >>> 16, n[1] &= 65535, n[0] += e[0] + t[0], n[0] &= 65535, [n[0] << 16 | n[1], n[2] << 16 | n[3]]
            },
            x64Multiply: function (e, t) {
                e = [e[0] >>> 16, 65535 & e[0], e[1] >>> 16, 65535 & e[1]], t = [t[0] >>> 16, 65535 & t[0], t[1] >>> 16, 65535 & t[1]];
                var n = [0, 0, 0, 0];
                return n[3] += e[3] * t[3], n[2] += n[3] >>> 16, n[3] &= 65535, n[2] += e[2] * t[3], n[1] += n[2] >>> 16, n[2] &= 65535, n[2] += e[3] * t[2], n[1] += n[2] >>> 16, n[2] &= 65535, n[1] += e[1] * t[3], n[0] += n[1] >>> 16, n[1] &= 65535, n[1] += e[2] * t[2], n[0] += n[1] >>> 16, n[1] &= 65535, n[1] += e[3] * t[1], n[0] += n[1] >>> 16, n[1] &= 65535, n[0] += e[0] * t[3] + e[1] * t[2] + e[2] * t[1] + e[3] * t[0], n[0] &= 65535, [n[0] << 16 | n[1], n[2] << 16 | n[3]]
            },
            x64Rotl: function (e, t) {
                return 32 === (t %= 64) ? [e[1], e[0]] : t < 32 ? [e[0] << t | e[1] >>> 32 - t, e[1] << t | e[0] >>> 32 - t] : (t -= 32, [e[1] << t | e[0] >>> 32 - t, e[0] << t | e[1] >>> 32 - t])
            },
            x64LeftShift: function (e, t) {
                return 0 === (t %= 64) ? e : t < 32 ? [e[0] << t | e[1] >>> 32 - t, e[1] << t] : [e[1] << t - 32, 0]
            },
            x64Xor: function (e, t) {
                return [e[0] ^ t[0], e[1] ^ t[1]]
            },
            x64Fmix: function (e) {
                return e = this.x64Xor(e, [0, e[0] >>> 1]), e = this.x64Multiply(e, [4283543511, 3981806797]), e = this.x64Xor(e, [0, e[0] >>> 1]), e = this.x64Multiply(e, [3301882366, 444984403]), e = this.x64Xor(e, [0, e[0] >>> 1])
            },
            x64hash128: function (e, t) {
                t = t || 0;
                for (var n = (e = e || "").length % 16, r = e.length - n, o = [0, t], i = [0, t], a = [0, 0], s = [0, 0], l = [2277735313, 289559509], c = [1291169091, 658871167], d = 0; d < r; d += 16) a = [255 & e.charCodeAt(d + 4) | (255 & e.charCodeAt(d + 5)) << 8 | (255 & e.charCodeAt(d + 6)) << 16 | (255 & e.charCodeAt(d + 7)) << 24, 255 & e.charCodeAt(d) | (255 & e.charCodeAt(d + 1)) << 8 | (255 & e.charCodeAt(d + 2)) << 16 | (255 & e.charCodeAt(d + 3)) << 24], s = [255 & e.charCodeAt(d + 12) | (255 & e.charCodeAt(d + 13)) << 8 | (255 & e.charCodeAt(d + 14)) << 16 | (255 & e.charCodeAt(d + 15)) << 24, 255 & e.charCodeAt(d + 8) | (255 & e.charCodeAt(d + 9)) << 8 | (255 & e.charCodeAt(d + 10)) << 16 | (255 & e.charCodeAt(d + 11)) << 24], a = this.x64Multiply(a, l), a = this.x64Rotl(a, 31), a = this.x64Multiply(a, c), o = this.x64Xor(o, a), o = this.x64Rotl(o, 27), o = this.x64Add(o, i), o = this.x64Add(this.x64Multiply(o, [0, 5]), [0, 1390208809]), s = this.x64Multiply(s, c), s = this.x64Rotl(s, 33), s = this.x64Multiply(s, l), i = this.x64Xor(i, s), i = this.x64Rotl(i, 31), i = this.x64Add(i, o), i = this.x64Add(this.x64Multiply(i, [0, 5]), [0, 944331445]);
                switch (a = [0, 0], s = [0, 0], n) {
                    case 15:
                        s = this.x64Xor(s, this.x64LeftShift([0, e.charCodeAt(d + 14)], 48));
                    case 14:
                        s = this.x64Xor(s, this.x64LeftShift([0, e.charCodeAt(d + 13)], 40));
                    case 13:
                        s = this.x64Xor(s, this.x64LeftShift([0, e.charCodeAt(d + 12)], 32));
                    case 12:
                        s = this.x64Xor(s, this.x64LeftShift([0, e.charCodeAt(d + 11)], 24));
                    case 11:
                        s = this.x64Xor(s, this.x64LeftShift([0, e.charCodeAt(d + 10)], 16));
                    case 10:
                        s = this.x64Xor(s, this.x64LeftShift([0, e.charCodeAt(d + 9)], 8));
                    case 9:
                        s = this.x64Xor(s, [0, e.charCodeAt(d + 8)]), s = this.x64Multiply(s, c), s = this.x64Rotl(s, 33), s = this.x64Multiply(s, l), i = this.x64Xor(i, s);
                    case 8:
                        a = this.x64Xor(a, this.x64LeftShift([0, e.charCodeAt(d + 7)], 56));
                    case 7:
                        a = this.x64Xor(a, this.x64LeftShift([0, e.charCodeAt(d + 6)], 48));
                    case 6:
                        a = this.x64Xor(a, this.x64LeftShift([0, e.charCodeAt(d + 5)], 40));
                    case 5:
                        a = this.x64Xor(a, this.x64LeftShift([0, e.charCodeAt(d + 4)], 32));
                    case 4:
                        a = this.x64Xor(a, this.x64LeftShift([0, e.charCodeAt(d + 3)], 24));
                    case 3:
                        a = this.x64Xor(a, this.x64LeftShift([0, e.charCodeAt(d + 2)], 16));
                    case 2:
                        a = this.x64Xor(a, this.x64LeftShift([0, e.charCodeAt(d + 1)], 8));
                    case 1:
                        a = this.x64Xor(a, [0, e.charCodeAt(d)]), a = this.x64Multiply(a, l), a = this.x64Rotl(a, 31), a = this.x64Multiply(a, c), o = this.x64Xor(o, a)
                }
                return o = this.x64Xor(o, [0, e.length]), i = this.x64Xor(i, [0, e.length]), o = this.x64Add(o, i), i = this.x64Add(i, o), o = this.x64Fmix(o), i = this.x64Fmix(i), o = this.x64Add(o, i), i = this.x64Add(i, o), ("00000000" + (o[0] >>> 0).toString(16)).slice(-8) + ("00000000" + (o[1] >>> 0).toString(16)).slice(-8) + ("00000000" + (i[0] >>> 0).toString(16)).slice(-8) + ("00000000" + (i[1] >>> 0).toString(16)).slice(-8)
            }
        }, e.VERSION = "1.1.1", e
    }),
    function () {
        (function () {
            return this
        })().true = {};
        var e = function (te) {
            "use strict";

            function ne(i) {
                var a = {};
                this.subscribe = function (e, t, n) {
                    if ("function" != typeof t) return !1;
                    a.hasOwnProperty(e) || (a[e] = {});
                    var r = Math.random().toString(35);
                    return a[e][r] = [t, !!n], r
                }, this.unsubscribe = function (e) {
                    for (var t in a)
                        if (a[t][e]) return delete a[t][e], !0;
                    return !1
                }, this.publish = function (e) {
                    if (a.hasOwnProperty(e)) {
                        var t = Array.prototype.slice.call(arguments, 1),
                            n = [];
                        for (var r in a[e]) {
                            var o = a[e][r];
                            try {
                                o[0].apply(i, t)
                            } catch (e) {
                                te.console
                            }
                            o[1] && n.push(r)
                        }
                        n.length && n.forEach(this.unsubscribe)
                    }
                }
            }

            function re(e, t, n, r) {
                var g = {};
                "object" == typeof e && (e = (g = e).orientation, t = g.unit || t, n = g.format || n, r = g.compress || g.compressPdf || r), t = t || "mm", n = n || "a4", e = ("" + (e || "P")).toLowerCase();

                function m(e) {
                    return e.toFixed(2)
                }

                function v(e) {
                    return e.toFixed(3)
                }

                function s(e) {
                    return ("0" + parseInt(e)).slice(-2)
                }

                function b(e) {
                    _ ? U[h].push(e) : (K += e.length + 1, V.push(e))
                }

                function d() {
                    return q[++F] = K, b(F + " 0 obj"), F
                }

                function u(e) {
                    b("stream"), b(e), b("endstream")
                }

                function o() {
                    for (var e in b("/ProcSet [/PDF /Text /ImageB /ImageC /ImageI]"), b("/Font <<"), H) H.hasOwnProperty(e) && b("/" + e + " " + H[e].objectNumber + " 0 R");
                    b(">>"), b("/XObject <<"), Y.publish("putXobjectDict"), b(">>")
                }

                function i() {
                    (function () {
                        for (var e in H) H.hasOwnProperty(e) && ((t = H[e]).objectNumber = d(), b("<</BaseFont/" + t.PostScriptName + "/Type/Font"), "string" == typeof t.encoding && b("/Encoding/" + t.encoding), b("/Subtype/Type1>>"), b("endobj"));
                        var t
                    })(), Y.publish("putResources"), q[2] = K, b("2 0 obj"), b("<<"), o(), b(">>"), b("endobj"), Y.publish("postPutResources")
                }

                function y(e, t, n) {
                    z.hasOwnProperty(t) || (z[t] = {}), z[t][n] = e
                }

                function w(e, t) {
                    return function (e, t) {
                        var n, r, o, i, a, s, l, c, d;
                        if (o = (t = t || {}).sourceEncoding || "Unicode", a = t.outputEncoding, (t.autoencode || a) && H[x].metadata && H[x].metadata[o] && H[x].metadata[o].encoding && (i = H[x].metadata[o].encoding, !a && H[x].encoding && (a = H[x].encoding), !a && i.codePages && (a = i.codePages[0]), "string" == typeof a && (a = i[a]), a)) {
                            for (l = !1, s = [], n = 0, r = e.length; n < r; n++) c = a[e.charCodeAt(n)], s.push(c ? String.fromCharCode(c) : e[n]), s[n].charCodeAt(0) >> 8 && (l = !0);
                            e = s.join("")
                        }
                        for (n = e.length; void 0 === l && 0 !== n;) e.charCodeAt(n - 1) >> 8 && (l = !0), n--;
                        if (!l) return e;
                        for (s = t.noBOM ? [] : [254, 255], n = 0, r = e.length; n < r; n++) {
                            if ((d = (c = e.charCodeAt(n)) >> 8) >> 8) throw new Error("Character at position " + n + " of string '" + e + "' exceeds 16bits. Cannot be encoded into UCS-2 BE");
                            s.push(d), s.push(c - (d << 8))
                        }
                        return String.fromCharCode.apply(void 0, s)
                    }(e, t).replace(/\\/g, "\\\\").replace(/\(/g, "\\(").replace(/\)/g, "\\)")
                }

                function a() {
                    (function (e, t) {
                        var n = "string" == typeof t && t.toLowerCase();
                        if ("string" == typeof e) {
                            var r = e.toLowerCase();
                            ie.hasOwnProperty(r) && (e = ie[r][0] / C, t = ie[r][1] / C)
                        }
                        if (Array.isArray(e) && (t = e[1], e = e[0]), n) {
                            switch (n.substr(0, 1)) {
                                case "l":
                                    e < t && (n = "s");
                                    break;
                                case "p":
                                    t < e && (n = "s")
                            }
                            "s" === n && (I = e, e = t, t = I)
                        }
                        _ = !0, U[++$] = [], j[$] = {
                            width: Number(e) || E,
                            height: Number(t) || T
                        }, Z($)
                    }).apply(this, arguments), b(m(D * C) + " w"), b(M), 0 !== W && b(W + " J"), 0 !== G && b(G + " j"), Y.publish("addPage", {
                        pageNumber: $
                    })
                }

                function l(e, t) {
                    var n;
                    e = void 0 !== e ? e : H[x].fontName, t = void 0 !== t ? t : H[x].fontStyle;
                    try {
                        n = z[e][t]
                    } catch (e) { }
                    if (!n) throw new Error("Unable to look up font label for font '" + e + "', '" + t + "'. Refer to getFontList() for available fonts.");
                    return n
                }

                function c() {
                    _ = !1, F = 2, V = [], q = [], b("%PDF-" + oe),
                        function () {
                            var e, t, n, r, o, i, a, s, l;
                            for (a = te.adler32cs || re.adler32cs, L && void 0 === a && (L = !1), e = 1; e <= $; e++) {
                                if (d(), s = (E = j[e].width) * C, l = (T = j[e].height) * C, b("<</Type /Page"), b("/Parent 1 0 R"), b("/Resources 2 0 R"), b("/MediaBox [0 0 " + m(s) + " " + m(l) + "]"), b("/Contents " + (F + 1) + " 0 R>>"), b("endobj"), t = U[e].join("\n"), d(), L) {
                                    for (n = [], r = t.length; r--;) n[r] = t.charCodeAt(r);
                                    i = a.from(t), (o = new se(6)).append(new Uint8Array(n)), t = o.flush(), (n = new Uint8Array(t.length + 6)).set(new Uint8Array([120, 156])), n.set(t, 2), n.set(new Uint8Array([255 & i, i >> 8 & 255, i >> 16 & 255, i >> 24 & 255]), t.length + 2), t = String.fromCharCode.apply(null, n), b("<</Length " + t.length + " /Filter [/FlateDecode]>>")
                                } else b("<</Length " + t.length + ">>");
                                u(t), b("endobj")
                            }
                            q[1] = K, b("1 0 obj"), b("<</Type /Pages");
                            var c = "/Kids [";
                            for (r = 0; r < $; r++) c += 3 + 2 * r + " 0 R ";
                            b(c + "]"), b("/Count " + $), b(">>"), b("endobj")
                        }(), i(), d(), b("<<"),
                        function () {
                            for (var e in b("/Producer (jsPDF " + re.version + ")"), X) X.hasOwnProperty(e) && X[e] && b("/" + e.substr(0, 1).toUpperCase() + e.substr(1) + " (" + w(X[e]) + ")");
                            var t = new Date,
                                n = t.getTimezoneOffset(),
                                r = n < 0 ? "+" : "-",
                                o = Math.floor(Math.abs(n / 60)),
                                i = Math.abs(n % 60),
                                a = [r, s(o), "'", s(i), "'"].join("");
                            b(["/CreationDate (D:", t.getFullYear(), s(t.getMonth() + 1), s(t.getDate()), s(t.getHours()), s(t.getMinutes()), s(t.getSeconds()), a, ")"].join(""))
                        }(), b(">>"), b("endobj"), d(), b("<<"),
                        function () {
                            switch (b("/Type /Catalog"), b("/Pages 1 0 R"), A = A || "fullwidth") {
                                case "fullwidth":
                                    b("/OpenAction [3 0 R /FitH null]");
                                    break;
                                case "fullheight":
                                    b("/OpenAction [3 0 R /FitV null]");
                                    break;
                                case "fullpage":
                                    b("/OpenAction [3 0 R /Fit]");
                                    break;
                                case "original":
                                    b("/OpenAction [3 0 R /XYZ null null 1]");
                                    break;
                                default:
                                    var e = "" + A;
                                    "%" === e.substr(e.length - 1) && (A = parseInt(A) / 100), "number" == typeof A && b("/OpenAction [3 0 R /XYZ null null " + m(A) + "]")
                            }
                            switch (O = O || "continuous") {
                                case "continuous":
                                    b("/PageLayout /OneColumn");
                                    break;
                                case "single":
                                    b("/PageLayout /SinglePage");
                                    break;
                                case "two":
                                case "twoleft":
                                    b("/PageLayout /TwoColumnLeft");
                                    break;
                                case "tworight":
                                    b("/PageLayout /TwoColumnRight")
                            }
                            S && b("/PageMode /" + S), Y.publish("putCatalog")
                        }(), b(">>"), b("endobj");
                    var e, t = K,
                        n = "0000000000";
                    for (b("xref"), b("0 " + (F + 1)), b(n + " 65535 f "), e = 1; e <= F; e++) b((n + q[e]).slice(-10) + " 00000 n ");
                    return b("trailer"), b("<<"), b("/Size " + (F + 1)), b("/Root " + F + " 0 R"), b("/Info " + (F - 1) + " 0 R"), b(">>"), b("startxref"), b(t), b("%%EOF"), _ = !0, V.join("\n")
                }

                function k(e) {
                    var t = "S";
                    return "F" === e ? t = "f" : "FD" === e || "DF" === e ? t = "B" : "f" !== e && "f*" !== e && "B" !== e && "B*" !== e || (t = e), t
                }

                function f() {
                    for (var e = c(), t = e.length, n = new ArrayBuffer(t), r = new Uint8Array(n); t--;) r[t] = e.charCodeAt(t);
                    return n
                }

                function p() {
                    return new Blob([f()], {
                        type: "application/pdf"
                    })
                }
                var x, C, I, h, E, T, S, A, O, B, L = (("" + n).toLowerCase(), !!r && "function" == typeof Uint8Array),
                    N = g.textColor || "0 g",
                    M = g.drawColor || "0 G",
                    P = g.fontSize || 16,
                    R = g.lineHeight || 1.15,
                    D = g.lineWidth || .200025,
                    F = 2,
                    _ = !1,
                    q = [],
                    H = {},
                    z = {},
                    $ = 0,
                    U = [],
                    j = {},
                    V = [],
                    W = 0,
                    G = 0,
                    K = 0,
                    X = {
                        title: "",
                        subject: "",
                        author: "",
                        keywords: "",
                        creator: ""
                    },
                    J = {},
                    Y = new ne(J),
                    Z = function (e) {
                        0 < e && e <= $ && (E = j[h = e].width, T = j[e].height)
                    },
                    Q = ((B = function (e, t) {
                        var n = "dataur" === ("" + e).substr(0, 6) ? "data:application/pdf;base64," + btoa(c()) : 0;
                        switch (e) {
                            case void 0:
                                return c();
                            case "save":
                                if (navigator.getUserMedia && (void 0 === te.URL || void 0 === te.URL.createObjectURL)) return J.output("dataurlnewwindow");
                                ae(p(), t), "function" == typeof ae.unload && te.setTimeout && setTimeout(ae.unload, 911);
                                break;
                            case "arraybuffer":
                                return f();
                            case "blob":
                                return p();
                            case "bloburi":
                            case "bloburl":
                                return te.URL && te.URL.createObjectURL(p()) || void 0;
                            case "datauristring":
                            case "dataurlstring":
                                return n;
                            case "dataurlnewwindow":
                                var r = te.open(n);
                                if (r || "undefined" == typeof safari) return r;
                            case "datauri":
                            case "dataurl":
                                return te.document.location.href = n;
                            default:
                                throw new Error('Output type "' + e + '" is not supported.')
                        }
                    }).foo = function () {
                        try {
                            return B.apply(this, arguments)
                        } catch (e) {
                            var t = e.stack || "";
                            ~t.indexOf(" at ") && (t = t.split(" at ")[1]);
                            var n = "Error in function " + t.split("\n")[0].split("<")[0] + ": " + e.message;
                            if (!te.console) throw new Error(n);
                            te.console.error(n, e), te.alert
                        }
                    }, (B.foo.bar = B).foo);
                switch (t) {
                    case "pt":
                        C = 1;
                        break;
                    case "mm":
                        C = 72 / 25.4;
                        break;
                    case "cm":
                        C = 72 / 2.54;
                        break;
                    case "in":
                        C = 72;
                        break;
                    case "px":
                        C = 96 / 72;
                        break;
                    case "pc":
                    case "em":
                        C = 12;
                        break;
                    case "ex":
                        C = 6;
                        break;
                    default:
                        throw "Invalid unit: " + t
                }
                for (var ee in J.internal = {
                    pdfEscape: w,
                    getStyle: k,
                    getFont: function () {
                        return H[l.apply(J, arguments)]
                    },
                    getFontSize: function () {
                        return P
                    },
                    getLineHeight: function () {
                        return P * R
                    },
                    write: function (e) {
                        b(1 === arguments.length ? e : Array.prototype.join.call(arguments, " "))
                    },
                    getCoordinateString: function (e) {
                        return m(e * C)
                    },
                    getVerticalCoordinateString: function (e) {
                        return m((T - e) * C)
                    },
                    collections: {},
                    newObject: d,
                    putStream: u,
                    events: Y,
                    scaleFactor: C,
                    pageSize: {
                        get width() {
                            return E
                        },
                        get height() {
                            return T
                        }
                    },
                    output: function (e, t) {
                        return Q(e, t)
                    },
                    getNumberOfPages: function () {
                        return U.length - 1
                    },
                    pages: U
                }, J.addPage = function () {
                    return a.apply(this, arguments), this
                }, J.setPage = function () {
                    return Z.apply(this, arguments), this
                }, J.setDisplayMode = function (e, t, n) {
                    return A = e, O = t, S = n, this
                }, J.text = function (e, t, n, r, o) {
                    function i(e) {
                        return e = e.split("\t").join(Array(g.TabLen || 9).join(" ")), w(e, r)
                    }
                    "number" == typeof e && (I = n, n = t, t = e, e = I), "string" == typeof e && e.match(/[\n\r]/) && (e = e.split(/\r\n|\r|\n/g)), "number" == typeof r && (o = r, r = null);
                    var a, s = "",
                        l = "Td";
                    if (o) {
                        o *= Math.PI / 180;
                        var c = Math.cos(o),
                            d = Math.sin(o);
                        s = [m(c), m(d), m(-1 * d), m(c), ""].join(" "), l = "Tm"
                    }
                    if ("noBOM" in (r = r || {}) || (r.noBOM = !0), "autoencode" in r || (r.autoencode = !0), "string" == typeof e) e = i(e);
                    else {
                        if (!(e instanceof Array)) throw new Error('Type of text must be string or Array. "' + e + '" is not recognized.');
                        for (var u = e.concat(), f = [], p = u.length; p--;) f.push(i(u.shift()));
                        var h = Math.ceil((T - n) * C / (P * R));
                        0 <= h && h < f.length + 1 && (a = f.splice(h - 1)), e = f.join(") Tj\nT* (")
                    }
                    return b("BT\n/" + x + " " + P + " Tf\n" + P * R + " TL\n" + N + "\n" + s + m(t * C) + " " + m((T - n) * C) + " " + l + "\n(" + e + ") Tj\nET"), a && (this.addPage(), this.text(a, t, 1.7 * P / C)), this
                }, J.lstext = function (e, t, n, r) {
                    for (var o = 0, i = e.length; o < i; o++ , t += r) this.text(e[o], t, n)
                }, J.line = function (e, t, n, r) {
                    return this.lines([
                        [n - e, r - t]
                    ], e, t)
                }, J.clip = function () {
                    b("W"), b("S")
                }, J.lines = function (e, t, n, r, o, i) {
                    var a, s, l, c, d, u, f, p, h, g, m;
                    for ("number" == typeof e && (I = n, n = t, t = e, e = I), r = r || [1, 1], b(v(t * C) + " " + v((T - n) * C) + " m "), a = r[0], s = r[1], c = e.length, g = t, m = n, l = 0; l < c; l++) 2 === (d = e[l]).length ? (g = d[0] * a + g, m = d[1] * s + m, b(v(g * C) + " " + v((T - m) * C) + " l")) : (u = d[0] * a + g, f = d[1] * s + m, p = d[2] * a + g, h = d[3] * s + m, g = d[4] * a + g, m = d[5] * s + m, b(v(u * C) + " " + v((T - f) * C) + " " + v(p * C) + " " + v((T - h) * C) + " " + v(g * C) + " " + v((T - m) * C) + " c"));
                    return i && b(" h"), null !== o && b(k(o)), this
                }, J.rect = function (e, t, n, r, o) {
                    return k(o), b([m(e * C), m((T - t) * C), m(n * C), m(-r * C), "re"].join(" ")), null !== o && b(k(o)), this
                }, J.triangle = function (e, t, n, r, o, i, a) {
                    return this.lines([
                        [n - e, r - t],
                        [o - n, i - r],
                        [e - o, t - i]
                    ], e, t, [1, 1], a, !0), this
                }, J.roundedRect = function (e, t, n, r, o, i, a) {
                    var s = 4 / 3 * (Math.SQRT2 - 1);
                    return this.lines([
                        [n - 2 * o, 0],
                        [o * s, 0, o, i - i * s, o, i],
                        [0, r - 2 * i],
                        [0, i * s, -o * s, i, -o, i],
                        [2 * o - n, 0],
                        [-o * s, 0, -o, -i * s, -o, -i],
                        [0, 2 * i - r],
                        [0, -i * s, o * s, -i, o, -i]
                    ], e + o, t, [1, 1], a), this
                }, J.ellipse = function (e, t, n, r, o) {
                    var i = 4 / 3 * (Math.SQRT2 - 1) * n,
                        a = 4 / 3 * (Math.SQRT2 - 1) * r;
                    return b([m((e + n) * C), m((T - t) * C), "m", m((e + n) * C), m((T - (t - a)) * C), m((e + i) * C), m((T - (t - r)) * C), m(e * C), m((T - (t - r)) * C), "c"].join(" ")), b([m((e - i) * C), m((T - (t - r)) * C), m((e - n) * C), m((T - (t - a)) * C), m((e - n) * C), m((T - t) * C), "c"].join(" ")), b([m((e - n) * C), m((T - (t + a)) * C), m((e - i) * C), m((T - (t + r)) * C), m(e * C), m((T - (t + r)) * C), "c"].join(" ")), b([m((e + i) * C), m((T - (t + r)) * C), m((e + n) * C), m((T - (t + a)) * C), m((e + n) * C), m((T - t) * C), "c"].join(" ")), null !== o && b(k(o)), this
                }, J.circle = function (e, t, n, r) {
                    return this.ellipse(e, t, n, n, r)
                }, J.setProperties = function (e) {
                    for (var t in X) X.hasOwnProperty(t) && e[t] && (X[t] = e[t]);
                    return this
                }, J.setFontSize = function (e) {
                    return P = e, this
                }, J.setFont = function (e, t) {
                    return x = l(e, t), this
                }, J.setFontStyle = J.setFontType = function (e) {
                    return x = l(void 0, e), this
                }, J.getFontList = function () {
                    var e, t, n, r = {};
                    for (e in z)
                        if (z.hasOwnProperty(e))
                            for (t in r[e] = n = [], z[e]) z[e].hasOwnProperty(t) && n.push(t);
                    return r
                }, J.setLineWidth = function (e) {
                    return b((e * C).toFixed(2) + " w"), this
                }, J.setDrawColor = function (e, t, n, r) {
                    var o;
                    return o = void 0 === t || void 0 === r && e === t === n ? "string" == typeof e ? e + " G" : m(e / 255) + " G" : void 0 === r ? "string" == typeof e ? [e, t, n, "RG"].join(" ") : [m(e / 255), m(t / 255), m(n / 255), "RG"].join(" ") : "string" == typeof e ? [e, t, n, r, "K"].join(" ") : [m(e), m(t), m(n), m(r), "K"].join(" "), b(o), this
                }, J.setFillColor = function (e, t, n, r) {
                    var o;
                    return o = void 0 === t || void 0 === r && e === t === n ? "string" == typeof e ? e + " g" : m(e / 255) + " g" : void 0 === r ? "string" == typeof e ? [e, t, n, "rg"].join(" ") : [m(e / 255), m(t / 255), m(n / 255), "rg"].join(" ") : "string" == typeof e ? [e, t, n, r, "k"].join(" ") : [m(e), m(t), m(n), m(r), "k"].join(" "), b(o), this
                }, J.setTextColor = function (e, t, n) {
                    if ("string" == typeof e && /^#[0-9A-Fa-f]{6}$/.test(e)) {
                        var r = parseInt(e.substr(1), 16);
                        e = r >> 16 & 255, t = r >> 8 & 255, n = 255 & r
                    }
                    return N = 0 === e && 0 === t && 0 === n || void 0 === t ? v(e / 255) + " g" : [v(e / 255), v(t / 255), v(n / 255), "rg"].join(" "), this
                }, J.CapJoinStyles = {
                    0: 0,
                    butt: 0,
                    but: 0,
                    miter: 0,
                    1: 1,
                    round: 1,
                    rounded: 1,
                    circle: 1,
                    2: 2,
                    projecting: 2,
                    project: 2,
                    square: 2,
                    bevel: 2
                }, J.setLineCap = function (e) {
                    var t = this.CapJoinStyles[e];
                    if (void 0 === t) throw new Error("Line cap style of '" + e + "' is not recognized. See or extend .CapJoinStyles property for valid styles");
                    return b((W = t) + " J"), this
                }, J.setLineJoin = function (e) {
                    var t = this.CapJoinStyles[e];
                    if (void 0 === t) throw new Error("Line join style of '" + e + "' is not recognized. See or extend .CapJoinStyles property for valid styles");
                    return b((G = t) + " j"), this
                }, J.output = Q, J.save = function (e) {
                    J.output("save", e)
                }, re.API) re.API.hasOwnProperty(ee) && ("events" === ee && re.API.events.length ? function (e, t) {
                    var n, r, o;
                    for (o = t.length - 1; - 1 !== o; o--) n = t[o][0], r = t[o][1], e.subscribe.apply(e, [n].concat("function" == typeof r ? [r] : r))
                }(Y, re.API.events) : J[ee] = re.API[ee]);
                return function () {
                    for (var e = "helvetica", t = "times", n = "courier", r = "normal", o = "bold", i = "italic", a = "bolditalic", s = [
                        ["Helvetica", e, r],
                        ["Helvetica-Bold", e, o],
                        ["Helvetica-Oblique", e, i],
                        ["Helvetica-BoldOblique", e, a],
                        ["Courier", n, r],
                        ["Courier-Bold", n, o],
                        ["Courier-Oblique", n, i],
                        ["Courier-BoldOblique", n, a],
                        ["Times-Roman", t, r],
                        ["Times-Bold", t, o],
                        ["Times-Italic", t, i],
                        ["Times-BoldItalic", t, a]
                    ], l = 0, c = s.length; l < c; l++) {
                        var d = (f = s[l][0], p = s[l][1], h = s[l][2], g = "StandardEncoding", void 0, m = "F" + (Object.keys(H).length + 1).toString(10), v = H[m] = {
                            id: m,
                            PostScriptName: f,
                            fontName: p,
                            fontStyle: h,
                            encoding: g,
                            metadata: {}
                        }, y(m, p, h), Y.publish("addFont", v), m),
                            u = s[l][0].split("-");
                        y(d, u[0], u[1] || "")
                    }
                    var f, p, h, g, m, v;
                    Y.publish("addFonts", {
                        fonts: H,
                        dictionary: z
                    })
                }(), x = "F1", a(n, e), Y.publish("initialized"), J
            }
            var oe = "1.3",
                ie = {
                    a0: [2383.94, 3370.39],
                    a1: [1683.78, 2383.94],
                    a2: [1190.55, 1683.78],
                    a3: [841.89, 1190.55],
                    a4: [595.28, 841.89],
                    a5: [419.53, 595.28],
                    a6: [297.64, 419.53],
                    a7: [209.76, 297.64],
                    a8: [147.4, 209.76],
                    a9: [104.88, 147.4],
                    a10: [73.7, 104.88],
                    b0: [2834.65, 4008.19],
                    b1: [2004.09, 2834.65],
                    b2: [1417.32, 2004.09],
                    b3: [1000.63, 1417.32],
                    b4: [708.66, 1000.63],
                    b5: [498.9, 708.66],
                    b6: [354.33, 498.9],
                    b7: [249.45, 354.33],
                    b8: [175.75, 249.45],
                    b9: [124.72, 175.75],
                    b10: [87.87, 124.72],
                    c0: [2599.37, 3676.54],
                    c1: [1836.85, 2599.37],
                    c2: [1298.27, 1836.85],
                    c3: [918.43, 1298.27],
                    c4: [649.13, 918.43],
                    c5: [459.21, 649.13],
                    c6: [323.15, 459.21],
                    c7: [229.61, 323.15],
                    c8: [161.57, 229.61],
                    c9: [113.39, 161.57],
                    c10: [79.37, 113.39],
                    dl: [311.81, 623.62],
                    letter: [612, 792],
                    "government-letter": [576, 756],
                    legal: [612, 1008],
                    "junior-legal": [576, 360],
                    ledger: [1224, 792],
                    tabloid: [792, 1224],
                    "credit-card": [153, 243]
                };
            return re.API = {
                events: []
            }, re.version = "1.0.272-git 2014-09-29T15:09:diegocr", "function" == typeof define && define.amd ? define("jsPDF", function () {
                return re
            }) : te.jsPDF = re, re
        }("undefined" != typeof self && self || "undefined" != typeof window && window || this);
        ! function () {
            "use strict";
            e.API.addHTML = function (e, c, d, u, f) {
                if ("undefined" == typeof html2canvas && "undefined" == typeof rasterizeHTML) throw new Error("You need either https://github.com/niklasvh/html2canvas or https://github.com/cburgmer/rasterizeHTML.js");
                "number" != typeof c && (u = c, f = d), "function" == typeof u && (f = u, u = null);
                var t = this.internal,
                    p = t.scaleFactor,
                    h = t.pageSize.width,
                    g = t.pageSize.height;
                if ((u = u || {}).onrendered = function (r) {
                    c = parseInt(c) || 0, d = parseInt(d) || 0;
                    var e = u.dim || {},
                        t = e.h || 0,
                        o = e.w || Math.min(h, r.width / p) - c,
                        i = "JPEG";
                    if (u.format && (i = u.format), r.height > g && u.pagesplit) {
                        var n = function () {
                            for (var e = 0; ;) {
                                var t = document.createElement("canvas");
                                t.width = Math.min(h * p, r.width), t.height = Math.min(g * p, r.height - e), t.getContext("2d").drawImage(r, 0, e, r.width, t.height, 0, 0, t.width, t.height);
                                var n = [t, c, e ? 0 : d, t.width / p, t.height / p, i, null, "SLOW"];
                                if (this.addImage.apply(this, n), (e += t.height) >= r.height) break;
                                this.addPage()
                            }
                            f(o, e, null, n)
                        }.bind(this);
                        if ("CANVAS" === r.nodeName) {
                            var a = new Image;
                            a.onload = n, a.src = r.toDataURL("image/png"), r = a
                        } else n()
                    } else {
                        var s = Math.random().toString(35),
                            l = [r, c, d, o, t, i, s, "SLOW"];
                        this.addImage.apply(this, l), f(o, t, s, l)
                    }
                }.bind(this), "undefined" != typeof html2canvas && !u.rstz) return html2canvas(e, u);
                if ("undefined" == typeof rasterizeHTML) return null;
                var n = "drawDocument";
                return "string" == typeof e && (n = /^http/.test(e) ? "drawURL" : "drawHTML"), u.width = u.width || h * p, rasterizeHTML[n](e, void 0, u).then(function (e) {
                    u.onrendered(e.image)
                }, function (e) {
                    f(null, e)
                })
            }
        }(),
            function (g) {
                "use strict";

                function m() {
                    var e = this.internal.collections[w + "images"];
                    for (var t in e) c.call(this, e[t])
                }

                function v() {
                    var e, t = this.internal.collections[w + "images"],
                        n = this.internal.write;
                    for (var r in t) n("/I" + (e = t[r]).i, e.n, "0", "R")
                }

                function b(e) {
                    return "object" == typeof e && 1 === e.nodeType
                }

                function y(e, t) {
                    var n;
                    if (t)
                        for (var r in t)
                            if (e === t[r].alias) {
                                n = t[r];
                                break
                            }
                    return n
                }
                var w = "addImage_",
                    k = ["jpeg", "jpg", "png"],
                    c = function (e) {
                        var t = this.internal.newObject(),
                            n = this.internal.write,
                            r = this.internal.putStream;
                        if (e.n = t, n("<</Type /XObject"), n("/Subtype /Image"), n("/Width " + e.w), n("/Height " + e.h), e.cs === this.color_spaces.INDEXED ? n("/ColorSpace [/Indexed /DeviceRGB " + (e.pal.length / 3 - 1) + " " + ("smask" in e ? t + 2 : t + 1) + " 0 R]") : (n("/ColorSpace /" + e.cs), e.cs === this.color_spaces.DEVICE_CMYK && n("/Decode [1 0 1 0 1 0 1 0]")), n("/BitsPerComponent " + e.bpc), "f" in e && n("/Filter /" + e.f), "dp" in e && n("/DecodeParms <<" + e.dp + ">>"), "trns" in e && e.trns.constructor == Array) {
                            for (var o = "", i = 0, a = e.trns.length; i < a; i++) o += e.trns[i] + " " + e.trns[i] + " ";
                            n("/Mask [" + o + "]")
                        }
                        if ("smask" in e && n("/SMask " + (t + 1) + " 0 R"), n("/Length " + e.data.length + ">>"), r(e.data), n("endobj"), "smask" in e) {
                            var s = "/Predictor 15 /Colors 1 /BitsPerComponent " + e.bpc + " /Columns " + e.w,
                                l = {
                                    w: e.w,
                                    h: e.h,
                                    cs: "DeviceGray",
                                    bpc: e.bpc,
                                    dp: s,
                                    data: e.smask
                                };
                            "f" in e && (l.f = e.f), c.call(this, l)
                        }
                        e.cs === this.color_spaces.INDEXED && (this.internal.newObject(), n("<< /Length " + e.pal.length + ">>"), r(this.arrayBufferToBinaryString(new Uint8Array(e.pal))), n("endobj"))
                    };
                g.color_spaces = {
                    DEVICE_RGB: "DeviceRGB",
                    DEVICE_GRAY: "DeviceGray",
                    DEVICE_CMYK: "DeviceCMYK",
                    CAL_GREY: "CalGray",
                    CAL_RGB: "CalRGB",
                    LAB: "Lab",
                    ICC_BASED: "ICCBased",
                    INDEXED: "Indexed",
                    PATTERN: "Pattern",
                    SEPERATION: "Seperation",
                    DEVICE_N: "DeviceN"
                }, g.decode = {
                    DCT_DECODE: "DCTDecode",
                    FLATE_DECODE: "FlateDecode",
                    LZW_DECODE: "LZWDecode",
                    JPX_DECODE: "JPXDecode",
                    JBIG2_DECODE: "JBIG2Decode",
                    ASCII85_DECODE: "ASCII85Decode",
                    ASCII_HEX_DECODE: "ASCIIHexDecode",
                    RUN_LENGTH_DECODE: "RunLengthDecode",
                    CCITT_FAX_DECODE: "CCITTFaxDecode"
                }, g.image_compression = {
                    NONE: "NONE",
                    FAST: "FAST",
                    MEDIUM: "MEDIUM",
                    SLOW: "SLOW"
                }, g.sHashCode = function (e) {
                    return Array.prototype.reduce && e.split("").reduce(function (e, t) {
                        return (e = (e << 5) - e + t.charCodeAt(0)) & e
                    }, 0)
                }, g.isString = function (e) {
                    return "string" == typeof e
                }, g.extractInfoFromBase64DataURI = function (e) {
                    return /^data:([\w]+?\/([\w]+?));base64,(.+?)$/g.exec(e)
                }, g.supportsArrayBuffer = function () {
                    return "undefined" != typeof ArrayBuffer && "undefined" != typeof Uint8Array
                }, g.isArrayBuffer = function (e) {
                    return !!this.supportsArrayBuffer() && e instanceof ArrayBuffer
                }, g.isArrayBufferView = function (e) {
                    return !!this.supportsArrayBuffer() && ("undefined" != typeof Uint32Array && (e instanceof Int8Array || e instanceof Uint8Array || "undefined" != typeof Uint8ClampedArray && e instanceof Uint8ClampedArray || e instanceof Int16Array || e instanceof Uint16Array || e instanceof Int32Array || e instanceof Uint32Array || e instanceof Float32Array || e instanceof Float64Array))
                }, g.binaryStringToUint8Array = function (e) {
                    for (var t = e.length, n = new Uint8Array(t), r = 0; r < t; r++) n[r] = e.charCodeAt(r);
                    return n
                }, g.arrayBufferToBinaryString = function (e) {
                    this.isArrayBuffer(e) && (e = new Uint8Array(e));
                    for (var t = "", n = e.byteLength, r = 0; r < n; r++) t += String.fromCharCode(e[r]);
                    return t
                }, g.arrayBufferToBase64 = function (e) {
                    for (var t, n = "", r = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", o = new Uint8Array(e), i = o.byteLength, a = i % 3, s = i - a, l = 0; l < s; l += 3) n += r[(16515072 & (t = o[l] << 16 | o[l + 1] << 8 | o[l + 2])) >> 18] + r[(258048 & t) >> 12] + r[(4032 & t) >> 6] + r[63 & t];
                    return 1 == a ? n += r[(252 & (t = o[s])) >> 2] + r[(3 & t) << 4] + "==" : 2 == a && (n += r[(64512 & (t = o[s] << 8 | o[1 + s])) >> 10] + r[(1008 & t) >> 4] + r[(15 & t) << 2] + "="), n
                }, g.createImageInfo = function (e, t, n, r, o, i, a, s, l, c, d, u) {
                    var f = {
                        alias: s,
                        w: t,
                        h: n,
                        cs: r,
                        bpc: o,
                        i: a,
                        data: e
                    };
                    return i && (f.f = i), l && (f.dp = l), c && (f.trns = c), d && (f.pal = d), u && (f.smask = u), f
                }, g.addImage = function (e, t, n, r, o, i, a, s, l) {
                    if ("string" != typeof t) {
                        var c = i;
                        i = o, o = r, r = n, n = t, t = c
                    }
                    if ("object" == typeof e && !b(e) && "imageData" in e) {
                        var d = e;
                        e = d.imageData, t = d.format || t, n = d.x || n || 0, r = d.y || r || 0, o = d.w || o, i = d.h || i, a = d.alias || a, s = d.compression || s, l = d.rotation || d.angle || l
                    }
                    if (isNaN(n) || isNaN(r)) throw new Error("Invalid coordinates passed to jsPDF.addImage");
                    var u, f, p = function () {
                        var e = this.internal.collections[w + "images"];
                        return e || (this.internal.collections[w + "images"] = e = {}, this.internal.events.subscribe("putResources", m), this.internal.events.subscribe("putXobjectDict", v)), e
                    }.call(this);
                    if (!(u = y(e, p)) && (b(e) && (e = function (e, t, n) {
                        if ("IMG" === e.nodeName && e.hasAttribute("src")) {
                            var r = "" + e.getAttribute("src");
                            if (!n && 0 === r.indexOf("data:image/")) return r;
                            !t && /\.png(?:[?#].*)?$/i.test(r) && (t = "png")
                        }
                        if ("CANVAS" === e.nodeName) var o = e;
                        else {
                            (o = document.createElement("canvas")).width = e.clientWidth || e.width, o.height = e.clientHeight || e.height;
                            var i = o.getContext("2d");
                            if (!i) throw "addImage requires canvas to be supported by browser.";
                            if (n) {
                                var a, s, l, c, d, u, f, p, h = Math.PI / 180;
                                "object" == typeof n && (a = n.x, s = n.y, l = n.bg, n = n.angle), p = n * h, c = Math.abs(Math.cos(p)), d = Math.abs(Math.sin(p)), u = o.width, f = o.height, o.width = f * d + u * c, o.height = f * c + u * d, isNaN(a) && (a = o.width / 2), isNaN(s) && (s = o.height / 2), i.clearRect(0, 0, o.width, o.height), i.fillStyle = l || "white", i.fillRect(0, 0, o.width, o.height), i.save(), i.translate(a, s), i.rotate(p), i.drawImage(e, -u / 2, -f / 2), i.rotate(-p), i.translate(-a, -s), i.restore()
                            } else i.drawImage(e, 0, 0, o.width, o.height)
                        }
                        return o.toDataURL("png" == ("" + t).toLowerCase() ? "image/png" : "image/jpeg")
                    }(e, t, l)), function (e) {
                        return null == e
                    }(a) && (a = function (e) {
                        return "string" == typeof e && g.sHashCode(e)
                    }(e)), !(u = y(a, p)))) {
                        if (this.isString(e)) {
                            var h = this.extractInfoFromBase64DataURI(e);
                            h ? (t = h[2], e = atob(h[3])) : 137 === e.charCodeAt(0) && 80 === e.charCodeAt(1) && 78 === e.charCodeAt(2) && 71 === e.charCodeAt(3) && (t = "png")
                        }
                        if (function (e) {
                            return -1 === k.indexOf(e)
                        }(t = (t || "JPEG").toLowerCase())) throw new Error("addImage currently only supports formats " + k + ", not '" + t + "'");
                        if (function (e) {
                            return "function" != typeof g["process" + e.toUpperCase()]
                        }(t)) throw new Error("please ensure that the plugin for '" + t + "' support is added");
                        if (this.supportsArrayBuffer() && (f = e, e = this.binaryStringToUint8Array(e)), !(u = this["process" + t.toUpperCase()](e, function (e) {
                            var t = 0;
                            return e && (t = Object.keys ? Object.keys(e).length : function (e) {
                                var t = 0;
                                for (var n in e) e.hasOwnProperty(n) && t++;
                                return t
                            }(e)), t
                        }(p), a, function (e) {
                            return e && "string" == typeof e && (e = e.toUpperCase()), e in g.image_compression ? e : g.image_compression.NONE
                        }(s), f))) throw new Error("An unkwown error occurred whilst processing the image")
                    }
                    return function (e, t, n, r, o, i, a) {
                        var s = function (e, t, n) {
                            return e || t || (t = e = -96), e < 0 && (e = -1 * n.w * 72 / e / this.internal.scaleFactor), t < 0 && (t = -1 * n.h * 72 / t / this.internal.scaleFactor), 0 === e && (e = t * n.w / n.h), 0 === t && (t = e * n.h / n.w), [e, t]
                        }.call(this, n, r, o),
                            l = this.internal.getCoordinateString,
                            c = this.internal.getVerticalCoordinateString;
                        n = s[0], r = s[1], a[i] = o, this.internal.write("q", l(n), "0 0", l(r), l(e), c(t + r), "cm /I" + o.i, "Do Q")
                    }.call(this, n, r, o, i, u, u.i, p), this
                };
                var l = function (e, t) {
                    return e.subarray(t, t + 5)
                };
                g.processJPEG = function (e, t, n, r, o) {
                    var i, a = this.color_spaces.DEVICE_RGB,
                        s = this.decode.DCT_DECODE;
                    return this.isString(e) ? (i = function (e) {
                        var t;
                        if (255 === !e.charCodeAt(0) || 216 === !e.charCodeAt(1) || 255 === !e.charCodeAt(2) || 224 === !e.charCodeAt(3) || !e.charCodeAt(6) === "J".charCodeAt(0) || !e.charCodeAt(7) === "F".charCodeAt(0) || !e.charCodeAt(8) === "I".charCodeAt(0) || !e.charCodeAt(9) === "F".charCodeAt(0) || 0 === !e.charCodeAt(10)) throw new Error("getJpegSize requires a binary string jpeg file");
                        for (var n = 256 * e.charCodeAt(4) + e.charCodeAt(5), r = 4, o = e.length; r < o;) {
                            if (r += n, 255 !== e.charCodeAt(r)) throw new Error("getJpegSize could not find the size of the image");
                            if (192 === e.charCodeAt(r + 1) || 193 === e.charCodeAt(r + 1) || 194 === e.charCodeAt(r + 1) || 195 === e.charCodeAt(r + 1) || 196 === e.charCodeAt(r + 1) || 197 === e.charCodeAt(r + 1) || 198 === e.charCodeAt(r + 1) || 199 === e.charCodeAt(r + 1)) return t = 256 * e.charCodeAt(r + 5) + e.charCodeAt(r + 6), [256 * e.charCodeAt(r + 7) + e.charCodeAt(r + 8), t, e.charCodeAt(r + 9)];
                            r += 2, n = 256 * e.charCodeAt(r) + e.charCodeAt(r + 1)
                        }
                    }(e), this.createImageInfo(e, i[0], i[1], 1 == i[3] ? this.color_spaces.DEVICE_GRAY : a, 8, s, t, n)) : (this.isArrayBuffer(e) && (e = new Uint8Array(e)), this.isArrayBufferView(e) ? (i = function (e) {
                        if (65496 != (e[0] << 8 | e[1])) throw new Error("Supplied data is not a JPEG");
                        for (var t, n = e.length, r = (e[4] << 8) + e[5], o = 4; o < n;) {
                            if (r = ((t = l(e, o += r))[2] << 8) + t[3], (192 === t[1] || 194 === t[1]) && 255 === t[0] && 7 < r) return {
                                width: ((t = l(e, o + 5))[2] << 8) + t[3],
                                height: (t[0] << 8) + t[1],
                                numcomponents: t[4]
                            };
                            o += 2
                        }
                        throw new Error("getJpegSizeFromBytes could not find the size of the image")
                    }(e), e = o || this.arrayBufferToBinaryString(e), this.createImageInfo(e, i.width, i.height, 1 == i.numcomponents ? this.color_spaces.DEVICE_GRAY : a, 8, s, t, n)) : null)
                }, g.processJPG = function () {
                    return this.processJPEG.apply(this, arguments)
                }
            }(e.API),
            function () {
                "use strict";
                e.API.autoPrint = function () {
                    var e;
                    return this.internal.events.subscribe("postPutResources", function () {
                        e = this.internal.newObject(), this.internal.write("<< /S/Named /Type/Action /N/Print >>", "endobj")
                    }), this.internal.events.subscribe("putCatalog", function () {
                        this.internal.write("/OpenAction " + e + " 0 R")
                    }), this
                }
            }(),
            function (T) {
                "use strict";

                function f(e, t, n, r, o) {
                    S = {
                        x: e,
                        y: t,
                        w: n,
                        h: r,
                        ln: o
                    }
                }

                function p() {
                    return S
                }
                var o, i, a, l, S = {
                    x: void 0,
                    y: void 0,
                    w: void 0,
                    h: void 0,
                    ln: void 0
                },
                    A = 1,
                    O = {
                        left: 0,
                        top: 0,
                        bottom: 0
                    };
                T.setHeaderFunction = function (e) {
                    l = e
                }, T.getTextDimensions = function (e) {
                    o = this.internal.getFont().fontName, i = this.table_font_size || this.internal.getFontSize(), a = this.internal.getFont().fontStyle;
                    var t, n, r = 19.049976 / 25.4;
                    return (n = document.createElement("font")).id = "jsPDFCell", n.style.fontStyle = a, n.style.fontName = o, n.style.fontSize = i + "pt", n.textContent = e, document.body.appendChild(n), t = {
                        w: (n.offsetWidth + 1) * r,
                        h: (n.offsetHeight + 1) * r
                    }, document.body.removeChild(n), t
                }, T.cellAddPage = function () {
                    var e = this.margins || O;
                    this.addPage(), f(e.left, e.top, void 0, void 0), A += 1
                }, T.cellInitialize = function () {
                    S = {
                        x: void 0,
                        y: void 0,
                        w: void 0,
                        h: void 0,
                        ln: void 0
                    }, A = 1
                }, T.cell = function (e, t, n, r, o, i, a) {
                    var s = p();
                    if (void 0 !== s.ln)
                        if (s.ln === i) e = s.x + s.w, t = s.y;
                        else {
                            var l = this.margins || O;
                            s.y + s.h + r + 13 >= this.internal.pageSize.height - l.bottom && (this.cellAddPage(), this.printHeaders && this.tableHeaderRow && this.printHeaderRow(i, !0)), t = p().y + p().h
                        }
                    if (void 0 !== o[0])
                        if (this.printingHeaderRow ? this.rect(e, t, n, r, "FD") : this.rect(e, t, n, r), "right" === a) {
                            if (o instanceof Array)
                                for (var c = 0; c < o.length; c++) {
                                    var d = o[c],
                                        u = this.getStringUnitWidth(d) * this.internal.getFontSize();
                                    this.text(d, e + n - u - 3, t + this.internal.getLineHeight() * (c + 1))
                                }
                        } else this.text(o, e + 3, t + this.internal.getLineHeight());
                    return f(e, t, n, r, i), this
                }, T.arrayMax = function (e, t) {
                    var n, r, o, i = e[0];
                    for (n = 0, r = e.length; n < r; n += 1) o = e[n], t ? -1 === t(i, o) && (i = o) : i < o && (i = o);
                    return i
                }, T.table = function (e, t, n, r, o) {
                    if (!n) throw "No data for PDF table";
                    var i, a, s, l, c, d, u, f, p, h, g = [],
                        m = [],
                        v = {},
                        b = {},
                        y = [],
                        w = [],
                        k = !1,
                        x = !0,
                        C = 12,
                        I = O;
                    if (I.width = this.internal.pageSize.width, o && (!0 === o.autoSize && (k = !0), !1 === o.printHeaders && (x = !1), o.fontSize && (C = o.fontSize), o.margins && (I = o.margins)), this.lnMod = 0, S = {
                        x: void 0,
                        y: void 0,
                        w: void 0,
                        h: void 0,
                        ln: void 0
                    }, A = 1, this.printHeaders = x, this.margins = I, this.setFontSize(C), this.table_font_size = C, null == r) g = Object.keys(n[0]);
                    else if (r[0] && "string" != typeof r[0]) {
                        for (a = 0, s = r.length; a < s; a += 1) i = r[a], g.push(i.name), m.push(i.prompt), b[i.name] = i.width * (19.049976 / 25.4)
                    } else g = r;
                    if (k)
                        for (h = function (e) {
                            return e[i]
                        }, a = 0, s = g.length; a < s; a += 1) {
                            for (v[i = g[a]] = n.map(h), y.push(this.getTextDimensions(m[a] || i).w), u = 0, l = (d = v[i]).length; u < l; u += 1) c = d[u], y.push(this.getTextDimensions(c).w);
                            b[i] = T.arrayMax(y)
                        }
                    if (x) {
                        var E = this.calculateLineHeight(g, b, m.length ? m : g);
                        for (a = 0, s = g.length; a < s; a += 1) i = g[a], w.push([e, t, b[i], E, String(m.length ? m[a] : i)]);
                        this.setTableHeaderRow(w), this.printHeaderRow(1, !1)
                    }
                    for (a = 0, s = n.length; a < s; a += 1) {
                        for (f = n[a], E = this.calculateLineHeight(g, b, f), u = 0, p = g.length; u < p; u += 1) i = g[u], this.cell(e, t, b[i], E, f[i], a + 2, i.align)
                    }
                    return this.lastCellPos = S, this.table_x = e, this.table_y = t, this
                }, T.calculateLineHeight = function (e, t, n) {
                    for (var r, o = 0, i = 0; i < e.length; i++) {
                        n[r = e[i]] = this.splitTextToSize(String(n[r]), t[r] - 3);
                        var a = this.internal.getLineHeight() * n[r].length + 3;
                        o < a && (o = a)
                    }
                    return o
                }, T.setTableHeaderRow = function (e) {
                    this.tableHeaderRow = e
                }, T.printHeaderRow = function (e, t) {
                    if (!this.tableHeaderRow) throw "Property tableHeaderRow does not exist.";
                    var n, r, o, i;
                    if (this.printingHeaderRow = !0, void 0 !== l) {
                        var a = l(this, A);
                        f(a[0], a[1], a[2], a[3], -1)
                    }
                    this.setFontStyle("bold");
                    var s = [];
                    for (o = 0, i = this.tableHeaderRow.length; o < i; o += 1) this.setFillColor(200, 200, 200), n = this.tableHeaderRow[o], t && (n[1] = this.margins && this.margins.top || 0, s.push(n)), r = [].concat(n), this.cell.apply(this, r.concat(e));
                    0 < s.length && this.setTableHeaderRow(s), this.setFontStyle("normal"), this.printingHeaderRow = !1
                }
            }(e.API),
            function (e) {
                var w, T, o, a, s, l, c, S, y, f, d, u, n, A, O, p, h, g, B;

                function t() { }
                w = function (e) {
                    return t.prototype = e, new t
                }, y = function (e) {
                    var t, n, r, o, i, a, s;
                    for (n = 0, r = e.length, t = void 0, a = o = !1; !o && n !== r;)(t = e[n] = e[n].trimLeft()) && (o = !0), n++;
                    for (n = r - 1; r && !a && -1 !== n;)(t = e[n] = e[n].trimRight()) && (a = !0), n--;
                    for (i = /\s+$/g, s = !0, n = 0; n !== r;) t = e[n].replace(/\s+/g, " "), s && (t = t.trimLeft()), t && (s = i.test(t)), e[n] = t, n++;
                    return e
                }, d = function (e) {
                    var t, n, r;
                    for (t = void 0, n = (r = e.split(",")).shift(); !t && n;) t = o[n.trim().toLowerCase()], n = r.shift();
                    return t
                }, u = function (e) {
                    var t;
                    return -1 < (e = "auto" === e ? "0px" : e).indexOf("em") && !isNaN(Number(e.replace("em", ""))) && (e = 18.719 * Number(e.replace("em", "")) + "px"), -1 < e.indexOf("pt") && !isNaN(Number(e.replace("pt", ""))) && (e = 1.333 * Number(e.replace("pt", "")) + "px"), void 0, 16, (t = n[e]) ? t : void 0 !== (t = {
                        "xx-small": 9,
                        "x-small": 11,
                        small: 13,
                        medium: 16,
                        large: 19,
                        "x-large": 23,
                        "xx-large": 28,
                        auto: 0
                    }[{
                            css_line_height_string: e
                        }]) ? n[e] = t / 16 : (t = parseFloat(e)) ? n[e] = t / 16 : (t = e.match(/([\d\.]+)(px)/), n[e] = 3 === t.length ? parseFloat(t[1]) / 16 : 1)
                }, S = function (e) {
                    var t, n, r, o, i;
                    return i = e, o = document.defaultView && document.defaultView.getComputedStyle ? document.defaultView.getComputedStyle(i, null) : i.currentStyle ? i.currentStyle : i.style, n = void 0, (t = {})["font-family"] = d((r = function (e) {
                        return e = e.replace(/-\D/g, function (e) {
                            return e.charAt(1).toUpperCase()
                        }), o[e]
                    })("font-family")) || "times", t["font-style"] = a[r("font-style")] || "normal", t["text-align"] = TextAlignMap[r("text-align")] || "left", "bold" === (n = s[r("font-weight")] || "normal") && (t["font-style"] = "normal" === t["font-style"] ? n : n + t["font-style"]), t["font-size"] = u(r("font-size")) || 1, t["line-height"] = u(r("line-height")) || 1, t.display = "inline" === r("display") ? "inline" : "block", n = "block" === t.display, t["margin-top"] = n && u(r("margin-top")) || 0, t["margin-bottom"] = n && u(r("margin-bottom")) || 0, t["padding-top"] = n && u(r("padding-top")) || 0, t["padding-bottom"] = n && u(r("padding-bottom")) || 0, t["margin-left"] = n && u(r("margin-left")) || 0, t["margin-right"] = n && u(r("margin-right")) || 0, t["padding-left"] = n && u(r("padding-left")) || 0, t["padding-right"] = n && u(r("padding-right")) || 0, t.float = l[r("cssFloat")] || "none", t.clear = c[r("clear")] || "none", t
                }, A = function (e, t, n) {
                    var r, o, i, a;
                    if (i = !1, a = o = void 0, r = n["#" + e.id])
                        if ("function" == typeof r) i = r(e, t);
                        else
                            for (o = 0, a = r.length; !i && o !== a;) i = r[o](e, t), o++;
                    if (r = n[e.nodeName], !i && r)
                        if ("function" == typeof r) i = r(e, t);
                        else
                            for (o = 0, a = r.length; !i && o !== a;) i = r[o](e, t), o++;
                    return i
                }, B = function (e, t) {
                    var n, r, o, i, a, s, l, c, d;
                    for (n = [], r = [], o = 0, d = e.rows[0].cells.length, l = e.clientWidth; o < d;) c = e.rows[0].cells[o], r[o] = {
                        name: c.textContent.toLowerCase().replace(/\s+/g, ""),
                        prompt: c.textContent.replace(/\r?\n/g, ""),
                        width: c.clientWidth / l * t.pdf.internal.pageSize.width
                    }, o++;
                    for (o = 1; o < e.rows.length;) {
                        for (s = e.rows[o], a = {}, i = 0; i < s.cells.length;) a[r[i].name] = s.cells[i].textContent.replace(/\r?\n/g, ""), i++;
                        n.push(a), o++
                    }
                    return {
                        rows: n,
                        headers: r
                    }
                };
                var L = {
                    SCRIPT: 1,
                    STYLE: 1,
                    NOSCRIPT: 1,
                    OBJECT: 1,
                    EMBED: 1,
                    SELECT: 1
                },
                    N = 1;
                T = function (e, o, t) {
                    var n, r, i, a, s, l, c, d;
                    for (r = e.childNodes, n = void 0, (s = "block" === (i = S(e)).display) && (o.setBlockBoundary(), o.setBlockStyle(i)), 19.049976 / 25.4, a = 0, l = r.length; a < l;) {
                        if ("object" == typeof (n = r[a])) {
                            if (o.executeWatchFunctions(n), 1 === n.nodeType && "HEADER" === n.nodeName) {
                                var u = n,
                                    f = o.pdf.margins_doc.top;
                                o.pdf.internal.events.subscribe("addPage", function () {
                                    o.y = f, T(u, o, t), o.pdf.margins_doc.top = o.y + 10, o.y += 10
                                }, !1)
                            }
                            if (8 === n.nodeType && "#comment" === n.nodeName) ~n.textContent.indexOf("ADD_PAGE") && (o.pdf.addPage(), o.y = o.pdf.margins_doc.top);
                            else if (1 !== n.nodeType || L[n.nodeName])
                                if (3 === n.nodeType) {
                                    var p = n.nodeValue;
                                    if (n.nodeValue && "LI" === n.parentNode.nodeName)
                                        if ("OL" === n.parentNode.parentNode.nodeName) p = N++ + ". " + p;
                                        else {
                                            var h = 16 * i["font-size"],
                                                g = 2;
                                            20 < h && (g = 3), d = function (e, t) {
                                                this.pdf.circle(e, t, g, "FD")
                                            }
                                        }
                                    o.addText(p, i)
                                } else "string" == typeof n && o.addText(n, i);
                            else {
                                var m;
                                if ("IMG" === n.nodeName) {
                                    var v = n.getAttribute("src");
                                    m = O[o.pdf.sHashCode(v) || v]
                                }
                                if (m) {
                                    o.pdf.internal.pageSize.height - o.pdf.margins_doc.bottom < o.y + n.height && o.y > o.pdf.margins_doc.top && (o.pdf.addPage(), o.y = o.pdf.margins_doc.top, o.executeWatchFunctions(n));
                                    var b = S(n),
                                        y = o.x,
                                        w = 12 / o.pdf.internal.scaleFactor,
                                        k = (b["margin-left"] + b["padding-left"]) * w,
                                        x = (b["margin-right"] + b["padding-right"]) * w,
                                        C = (b["margin-top"] + b["padding-top"]) * w,
                                        I = (b["margin-bottom"] + b["padding-bottom"]) * w;
                                    y += void 0 !== b.float && "right" === b.float ? o.settings.width - n.width - x : k, o.pdf.addImage(m, y, o.y + C, n.width, n.height), m = void 0, "right" === b.float || "left" === b.float ? (o.watchFunctions.push(function (e, t, n, r) {
                                        return o.y >= t ? (o.x += e, o.settings.width += n, !0) : !!(r && 1 === r.nodeType && !L[r.nodeName] && o.x + r.width > o.pdf.margins_doc.left + o.pdf.margins_doc.width) && (o.x += e, o.y = t, o.settings.width += n, !0)
                                    }.bind(this, "left" === b.float ? -n.width - k - x : 0, o.y + n.height + C + I, n.width)), o.watchFunctions.push(function (e, t, n) {
                                        return !(o.y < e && t === o.pdf.internal.getNumberOfPages()) || 1 === n.nodeType && "both" === S(n).clear && (o.y = e, !0)
                                    }.bind(this, o.y + n.height, o.pdf.internal.getNumberOfPages())), o.settings.width -= n.width + k + x, "left" === b.float && (o.x += n.width + k + x)) : o.y += n.height + I
                                } else if ("TABLE" === n.nodeName) c = B(n, o), o.y += 10, o.pdf.table(o.x, o.y, c.rows, c.headers, {
                                    autoSize: !1,
                                    printHeaders: !0,
                                    margins: o.pdf.margins_doc
                                }), o.y = o.pdf.lastCellPos.y + o.pdf.lastCellPos.h + 20;
                                else if ("OL" === n.nodeName || "UL" === n.nodeName) N = 1, A(n, o, t) || T(n, o, t), o.y += 10;
                                else if ("LI" === n.nodeName) {
                                    var E = o.x;
                                    o.x += "UL" === n.parentNode.nodeName ? 22 : 10, o.y += 3, A(n, o, t) || T(n, o, t), o.x = E
                                } else "BR" === n.nodeName ? o.y += i["font-size"] * o.pdf.internal.scaleFactor : A(n, o, t) || T(n, o, t)
                            }
                        }
                        a++
                    }
                    return s ? o.setBlockBoundary(d) : void 0
                }, O = {}, p = function (e, i, t, n) {
                    function a() {
                        i.pdf.internal.events.publish("imagesLoaded"), n(s)
                    }

                    function r(t, n, r) {
                        if (t) {
                            var o = new Image;
                            s = ++c, o.crossOrigin = "", o.onerror = o.onload = function () {
                                if (o.complete && (0 === o.src.indexOf("data:image/") && (o.width = n || o.width || 0, o.height = r || o.height || 0), o.width + o.height)) {
                                    var e = i.pdf.sHashCode(t) || t;
                                    O[e] = O[e] || o
                                } --c || a()
                            }, o.src = t
                        }
                    }
                    for (var s, o = e.getElementsByTagName("img"), l = o.length, c = 0; l--;) r(o[l].getAttribute("src"), o[l].width, o[l].height);
                    return c || a()
                }, h = function (e, i, a) {
                    var s = e.getElementsByTagName("footer");
                    if (0 < s.length) {
                        s = s[0];
                        var t = i.pdf.internal.write,
                            n = i.y;
                        i.pdf.internal.write = function () { }, T(s, i, a);
                        var l = Math.ceil(i.y - n) + 5;
                        i.y = n, i.pdf.internal.write = t, i.pdf.margins_doc.bottom += l;

                        function r(e) {
                            var t = void 0 !== e ? e.pageNumber : 1,
                                n = i.y;
                            i.y = i.pdf.internal.pageSize.height - i.pdf.margins_doc.bottom, i.pdf.margins_doc.bottom -= l;
                            for (var r = s.getElementsByTagName("span"), o = 0; o < r.length; ++o) - 1 < (" " + r[o].className + " ").replace(/[\n\t]/g, " ").indexOf(" pageCounter ") && (r[o].innerHTML = t), -1 < (" " + r[o].className + " ").replace(/[\n\t]/g, " ").indexOf(" totalPages ") && (r[o].innerHTML = "###jsPDFVarTotalPages###");
                            T(s, i, a), i.pdf.margins_doc.bottom += l, i.y = n
                        }
                        for (var o = s.getElementsByTagName("span"), c = 0; c < o.length; ++c) - 1 < (" " + o[c].className + " ").replace(/[\n\t]/g, " ").indexOf(" totalPages ") && i.pdf.internal.events.subscribe("htmlRenderingFinished", i.pdf.putTotalPages.bind(i.pdf, "###jsPDFVarTotalPages###"), !0);
                        i.pdf.internal.events.subscribe("addPage", r, !1), r(), L.FOOTER = 1
                    }
                }, g = function (e, t, n, r, o, i) {
                    if (!t) return !1;
                    var a, s, l, c;
                    "string" == typeof t || t.parentNode || (t = "" + t.innerHTML), "string" == typeof t && (a = t.replace(/<\/?script[^>]*?>/gi, ""), c = "jsPDFhtmlText" + Date.now().toString() + (1e3 * Math.random()).toFixed(0), (l = document.createElement("div")).style.cssText = "position: absolute !important;clip: rect(1px 1px 1px 1px); /* IE6, IE7 */clip: rect(1px, 1px, 1px, 1px);padding:0 !important;border:0 !important;height: 1px !important;width: 1px !important; top:auto;left:-100px;overflow: hidden;", l.innerHTML = '<iframe style="height:1px;width:1px" name="' + c + '" />', document.body.appendChild(l), (s = window.frames[c]).document.body.innerHTML = a, t = s.document.body);
                    var d, u = new f(e, n, r, o);
                    return p.call(this, t, u, o.elementHandlers, function (e) {
                        h(t, u, o.elementHandlers), T(t, u, o.elementHandlers), u.pdf.internal.events.publish("htmlRenderingFinished"), d = u.dispose(), "function" == typeof i && i(d)
                    }), d || {
                        x: u.x,
                        y: u.y
                    }
                }, (f = function (e, t, n, r) {
                    return this.pdf = e, this.x = t, this.y = n, this.settings = r, this.watchFunctions = [], this.init(), this
                }).prototype.init = function () {
                    return this.paragraph = {
                        text: [],
                        style: []
                    }, this.pdf.internal.write("q")
                }, f.prototype.dispose = function () {
                    return this.pdf.internal.write("Q"), {
                        x: this.x,
                        y: this.y,
                        ready: !0
                    }
                }, f.prototype.executeWatchFunctions = function (e) {
                    var t = !1,
                        n = [];
                    if (0 < this.watchFunctions.length) {
                        for (var r = 0; r < this.watchFunctions.length; ++r) !0 === this.watchFunctions[r](e) ? t = !0 : n.push(this.watchFunctions[r]);
                        this.watchFunctions = n
                    }
                    return t
                }, f.prototype.splitFragmentsIntoLines = function (e, t) {
                    var n, r, o, i, a, s, l, c, d, u, f, p, h, g;
                    for (12, u = this.pdf.internal.scaleFactor, i = {}, s = l = c = g = a = o = d = r = void 0, p = [f = []], n = 0, h = this.settings.width; e.length;)
                        if (a = e.shift(), g = t.shift(), a)
                            if ((o = i[(r = g["font-family"]) + (d = g["font-style"])]) || (o = this.pdf.internal.getFont(r, d).metadata.Unicode, i[r + d] = o), c = {
                                widths: o.widths,
                                kerning: o.kerning,
                                fontSize: 12 * g["font-size"],
                                textIndent: n
                            }, h < n + (l = this.pdf.getStringUnitWidth(a, c) * c.fontSize / u)) {
                                for (s = this.pdf.splitTextToSize(a, h, c), f.push([s.shift(), g]); s.length;) f = [
                                    [s.shift(), g]
                                ], p.push(f);
                                n = this.pdf.getStringUnitWidth(f[0][0], c) * c.fontSize / u
                            } else f.push([a, g]), n += l;
                    if (void 0 !== g["text-align"] && ("center" === g["text-align"] || "right" === g["text-align"] || "justify" === g["text-align"]))
                        for (var m = 0; m < p.length; ++m) {
                            var v = this.pdf.getStringUnitWidth(p[m][0][0], c) * c.fontSize / u;
                            0 < m && (p[m][0][1] = w(p[m][0][1]));
                            var b = h - v;
                            if ("right" === g["text-align"]) p[m][0][1]["margin-left"] = b;
                            else if ("center" === g["text-align"]) p[m][0][1]["margin-left"] = b / 2;
                            else if ("justify" === g["text-align"]) {
                                var y = p[m][0][0].split(" ").length - 1;
                                p[m][0][1]["word-spacing"] = b / y, m === p.length - 1 && (p[m][0][1]["word-spacing"] = 0)
                            }
                        }
                    return p
                }, f.prototype.RenderTextFragment = function (e, t) {
                    var n, r;
                    r = 0, this.pdf.internal.pageSize.height - this.pdf.margins_doc.bottom < this.y + this.pdf.internal.getFontSize() && (this.pdf.internal.write("ET", "Q"), this.pdf.addPage(), this.y = this.pdf.margins_doc.top, this.pdf.internal.write("q", "BT 0 g", this.pdf.internal.getCoordinateString(this.x), this.pdf.internal.getVerticalCoordinateString(this.y), "Td"), r = Math.max(r, t["line-height"], t["font-size"]), this.pdf.internal.write(0, (-12 * r).toFixed(2), "Td")), n = this.pdf.internal.getFont(t["font-family"], t["font-style"]), void 0 !== t["word-spacing"] && 0 < t["word-spacing"] && this.pdf.internal.write(t["word-spacing"].toFixed(2), "Tw"), this.pdf.internal.write("/" + n.id, (12 * t["font-size"]).toFixed(2), "Tf", "(" + this.pdf.internal.pdfEscape(e) + ") Tj"), void 0 !== t["word-spacing"] && this.pdf.internal.write(0, "Tw")
                }, f.prototype.renderParagraph = function (e) {
                    var t, n, r, o, i, a, s, l, c, d, u, f, p, h;
                    if (r = y(this.paragraph.text), p = this.paragraph.style, t = this.paragraph.blockstyle, f = this.paragraph.blockstyle || {}, this.paragraph = {
                        text: [],
                        style: [],
                        blockstyle: {},
                        priorblockstyle: t
                    }, r.join("").trim()) {
                        s = this.splitFragmentsIntoLines(r, p), l = a = void 0, n = 12 / this.pdf.internal.scaleFactor, u = (Math.max((t["margin-top"] || 0) - (f["margin-bottom"] || 0), 0) + (t["padding-top"] || 0)) * n, d = ((t["margin-bottom"] || 0) + (t["padding-bottom"] || 0)) * n, c = this.pdf.internal.write, i = o = void 0, this.y += u, c("q", "BT 0 g", this.pdf.internal.getCoordinateString(this.x), this.pdf.internal.getVerticalCoordinateString(this.y), "Td");
                        for (var g = 0; s.length;) {
                            for (o = l = 0, i = (a = s.shift()).length; o !== i;) a[o][0].trim() && (l = Math.max(l, a[o][1]["line-height"], a[o][1]["font-size"]), h = 7 * a[o][1]["font-size"]), o++;
                            var m = 0;
                            for (void 0 !== a[0][1]["margin-left"] && 0 < a[0][1]["margin-left"] && (wantedIndent = this.pdf.internal.getCoordinateString(a[0][1]["margin-left"]), m = wantedIndent - g, g = wantedIndent), c(m, (-12 * l).toFixed(2), "Td"), o = 0, i = a.length; o !== i;) a[o][0] && this.RenderTextFragment(a[o][0], a[o][1]), o++;
                            if (this.y += l * n, this.executeWatchFunctions(a[0][1]) && 0 < s.length) {
                                var v = [],
                                    b = [];
                                s.forEach(function (e) {
                                    for (var t = 0, n = e.length; t !== n;) e[t][0] && (v.push(e[t][0] + " "), b.push(e[t][1])), ++t
                                }), s = this.splitFragmentsIntoLines(y(v), b), c("ET", "Q"), c("q", "BT 0 g", this.pdf.internal.getCoordinateString(this.x), this.pdf.internal.getVerticalCoordinateString(this.y), "Td")
                            }
                        }
                        return e && "function" == typeof e && e.call(this, this.x - 9, this.y - h / 2), c("ET", "Q"), this.y += d
                    }
                }, f.prototype.setBlockBoundary = function (e) {
                    return this.renderParagraph(e)
                }, f.prototype.setBlockStyle = function (e) {
                    return this.paragraph.blockstyle = e
                }, f.prototype.addText = function (e, t) {
                    return this.paragraph.text.push(e), this.paragraph.style.push(t)
                }, o = {
                    helvetica: "helvetica",
                    "sans-serif": "helvetica",
                    "times new roman": "times",
                    serif: "times",
                    times: "times",
                    monospace: "courier",
                    courier: "courier"
                }, s = {
                    100: "normal",
                    200: "normal",
                    300: "normal",
                    400: "normal",
                    500: "bold",
                    600: "bold",
                    700: "bold",
                    800: "bold",
                    900: "bold",
                    normal: "normal",
                    bold: "bold",
                    bolder: "bold",
                    lighter: "normal"
                }, a = {
                    normal: "normal",
                    italic: "italic",
                    oblique: "italic"
                }, TextAlignMap = {
                    left: "left",
                    right: "right",
                    center: "center",
                    justify: "justify"
                }, l = {
                    none: "none",
                    right: "right",
                    left: "left"
                }, c = {
                    none: "none",
                    both: "both"
                }, n = {
                    normal: 1
                }, e.fromHTML = function (e, t, n, r, o, i) {
                    "use strict";
                    return this.margins_doc = i || {
                        top: 0,
                        bottom: 0
                    }, (r = r || {}).elementHandlers || (r.elementHandlers = {}), g(this, e, isNaN(t) ? 4 : t, isNaN(n) ? 4 : n, r, o)
                }
            }(e.API),
            function () {
                "use strict";
                var t, n, r;
                e.API.addJS = function (e) {
                    return r = e, this.internal.events.subscribe("postPutResources", function () {
                        t = this.internal.newObject(), this.internal.write("<< /Names [(EmbeddedJS) " + (t + 1) + " 0 R] >>", "endobj"), n = this.internal.newObject(), this.internal.write("<< /S /JavaScript /JS (", r, ") >>", "endobj")
                    }), this.internal.events.subscribe("putCatalog", function () {
                        void 0 !== t && void 0 !== n && this.internal.write("/Names <</JavaScript " + t + " 0 R>>")
                    }), this
                }
            }(),
            function (T) {
                "use strict";

                function S(e, t, n, r) {
                    var o = 5,
                        i = v;
                    switch (r) {
                        case T.image_compression.FAST:
                            o = 3, i = m;
                            break;
                        case T.image_compression.MEDIUM:
                            o = 6, i = b;
                            break;
                        case T.image_compression.SLOW:
                            o = 9, i = y
                    }
                    e = g(e, t, n, i);
                    var a = new Uint8Array(p(o)),
                        s = h(e),
                        l = new se(o),
                        c = l.append(e),
                        d = l.flush(),
                        u = a.length + c.length + d.length,
                        f = new Uint8Array(u + 4);
                    return f.set(a), f.set(c, a.length), f.set(d, a.length + c.length), f[u++] = s >>> 24 & 255, f[u++] = s >>> 16 & 255, f[u++] = s >>> 8 & 255, f[u++] = 255 & s, T.arrayBufferToBinaryString(f)
                }

                function e(e) {
                    var t = Array.apply([], e);
                    return t.unshift(0), t
                }
                var A = function () {
                    var e = "function" == typeof se;
                    if (!e) throw new Error("requires deflate.js for compression");
                    return e
                },
                    p = function (e, t) {
                        var n = Math.LOG2E * Math.log(32768) - 8 << 4 | 8,
                            r = n << 8;
                        return r |= Math.min(3, (t - 1 & 255) >> 1) << 6, r |= 0, [n, 255 & (r += 31 - r % 31)]
                    },
                    h = function (e, t) {
                        for (var n, r = 1, o = 0, i = e.length, a = 0; 0 < i;) {
                            for (i -= n = t < i ? t : i; o += r += e[a++], --n;);
                            r %= 65521, o %= 65521
                        }
                        return (o << 16 | r) >>> 0
                    },
                    g = function (e, t, n, r) {
                        for (var o, i, a, s = e.length / t, l = new Uint8Array(e.length + s), c = w(), d = 0; d < s; d++) {
                            if (a = d * t, o = e.subarray(a, a + t), r) l.set(r(o, n, i), a + d);
                            else {
                                for (var u = 0, f = c.length, p = []; u < f; u++) p[u] = c[u](o, n, i);
                                var h = k(p.concat());
                                l.set(p[h], a + d)
                            }
                            i = o
                        }
                        return l
                    },
                    m = function (e, t) {
                        var n, r = [],
                            o = 0,
                            i = e.length;
                        for (r[0] = 1; o < i; o++) n = e[o - t] || 0, r[o + 1] = e[o] - n + 256 & 255;
                        return r
                    },
                    v = function (e, t, n) {
                        var r, o = [],
                            i = 0,
                            a = e.length;
                        for (o[0] = 2; i < a; i++) r = n && n[i] || 0, o[i + 1] = e[i] - r + 256 & 255;
                        return o
                    },
                    b = function (e, t, n) {
                        var r, o, i = [],
                            a = 0,
                            s = e.length;
                        for (i[0] = 3; a < s; a++) r = e[a - t] || 0, o = n && n[a] || 0, i[a + 1] = e[a] + 256 - (r + o >>> 1) & 255;
                        return i
                    },
                    y = function (e, t, n) {
                        var r, o, i, a, s = [],
                            l = 0,
                            c = e.length;
                        for (s[0] = 4; l < c; l++) r = e[l - t] || 0, o = n && n[l] || 0, i = n && n[l - t] || 0, a = d(r, o, i), s[l + 1] = e[l] - a + 256 & 255;
                        return s
                    },
                    d = function (e, t, n) {
                        var r = e + t - n,
                            o = Math.abs(r - e),
                            i = Math.abs(r - t),
                            a = Math.abs(r - n);
                        return o <= i && o <= a ? e : i <= a ? t : n
                    },
                    w = function () {
                        return [e, m, v, b, y]
                    },
                    k = function (e) {
                        for (var t, n, r, o = 0, i = e.length; o < i;)((t = a(e[o].slice(1))) < n || !n) && (n = t, r = o), o++;
                        return r
                    },
                    a = function (e) {
                        for (var t = 0, n = e.length, r = 0; t < n;) r += Math.abs(e[t++]);
                        return r
                    };
                T.processPNG = function (e, t, n, r) {
                    var o, i, a, s, l, c, d = this.color_spaces.DEVICE_RGB,
                        u = this.decode.FLATE_DECODE,
                        f = 8;
                    if (this.isArrayBuffer(e) && (e = new Uint8Array(e)), this.isArrayBufferView(e)) {
                        if ("function" != typeof PNG || "function" != typeof O) throw new Error("PNG support requires png.js and zlib.js");
                        if (e = (o = new PNG(e)).imgData, f = o.bits, d = o.colorSpace, s = o.colors, -1 !== [4, 6].indexOf(o.colorType)) {
                            if (8 === o.bits)
                                for (var p, h = (E = new window["Uint" + o.pixelBitlength + "Array"](o.decodePixels().buffer)).length, g = new Uint8Array(h * o.colors), m = new Uint8Array(h), v = o.pixelBitlength - o.bits, b = 0, y = 0; b < h; b++) {
                                    for (w = E[b], p = 0; p < v;) g[y++] = w >>> p & 255, p += o.bits;
                                    m[b] = w >>> p & 255
                                }
                            if (16 === o.bits) {
                                h = (E = new Uint32Array(o.decodePixels().buffer)).length, g = new Uint8Array(h * (32 / o.pixelBitlength) * o.colors), m = new Uint8Array(h * (32 / o.pixelBitlength));
                                for (var w, k = 1 < o.colors, x = (b = 0, y = 0, 0); b < h;) w = E[b++], g[y++] = w >>> 0 & 255, k && (g[y++] = w >>> 16 & 255, w = E[b++], g[y++] = w >>> 0 & 255), m[x++] = w >>> 16 & 255;
                                f = 8
                            } ! function (e) {
                                return e !== T.image_compression.NONE && A()
                            }(r) ? (e = g, c = m, u = null) : (e = S(g, o.width * o.colors, o.colors, r), c = S(m, o.width, 1, r))
                        }
                        if (3 === o.colorType && (d = this.color_spaces.INDEXED, l = o.palette, o.transparency.indexed)) {
                            var C = o.transparency.indexed,
                                I = 0;
                            for (b = 0, h = C.length; b < h; ++b) I += C[b];
                            if ((I /= 255) === h - 1 && -1 !== C.indexOf(0)) a = [C.indexOf(0)];
                            else if (I !== h) {
                                var E = o.decodePixels();
                                for (m = new Uint8Array(E.length), b = 0, h = E.length; b < h; b++) m[b] = C[E[b]];
                                c = S(m, o.width, 1)
                            }
                        }
                        return i = u === this.decode.FLATE_DECODE ? "/Predictor 15 /Colors " + s + " /BitsPerComponent " + f + " /Columns " + o.width : "/Colors " + s + " /BitsPerComponent " + f + " /Columns " + o.width, (this.isArrayBuffer(e) || this.isArrayBufferView(e)) && (e = this.arrayBufferToBinaryString(e)), (c && this.isArrayBuffer(c) || this.isArrayBufferView(c)) && (c = this.arrayBufferToBinaryString(c)), this.createImageInfo(e, o.width, o.height, d, f, u, t, n, i, a, l, c)
                    }
                    throw new Error("Unsupported PNG image data, try using JPEG instead.")
                }
            }(e.API),
            function () {
                "use strict";
                e.API.addSVG = function (e, t, n, r, o) {
                    function i(e) {
                        for (var t = parseFloat(e[1]), n = parseFloat(e[2]), r = [], o = 3, i = e.length; o < i;) "c" === e[o] ? (r.push([parseFloat(e[o + 1]), parseFloat(e[o + 2]), parseFloat(e[o + 3]), parseFloat(e[o + 4]), parseFloat(e[o + 5]), parseFloat(e[o + 6])]), o += 7) : "l" === e[o] ? (r.push([parseFloat(e[o + 1]), parseFloat(e[o + 2])]), o += 3) : o += 1;
                        return [t, n, r]
                    }
                    if (void 0 === t || void 0 === n) throw new Error("addSVG needs values for 'x' and 'y'");
                    var a, s, l, c, d, u, f, p, h = (f = document, p = f.createElement("iframe"), c = ".jsPDF_sillysvg_iframe {display:none;position:absolute;}", (u = (d = f).createElement("style")).type = "text/css", u.styleSheet ? u.styleSheet.cssText = c : u.appendChild(d.createTextNode(c)), void d.getElementsByTagName("head")[0].appendChild(u), p.name = "childframe", p.setAttribute("width", 0), p.setAttribute("height", 0), p.setAttribute("frameborder", "0"), p.setAttribute("scrolling", "no"), p.setAttribute("seamless", "seamless"), p.setAttribute("class", "jsPDF_sillysvg_iframe"), f.body.appendChild(p), p),
                        g = (a = e, (l = ((s = h).contentWindow || s.contentDocument).document).write(a), l.close(), l.getElementsByTagName("svg")[0]),
                        m = [1, 1],
                        v = parseFloat(g.getAttribute("width")),
                        b = parseFloat(g.getAttribute("height"));
                    v && b && (r && o ? m = [r / v, o / b] : r ? m = [r / v, r / v] : o && (m = [o / b, o / b]));
                    var y, w, k, x, C = g.childNodes;
                    for (y = 0, w = C.length; y < w; y++)(k = C[y]).tagName && "PATH" === k.tagName.toUpperCase() && ((x = i(k.getAttribute("d").split(" ")))[0] = x[0] * m[0] + t, x[1] = x[1] * m[1] + n, this.lines.call(this, x[2], x[0], x[1], m));
                    return this
                }
            }(),
            function (e) {
                "use strict";

                function w(e) {
                    for (var t = e.length, n = 0; t;) n += e[--t];
                    return n
                }

                function k(e, t, n, r) {
                    for (var o = [], i = 0, a = e.length, s = 0; i !== a && s + t[i] < n;) s += t[i], i++;
                    o.push(e.slice(0, i));
                    var l = i;
                    for (s = 0; i !== a;) s + t[i] > r && (o.push(e.slice(l, i)), s = 0, l = i), s += t[i], i++;
                    return l !== i && o.push(e.slice(l, i)), o
                }

                function d(e, t, n) {
                    var r, o, i, a, s, l, c = [],
                        d = [c],
                        u = (n = n || {}).textIndent || 0,
                        f = 0,
                        p = 0,
                        h = e.split(" "),
                        g = x(" ", n)[0];
                    if (l = -1 === n.lineIndent ? h[0].length + 2 : n.lineIndent || 0) {
                        var m = Array(l).join(" "),
                            v = [];
                        h.map(function (e) {
                            1 < (e = e.split(/\s*\n/)).length ? v = v.concat(e.map(function (e, t) {
                                return (t && e.length ? "\n" : "") + e
                            })) : v.push(e[0])
                        }), h = v, l = C(m, n)
                    }
                    for (i = 0, a = h.length; i < a; i++) {
                        var b = 0;
                        if (r = h[i], l && "\n" == r[0] && (r = r.substr(1), b = 1), o = x(r, n), t < u + f + (p = w(o)) || b) {
                            if (t < p) {
                                for (s = k(r, o, t - (u + f), t), c.push(s.shift()), c = [s.pop()]; s.length;) d.push([s.shift()]);
                                p = w(o.slice(r.length - c[0].length))
                            } else c = [r];
                            d.push(c), u = p + l, f = g
                        } else c.push(r), u += f + p, f = g
                    }
                    if (l) var y = function (e, t) {
                        return (t ? m : "") + e.join(" ")
                    };
                    else y = function (e) {
                        return e.join(" ")
                    };
                    return d.map(y)
                }
                var x = e.getCharWidthsArray = function (e, t) {
                    var n, r, o, i = (t = t || {}).widths ? t.widths : this.internal.getFont().metadata.Unicode.widths,
                        a = i.fof ? i.fof : 1,
                        s = t.kerning ? t.kerning : this.internal.getFont().metadata.Unicode.kerning,
                        l = s.fof ? s.fof : 1,
                        c = 0,
                        d = i[0] || a,
                        u = [];
                    for (n = 0, r = e.length; n < r; n++) o = e.charCodeAt(n), u.push((i[o] || d) / a + (s[o] && s[o][c] || 0) / l), c = o;
                    return u
                },
                    C = e.getStringUnitWidth = function (e, t) {
                        return w(x.call(this, e, t))
                    };
                e.splitTextToSize = function (e, t, n) {
                    var r, o = (n = n || {}).fontSize || this.internal.getFontSize(),
                        i = function (e) {
                            var t = {
                                0: 1
                            },
                                n = {};
                            if (e.widths && e.kerning) return {
                                widths: e.widths,
                                kerning: e.kerning
                            };
                            var r = this.internal.getFont(e.fontName, e.fontStyle),
                                o = "Unicode";
                            return r.metadata[o] ? {
                                widths: r.metadata[o].widths || t,
                                kerning: r.metadata[o].kerning || n
                            } : {
                                    widths: t,
                                    kerning: n
                                }
                        }.call(this, n);
                    r = Array.isArray(e) ? e : e.split(/\r?\n/);
                    var a = 1 * this.internal.scaleFactor * t / o;
                    i.textIndent = n.textIndent ? 1 * n.textIndent * this.internal.scaleFactor / o : 0, i.lineIndent = n.lineIndent;
                    var s, l, c = [];
                    for (s = 0, l = r.length; s < l; s++) c = c.concat(d(r[s], a, i));
                    return c
                }
            }(e.API),
            function (e) {
                "use strict";

                function t(e) {
                    for (var t = "klmnopqrstuvwxyz", n = {}, r = 0; r < t.length; r++) n[t[r]] = "0123456789abcdef"[r];
                    var o, i, a, s, l, c = {},
                        d = 1,
                        u = c,
                        f = [],
                        p = "",
                        h = "",
                        g = e.length - 1;
                    for (r = 1; r != g;) l = e[r], r += 1, "'" == l ? i = i ? (s = i.join(""), o) : [] : i ? i.push(l) : "{" == l ? (f.push([u, s]), u = {}, s = o) : "}" == l ? ((a = f.pop())[0][a[1]] = u, s = o, u = a[0]) : "-" == l ? d = -1 : s === o ? n.hasOwnProperty(l) ? (p += n[l], s = parseInt(p, 16) * d, d = 1, p = "") : p += l : n.hasOwnProperty(l) ? (h += n[l], u[s] = parseInt(h, 16) * d, d = 1, s = o, h = "") : h += l;
                    return c
                }
                var n = {
                    codePages: ["WinAnsiEncoding"],
                    WinAnsiEncoding: t("{19m8n201n9q201o9r201s9l201t9m201u8m201w9n201x9o201y8o202k8q202l8r202m9p202q8p20aw8k203k8t203t8v203u9v2cq8s212m9t15m8w15n9w2dw9s16k8u16l9u17s9z17x8y17y9y}")
                },
                    s = {
                        Unicode: {
                            Courier: n,
                            "Courier-Bold": n,
                            "Courier-BoldOblique": n,
                            "Courier-Oblique": n,
                            Helvetica: n,
                            "Helvetica-Bold": n,
                            "Helvetica-BoldOblique": n,
                            "Helvetica-Oblique": n,
                            "Times-Roman": n,
                            "Times-Bold": n,
                            "Times-BoldItalic": n,
                            "Times-Italic": n
                        }
                    },
                    l = {
                        Unicode: {
                            "Courier-Oblique": t("{'widths'{k3w'fof'6o}'kerning'{'fof'-6o}}"),
                            "Times-BoldItalic": t("{'widths'{k3o2q4ycx2r201n3m201o6o201s2l201t2l201u2l201w3m201x3m201y3m2k1t2l2r202m2n2n3m2o3m2p5n202q6o2r1w2s2l2t2l2u3m2v3t2w1t2x2l2y1t2z1w3k3m3l3m3m3m3n3m3o3m3p3m3q3m3r3m3s3m203t2l203u2l3v2l3w3t3x3t3y3t3z3m4k5n4l4m4m4m4n4m4o4s4p4m4q4m4r4s4s4y4t2r4u3m4v4m4w3x4x5t4y4s4z4s5k3x5l4s5m4m5n3r5o3x5p4s5q4m5r5t5s4m5t3x5u3x5v2l5w1w5x2l5y3t5z3m6k2l6l3m6m3m6n2w6o3m6p2w6q2l6r3m6s3r6t1w6u1w6v3m6w1w6x4y6y3r6z3m7k3m7l3m7m2r7n2r7o1w7p3r7q2w7r4m7s3m7t2w7u2r7v2n7w1q7x2n7y3t202l3mcl4mal2ram3man3mao3map3mar3mas2lat4uau1uav3maw3way4uaz2lbk2sbl3t'fof'6obo2lbp3tbq3mbr1tbs2lbu1ybv3mbz3mck4m202k3mcm4mcn4mco4mcp4mcq5ycr4mcs4mct4mcu4mcv4mcw2r2m3rcy2rcz2rdl4sdm4sdn4sdo4sdp4sdq4sds4sdt4sdu4sdv4sdw4sdz3mek3mel3mem3men3meo3mep3meq4ser2wes2wet2weu2wev2wew1wex1wey1wez1wfl3rfm3mfn3mfo3mfp3mfq3mfr3tfs3mft3rfu3rfv3rfw3rfz2w203k6o212m6o2dw2l2cq2l3t3m3u2l17s3x19m3m}'kerning'{cl{4qu5kt5qt5rs17ss5ts}201s{201ss}201t{cks4lscmscnscoscpscls2wu2yu201ts}201x{2wu2yu}2k{201ts}2w{4qx5kx5ou5qx5rs17su5tu}2x{17su5tu5ou}2y{4qx5kx5ou5qx5rs17ss5ts}'fof'-6ofn{17sw5tw5ou5qw5rs}7t{cksclscmscnscoscps4ls}3u{17su5tu5os5qs}3v{17su5tu5os5qs}7p{17su5tu}ck{4qu5kt5qt5rs17ss5ts}4l{4qu5kt5qt5rs17ss5ts}cm{4qu5kt5qt5rs17ss5ts}cn{4qu5kt5qt5rs17ss5ts}co{4qu5kt5qt5rs17ss5ts}cp{4qu5kt5qt5rs17ss5ts}6l{4qu5ou5qw5rt17su5tu}5q{ckuclucmucnucoucpu4lu}5r{ckuclucmucnucoucpu4lu}7q{cksclscmscnscoscps4ls}6p{4qu5ou5qw5rt17sw5tw}ek{4qu5ou5qw5rt17su5tu}el{4qu5ou5qw5rt17su5tu}em{4qu5ou5qw5rt17su5tu}en{4qu5ou5qw5rt17su5tu}eo{4qu5ou5qw5rt17su5tu}ep{4qu5ou5qw5rt17su5tu}es{17ss5ts5qs4qu}et{4qu5ou5qw5rt17sw5tw}eu{4qu5ou5qw5rt17ss5ts}ev{17ss5ts5qs4qu}6z{17sw5tw5ou5qw5rs}fm{17sw5tw5ou5qw5rs}7n{201ts}fo{17sw5tw5ou5qw5rs}fp{17sw5tw5ou5qw5rs}fq{17sw5tw5ou5qw5rs}7r{cksclscmscnscoscps4ls}fs{17sw5tw5ou5qw5rs}ft{17su5tu}fu{17su5tu}fv{17su5tu}fw{17su5tu}fz{cksclscmscnscoscps4ls}}}"),
                            "Helvetica-Bold": t("{'widths'{k3s2q4scx1w201n3r201o6o201s1w201t1w201u1w201w3m201x3m201y3m2k1w2l2l202m2n2n3r2o3r2p5t202q6o2r1s2s2l2t2l2u2r2v3u2w1w2x2l2y1w2z1w3k3r3l3r3m3r3n3r3o3r3p3r3q3r3r3r3s3r203t2l203u2l3v2l3w3u3x3u3y3u3z3x4k6l4l4s4m4s4n4s4o4s4p4m4q3x4r4y4s4s4t1w4u3r4v4s4w3x4x5n4y4s4z4y5k4m5l4y5m4s5n4m5o3x5p4s5q4m5r5y5s4m5t4m5u3x5v2l5w1w5x2l5y3u5z3r6k2l6l3r6m3x6n3r6o3x6p3r6q2l6r3x6s3x6t1w6u1w6v3r6w1w6x5t6y3x6z3x7k3x7l3x7m2r7n3r7o2l7p3x7q3r7r4y7s3r7t3r7u3m7v2r7w1w7x2r7y3u202l3rcl4sal2lam3ran3rao3rap3rar3ras2lat4tau2pav3raw3uay4taz2lbk2sbl3u'fof'6obo2lbp3xbq3rbr1wbs2lbu2obv3rbz3xck4s202k3rcm4scn4sco4scp4scq6ocr4scs4mct4mcu4mcv4mcw1w2m2zcy1wcz1wdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3xek3rel3rem3ren3reo3rep3req5ter3res3ret3reu3rev3rew1wex1wey1wez1wfl3xfm3xfn3xfo3xfp3xfq3xfr3ufs3xft3xfu3xfv3xfw3xfz3r203k6o212m6o2dw2l2cq2l3t3r3u2l17s4m19m3r}'kerning'{cl{4qs5ku5ot5qs17sv5tv}201t{2ww4wy2yw}201w{2ks}201x{2ww4wy2yw}2k{201ts201xs}2w{7qs4qu5kw5os5qw5rs17su5tu7tsfzs}2x{5ow5qs}2y{7qs4qu5kw5os5qw5rs17su5tu7tsfzs}'fof'-6o7p{17su5tu5ot}ck{4qs5ku5ot5qs17sv5tv}4l{4qs5ku5ot5qs17sv5tv}cm{4qs5ku5ot5qs17sv5tv}cn{4qs5ku5ot5qs17sv5tv}co{4qs5ku5ot5qs17sv5tv}cp{4qs5ku5ot5qs17sv5tv}6l{17st5tt5os}17s{2kwclvcmvcnvcovcpv4lv4wwckv}5o{2kucltcmtcntcotcpt4lt4wtckt}5q{2ksclscmscnscoscps4ls4wvcks}5r{2ks4ws}5t{2kwclvcmvcnvcovcpv4lv4wwckv}eo{17st5tt5os}fu{17su5tu5ot}6p{17ss5ts}ek{17st5tt5os}el{17st5tt5os}em{17st5tt5os}en{17st5tt5os}6o{201ts}ep{17st5tt5os}es{17ss5ts}et{17ss5ts}eu{17ss5ts}ev{17ss5ts}6z{17su5tu5os5qt}fm{17su5tu5os5qt}fn{17su5tu5os5qt}fo{17su5tu5os5qt}fp{17su5tu5os5qt}fq{17su5tu5os5qt}fs{17su5tu5os5qt}ft{17su5tu5ot}7m{5os}fv{17su5tu5ot}fw{17su5tu5ot}}}"),
                            Courier: t("{'widths'{k3w'fof'6o}'kerning'{'fof'-6o}}"),
                            "Courier-BoldOblique": t("{'widths'{k3w'fof'6o}'kerning'{'fof'-6o}}"),
                            "Times-Bold": t("{'widths'{k3q2q5ncx2r201n3m201o6o201s2l201t2l201u2l201w3m201x3m201y3m2k1t2l2l202m2n2n3m2o3m2p6o202q6o2r1w2s2l2t2l2u3m2v3t2w1t2x2l2y1t2z1w3k3m3l3m3m3m3n3m3o3m3p3m3q3m3r3m3s3m203t2l203u2l3v2l3w3t3x3t3y3t3z3m4k5x4l4s4m4m4n4s4o4s4p4m4q3x4r4y4s4y4t2r4u3m4v4y4w4m4x5y4y4s4z4y5k3x5l4y5m4s5n3r5o4m5p4s5q4s5r6o5s4s5t4s5u4m5v2l5w1w5x2l5y3u5z3m6k2l6l3m6m3r6n2w6o3r6p2w6q2l6r3m6s3r6t1w6u2l6v3r6w1w6x5n6y3r6z3m7k3r7l3r7m2w7n2r7o2l7p3r7q3m7r4s7s3m7t3m7u2w7v2r7w1q7x2r7y3o202l3mcl4sal2lam3man3mao3map3mar3mas2lat4uau1yav3maw3tay4uaz2lbk2sbl3t'fof'6obo2lbp3rbr1tbs2lbu2lbv3mbz3mck4s202k3mcm4scn4sco4scp4scq6ocr4scs4mct4mcu4mcv4mcw2r2m3rcy2rcz2rdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3rek3mel3mem3men3meo3mep3meq4ser2wes2wet2weu2wev2wew1wex1wey1wez1wfl3rfm3mfn3mfo3mfp3mfq3mfr3tfs3mft3rfu3rfv3rfw3rfz3m203k6o212m6o2dw2l2cq2l3t3m3u2l17s4s19m3m}'kerning'{cl{4qt5ks5ot5qy5rw17sv5tv}201t{cks4lscmscnscoscpscls4wv}2k{201ts}2w{4qu5ku7mu5os5qx5ru17su5tu}2x{17su5tu5ou5qs}2y{4qv5kv7mu5ot5qz5ru17su5tu}'fof'-6o7t{cksclscmscnscoscps4ls}3u{17su5tu5os5qu}3v{17su5tu5os5qu}fu{17su5tu5ou5qu}7p{17su5tu5ou5qu}ck{4qt5ks5ot5qy5rw17sv5tv}4l{4qt5ks5ot5qy5rw17sv5tv}cm{4qt5ks5ot5qy5rw17sv5tv}cn{4qt5ks5ot5qy5rw17sv5tv}co{4qt5ks5ot5qy5rw17sv5tv}cp{4qt5ks5ot5qy5rw17sv5tv}6l{17st5tt5ou5qu}17s{ckuclucmucnucoucpu4lu4wu}5o{ckuclucmucnucoucpu4lu4wu}5q{ckzclzcmzcnzcozcpz4lz4wu}5r{ckxclxcmxcnxcoxcpx4lx4wu}5t{ckuclucmucnucoucpu4lu4wu}7q{ckuclucmucnucoucpu4lu}6p{17sw5tw5ou5qu}ek{17st5tt5qu}el{17st5tt5ou5qu}em{17st5tt5qu}en{17st5tt5qu}eo{17st5tt5qu}ep{17st5tt5ou5qu}es{17ss5ts5qu}et{17sw5tw5ou5qu}eu{17sw5tw5ou5qu}ev{17ss5ts5qu}6z{17sw5tw5ou5qu5rs}fm{17sw5tw5ou5qu5rs}fn{17sw5tw5ou5qu5rs}fo{17sw5tw5ou5qu5rs}fp{17sw5tw5ou5qu5rs}fq{17sw5tw5ou5qu5rs}7r{cktcltcmtcntcotcpt4lt5os}fs{17sw5tw5ou5qu5rs}ft{17su5tu5ou5qu}7m{5os}fv{17su5tu5ou5qu}fw{17su5tu5ou5qu}fz{cksclscmscnscoscps4ls}}}"),
                            Helvetica: t("{'widths'{k3p2q4mcx1w201n3r201o6o201s1q201t1q201u1q201w2l201x2l201y2l2k1w2l1w202m2n2n3r2o3r2p5t202q6o2r1n2s2l2t2l2u2r2v3u2w1w2x2l2y1w2z1w3k3r3l3r3m3r3n3r3o3r3p3r3q3r3r3r3s3r203t2l203u2l3v1w3w3u3x3u3y3u3z3r4k6p4l4m4m4m4n4s4o4s4p4m4q3x4r4y4s4s4t1w4u3m4v4m4w3r4x5n4y4s4z4y5k4m5l4y5m4s5n4m5o3x5p4s5q4m5r5y5s4m5t4m5u3x5v1w5w1w5x1w5y2z5z3r6k2l6l3r6m3r6n3m6o3r6p3r6q1w6r3r6s3r6t1q6u1q6v3m6w1q6x5n6y3r6z3r7k3r7l3r7m2l7n3m7o1w7p3r7q3m7r4s7s3m7t3m7u3m7v2l7w1u7x2l7y3u202l3rcl4mal2lam3ran3rao3rap3rar3ras2lat4tau2pav3raw3uay4taz2lbk2sbl3u'fof'6obo2lbp3rbr1wbs2lbu2obv3rbz3xck4m202k3rcm4mcn4mco4mcp4mcq6ocr4scs4mct4mcu4mcv4mcw1w2m2ncy1wcz1wdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3xek3rel3rem3ren3reo3rep3req5ter3mes3ret3reu3rev3rew1wex1wey1wez1wfl3rfm3rfn3rfo3rfp3rfq3rfr3ufs3xft3rfu3rfv3rfw3rfz3m203k6o212m6o2dw2l2cq2l3t3r3u1w17s4m19m3r}'kerning'{5q{4wv}cl{4qs5kw5ow5qs17sv5tv}201t{2wu4w1k2yu}201x{2wu4wy2yu}17s{2ktclucmucnu4otcpu4lu4wycoucku}2w{7qs4qz5k1m17sy5ow5qx5rsfsu5ty7tufzu}2x{17sy5ty5oy5qs}2y{7qs4qz5k1m17sy5ow5qx5rsfsu5ty7tufzu}'fof'-6o7p{17sv5tv5ow}ck{4qs5kw5ow5qs17sv5tv}4l{4qs5kw5ow5qs17sv5tv}cm{4qs5kw5ow5qs17sv5tv}cn{4qs5kw5ow5qs17sv5tv}co{4qs5kw5ow5qs17sv5tv}cp{4qs5kw5ow5qs17sv5tv}6l{17sy5ty5ow}do{17st5tt}4z{17st5tt}7s{fst}dm{17st5tt}dn{17st5tt}5o{ckwclwcmwcnwcowcpw4lw4wv}dp{17st5tt}dq{17st5tt}7t{5ow}ds{17st5tt}5t{2ktclucmucnu4otcpu4lu4wycoucku}fu{17sv5tv5ow}6p{17sy5ty5ow5qs}ek{17sy5ty5ow}el{17sy5ty5ow}em{17sy5ty5ow}en{5ty}eo{17sy5ty5ow}ep{17sy5ty5ow}es{17sy5ty5qs}et{17sy5ty5ow5qs}eu{17sy5ty5ow5qs}ev{17sy5ty5ow5qs}6z{17sy5ty5ow5qs}fm{17sy5ty5ow5qs}fn{17sy5ty5ow5qs}fo{17sy5ty5ow5qs}fp{17sy5ty5qs}fq{17sy5ty5ow5qs}7r{5ow}fs{17sy5ty5ow5qs}ft{17sv5tv5ow}7m{5ow}fv{17sv5tv5ow}fw{17sv5tv5ow}}}"),
                            "Helvetica-BoldOblique": t("{'widths'{k3s2q4scx1w201n3r201o6o201s1w201t1w201u1w201w3m201x3m201y3m2k1w2l2l202m2n2n3r2o3r2p5t202q6o2r1s2s2l2t2l2u2r2v3u2w1w2x2l2y1w2z1w3k3r3l3r3m3r3n3r3o3r3p3r3q3r3r3r3s3r203t2l203u2l3v2l3w3u3x3u3y3u3z3x4k6l4l4s4m4s4n4s4o4s4p4m4q3x4r4y4s4s4t1w4u3r4v4s4w3x4x5n4y4s4z4y5k4m5l4y5m4s5n4m5o3x5p4s5q4m5r5y5s4m5t4m5u3x5v2l5w1w5x2l5y3u5z3r6k2l6l3r6m3x6n3r6o3x6p3r6q2l6r3x6s3x6t1w6u1w6v3r6w1w6x5t6y3x6z3x7k3x7l3x7m2r7n3r7o2l7p3x7q3r7r4y7s3r7t3r7u3m7v2r7w1w7x2r7y3u202l3rcl4sal2lam3ran3rao3rap3rar3ras2lat4tau2pav3raw3uay4taz2lbk2sbl3u'fof'6obo2lbp3xbq3rbr1wbs2lbu2obv3rbz3xck4s202k3rcm4scn4sco4scp4scq6ocr4scs4mct4mcu4mcv4mcw1w2m2zcy1wcz1wdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3xek3rel3rem3ren3reo3rep3req5ter3res3ret3reu3rev3rew1wex1wey1wez1wfl3xfm3xfn3xfo3xfp3xfq3xfr3ufs3xft3xfu3xfv3xfw3xfz3r203k6o212m6o2dw2l2cq2l3t3r3u2l17s4m19m3r}'kerning'{cl{4qs5ku5ot5qs17sv5tv}201t{2ww4wy2yw}201w{2ks}201x{2ww4wy2yw}2k{201ts201xs}2w{7qs4qu5kw5os5qw5rs17su5tu7tsfzs}2x{5ow5qs}2y{7qs4qu5kw5os5qw5rs17su5tu7tsfzs}'fof'-6o7p{17su5tu5ot}ck{4qs5ku5ot5qs17sv5tv}4l{4qs5ku5ot5qs17sv5tv}cm{4qs5ku5ot5qs17sv5tv}cn{4qs5ku5ot5qs17sv5tv}co{4qs5ku5ot5qs17sv5tv}cp{4qs5ku5ot5qs17sv5tv}6l{17st5tt5os}17s{2kwclvcmvcnvcovcpv4lv4wwckv}5o{2kucltcmtcntcotcpt4lt4wtckt}5q{2ksclscmscnscoscps4ls4wvcks}5r{2ks4ws}5t{2kwclvcmvcnvcovcpv4lv4wwckv}eo{17st5tt5os}fu{17su5tu5ot}6p{17ss5ts}ek{17st5tt5os}el{17st5tt5os}em{17st5tt5os}en{17st5tt5os}6o{201ts}ep{17st5tt5os}es{17ss5ts}et{17ss5ts}eu{17ss5ts}ev{17ss5ts}6z{17su5tu5os5qt}fm{17su5tu5os5qt}fn{17su5tu5os5qt}fo{17su5tu5os5qt}fp{17su5tu5os5qt}fq{17su5tu5os5qt}fs{17su5tu5os5qt}ft{17su5tu5ot}7m{5os}fv{17su5tu5ot}fw{17su5tu5ot}}}"),
                            "Courier-Bold": t("{'widths'{k3w'fof'6o}'kerning'{'fof'-6o}}"),
                            "Times-Italic": t("{'widths'{k3n2q4ycx2l201n3m201o5t201s2l201t2l201u2l201w3r201x3r201y3r2k1t2l2l202m2n2n3m2o3m2p5n202q5t2r1p2s2l2t2l2u3m2v4n2w1t2x2l2y1t2z1w3k3m3l3m3m3m3n3m3o3m3p3m3q3m3r3m3s3m203t2l203u2l3v2l3w4n3x4n3y4n3z3m4k5w4l3x4m3x4n4m4o4s4p3x4q3x4r4s4s4s4t2l4u2w4v4m4w3r4x5n4y4m4z4s5k3x5l4s5m3x5n3m5o3r5p4s5q3x5r5n5s3x5t3r5u3r5v2r5w1w5x2r5y2u5z3m6k2l6l3m6m3m6n2w6o3m6p2w6q1w6r3m6s3m6t1w6u1w6v2w6w1w6x4s6y3m6z3m7k3m7l3m7m2r7n2r7o1w7p3m7q2w7r4m7s2w7t2w7u2r7v2s7w1v7x2s7y3q202l3mcl3xal2ram3man3mao3map3mar3mas2lat4wau1vav3maw4nay4waz2lbk2sbl4n'fof'6obo2lbp3mbq3obr1tbs2lbu1zbv3mbz3mck3x202k3mcm3xcn3xco3xcp3xcq5tcr4mcs3xct3xcu3xcv3xcw2l2m2ucy2lcz2ldl4mdm4sdn4sdo4sdp4sdq4sds4sdt4sdu4sdv4sdw4sdz3mek3mel3mem3men3meo3mep3meq4mer2wes2wet2weu2wev2wew1wex1wey1wez1wfl3mfm3mfn3mfo3mfp3mfq3mfr4nfs3mft3mfu3mfv3mfw3mfz2w203k6o212m6m2dw2l2cq2l3t3m3u2l17s3r19m3m}'kerning'{cl{5kt4qw}201s{201sw}201t{201tw2wy2yy6q-t}201x{2wy2yy}2k{201tw}2w{7qs4qy7rs5ky7mw5os5qx5ru17su5tu}2x{17ss5ts5os}2y{7qs4qy7rs5ky7mw5os5qx5ru17su5tu}'fof'-6o6t{17ss5ts5qs}7t{5os}3v{5qs}7p{17su5tu5qs}ck{5kt4qw}4l{5kt4qw}cm{5kt4qw}cn{5kt4qw}co{5kt4qw}cp{5kt4qw}6l{4qs5ks5ou5qw5ru17su5tu}17s{2ks}5q{ckvclvcmvcnvcovcpv4lv}5r{ckuclucmucnucoucpu4lu}5t{2ks}6p{4qs5ks5ou5qw5ru17su5tu}ek{4qs5ks5ou5qw5ru17su5tu}el{4qs5ks5ou5qw5ru17su5tu}em{4qs5ks5ou5qw5ru17su5tu}en{4qs5ks5ou5qw5ru17su5tu}eo{4qs5ks5ou5qw5ru17su5tu}ep{4qs5ks5ou5qw5ru17su5tu}es{5ks5qs4qs}et{4qs5ks5ou5qw5ru17su5tu}eu{4qs5ks5qw5ru17su5tu}ev{5ks5qs4qs}ex{17ss5ts5qs}6z{4qv5ks5ou5qw5ru17su5tu}fm{4qv5ks5ou5qw5ru17su5tu}fn{4qv5ks5ou5qw5ru17su5tu}fo{4qv5ks5ou5qw5ru17su5tu}fp{4qv5ks5ou5qw5ru17su5tu}fq{4qv5ks5ou5qw5ru17su5tu}7r{5os}fs{4qv5ks5ou5qw5ru17su5tu}ft{17su5tu5qs}fu{17su5tu5qs}fv{17su5tu5qs}fw{17su5tu5qs}}}"),
                            "Times-Roman": t("{'widths'{k3n2q4ycx2l201n3m201o6o201s2l201t2l201u2l201w2w201x2w201y2w2k1t2l2l202m2n2n3m2o3m2p5n202q6o2r1m2s2l2t2l2u3m2v3s2w1t2x2l2y1t2z1w3k3m3l3m3m3m3n3m3o3m3p3m3q3m3r3m3s3m203t2l203u2l3v1w3w3s3x3s3y3s3z2w4k5w4l4s4m4m4n4m4o4s4p3x4q3r4r4s4s4s4t2l4u2r4v4s4w3x4x5t4y4s4z4s5k3r5l4s5m4m5n3r5o3x5p4s5q4s5r5y5s4s5t4s5u3x5v2l5w1w5x2l5y2z5z3m6k2l6l2w6m3m6n2w6o3m6p2w6q2l6r3m6s3m6t1w6u1w6v3m6w1w6x4y6y3m6z3m7k3m7l3m7m2l7n2r7o1w7p3m7q3m7r4s7s3m7t3m7u2w7v3k7w1o7x3k7y3q202l3mcl4sal2lam3man3mao3map3mar3mas2lat4wau1vav3maw3say4waz2lbk2sbl3s'fof'6obo2lbp3mbq2xbr1tbs2lbu1zbv3mbz2wck4s202k3mcm4scn4sco4scp4scq5tcr4mcs3xct3xcu3xcv3xcw2l2m2tcy2lcz2ldl4sdm4sdn4sdo4sdp4sdq4sds4sdt4sdu4sdv4sdw4sdz3mek2wel2wem2wen2weo2wep2weq4mer2wes2wet2weu2wev2wew1wex1wey1wez1wfl3mfm3mfn3mfo3mfp3mfq3mfr3sfs3mft3mfu3mfv3mfw3mfz3m203k6o212m6m2dw2l2cq2l3t3m3u1w17s4s19m3m}'kerning'{cl{4qs5ku17sw5ou5qy5rw201ss5tw201ws}201s{201ss}201t{ckw4lwcmwcnwcowcpwclw4wu201ts}2k{201ts}2w{4qs5kw5os5qx5ru17sx5tx}2x{17sw5tw5ou5qu}2y{4qs5kw5os5qx5ru17sx5tx}'fof'-6o7t{ckuclucmucnucoucpu4lu5os5rs}3u{17su5tu5qs}3v{17su5tu5qs}7p{17sw5tw5qs}ck{4qs5ku17sw5ou5qy5rw201ss5tw201ws}4l{4qs5ku17sw5ou5qy5rw201ss5tw201ws}cm{4qs5ku17sw5ou5qy5rw201ss5tw201ws}cn{4qs5ku17sw5ou5qy5rw201ss5tw201ws}co{4qs5ku17sw5ou5qy5rw201ss5tw201ws}cp{4qs5ku17sw5ou5qy5rw201ss5tw201ws}6l{17su5tu5os5qw5rs}17s{2ktclvcmvcnvcovcpv4lv4wuckv}5o{ckwclwcmwcnwcowcpw4lw4wu}5q{ckyclycmycnycoycpy4ly4wu5ms}5r{cktcltcmtcntcotcpt4lt4ws}5t{2ktclvcmvcnvcovcpv4lv4wuckv}7q{cksclscmscnscoscps4ls}6p{17su5tu5qw5rs}ek{5qs5rs}el{17su5tu5os5qw5rs}em{17su5tu5os5qs5rs}en{17su5qs5rs}eo{5qs5rs}ep{17su5tu5os5qw5rs}es{5qs}et{17su5tu5qw5rs}eu{17su5tu5qs5rs}ev{5qs}6z{17sv5tv5os5qx5rs}fm{5os5qt5rs}fn{17sv5tv5os5qx5rs}fo{17sv5tv5os5qx5rs}fp{5os5qt5rs}fq{5os5qt5rs}7r{ckuclucmucnucoucpu4lu5os}fs{17sv5tv5os5qx5rs}ft{17ss5ts5qs}fu{17sw5tw5qs}fv{17sw5tw5qs}fw{17ss5ts5qs}fz{ckuclucmucnucoucpu4lu5os5rs}}}"),
                            "Helvetica-Oblique": t("{'widths'{k3p2q4mcx1w201n3r201o6o201s1q201t1q201u1q201w2l201x2l201y2l2k1w2l1w202m2n2n3r2o3r2p5t202q6o2r1n2s2l2t2l2u2r2v3u2w1w2x2l2y1w2z1w3k3r3l3r3m3r3n3r3o3r3p3r3q3r3r3r3s3r203t2l203u2l3v1w3w3u3x3u3y3u3z3r4k6p4l4m4m4m4n4s4o4s4p4m4q3x4r4y4s4s4t1w4u3m4v4m4w3r4x5n4y4s4z4y5k4m5l4y5m4s5n4m5o3x5p4s5q4m5r5y5s4m5t4m5u3x5v1w5w1w5x1w5y2z5z3r6k2l6l3r6m3r6n3m6o3r6p3r6q1w6r3r6s3r6t1q6u1q6v3m6w1q6x5n6y3r6z3r7k3r7l3r7m2l7n3m7o1w7p3r7q3m7r4s7s3m7t3m7u3m7v2l7w1u7x2l7y3u202l3rcl4mal2lam3ran3rao3rap3rar3ras2lat4tau2pav3raw3uay4taz2lbk2sbl3u'fof'6obo2lbp3rbr1wbs2lbu2obv3rbz3xck4m202k3rcm4mcn4mco4mcp4mcq6ocr4scs4mct4mcu4mcv4mcw1w2m2ncy1wcz1wdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3xek3rel3rem3ren3reo3rep3req5ter3mes3ret3reu3rev3rew1wex1wey1wez1wfl3rfm3rfn3rfo3rfp3rfq3rfr3ufs3xft3rfu3rfv3rfw3rfz3m203k6o212m6o2dw2l2cq2l3t3r3u1w17s4m19m3r}'kerning'{5q{4wv}cl{4qs5kw5ow5qs17sv5tv}201t{2wu4w1k2yu}201x{2wu4wy2yu}17s{2ktclucmucnu4otcpu4lu4wycoucku}2w{7qs4qz5k1m17sy5ow5qx5rsfsu5ty7tufzu}2x{17sy5ty5oy5qs}2y{7qs4qz5k1m17sy5ow5qx5rsfsu5ty7tufzu}'fof'-6o7p{17sv5tv5ow}ck{4qs5kw5ow5qs17sv5tv}4l{4qs5kw5ow5qs17sv5tv}cm{4qs5kw5ow5qs17sv5tv}cn{4qs5kw5ow5qs17sv5tv}co{4qs5kw5ow5qs17sv5tv}cp{4qs5kw5ow5qs17sv5tv}6l{17sy5ty5ow}do{17st5tt}4z{17st5tt}7s{fst}dm{17st5tt}dn{17st5tt}5o{ckwclwcmwcnwcowcpw4lw4wv}dp{17st5tt}dq{17st5tt}7t{5ow}ds{17st5tt}5t{2ktclucmucnu4otcpu4lu4wycoucku}fu{17sv5tv5ow}6p{17sy5ty5ow5qs}ek{17sy5ty5ow}el{17sy5ty5ow}em{17sy5ty5ow}en{5ty}eo{17sy5ty5ow}ep{17sy5ty5ow}es{17sy5ty5qs}et{17sy5ty5ow5qs}eu{17sy5ty5ow5qs}ev{17sy5ty5ow5qs}6z{17sy5ty5ow5qs}fm{17sy5ty5ow5qs}fn{17sy5ty5ow5qs}fo{17sy5ty5ow5qs}fp{17sy5ty5qs}fq{17sy5ty5ow5qs}7r{5ow}fs{17sy5ty5ow5qs}ft{17sv5tv5ow}7m{5ow}fv{17sv5tv5ow}fw{17sv5tv5ow}}}")
                        }
                    };
                e.events.push(["addFonts", function (e) {
                    var t, n, r, o, i, a = "Unicode";
                    for (n in e.fonts) e.fonts.hasOwnProperty(n) && (t = e.fonts[n], (r = l[a][t.PostScriptName]) && ((o = t.metadata[a] ? t.metadata[a] : t.metadata[a] = {}).widths = r.widths, o.kerning = r.kerning), (i = s[a][t.PostScriptName]) && (((o = t.metadata[a] ? t.metadata[a] : t.metadata[a] = {}).encoding = i).codePages && i.codePages.length && (t.encoding = i.codePages[0])))
                }])
            }(e.API),
            function () {
                "use strict";
                e.API.putTotalPages = function (e) {
                    for (var t = new RegExp(e, "g"), n = 1; n <= this.internal.getNumberOfPages(); n++)
                        for (var r = 0; r < this.internal.pages[n].length; r++) this.internal.pages[n][r] = this.internal.pages[n][r].replace(t, this.internal.getNumberOfPages());
                    return this
                }
            }(),
            function (e) {
                "use strict";
                if (e.URL = e.URL || e.webkitURL, e.Blob && e.URL) try {
                    return new Blob
                } catch (e) { }
                var a = e.BlobBuilder || e.WebKitBlobBuilder || e.MozBlobBuilder || function (e) {
                    function s(e) {
                        return Object.prototype.toString.call(e).match(/^\[object\s(.*)\]$/)[1]
                    }

                    function t() {
                        this.data = []
                    }

                    function l(e, t, n) {
                        this.data = e, this.size = e.length, this.type = t, this.encoding = n
                    }

                    function c(e) {
                        this.code = this[this.name = e]
                    }
                    var n = t.prototype,
                        r = l.prototype,
                        d = e.FileReaderSync,
                        o = "NOT_FOUND_ERR SECURITY_ERR ABORT_ERR NOT_READABLE_ERR ENCODING_ERR NO_MODIFICATION_ALLOWED_ERR INVALID_STATE_ERR SYNTAX_ERR".split(" "),
                        i = o.length,
                        a = e.URL || e.webkitURL || e,
                        u = a.createObjectURL,
                        f = a.revokeObjectURL,
                        p = a,
                        h = e.btoa,
                        g = e.atob,
                        m = e.ArrayBuffer,
                        v = e.Uint8Array,
                        b = /^[\w-]+:\/*\[?[\w\.:-]+\]?(?::[0-9]+)?/;
                    for (l.fake = r.fake = !0; i--;) c.prototype[o[i]] = i + 1;
                    return a.createObjectURL || (p = e.URL = function (e) {
                        var t, n = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
                        return n.href = e, "origin" in n || ("data:" === n.protocol.toLowerCase() ? n.origin = null : (t = e.match(b), n.origin = t && t[1])), n
                    }), p.createObjectURL = function (e) {
                        var t, n = e.type;
                        return null === n && (n = "application/octet-stream"), e instanceof l ? (t = "data:" + n, "base64" === e.encoding ? t + ";base64," + e.data : "URI" === e.encoding ? t + "," + decodeURIComponent(e.data) : h ? t + ";base64," + h(e.data) : t + "," + encodeURIComponent(e.data)) : u ? u.call(a, e) : void 0
                    }, p.revokeObjectURL = function (e) {
                        "data:" !== e.substring(0, 5) && f && f.call(a, e)
                    }, n.append = function (e) {
                        var t = this.data;
                        if (v && (e instanceof m || e instanceof v)) {
                            for (var n = "", r = new v(e), o = 0, i = r.length; o < i; o++) n += String.fromCharCode(r[o]);
                            t.push(n)
                        } else if ("Blob" === s(e) || "File" === s(e)) {
                            if (!d) throw new c("NOT_READABLE_ERR");
                            var a = new d;
                            t.push(a.readAsBinaryString(e))
                        } else e instanceof l ? "base64" === e.encoding && g ? t.push(g(e.data)) : "URI" === e.encoding ? t.push(decodeURIComponent(e.data)) : "raw" === e.encoding && t.push(e.data) : ("string" != typeof e && (e += ""), t.push(unescape(encodeURIComponent(e))))
                    }, n.getBlob = function (e) {
                        return arguments.length || (e = null), new l(this.data.join(""), e, "raw")
                    }, n.toString = function () {
                        return "[object BlobBuilder]"
                    }, r.slice = function (e, t, n) {
                        var r = arguments.length;
                        return r < 3 && (n = null), new l(this.data.slice(e, 1 < r ? t : this.data.length), n, this.encoding)
                    }, r.toString = function () {
                        return "[object Blob]"
                    }, r.close = function () {
                        this.size = 0, delete this.data
                    }, t
                }(e);
                e.Blob = function (e, t) {
                    var n = t && t.type || "",
                        r = new a;
                    if (e)
                        for (var o = 0, i = e.length; o < i; o++) r.append(e[o]);
                    return r.getBlob(n)
                }
            }("undefined" != typeof self && self || "undefined" != typeof window && window || this.content || this);
        var t, n, ae = ae || "undefined" != typeof navigator && navigator.msSaveOrOpenBlob && navigator.msSaveOrOpenBlob.bind(navigator) || function (f) {
            "use strict";
            if ("undefined" == typeof navigator || !/MSIE [1-9]\./.test(navigator.userAgent)) {
                var p = f.document,
                    h = function () {
                        return f.URL || f.webkitURL || f
                    },
                    g = p.createElementNS("http://www.w3.org/1999/xhtml", "a"),
                    m = "download" in g,
                    v = f.webkitRequestFileSystem,
                    b = f.requestFileSystem || v || f.mozRequestFileSystem,
                    i = function (e) {
                        (f.setImmediate || f.setTimeout)(function () {
                            throw e
                        }, 0)
                    },
                    y = "application/octet-stream",
                    w = 0,
                    k = function (e) {
                        function t() {
                            "string" == typeof e ? h().revokeObjectURL(e) : e.remove()
                        }
                        f.chrome ? t() : setTimeout(t, 10)
                    },
                    x = function (e, t, n) {
                        for (var r = (t = [].concat(t)).length; r--;) {
                            var o = e["on" + t[r]];
                            if ("function" == typeof o) try {
                                o.call(e, n || e)
                            } catch (e) {
                                i(e)
                            }
                        }
                    },
                    n = function (r, n) {
                        function e() {
                            x(l, "writestart progress write writeend".split(" "))
                        }

                        function o() {
                            !d && t || (t = h().createObjectURL(r)), a ? a.location.href = t : null == f.open(t, "_blank") && "undefined" != typeof safari && (f.location.href = t), l.readyState = l.DONE, e(), k(t)
                        }

                        function i(e) {
                            return function () {
                                return l.readyState !== l.DONE ? e.apply(this, arguments) : void 0
                            }
                        }
                        var t, a, s, l = this,
                            c = r.type,
                            d = !1,
                            u = {
                                create: !0,
                                exclusive: !1
                            };
                        return l.readyState = l.INIT, n = n || "download", m ? (t = h().createObjectURL(r), g.href = t, g.download = n, function (e) {
                            var t = p.createEvent("MouseEvents");
                            t.initMouseEvent("click", !0, !1, f, 0, 0, 0, 0, 0, !1, !1, !1, !1, 0, null), e.dispatchEvent(t)
                        }(g), l.readyState = l.DONE, e(), void k(t)) : (f.chrome && c && c !== y && (s = r.slice || r.webkitSlice, r = s.call(r, 0, r.size, y), d = !0), v && "download" !== n && (n += ".download"), c !== y && !v || (a = f), b ? (w += r.size, void b(f.TEMPORARY, w, i(function (e) {
                            e.root.getDirectory("saved", u, i(function (e) {
                                function t() {
                                    e.getFile(n, u, i(function (n) {
                                        n.createWriter(i(function (t) {
                                            t.onwriteend = function (e) {
                                                a.location.href = n.toURL(), l.readyState = l.DONE, x(l, "writeend", e), k(n)
                                            }, t.onerror = function () {
                                                var e = t.error;
                                                e.code !== e.ABORT_ERR && o()
                                            }, "writestart progress write abort".split(" ").forEach(function (e) {
                                                t["on" + e] = l["on" + e]
                                            }), t.write(r), l.abort = function () {
                                                t.abort(), l.readyState = l.DONE
                                            }, l.readyState = l.WRITING
                                        }), o)
                                    }), o)
                                }
                                e.getFile(n, {
                                    create: !1
                                }, i(function (e) {
                                    e.remove(), t()
                                }), i(function (e) {
                                    e.code === e.NOT_FOUND_ERR ? t() : o()
                                }))
                            }), o)
                        }), o)) : void o())
                    },
                    e = n.prototype;
                return e.abort = function () {
                    this.readyState = this.DONE, x(this, "abort")
                }, e.readyState = e.INIT = 0, e.WRITING = 1, e.DONE = 2, e.error = e.onwritestart = e.onprogress = e.onwrite = e.onabort = e.onerror = e.onwriteend = null,
                    function (e, t) {
                        return new n(e, t)
                    }
            }
        }("undefined" != typeof self && self || "undefined" != typeof window && window || this.content);
        "undefined" != typeof module && null !== module && (module.exports = ae), t = e, n = function () {
            function n(e, t) {
                for (var n = 65535 & e, r = e >>> 16, o = 0, i = t.length; o < i; o++) r = (r + (n = (n + (255 & t.charCodeAt(o))) % u)) % u;
                return (r << 16 | n) >>> 0
            }

            function r(e, t) {
                for (var n = 65535 & e, r = e >>> 16, o = 0, i = t.length; o < i; o++) r = (r + (n = (n + t[o]) % u)) % u;
                return (r << 16 | n) >>> 0
            }
            var e, t, o, i, a, s = "function" == typeof ArrayBuffer && "function" == typeof Uint8Array,
                l = null,
                c = function () {
                    if (!s) return function () {
                        return !1
                    };
                    try {
                        var e = require("buffer");
                        "function" == typeof e.Buffer && (l = e.Buffer)
                    } catch (e) { }
                    return function (e) {
                        return e instanceof ArrayBuffer || null !== l && e instanceof l
                    }
                }(),
                d = null !== l ? function (e) {
                    return new l(e, "utf8").toString("binary")
                } : function (e) {
                    return unescape(encodeURIComponent(e))
                },
                u = 65521,
                f = {},
                p = f.Adler32 = (((a = (i = function (e) {
                    if (!(this instanceof i)) throw new TypeError("Constructor cannot called be as a function.");
                    if (!isFinite(e = null == e ? 1 : +e)) throw new Error("First arguments needs to be a finite number.");
                    this.checksum = e >>> 0
                }).prototype = {}).constructor = i).from = ((o = function (e) {
                    if (!(this instanceof i)) throw new TypeError("Constructor cannot called be as a function.");
                    if (null == e) throw new Error("First argument needs to be a string.");
                    this.checksum = n(1, e.toString())
                }).prototype = a, o), i.fromUtf8 = ((t = function (e) {
                    if (!(this instanceof i)) throw new TypeError("Constructor cannot called be as a function.");
                    if (null == e) throw new Error("First argument needs to be a string.");
                    var t = d(e.toString());
                    this.checksum = n(1, t)
                }).prototype = a, t), s && (i.fromBuffer = ((e = function (e) {
                    if (!(this instanceof i)) throw new TypeError("Constructor cannot called be as a function.");
                    if (!c(e)) throw new Error("First argument needs to be ArrayBuffer.");
                    var t = new Uint8Array(e);
                    return this.checksum = r(1, t)
                }).prototype = a, e)), a.update = function (e) {
                    if (null == e) throw new Error("First argument needs to be a string.");
                    return e = e.toString(), this.checksum = n(this.checksum, e)
                }, a.updateUtf8 = function (e) {
                    if (null == e) throw new Error("First argument needs to be a string.");
                    var t = d(e.toString());
                    return this.checksum = n(this.checksum, t)
                }, s && (a.updateBuffer = function (e) {
                    if (!c(e)) throw new Error("First argument needs to be ArrayBuffer.");
                    var t = new Uint8Array(e);
                    return this.checksum = r(this.checksum, t)
                }), a.clone = function () {
                    return new p(this.checksum)
                }, i);
            return f.from = function (e) {
                if (null == e) throw new Error("First argument needs to be a string.");
                return n(1, e.toString())
            }, f.fromUtf8 = function (e) {
                if (null == e) throw new Error("First argument needs to be a string.");
                var t = d(e.toString());
                return n(1, t)
            }, s && (f.fromBuffer = function (e) {
                if (!c(e)) throw new Error("First argument need to be ArrayBuffer.");
                var t = new Uint8Array(e);
                return r(1, t)
            }), f
        }, "object" == typeof module ? module.exports = n() : t.adler32cs = n();
        var i, r, se = function () {
            function ue() {
                function l(e, t) {
                    for (var n = 0; n |= 1 & e, e >>>= 1, n <<= 1, 0 < --t;);
                    return n >>> 1
                }
                var p = this;
                p.build_tree = function (e) {
                    var t, n, r, o = p.dyn_tree,
                        i = p.stat_desc.static_tree,
                        a = p.stat_desc.elems,
                        s = -1;
                    for (e.heap_len = 0, e.heap_max = g, t = 0; t < a; t++) 0 !== o[2 * t] ? (e.heap[++e.heap_len] = s = t, e.depth[t] = 0) : o[2 * t + 1] = 0;
                    for (; e.heap_len < 2;) o[2 * (r = e.heap[++e.heap_len] = s < 2 ? ++s : 0)] = 1, e.depth[r] = 0, e.opt_len-- , i && (e.static_len -= i[2 * r + 1]);
                    for (p.max_code = s, t = Math.floor(e.heap_len / 2); 1 <= t; t--) e.pqdownheap(o, t);
                    for (r = a; t = e.heap[1], e.heap[1] = e.heap[e.heap_len--], e.pqdownheap(o, 1), n = e.heap[1], e.heap[--e.heap_max] = t, e.heap[--e.heap_max] = n, o[2 * r] = o[2 * t] + o[2 * n], e.depth[r] = Math.max(e.depth[t], e.depth[n]) + 1, o[2 * t + 1] = o[2 * n + 1] = r, e.heap[1] = r++ , e.pqdownheap(o, 1), 2 <= e.heap_len;);
                    e.heap[--e.heap_max] = e.heap[1],
                        function (e) {
                            var t, n, r, o, i, a, s = p.dyn_tree,
                                l = p.stat_desc.static_tree,
                                c = p.stat_desc.extra_bits,
                                d = p.stat_desc.extra_base,
                                u = p.stat_desc.max_length,
                                f = 0;
                            for (o = 0; o <= h; o++) e.bl_count[o] = 0;
                            for (s[2 * e.heap[e.heap_max] + 1] = 0, t = e.heap_max + 1; t < g; t++) u < (o = s[2 * s[2 * (n = e.heap[t]) + 1] + 1] + 1) && (o = u, f++), s[2 * n + 1] = o, n > p.max_code || (e.bl_count[o]++ , i = 0, d <= n && (i = c[n - d]), a = s[2 * n], e.opt_len += a * (o + i), l && (e.static_len += a * (l[2 * n + 1] + i)));
                            if (0 !== f) {
                                do {
                                    for (o = u - 1; 0 === e.bl_count[o];) o--;
                                    e.bl_count[o]-- , e.bl_count[o + 1] += 2, e.bl_count[u]-- , f -= 2
                                } while (0 < f);
                                for (o = u; 0 !== o; o--)
                                    for (n = e.bl_count[o]; 0 !== n;)(r = e.heap[--t]) > p.max_code || (s[2 * r + 1] != o && (e.opt_len += (o - s[2 * r + 1]) * s[2 * r], s[2 * r + 1] = o), n--)
                            }
                        }(e),
                        function (e, t, n) {
                            var r, o, i, a = [],
                                s = 0;
                            for (r = 1; r <= h; r++) a[r] = s = s + n[r - 1] << 1;
                            for (o = 0; o <= t; o++) 0 !== (i = e[2 * o + 1]) && (e[2 * o] = l(a[i]++, i))
                        }(o, p.max_code, e.bl_count)
                }
            }

            function fe(e, t, n, r, o) {
                var i = this;
                i.static_tree = e, i.extra_bits = t, i.extra_base = n, i.elems = r, i.max_length = o
            }

            function e(e, t, n, r, o) {
                var i = this;
                i.good_length = e, i.max_lazy = t, i.nice_length = n, i.max_chain = r, i.func = o
            }

            function pe(e, t, n, r) {
                var o = e[2 * t],
                    i = e[2 * n];
                return o < i || o == i && r[t] <= r[n]
            }

            function n() {
                function a() {
                    var e;
                    for (e = 0; e < 286; e++) J[2 * e] = 0;
                    for (e = 0; e < 30; e++) Y[2 * e] = 0;
                    for (e = 0; e < 19; e++) Z[2 * e] = 0;
                    J[512] = 1, se.opt_len = se.static_len = 0, te = re = 0
                }

                function s(e, t) {
                    var n, r, o = -1,
                        i = e[1],
                        a = 0,
                        s = 7,
                        l = 4;
                    for (0 === i && (s = 138, l = 3), e[2 * (t + 1) + 1] = 65535, n = 0; n <= t; n++) r = i, i = e[2 * (n + 1) + 1], ++a < s && r == i || (a < l ? Z[2 * r] += a : 0 !== r ? (r != o && Z[2 * r]++ , Z[32]++) : a <= 10 ? Z[34]++ : Z[36]++ , o = r, l = (a = 0) === i ? (s = 138, 3) : r == i ? (s = 6, 3) : (s = 7, 4))
                }

                function l(e) {
                    se.pending_buf[se.pending++] = e
                }

                function o(e) {
                    l(255 & e), l(e >>> 8 & 255)
                }

                function c(e, t) {
                    var n, r = t;
                    16 - r < ae ? (o(ie |= (n = e) << ae & 65535), ie = n >>> 16 - ae, ae += r - 16) : (ie |= e << ae & 65535, ae += r)
                }

                function d(e, t) {
                    var n = 2 * e;
                    c(65535 & t[n], 65535 & t[1 + n])
                }

                function u(e, t) {
                    var n, r, o = -1,
                        i = e[1],
                        a = 0,
                        s = 7,
                        l = 4;
                    for (0 === i && (s = 138, l = 3), n = 0; n <= t; n++)
                        if (r = i, i = e[2 * (n + 1) + 1], !(++a < s && r == i)) {
                            if (a < l)
                                for (; d(r, Z), 0 != --a;);
                            else 0 !== r ? (r != o && (d(r, Z), a--), d(16, Z), c(a - 3, 2)) : a <= 10 ? (d(17, Z), c(a - 3, 3)) : (d(18, Z), c(a - 11, 7));
                            o = r, l = (a = 0) === i ? (s = 138, 3) : r == i ? (s = 6, 3) : (s = 7, 4)
                        }
                }

                function f() {
                    16 == ae ? (o(ie), ae = ie = 0) : 8 <= ae && (l(255 & ie), ie >>>= 8, ae -= 8)
                }

                function p(e, t) {
                    var n, r, o;
                    if (se.pending_buf[ne + 2 * te] = e >>> 8 & 255, se.pending_buf[ne + 2 * te + 1] = 255 & e, se.pending_buf[Q + te] = 255 & t, te++ , 0 === e ? J[2 * t]++ : (re++ , e-- , J[2 * (ue._length_code[t] + 256 + 1)]++ , Y[2 * ue.d_code(e)]++), 0 == (8191 & te) && 2 < W) {
                        for (n = 8 * te, r = H - D, o = 0; o < 30; o++) n += Y[2 * o] * (5 + ue.extra_dbits[o]);
                        if (n >>>= 3, re < Math.floor(te / 2) && n < Math.floor(r / 2)) return !0
                    }
                    return te == ee - 1
                }

                function h(e, t) {
                    var n, r, o, i, a = 0;
                    if (0 !== te)
                        for (; n = se.pending_buf[ne + 2 * a] << 8 & 65280 | 255 & se.pending_buf[ne + 2 * a + 1], r = 255 & se.pending_buf[Q + a], a++ , 0 === n ? d(r, e) : (d((o = ue._length_code[r]) + 256 + 1, e), 0 !== (i = ue.extra_lbits[o]) && c(r -= ue.base_length[o], i), d(o = ue.d_code(--n), t), 0 !== (i = ue.extra_dbits[o]) && c(n -= ue.base_dist[o], i)), a < te;);
                    d(256, e), oe = e[513]
                }

                function g() {
                    8 < ae ? o(ie) : 0 < ae && l(255 & ie), ae = ie = 0
                }

                function m(e, t, n) {
                    c(0 + (n ? 1 : 0), 3),
                        function (e, t, n) {
                            g(), oe = 8, n && (o(t), o(~t)), se.pending_buf.set(A.subarray(e, e + t), se.pending), se.pending += t
                        }(e, t, !0)
                }

                function t(e, t, n) {
                    var r, o, i = 0;
                    0 < W ? (le.build_tree(se), ce.build_tree(se), i = function () {
                        var e;
                        for (s(J, le.max_code), s(Y, ce.max_code), de.build_tree(se), e = 18; 3 <= e && 0 === Z[2 * ue.bl_order[e] + 1]; e--);
                        return se.opt_len += 3 * (e + 1) + 5 + 5 + 4, e
                    }(), r = se.opt_len + 3 + 7 >>> 3, (o = se.static_len + 3 + 7 >>> 3) <= r && (r = o)) : r = o = t + 5, t + 4 <= r && -1 != e ? m(e, t, n) : o == r ? (c(2 + (n ? 1 : 0), 3), h(fe.static_ltree, fe.static_dtree)) : (c(4 + (n ? 1 : 0), 3), function (e, t, n) {
                        var r;
                        for (c(e - 257, 5), c(t - 1, 5), c(n - 4, 4), r = 0; r < n; r++) c(Z[2 * ue.bl_order[r] + 1], 3);
                        u(J, e - 1), u(Y, t - 1)
                    }(le.max_code + 1, ce.max_code + 1, i + 1), h(J, Y)), a(), n && g()
                }

                function v(e) {
                    t(0 <= D ? D : -1, H - D, e), D = H, k.flush_pending()
                }

                function b() {
                    var e, t, n, r;
                    do {
                        if (0 === (r = i - $ - H) && 0 === H && 0 === $) r = E;
                        else if (-1 == r) r--;
                        else if (E + E - me <= H) {
                            for (A.set(A.subarray(E, E + E), 0), z -= E, H -= E, D -= E, n = e = N; t = 65535 & B[--n], B[n] = E <= t ? t - E : 0, 0 != --e;);
                            for (n = e = E; t = 65535 & O[--n], O[n] = E <= t ? t - E : 0, 0 != --e;);
                            r += E
                        }
                        if (0 === k.avail_in) return;
                        e = k.read_buf(A, H + $, r), 3 <= ($ += e) && (L = ((L = 255 & A[H]) << R ^ 255 & A[H + 1]) & P)
                    } while ($ < me && 0 !== k.avail_in)
                }

                function y(e) {
                    var t, n, r = j,
                        o = H,
                        i = U,
                        a = E - me < H ? H - (E - me) : 0,
                        s = X,
                        l = S,
                        c = H + 258,
                        d = A[o + i - 1],
                        u = A[o + i];
                    K <= U && (r >>= 2), $ < s && (s = $);
                    do {
                        if (A[(t = e) + i] == u && A[t + i - 1] == d && A[t] == A[o] && A[++t] == A[o + 1]) {
                            o += 2, t++;
                            do { } while (A[++o] == A[++t] && A[++o] == A[++t] && A[++o] == A[++t] && A[++o] == A[++t] && A[++o] == A[++t] && A[++o] == A[++t] && A[++o] == A[++t] && A[++o] == A[++t] && o < c);
                            if (n = 258 - (c - o), o = c - 258, i < n) {
                                if (z = e, s <= (i = n)) break;
                                d = A[o + i - 1], u = A[o + i]
                            }
                        }
                    } while ((e = 65535 & O[e & l]) > a && 0 != --r);
                    return i <= $ ? i : $
                }

                function w(e) {
                    return e.total_in = e.total_out = 0, e.msg = null, se.pending = 0, se.pending_out = 0, x = 113, I = 0, le.dyn_tree = J, le.stat_desc = fe.static_l_desc, ce.dyn_tree = Y, ce.stat_desc = fe.static_d_desc, de.dyn_tree = Z, de.stat_desc = fe.static_bl_desc, ae = ie = 0, oe = 8, a(),
                        function () {
                            var e;
                            for (i = 2 * E, e = B[N - 1] = 0; e < N - 1; e++) B[e] = 0;
                            V = he[W].max_lazy, K = he[W].good_length, X = he[W].nice_length, j = he[W].max_chain, F = U = 2, L = q = $ = D = H = 0
                        }(), 0
                }
                var k, x, C, I, E, T, S, A, i, O, B, L, N, M, P, R, D, F, _, q, H, z, $, U, j, V, W, G, K, X, J, Y, Z, Q, ee, te, ne, re, oe, ie, ae, se = this,
                    le = new ue,
                    ce = new ue,
                    de = new ue;
                se.depth = [], se.bl_count = [], se.heap = [], J = [], Y = [], Z = [], se.pqdownheap = function (e, t) {
                    for (var n = se.heap, r = n[t], o = t << 1; o <= se.heap_len && (o < se.heap_len && pe(e, n[o + 1], n[o], se.depth) && o++ , !pe(e, r, n[o], se.depth));) n[t] = n[o], t = o, o <<= 1;
                    n[t] = r
                }, se.deflateInit = function (e, t, n, r, o, i) {
                    return r = r || 8, o = o || 8, i = i || 0, e.msg = null, -1 == t && (t = 6), o < 1 || 9 < o || 8 != r || n < 9 || 15 < n || t < 0 || 9 < t || i < 0 || 2 < i ? -2 : (e.dstate = se, S = (E = 1 << (T = n)) - 1, P = (N = 1 << (M = o + 7)) - 1, R = Math.floor((M + 3 - 1) / 3), A = new Uint8Array(2 * E), O = [], B = [], ee = 1 << o + 6, se.pending_buf = new Uint8Array(4 * ee), C = 4 * ee, ne = Math.floor(ee / 2), Q = 3 * ee, W = t, G = i, 255 & r, w(e))
                }, se.deflateEnd = function () {
                    return 42 != x && 113 != x && 666 != x ? -2 : (se.pending_buf = null, A = O = B = null, se.dstate = null, 113 == x ? -3 : 0)
                }, se.deflateParams = function (e, t, n) {
                    var r = 0;
                    return -1 == t && (t = 6), t < 0 || 9 < t || n < 0 || 2 < n ? -2 : (he[W].func != he[t].func && 0 !== e.total_in && (r = e.deflate(1)), W != t && (V = he[W = t].max_lazy, K = he[W].good_length, X = he[W].nice_length, j = he[W].max_chain), G = n, r)
                }, se.deflateSetDictionary = function (e, t, n) {
                    var r, o = n,
                        i = 0;
                    if (!t || 42 != x) return -2;
                    if (o < 3) return 0;
                    for (E - me < o && (i = n - (o = E - me)), A.set(t.subarray(i, i + o), 0), D = H = o, L = ((L = 255 & A[0]) << R ^ 255 & A[1]) & P, r = 0; r <= o - 3; r++) L = (L << R ^ 255 & A[r + 2]) & P, O[r & S] = B[L], B[L] = r;
                    return 0
                }, se.deflate = function (e, t) {
                    var n, r, o, i, a;
                    if (4 < t || t < 0) return -2;
                    if (!e.next_out || !e.next_in && 0 !== e.avail_in || 666 == x && 4 != t) return e.msg = ge[4], -2;
                    if (0 === e.avail_out) return e.msg = ge[7], -5;
                    if (k = e, i = I, I = t, 42 == x && (r = 8 + (T - 8 << 4) << 8, 3 < (o = (W - 1 & 255) >> 1) && (o = 3), r |= o << 6, 0 !== H && (r |= 32), x = 113, function (e) {
                        l(e >> 8 & 255), l(255 & e)
                    }(r += 31 - r % 31)), 0 !== se.pending) {
                        if (k.flush_pending(), 0 === k.avail_out) return I = -1, 0
                    } else if (0 === k.avail_in && t <= i && 4 != t) return k.msg = ge[7], -5;
                    if (666 == x && 0 !== k.avail_in) return e.msg = ge[7], -5;
                    if (0 !== k.avail_in || 0 !== $ || 0 != t && 666 != x) {
                        switch (a = -1, he[W].func) {
                            case 0:
                                a = function (e) {
                                    var t, n = 65535;
                                    for (C - 5 < n && (n = C - 5); ;) {
                                        if ($ <= 1) {
                                            if (b(), 0 === $ && 0 == e) return 0;
                                            if (0 === $) break
                                        }
                                        if (H += $, t = D + n, (($ = 0) === H || t <= H) && ($ = H - t, H = t, v(!1), 0 === k.avail_out)) return 0;
                                        if (E - me <= H - D && (v(!1), 0 === k.avail_out)) return 0
                                    }
                                    return v(4 == e), 0 === k.avail_out ? 4 == e ? 2 : 0 : 4 == e ? 3 : 1
                                }(t);
                                break;
                            case 1:
                                a = function (e) {
                                    for (var t, n = 0; ;) {
                                        if ($ < me) {
                                            if (b(), $ < me && 0 == e) return 0;
                                            if (0 === $) break
                                        }
                                        if (3 <= $ && (L = (L << R ^ 255 & A[H + 2]) & P, n = 65535 & B[L], O[H & S] = B[L], B[L] = H), 0 !== n && (H - n & 65535) <= E - me && 2 != G && (F = y(n)), 3 <= F)
                                            if (t = p(H - z, F - 3), $ -= F, F <= V && 3 <= $) {
                                                for (F--; L = (L << R ^ 255 & A[++H + 2]) & P, n = 65535 & B[L], O[H & S] = B[L], B[L] = H, 0 != --F;);
                                                H++
                                            } else H += F, F = 0, L = ((L = 255 & A[H]) << R ^ 255 & A[H + 1]) & P;
                                        else t = p(0, 255 & A[H]), $-- , H++;
                                        if (t && (v(!1), 0 === k.avail_out)) return 0
                                    }
                                    return v(4 == e), 0 === k.avail_out ? 4 == e ? 2 : 0 : 4 == e ? 3 : 1
                                }(t);
                                break;
                            case 2:
                                a = function (e) {
                                    for (var t, n, r = 0; ;) {
                                        if ($ < me) {
                                            if (b(), $ < me && 0 == e) return 0;
                                            if (0 === $) break
                                        }
                                        if (3 <= $ && (L = (L << R ^ 255 & A[H + 2]) & P, r = 65535 & B[L], O[H & S] = B[L], B[L] = H), U = F, _ = z, F = 2, 0 !== r && U < V && (H - r & 65535) <= E - me && (2 != G && (F = y(r)), F <= 5 && (1 == G || 3 == F && 4096 < H - z) && (F = 2)), 3 <= U && F <= U) {
                                            for (n = H + $ - 3, t = p(H - 1 - _, U - 3), $ -= U - 1, U -= 2; ++H <= n && (L = (L << R ^ 255 & A[H + 2]) & P, r = 65535 & B[L], O[H & S] = B[L], B[L] = H), 0 != --U;);
                                            if (q = 0, F = 2, H++ , t && (v(!1), 0 === k.avail_out)) return 0
                                        } else if (0 !== q) {
                                            if ((t = p(0, 255 & A[H - 1])) && v(!1), H++ , $-- , 0 === k.avail_out) return 0
                                        } else q = 1, H++ , $--
                                    }
                                    return 0 !== q && (t = p(0, 255 & A[H - 1]), q = 0), v(4 == e), 0 === k.avail_out ? 4 == e ? 2 : 0 : 4 == e ? 3 : 1
                                }(t)
                        }
                        if (2 != a && 3 != a || (x = 666), 0 == a || 2 == a) return 0 === k.avail_out && (I = -1), 0;
                        if (1 == a) {
                            if (1 == t) c(2, 3), d(256, fe.static_ltree), f(), 1 + oe + 10 - ae < 9 && (c(2, 3), d(256, fe.static_ltree), f()), oe = 7;
                            else if (m(0, 0, !1), 3 == t)
                                for (n = 0; n < N; n++) B[n] = 0;
                            if (k.flush_pending(), 0 === k.avail_out) return I = -1, 0
                        }
                    }
                    return 4 != t ? 0 : 1
                }
            }

            function t() {
                var e = this;
                e.next_in_index = 0, e.next_out_index = 0, e.avail_in = 0, e.total_in = 0, e.avail_out = 0, e.total_out = 0
            }
            var h = 15,
                g = 573,
                r = [0, 1, 2, 3, 4, 4, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 0, 0, 16, 17, 18, 18, 19, 19, 20, 20, 20, 20, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 22, 22, 23, 23, 23, 23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29];
            ue._length_code = [0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13, 14, 14, 14, 14, 15, 15, 15, 15, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18, 18, 19, 19, 19, 19, 19, 19, 19, 19, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 28], ue.base_length = [0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 14, 16, 20, 24, 28, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 0], ue.base_dist = [0, 1, 2, 3, 4, 6, 8, 12, 16, 24, 32, 48, 64, 96, 128, 192, 256, 384, 512, 768, 1024, 1536, 2048, 3072, 4096, 6144, 8192, 12288, 16384, 24576], ue.d_code = function (e) {
                return e < 256 ? r[e] : r[256 + (e >>> 7)]
            }, ue.extra_lbits = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0], ue.extra_dbits = [0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13], ue.extra_blbits = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 7], ue.bl_order = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15], fe.static_ltree = [12, 8, 140, 8, 76, 8, 204, 8, 44, 8, 172, 8, 108, 8, 236, 8, 28, 8, 156, 8, 92, 8, 220, 8, 60, 8, 188, 8, 124, 8, 252, 8, 2, 8, 130, 8, 66, 8, 194, 8, 34, 8, 162, 8, 98, 8, 226, 8, 18, 8, 146, 8, 82, 8, 210, 8, 50, 8, 178, 8, 114, 8, 242, 8, 10, 8, 138, 8, 74, 8, 202, 8, 42, 8, 170, 8, 106, 8, 234, 8, 26, 8, 154, 8, 90, 8, 218, 8, 58, 8, 186, 8, 122, 8, 250, 8, 6, 8, 134, 8, 70, 8, 198, 8, 38, 8, 166, 8, 102, 8, 230, 8, 22, 8, 150, 8, 86, 8, 214, 8, 54, 8, 182, 8, 118, 8, 246, 8, 14, 8, 142, 8, 78, 8, 206, 8, 46, 8, 174, 8, 110, 8, 238, 8, 30, 8, 158, 8, 94, 8, 222, 8, 62, 8, 190, 8, 126, 8, 254, 8, 1, 8, 129, 8, 65, 8, 193, 8, 33, 8, 161, 8, 97, 8, 225, 8, 17, 8, 145, 8, 81, 8, 209, 8, 49, 8, 177, 8, 113, 8, 241, 8, 9, 8, 137, 8, 73, 8, 201, 8, 41, 8, 169, 8, 105, 8, 233, 8, 25, 8, 153, 8, 89, 8, 217, 8, 57, 8, 185, 8, 121, 8, 249, 8, 5, 8, 133, 8, 69, 8, 197, 8, 37, 8, 165, 8, 101, 8, 229, 8, 21, 8, 149, 8, 85, 8, 213, 8, 53, 8, 181, 8, 117, 8, 245, 8, 13, 8, 141, 8, 77, 8, 205, 8, 45, 8, 173, 8, 109, 8, 237, 8, 29, 8, 157, 8, 93, 8, 221, 8, 61, 8, 189, 8, 125, 8, 253, 8, 19, 9, 275, 9, 147, 9, 403, 9, 83, 9, 339, 9, 211, 9, 467, 9, 51, 9, 307, 9, 179, 9, 435, 9, 115, 9, 371, 9, 243, 9, 499, 9, 11, 9, 267, 9, 139, 9, 395, 9, 75, 9, 331, 9, 203, 9, 459, 9, 43, 9, 299, 9, 171, 9, 427, 9, 107, 9, 363, 9, 235, 9, 491, 9, 27, 9, 283, 9, 155, 9, 411, 9, 91, 9, 347, 9, 219, 9, 475, 9, 59, 9, 315, 9, 187, 9, 443, 9, 123, 9, 379, 9, 251, 9, 507, 9, 7, 9, 263, 9, 135, 9, 391, 9, 71, 9, 327, 9, 199, 9, 455, 9, 39, 9, 295, 9, 167, 9, 423, 9, 103, 9, 359, 9, 231, 9, 487, 9, 23, 9, 279, 9, 151, 9, 407, 9, 87, 9, 343, 9, 215, 9, 471, 9, 55, 9, 311, 9, 183, 9, 439, 9, 119, 9, 375, 9, 247, 9, 503, 9, 15, 9, 271, 9, 143, 9, 399, 9, 79, 9, 335, 9, 207, 9, 463, 9, 47, 9, 303, 9, 175, 9, 431, 9, 111, 9, 367, 9, 239, 9, 495, 9, 31, 9, 287, 9, 159, 9, 415, 9, 95, 9, 351, 9, 223, 9, 479, 9, 63, 9, 319, 9, 191, 9, 447, 9, 127, 9, 383, 9, 255, 9, 511, 9, 0, 7, 64, 7, 32, 7, 96, 7, 16, 7, 80, 7, 48, 7, 112, 7, 8, 7, 72, 7, 40, 7, 104, 7, 24, 7, 88, 7, 56, 7, 120, 7, 4, 7, 68, 7, 36, 7, 100, 7, 20, 7, 84, 7, 52, 7, 116, 7, 3, 8, 131, 8, 67, 8, 195, 8, 35, 8, 163, 8, 99, 8, 227, 8], fe.static_dtree = [0, 5, 16, 5, 8, 5, 24, 5, 4, 5, 20, 5, 12, 5, 28, 5, 2, 5, 18, 5, 10, 5, 26, 5, 6, 5, 22, 5, 14, 5, 30, 5, 1, 5, 17, 5, 9, 5, 25, 5, 5, 5, 21, 5, 13, 5, 29, 5, 3, 5, 19, 5, 11, 5, 27, 5, 7, 5, 23, 5], fe.static_l_desc = new fe(fe.static_ltree, ue.extra_lbits, 257, 286, h), fe.static_d_desc = new fe(fe.static_dtree, ue.extra_dbits, 0, 30, h), fe.static_bl_desc = new fe(null, ue.extra_blbits, 0, 19, 7);
            var he = [new e(0, 0, 0, 0, 0), new e(4, 4, 8, 4, 1), new e(4, 5, 16, 8, 1), new e(4, 6, 32, 32, 1), new e(4, 4, 16, 16, 2), new e(8, 16, 32, 32, 2), new e(8, 16, 128, 128, 2), new e(8, 32, 128, 256, 2), new e(32, 128, 258, 1024, 2), new e(32, 258, 258, 4096, 2)],
                ge = ["need dictionary", "stream end", "", "", "stream error", "data error", "", "buffer error", "", ""],
                me = 262;
            return t.prototype = {
                deflateInit: function (e, t) {
                    return this.dstate = new n, t = t || h, this.dstate.deflateInit(this, e, t)
                },
                deflate: function (e) {
                    return this.dstate ? this.dstate.deflate(this, e) : -2
                },
                deflateEnd: function () {
                    if (!this.dstate) return -2;
                    var e = this.dstate.deflateEnd();
                    return this.dstate = null, e
                },
                deflateParams: function (e, t) {
                    return this.dstate ? this.dstate.deflateParams(this, e, t) : -2
                },
                deflateSetDictionary: function (e, t) {
                    return this.dstate ? this.dstate.deflateSetDictionary(this, e, t) : -2
                },
                read_buf: function (e, t, n) {
                    var r = this,
                        o = r.avail_in;
                    return n < o && (o = n), 0 === o ? 0 : (r.avail_in -= o, e.set(r.next_in.subarray(r.next_in_index, r.next_in_index + o), t), r.next_in_index += o, r.total_in += o, o)
                },
                flush_pending: function () {
                    var e = this,
                        t = e.dstate.pending;
                    t > e.avail_out && (t = e.avail_out), 0 !== t && (e.next_out.set(e.dstate.pending_buf.subarray(e.dstate.pending_out, e.dstate.pending_out + t), e.next_out_index), e.next_out_index += t, e.dstate.pending_out += t, e.total_out += t, e.avail_out -= t, e.dstate.pending -= t, 0 === e.dstate.pending && (e.dstate.pending_out = 0))
                }
            },
                function (e) {
                    var s = new t,
                        l = new Uint8Array(512);
                    void 0 === e && (e = -1), s.deflateInit(e), s.next_out = l, this.append = function (e, t) {
                        var n, r = [],
                            o = 0,
                            i = 0,
                            a = 0;
                        if (e.length) {
                            s.next_in_index = 0, s.next_in = e, s.avail_in = e.length;
                            do {
                                if (s.next_out_index = 0, s.avail_out = 512, 0 != s.deflate(0)) throw "deflating: " + s.msg;
                                s.next_out_index && r.push(512 == s.next_out_index ? new Uint8Array(l) : new Uint8Array(l.subarray(0, s.next_out_index))), a += s.next_out_index, t && 0 < s.next_in_index && s.next_in_index != o && (t(s.next_in_index), o = s.next_in_index)
                            } while (0 < s.avail_in || 0 === s.avail_out);
                            return n = new Uint8Array(a), r.forEach(function (e) {
                                n.set(e, i), i += e.length
                            }), n
                        }
                    }, this.flush = function () {
                        var e, t, n = [],
                            r = 0,
                            o = 0;
                        do {
                            if (s.next_out_index = 0, s.avail_out = 512, 1 != (e = s.deflate(4)) && 0 != e) throw "deflating: " + s.msg;
                            0 < 512 - s.avail_out && n.push(new Uint8Array(l.subarray(0, s.next_out_index))), o += s.next_out_index
                        } while (0 < s.avail_in || 0 === s.avail_out);
                        return s.deflateEnd(), t = new Uint8Array(o), n.forEach(function (e) {
                            t.set(e, r), r += e.length
                        }), t
                    }
                }
        }();
        i = "undefined" != typeof window && window || this, r = function () {
            function o(e) {
                var t, n, r, o, i, a, s, l, c, d, u, f, p, h;
                for (this.data = e, this.pos = 8, this.palette = [], this.imgData = [], this.transparency = {}, this.animation = null, this.text = {}, a = null; ;) {
                    switch (t = this.readUInt32(), c = function () {
                        var e, t;
                        for (t = [], e = 0; e < 4; ++e) t.push(String.fromCharCode(this.data[this.pos++]));
                        return t
                    }.call(this).join("")) {
                        case "IHDR":
                            this.width = this.readUInt32(), this.height = this.readUInt32(), this.bits = this.data[this.pos++], this.colorType = this.data[this.pos++], this.compressionMethod = this.data[this.pos++], this.filterMethod = this.data[this.pos++], this.interlaceMethod = this.data[this.pos++];
                            break;
                        case "acTL":
                            this.animation = {
                                numFrames: this.readUInt32(),
                                numPlays: this.readUInt32() || 1 / 0,
                                frames: []
                            };
                            break;
                        case "PLTE":
                            this.palette = this.read(t);
                            break;
                        case "fcTL":
                            a && this.animation.frames.push(a), this.pos += 4, a = {
                                width: this.readUInt32(),
                                height: this.readUInt32(),
                                xOffset: this.readUInt32(),
                                yOffset: this.readUInt32()
                            }, i = this.readUInt16(), o = this.readUInt16() || 100, a.delay = 1e3 * i / o, a.disposeOp = this.data[this.pos++], a.blendOp = this.data[this.pos++], a.data = [];
                            break;
                        case "IDAT":
                        case "fdAT":
                            for ("fdAT" === c && (this.pos += 4, t -= 4), e = (null != a ? a.data : void 0) || this.imgData, f = 0; 0 <= t ? f < t : t < f; 0 <= t ? ++f : --f) e.push(this.data[this.pos++]);
                            break;
                        case "tRNS":
                            switch (this.transparency = {}, this.colorType) {
                                case 3:
                                    if (r = this.palette.length / 3, this.transparency.indexed = this.read(t), this.transparency.indexed.length > r) throw new Error("More transparent colors than palette size");
                                    if (0 < (d = r - this.transparency.indexed.length))
                                        for (p = 0; 0 <= d ? p < d : d < p; 0 <= d ? ++p : --p) this.transparency.indexed.push(255);
                                    break;
                                case 0:
                                    this.transparency.grayscale = this.read(t)[0];
                                    break;
                                case 2:
                                    this.transparency.rgb = this.read(t)
                            }
                            break;
                        case "tEXt":
                            s = (u = this.read(t)).indexOf(0), l = String.fromCharCode.apply(String, u.slice(0, s)), this.text[l] = String.fromCharCode.apply(String, u.slice(s + 1));
                            break;
                        case "IEND":
                            return a && this.animation.frames.push(a), this.colors = function () {
                                switch (this.colorType) {
                                    case 0:
                                    case 3:
                                    case 4:
                                        return 1;
                                    case 2:
                                    case 6:
                                        return 3
                                }
                            }.call(this), this.hasAlphaChannel = 4 === (h = this.colorType) || 6 === h, n = this.colors + (this.hasAlphaChannel ? 1 : 0), this.pixelBitlength = this.bits * n, this.colorSpace = function () {
                                switch (this.colors) {
                                    case 1:
                                        return "DeviceGray";
                                    case 3:
                                        return "DeviceRGB"
                                }
                            }.call(this), void (this.imgData = new Uint8Array(this.imgData));
                        default:
                            this.pos += t
                    }
                    if (this.pos += 4, this.pos > this.data.length) throw new Error("Incomplete or corrupt PNG file")
                }
            }
            var c, n, r;
            o.load = function (e, t, n) {
                var r;
                return "function" == typeof t && (n = t), (r = new XMLHttpRequest).open("GET", e, !0), r.responseType = "arraybuffer", r.onload = function () {
                    var e;
                    return e = new o(new Uint8Array(r.response || r.mozResponseArrayBuffer)), "function" == typeof (null != t ? t.getContext : void 0) && e.render(t), "function" == typeof n ? n(e) : void 0
                }, r.send(null)
            }, o.prototype.read = function (e) {
                var t, n;
                for (n = [], t = 0; 0 <= e ? t < e : e < t; 0 <= e ? ++t : --t) n.push(this.data[this.pos++]);
                return n
            }, o.prototype.readUInt32 = function () {
                return this.data[this.pos++] << 24 | this.data[this.pos++] << 16 | this.data[this.pos++] << 8 | this.data[this.pos++]
            }, o.prototype.readUInt16 = function () {
                return this.data[this.pos++] << 8 | this.data[this.pos++]
            }, o.prototype.decodePixels = function (e) {
                var t, n, r, o, i, a, s, l, c, d, u, f, p, h, g, m, v, b, y, w, k, x, C;
                if (null == e && (e = this.imgData), 0 === e.length) return new Uint8Array(0);
                for (e = (e = new O(e)).getBytes(), m = (f = this.pixelBitlength / 8) * this.width, p = new Uint8Array(m * this.height), a = e.length, n = h = g = 0; h < a;) {
                    switch (e[h++]) {
                        case 0:
                            for (o = y = 0; y < m; o = y += 1) p[n++] = e[h++];
                            break;
                        case 1:
                            for (o = w = 0; w < m; o = w += 1) t = e[h++], i = o < f ? 0 : p[n - f], p[n++] = (t + i) % 256;
                            break;
                        case 2:
                            for (o = k = 0; k < m; o = k += 1) t = e[h++], r = (o - o % f) / f, v = g && p[(g - 1) * m + r * f + o % f], p[n++] = (v + t) % 256;
                            break;
                        case 3:
                            for (o = x = 0; x < m; o = x += 1) t = e[h++], r = (o - o % f) / f, i = o < f ? 0 : p[n - f], v = g && p[(g - 1) * m + r * f + o % f], p[n++] = (t + Math.floor((i + v) / 2)) % 256;
                            break;
                        case 4:
                            for (o = C = 0; C < m; o = C += 1) t = e[h++], r = (o - o % f) / f, i = o < f ? 0 : p[n - f], 0 === g ? v = b = 0 : (v = p[(g - 1) * m + r * f + o % f], b = r && p[(g - 1) * m + (r - 1) * f + o % f]), s = i + v - b, l = Math.abs(s - i), d = Math.abs(s - v), u = Math.abs(s - b), c = l <= d && l <= u ? i : d <= u ? v : b, p[n++] = (t + c) % 256;
                            break;
                        default:
                            throw new Error("Invalid filter algorithm: " + e[h - 1])
                    }
                    g++
                }
                return p
            }, o.prototype.decodePalette = function () {
                var e, t, n, r, o, i, a, s, l;
                for (n = this.palette, i = this.transparency.indexed || [], o = new Uint8Array((i.length || 0) + n.length), r = 0, n.length, t = a = e = 0, s = n.length; a < s; t = a += 3) o[r++] = n[t], o[r++] = n[t + 1], o[r++] = n[t + 2], o[r++] = null != (l = i[e++]) ? l : 255;
                return o
            }, o.prototype.copyToImageData = function (e, t) {
                var n, r, o, i, a, s, l, c, d, u, f;
                if (r = this.colors, d = null, n = this.hasAlphaChannel, this.palette.length && (d = null != (f = this._decodedPalette) ? f : this._decodedPalette = this.decodePalette(), r = 4, n = !0), c = (o = e.data || e).length, a = d || t, i = s = 0, 1 === r)
                    for (; i < c;) l = d ? 4 * t[i / 4] : s, u = a[l++], o[i++] = u, o[i++] = u, o[i++] = u, o[i++] = n ? a[l++] : 255, s = l;
                else
                    for (; i < c;) l = d ? 4 * t[i / 4] : s, o[i++] = a[l++], o[i++] = a[l++], o[i++] = a[l++], o[i++] = n ? a[l++] : 255, s = l
            }, o.prototype.decode = function () {
                var e;
                return e = new Uint8Array(this.width * this.height * 4), this.copyToImageData(e, this.decodePixels()), e
            };
            try {
                n = i.document.createElement("canvas"), r = n.getContext("2d")
            } catch (e) {
                return -1
            }
            return c = function (e) {
                var t;
                return r.width = e.width, r.height = e.height, r.clearRect(0, 0, e.width, e.height), r.putImageData(e, 0, 0), (t = new Image).src = n.toDataURL(), t
            }, o.prototype.decodeFrames = function (e) {
                var t, n, r, o, i, a, s, l;
                if (this.animation) {
                    for (l = [], n = i = 0, a = (s = this.animation.frames).length; i < a; n = ++i) t = s[n], r = e.createImageData(t.width, t.height), o = this.decodePixels(new Uint8Array(t.data)), this.copyToImageData(r, o), t.imageData = r, l.push(t.image = c(r));
                    return l
                }
            }, o.prototype.renderFrame = function (e, t) {
                var n, r, o;
                return n = (r = this.animation.frames)[t], o = r[t - 1], 0 === t && e.clearRect(0, 0, this.width, this.height), 1 === (null != o ? o.disposeOp : void 0) ? e.clearRect(o.xOffset, o.yOffset, o.width, o.height) : 2 === (null != o ? o.disposeOp : void 0) && e.putImageData(o.imageData, o.xOffset, o.yOffset), 0 === n.blendOp && e.clearRect(n.xOffset, n.yOffset, n.width, n.height), e.drawImage(n.image, n.xOffset, n.yOffset)
            }, o.prototype.animate = function (n) {
                var r, o, i, a, s, e, l = this;
                return o = 0, e = this.animation, a = e.numFrames, i = e.frames, s = e.numPlays, (r = function () {
                    var e, t;
                    return e = o++ % a, t = i[e], l.renderFrame(n, e), 1 < a && o / a < s ? l.animation._timeout = setTimeout(r, t.delay) : void 0
                })()
            }, o.prototype.stopAnimation = function () {
                var e;
                return clearTimeout(null != (e = this.animation) ? e._timeout : void 0)
            }, o.prototype.render = function (e) {
                var t, n;
                return e._png && e._png.stopAnimation(), e._png = this, e.width = this.width, e.height = this.height, t = e.getContext("2d"), this.animation ? (this.decodeFrames(t), this.animate(t)) : (n = t.createImageData(this.width, this.height), this.copyToImageData(n, this.decodePixels()), t.putImageData(n, 0, 0))
            }, o
        }(), i.PNG = r;
        var o, u, a = (s.prototype = {
            ensureBuffer: function (e) {
                var t = this.buffer,
                    n = t ? t.byteLength : 0;
                if (e < n) return t;
                for (var r = 512; r < e;) r <<= 1;
                for (var o = new Uint8Array(r), i = 0; i < n; ++i) o[i] = t[i];
                return this.buffer = o
            },
            getByte: function () {
                for (var e = this.pos; this.bufferLength <= e;) {
                    if (this.eof) return null;
                    this.readBlock()
                }
                return this.buffer[this.pos++]
            },
            getBytes: function (e) {
                var t = this.pos;
                if (e) {
                    this.ensureBuffer(t + e);
                    for (var n = t + e; !this.eof && this.bufferLength < n;) this.readBlock();
                    var r = this.bufferLength;
                    r < n && (n = r)
                } else {
                    for (; !this.eof;) this.readBlock();
                    n = this.bufferLength
                }
                return this.pos = n, this.buffer.subarray(t, n)
            },
            lookChar: function () {
                for (var e = this.pos; this.bufferLength <= e;) {
                    if (this.eof) return null;
                    this.readBlock()
                }
                return String.fromCharCode(this.buffer[this.pos])
            },
            getChar: function () {
                for (var e = this.pos; this.bufferLength <= e;) {
                    if (this.eof) return null;
                    this.readBlock()
                }
                return String.fromCharCode(this.buffer[this.pos++])
            },
            makeSubStream: function (e, t, n) {
                for (var r = e + t; this.bufferLength <= r && !this.eof;) this.readBlock();
                return new Stream(this.buffer, e, t, n)
            },
            skip: function (e) {
                e = e || 1, this.pos += e
            },
            reset: function () {
                this.pos = 0
            }
        }, s),
            O = function () {
                function O(e) {
                    throw new Error(e)
                }

                function e(e) {
                    var t = 0,
                        n = e[t++],
                        r = e[t++]; - 1 != n && -1 != r || O("Invalid header in flate stream"), 8 != (15 & n) && O("Unknown compression method in flate stream"), ((n << 8) + r) % 31 != 0 && O("Bad FCHECK in flate stream"), 32 & r && O("FDICT bit set in flate stream"), this.bytes = e, this.bytesPos = 2, this.codeSize = 0, this.codeBuf = 0, a.call(this)
                }
                if ("undefined" != typeof Uint32Array) {
                    var B = new Uint32Array([16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15]),
                        L = new Uint32Array([3, 4, 5, 6, 7, 8, 9, 10, 65547, 65549, 65551, 65553, 131091, 131095, 131099, 131103, 196643, 196651, 196659, 196667, 262211, 262227, 262243, 262259, 327811, 327843, 327875, 327907, 258, 258, 258]),
                        N = new Uint32Array([1, 2, 3, 4, 65541, 65543, 131081, 131085, 196625, 196633, 262177, 262193, 327745, 327777, 393345, 393409, 459009, 459137, 524801, 525057, 590849, 591361, 657409, 658433, 724993, 727041, 794625, 798721, 868353, 876545]),
                        M = [new Uint32Array([459008, 524368, 524304, 524568, 459024, 524400, 524336, 590016, 459016, 524384, 524320, 589984, 524288, 524416, 524352, 590048, 459012, 524376, 524312, 589968, 459028, 524408, 524344, 590032, 459020, 524392, 524328, 59e4, 524296, 524424, 524360, 590064, 459010, 524372, 524308, 524572, 459026, 524404, 524340, 590024, 459018, 524388, 524324, 589992, 524292, 524420, 524356, 590056, 459014, 524380, 524316, 589976, 459030, 524412, 524348, 590040, 459022, 524396, 524332, 590008, 524300, 524428, 524364, 590072, 459009, 524370, 524306, 524570, 459025, 524402, 524338, 590020, 459017, 524386, 524322, 589988, 524290, 524418, 524354, 590052, 459013, 524378, 524314, 589972, 459029, 524410, 524346, 590036, 459021, 524394, 524330, 590004, 524298, 524426, 524362, 590068, 459011, 524374, 524310, 524574, 459027, 524406, 524342, 590028, 459019, 524390, 524326, 589996, 524294, 524422, 524358, 590060, 459015, 524382, 524318, 589980, 459031, 524414, 524350, 590044, 459023, 524398, 524334, 590012, 524302, 524430, 524366, 590076, 459008, 524369, 524305, 524569, 459024, 524401, 524337, 590018, 459016, 524385, 524321, 589986, 524289, 524417, 524353, 590050, 459012, 524377, 524313, 589970, 459028, 524409, 524345, 590034, 459020, 524393, 524329, 590002, 524297, 524425, 524361, 590066, 459010, 524373, 524309, 524573, 459026, 524405, 524341, 590026, 459018, 524389, 524325, 589994, 524293, 524421, 524357, 590058, 459014, 524381, 524317, 589978, 459030, 524413, 524349, 590042, 459022, 524397, 524333, 590010, 524301, 524429, 524365, 590074, 459009, 524371, 524307, 524571, 459025, 524403, 524339, 590022, 459017, 524387, 524323, 589990, 524291, 524419, 524355, 590054, 459013, 524379, 524315, 589974, 459029, 524411, 524347, 590038, 459021, 524395, 524331, 590006, 524299, 524427, 524363, 590070, 459011, 524375, 524311, 524575, 459027, 524407, 524343, 590030, 459019, 524391, 524327, 589998, 524295, 524423, 524359, 590062, 459015, 524383, 524319, 589982, 459031, 524415, 524351, 590046, 459023, 524399, 524335, 590014, 524303, 524431, 524367, 590078, 459008, 524368, 524304, 524568, 459024, 524400, 524336, 590017, 459016, 524384, 524320, 589985, 524288, 524416, 524352, 590049, 459012, 524376, 524312, 589969, 459028, 524408, 524344, 590033, 459020, 524392, 524328, 590001, 524296, 524424, 524360, 590065, 459010, 524372, 524308, 524572, 459026, 524404, 524340, 590025, 459018, 524388, 524324, 589993, 524292, 524420, 524356, 590057, 459014, 524380, 524316, 589977, 459030, 524412, 524348, 590041, 459022, 524396, 524332, 590009, 524300, 524428, 524364, 590073, 459009, 524370, 524306, 524570, 459025, 524402, 524338, 590021, 459017, 524386, 524322, 589989, 524290, 524418, 524354, 590053, 459013, 524378, 524314, 589973, 459029, 524410, 524346, 590037, 459021, 524394, 524330, 590005, 524298, 524426, 524362, 590069, 459011, 524374, 524310, 524574, 459027, 524406, 524342, 590029, 459019, 524390, 524326, 589997, 524294, 524422, 524358, 590061, 459015, 524382, 524318, 589981, 459031, 524414, 524350, 590045, 459023, 524398, 524334, 590013, 524302, 524430, 524366, 590077, 459008, 524369, 524305, 524569, 459024, 524401, 524337, 590019, 459016, 524385, 524321, 589987, 524289, 524417, 524353, 590051, 459012, 524377, 524313, 589971, 459028, 524409, 524345, 590035, 459020, 524393, 524329, 590003, 524297, 524425, 524361, 590067, 459010, 524373, 524309, 524573, 459026, 524405, 524341, 590027, 459018, 524389, 524325, 589995, 524293, 524421, 524357, 590059, 459014, 524381, 524317, 589979, 459030, 524413, 524349, 590043, 459022, 524397, 524333, 590011, 524301, 524429, 524365, 590075, 459009, 524371, 524307, 524571, 459025, 524403, 524339, 590023, 459017, 524387, 524323, 589991, 524291, 524419, 524355, 590055, 459013, 524379, 524315, 589975, 459029, 524411, 524347, 590039, 459021, 524395, 524331, 590007, 524299, 524427, 524363, 590071, 459011, 524375, 524311, 524575, 459027, 524407, 524343, 590031, 459019, 524391, 524327, 589999, 524295, 524423, 524359, 590063, 459015, 524383, 524319, 589983, 459031, 524415, 524351, 590047, 459023, 524399, 524335, 590015, 524303, 524431, 524367, 590079]), 9],
                        P = [new Uint32Array([327680, 327696, 327688, 327704, 327684, 327700, 327692, 327708, 327682, 327698, 327690, 327706, 327686, 327702, 327694, 0, 327681, 327697, 327689, 327705, 327685, 327701, 327693, 327709, 327683, 327699, 327691, 327707, 327687, 327703, 327695, 0]), 5];
                    return (e.prototype = Object.create(a.prototype)).getBits = function (e) {
                        for (var t, n = this.codeSize, r = this.codeBuf, o = this.bytes, i = this.bytesPos; n < e;) void 0 === (t = o[i++]) && O("Bad encoding in flate stream"), r |= t << n, n += 8;
                        return t = r & (1 << e) - 1, this.codeBuf = r >> e, this.codeSize = n -= e, this.bytesPos = i, t
                    }, e.prototype.getCode = function (e) {
                        for (var t = e[0], n = e[1], r = this.codeSize, o = this.codeBuf, i = this.bytes, a = this.bytesPos; r < n;) {
                            var s;
                            void 0 === (s = i[a++]) && O("Bad encoding in flate stream"), o |= s << r, r += 8
                        }
                        var l = t[o & (1 << n) - 1],
                            c = l >> 16,
                            d = 65535 & l;
                        return (0 == r || r < c || 0 == c) && O("Bad encoding in flate stream"), this.codeBuf = o >> c, this.codeSize = r - c, this.bytesPos = a, d
                    }, e.prototype.generateHuffmanTable = function (e) {
                        for (var t = e.length, n = 0, r = 0; r < t; ++r) e[r] > n && (n = e[r]);
                        for (var o = 1 << n, i = new Uint32Array(o), a = 1, s = 0, l = 2; a <= n; ++a, s <<= 1, l <<= 1)
                            for (var c = 0; c < t; ++c)
                                if (e[c] == a) {
                                    var d = 0,
                                        u = s;
                                    for (r = 0; r < a; ++r) d = d << 1 | 1 & u, u >>= 1;
                                    for (r = d; r < o; r += l) i[r] = a << 16 | c;
                                    ++s
                                }
                        return [i, n]
                    }, e.prototype.readBlock = function () {
                        function e(e, t, n, r, o) {
                            for (var i = e.getBits(n) + r; 0 < i--;) t[l++] = o
                        }
                        var t = this.getBits(3);
                        if (1 & t && (this.eof = !0), 0 != (t >>= 1)) {
                            var n, r;
                            if (1 == t) n = M, r = P;
                            else if (2 == t) {
                                for (var o = this.getBits(5) + 257, i = this.getBits(5) + 1, a = this.getBits(4) + 4, s = Array(B.length), l = 0; l < a;) s[B[l++]] = this.getBits(3);
                                for (var c = this.generateHuffmanTable(s), d = 0, u = (l = 0, o + i), f = new Array(u); l < u;) {
                                    var p = this.getCode(c);
                                    16 == p ? e(this, f, 2, 3, d) : 17 == p ? e(this, f, 3, 3, d = 0) : 18 == p ? e(this, f, 7, 11, d = 0) : f[l++] = d = p
                                }
                                n = this.generateHuffmanTable(f.slice(0, o)), r = this.generateHuffmanTable(f.slice(o, u))
                            } else O("Unknown block type in flate stream");
                            for (var h = (T = this.buffer) ? T.length : 0, g = this.bufferLength; ;) {
                                var m = this.getCode(n);
                                if (m < 256) h <= g + 1 && (h = (T = this.ensureBuffer(g + 1)).length), T[g++] = m;
                                else {
                                    if (256 == m) return void (this.bufferLength = g);
                                    var v = (m = L[m -= 257]) >> 16;
                                    0 < v && (v = this.getBits(v));
                                    d = (65535 & m) + v;
                                    m = this.getCode(r), 0 < (v = (m = N[m]) >> 16) && (v = this.getBits(v));
                                    var b = (65535 & m) + v;
                                    h <= g + d && (h = (T = this.ensureBuffer(g + d)).length);
                                    for (var y = 0; y < d; ++y, ++g) T[g] = T[g - b]
                                }
                            }
                        } else {
                            var w, k = this.bytes,
                                x = this.bytesPos;
                            void 0 === (w = k[x++]) && O("Bad block header in flate stream");
                            var C = w;
                            void 0 === (w = k[x++]) && O("Bad block header in flate stream"), C |= w << 8, void 0 === (w = k[x++]) && O("Bad block header in flate stream");
                            var I = w;
                            void 0 === (w = k[x++]) && O("Bad block header in flate stream"), (I |= w << 8) != (65535 & ~C) && O("Bad uncompressed block length in flate stream"), this.codeBuf = 0, this.codeSize = 0;
                            var E = this.bufferLength,
                                T = this.ensureBuffer(E + C),
                                S = E + C;
                            this.bufferLength = S;
                            for (var A = E; A < S; ++A) {
                                if (void 0 === (w = k[x++])) {
                                    this.eof = !0;
                                    break
                                }
                                T[A] = w
                            }
                            this.bytesPos = x
                        }
                    }, e
                }
            }();

        function s() {
            this.pos = 0, this.bufferLength = 0, this.eof = !1, this.buffer = null
        }
        o = "undefined" != typeof self && self || "undefined" != typeof window && window || this, u = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", void 0 === o.btoa && (o.btoa = function (e) {
            var t, n, r, o, i, a = 0,
                s = 0,
                l = "",
                c = [];
            if (!e) return e;
            for (; t = (i = e.charCodeAt(a++) << 16 | e.charCodeAt(a++) << 8 | e.charCodeAt(a++)) >> 18 & 63, n = i >> 12 & 63, r = i >> 6 & 63, o = 63 & i, c[s++] = u.charAt(t) + u.charAt(n) + u.charAt(r) + u.charAt(o), a < e.length;);
            l = c.join("");
            var d = e.length % 3;
            return (d ? l.slice(0, d - 3) : l) + "===".slice(d || 3)
        }), void 0 === o.atob && (o.atob = function (e) {
            var t, n, r, o, i, a, s = 0,
                l = 0,
                c = [];
            if (!e) return e;
            for (e += ""; t = (a = u.indexOf(e.charAt(s++)) << 18 | u.indexOf(e.charAt(s++)) << 12 | (o = u.indexOf(e.charAt(s++))) << 6 | (i = u.indexOf(e.charAt(s++)))) >> 16 & 255, n = a >> 8 & 255, r = 255 & a, c[l++] = 64 == o ? String.fromCharCode(t) : 64 == i ? String.fromCharCode(t, n) : String.fromCharCode(t, n, r), s < e.length;);
            return c.join("")
        }), Array.prototype.map || (Array.prototype.map = function (e) {
            if (null == this || "function" != typeof e) throw new TypeError;
            for (var t = Object(this), n = t.length >>> 0, r = new Array(n), o = 1 < arguments.length ? arguments[1] : void 0, i = 0; i < n; i++) i in t && (r[i] = e.call(o, t[i], i, t));
            return r
        }), Array.isArray || (Array.isArray = function (e) {
            return "[object Array]" === Object.prototype.toString.call(e)
        }), Array.prototype.forEach || (Array.prototype.forEach = function (e, t) {
            "use strict";
            if (null == this || "function" != typeof e) throw new TypeError;
            for (var n = Object(this), r = n.length >>> 0, o = 0; o < r; o++) o in n && e.call(t, n[o], o, n)
        }), Object.keys || (Object.keys = function () {
            "use strict";
            var o = Object.prototype.hasOwnProperty,
                i = !{
                    toString: null
                }.propertyIsEnumerable("toString"),
                a = ["toString", "toLocaleString", "valueOf", "hasOwnProperty", "isPrototypeOf", "propertyIsEnumerable", "constructor"],
                s = a.length;
            return function (e) {
                if ("object" != typeof e && ("function" != typeof e || null === e)) throw new TypeError;
                var t, n, r = [];
                for (t in e) o.call(e, t) && r.push(t);
                if (i)
                    for (n = 0; n < s; n++) o.call(e, a[n]) && r.push(a[n]);
                return r
            }
        }()), String.prototype.trim || (String.prototype.trim = function () {
            return this.replace(/^\s+|\s+$/g, "")
        }), String.prototype.trimLeft || (String.prototype.trimLeft = function () {
            return this.replace(/^\s+/g, "")
        }), String.prototype.trimRight || (String.prototype.trimRight = function () {
            return this.replace(/\s+$/g, "")
        })
    }(), window.Modernizr = function (e, n, i) {
        function r(e) {
            h.cssText = e
        }

        function a(e, t) {
            return typeof e === t
        }

        function o(e, t) {
            return !!~("" + e).indexOf(t)
        }

        function s(e, t) {
            for (var n in e) {
                var r = e[n];
                if (!o(r, "-") && h[r] !== i) return "pfx" != t || r
            }
            return !1
        }

        function t(e, t, n) {
            var r = e.charAt(0).toUpperCase() + e.slice(1),
                o = (e + " " + v.join(r + " ") + r).split(" ");
            return a(t, "string") || a(t, "undefined") ? s(o, t) : function (e, t, n) {
                for (var r in e) {
                    var o = t[e[r]];
                    if (o !== i) return !1 === n ? e[r] : a(o, "function") ? o.bind(n || t) : o
                }
                return !1
            }(o = (e + " " + b.join(r + " ") + r).split(" "), t, n)
        }
        var l, c, d = {},
            u = n.documentElement,
            f = "modernizr",
            p = n.createElement(f),
            h = p.style,
            g = " -webkit- -moz- -o- -ms- ".split(" "),
            m = "Webkit Moz O ms",
            v = m.split(" "),
            b = m.toLowerCase().split(" "),
            y = {},
            w = [],
            k = w.slice,
            x = {}.hasOwnProperty;
        for (var C in c = a(x, "undefined") || a(x.call, "undefined") ? function (e, t) {
            return t in e && a(e.constructor.prototype[t], "undefined")
        } : function (e, t) {
            return x.call(e, t)
        }, Function.prototype.bind || (Function.prototype.bind = function (r) {
            var o = this;
            if ("function" != typeof o) throw new TypeError;
            var i = k.call(arguments, 1),
                a = function () {
                    if (this instanceof a) {
                        function e() { }
                        e.prototype = o.prototype;
                        var t = new e,
                            n = o.apply(t, i.concat(k.call(arguments)));
                        return Object(n) === n ? n : t
                    }
                    return o.apply(r, i.concat(k.call(arguments)))
                };
            return a
        }), y.canvas = function () {
            var e = n.createElement("canvas");
            return !!e.getContext && !!e.getContext("2d")
        }, y.rgba = function () {
            return r("background-color:rgba(150,255,150,.5)"), o(h.backgroundColor, "rgba")
        }, y.backgroundsize = function () {
            return t("backgroundSize")
        }, y.borderradius = function () {
            return t("borderRadius")
        }, y.opacity = function () {
            return function (e, t) {
                r(g.join(e + ";") + (t || ""))
            }("opacity:.55"), /^0.55$/.test(h.opacity)
        }, y.video = function () {
            var e = n.createElement("video"),
                t = !1;
            try {
                (t = !!e.canPlayType) && ((t = new Boolean(t)).ogg = e.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, ""), t.h264 = e.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, ""), t.webm = e.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, ""))
            } catch (e) { }
            return t
        }, y.audio = function () {
            var e = n.createElement("audio"),
                t = !1;
            try {
                (t = !!e.canPlayType) && ((t = new Boolean(t)).ogg = e.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""), t.mp3 = e.canPlayType("audio/mpeg;").replace(/^no$/, ""), t.wav = e.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""), t.m4a = (e.canPlayType("audio/x-m4a;") || e.canPlayType("audio/aac;")).replace(/^no$/, ""))
            } catch (e) { }
            return t
        }, y.localstorage = function () {
            try {
                return localStorage.setItem(f, f), localStorage.removeItem(f), !0
            } catch (e) {
                return !1
            }
        }, y) c(y, C) && (l = C.toLowerCase(), d[l] = y[C](), w.push((d[l] ? "" : "no-") + l));
        return d.addTest = function (e, t) {
            if ("object" == typeof e)
                for (var n in e) c(e, n) && d.addTest(n, e[n]);
            else {
                if (e = e.toLowerCase(), d[e] !== i) return d;
                t = "function" == typeof t ? t() : t, u.className += " " + (t ? "" : "no-") + e, d[e] = t
            }
            return d
        }, r(""), p = null,
            function (e, a) {
                function s() {
                    var e = h.elements;
                    return "string" == typeof e ? e.split(" ") : e
                }

                function l(e) {
                    var t = p[e[u]];
                    return t || (t = {}, f++ , e[u] = f, p[f] = t), t
                }

                function r(e, t, n) {
                    return t = t || a, c ? t.createElement(e) : !(r = (n = n || l(t)).cache[e] ? n.cache[e].cloneNode() : d.test(e) ? (n.cache[e] = n.createElem(e)).cloneNode() : n.createElem(e)).canHaveChildren || i.test(e) || r.tagUrn ? r : n.frag.appendChild(r);
                    var r
                }

                function t(e) {
                    var t = l(e = e || a);
                    return !h.shivCSS || n || t.hasCSS || (t.hasCSS = !! function (e, t) {
                        var n = e.createElement("p"),
                            r = e.getElementsByTagName("head")[0] || e.documentElement;
                        return n.innerHTML = "x<style>" + t + "</style>", r.insertBefore(n.lastChild, r.firstChild)
                    }(e, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")), c || function (t, n) {
                        n.cache || (n.cache = {}, n.createElem = t.createElement, n.createFrag = t.createDocumentFragment, n.frag = n.createFrag()), t.createElement = function (e) {
                            return h.shivMethods ? r(e, t, n) : n.createElem(e)
                        }, t.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + s().join().replace(/[\w\-]+/g, function (e) {
                            return n.createElem(e), n.frag.createElement(e), 'c("' + e + '")'
                        }) + ");return n}")(h, n.frag)
                    }(e, t), e
                }
                var n, c, o = e.html5 || {},
                    i = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
                    d = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
                    u = "_html5shiv",
                    f = 0,
                    p = {};
                ! function () {
                    try {
                        var e = a.createElement("a");
                        e.innerHTML = "<xyz></xyz>", n = "hidden" in e, c = 1 == e.childNodes.length || function () {
                            a.createElement("a");
                            var e = a.createDocumentFragment();
                            return void 0 === e.cloneNode || void 0 === e.createDocumentFragment || void 0 === e.createElement
                        }()
                    } catch (e) {
                        c = n = !0
                    }
                }();
                var h = {
                    elements: o.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
                    version: "3.7.0",
                    shivCSS: !1 !== o.shivCSS,
                    supportsUnknownElements: c,
                    shivMethods: !1 !== o.shivMethods,
                    type: "default",
                    shivDocument: t,
                    createElement: r,
                    createDocumentFragment: function (e, t) {
                        if (e = e || a, c) return e.createDocumentFragment();
                        for (var n = (t = t || l(e)).frag.cloneNode(), r = 0, o = s(), i = o.length; r < i; r++) n.createElement(o[r]);
                        return n
                    }
                };
                e.html5 = h, t(a)
            }(this, n), d._version = "2.8.3", d._prefixes = g, d._domPrefixes = b, d._cssomPrefixes = v, d.testProp = function (e) {
                return s([e])
            }, d.testAllProps = t, u.className = u.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + " js " + w.join(" "), d
    }(0, this.document),
    function (e, f) {
        function u(e) {
            return "[object Function]" == o.call(e)
        }

        function p(e) {
            return "string" == typeof e
        }

        function h() { }

        function g(e) {
            return !e || "loaded" == e || "complete" == e || "uninitialized" == e
        }

        function m() {
            var e = w.shift();
            k = 1, e ? e.t ? b(function () {
                ("c" == e.t ? v.injectCss : v.injectJs)(e.s, 0, e.a, e.x, e.e, 1)
            }, 0) : (e(), m()) : k = 0
        }

        function t(e, t, n, r, o) {
            return k = 0, t = t || "j", p(e) ? function (n, r, e, t, o, i, a) {
                function s(e) {
                    if (!c && g(l.readyState) && (u.r = c = 1, k || m(), l.onload = l.onreadystatechange = null, e))
                        for (var t in "img" != n && b(function () {
                            C.removeChild(l)
                        }, 50), E[r]) E[r].hasOwnProperty(t) && E[r][t].onload()
                }
                a = a || v.errorTimeout;
                var l = f.createElement(n),
                    c = 0,
                    d = 0,
                    u = {
                        t: e,
                        s: r,
                        e: o,
                        a: i,
                        x: a
                    };
                1 === E[r] && (d = 1, E[r] = []), "object" == n ? l.data = r : (l.src = r, l.type = n), l.width = l.height = "0", l.onerror = l.onload = l.onreadystatechange = function () {
                    s.call(this, d)
                }, w.splice(t, 0, u), "img" != n && (d || 2 === E[r] ? (C.insertBefore(l, x ? null : y), b(s, a)) : E[r].push(l))
            }("c" == t ? l : a, e, t, this.i++, n, r, o) : (w.splice(this.i++, 0, e), 1 == w.length && m()), this
        }

        function s() {
            var e = v;
            return e.loader = {
                load: t,
                i: 0
            }, e
        }
        var n, v, r = f.documentElement,
            b = e.setTimeout,
            y = f.getElementsByTagName("script")[0],
            o = {}.toString,
            w = [],
            k = 0,
            i = "MozAppearance" in r.style,
            x = i && !!f.createRange().compareNode,
            C = x ? r : y.parentNode,
            a = (r = e.opera && "[object Opera]" == o.call(e.opera), r = !!f.attachEvent && !r, i ? "object" : r ? "script" : "img"),
            l = r ? "script" : a,
            c = Array.isArray || function (e) {
                return "[object Array]" == o.call(e)
            },
            I = [],
            E = {},
            T = {
                timeout: function (e, t) {
                    return t.length && (e.timeout = t[0]), e
                }
            };
        (v = function (e) {
            function d(e, t, n, r, o) {
                var i = function (e) {
                    e = e.split("!");
                    var t, n, r, o = I.length,
                        i = e.pop(),
                        a = e.length;
                    for (i = {
                        url: i,
                        origUrl: i,
                        prefixes: e
                    }, n = 0; n < a; n++) r = e[n].split("="), (t = T[r.shift()]) && (i = t(i, r));
                    for (n = 0; n < o; n++) i = I[n](i);
                    return i
                }(e),
                    a = i.autoCallback;
                i.url.split(".").pop().split("?").shift(), i.bypass || (t = t && (u(t) ? t : t[e] || t[r] || t[e.split("/").pop().split("?")[0]]), i.instead ? i.instead(e, t, n, r, o) : (E[i.url] ? i.noexec = !0 : E[i.url] = 1, n.load(i.url, i.forceCSS || !i.forceJS && "css" == i.url.split(".").pop().split("?").shift() ? "c" : void 0, i.noexec, i.attrs, i.timeout), (u(t) || u(a)) && n.load(function () {
                    s(), t && t(i.origUrl, o, r), a && a(i.origUrl, o, r), E[i.url] = 2
                })))
            }

            function t(e, t) {
                function n(n, e) {
                    if (n) {
                        if (p(n)) e || (s = function () {
                            var e = [].slice.call(arguments);
                            l.apply(this, e), c()
                        }), d(n, s, t, 0, i);
                        else if (Object(n) === n)
                            for (o in r = function () {
                                var e, t = 0;
                                for (e in n) n.hasOwnProperty(e) && t++;
                                return t
                            }(), n) n.hasOwnProperty(o) && (e || --r || (u(s) ? s = function () {
                                var e = [].slice.call(arguments);
                                l.apply(this, e), c()
                            } : s[o] = function (t) {
                                return function () {
                                    var e = [].slice.call(arguments);
                                    t && t.apply(this, e), c()
                                }
                            }(l[o])), d(n[o], s, t, o, i))
                    } else e || c()
                }
                var r, o, i = !!e.test,
                    a = e.load || e.both,
                    s = e.callback || h,
                    l = s,
                    c = e.complete || h;
                n(i ? e.yep : e.nope, !!a), a && n(a)
            }
            var n, r, o = this.yepnope.loader;
            if (p(e)) d(e, 0, o, 0);
            else if (c(e))
                for (n = 0; n < e.length; n++) p(r = e[n]) ? d(r, 0, o, 0) : c(r) ? v(r) : Object(r) === r && t(r, o);
            else Object(e) === e && t(e, o)
        }).addPrefix = function (e, t) {
            T[e] = t
        }, v.addFilter = function (e) {
            I.push(e)
        }, v.errorTimeout = 1e4, null == f.readyState && f.addEventListener && (f.readyState = "loading", f.addEventListener("DOMContentLoaded", n = function () {
            f.removeEventListener("DOMContentLoaded", n, 0), f.readyState = "complete"
        }, 0)), e.yepnope = s(), e.yepnope.executeStack = m, e.yepnope.injectJs = function (e, t, n, r, o, i) {
            var a, s, l = f.createElement("script");
            r = r || v.errorTimeout;
            for (s in l.src = e, n) l.setAttribute(s, n[s]);
            t = i ? m : t || h, l.onreadystatechange = l.onload = function () {
                !a && g(l.readyState) && (a = 1, t(), l.onload = l.onreadystatechange = null)
            }, b(function () {
                a || t(a = 1)
            }, r), o ? l.onload() : y.parentNode.insertBefore(l, y)
        }, e.yepnope.injectCss = function (e, t, n, r, o, i) {
            var a;
            r = f.createElement("link"), t = i ? m : t || h;
            for (a in r.href = e, r.rel = "stylesheet", r.type = "text/css", n) r.setAttribute(a, n[a]);
            o || (y.parentNode.insertBefore(r, y), b(t, 0))
        }
    }(this, document), Modernizr.load = function () {
        yepnope.apply(window, [].slice.call(arguments, 0))
    },
    function (e) {
        "use strict";
        var t = e.prototype,
            i = t.parseFromString;
        try {
            if ((new e).parseFromString("", "text/html")) return
        } catch (e) { }
        t.parseFromString = function (e, t) {
            if (/^\s*text\/html\s*(?:;|$)/i.test(t)) {
                var n, r = document.implementation.createHTMLDocument(""),
                    o = r.documentElement;
                return o.innerHTML = e, n = o.firstElementChild, 1 === o.childElementCount && "html" === n.localName.toLowerCase() && r.replaceChild(n, o), r
            }
            return i.apply(this, arguments)
        }
    }(DOMParser), ph = {
        removeHash: function (e) {
            return -1 == e.indexOf("#") ? e : e.substring(0, e.lastIndexOf("#"))
        },
        getHash: function (e) {
            if (0 <= e.indexOf("#")) return e.substr(e.lastIndexOf("#") + 1)
        },
        normalize: function (e) {
            var t = ph.normalizedArray(e).join("/");
            return arguments.length && "/" == e.substr(0, 1) && (t = "file:///" + t), t
        },
        normalizedArray: function (e) {
            for (var t = (e = e.replace(/\\/g, "/")).split("/"), n = t.length; n--;) t[n] ? ".." == t[n] && 0 < n && ".." != t[n - 1] && (t.splice(n, 1), t.splice(n - 1, 1)) : t.splice(n, 1);
            return t[0].match(/http[s]?:$/g) && (t[0] += "//" + t[1], t.splice(1, 1)), "file:" == t[0] && (t[0] += "///" + t[1], t.splice(1, 1)), t
        },
        basename: function (e) {
            if (-1 == e.indexOf("/")) return e;
            var t = ph.normalizedArray(e);
            return t[t.length - 1]
        },
        removeFilename: function (e) {
            var t = ph.normalizedArray(e);
            return -1 == t[t.length - 1].indexOf(".") ? e : (t.splice(t.length - 1, 1), t.join("/"))
        },
        removeExtension: function (e) {
            if (-1 == e.indexOf(".")) return e;
            var t = e.split(".");
            return t.splice(t.length - 1, 1), t.join(".")
        },
        getExtension: function (e) {
            if (-1 == e.indexOf(".")) return "";
            var t = e.split(".");
            return t[t.length - 1]
        },
        dirname: function (e) {
            if (-1 == e.indexOf("/")) return e;
            var t = ph.normalizedArray(e);
            return 0 <= t[t.length - 1].indexOf(".") && t.splice(t.length - 1, 1), 1 == t.length ? t[0] : t[t.length - 1]
        },
        relative: function (e, t) {
            var n, r, o = ph.normalizedArray(e),
                i = ph.normalizedArray(t),
                a = new Array;
            for (0 <= o[o.length - 1].indexOf(".") && o.splice(o.length - 1, 1), n = 0; n < o.length && o[n] == i[n]; n++);
            for (r = n; r < o.length; r++) a.push("..");
            for (r = n; r < i.length; r++) a.push(i[r]);
            return a.join("/")
        },
        join: function () {
            for (var e = new Array, t = 0; t < arguments.length; t++) {
                var n = ph.normalizedArray(arguments[t]);
                t < arguments.length - 1 && !isAbsoluteURLPath(n[n.length - 1]) && 0 <= n[n.length - 1].indexOf(".") && n.splice(n.length - 1, 1), e = e.concat(n)
            }
            var r = ph.normalize(e.join("/"));
            return "/" == arguments[0].substr(0, 1) && (r = desktop ? "file:////" + r : "/" + r), r
        },
        parent: function (e) {
            e = ph.removeFilename(e);
            var t = ph.normalizedArray(e);
            t.splice(t.length - 1, 1);
            var n = t.join("/");
            return arguments.length && "/" == e.substr(0, 1) && (n = "/" + n), n
        }
    };
var LoggerWorker, startDate = Date.now();
try {
    (loggerWorker = new Worker(rootDir + "js/app/workers/logger.js")).addEventListener("error", function () {
        loggerWorker = null
    }, !1)
} catch (e) { }
var kLog = function (e) {
    if (debug) {
        null == e && (e = "null"), debugDuration && (e = Date.now() - startDate + " " + e);
        try {
            if (loggerWorker) return void loggerWorker.postMessage({
                func: "log",
                params: arguments
            })
        } catch (e) { }
        console.log.apply(this, arguments)
    }
},
    log = kLog;

function guid() {
    function e() {
        return Math.floor(65536 * (1 + Math.random())).toString(16).substring(1)
    }
    return e() + e() + "-" + e() + "-" + e() + "-" + e() + "-" + e() + e() + e()
}

function randStr(e, t) {
    return (t = t || "s") + Math.random().toString(36).substring(5).substr(0, e)
}

function getDateTime() {
    var e = new Date,
        t = e.getFullYear(),
        n = e.getMonth() + 1,
        r = e.getDate(),
        o = e.getHours(),
        i = e.getMinutes(),
        a = e.getSeconds();
    if (1 == n.toString().length) n = "0" + n;
    if (1 == r.toString().length) r = "0" + r;
    if (1 == o.toString().length) o = "0" + o;
    if (1 == i.toString().length) i = "0" + i;
    if (1 == a.toString().length) a = "0" + a;
    return t + "/" + n + "/" + r + " " + o + ":" + i + ":" + a
}

function getFormData(e) {
    e = e || {};
    var t = new FormData;
    for (var n in e) e[n] && t.append(n, e[n]);
    return t
}

function newBlob(t, n) {
    try {
        return new Blob([t], {
            type: n
        })
    } catch (e) {
        if (window.BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder || window.MSBlobBuilder, "TypeError" == e.name && window.BlobBuilder) {
            var r = new BlobBuilder;
            return r.append(t.buffer), r.getBlob(n)
        }
        if ("InvalidStateError" == e.name) return new Blob([t.buffer], {
            type: n
        })
    }
}
var currentIndex, currentVIndex, Base64Binary = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    decodeArrayBuffer: function (e) {
        var t = e.length / 4 * 3,
            n = new ArrayBuffer(t);
        return this.decode(e, n), n
    },
    decode: function (e, t) {
        var n, r, o, i, a, s, l, c = this._keyStr.indexOf(e.charAt(e.length - 1)),
            d = this._keyStr.indexOf(e.charAt(e.length - 2)),
            u = e.length / 4 * 3;
        64 == c && u-- , 64 == d && u--;
        var f = 0,
            p = 0;
        for (n = t ? new Uint8Array(t) : new Uint8Array(u), e = e.replace(/[^A-Za-z0-9\+\/\=]/g, ""), f = 0; f < u; f += 3) r = this._keyStr.indexOf(e.charAt(p++)) << 2 | (a = this._keyStr.indexOf(e.charAt(p++))) >> 4, o = (15 & a) << 4 | (s = this._keyStr.indexOf(e.charAt(p++))) >> 2, i = (3 & s) << 6 | (l = this._keyStr.indexOf(e.charAt(p++))), n[f] = r, 64 != s && (n[f + 1] = o), 64 != l && (n[f + 2] = i);
        return n
    }
};

function clone(e) {
    if (null == e || "object" != typeof e) return e;
    var t = e.constructor();
    for (var n in e) e.hasOwnProperty(n) && (t[n] = clone(e[n]));
    return t
}

function rgb2hex(e) {
    if (/^#[0-9A-F]{6}$/i.test(e)) return e;

    function t(e) {
        return ("0" + parseInt(e).toString(16)).slice(-2)
    }
    return (e = e.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/)) ? "#" + t(e[1]) + t(e[2]) + t(e[3]) : void 0
}

function timeOut(e, t) {
    var n = setInterval(function () {
        clearInterval(n), e()
    }, t);
    return n
}

function wrapNode(e, t) {
    var n = document.createRange();
    n.selectNode(e), n.surroundContents(document.createElement(t))
}

function wrapNodeFromTo(e, t, n, r) {
    var o = document.createRange();
    o.setStart(e, n), o.setEnd(e, r), o.surroundContents(document.createElement(t))
}

function XMLEscape(e, t) {
    for (var n = "", r = 0; r < e.length; r++) {
        var o = e.charAt(r);
        n += "&" == o ? "&amp;" : "'" == o ? t ? "&apos;" : "&#39;" : o
    }
    return n
}

function replaceHTML(e, t) {
    var n = "string" == typeof e ? document.getElementById(e) : e,
        r = n.cloneNode(!1);
    return r.innerHTML = t, n.parentNode.replaceChild(r, n), r
}

function kTemplate(e) {
    return mobile ? (config.overflowScroll ? "templates/mobileOverflow/" : "templates/mobile/") + e : "templates/web/" + e
}

function arrayContainsProperty(e, t, n) {
    for (var r = 0; r < e.length; r++)
        if (e[r][t] == n) return !0;
    return !1
}

function cacheBust(e) {
    if (desktop && (0 == e.indexOf("/") || 0 == e.indexOf("file://") || 0 == e.indexOf("chrome-extension://"))) return e;
    var t = "?";
    return 0 <= e.indexOf("?") && (t = "&"), e + t + Math.floor(9999 * Math.random())
}

function addPrefixedEvent(e, t, n) {
    for (var r = ["webkit", "moz", "MS", "o", ""], o = 0; o < r.length; o++) r[o] || (t = t.toLowerCase()), e.addEventListener(r[o] + t, n, !1)
}

function removePrefixedEvent(e, t, n) {
    for (var r = ["webkit", "moz", "MS", "o", ""], o = 0; o < r.length; o++) r[o] || (t = t.toLowerCase()), e.removeEventListener(r[o] + t, n, !1)
}

function redrawForChrome(e) {
    -1 != navigator.userAgent.toLowerCase().indexOf("chrome") && (e.style.display = "inline-block", e.offsetHeight, e.style.display = "")
}

function pauseVideoAndAudio(e) {
    for (var t = e.getElementsByTagName("audio"), n = t.length; n--;) try {
        t[n].pause()
    } catch (e) { }
    var r = e.getElementsByTagName("video");
    for (n = r.length; n--;) try {
        r[n].pause()
    } catch (e) { }
}

function isEmpty(e) {
    for (var t in e)
        if (e.hasOwnProperty(t)) return !1;
    return JSON.stringify(e) === JSON.stringify({})
}

function asyncCheck(e, t, n, r) {
    return e ? r() : (n.push(r), t.apply(this, n))
}

function autoprefixTransform(e, t) {
    t = t || "", e.style.transform = e.style.msTransform = e.style.webkitTransform = e.style.mozTransform = t
}

function deleteTransform(e) {
    e && (e.style.transform = e.style.msTransform = e.style.webkitTransform = e.style.mozTransform = null)
}

function deleteTransformOrigin(e) {
    e && (e.style.transformOrigin = e.style.msTransformOrigin = e.style.webkitTransformOrigin = e.style.mozTransformOrigin = null)
}

function getChildIndex(e) {
    for (var t = 0; null != (e = e.previousSibling);) t++;
    return t
}

function getFilteredChildIndex(e) {
    for (var t = 0; e = e.previousSibling;) "#text" != e.nodeName && 8 != e.nodeType && t++;
    return t
}

function autoprefixTransformOrigin(e, t) {
    t = t || "", e.style.transformOrigin = e.style.msTransformOrigin = e.style.webkitTransformOrigin = e.style.mozTransformOrigin = t
}

function detectIE() {
    var e = window.navigator.userAgent,
        t = e.indexOf("MSIE ");
    if (0 < t) return parseInt(e.substring(t + 5, e.indexOf(".", t)), 10);
    if (0 < e.indexOf("Trident/")) {
        var n = e.indexOf("rv:");
        return parseInt(e.substring(n + 3, e.indexOf(".", n)), 10)
    }
    var r = e.indexOf("Edge/");
    return 0 < r && parseInt(e.substring(r + 5, e.indexOf(".", r)), 10)
}

function getImgDataSrc(e, t) {
    var n = new XMLHttpRequest;
    n.open("GET", e), n.responseType = "blob", n.onload = function () {
        var e = new FileReader;
        e.onloadend = function () {
            t && t(e.result)
        }, e.readAsDataURL(n.response)
    }, n.send()
}

function isAbsoluteURLPath(e) {
    return 0 == e.trim().indexOf("http://") || (0 == e.trim().indexOf("https://") || (0 == e.trim().indexOf("//") || (0 == e.trim().indexOf("data:") || void 0)))
}

function isAbsoluteURLAndNotDataPath(e) {
    return 0 == e.trim().indexOf("http://") || (0 == e.trim().indexOf("https://") || (0 == e.trim().indexOf("//") || void 0))
}

function isKotobeeHosted(e) {
    var t = (e = e || window.location.href).split("://");
    if (!(t.length <= 1)) {
        for (var n = t[1], r = ["kotobee.com"], o = 0; o < r.length; o++) {
            if (0 <= n.indexOf(r[o] + "/ebook")) return !0;
            if (0 <= n.indexOf(r[o] + "/library")) return !0;
            if (0 <= n.indexOf(r[o] + "/lti")) return !0
        }
        if (0 <= e.indexOf(".kotobee.com")) {
            var i = e.replace(/https?:\/\/(.*?\.)kotobee\.com.*/g, "$1");
            if (i) {
                var a = i.replace(/(.*?)\./g, "$1");
                return "www" != a && ("dev" != a && ("test" != a && "qa" != a))
            }
        }
    }
}

function decodeHTMLEntities(e) {
    for (var t = [
        ["amp", "&"],
        ["apos", "'"],
        ["#x27", "'"],
        ["#x2F", "/"],
        ["#39", "'"],
        ["#47", "/"],
        ["lt", "<"],
        ["gt", ">"],
        ["nbsp", " "],
        ["quot", '"']
    ], n = 0, r = t.length; n < r; ++n) e = e.replace(new RegExp("&" + t[n][0] + ";", "g"), t[n][1]);
    return e
}

function addFormHiddenInput(e, t, n) {
    var r = document.createElement("input");
    r.setAttribute("name", e), r.setAttribute("type", "hidden"), r.setAttribute("value", t), n.appendChild(r)
}

function getElemMargin(e, t) {
    return getElemDim(e, "margin", t)
}

function getElemPadding(e, t) {
    return getElemDim(e, "padding", t)
}

function getElemDim(e, t, n) {
    var r = {},
        o = (n = n || {}).computed ? getComputedStyle(e) : e.style;
    return r.left = o[t + "Left"].split("px")[0], r.right = o[t + "Right"].split("px")[0], "auto" == r.left ? r.x = "auto" : "auto" == r.left ? r.x = "auto" : r.x = Math.round(r.left) + Math.round(r.right), r.top = o[t + "Top"].split("px")[0], r.bottom = o[t + "Bottom"].split("px")[0], "auto" == r.top ? r.y = "auto" : "auto" == r.bottom ? r.y = "auto" : r.y = Math.round(r.top) + Math.round(r.bottom), r
}

function makeArrayUnique(e) {
    for (var t = 0; t < e.length; t++)
        for (var n = e[t], r = e.length; r--;) t != r && e[r].length + "  " + n.length && e[r] == n && e.splice(r, 1)
}

function cloneNodeAttributes(e) {
    var t = document.createElement(e.nodeName);
    return copyAttributes(e, t), t
}

function copyAttributes(e, t) {
    for (var n = 0; n < e.attributes.length; n++) t.setAttribute(e.attributes[n].name, e.attributes[n].value)
}

function getBoundingBox(e) {
    if (3 != e.nodeType) return e.getBoundingClientRect();
    var t = document.createRange();
    t.selectNode(e);
    var n = t.getBoundingClientRect();
    return t.detach(), n
}

function parents(e, t) {
    t = t || {};
    for (var n = []; e && (!t.stopAtId || e.id != t.stopAtId); e = e.parentNode) t.filterByClass && !e.classList.contains(t.filterByClass) || n.unshift(e);
    return n
}

function getCurrentTime() {
    return Math.floor(Date.now() / 1e3)
}

function getQueryStringValue(e) {
    var t = window.location.href.split("?");
    if (!(t.length <= 1))
        for (var n = t[1].split("&"), r = 0; r < n.length; r++) {
            var o = n[r].split("=");
            if (o[0] == e) return o[1]
        }
}

function copyObjToObj(e, t, n) {
    for (var r in n = n || {}, e) n.onlyIfNotExists && null != t[r] || (t[r] = e[r])
}

function traverseToBottom(e, t) {
    for (; e && e.children.length;) {
        if (t && e.id == t) return e;
        e = e.children[0]
    }
    return e
}

function traverseToTop(e, t) {
    for (; e && e.parentNode;) {
        if (t && e.id == t) return e;
        e = e.parentNode
    }
    return !e || e.parentNode ? null : e.id != t ? null : e
}

function getDomainFromURL(e) {
    var t = new URL(e);
    return t.protocol + "//" + t.hostname
}

function hasParent(e, t, n) {
    n = n || 3;
    for (var r = 0; r < n; r++) {
        if ("body" == (e = e.parentNode).nodeName.toLowerCase()) return;
        if (hasClass(e, t)) return !0
    }
}

function hasParentNode(e, t, n) {
    n = n || 3;
    for (var r = 0; r < n; r++) {
        var o = (e = e.parentNode).nodeName.toLowerCase();
        if ("body" == o) return;
        if (o == t) return !0
    }
}

function XORCipher() {
    var u = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    return {
        encode: function (e, t) {
            return function (e) {
                var t, n, r, o, i, a, s, l, c = 0,
                    d = "";
                if (!e) return e;
                for (; t = e[c++], n = e[c++], r = e[c++], o = (s = t << 16 | n << 8 | r) >> 12 & 63, i = s >> 6 & 63, a = 63 & s, d += u.charAt(s >> 18 & 63) + u.charAt(o) + u.charAt(i) + u.charAt(a), c < e.length;);
                return ((l = e.length % 3) ? d.slice(0, l - 3) : d) + "===".slice(l || 3)
            }(t = function (n, e) {
                return o(e, function (e, t) {
                    return e.charCodeAt(0) ^ r(n, t)
                })
            }(e, t))
        },
        decode: function (e, t) {
            return function (n, e) {
                return o(e, function (e, t) {
                    return String.fromCharCode(e ^ r(n, t))
                }).join("")
            }(e, t = function (e) {
                var t, n, r, o, i, a, s, l, c = 0,
                    d = [];
                if (!e) return e;
                e += "";
                for (; o = u.indexOf(e.charAt(c++)), i = u.indexOf(e.charAt(c++)), a = u.indexOf(e.charAt(c++)), s = u.indexOf(e.charAt(c++)), t = (l = o << 18 | i << 12 | a << 6 | s) >> 16 & 255, n = l >> 8 & 255, r = 255 & l, d.push(t), 64 !== a && (d.push(n), 64 !== s && d.push(r)), c < e.length;);
                return d
            }(t))
        }
    };

    function r(e, t) {
        return e.charCodeAt(Math.floor(t % e.length))
    }

    function o(e, t) {
        for (var n = [], r = 0; r < e.length; r++) n[r] = t(e[r], r);
        return n
    }
}

function isFunction(e) {
    return e && "[object Function]" === {}.toString.call(e)
}
var preview, previewParams, app, native, bookPath, noFlash, desktop, chromeApp, os, uuid, loggerWorker, parser = new DOMParser,
    mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
    mobileDebug = !1,
    ios = /iPhone|iPad|iPod/i.test(navigator.userAgent),
    readerV = 1.4,
    debug = !1,
    isReaderApp = !0,
    kotobee = {},
    partitionEnabled = "Android" == mobile && config.kotobee.segregate,
    isKotobee = !0,
    debugDuration = !1,
    overflowScroll = !mobile | config.overflowScroll;

function stopAllAudio() {
    for (var e = document.getElementsByTagName("audio"), t = 0; t < e.length; t++) e[t].pause(), e[t].src = "";
    try {
        document.bgAudio && (document.bgAudio.pause(), document.bgAudio.src = "")
    } catch (e) { }
}

function applyPreview(e) {
    var t = angular.element(document.body).scope();
    t.config = config = e, t.$apply(), angular.element(document.body).scope().injectUserCSS(e)
}

function previewPage(e, t) {
    var n = angular.element(document.body).scope();
    n.previewPage && n.previewPage(e, t)
}

function getCookie(e) {
    var t = ("; " + document.cookie).split("; " + e + "=");
    if (2 == t.length) return t.pop().split(";").shift()
}

function getVar(e) {
    return Modernizr.localstorage ? localStorage[e] : getCookie("continueWithoutHTML5")
}

function setVar(e, t) {
    Modernizr.localstorage ? localStorage[e] = t : document.cookie = e + "=" + t
}

function getGlobal() {
    return angular.element(document.body).scope()
}

function closePrintIframe() {
    var e = document.getElementById("printContainer");
    e && e.parentNode.removeChild(e)
}

function putTest(e) { }

function continueLocally() {
    setVar("continueLocally", "true"), location.reload()
}

function continueWithoutHTML5() {
    setVar("continueWithoutHTML5", "true"), location.reload()
}
"undefined" == typeof rootDir && (rootDir = ""), null == config.mediaTab && (config.mediaTab = !0), null == config.settingsTab && (config.settingsTab = !0), null == config.notebookTab && (config.notebookTab = !0), null == config.textSizeControls && (config.textSizeControls = !0), null == config.chaptersTab && (config.chaptersTab = !0), null == config.showCategories && (config.showCategories = !0), null == config.styleControls && (config.styleControls = !1),
    function () {
        function e() {
            for (var e, t = document.getElementsByTagName("head")[0], n = t.innerHTML, r = [], o = document.getElementsByTagName("script"), i = 0; i < o.length; i++)
                if (o[i].getAttribute("src") && 0 <= o[i].getAttribute("src").indexOf("lib/ionic/release/js")) {
                    var a = o[i].getAttribute("src").split("lib/ionic/release/js");
                    rootDir = a[0];
                    break
                }
            try {
                (loggerWorker = new Worker(rootDir + "js/workers/logger.js")).addEventListener("error", function () {
                    loggerWorker = null
                }, !1)
            } catch (e) { }
            e = [rootDir + "lib/ionic/release/css/ionic.min.css"];
            for (i = 0; i < r.length; i++) n += '<script src="' + r[i] + '"><\/script>';
            for (i = 0; i < e.length; i++) n = '<link rel="stylesheet" href="' + e[i] + '">' + n;
            t.innerHTML = n
        }
        if (detectIE()) {
            ! function e() {
                var t = document.getElementsByTagName("body");
                t.length ? addClass(t[0], "ie") : timeOut(e, 100)
            }()
        }
        if (0 < window.location.href.indexOf("?preview")) {
            mobileDebug = preview = !0;
            var t = window.location.href.split("?");
            t = t[1].split("&"), previewParams = {};
            for (var n = 0; n < t.length; n++) {
                var r = t[n].split("=");
                previewParams[r[0]] = r[1]
            }
            mobile = previewParams.mobile
        } else {
            if (config.gAnalyticsID) {
                var o = "";
                isKotobeeHosted() && (o = "/bundles/vijualib/templates/reader/public/");
                var i = "";
                (native || desktop) && (i = "ga('set','checkProtocolTask',null);ga('set','anonymizeIp',true);ga('set','forceSSL',true)");
                var a = "'auto'";
                native ? a = "{'storage':'none','clientId':device.uuid}" : desktop && (a = "{'storage':'none'}");
                var s = document.createElement("script");
                s.innerHTML = "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','" + o + "lib/gAnalytics/analytics.js','ga');ga('create', '" + config.gAnalyticsID + "', " + a + ");" + i;
                var l = document.getElementsByTagName("head");
                l.length && l[0].appendChild(s)
            }
            if ("file:" == window.location.protocol || "undefined" != typeof require) {
                if (mobile) native = !0;
                else if ("undefined" != typeof require) desktop = !0, os = "mac", process.env.OS && 0 <= process.env.OS.toLowerCase().indexOf("windows") && (os = 0 <= process.env.PROCESSOR_ARCHITECTURE.indexOf("64") ? "win64" : "win32");
                else if (window.chrome && chrome.app && chrome.app.runtime && chrome.storage) {
                    try {
                        if (!chromeEnabled) return
                    } catch (e) {
                        return
                    }
                    chromeApp = !0
                } else if ("true" != getVar("continueLocally")) return timeOut(function () {
                    document.getElementById("localFallback").style.display = "block", document.getElementById("kotobeeLink").style.display = "none", document.getElementsByTagName("body")[0].style.cssText = "overflow-y: scroll;"
                }, 100), e()
            } else "chrome-extension:" == window.location.protocol && (chromeApp = !0)
        }
        if (!chromeApp) {
            if ("true" != getVar("continueWithoutHTML5") && !(Modernizr.audio && Modernizr.video && Modernizr.backgroundsize && Modernizr.borderradius && Modernizr.canvas && Modernizr.opacity && Modernizr.localstorage && Modernizr.rgba)) return timeOut(function () {
                document.getElementById("html5Fallback").style.display = "block", document.getElementById("kotobeeLink").style.display = "none", document.getElementsByTagName("body")[0].style.cssText = "overflow-y: scroll;"
            }, 100), e();
            setVar("continueLocally", "false"), setVar("continueWithoutHTML5", "false")
        }
        e(), angular.module("ngIOS9UIWebViewPatch", ["ng"]).config(["$provide", function (e) {
            "use strict";
            e.decorator("$browser", ["$delegate", "$window", function (e, t) {
                if (a = t.navigator.userAgent, /(iPhone|iPad|iPod).* OS 9_\d/.test(a) && !/Version\/9\./.test(a)) return o = null, i = (r = e).url, r.url = function () {
                    return arguments.length ? (o = arguments[0], i.apply(r, arguments)) : o || i.apply(r, arguments)
                }, window.addEventListener("popstate", n, !1), window.addEventListener("hashchange", n, !1), r;

                function n() {
                    o = null
                }
                var r, o, i, a;
                return e
            }])
        }]);
        var c = ["ionic", "angular.css.injector", "ngIOS9UIWebViewPatch"];
        "undefined" != typeof scormEnabled && scormEnabled && c.push("scormwrapper"), debug && !isReaderApp || c.push("templates"), app = angular.module("reader.Kotobee", c), ionic.Platform.isFullScreen = !1, app.config(["$stateProvider", "$urlRouterProvider", "$ionicConfigProvider", "$compileProvider", function (e, t, n, r) {
            (chromeApp || desktop) && (r.imgSrcSanitizationWhitelist(/^\s*((https?|ftp|file|blob|chrome-extension):|data:image\/)/), r.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|chrome-extension):/)), n.views.swipeBackEnabled(!1), t.otherwise("/loading"), e.state("home", {
                url: "/loading",
                templateUrl: kTemplate("views/home.html")
            }).state("login", {
                url: "/login",
                templateUrl: kTemplate("views/login.html")
            }).state("register", {
                url: "/register",
                templateUrl: kTemplate("views/register.html")
            }).state("forgotPwd", {
                url: "/forgotpwd",
                templateUrl: kTemplate("views/forgotPwd.html")
            }).state("redeemCode", {
                url: "/redeemcode",
                templateUrl: kTemplate("views/redeemCode.html")
            }).state("library", {
                url: "/library",
                templateUrl: kTemplate("views/library.html")
            }).state("library_tab", {
                url: "/library/tab/:tab",
                templateUrl: kTemplate("views/library.html")
            }).state("reader", {
                url: "/reader",
                templateUrl: kTemplate("views/reader.html")
            }).state("reader.chapter", {
                url: "/chapter/:ch",
                templateUrl: kTemplate("views/reader.html")
            }).state("reader.chapterhash", {
                url: "/chapter/:ch/:hash",
                templateUrl: kTemplate("views/reader.html")
            }).state("libraryreader", {
                url: "/book/:book/reader",
                templateUrl: kTemplate("views/reader.html")
            }).state("libraryreader.chapter", {
                url: "/chapter/:ch",
                templateUrl: kTemplate("views/reader.html")
            }).state("libraryreader.chapterhash", {
                url: "/chapter/:ch/:hash",
                templateUrl: kTemplate("views/reader.html")
            }).state("app", {
                url: "/app",
                templateUrl: kTemplate("views/app.html")
            })
        }]), putTest("prerun timeout start"), window.setTimeout(function () {
            putTest("prerun timeout found!!")
        }, 300), app.run(["$ionicPlatform", "$location", "chapters", "library", function (e, t, n, r) {
            var o, i = t.path();
            0 == i.indexOf("/book/") && 0 < (o = i.split("/book/")[1].split("/")).length && (r.startBook = Number(o[0]));
            0 <= i.indexOf("/reader/chapter/") && (0 < (o = i.split("/reader/chapter/")[1].split("/")).length && (n.startChapter = Number(o[0])), 1 < o.length && (n.startHash = o[1]));
            t.path("/loading"), e.ready(function () {
                try {
                    try {
                        ios || StatusBar && (StatusBar.overlaysWebView(!1), StatusBar.backgroundColorByHexString("#2c1c3c"))
                    } catch (e) { }
                    try {
                        document.addEventListener("deviceready", function () { }, !1)
                    } catch (e) { }
                    try {
                        navigator.splashscreen && setTimeout(function () {
                            navigator.splashscreen.hide()
                        }, 1e3)
                    } catch (e) { }
                    putTest("app 1"), putTest(setTimeout), putTest(window.setTimeout), putTest(window.setTimeout == setTimeout), window.setTimeout(function () {
                        putTest("timeout A")
                    }, 100), putTest("app 1b"), window.setTimeout(function () {
                        putTest("timeout B")
                    }, 300), putTest("app 1c"), setTimeout(function () {
                        putTest("timeout C")
                    }, 600), putTest("app 1d"), window.setTimeout(function () {
                        putTest("timeout D")
                    }, 600), putTest("app 1e"), setTimeout(function () {
                        putTest("app 1a");
                        try {
                            cordova, window.cordova, cordova.plugins, window.cordova.plugins;
                            putTest("app 2")
                        } catch (e) {
                            putTest("app 3")
                        }
                        putTest("app 4"), window.cordova && window.cordova.plugins.Keyboard && cordova.plugins.Keyboard.hideKeyboardAccessoryBar(!0), putTest("app 5"), window.StatusBar && StatusBar.styleDefault(), putTest("app 6"), putTest(angular.element(document.body).scope().start), angular.element(document.body).scope().start(), putTest("app 7")
                    }, native ? 1e3 : 0), putTest("setTimeout called")
                } catch (e) {
                    putTest("in catch"), putTest(e)
                }
            })
        }])
    }();
log = kLog;
app.directive("iframeOnload", [function () {
    return {
        scope: !1,
        link: function (e, t, n) {
            t.on("load", function () {
                return e.modal.siteOptions.loaded()
            })
        }
    }
}]);

app.directive("containerScroll", ["$timeout", function (i) {
    return {
        scope: !1,
        link: function (e, t, n) {
            var r = Math.round(99999 * Math.random());
            addClass(t[0], "containerMobileScroll containerScroll" + r),
                function n() {
                    i(function () {
                        var t = document.getElementsByClassName("containerScroll" + r);

                        function e(e) {
                            t[0].getElementsByClassName("kbContent");
                            e.touches && 1 < e.touches.length || e.stopPropagation()
                        }
                        t.length ? t[0].addEventListener("touchstart", e) : ++o < 50 && n()
                    }, 40)
                }();
            var o = 0
        }
    }
}]);

app.directive("enterWithoutShift", function () {
    return function (t, e, n) {
        e.bind("keydown keypress", function (e) {
            e.shiftKey || 13 === e.which && (t.$apply(function () {
                t.$eval(n.enterWithoutShift)
            }), e.preventDefault())
        })
    }
});

app.directive("enterWithCtrl", function () {
    return function (t, e, n) {
        e.bind("keydown keypress", function (e) {
            e.ctrlKey && 13 === e.which && (t.$apply(function () {
                t.$eval(n.enterWithCtrl)
            }), e.preventDefault())
        })
    }
});

app.directive("searchheader", ["$timeout", "$parse", function (i, a) {
    return {
        restrict: "E",
        replace: !1,
        scope: {
            obj: "=obj",
            prop: "=prop",
            kotobeeGradient: "=kotobeeGradient",
            onValue: "=onValue",
            offValue: "=offValue",
            noCancel: "=noCancel",
            searchKeyOb: "=searchKeyOb",
            onsearch: "&"
        },
        templateUrl: kTemplate("headers/headerSearch.html"),
        link: function (n, r, o) {
            n.$watch("obj[prop]", function (e, t) {
                e === a(o.onValue)(n) && i(function () {
                    r[0].getElementsByTagName("input")[0].focus()
                })
            }), n.hide = function () {
                n.obj[n.prop] = n.offValue
            }
        }
    }
}]);

app.directive("ngEnter", function () {
    return function (t, e, n) {
        e.bind("keydown keypress", function (e) {
            13 === e.which && (t.$apply(function () {
                t.$eval(n.ngEnter)
            }), e.preventDefault())
        })
    }
});

app.directive("nextchapterscroll", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("components/nextChapterScroll.html")
    }
});

app.directive("autofocusme", ["$timeout", function (r) {
    return {
        link: function (e, t, n) {
            r(function () {
                t[0].focus()
            }, 350)
        }
    }
}]);

app.directive("readerheader", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("headers/readerHeader.html"),
        scope: !1
    }
});

app.directive("headerdefault", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("headers/headerDefault.html"),
        scope: !1
    }
});

app.directive("headerselection", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("headers/headerSelection.html"),
        scope: !1
    }
});

app.directive("headersearchitems", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("headers/headerSearchItems.html"),
        scope: !1
    }
});

app.directive("headertextsize", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("headers/headerTextSize.html"),
        scope: !1
    }
});

app.directive("mouseEvents", ["$ionicGesture", "settings", "nav", "stg", "$ionicScrollDelegate", function (e, t, r, n, o) {
    return {
        restrict: "A",
        link: function (e, t, n) {
            r.init(t)
        }
    }
}]);

app.directive("vanillaScroll", function () {
    return {
        restrict: "A",
        link: function (e, t, n) {
            t[0].addEventListener("scroll", function () {
                e.scroll(this)
            })
        }
    }
});

app.directive("selectOptions", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("headers/selectOptions.html")
    }
});

app.directive("media", function () {
    return {
        restrict: "E",
        replace: !0,
        templateUrl: kTemplate("panels/media.html"),
        scope: !1
    }
});

app.directive("noteitem", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("components/noteItem.html")
    }
});

app.directive("bookmarkitem", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("components/bookmarkItem.html")
    }
});

app.directive("hlightitem", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("components/hlightItem.html")
    }
});

app.directive("bookThumb", function () {
    return {
        restrict: "E",
        replace: !0,
        templateUrl: kTemplate("panels/bookThumb.html")
    }
});

app.directive("thumbnail", function () {
    return {
        restrict: "A",
        link: function (e, t, n) {
            t.attr("style", "background-size:cover,width:50px,height:50px")
        }
    }
});

app.directive("showafterimageload", function () {
    return {
        restrict: "A",
        link: function (e, t, n) {
            t.bind("load", function (e) {
                e.currentTarget.style.opacity = 1, autoprefixTransform(e.currentTarget, "translate(-50%, -50%) scale(1)")
            })
        }
    }
});


app.directive("headerShrink", ["autohide", function (r) {
    return {
        restrict: "A",
        link: function (e, t, n) {
            r.setup(e, t, n)
        }
    }
}]);

app.directive("categoryList", ["autohide", function (e) {
    return {
        restrict: "E",
        templateUrl: kTemplate("components/categoryList.html"),
        scope: {
            rtl: "=rtl",
            list: "=list",
            parent: "=parent",
            backlist: "=backlist",
            odd: "=odd",
            categoryClicked: "=clicked",
            categoryBackClicked: "=backClicked"
        }
    }
}]);

app.controller("BookInfoCtrl", ["$scope", "np", "$timeout", "bookinfo", "$element", "$rootScope", "translit", "crdv", "sync", "status", "stg", "book", "modals", "chapters", "$location", "backend", function (t, n, e, r, o, i, a, s, l, c, d, u, f, p, h, g) {
    t.init = function () { };
    t.open = function (e) {
        e, u.open(e)
    };
    t.download = function (e) {
        r.download(e)
    };
    t.actionBtnClicked = function (e) {
        e.redirect ? desktop ? n.openLink(e.redirect) : native && s.openInNativeBrowser(e.redirect) : r.action(e)
    };
    t.delete = function (e) {
        r.delete(e)
    };
    t.addToFav = function (e) {
        r.addToFav(e)
    };
    t.toggleFav = function (e) {
        r.toggleFav(e)
    };
    t.removeFav = function (e) {
        r.removeFav(e)
    };
    t.categoryClicked = function (e) {
        f.hide(), t.modal.bookInfoOptions.categoryClicked(e)
    };
    t.redeemCodeClicked = function () {
        r.redeemCode()
    };
}]);

app.controller("BookSettingsCtrl", ["$scope", "stg", "crdv", "spread", "$element", "nav", "backend", "$location", "$ionicSideMenuDelegate", "cssParser", "modals", "sync", "markers", "settings", "$timeout", function (e, t, n, r, o, i, a, s, l, c, d, u, f, p, h) {
    e.init = function () {
        e.navAnims = [{
            label: "flip",
            val: "navBookBlock"
        }, {
            label: "card",
            val: "navFlip"
        }, {
            label: "slide",
            val: "navSlide"
        }, {
            label: "fade",
            val: "navFade"
        }], e.navModes = [{
            label: "vertical",
            val: "vertical"
        }, {
            label: "horizontal",
            val: "horizontal"
        }], e.viewModes = [{
            label: "auto",
            val: "auto"
        }, {
            label: "singlePage",
            val: "single"
        }, {
            label: "doublePage",
            val: "double"
        }]
    }, e.stylingChanged = function () {
        h(p.stylingChanged, 300)
    }, e.navAnimChanged = function () {
        p.saveSettings()
    }, e.viewModeChanged = function () {
        h(p.viewModeChanged, 300), t.writeSlot("settings" + t.getBookId(!0), t.data.settings)
    }, e.navModeChanged = function () {
        p.saveSettings()
    }, e.lineAdjustChanged = function () {
        h(p.lineAdjustChanged, 300)
    }, e.singlePageScrollChanged = function () {
        h(p.viewModeChanged, 300)
    }, e.editFontSize = function () {
        e.header.mode = "textSize", l.toggleRight(!1), l.toggleLeft(!1)
    }, e.langChanged = function () {
        l.toggleLeft(!1), l.toggleRight(!1), h(p.langChanged, 500)
    }, e.sync = function () {
        l.toggleRight(!1), l.toggleLeft(!1), p.sync()
    }, e.fullscreen = function () {
        l.toggleRight(!1), l.toggleLeft(!1), i.fullscreen()
    }, e.logout = function () {
        l.toggleRight(!1), l.toggleLeft(!1), a.logout()
    }, e.about = function () {
        e.openAuthorWebsite()
    }, e.exit = function () {
        d.show({
            mode: "exit",
            backdropClickToClose: !1,
            exitOptions: {
                exit: function () {
                    n.exit()
                }
            }
        })
    }
}]);

app.controller("CatMenuCtrl", ["$scope", "backend", "translit", "$ionicScrollDelegate", "$ionicModal", "$timeout", "stg", function (e, t, n, r, o, i, a) { }]);

app.controller("ChaptersCtrl", ["$scope", "initializer", "$stateParams", "status", "stg", "nav", "modals", "backend", "$ionicHistory", "crdv", "virt", "chapters", "$ionicGesture", "cache", "$q", "book", "$timeout", "$ionicPopup", "$ionicSideMenuDelegate", "$location", "$ionicScrollDelegate", "selection", "$filter", function (e, t, n, r, o, i, a, s, l, c, d, u, f, p, h, g, m, v, b, y, w, k, x) {
    e.init = function () {
        putTest("ChaptersCtrl init"), e.$on("$stateChangeStart", function (e, t, n) {
            if (0 == t.name.indexOf("reader") || 0 == t.name.indexOf("libraryreader")) {
                var r = n.ch ? n.ch : 0,
                    o = n.hash ? n.hash : null,
                    i = getQueryStringValue("vi");
                u.loadChapterByIndex(Number(r), {
                    hash: o,
                    vIndex: i
                })
            }
        }), e.native || e.desktop || e.$on("$ionicView.beforeEnter", function () {
            if (!t.initialized || !config.kotobee.public && !s.userLoggedIn()) {
                var e = "loading";
                l.forwardView() ? e = l.forwardView().stateId : l.backView() && (e = l.backView().stateId), l.nextViewOptions({
                    disableAnimate: !0
                }), y.path("/" + e)
            }
        })
    }, e.nextChapter = function () {
        return u.nextChapter()
    }, e.prevChapter = function () {
        return u.prevChapter()
    }, e.pinched = function (e) { }, e.showBookInfo = function () {
        native && c.vibrate();
        var e = o.data.book;
        e.coverImg = o.data.epub.bookThumb, e.coverStyle = {
            "background-image": "url('" + e.coverImg + "')"
        }, e.name = e.meta.dc.title;
        var t = {};
        t.book = e, config.kotobee.cloudid && "book" == config.kotobee.mode && (t.book.stats = config.kotobee.stats), t.noAction = !0, a.show({
            mode: "bookInfo",
            dynamic: !0,
            animation: "slide-in-up",
            bookInfoOptions: t
        })
    }
}]);

app.controller("ContentCtrl", ["$location", "nav", "virt", "$sce", "$filter", "stg", "crdv", "$rootScope", "$element", "cache", "$scope", "settings", "$ionicSideMenuDelegate", "$http", "chapters", "$timeout", "selection", "book", "$ionicScrollDelegate", function (e, t, r, n, o, i, a, s, l, c, d, u, f, p, h, g, m, v, b) {
    d.init = function () { }, d.mouseDown = function () {
        mobile && t.resize(), r.enable()
    }, d.mouseUp = function () {
        r.disableAfter(2e3)
    };
    var y;
    d.scroll = function () {
        if (i.data.book) {
            try {
                var e = b.$getByHandle("content"),
                    t = new Event("kotobeeScrolled");
                if (e) {
                    var n = e.getScrollPosition();
                    t.left = n.left, t.top = n.top, t.zoom = n.zoom
                }
                document.dispatchEvent(t)
            } catch (e) { }
            kotobee && kotobee.dispatchEvent("scrolled", n), config.preserveLoc && "app" != config.kotobee.mode && (y && clearTimeout(y), y = setTimeout(function () {
                if ("app" != config.kotobee.mode) {
                    var e = b.$getByHandle("content");
                    if (e) {
                        var t = e.getScrollPosition();
                        t && i.writeSlot("lastLocY" + i.getBookId(), t.top)
                    }
                }
            }, 2e3)), mobile || (r.enable(), r.disableAfter(2e3))
        }
    }
}]);

app.controller("ForgotPwdCtrl", ["$scope", "initializer", "popovers", "crdv", "design", "stg", "$location", "modals", "$ionicHistory", "error", "backend", "auth", "cache", "translit", "$window", function (e, t, n, r, o, i, a, s, l, c, d, u, f, p, h) {
    e.init = function () {
        mobile || (o.stylize("forgotPwd", o.getBgStyle()), d.addListener("pathsChanged", function () {
            o.stylize("forgotPwd", o.getBgStyle())
        })), e.$on("$ionicView.afterEnter", function () {
            f.setOb("title", replaceHTML(f.getOb("title"), p.get("forgotPwd")))
        }), e.native || e.desktop || e.$on("$ionicView.beforeEnter", function () {
            if (!t.initialized || d.cloudParam("public") || i.data.user && d.userLoggedIn()) {
                var e = "loading";
                l.forwardView() ? e = l.forwardView().stateId : l.backView() && (e = l.backView().stateId), l.nextViewOptions({
                    disableAnimate: !0
                }), a.path("/" + e)
            }
        })
    }, e.showMenu = function () {
        var e = [{
            name: p.get("exit"),
            func: function () {
                n.hide(), s.show({
                    mode: "exit",
                    backdropClickToClose: !1,
                    exitOptions: {
                        exit: function () {
                            r.exit()
                        }
                    }
                })
            }
        }];
        n.show({
            mode: "cornerList",
            listOb: e,
            backdropClickToClose: !1
        })
    }, e.submit = function () {
        e.user && e.user.email && d.forgotPwd(e.user, function (e) {
            e.error ? c.update({
                error: "s_authError" == e.error ? "accessDenied" : e.error
            }) : (s.show({
                mode: "success",
                successOptions: {
                    msg: p.get("pwdEmailSent")
                }
            }), h.history.back())
        })
    }, e.backClicked = function () {
        h.history.back()
    }
}]);

app.controller("GlobalCtrl", ["cache", "modals", "popovers", "np", "book", "$http", "$compile", "$rootScope", "initializer", "translit", "error", "notes", "$sce", "design", "settings", "status", "tinCan", "$scope", "backend", "stg", "$location", function (e, t, n, r, o, i, a, s, l, c, d, u, f, p, h, g, m, v, b, y, w) {
    putTest = function (e) { }, v.init = function () {
        putTest("Global init"), v.mobile = mobile, v.native = native, v.desktop = desktop, v.ios = ios, v.preview = preview, v.config = config, putTest("Global init good"), r.init(function () { })
    }, v.start = function () {
        putTest("Global Start"), putTest("TO HOME!"), v.cloudParam = b.cloudParam, t.setScope(v), n.setScope(v), v.started = !0, putTest("A!"), setTimeout(function () {
            putTest("timeout postInitialize"), l.postInitialize(v)
        }, 700), v.$$phase || v.$apply()
    }, v.tinCanShortcut = function (e) {
        m.send(e)
    }, v.questionsAddToNotebookShortcut = function (e, t) {
        u.addNotesFromQuestions(e, t || angular.noop)
    }, v.getNotebookShortcut = function (o, i) {
        y.getNotes(function (e) {
            var t = [currentIndex];
            y.data.currentRPageOb && t.push(y.data.currentRPageOb.index);
            for (var n = 0; n < t.length; n++)
                for (var r = 0; r < e.length; r++) e[r].chapter == t[n] && 0 == e[r].location.indexOf(o.rootIndex) && (e[r].location = e[r].location.split(o.rootIndex)[1], u.addIcon(e[r], o.root));
            i && i()
        })
    }, v.location = function (e) {
        w.path(e)
    }, v.kTemplate = function (e) {
        return kTemplate(e)
    }, v.translit = function (e) {
        return c.get(e)
    }, v.htmlSafe = function (e) {
        return f.trustAsHtml(e)
    }, v.openAuthorWebsite = function () {
        var e = y.data.settings.language;
        "en" != e && "es" != e && "ar" != e && "fr" != e && (e = "en"), window.open("https://www.kotobee.com/" + e + "/products/author", "_system")
    }, v.applyNativeScroll = function (t) {
        var n, e = t.getElementsByClassName("kbContent");
        if (e.length && !(e[0].scrollHeight <= t.offsetHeight)) {
            t.children.length && "ion-scroll" == t.children[0].nodeName.toLowerCase() && (n = t.children[0], t.appendChild(e[0]), t.removeChild(n), t.scrollTop = 0), (n = document.createElement("ion-scroll")).setAttribute("scrollY", "true"), n.setAttribute("overflow-scroll", "false"), n.setAttribute("style", "width:100%;height:100%"), n.setAttribute("container-scroll", "true");
            for (var r = 0; r < t.children.length; r++) n.appendChild(t.children[r]);
            setTimeout(function () {
                var e = a(n.outerHTML)(v);
                angular.element(t).append(e)
            }, 0)
        }
    }, v.globalClick = function () { }, v.globalTouch = function () { }, v.refreshSignatures = function () {
        b.refreshSignatures()
    }, v.testModals = function () {
        t.show({
            mode: "loading",
            loadingMsg: c.get("pleaseWait")
        });
        t.show({
            mode: "login",
            dynamic: !0,
            animation: "slide-in-up"
        }), t.show({
            mode: "login",
            dynamic: !0,
            animation: "slide-in-up"
        }), t.show({
            mode: "login",
            dynamic: !0,
            animation: "slide-in-up"
        }), t.show({
            mode: "bookInfo",
            dynamic: !0,
            animation: "slide-in-up"
        }), t.show({
            mode: "bookInfo",
            dynamic: !0,
            animation: "slide-in-up"
        }), t.show({
            mode: "update",
            dynamic: !0,
            animation: "slide-in-up",
            backdropClickToClose: !0
        }), t.show({
            mode: "bookInfo",
            dynamic: !0,
            animation: "slide-in-up"
        }), t.show({
            mode: "generalError"
        }), t.show({
            mode: "loading",
            loadingMsg: c.get("pleaseWait"),
            backdropClickToClose: !0
        }), t.show({
            mode: "note",
            dynamic: !0,
            animation: "slide-in-up",
            backdropClickToClose: !0
        }), t.show({
            mode: "hlight"
        }), t.show({
            mode: "externalSite",
            dynamic: !0,
            animation: "slide-in-up",
            backdropClickToClose: !0
        }), t.show({
            mode: "bookInfo",
            dynamic: !0,
            animation: "slide-in-up"
        }), t.show({
            mode: "flashUnsupported",
            flashOb: {
                text: "Text for flash unsupported site"
            }
        }), t.show({
            mode: "success",
            successOptions: {
                msg: "SUCCESS MSG"
            }
        }), t.show({
            mode: "localOnlyError"
        }), t.show({
            mode: "xFrameError"
        }), t.show({
            mode: "networkError"
        }), t.show({
            mode: "exit"
        }), t.show({
            mode: "waitTimeout"
        }), t.show({
            mode: "backToLib"
        }), t.show({
            mode: "notebook",
            dynamic: !0,
            animation: "slide-in-up",
            scope: {
                notebookClose: t.hide
            }
        }), t.show({
            mode: "search",
            dynamic: !0,
            animation: "slide-in-up"
        }), t.show({
            mode: "image",
            dynamic: !0,
            animation: "slide-in-up"
        }), t.show({
            mode: "libraryInfo",
            dynamic: !0,
            animation: "slide-in-up"
        }), t.show({
            mode: "bookInfo",
            dynamic: !0,
            animation: "slide-in-up"
        }), t.show({
            mode: "encryptionKey",
            backdropClickToClose: !0
        });
        t.show({
            mode: "loading",
            loadingMsg: c.get("pleaseWait"),
            timeLimit: 3e3
        })
    }, v.injectUserCSS = function (e) {
        p.injectUserCSS(e)
    }
}]);

app.controller("HomeCtrl", ["$scope", "design", "backend", "stg", "initializer", "$timeout", "$ionicHistory", "$location", function (e, t, n, r, o, i, a, s) {
    function l() { }
    e.init = function () {
        putTest("HomeCtrl!"), o.initialized || o.initializing || o.preInitialize(e), n.addListener("pathsChanged", l)
    }
}]);

app.controller("LibraryCtrl", ["$scope", "error", "bookinfo", "autohide", "$sce", "$location", "$ionicPlatform", "$rootScope", "$element", "$ionicHistory", "auth", "modals", "popovers", "translit", "library", "chapters", "status", "book", "$timeout", "$ionicPopup", "$ionicModal", "$ionicScrollDelegate", "backend", "crdv", "$filter", "stg", "settings", function (o, t, n, e, r, i, a, s, l, c, d, u, f, p, h, g, m, v, b, y, w, k, x, C, I, E, T) {
    var S, A, O, B, L;

    function N(e) {
        for (var t = 0; t < o.sortOb.options.length; t++)
            if (o.sortOb.options[t].val == e) return o.sortOb.options[t]
    }

    function M() {
        if (h.initialize(), h.addListener(P), e.libInitialized(), o.initialized = !0, o.catList = E.data.currentLibrary.categories, x.userLoggedIn() && ("account" != E.data.currentLibrary.authbookview || x.cloudParam("public") || (o.supportsAccountView = !0)), "categories" == E.data.currentLibrary.startingpage) F(), R("categories");
        else if (0 == i.path().indexOf("/library/tab/downloads")) o.downloadedClicked();
        else if (o.supportsAccountView) R("account"), b(function () {
            D({
                account: !0
            })
        });
        else if (!native && !desktop || x.userLoggedIn() || config.kotobee.public || !config.kotobee.offline) {
            if (R("all"), !x.libraryHasPublicBooks() && !x.cloudParam("public") && "hide" == E.data.currentLibrary.authbookview && !x.userLoggedIn()) return o.paneMsg = "noEbooksAvailable", void F();
            b(D)
        } else R("downloaded"), o.paneMsg = null, 0 == E.data.downloaded.length && (o.paneMsg = "noEbooksDownloaded"), F();
        h.startBook && (function (e) {
            if (!x.userLoggedIn()) return;
            if (E.data.currentLibrary && E.data.currentLibrary.books)
                for (var t = 0; t < E.data.currentLibrary.books.length; t++)
                    if (E.data.currentLibrary.books[t].id == e) return v.open(E.data.currentLibrary.books[t]);
            x.getEbook({
                bid: e
            }, function (e) {
                v.open(e)
            })
        }(h.startBook), h.startBook = null)
    }

    function P() {
        var e, t = arguments[0];
        "preload" == t && (arguments[1].accumulated || (e = !0), mobile || (e = !0), o.paneMsg = null, e && function () {
            for (var e = document.getElementsByClassName("libLoaderAnim"), t = 0; t < e.length; t++) removeClass(e[t], "hide")
        }());
        if ("postload" == t) {
            var n = arguments[2];
            e = !1, o.stopInfiniteScroll(), S = !1, o.stopRefresher();
            var r = function (e) {
                {
                    if (L) {
                        for (var t = 0, n = 0; n < e.length; n++)
                            if (e[n].searchresults)
                                for (var r = 0; r < e[n].searchresults.length; r++) e[n].searchresults[r].results && (t += e[n].searchresults[r].results.length);
                        return t
                    }
                    return e.length
                }
            }(n);
            o.moreData = r >= h.maxResults, o.moreData && h.setMax(r)
        }
    }

    function R(e) {
        o.tab = e, mobile ? k.$getByHandle("thumbs").scrollTop(!1) : document.getElementById("libraryThumbs").scrollTop = 0
    }

    function D(e) {
        e = e || {}, o.paneMsg = null, O && (e.catid = O.id, e.listmode = O.listmode), A && (e.key = A), L && (e.contentkey = L), h.getEbooks(e, function (e) {
            "all" != o.tab && "account" != o.tab || (e.empty && (o.paneMsg = "noEbooksAvailable"), o.spaceAvailable() || F(), o.$$phase || o.$apply())
        })
    }

    function F() {
        for (var e = document.getElementsByClassName("libLoaderAnim"), t = 0; t < e.length; t++) addClass(e[t], "hide")
    }

    function _(e, t) {
        t = t || E.data.currentLibrary.categories;
        for (var n = 0; n < t.length; n++) {
            if (t[n].id == e) return t[n];
            if (t[n].children) {
                var r = _(e, t[n].children);
                if (r) return r
            }
        }
    }

    function q() {
        B && B(), B = a.registerBackButtonAction(function (e) {
            o.searchOb.enabled ? o.disableSearch() : "categories" == o.tab && o.catList != E.data.currentLibrary.categories ? o.categoryBackClicked() : O ? o.back() : A ? o.back() : L && o.back(), o.$apply()
        }, 102)
    }
    o.initialized = !1, o.init = function () {
        document.getElementsByClassName("libraryView");
        x.setLibraryLoginCallback(function () {
            M()
        }), x.setLibraryLogoutCallback(function () {
            o.supportsAccountView && "all" != o.tab && (R("all"), b(D)), o.supportsAccountView = !1, F()
        }), o.sortOb = {}, o.sortOb.options = [{
            name: "dateAddedNewestFirst",
            data: "date",
            dir: "desc",
            val: "dateDesc"
        }, {
            name: "dateAddedOldestFirst",
            data: "date",
            dir: "asc",
            val: "dateAsc"
        }, {
            name: "titleA-Z",
            data: "name",
            dir: "asc",
            val: "titleAsc"
        }, {
            name: "titleZ-A",
            data: "name",
            dir: "desc",
            val: "titleDesc"
        }, {
            name: "categoryA-Z",
            data: "cat",
            dir: "asc",
            val: "catAsc"
        }, {
            name: "categoryZ-A",
            data: "cat",
            dir: "desc",
            val: "catDesc"
        }], o.sortOb.method = N("dateDesc"), E.data.currentLibrary && E.data.currentLibrary.defaultsort && (o.sortOb.method = N(E.data.currentLibrary.defaultsort)), h.sortMethod = o.sortOb.method, o.$on("$ionicView.beforeEnter", function (e, t) {
            if (null, !(o.native || o.desktop || d.authenticated && "library" == config.kotobee.mode)) {
                var n = "loading";
                c.forwardView() ? n = c.forwardView().stateId : c.backView() && (n = c.backView().stateId), c.nextViewOptions({
                    disableAnimate: !0
                }), i.path("/" + n)
            }
            c.forwardView() && c.forwardView().stateId, c.backView() && c.backView().stateId, "forward" == t.direction && (E.data.currentLibrary.books = [], E.data.favorites = [])
        }), o.$on("$ionicView.afterEnter", function (e, t) {
            if ("back" == t.direction) return F(), void h.setBrowserTitle();
            x.cloudParam("entry") ? x.login().then(function (e) {
                e.error && M()
            }) : (x.cloudParam("public"), M())
        })
    }, o.userLogin = function () {
        x.userLoggedIn() ? x.logout() : t.update({
            error: "s_authError"
        })
    }, o.langSelected = function (e) {
        o.langDropdownExpanded = !1, E.data.settings.language = e, T.langChanged()
    }, o.showLibraryMenu = function () {
        f.show({
            mode: "libraryMenu",
            menuItem: o.menuItemClicked,
            backdropClickToClose: !1
        })
    }, o.spaceAvailable = function () {
        var e = document.getElementById("libraryThumbs"),
            t = document.getElementById("libraryThumbsContent");
        if (e.offsetHeight > t.offsetHeight) {
            if (S) return;
            if (!o.moreDataExists()) return;
            return S = !0, o.loadMore(), !0
        }
    }, o.scroll = function (e) {
        if ((L ? .95 : .8) < (e = e || document.getElementById("libraryThumbs")).scrollTop / (document.getElementById("libraryThumbsContent").offsetHeight - e.offsetHeight)) {
            if (S) return;
            if (!o.moreDataExists()) return;
            S = !0, o.loadMore()
        }
    }, o.bookClicked = function (e) {
        n.show({
            book: e,
            categoryClicked: o.categoryClicked
        })
    }, o.refresherExists = function () {
        try {
            if (!E.data.currentLibrary) return !1;
            if ("categories" == o.tab) return !1;
            if ("downloaded" == o.tab) return !1;
            if ("favorites" == o.tab) return !0;
            if ("account" == o.tab) return !0;
            if ("all" == o.tab) return !0
        } catch (e) {
            return !1
        }
        return !0
    }, o.doRefresh = function () {
        var e = {
            accumulated: !0
        };
        "all" == o.tab ? D(e) : "account" == o.tab ? (e.account = !0, D(e)) : "favorites" == o.tab ? o.loadFavorites(e) : o.stopRefresher()
    }, o.sort = function () {
        h.sortMethod = o.sortOb.method, "downloaded" == o.tab ? I("orderBy")(o.data.downloaded, o.sortOb.method.data) : "account" == o.tab ? D({
            account: !0
        }) : D()
    }, o.menuItemClicked = function (e) {
        "language" == e && T.langChanged(), "redeemCode" == e && (f.hide(), o.redeemCodeClicked()), "info" == e && (f.hide(), u.show({
            mode: "libraryInfo",
            dynamic: !0,
            animation: "slide-in-up",
            libraryInfo: E.data.currentLibrary.libraryinfo
        })), "logout" == e && (f.hide(), o.data.user.pwd || o.data.user.code ? x.logout() : t.update({
            error: "s_authError"
        })), "exit" == e && (f.hide(), u.show({
            mode: "exit",
            backdropClickToClose: !1,
            exitOptions: {
                exit: function () {
                    C.exit()
                }
            }
        }))
    }, o.loadFavorites = function (e) {
        e = e || {}, o.paneMsg = null, O && (e.catid = O.id), A && (e.key = A), L && (e.contentkey = L), e.favs = !0, h.getEbooks(e, function (e) {
            "favorites" == o.tab && (e.error && (o.paneMsg = "failedToGetFavs"), 0 == e.length && (o.paneMsg = "noFavsAvailable"), o.spaceAvailable() || F())
        })
    }, o.searchEbooksOld = function (e) {
        o.subMode = "search", o.title = "searchResults", A = h.currentKey = e, O = h.currentCat = null, o.supportsAccountView && "all" != o.tab ? (R("account"), b(function () {
            D({
                account: !0
            })
        })) : (R("all"), b(D)), q()
    }, o.searchEbooks = function (e) {
        if (e && e.length && 0 != e.length) {
            if (e.length < 3) return t.update({
                error: "lessThan3Characters"
            }), void u.hide();
            o.subMode = "search", o.title = "searchResults", config.searchLibContent ? L = h.currentContentKey = e : A = h.currentKey = e, O = h.currentCat = null, o.supportsAccountView && "all" != o.tab ? (R("account"), b(function () {
                D({
                    account: !0
                })
            })) : (R("all"), b(D)), q()
        }
    }, o.enableSearch = function () {
        o.searchOb.enabled = !0, q()
    }, o.disableSearch = function () {
        o.searchOb.enabled = !1, B && B()
    }, o.loadMore = function () {
        var e = {
            more: !0,
            accumulated: !0
        };
        "all" == o.tab ? D(e) : "account" == o.tab ? (e.account = !0, D(e)) : "favorites" == o.tab ? o.loadFavorites(e) : (o.stopInfiniteScroll(), S = !0)
    }, o.stopInfiniteScroll = function () {
        s.$broadcast("scroll.infiniteScrollComplete")
    }, o.stopRefresher = function () {
        s.$broadcast("scroll.refreshComplete")
    }, o.moreDataExists = function () {
        try {
            if (!E.data.currentLibrary) return !1;
            if ("categories" == o.tab) return !1;
            if ("downloaded" == o.tab) return !1;
            var e;
            if ("favorites" == o.tab && (e = o.data.favorites), "all" == o.tab && (e = E.data.currentLibrary.books), "account" == o.tab && (e = E.data.currentLibrary.books), 0 == e.length) return !1
        } catch (e) {
            return !1
        }
        return o.moreData
    }, o.back = function () {
        o.subMode = !1, o.title = null, A = h.currentKey = null, L = h.currentContentKey = null, O = h.currentCat = null, o.supportsAccountView && "all" != o.tab ? (R("account"), b(function () {
            D({
                account: !0
            })
        })) : (R("all"), b(D)), B && B()
    }, o.categoryBackClicked = function () {
        if (o.catList && o.catList.length) {
            var e = o.catList[0].parent;
            for (e = _(e.id); ;) {
                if (!(e = e.parent)) return void (o.catList = E.data.currentLibrary.categories);
                if ("separate" == (e = _(e.id)).submode) return void (o.catList = e.children)
            }
        }
    }, o.categoryClicked = function (e) {
        if (native && C.vibrate(), "expand" != e.submode)
            if ("separate" != e.submode) {
                O = h.currentCat = e, A = h.currentKey = null, L = h.currentContentKey = null, O.id, o.subMode = "category", o.title = O.name, o.supportsAccountView ? (R("account"), b(function () {
                    D({
                        account: !0
                    })
                })) : (R("all"), b(D)), q()
            } else o.catList = e.children;
        else e.expand = !e.expand
    }, o.categoriesClicked = function () {
        native && C.vibrate(), R("categories"), o.paneMsg = null, F()
    }, o.accountClicked = function () {
        native && C.vibrate(), R("account"), F(), b(function () {
            D({
                account: !0
            })
        })
    }, o.allClicked = function () {
        native && C.vibrate(), R("all"), b(D)
    }, o.downloadedClicked = function () {
        native && C.vibrate(), R("downloaded"), o.paneMsg = null, 0 == E.data.downloaded.length && (o.paneMsg = "noEbooksDownloaded"), F()
    }, o.favoritesClicked = function () {
        native && C.vibrate(), R("favorites"), o.loadFavorites()
    }, o.redeemCodeClicked = function () {
        d.redeemCode({
            source: "topmenu"
        }, function () {
            o.allClicked()
        })
    }
}]);

app.controller("LoginCtrl", ["$scope", "initializer", "crdv", "$ionicHistory", "popovers", "design", "modals", "error", "backend", "auth", "stg", "cache", "translit", "$location", function (t, n, r, o, i, e, a, s, l, c, d, u, f, p) {
    t.model = {};
    t.init = function () {
        mobile || (e.stylize("login", e.getBgStyle()), l.addListener("pathsChanged", function () {
            e.stylize("login", e.getBgStyle())
        })), "email" == config.kotobee.loginmode ? t.canLoginByEmail = !0 : "code" == config.kotobee.loginmode ? t.canLoginByCode = !0 : t.canLoginByEmail = t.canLoginByCode = !0, t.mode = t.canLoginByEmail ? "email" : "code", t.$on("$ionicView.afterEnter", function () {
            u.setOb("title", replaceHTML(u.getOb("title"), f.get("login")))
        }), t.native || t.desktop || t.$on("$ionicView.beforeEnter", function () {
            if (!n.initialized || config.kotobee.public || l.userLoggedIn()) {
                var e = "loading";
                o.forwardView() ? e = o.forwardView().stateId : o.backView() && (e = o.backView().stateId), o.nextViewOptions({
                    disableAnimate: !0
                }), p.path("/" + e)
            }
        })
    }, t.showMenu = function () {
        var e = [{
            name: f.get("exit"),
            func: function () {
                i.hide(), a.show({
                    mode: "exit",
                    backdropClickToClose: !1,
                    exitOptions: {
                        exit: function () {
                            r.exit()
                        }
                    }
                })
            }
        }];
        i.show({
            mode: "cornerList",
            listOb: e,
            backdropClickToClose: !1
        })
    }, t.login = function () {
        if (t.model) {
            if ("code" == t.mode) {
                if (!t.model.code) return void s.update({
                    error: "enterYourCode"
                });
                d.data.user.code = t.model.code, d.data.user.email = d.data.user.pwd = null
            } else {
                if (!d.data.user.email) return void s.update({
                    error: "enterYourEmail"
                });
                if (!t.model.pwd) return void s.update({
                    error: "enterYourPwd"
                });
                d.data.user.pwd = t.model.pwd, d.data.user.code = null
            }
            config.kotobee.permsLoaded ? e() : c.getPermissions(e)
        }

        function e() {
            l.login({
                dontShowErrorDialog: !0,
                userInput: !0
            }).then(function (e) {
                e.error ? s.update({
                    error: "s_authError" == e.error ? "accessDenied" : e.error
                }) : (t.model.pwd = null, t.model.code = null, c.startup())
            })
        }
    }, t.register = function () {
        a.hide(), p.path("/register")
    }, t.forgotPassword = function () {
        p.path("/forgotpwd")
    }, t.signInByEmail = function () {
        t.mode = "email"
    }, t.redeemCode = function () {
        t.mode = "code"
    }
}]);

app.controller("MediaCtrl", ["$ionicScrollDelegate", "media", "$ionicModal", "$location", "$rootScope", "$scope", "selection", "crdv", "stg", "$ionicLoading", function (e, t, n, r, o, i, a, s, l, c) {
    i.init = function () { }, i.mediaClicked = function (e) {
        t.show(e)
    }, i.hide = function () {
        t.hide()
    }
}]);

app.controller("NotebookCtrl", ["$scope", "$ionicScrollDelegate", "$timeout", "crdv", "translit", "$rootScope", "selection", "book", "chapters", "$location", "modals", "$stateParams", "hlights", "stg", function (o, n, r, i, d, a, s, u, l, e, c, t, f, p) {
    o.model = {}, o.ui = {}, o.noteBtns = {
        notes: !0,
        bookmarks: !0,
        hlights: !0
    };
    o.init = function () { }, o.notebookClose = function () {
        c.hide(), o.bookmarkContent = null, o.noteContent = null, o.hlightContent = null, o.notebook = {}
    }, o.noteBtnClicked = function (e, t) {
        "all" == e ? t.notes = t.bookmarks = t.hlights = !(t.notes && t.bookmarks && t.hlights) : (t.notes = t.bookmarks = t.hlights = !1, t[e] = !0), overflowScroll ? document.getElementById("notebookContent").scrollTop = 0 : n.$getByHandle("notebookScroll").scrollTop()
    }, o.pdfExport = function () {
        var e = o.serializeNotebookHtml(),
            t = (p.data.book.meta.dc.title ? p.data.book.meta.dc.title : d.get("book")) + ".pdf";
        r(function () {
            i.createPDF(e, t, function (e) { })
        }, 500)
    }, o.serializeNotebookHtml = function () {
        var e = "<br/>",
            t = '<html><head><meta charset="UTF-8" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body>';
        t += c([d.get("notebookFor"), ' "' + p.data.book.meta.dc.title + '"'], "h1"), t += e + e;
        for (var n = p.data.notebookContent.results, r = 0; r < n.length; r++)
            if (n[r]) {
                var o = "";
                o += c(u.getTitleByIndex(r), "h2") + e;
                var i = "..................................................................................";
                o += c([i + i], "p");
                for (var a = 0; a < n[r].length; a++) {
                    var s = n[r][a],
                        l = "";
                    null != s.nid ? "page" == s.type ? l += c([d.get("pageNote") + ": ", s.note], "p") + e : "content" == s.type && (l += c([d.get("noteFor"), ' "' + s.src + '"'], "p") + e, l += c([d.get("note") + ": ", s.note], "p") + e) : null != s.hid ? (l += c([d.get("highlightFor"), ' "' + s.src + '"'], "p") + e, l += c([d.get("highlightColor") + ": " + s.color + e], "p", 'style="background-color:' + s.color + '"')) : null != s.bmid && (l += c([d.get("contentBookmark")], "p") + e), o += c([l], "p")
                }
                t += c([o], "p", 'style="border:solid 3px #000;background-color:#ddd"')
            }
        function c(e, t, n) {
            for (var r = "", o = 0; o < e.length; o++) {
                var i = e[o];
                if (s = /^[\u0000-\u007fñáéíóúü]*$/.test(i)) r += s ? i : "<em>Non-unicode text</em>";
                else
                    for (var a = 0; a < i.length; a++) {
                        var s;
                        r += (s = /^[\u0000-\u007fñáéíóúü]*$/.test(i[a])) ? i[a] : "?"
                    }
            }
            return "<" + t + (n ? " " + n : "") + ">" + r + "</" + t + ">"
        }
        return t += "</body></html>"
    }, o.shareNotebook = function () {
        var e = o.serializeNotebookTxt();
        i.share(e)
    }, o.serializeNotebookTxt = function () {
        for (var e = "\n", t = d.get("notebookFor") + ' "' + p.data.book.meta.dc.title + '"' + e, n = t.length; n--;) t += "#";
        t += e + e;
        for (var r = p.data.notebookContent.results, o = 0; o < r.length; o++)
            if (r[o]) {
                var i = d.get("chapter") + " " + (o + 1);
                t += i + e;
                for (n = i.length; n--;) t += "_";
                t += e + e;
                for (var a = 0; a < r[o].length; a++) {
                    var s = r[o][a];
                    null != s.nid ? "page" == s.type ? t += d.get("pageNote") + ": " + s.note + e : "content" == s.type && (s.src ? t += d.get("noteFor") + ' "' + s.src + '"' + e : t += d.get("note") + e, t += d.get("note") + ": " + s.note + e) : null != s.hid ? (t += d.get("highlightFor") + ' "' + s.src + '"' + e, t += d.get("highlightColor") + " " + s.color + e) : null != s.bmid && (t += d.get("contentBookmark") + e), t += "........................\n\n"
                }
            }
        return t
    }, o.deleteFromNotebookContent = function (e, t) {
        for (var n = p.data.notebookContent.results, r = 0; r < n.length; r++)
            if (n[r])
                for (var o = 0; o < n[r].length; o++)
                    if (n[r][o][e] == t) return n[r].splice(o, 1), void (n[r].length || n.splice(r, 1))
    }, o.noteItemClicked = function (e) {
        var t = e.chapter,
            n = e.location;
        n = String(n).split(",")[0], c.hide(), r(function () {
            l.redirect(t, {
                hash: n
            })
        }, 100)
    }, o.deleteNote = function (r, e) {
        p.deleteNote(r, null, function (e) {
            o.noteContent = e, s.deleteNoteMarker(r), o.deleteFromNotebookContent("nid", r);
            for (var t = document.getElementsByClassName("note" + r), n = t.length; n--;) n == t.length - 1 && (removeClass(t[n], "last"), t[n].removeAttribute("nloc"), t[n].removeAttribute("ncontent")), removeClass(t[n], "contentNoteBG"), removeClass(t[n], "note" + r);
            a.$$phase || a.$apply()
        }), e.stopPropagation()
    }, o.deleteBookmark = function (t, e) {
        p.deleteBookmark(t, null, function (e) {
            o.bookmarkContent = e, s.deleteBookMarker(t), o.deleteFromNotebookContent("bmid", t), a.$$phase || a.$apply()
        }), e.stopPropagation()
    }, o.deleteHlight = function (t, n) {
        f.delete(t, null, function (e) {
            o.hlightContent = e, o.deleteFromNotebookContent("hid", t), n.stopPropagation(), a.$$phase || a.$apply()
        })
    }
}]);

app.controller("PreviewCtrl", ["cache", "modals", "popovers", "bookinfo", "crdv", "np", "book", "$http", "$rootScope", "initializer", "translit", "error", "notes", "$sce", "design", "settings", "status", "tinCan", "$scope", "backend", "stg", "$location", function (e, t, n, r, o, i, a, s, l, c, d, u, f, p, h, g, m, v, b, y, w, k) {
    b.init = function () {
        putTest("Preview init"), b.mobile = mobile, b.native = native, b.desktop = desktop, b.ios = ios, b.preview = preview, b.config = config, putTest("Global init good"), i.init(function () { })
    }, b.start = function () {
        l.data = w.data, l.view = w.view, b.cloudParam = y.cloudParam, t.setScope(b), n.setScope(b), b.started = !0
    }, b.previewPage = function (e, t) {
        b.started ? (t = t || {
            lang: "en",
            book: {
                name: "Alice",
                description: "A great book indeed"
            }
        }, "bookInfo" == (e = e || "bookInfo") && (config = t.config ? t.config : {
            kotobee: {
                entry: 1,
                mode: "library"
            }
        }, w.data.user = t.user ? t.user : {
            loggedIn: !0
        }, w.data.currentLibrary = t.library ? t.library : {}, d.setLang(t.lang ? t.lang : "en"), r.show({
            book: t.book
        }, {
                animation: "none"
            }))) : setTimeout(function () {
                b.previewPage(e, t)
            }, 100)
    }, b.kTemplate = function (e) {
        return kTemplate(e)
    }, b.translit = function (e) {
        return d.get(e)
    }, b.htmlSafe = function (e) {
        return p.trustAsHtml(e)
    }
}]);

app.controller("ReaderCtrl", ["$ionicScrollDelegate", "modals", "library", "$sce", "$location", "$rootScope", "$scope", "selection", "$ionicSideMenuDelegate", "$state", "$ionicPopover", "chapters", "book", "$timeout", "stg", function (e, t, n, r, o, i, a, s, l, c, d, u, f, p, h) {
    a.init = function () {
        a.mode = "reading"
    }, a.chapterClicked = function (e) {
        u.displayChapters()
    }, a.backClicked = function (e) {
        l.isOpenLeft() || t.show({
            mode: "backToLib",
            backdropClickToClose: !1,
            backToLibOptions: {
                back: function () {
                    t.hide(), f.removeBookConfigElem(), u.clearContent(), setTimeout(n.setBrowserTitle, 600), setTimeout(h.resetBook, 600), o.path("/library")
                }
            }
        })
    }, a.searchClicked = function (e) {
        a.applyHeader("search")
    }, a.menuClicked = function (e) { }, a.setting1Clicked = function (e) { }, a.setting2Clicked = function (e) { }, a.setting3Clicked = function (e) { }, a.linkClicked = function () { }
}]);

app.controller("RedeemCodeCtrl", ["$scope", "initializer", "popovers", "crdv", "design", "stg", "$location", "modals", "$ionicHistory", "error", "backend", "auth", "cache", "translit", "$window", function (t, n, r, o, e, i, a, s, l, c, d, u, f, p, h) {
    t.init = function () {
        mobile || (e.stylize("redeemCode", e.getBgStyle()), d.addListener("pathsChanged", function () {
            e.stylize("redeemCode", e.getBgStyle())
        })), t.$on("$ionicView.afterEnter", function () {
            f.setOb("title", replaceHTML(f.getOb("title"), p.get("redeemCode")))
        }), t.native || t.desktop || t.$on("$ionicView.beforeEnter", function () {
            if (!n.initialized || d.cloudParam("public") || i.data.user && d.userLoggedIn()) {
                var e = "loading";
                l.forwardView() ? e = l.forwardView().stateId : l.backView() && (e = l.backView().stateId), l.nextViewOptions({
                    disableAnimate: !0
                }), a.path("/" + e)
            }
        })
    }, t.showMenu = function () {
        var e = [{
            name: p.get("exit"),
            func: function () {
                r.hide(), s.show({
                    mode: "exit",
                    backdropClickToClose: !1,
                    exitOptions: {
                        exit: function () {
                            o.exit()
                        }
                    }
                })
            }
        }];
        r.show({
            mode: "cornerList",
            listOb: e,
            backdropClickToClose: !1
        })
    }, t.submit = function () {
        function e() {
            d.cloudParam("entry") ? (h.history.back(), d.login()) : d.login().then(function (e) {
                e.error ? c.update({
                    error: "s_authError" == e.error ? "accessDenied" : e.error
                }) : u.startup()
            })
        }
        t.user && t.user.code && (i.data.user.code = t.user.code, i.data.user.email = i.data.user.pwd = null, config.kotobee.permsLoaded ? e() : u.getPermissions(e))
    }, t.backClicked = function () {
        h.history.back()
    }
}]);

app.controller("RegisterCtrl", ["$scope", "initializer", "popovers", "crdv", "modals", "design", "stg", "$ionicHistory", "$location", "translit", "backend", "backend", "translit", "cache", "error", "$window", function (t, n, r, o, i, e, a, s, l, c, d, d, c, u, f, p) {
    t.init = function () {
        t.user = {
            email1: "",
            email2: ""
        }, mobile || (e.stylize("register", e.getBgStyle()), d.addListener("pathsChanged", function () {
            e.stylize("register", e.getBgStyle())
        })), t.$on("$ionicView.afterEnter", function () {
            u.setOb("title", replaceHTML(u.getOb("title"), c.get("register")))
        }), t.native || t.desktop || t.$on("$ionicView.beforeEnter", function () {
            if (!n.initialized || config.kotobee.public || d.userLoggedIn()) {
                var e = "loading";
                s.forwardView() ? e = s.forwardView().stateId : s.backView() && (e = s.backView().stateId), s.nextViewOptions({
                    disableAnimate: !0
                }), l.path("/" + e)
            }
        })
    }, t.showMenu = function () {
        var e = [{
            name: c.get("exit"),
            func: function () {
                r.hide(), i.show({
                    mode: "exit",
                    backdropClickToClose: !1,
                    exitOptions: {
                        exit: function () {
                            o.exit()
                        }
                    }
                })
            }
        }];
        r.show({
            mode: "cornerList",
            listOb: e,
            backdropClickToClose: !1
        })
    }, t.create = function () {
        ! function () {
            var e = t.user;
            return -1 != e.email1.indexOf("@") && /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test(e.email1) ? e.email1 == e.email2 || (f.update({
                error: "emailsDontMatch"
            }), !1) : (f.update({
                error: "emailFormatError"
            }), !1)
        }() || d.register(t.user, function (e) {
            if (e.error) f.update(e);
            else {
                var t = "d_accountCreated";
                e.success && e.success.approvalRequired && (t = "d_accountCreatedApprovalRequired"), i.show({
                    mode: "success",
                    successOptions: {
                        msg: c.get(t)
                    }
                }), p.history.back()
            }
        })
    }, t.backClicked = function () {
        p.history.back()
    }
}]);

app.controller("RendererCtrl", ["$rootScope", "$ionicHistory", "modals", "nav", "backend", "library", "settings", "book", "auth", "status", "popovers", "$sce", "error", "translit", "cache", "$ionicPlatform", "bookmarks", "hlights", "notes", "$ionicSideMenuDelegate", "$scope", "$ionicScrollDelegate", "$filter", "chapters", "$ionicPopup", "$location", "$timeout", "selection", "crdv", "$ionicLoading", "stg", "search", "$ionicModal", function (s, t, l, n, r, e, o, c, i, d, a, u, f, p, h, g, m, v, b, y, w, k, x, C, I, E, T, S, A, O, B, L, N) {
    function M(e) {
        "c" == e.key && e.ctrlKey && S.copyToClipboard()
    }
    w.init = function () {
        s.renderScope = w, S.listener = w.selection, w.searchContent = {}, w.sideMenuMode = "bookSettings";
        var e = g.registerBackButtonAction(function (e) {
            kInteractive.frameIsOpen ? kInteractive.closeFrame() : kInteractive.alertIsOpen ? kInteractive.closeAlert() : kInteractive.videoIsFullscreen ? kInteractive.closeFullscreenVideo() : "default" != w.header.mode ? "selection" == w.header.mode ? S.reset() : w.applyHeader("default") : B.data.book.chapter.viewport && n.getFxlScale() != n.getFxlFit().bestFit ? n.fxlZoomFit() : "library" == config.kotobee.mode ? E.path("/library") : s.readerApp ? E.path("/app") : l.show({
                mode: "exit",
                backdropClickToClose: !1,
                exitOptions: {
                    exit: function () {
                        A.exit()
                    }
                }
            }), w.$apply()
        }, 103);
        w.$on("$destroy", e), w.native || w.desktop || w.$on("$ionicView.beforeEnter", function () {
            if (!i.authenticated) {
                var e = "loading";
                t.forwardView() ? e = t.forwardView().stateId : t.backView() && (e = t.backView().stateId), t.nextViewOptions({
                    disableAnimate: !0
                }), E.path("/" + e)
            }
        })
    }, w.setSideMenuMode = function (e) {
        w.sideMenuMode = e
    }, w.applyHeader = function (e) {
        try {
            w.header.mode = e
        } catch (e) { }
    }, w.header = {
        mode: "default"
    }, w.selection = function (e) {
        mobile ? "activated" == e || "selectionFixed" == e ? w.applyHeader("selection") : "deactivated" == e && w.applyHeader("default") : "selectionFixed" == e ? (addClass(document.getElementById("selectionOptions"), "showBar"), document.getElementById("copyBtn").setAttribute("data-clipboard-text", S.getSelectedText()), window.addEventListener("keydown", M)) : "deactivated" == e && (removeClass(document.getElementById("selectionOptions"), "showBar"), window.removeEventListener("keydown", M))
    }, w.contentLink = function (e) {
        C.applyChapterStateFromURL(e.getAttribute("href"))
    }, w.chapterLink = function (e) {
        if (0 < e.parentElement.getElementsByTagName("ol").length) {
            var t = e.offsetHeight + 20;
            if (overflowScroll || k.$getByHandle("chapters").scrollBy(0, t, !0), !e.getAttribute("href") || e.getAttribute("data-dontshow")) return void (hasClass(e.parentElement, "expanded") ? removeClass(e.parentElement, "expanded") : addClass(e.parentElement, "expanded"));
            addClass(e.parentElement, "expanded")
        }
        y.toggleLeft(!1), y.toggleRight(!1), E.search("vi");
        var n = C.getIndexFromTocElem(e);
        C.applyChapterStateFromURL(e.getAttribute("href"), {
            vIndex: 0,
            index: n
        }), w.$$phase || w.$apply()
    }, w.targetCurrentChapter = function (e) {
        C.highlightCurrentTocChapter()
    }, w.externalLink = function (e) {
        var t = e.getAttribute("href"),
            n = e.getAttribute("target");
        r.externalLink(t, {
            target: n
        })
    }, w.bookmarkClicked = function (e) {
        m.addBookmark()
    }, w.noteClicked = function (e, t) {
        b.noteClicked(e ? [e] : null, t)
    }, w.search = function (e, n, t, r) {
        if (e && e.length && 0 != e.length) {
            if (e.length < 3) return f.update({
                error: "lessThan3Characters"
            }), void l.hide();
            w.applyHeader("default"), null == t && (w.searchStack = [0]), d.update(p.get("searching"), null, {
                dots: !0,
                timeoutDuration: 6e4
            }), n || (n = "all", t = t || 0);
            var o = {
                key: e,
                mode: n,
                limit: 40,
                start: t,
                array: r
            };
            T(function () {
                L.start(o, function (e) {
                    d.update(null), w.searchContent = e;
                    for (var t = 0; t < w.searchContent.results.length; t++)
                        if (w.searchContent.results[t] && w.searchContent.results[t].matches.length) {
                            w.searchContent.notEmpty = !0;
                            break
                        }
                    "all" == n ? (w.$broadcast("scroll.infiniteScrollComplete"), overflowScroll ? document.getElementById("searchScroll").scrollTop = 0 : k.$getByHandle("searchScroll").scrollTop()) : "chapter" == n && T(function () {
                        w.applyHeader("searchItems")
                    }, 500)
                })
            }, 500)
        }
    }, w.searchMore = function () {
        for (var e, t = w.searchContent.results.length; t--;)
            if (w.searchContent.results[t] && w.searchContent.results[t].matches[0]) {
                e = w.searchContent.results[t].matches[0];
                break
            }
        e ? (w.searchStack.push(e.chapter + 1), overflowScroll ? document.getElementById("searchScroll").scrollTop = 0 : k.$getByHandle("searchScroll").scrollTop(), w.search(w.searchContent.key, "all", e.chapter + 1)) : w.searchContent.more = !1
    }, w.searchBack = function () {
        if (!(w.searchStack.length <= 1)) {
            w.searchStack.pop();
            var e = w.searchStack.pop();
            overflowScroll ? document.getElementById("searchScroll").scrollTop = 0 : k.$getByHandle("searchScroll").scrollTop(), w.search(w.searchContent.key, "all", e)
        }
    }, w.locateInChapter = function (e) {
        l.hide(), w.search(e, "chapter")
    }, w.clearResults = function () {
        w.searchContent = {}, w.applyHeader("default"), overflowScroll ? document.getElementById("searchScroll").scrollTop = 0 : k.$getByHandle("searchScroll").scrollTop()
    }, w.searchItemClickedVeryOld = function (e) {
        var r = e.item;
        e.index, e.matches;
        l.hide(), timeOut(function () {
            C.addListener(function e(t) {
                if ("newChapter" == t) {
                    C.removeListener(e);
                    var n = S.getElemsByLocation(r.location)[0];
                    timeOut(function () {
                        addClass(n, "searchItemHighlightFade"), timeOut(function () {
                            removeClass(n, "searchItemHighlightFade")
                        }, 2200)
                    }, 1200)
                }
            }), E.path(c.root + "/reader/chapter/" + r.chapter + "/" + r.location)
        }, 300)
    }, w.searchItemClicked = function (e) {
        L.searchItemClicked(e, {
            timer: !0
        }, function () {
            s.renderScope.applyHeader(""), T(function () {
                s.renderScope.applyHeader("searchItems"), s.$$phase || s.$apply()
            })
        })
    }, w.searchItemClickedOld = function (e, t) {
        var i = e.item,
            n = e.index,
            r = e.matches;

        function a(e) {
            "renderedChapter" == e && (d.update(), C.removeListener(a), r.index = n, w.applyHeader("searchItems"))
        }
        l.hide(), timeOut(function () {
            d.update(p.get("pleaseWait"), null, {
                dots: !0
            }), C.addListener(a);
            var n = i.chapter,
                r = i.location,
                o = {
                    hash: r
                };
            C.getPageOb({
                page: n
            }, function (e) {
                if (C.shouldBePaged(e)) {
                    var t = S.getPageByLocation(e, r);
                    null != t && (o.vIndex = t, E.search({
                        vi: t
                    }))
                }
                if (E.path(c.root + "/reader/chapter/" + n + "/" + r, o), e.index == currentIndex) {
                    if (o.vIndex && o.vIndex == currentVIndex) return void a("renderedChapter");
                    C.loadChapterByIndex(currentIndex, o)
                }
                s.$$phase || s.$apply()
            })
        }, 300)
    }, w.fxlZoomIn = function (e) {
        n.fxlZoomIn(e)
    }, w.fxlZoomOut = function (e) {
        n.fxlZoomOut(e)
    }, w.fxlZoomFit = function (e) {
        n.fxlZoomFit(e)
    }
}]);

app.controller("SearchItemsCtrl", ["$ionicScrollDelegate", "search", "book", "chapters", "cache", "$ionicModal", "$location", "virt", "$rootScope", "$scope", "selection", function (e, n, t, o, r, i, a, s, l, c, d) {
    c.index = 0;
    c.init = function () {
        c.index = 0, c.titleMode = 3, c.results = [];
        var e = c.searchContent.results[currentIndex];
        e && (c.results = e.matches), c.results.index && (c.index = c.results.index), 0 == c.results.length ? c.titleMode = "empty" : c.highlightCurrentItem({
            initial: !0
        }), c.$on("$destroy", function () {
            n.removeHighlights()
        })
    }, c.backClicked = function () {
        c.applyHeader("default")
    }, c.up = function () {
        0 == c.index && (c.index = c.results.length), c.index-- , c.highlightCurrentItem()
    }, c.down = function () {
        c.index == c.results.length - 1 && (c.index = -1), c.index++ , c.highlightCurrentItem()
    }, c.highlightCurrentItem = function (e) {
        var t;
        c.results && o.getPageOb({
            page: currentIndex
        }, function (e) {
            n.removeHighlights(), t = c.results[c.index].location, o.addListener(r), o.redirect(currentIndex, {
                hash: t
            })
        });

        function r(e) {
            if ("renderedChapter" == e) {
                0, o.removeListener(r), o.hideLoader();
                var t = o.getCurrentHash(),
                    n = d.getElemsByLocation(t);
                n[0] && (n[0].offsetParent ? (c.titleMode = "title", addClass(n[0], "searchItemHighlight"), s.safeScrollToElem("content", n[0], !0)) : c.titleMode = "hidden")
            }
        }
    }
}]);

app.controller("SelectionCtrl", ["$ionicScrollDelegate", "chapters", "translit", "error", "status", "modals", "notes", "$window", "$location", "$sce", "$rootScope", "backend", "hlights", "$scope", "selection", "crdv", "stg", function (e, l, r, o, i, t, a, n, s, c, d, u, f, p, h, g, m) {
    p.init = function () { }, p.tts = function () {
        if (config.tts) {
            var e = h.getSelectedText();
            if (e)
                if (desktop) o.update({
                    error: "speechUnsupportedInDesktop"
                });
                else if (preview) o.update({
                    error: "speechUnsupportedInPreview"
                });
                else if (native && "undefined" != typeof TTS) g.tts(e, function (e) { });
                else try {
                    if ("speechSynthesis" in window && SpeechSynthesisUtterance) {
                        i.showAndHide(r.get("preparingVoice"), 600, {
                            dots: !0
                        }), window.speechSynthesis.cancel();
                        var t = new SpeechSynthesisUtterance,
                            n = window.speechSynthesis.getVoices();
                        if (t.lang = config.defaultTtsLanguage ? config.defaultTtsLanguage : "en-US", t.voice = n.filter(function (e) {
                            return e.lang == t.lang
                        })[0], !t.voice) return;
                        t.text = e, window.speechSynthesis.speak(t)
                    } else o.update({
                        error: "speechUnsupported"
                    })
                } catch (e) {
                    o.update({
                        error: "speechUnsupported"
                    })
                }
        }
    }, p.copy = function () {
        h.copyToClipboard()
    }, p.share = function () {
        if (config.share) {
            var e = h.getSelectedText();
            e && g.share(e)
        }
    }, p.note = function (e) {
        if (config.annotations) {
            var t, n, r = h.getLocation(),
                o = [];
            r.left && ((t = {
                type: "content"
            }).location = r.left, n = h.getElemsByLocation(t.location), t.chapter = l.getElemChapterIndex(n[0]), t.src = h.getSelectedText(n), t.event = e, o.push(t)), r.right && ((t = {
                type: "content"
            }).location = r.right, n = h.getElemsByLocation(t.location), t.chapter = l.getElemChapterIndex(n[0]), t.src = h.getSelectedText(n), t.event = e, o.push(t)), a.noteClicked(o, function (e) {
                h.reset()
            })
        }
    }, p.highlight = function () {
        if (config.annotations) {
            var e, t = h.getSelectedNodes(),
                n = h.getLocation(),
                r = [];
            n.left && ((e = {}).location = n.left, t = h.getElemsByLocation(e.location), e.chapter = l.getElemChapterIndex(t[0]), e.src = h.getSelectedText(t), r.push(e)), n.right && ((e = {}).location = n.right, t = h.getElemsByLocation(e.location), e.chapter = l.getElemChapterIndex(t[0]), e.src = h.getSelectedText(t), r.push(e));
            for (var o = 0; o < t.length; o++)
                if (hasClass(t[o], "contentHlightBG")) {
                    for (var i = getClassThatStartsWith(t[o], "hlight"), a = 0; a < r.length; a++) r[a].hid = i.substr(6);
                    return void m.getHlightById(r[0].hid, function (e) {
                        for (var t = 0; t < r.length; t++) r[t].color = e.color;
                        s()
                    })
                }
            s()
        }

        function s() {
            f.showPopup(r, function (e) {
                h.reset()
            })
        }
    }, p.google = function () {
        if (config.googleLookup) {
            var e = h.getSelectedText();
            if (e) try {
                var t = "http://www.google.com/custom?q=" + e;
                return void u.externalLink(t, {
                    secure: !0
                })
            } catch (e) { }
        }
    }, p.wikipedia = function () {
        if (config.wikipediaLookup) {
            var e = h.getSelectedText();
            if (e) try {
                var t = "https://" + (config.wikipediaLang ? config.wikipediaLang : "en") + ".wikipedia.org/wiki/" + e;
                u.externalLink(t, {
                    secure: !0
                })
            } catch (e) { }
        }
    }, p.backClicked = function () {
        h.reset()
    }
}]);

app.controller("SelectionOptionsCtrl", ["$scope", "$http", "stg", "$ionicScrollDelegate", "$ionicPopup", "$timeout", "selection", function (e, t, n, r, o, i, a) {
    e.init = function () { }, e.expand = function () {
        a.expand()
    }, e.extendRight = function () {
        a.extendRight()
    }, e.extendLeft = function () {
        a.extendLeft()
    }
}]);

app.controller("TabMenuCtrl", ["$scope", "cache", "modals", "nav", "settings", "book", "notes", "$rootScope", "$ionicSideMenuDelegate", "library", "$stateParams", "$location", "$sce", "$ionicScrollDelegate", "chapters", "virt", "translit", "$ionicModal", "hlights", "$timeout", "$filter", "stg", "crdv", "selection", function (t, e, n, r, o, i, a, c, s, l, d, u, f, p, h, g, m, v, b, y, w, k, x, C) {
    t.init = function () {
        t.$watch(function () {
            return s.isOpenRight()
        }, function (e) {
            e || y(function () {
                t.setSideMenuMode("bookSettings")
            }, 100)
        })
    }, t.chaptersClicked = function () {
        h.displayChapters()
    }, t.mediaClicked = function () {
        if (k.data.settings.rtl) {
            if (s.isOpenLeft() && "media" != t.sideMenuMode) return s.toggleLeft(), void y(t.mediaClicked, 300)
        } else if (s.isOpenRight() && "media" != t.sideMenuMode) return s.toggleRight(), void y(t.mediaClicked, 300);
        native && x.vibrate(), t.setSideMenuMode("media"), mobile && r.resize(), k.data.settings.rtl ? s.toggleLeft() : s.toggleRight()
    }, t.notebookClicked = function () {
        c.widescreen ? t.chaptersClicked() : (native && x.vibrate(), a.updateNotebookArray({}, function () {
            overflowScroll || r.resize(), n.show({
                mode: "notebook",
                dynamic: !0,
                scope: t,
                animation: "slide-in-up",
                cb: function () {
                    overflowScroll ? document.getElementById("notebookContent").scrollTop = 0 : p.$getByHandle("notebookScroll").scrollTop()
                }
            })
        }))
    }, t.pdfClicked = function () {
        h.pdfPrint()
    }, t.printClicked = function () {
        h.print()
    }, t.settingsClicked = function () {
        if (k.data.settings.rtl) {
            if (s.isOpenLeft() && "bookSettings" != t.sideMenuMode) return s.toggleLeft(), void y(t.settingsClicked, 300)
        } else if (s.isOpenRight() && "bookSettings" != t.sideMenuMode) return s.toggleRight(), void y(t.settingsClicked, 300);
        native && x.vibrate(), t.setSideMenuMode("bookSettings"), mobile && r.resize(), k.data.settings.rtl ? s.toggleLeft() : s.toggleRight()
    }, t.searchClicked = function () {
        native && x.vibrate(), mobile && r.resize(), n.show({
            mode: "search",
            dynamic: !0,
            animation: "slide-in-up",
            scope: t,
            cb: function () {
                t.searchContent.notEmpty || y(function () {
                    x.showKeyboard()
                }, 200)
            }
        })
    }, t.settingsClose = function () {
        n.hide(), k.writeSlot("settings" + k.getBookId(!0), t.data.settings)
    };
    var I = [];
    t.customTabClicked = function (e, t) {
        var n = document.createElement("div");
        if (e.relToRoot = !0, "audio" == e.type) {
            var r, o = ph.join(bookPath, e.audio);
            e.id || (e.id = Math.round(1e4 * Math.random()));
            for (var i = 0; i < I.length; i++)
                if (I[i].getAttribute("id") == "audio-" + e.id && !(r = I[i]).paused) return r.pause(), void (r.src = "");
            if (!r) {
                (r = document.createElement("audio")).setAttribute("data-dontclose", !0), r.setAttribute("id", "audio-" + e.id), r.setAttribute("controls", "true"), r.setAttribute("autoplay", "true"), r.setAttribute("data-tap-disabled", "false");
                var a = document.createElement("source");
                a.src = o, r.appendChild(a), r.appendChild(document.createTextNode("Your browser does not support the audio element")), r.className = "ki-noHighlight", r.oncanplay = function () {
                    kInteractive.tinCan({
                        verb: "played",
                        activity: "Audio: " + o
                    })
                }
            }
            if (r.setAttribute("src", o), r.play(), I.push(r), kInteractive.scorm) {
                var s = {};
                s.id = kInteractive.getScormId(e.title, o), s.description = "Played audio link: " + o, s.type = "other", s.learnerResponses = "Played", s.objective = e.options ? e.options.objective : null, s.timestamp = new Date, kInteractive.scorm.setInteractions([s])
            }
        } else {
            kInteractive.writeData(n, e);
            var l = e.url;
            if (e.hash && (l += e.hash), n.className = "kInteractive link", !l) return kInteractive.action(n, t), void t.stopPropagation();
            n.setAttribute("href", l), e.target && "_blank" == e.target.toLowerCase() && n.setAttribute("target", "_BLANK"), l.match(/http[s]?:/g) ? c.renderScope.externalLink(n) : 0 == l.indexOf("/") ? window.open(l) : 0 == l.indexOf("mailto:") ? window.location.href = l : (0 != l.indexOf("#") && (l = ph.join(k.data.book.chapter.url, l)), h.applyChapterStateFromURL(l))
        }
    }
}]);

app.controller("TextSizeCtrl", ["$scope", "$location", "cssParser", "stg", "settings", "markers", "$injector", function (t, e, n, r, o, i, a) {
    function s() {
        "rfl" == r.data.book.chapter.layout && ("double" != r.data.book.chapter.view && r.data.settings.pageScroll || a.get("chapters").regenerateSpreads(null, function () { }))
    }
    t.backClicked = function () {
        t.applyHeader("default")
    }, t.plus = function () {
        var e = o.getTextSize();
        30 < e || (e = Math.round(10 * (e + .1)) / 10, t.data.settings.textSize = e + "em", o.updateTextSize(), o.writeTextSize(), s())
    }, t.minus = function () {
        var e = o.getTextSize();
        e <= .2 || (e = Math.round(10 * (e - .1)) / 10, t.data.settings.textSize = e + "em", o.updateTextSize(), o.writeTextSize(), s())
    }
}]);

app.controller("ThumbCtrl", ["$scope", "$filter", "$rootScope", "np", "$element", "stg", "$ionicModal", "modals", "chapters", "backend", "book", "$location", "$timeout", "$ionicLoading", "$ionicPopup", "crdv", function (n, e, t, r, o, i, a, s, l, c, d, u, f, p, h, g) {
    n.init = function () {
        if (n.book.coverImg = d.getCoverImgPath(n.book), chromeApp)
            if (i.data.chromeAssets[n.book.coverImg]) n.book.coverImg = i.data.chromeAssets[n.book.coverImg];
            else {
                var t = n.book.coverImg;
                delete n.book.coverImg, getImgDataSrc(t, function (e) {
                    n.book.coverImg = e, i.data.chromeAssets[t] = e, n.$apply()
                })
            }
    }
}]);

app.filter("bookSetup", ["$rootScope", "stg", "selection", "media", "chapters", "cache", "$ionicScrollDelegate", function (c, d, r, u, f, n, e) {
    return function () {
        var e = document.getElementById("chaptersContent"),
            l = document.getElementById("epubContainer");

        function t(e) {
            r.active() && r.reset();
            var t = e.target;
            if ("sections" != t.nodeName.toLowerCase() && !hasClass(t, "k-section") && "video" != t.nodeName.toLowerCase() && "audio" != t.nodeName.toLowerCase()) {
                if (t.hasAttribute("contenteditable")) return e.stopPropagation(), void t.focus();
                for (var n = t.childNodes.length; n--;)
                    if ("#text" == t.childNodes[n].nodeName && "" != t.childNodes[n].nodeValue.replace(/\s+/g, " ").trim()) return void r.elementDown(t, e)
            }
        }
        n.setOb("epubContainer", l), n.setOb("chaptersContent", e), document.getElementById("chapters").addEventListener("click", function (e) {
            var t = e.target;
            if ("a" == t.nodeName.toLowerCase()) {
                if (!r(t)) return;
                var n = t.getAttribute("href");
                if (!n) return;
                if (n.match(/http[s]?:/g)) return;
                if (0 == n.indexOf("/")) return;
                c.renderScope.chapterLink(t)
            } else if ("i" == t.nodeName.toLowerCase()) {
                if (!r(t)) return;
                if (0 < t.parentElement.getElementsByTagName("ol").length) t.offsetHeight, t.parentElement.classList.toggle("expanded")
            }

            function r(e) {
                for (var t = e.parentNode; t && t != document.body;) {
                    if (hasClass(t, "menu") && (hasClass(t, "menu-left") || hasClass(t, "menu-right"))) return !0;
                    t = t.parentNode
                }
            }
        }), l.addEventListener("touchstart", t), l.addEventListener("mousedown", t), l.addEventListener("click", function (e) {
            var t = e.target;
            if (t) {
                for (var n = t; n && n != l;) {
                    if ("a" == n.nodeName.toLowerCase()) {
                        if (hasClass(n, "ki-btn")) kInteractive.action(n, e);
                        else {
                            var r = n.getAttribute("href");
                            if (!r) return kInteractive.action(n, e), void e.stopPropagation();
                            r.match(/http[s]?:/g) ? c.renderScope.externalLink(n) : 0 == r.indexOf("/") ? window.open(r) : 0 == r.indexOf("mailto:") ? window.location.href = r : (0 != r.indexOf("#") && (r = ph.join(d.data.book.chapter.url, r)), f.applyChapterStateFromURL(r))
                        }
                        return void e.stopPropagation()
                    }
                    n = n.parentNode
                }
                if ("video" != t.nodeName.toLowerCase() && "audio" != t.nodeName.toLowerCase()) {
                    if ("img" == t.nodeName.toLowerCase()) {
                        if (hasClass(t, "kInteractive") && "none" != kInteractive.readData(t).behavior) return kInteractive.action(t, e), void e.stopPropagation();
                        var o = t.parentNode;
                        if (hasClass(o, "kInteractive")) {
                            if (hasClass(o, "widget")) return kInteractive.action(t, e), void e.stopPropagation();
                            if (hasClass(o, "questions")) return kInteractive.action(t, e), void e.stopPropagation()
                        }
                        var i = !0,
                            a = kInteractive.readData(t);
                        if (a && a.popup || (i = !1), t.hasAttribute("static") && (i = !1), t.hasAttribute("data-static") && (i = !1), i) {
                            var s = {
                                thumbnail: t.getAttribute("src"),
                                title: t.getAttribute("alt")
                            };
                            return u.show(s), void e.stopPropagation()
                        }
                    }
                    return hasClass(t, "ki-btn") ? (kInteractive.action(t, e), void e.stopPropagation()) : hasClass(t, "kInteractive") ? (kInteractive.action(t, e), void e.stopPropagation()) : t.hasAttribute("contenteditable") ? (t.focus(), void e.stopPropagation()) : void ("input" != t.nodeName.toLowerCase() || e.stopPropagation())
                }
            }
        })
    }
}]);

app.filter("chapterHtml", ["$injector", function (d) {
    return function (e) {
        if (e) {
            for (var t = e.getElementsByTagName("li"), n = t.length; n--;) {
                addClass(t[n], "item");
                var r = t[n].getElementsByTagName("ol").length ? " parent" : "";
                t[n].innerHTML = '<i class="icon' + r + '"> </i> ' + t[n].innerHTML
            }
            e.innerHTML = e.innerHTML.replace(/(<a .*?)(>)/g, '$1 onclick="return false;" $2');
            var o = d.get("book"),
                i = d.get("chapters");
            if (o.allowedChapters) {
                var a = e.getElementsByTagName("a");
                for (n = 0; n < o.allowedChapters.length; n++) {
                    var s = o.allowedChapters[n],
                        l = "";
                    isNaN(s) ? s : (s = Number(s), l = i.getChapterURL(s));
                    for (var c = 0; c < a.length; c++) l == a[c].getAttribute("href") && a[c].setAttribute("data-allow", !0)
                }
                for (c = 0; c < a.length; c++) a[c].hasAttribute("data-allow") ? a[c].removeAttribute("data-allow") : (a[c].className = "restricted", a[c].setAttribute("href", ""))
            }
        }
    }
}]);

app.filter("contentHtml", ["$rootScope", "$filter", "selection", "stg", function (e, t, n, T) {
    return function (e, t) {
        var n = document.createElement("div");
        n.setAttribute("id", "epubContent"), t = t || T.data.book.chapter.absoluteURL;
        for (var r = e.getElementsByTagName("body")[0], o = [], i = 0; i < r.childNodes.length; i++) 3 == r.childNodes[i].nodeType && o.push(r.childNodes[i]);
        for (i = 0; i < o.length; i++) {
            var a = o[i],
                s = document.createElement("span");
            s.textContent = a.textContent, a.nextSibling ? r.insertBefore(s, a.nextSibling) : r.appendChild(s), r.removeChild(a)
        }
        kInteractive && kInteractive.preRender(e, {
            chapterUrl: t
        });
        var l = getElementsByAttribute(e, "*", "src");
        for (i = 0; i < l.length; i++) {
            if (shouldParse(h = l[i].getAttribute("src")) && l[i].setAttribute("src", ph.join(t, h)), "img" == l[i].nodeName.toLowerCase() && (hasClass(l[i], "kInteractive") || hasClass(l[i].parentNode, "kInteractive") || l[i].style.height || l[i].getAttribute("height") || (l[i].style.maxWidth = "100%"), chromeApp && l[i].getAttribute("src"))) {
                var c = l[i],
                    d = c.getAttribute("src");
                if (isAbsoluteURLAndNotDataPath(d)) {
                    var u = c.getAttribute("chrome");
                    T.data.chromeAssets[d] ? c.setAttribute("src", T.data.chromeAssets[d]) : u && T.data.chromeAssets[u] ? c.setAttribute("src", T.data.chromeAssets[u]) : (u = Math.round(1e5 * Math.random()), c.setAttribute("src", ""), c.setAttribute("chrome", u), getImgDataSrc(d, function (t) {
                        setTimeout(function () {
                            T.data.chromeAssets[u] = t;
                            var e = getElementsByAttribute(document, "img", "chrome", u);
                            e.length && e[0].setAttribute("src", t)
                        }, 300)
                    }))
                }
            }
        }
        var f = getElementsByAttribute(e, "*", "poster");
        for (i = 0; i < f.length; i++) {
            shouldParse(h = f[i].getAttribute("poster")) && f[i].setAttribute("poster", ph.join(t, h))
        }
        var p = getElementsByAttribute(e, "image", "xlink:href");
        for (i = 0; i < p.length; i++) {
            var h;
            shouldParse(h = p[i].getAttribute("xlink:href")) && p[i].setAttribute("xlink:href", ph.join(t, h))
        }
        var g = getElementsByAttribute(e, "*", "style");
        for (i = 0; i < g.length; i++) {
            var m = g[i].getAttribute("style"); - 1 != m.toLowerCase().indexOf("url(") && (hasParent(g[i], "kInteractive", 5) || g[i].setAttribute("style", m.replace(/(.*?url\()(.+?)(\).*?)/g, v)))
        }

        function v() {
            var e = arguments[2];
            return 0 == e.indexOf('"') && (e = e.split('"')[1]), 0 == e.indexOf("'") && (e = e.split("'")[1]), 0 == e.indexOf("&quot;") && (e = e.split("&quot;")[1]), -1 != e.indexOf("http://") ? arguments[0] : -1 != e.indexOf("https://") ? arguments[0] : 0 == e.indexOf("data:") ? arguments[0] : (e = ph.join(t, e), arguments[1] + e + arguments[3])
        }
        var b = document.createElement("sections"),
            y = e.getElementsByTagName("body")[0];
        if (partitionEnabled) {
            for (var w = new Array, k = 0; ;) {
                var x = !0,
                    C = !1,
                    I = 0;
                w[k] = document.createElement("section"), w[k].setAttribute("sec", k), addClass(w[k], "k-section");
                for (i = y.childNodes.length; i--;)
                    if (!y.childNodes[i].style || "absolute" != y.childNodes[i].style.position) {
                        if (!((I += (null != y.childNodes[i].textContent ? y.childNodes[i].textContent : y.childNodes[i].data).length) < 4e3 || x)) {
                            C = !0;
                            break
                        }
                        x = !1, w[k].insertBefore(y.childNodes[i], w[k].firstChild)
                    }
                if (!C) break;
                k++
            }
            for (i = 0; i < w.length; i++) b.insertBefore(w[i], b.firstChild)
        } else {
            var E = document.createElement("section");
            E.setAttribute("sec", "0"), addClass(E, "k-section");
            for (i = y.childNodes.length; i--;) E.insertBefore(y.childNodes[i], E.firstChild);
            b.appendChild(E)
        }
        return y.appendChild(b), n.innerHTML = e.getElementsByTagName("body")[0].innerHTML, pauseVideoAndAudio(n), n
    }
}]);

app.filter("contentJs", ["$rootScope", "cache", "cssParser", "$filter", "selection", "stg", "$q", "settings", "chapters", "bookmarks", "hlights", "notes", function (e, s, t, n, r, l, c, d, o, u, f, p) {
    return function (e) {
        var o, i = c.defer(),
            t = s.getOb("epubContent");

        function a() {
            function e() {
                if (t++ , "#e7e3d9" == rgb2hex(window.getComputedStyle(s.getOb("epubContent")).getPropertyValue("background-color"))) {
                    clearInterval(r), removeClass(o, "hide");
                    for (var e = document.getElementsByTagName("style"), t = 0; t < e.length; t++)
                        if (e[t].hasAttribute("kotobee")) {
                            var n = e[t].innerHTML;
                            n = n.replace(/[^}]*?{[^}]*?background-color:#e7e3d9 !important;.*?}/g, ""), e[t].innerHTML = n
                        }
                    d.initialize(), i.resolve({})
                }
            }
            removeClass(s.getOb("epubContent"), "bgInherit");
            var r = setInterval(e, 30);
            e()
        }
        return document.getElementById("epubMeta") ? (document.getElementById("epubMeta").innerHTML = "", o = document.getElementById("epubMeta")) : ((o = document.createElement("div")).setAttribute("id", "epubMeta"), o.className = "hide", t.parentNode.insertBefore(o, t.nextSibling)), s.setOb("epubMeta", o), r.newChapter(), e ? l.getBookmarks(function (r) {
            l.getNotes(function (n) {
                l.getHlights(function (e) {
                    for (var t = 0; t < r.length; t++) r[t].chapter == currentIndex && u.addIcon(r[t]);
                    for (t = 0; t < n.length; t++) n[t].chapter == currentIndex && p.addIcon(n[t]);
                    for (t = 0; t < e.length; t++) e[t].chapter == currentIndex && f.addHlight(e[t]);
                    a()
                })
            })
        }) : a(), i.promise
    }
}]);

app.filter("contentVirt", ["stg", "bookmarks", "notes", "workers", "hlights", "$timeout", "$filter", "cache", "$ionicScrollDelegate", function (C, I, E, T, S, A, e, O, t) {
    var B, L, N, M, P = [];
    return function (e, t, c) {
        try {
            function n(e) {
                if (e) {
                    for (var t = P.length; t--;)
                        if (P[t] == e) return;
                    var n = O.getOb("epubContent").className;
                    addClass(O.getOb("epubContent"), "reveal");
                    var r = e.offsetTop - O.getOb("epubContent").offsetTop;
                    0 != r && (e.style.marginTop = r + "px", P.push(e), O.getOb("epubContent").className = n)
                }
            }
            var d = [];

            function r(e, t) {
                (t = t || {}).id = Math.round(9999 * Math.random());
                t.cb;
                if (null == t.includeAnnotations && (t.includeAnnotations = !0), hasClass(e, "parsed")) return t.cb && t.cb(e), e;
                addClass(e, "parsed");
                var n = e.innerHTML;
                if (T.supported() && t.allowWorkers) {
                    var o = {};
                    return d.push({
                        html: n,
                        el: e,
                        options: t
                    }), 1 == d.length && function r() {
                        o.html = d[0].html;
                        T.call("htmlParser", "parse", [o], function (e) {
                            var t = replaceHTML(d[0].el, e),
                                n = d[0].options;
                            n.includeAnnotations && u(t), d.shift(), n.cb && n.cb(t), d.length && r()
                        })
                    }(), !0
                }
                var r, i = [];
                n.indexOf("<pre"), n.indexOf("<code"), n.indexOf("<script");
                i.push(/([< \/][^>]*?>)((\s*[^<\s]+\s+?)+)([^<\s]+\s*)(<)/g), r = /(>)([^<\n]*?[^<]+?)(<[^\/])/g;
                for (var a = 0; a < i.length; a++) {
                    var s = i[a];
                    n = n.replace(s, function (e, t) {
                        if (0 <= t.indexOf('class="parsed"')) return e;
                        if (0 == t.indexOf("<pre")) return e;
                        if (0 == t.indexOf("<code")) return e;
                        if (0 == t.indexOf("<script")) return e;
                        var n = "",
                            r = arguments[2].split(" ");
                        "" == r[r.length - 1] && r.splice(-1, 1), r.push(arguments[4]);
                        for (var o = 0; o < r.length; o++) {
                            var i = o == r.length - 1 ? "" : " ";
                            n += "<span>" + r[o] + i + "</span>"
                        }
                        return t + n + "<"
                    })
                }
                r && (n = n.replace(r, function (e, t) {
                    return arguments[2].trim() ? t + ("<span>" + arguments[2] + "</span>") + arguments[3] : e
                }));
                var l = replaceHTML(e, n = n.replace(/(<a [\s\S]*?)(>)/g, '$1 onclick="return false;" $2'));
                return t.includeAnnotations && u(l), c && c(), l
            }

            function u(e) {
                for (var i = 0, t = e; null != (t = t.previousSibling);) i++;
                var a = [currentIndex];
                C.data.currentRPageOb && a.push(C.data.currentRPageOb.index), A(function () {
                    C.getBookmarks(function (o) {
                        C.getNotes(function (r) {
                            C.getHlights(function (e) {
                                for (var t = 0; t < a.length; t++) {
                                    for (var n = 0; n < o.length; n++) o[n].chapter == a[t] && String(o[n].location).split(".")[1] == i && I.addIcon(o[n]);
                                    for (n = 0; n < r.length; n++) r[n].chapter == a[t] && String(r[n].location).split(".")[1] == i && E.addIcon(r[n]);
                                    for (n = 0; n < e.length; n++) e[n].chapter == a[t] && String(e[n].location).split(".")[1] == i && S.addHlight(e[n])
                                }
                            })
                        })
                    })
                }, 1e3)
            }
            if ("reset" == e) return B = L = null, N = !0, void (P = []);
            var o = O.getOb("epubContent");
            if ("reveal" == e) return void addClass(o, "reveal");
            if ("undoReveal" == e) return void removeClass(o, "reveal");
            if ("parse" == e) return void r(t, {
                includeAnnotations: !1,
                allowWorkers: !0,
                source: "parse mode",
                cb: c
            });
            if ("parseAndAnnotations" == e) return void r(t, {
                allowWorkers: !0,
                source: "parseAndAnnotations mode"
            });
            var i, a, s, l = o.parentNode.parentNode,
                f = l.style.webkitTransform || l.style.mozTransform || l.style.transform,
                p = (i = null == e && null != t ? t : !overflowScroll || !config.overflowScroll && preview && mobile ? -Number(f.split(",")[1].slice(0, -2)) : O.getOb("epubContainer").scrollTop) < M ? "up" : "down",
                h = (M = i) + l.parentNode.offsetHeight,
                g = o.children;
            if (N) {
                for (var m = [], v = 0, b = 0; b < g.length; b++) {
                    for (var y = [], w = g[b], k = 0; k < w.childNodes.length; k++) y[k] = {}, w.childNodes[k].offsetTop + w.childNodes[k].offsetHeight < i - 200 ? y[k].add = ["cNone", "noMargin"] : w.childNodes[k].offsetTop > h + 200 ? y[k].add = ["cNone", "noMargin"] : (0 == v && (y[k].setMargin = !0, v = 1, a = k), y[k].remove = ["cNone", "noMargin"], y[k].setParse = !0, s = k);
                    for (k = 0; k < y.length; k++) y[k].setMargin && n(w.childNodes[k]), y[k].setParse && m.push([w.childNodes[k], {
                        source: "from clean",
                        allowWorkers: !0
                    }]), addRemoveClass(w.childNodes[k], y[k].add, y[k].remove);
                    for (k = 0; k < y.length; k++);
                    null != a && (B = w.childNodes[a]), null != s && (L = w.childNodes[s])
                }
                if (N = !1, m.length)
                    for (k = 0; k < m.length; k++) {
                        var x = m[k];
                        k == m.length - 1 && (x[1].cb = c), r(x[0], x[1])
                    } else c && c();
                return
            }
            if ("fxl" == C.data.book.chapter.layout) return;
            if (1 < g.length) return;
            if (1 == g[0].childNodes.length) return;
            w = g[0];
            if ("up" == p) {
                if (!(B.offsetTop + B.offsetHeight) < i - 0 && !(B.offsetTop > i - 200)) return;
                if (L == w.childNodes[0]) return
            } else if ("down" == p) {
                if (!(L.offsetTop > h + 0 || L.offsetTop + L.offsetHeight < h + 200)) return;
                if (B == w.childNodes[w.childNodes.length - 1]) return
            }
            B && (B.offsetTop + B.offsetHeight < i - 0 ? (addClass(B, "cNone"), addClass(B, "noMargin"), B.nextSibling && (removeClass(B = B.nextSibling, "noMargin"), n(B), B = r(B, {
                source: "topElem1"
            }))) : B.offsetTop > i - 200 && B.previousSibling && (addClass(B, "noMargin"), removeClass(B = B.previousSibling, "cNone"), removeClass(B, "noMargin"), n(B), B = r(B, {
                source: "topElem2"
            }))), L && (L.offsetTop > h + 0 ? (addClass(L, "cNone"), L.previousSibling && (removeClass(L = L.previousSibling, "cNone"), L = r(L, {
                source: "bottomElem1"
            }))) : L.offsetTop + L.offsetHeight < h + 200 && L.nextSibling && (removeClass(L = L.nextSibling, "cNone"), L = r(L, {
                source: "bottomElem2"
            })))
        } catch (e) { }
    }
}]);

app.filter("js", ["$rootScope", "$q", "jsParser", function (e, l, c) {
    return function (e, t) {
        var n = l.defer();
        t = t || {};
        for (var r = [], o = e.getElementsByTagName("script"), i = 0; i < o.length; i++)
            if (o[i].getAttribute("src")) {
                var a = o[i].getAttribute("src");
                if (0 <= a.indexOf("/kotobeeInteractive.js")) continue;
                a.indexOf(":/"), r.push({
                    mode: "url",
                    val: a
                })
            } else r.push({
                mode: "code",
                val: o[i].innerHTML
            });
        var s = e.getElementsByTagName("body")[0];
        return s.getAttribute("onload") && r.push({
            mode: "code",
            val: s.getAttribute("onload")
        }), t.noJsParse ? n.resolve() : c.inject(r, function (e) {
            n.resolve(e)
        }), n.promise
    }
}]);

app.filter("coverPath", ["stg", function (r) {
    return function (e, t) {
        var n = (config.kotobee.liburl ? config.kotobee.liburl : "") + "img/ui/defaultCover.png";
        return e ? isAbsoluteURLPath(e) ? e : t ? (config.kotobee.liburl ? config.kotobee.liburl : "../../") + "books/" + t + "/EPUB/" + e : ph.join(bookPath, r.data.packageURL, e) : n
    }
}]);

app.filter("categoryStyle", ["stg", function (e) {
    return function (e) {
        var t = "";
        isKotobeeHosted() && (t = publicRoot);
        var n = "";
        "image" != e.display && "imagetext" != e.display || !e.img || (n += "background-image:url(" + (t + "imgUser/" + e.img) + ");");
        return "image" == e.display && (n += "color:transparent;"), e.css && (n += e.css), n
    }
}]);

app.filter("catImgPath", ["stg", function (e) {
    return function (e) {
        var t = (config.kotobee.liburl ? config.kotobee.liburl : "") + "imgUser/" + e;
        return config.kotobee.liburl + "/" + ph.join(bookPath, t)
    }
}]);

app.filter("decodeHtmlEntities", function () {
    return function (e) {
        return decodeHTMLEntities(e)
    }
});

app.filter("nativeLinks", function () {
    return function (e) {
        var t = document.createElement("div");
        t.innerHTML = e;
        for (var n = t.getElementsByTagName("a"), r = 0; r < n.length; r++) {
            var o = n[r].getAttribute("href");
            n[r].setAttribute("onclick", "window.open('" + o + "', '_system');"), n[r].setAttribute("href", "#")
        }
        return t.innerHTML
    }
});

app.filter("moreDataExists", ["stg", function (r) {
    return function (e, t) {
        try {
            if (!r.currentLibrary) return !1;
            if ("categories" == t) return !1;
            if ("downloaded" == t) return !1;
            var n;
            if ("favorites" == t && (n = r.favorites), "all" == t && (n = r.currentLibrary.books), 0 == n.length) return !1
        } catch (e) {
            return !1
        }
        return e
    }
}]);

app.filter("round", function () {
    return function (e) {
        return Math.round(e)
    }
});

app.filter("arrayEmpty", ["$filter", function (n) {
    return function (e) {
        if (!e) return !0;
        for (var t = 0; t < e.length; t++)
            if (Array.isArray(e[t])) {
                if (!n("arrayEmpty")(e[t])) return !1
            } else if (e[t]) return !1;
        return !0
    }
}]);

app.filter("langFromId", ["stg", function (n) {
    return function (e) {
        for (var t = 0; t < n.data.languages.length; t++)
            if (n.data.languages[t].val == e) return n.data.languages[t].label;
        return e
    }
}]);

app.filter("unsafe", ["$sce", function (e) {
    return e.trustAsHtml
}]);

app.filter("nl2br", function () {
    return function (e) {
        return e ? 0 <= e.indexOf("</") ? e : e.replace(/\n/g, "<br>") : ""
    }
});

app.filter("releasePlatform", function () {
    return function (e) {
        switch (e) {
            case "win32":
                return "Windows 32-bit";
            case "win64":
                return "Windows 64-bit";
            case "win":
                return "Windows";
            case "mac":
                return "Macintosh"
        }
    }
});

app.filter("styles", ["$rootScope", "$q", "cssParser", "$filter", "selection", "stg", function (e, u, f, t, n, r) {
    return function (e, t) {
        var n = u.defer();
        t = t || {};
        for (var r, o = [], i = e.getElementsByTagName("link"), a = 0; a < i.length; a++)
            if ("text/css" == i[a].getAttribute("type") && i[a].getAttribute("href").length) {
                var s = ph.join(t.path, i[a].getAttribute("href")),
                    l = !1;
                if (isAbsoluteURLPath(i[a].getAttribute("href")) && (s = i[a].getAttribute("href"), l = !0), c(s)) continue;
                s == bookPath + "EPUB/css/base.css" && (r = !0), o.push({
                    type: "path",
                    val: s,
                    dontCache: l
                })
            }
        function c(e) {
            if (0 <= e.indexOf("/css/kotobeeInteractive.css")) return !0
        }
        r || o.unshift({
            type: "path",
            val: bookPath + "EPUB/css/base.css",
            dontCache: !1
        });
        var d = e.getElementsByTagName("style");
        for (a = 0; a < d.length; a++) o.push({
            type: "local",
            val: d[a].innerHTML,
            root: t.path
        });
        return t.noCssParse ? n.resolve() : f.inject(o, t, function (e) {
            n.resolve(e)
        }), n.promise
    }
}]);

app.filter("t", ["translit", function (n) {
    return function (e, t) {
        return n.getLang() ? n.get(e, Array.prototype.slice.call(arguments, 2)) : ""
    }
}]);

app.factory("app", ["$q", "backend", "modals", "$location", "sync", "$ionicHistory", "book", "chapters", "$compile", "translit", function (e, t, n, r, o, i, a, s, l, c) {
    return {}
}]);

app.factory("auth", ["$q", "backend", "modals", "$location", "sync", "$ionicHistory", "status", "stg", "book", "view", "chapters", "app", "$compile", "translit", function (t, o, i, n, r, a, s, e, l, c, d, u, f, p) {
    var h = {};

    function g() { }
    return h.authenticate = function () {
        var e = t.defer();
        return putTest("inside"), h.getPermissions(function (e) {
            c.generate();
            try {
                if ("book" == config.kotobee.mode && isKotobeeHosted())
                    if (config.kotobee.public) bookPath = publicRoot + "epub/";
                    else {
                        var t = publicRoot.split("/books/")[1].split("/")[0];
                        bookPath = "/books/" + t + "/EPUB/epub/"
                    }
                if ((native || desktop) && e && o.cloudParam("offline") && "offline" == e.error) return void h.startup({
                    tab: "downloads"
                })
            } catch (e) { }
            o.cloudParam("public") || o.cloudParam("entry") ? h.startup() : o.login().then(function (e) {
                e.error ? (i.hide(), n.path("/login")) : h.startup()
            })
        }), e.promise
    }, h.getPermissions = function (n) {
        "library" == config.kotobee.mode ? o.getUnauthLibrary(function (e) {
            if ("offline" != (e = e || {}).error) {
                if (e.cloud)
                    for (var t in e.cloud) config.kotobee[t] = e.cloud[t];
                config.kotobee.permsLoaded = !0, g(), n()
            } else n(e)
        }) : "book" == config.kotobee.mode ? config.kotobee.cloudid ? o.getPermissions(function (e) {
            for (var t in e) config.kotobee[t] = e[t];
            config.kotobee.permsLoaded = !0, g(), n()
        }) : (config.kotobee.public = !0, g(), n()) : u.getPermissions(n)
    }, h.startup = function (e) {
        if (e = e || {}, native && function () {
            if ({}.watermarkPlaceholder) {
                var e = '<div id="watermark"><span>' + p.get("createdUsing") + '</span><a href="#" class="inlineBlock" style="vertical-align: sub" onclick="window.open(\'https://www.kotobee.com\', \'_system\');return false;"><img width="163" src="' + rootDir + 'img/ui/authorLogo.png"></a></div>',
                    t = document.createElement("div");
                t.innerHTML = e, document.body.insertBefore(t, document.getElementById("html5Fallback")), setTimeout(function () {
                    addClass(document.getElementById("watermark"), "show")
                }, 4e3)
            }
        }(), "library" == config.kotobee.mode) {
            h.authenticated = !0;
            var t = "";
            e.tab && (t = "/tab/" + e.tab);
            try {
                n.path("/library" + t)
            } catch (e) { }
            a.clearHistory()
        } else "book" == config.kotobee.mode ? l.loadBook().then(function () {
            h.authenticated = !0, n.path("/reader"), a.clearHistory(), r.initialize(), setTimeout(function () {
                d.initialize()
            }, 1e3)
        }) : "app" == config.kotobee.mode && (h.authenticated = !0, n.path("/app"), a.clearHistory());
        kotobee && kotobee.dispatchEvent("ready")
    }, h.redeemCode = function (e, t) {
        var n = clone(e),
            r = {
                model: {},
                submit: function () {
                    i.hide(), n.code = r.model.code, n.code && (s.update(p.get("pleaseWait"), null, {
                        dots: !0
                    }), o.redeemCode(n, function (e) {
                        e && !e.error ? (s.update(), t()) : s.showAndHide(p.get(e ? e.error : "unexpectedError"), 3e3)
                    }))
                }
            };
        i.show({
            mode: "redeemCode",
            redeemCodeInfo: r
        })
    }, h
}]);

app.factory("autohide", ["$document", "$injector", "cache", "selection", "chapters", "library", "stg", function (i, a, e, n, s, t, o) {
    var l, c, d, u, f, p, h, g, r = {},
        m = !0;

    function v() {
        var e = {},
            t = document.getElementById("libraryThumbs");
        if (t && "none" != getComputedStyle(t).display) e.mode = "library", e.header = i[0].getElementById("libraryHeader"), e.footer = i[0].getElementById("libraryFooter"), e.footer && (e.footerHeight = e.footer.offsetHeight), e.scope = angular.element(t).scope(), e.libTabs = i[0].getElementById("libraryTabs"), e.libTabs && (e.libTabHeight = e.libTabs.offsetHeight);
        else {
            e.mode = "reader", e.header = i[0].getElementById("readerHeader"), e.scope = angular.element(e.header).scope(), e.tabMenu = i[0].getElementById("tabMenu"), e.tabMenuHeight = e.tabMenu.offsetHeight;
            var n = i[0].body.getElementsByClassName("navControls");
            if (n.length) {
                var r = (n = n[n.length - 1]).getElementsByClassName("next");
                r.length && (e.navNext = r[0]);
                var o = n.getElementsByClassName("prev");
                o.length && (e.navPrev = o[0]), e.navWidth = e.navNext.offsetWidth
            }
        }
        return e.headerHeight = e.header.offsetHeight, ios && (e.headerHeight -= 20), e
    }

    function b(e) {
        if (e.target) {
            var t = e.target.nodeName.toLowerCase();
            if ("button" == t) return;
            if ("a" == t) return;
            if ("input" == t) return;
            if (hasParentNode(e.target, "button")) return;
            if (hasParentNode(e.target, "a")) return;
            if (hasParentNode(e.target, "input")) return
        }
        c = !0, n.active() ? n.reset() : "hidden" == l ? I("buttons") : "buttons" == l ? I("hidden") : "midway" == l && I("buttons")
    }

    function y(n) {
        p = null;
        var e = v();
        if ("reader" == e.mode) "neverShow" == config.autohideReaderMode && h && (new Date).getTime() - g < 800 && Math.abs(n.clientX - h.x) < 50 && i("tab", "hidden");
        else if ("library" == e.mode) {
            if ("showOnScroll" == config.autohideLibMode) {
                var t = document.getElementsByClassName("bookThumbsContainer")[0],
                    r = document.getElementById("libraryThumbs"),
                    o = Number(getElemPadding(r).top);
                o = o || 0, t.offsetHeight < r.offsetHeight - o && I("buttons")
            }
            "neverShow" == config.autohideLibMode && h && (new Date).getTime() - g < 800 && Math.abs(n.clientX - h.x) < 50 && i("header", "hidden")
        }

        function i(e, t) {
            Math.abs(n.clientY - h.y) < 15 || (n.clientY < h.y ? h.y > .85 * document.body.clientHeight && I(e) : n.clientY > .85 * document.body.clientHeight && I(t), n.clientY > h.y ? h.y < .15 * document.body.clientHeight && I(e) : n.clientY < .15 * document.body.clientHeight && I(t))
        }
        h = null
    }

    function w(e) {
        h = {
            x: e.clientX,
            y: e.clientY
        }, g = (new Date).getTime()
    }

    function k(e) {
        var t = 40;
        if (h) {
            var n = p = p || v();
            if ("library" == n.mode && (t = 5), !("reader" == n.mode && "showOnTap" == config.autohideReaderMode || "library" == n.mode && "showOnTap" == config.autohideLibMode)) {
                if (f = f || Math.max(t, e.detail.scrollTop), c) return c = !1, f = Math.max(t, e.detail.scrollTop), void ("hidden" == l && (f -= n.headerHeight, u = n.headerHeight));
                if ("reader" == n.mode) {
                    if (m) return;
                    if ("default" != n.scope.header.mode) return;
                    if (a.get("nav").transforming) return
                } else if ("library" == n.mode) {
                    if (n.scope.subMode) return;
                    if (n.scope.searchOb.enabled) return
                }
                d = n.headerHeight - (n.headerHeight - (e.detail.scrollTop - f)), l = d >= n.headerHeight ? (f = e.detail.scrollTop - n.headerHeight, d = n.headerHeight, "hidden") : d < 0 ? (f = Math.max(t, e.detail.scrollTop), d = 0, "buttons") : "midway", null == u && (u = 0);
                var r = removeClass;
                if (u == n.headerHeight) {
                    if (d == n.headerHeight) return;
                    if (14 < d) return;
                    f = Math.max(t, e.detail.scrollTop), d = 0, r = addClass
                }
                d + u != 0 && (u = d, x(r, {
                    dims: n
                }))
            }
        }
    }

    function x(e, t) {
        var n = (t = t || {}).dims;
        if (n = n || v(), e(n.header, "ease"), C(n.header, d, n.headerHeight, "up", !!ios), "reader" == n.mode) {
            if (t.headerOnly) return;
            if (t.tabOnly) return void r();
            e(n.navNext, "ease"), e(n.navPrev, "ease"), C(n.navNext, d * n.navWidth / n.headerHeight, n.navWidth, o.data.settings.rtl ? "left" : "right"), C(n.navPrev, d * n.navWidth / n.headerHeight, n.navWidth, o.data.settings.rtl ? "right" : "left"), r()
        } else if ("library" == n.mode) {
            if (n.libTabs || (n = v()), n.libTabs && (e(n.libTabs, "ease"), C(n.libTabs, d, n.libTabHeight, "up")), t.headerOnly) return;
            n.footer && (e(n.footer, "ease"), C(n.footer, d * n.footerHeight / n.headerHeight, n.footerHeight, "down", !!ios))
        }

        function r() {
            e(n.tabMenu, "ease"), C(n.tabMenu, d * n.tabMenuHeight / n.headerHeight, n.tabMenuHeight, "down", !!ios)
        }
    }

    function C(r, o, e, i, a) {
        var s = 1 - (o = Math.min(e, o)) / 44;
        ionic.requestAnimationFrame(function () {
            var e;
            if (r && ("up" == i ? e = "translate3d(0, -" + o + "px, 0)" : "down" == i ? e = "translate3d(0, " + o + "px, 0)" : "right" == i ? e = "translate3d(" + o + "px, 0, 0)" : "left" == i && (e = "translate3d(-" + o + "px, 0, 0)"), r.style[ionic.CSS.TRANSFORM] = e, a))
                for (var t = 0, n = r.children.length; t < n; t++) r.children[t].style.opacity = s
        })
    }

    function I(e) {
        var t = v();
        "buttons" == e ? (u = d = 0, x(addClass)) : "header" == e ? (u = d = 0, x(addClass, {
            headerOnly: !0
        })) : "tab" == e ? (u = d = 0, x(addClass, {
            tabOnly: !0
        })) : "hidden" == e && (u = d = t.headerHeight, x(addClass, {
            dims: t
        })), l = e
    }
    return r.initialize = function () {
        mobile && (document.body.addEventListener(mobile && !mobileDebug ? "touchend" : "mouseup", y), document.body.addEventListener(mobile && !mobileDebug ? "touchstart" : "mousedown", w), config.autohideHeader && !config.autohideReaderMode && (config.autohideReaderMode = "showOnScroll"), config.autohideHeader || (config.autohideReaderMode = "alwaysShow"), config.autohideLibHeader && !config.autohideLibMode && (config.autohideLibMode = "showOnScroll"), config.autohideLibHeader || (config.autohideLibMode = "alwaysShow"))
    }, r.setup = function (e, t, n) {
        if (l = "buttons", "reader" == n.headerShrink) {
            if ("alwaysShow" == config.autohideReaderMode) return;
            if ("neverShow" == config.autohideReaderMode) return I("hidden"), addClass(document.getElementById("chapters"), "fillSpace"), e.$watch("header.mode", function (e, t) {
                e != t && I("selection" == e || "searchItems" == e || "textSize" == e ? "header" : "hidden")
            }), void addClass(document.getElementById("tabMenu"), "transparent");
            "showOnTap" == config.autohideReaderMode && (I("hidden"), addClass(document.getElementById("chapters"), "transparentBtns"), addClass(document.getElementById("chapters"), "fillSpace"))
        }
        if ("library" == n.headerShrink) {
            if ("alwaysShow" == config.autohideLibMode) return;
            if ("neverShow" == config.autohideLibMode) return;
            config.autohideLibMode
        }
        var r, o;
        (a.get("nav").addListener("tap", b), "reader" == n.headerShrink) ? (e.$watch("data.settings.textSize", function (e, t) {
            !r && (r = !0, "showOnTap" == config.autohideReaderMode) || I("buttons")
        }), e.$watch("header.mode", function (e, t) {
            !o && (o = !0, "showOnTap" == config.autohideReaderMode) || I("buttons")
        }), s.addListener(function (e) {
            "newChapter" == e && setTimeout(function () {
                m = !1
            }, 1500)
        }), "showOnTap" == config.autohideReaderMode && I("hidden")) : "library" == n.headerShrink && (e.$watch("subMode", function (e, t) {
            I("buttons")
        }), e.$watch("searchOb.enabled", function (e, t) {
            I("buttons")
        }));
        t.bind("scroll", k)
    }, r.libInitialized = function () {
        setTimeout(function () {
            "neverShow" == config.autohideLibMode && I("hidden"), "showOnTap" == config.autohideLibMode && I("hidden")
        })
    }, r
}]);

app.factory("backend", ["$http", "error", "translit", "cache", "$location", "$injector", "$ionicHistory", "$rootScope", "modals", "$window", "status", "crdv", "$sce", "$timeout", "stg", "$q", "sync", function (s, l, c, e, n, d, t, u, f, p, r, h, g, m, v, b, o) {
    var y, w, k = {},
        i = [],
        x = {
            transformRequest: angular.identity,
            headers: {
                "Content-Type": void 0
            }
        };

    function C() {
        try {
            if ("undefined" != typeof lti && lti && email && hash) {
                v.data.user.email = email, v.data.user.pwd = hash, v.data.user.rememberMe = !0;
                var e = document.getElementsByTagName("head")[0];
                if (e.length) {
                    e[0];
                    for (var t = document.getElementsByTagName("script"), n = 0; n < t.length; n++)
                        if (t[n].hasAttribute("lti")) {
                            t[n].parentNode.removeChild(t[n]);
                            break
                        }
                }
            }
        } catch (e) { }
    }
    k.getAuthVarsOld = function () {
        return {
            libid: config.kotobee.libid,
            email: v.data.user.email,
            pwd: v.data.user.pwd
        }
    }, k.getAuthVars = function () {
        var e = {
            email: v.data.user.email,
            pwd: v.data.user.pwd,
            code: v.data.user.code,
            env: config.kotobee.mode
        };
        return C(), e.cloudid = config.kotobee.cloudid, "library" == e.env && v.data.book && (e.bid = v.data.book.id), uuid && (e.fingerprint = uuid), e
    }, k.addListener = function (e, t) {
        i.push({
            event: e,
            func: t
        })
    }, k.initialize = function (n) {
        n = n || {
            putTest: function () { },
            $$phase: !0
        };
        putTest("X");
        var r = b.defer();
        return putTest("Y"), putTest("Z"), putTest("J"), l.detect("s_authError", k.loginErrorDetection), putTest("K"), v.readSlot("user" + config.kotobee.cloudid, function (e) {
            if (config.kotobee.cloudid) {
                var t = e;
                t && (v.data.user = t)
            }
            v.data.user || (v.data.user = {}), 0 == v.data.user.length && (v.data.user = {}), v.data.user.loggedIn = !1, putTest("L"), h.initialize(n, function () {
                preview ? r.resolve() : k.getFingerprint(function (e) {
                    uuid = e, r.resolve()
                })
            })
        }), r.promise
    }, k.getFingerprint = function (i) {
        function a() {
            var e = {
                excludeUserAgent: !0,
                excludeScreenResolution: !0,
                excludeDoNotTrack: !0,
                excludeAvailableScreenResolution: !0,
                excludeOpenDatabase: !0,
                excludePlugins: !0,
                excludeCanvas: !0,
                excludeWebGL: !0,
                excludeJsFonts: !0,
                excludeCanvas: !1,
                excludeWebGL: !1
            };
            new Fingerprint2(e).get(function (e, t) {
                try {
                    e = e || process.env.USERNAME, v.writeSlot("fingerprint", e, function () {
                        i(e)
                    })
                } catch (e) { }
            })
        }
        native && window.plugins && window.plugins.uniqueDeviceID ? window.plugins.uniqueDeviceID.get(function (e) {
            e && (e.length ? i(e) : a())
        }, a) : desktop ? "mac" == os ? a() : require("child_process").exec("wmic CPU get ProcessorId", {
            cwd: "."
        }, function (e, t, n) {
            if (t) {
                var r = t.toUpperCase().split("PROCESSORID");
                if (r.length) {
                    var o = r[1].trim();
                    o.length ? i(o) : a()
                } else a()
            } else a()
        }) : a()
    };
    var I = null,
        a = null;
    k.setLibraryLoginCallback = function (e) {
        I = e
    }, k.setLibraryLogoutCallback = function (e) {
        a = e
    }, k.loginErrorDetection = function (t) {
        v.data.user.loggedIn = !1, t && ("email" == config.kotobee.loginmode ? t.loginOptions.canLoginByEmail = !0 : "code" == config.kotobee.loginmode ? t.loginOptions.canLoginByCode = !0 : t.loginOptions.canLoginByEmail = t.loginOptions.canLoginByCode = !0, t.loginOptions.mode = t.loginOptions.canLoginByEmail ? "email" : "code", t.loginOptions.signIn = function () {
            f.hide(), setTimeout(function () {
                if ("code" == t.loginOptions.mode) {
                    if (!t.model || !t.model.code) return void l.update({
                        error: "enterYourCode"
                    });
                    v.data.user.code = t.model.code, v.data.user.email = v.data.user.pwd = null
                } else {
                    if (!v.data.user.email) return void l.update({
                        error: "enterYourEmail"
                    });
                    if (!t.model || !t.model.pwd) return void l.update({
                        error: "enterYourPwd"
                    });
                    v.data.user.pwd = t.model.pwd, v.data.user.code = null
                }

                function e() {
                    k.login({
                        userInput: !0
                    }).then(function (e) {
                        e.error || t.model && (t.model.pwd = t.model.code = null)
                    })
                }
                config.kotobee.permsLoaded ? e() : d.get("auth").getPermissions(e)
            }, 800)
        }, t.loginOptions.register = function () {
            f.hide(), n.path("/register")
        }, t.loginOptions.forgotPassword = function () {
            f.hide(), n.path("/forgotpwd")
        }, t.loginOptions.redeemCode = function () {
            t.loginOptions.mode = "code"
        }, t.loginOptions.signInByEmail = function () {
            t.loginOptions.mode = "email"
        })
    };
    var E = null;
    k.refreshSignatures = function (t) {
        var e = config.kotobee.liburl + "user/signatures";
        s.post(e, getFormData(k.getAuthVars()), x).success(function (e) {
            e.error || (v.data.user.signatures = e.signatures, t && t())
        })
    };
    var T = {
        ping: !0
    };

    function S(e) {
        if ((v.data.currentLibrary = e) && e.categories) {
            for (var t = [], n = 0; n < e.categories.length; n++) t.push(e.categories[n]);
            for (n = 0; n < e.categories.length; n++)
                for (var r = e.categories[n], o = 0; o < t.length; o++) t[o].parent && t[o].parent.id == r.id && (r.children || (r.children = []), r.children.push(t[o]), t[o].nested = !0);
            for (n = 0; n < e.categories.length; n++) e.categories[n].nested && (e.categories.splice(n, 1), n--)
        }
    }
    return k.login = function (o, e) {
        o = o || {};
        var t = {
            func: arguments.callee,
            args: arguments
        };
        document.body.focus = null;
        var i, n, a = b.defer();
        o.ping || (i = f.show({
            mode: "loading",
            loadingMsg: c.get("pleaseWait"),
            timeLimit: 1e4
        }), v.data.user.loggedIn = !1), e && (E = e), "book" == config.kotobee.mode && (n = config.kotobee.liburl + "library/auth"), "library" == config.kotobee.mode && (n = config.kotobee.liburl + "library/get"), C(), preview || (k.fingerprint = uuid);
        var r = k.getAuthVars();
        return o.userInput && (r.input = !0), r.platformdesc = navigator.userAgent, native ? (r.platform = "mobile", device && (r.platformdesc = device.platform + " " + device.model + " " + device.version + " " + (device.manufacturer ? device.manufacturer : ""))) : r.platform = desktop ? "desktop" : "web", r.v = readerV, o.ping && (r.ping = !0, r.pwd = o.pwd), r.v = config.v, s.post(n, getFormData(r), x).success(function (e) {
            if (clearTimeout(void 0), i && f.hide(i), e.error) return e.public && (w = !0), o.dontShowErrorDialog || l.update(e), v.data.user.pwd = null, v.data.user.code = null, o.ping && k.logout(), void a.resolve(e);
            var t = 3e5;
            if (void 0 === t && (t = 3e5), y && clearTimeout(y), y = setTimeout(function () {
                e.pwd && (T.pwd = e.pwd), k.login(T)
            }, t), !o.ping)
                if (v.data.user.pwd = e.pwd, v.data.user.id = e.userid, v.data.user.name = e.username, v.data.user.lastchap = e.lastchap, v.data.user.signatures = e.signatures, v.data.user.role = e.role, "book" == config.kotobee.mode && e.chapters && d.get("book").setAllowedChapters(e.chapters), v.data.user.rememberMe) {
                    var n = {
                        rememberMe: !0
                    };
                    v.data.user.email && v.data.user.pwd ? (n.email = v.data.user.email, n.pwd = v.data.user.pwd) : v.data.user.code && (n.code = v.data.user.code), v.writeSlot("user" + config.kotobee.cloudid, n, r)
                } else v.clearSlot("user" + config.kotobee.cloudid, r);
            function r() {
                if (!e.cloudid && !e.success) return o.dontShowErrorDialog || l.update({
                    error: "s_authError"
                }), void a.resolve({
                    error: "s_authError"
                });
                v.data.user.loggedIn = !0, "library" == config.kotobee.mode && (S(e), I && I()), a.resolve(e), E && (E(e), E = null)
            }
        }).error(function (e) {
            clearTimeout(void 0), f.hide(), l.update({
                error: "networkError",
                ref: t
            })
        }), a.promise
    }, k.libraryHasPublicBooks = function () {
        return w
    }, k.logout = function () {
        v.data.user.pwd = null, v.data.user.loggedIn = !1, v.data.user.email = null, v.data.user.code = null, v.writeSlot("user" + config.kotobee.cloudid, v.data.user, function () {
            "library" == config.kotobee.mode ? m(function () {
                if (l.update({
                    error: "s_authError"
                }), k.cloudParam("entry")) {
                    for (var e = 0; e < v.data.currentLibrary.books.length; e++) v.data.currentLibrary.books[e].favorite = !1;
                    h.applyExistingBooks()
                } else v.data.currentLibrary = {};
                a && a()
            }, 200) : (n.path("/login"), t.clearHistory())
        })
    }, k.getPermissions = function (n) {
        var r = {
            func: arguments.callee,
            args: arguments
        },
            e = {};
        return e.env = config.kotobee.mode, e.cloudid = config.kotobee.cloudid, s.post(config.kotobee.liburl + "library/permissions", getFormData(e), x).success(function (e) {
            if (e.error) return l.update({
                error: "errorAndCallback",
                msg: e.error,
                cb: h.exit
            }), angular.element(document.getElementById("home")).scope().hideLoading = !0, void (u.$$phase || u.$apply());
            n && n(e)
        }).error(function (e, t) {
            (native || desktop) && k.cloudParam("offline") ? n && n({
                error: "offline"
            }) : l.update({
                error: "networkError",
                ref: r
            })
        })
    }, k.register = function (e, t) {
        var n = {
            func: arguments.callee,
            args: arguments
        },
            r = f.show({
                mode: "loading",
                loadingMsg: c.get("registeringAccount")
            }),
            o = k.getAuthVars();
        o.email = e.email1, o.promocode = e.promocode, o.root = config.kotobee.liburl;
        var i = getFormData(o);
        return i.append("platform", navigator.userAgent), s.post(config.kotobee.liburl + "user/register", i, x).success(function (e) {
            f.hide(r), e.error ? l.update(e) : t && t(e)
        }).error(function (e) {
            l.update({
                error: "networkError",
                ref: n
            })
        })
    }, k.forgotPwd = function (e, t) {
        var n = {
            func: arguments.callee,
            args: arguments
        },
            r = f.show({
                mode: "loading",
                loadingMsg: c.get("retrieving")
            }),
            o = k.getAuthVars();
        o.email = e.email, o.root = config.kotobee.liburl;
        var i = getFormData(o);
        return s.post(config.kotobee.liburl + "user/forgotpwd", i, x).success(function (e) {
            f.hide(r), e.error ? l.update(e) : t && t(e)
        }).error(function (e) {
            l.update({
                error: "networkError",
                ref: n
            })
        })
    }, k.redeemCode = function (e, t) {
        var n = {
            func: arguments.callee,
            args: arguments
        },
            r = getFormData(k.getAuthVars());
        r.append("redeemcode", e.code), r.append("platform", navigator.userAgent), s.post(config.kotobee.liburl + "user/redeemcode", r, x).success(function (e) {
            t(e)
        }).error(function () {
            l.update({
                error: "networkError",
                ref: n
            })
        })
    }, k.userLoggedIn = function () {
        if (v.data.user.loggedIn) return !0;
        try {
            if ("library" == config.kotobee.mode && v.data.book && "public" == v.data.book.accessmode) return !0
        } catch (e) { }
    }, k.downloadEbookTest = function (e, t) {
        var n = {
            id: e.id,
            mode: e.binary
        };
        return k.sendRawPost("http://192.168.1.5/files/test.zip", n, {
            responseType: "blob",
            onProgress: function (e) { }
        }, function (e) {
            if (e.error) return l.update(e), void t();
            t(e)
        })
    }, k.downloadEbook = function (e, t, n) {
        try {
            var r = {
                responseType: "blob",
                onProgress: t
            },
                o = {
                    id: e.id,
                    mode: "binary",
                    requester: "reader"
                };
            return k.sendRawPost(config.kotobee.liburl + "library/ebook/download", o, r, function (e) {
                if (e.error) return l.update(e), void n();
                n(e)
            })
        } catch (e) { }
    }, k.tincan = function (e, t) {
        var n = getFormData(k.getAuthVars());
        for (var r in e) n.append(r, e[r]);
        s.post(config.kotobee.liburl + "tincan", n, x).success(function (e) {
            t && t()
        }).error(function (e, t) { })
    }, k.getUnauthLibrary = function (n) {
        if ("library" == config.kotobee.mode) {
            var r = {
                func: arguments.callee,
                args: arguments
            },
                e = getFormData(k.getAuthVars());
            s.post(config.kotobee.liburl + "library/getbasic", e, x).success(function (e) {
                S(e), n && n(e)
            }).error(function (e, t) {
                (native || desktop) && k.cloudParam("offline") ? n && n({
                    error: "offline"
                }) : l.update({
                    error: "networkError",
                    ref: r
                })
            })
        }
    }, k.addtoFav = function (e, t) {
        var n = {
            func: arguments.callee,
            args: arguments
        },
            r = getFormData(k.getAuthVars());
        r.append("bid", e.id), r.append("cloudid", k.cloudParam("cloudid")), r.append("type", "book"), s.post(config.kotobee.liburl + "library/favorite/add", r, x).success(function (e) {
            if (e.error) return l.update(e), void t();
            t(e)
        }).error(function () {
            l.update({
                error: "networkError",
                ref: n
            })
        })
    }, k.removeFav = function (e, t) {
        var n = {
            func: arguments.callee,
            args: arguments
        },
            r = getFormData(k.getAuthVars());
        r.append("bid", e.id), r.append("cloudid", k.cloudParam("cloudid")), r.append("type", "book"), s.post(config.kotobee.liburl + "library/favorite/remove", r, x).success(function (e) {
            if (e.error) return l.update(e), void t();
            t(e)
        }).error(function (e) {
            l.update({
                error: "networkError",
                ref: n
            })
        })
    }, k.getServerLang = function (e, t) {
        if (isKotobeeHosted()) {
            var n = getFormData();
            return n.append("lang", e.lang), n.append("root", rootDir), n.append("v", readerV), n.append("base", publicRoot), s.post(config.kotobee.liburl + "library/getlang", n, x)
        }
        return s.get(cacheBust(rootDir + "locales/" + e.lang + ".js"))
    }, k.getS3Paths = function (t) {
        var e = getFormData();
        s.post(config.kotobee.liburl + "library/s3paths", e, x).success(function (e) {
            v.data.paths = e,
                function (e) {
                    for (var t = 0; t < i.length; t++) i[t].event == e && i[t].func()
                }("pathsChanged"), t && t(e)
        }).error(function () { })
    }, k.addBookStat = function (e, t) {
        if (e = e || {}, !preview && config.kotobee.cloudid) {
            window.location.href;
            0;
            var n = getFormData(k.getAuthVars());
            n.append("type", e.type), s.post(config.kotobee.liburl + "library/ebook/stat", n, x).success(function (e) { }).error(function () { })
        }
    }, k.addStat = function (e, t) {
        if (e = e || {}, !preview) {
            var n = window.location.href;
            if (!(0 <= n.indexOf("localhost/") || 0 <= n.indexOf("localhost:"))) {
                var r = getFormData(k.getAuthVars());
                r.append("software", u.readerApp ? "readerapp" : "reader"), r.append("platform", navigator.userAgent), r.append("assetType", config.kotobee.mode);
                try {
                    r.append("assetName", "book" == config.kotobee.mode ? v.data.book.meta.dc.title : v.data.currentLibrary.name)
                } catch (e) { }
                r.append("assetPath", n.split("#")[0]), config.kotobee.cloudid && r.append("assetId", config.kotobee.cloudid), v.data.user.email && r.append("email", v.data.user.email), v.data.user.code && r.append("code", v.data.user.code), e.action && r.append("action", e.action), e.param && r.append("param", e.param), s.post(config.kotobee.liburl + "stat/add", r, x).success(function (e) { }).error(function () { })
            }
        }
    }, k.setUserData = function (e, t) {
        var n = getFormData(k.getAuthVars());
        n.append("label", e.label), n.append("data", e.data), e.overwrite && n.append("overwrite", e.overwrite), e.category && n.append("category", e.category), e.subcategory && n.append("subcategory", e.subcategory), e.options && n.append("options", e.options), s.post(config.kotobee.liburl + "cloud/set", n, x).success(function (e) {
            if (e.error) throw e;
            t(e)
        }).error(function () {
            throw "Network Error. Please try again"
        })
    }, k.getUserData = function (e, t) {
        var n = getFormData(k.getAuthVars());
        e.label && n.append("label", e.label), e.category && n.append("category", e.category), e.subcategory && n.append("subcategory", e.subcategory), e.allUsers && n.append("allUsers", !0), e.options && n.append("options", e.options), s.post(config.kotobee.liburl + "cloud/get", n, x).success(function (e) {
            if (e.error) throw e;
            t(e)
        }).error(function () {
            throw "Network Error. Please try again"
        })
    }, k.getUserList = function (e, t) {
        var n = getFormData(k.getAuthVars());
        e.role && n.append("role", e.role), s.post(config.kotobee.liburl + "cloud/users", n, x).success(function (e) {
            if (e.error) throw e;
            t(e)
        }).error(function () {
            throw "Network Error. Please try again"
        })
    }, k.getUserDataArray = function (e, t) {
        var n = getFormData(k.getAuthVars());
        e.array && n.append("array", e.array), s.post(config.kotobee.liburl + "cloud/getarray", n, x).success(function (e) {
            if (e.error) throw e;
            t(e)
        }).error(function () {
            throw "Network Error. Please try again"
        })
    }, k.deleteUserData = function (e, t) {
        var n = getFormData(k.getAuthVars());
        n.append("label", e.label), n.append("data", e.data), e.all && n.append("all", e.all), e.category && n.append("category", e.category), e.subcategory && n.append("subcategory", e.subcategory), e.options && n.append("options", e.options), s.post(config.kotobee.liburl + "cloud/delete", n, x).success(function (e) {
            if (e.error) throw e;
            t(e)
        }).error(function () {
            throw "Network Error. Please try again"
        })
    }, k.externalLink = function (n, e) {
        try {
            if ((e = e || {}).secure && !native && !desktop && !preview) return void p.open(n);

            function r(e, t) {
                native ? h.openInNativeBrowser(e, t) : desktop ? require("nw.gui").Shell.openExternal(e) : window.open(e, "_blank")
            }
            if (e.target && "_blank" == e.target.toLowerCase()) return void r(n, !0);
            if (native) return void r(n);
            var o = {
                title: c.get("externalLink"),
                src: g.trustAsResourceUrl(n),
                openInNativeBrowser: r,
                status: "loading"
            },
                i = setTimeout(function () {
                    o.status = "", o.error = c.get("errorLoadingPage"), u.$$phase || u.$apply()
                }, 1e4);
            o.loaded = function (e, t, n) {
                o.status = "loaded", clearTimeout(i), u.$$phase || u.$apply()
            };
            var a = f.show({
                mode: "externalSite",
                animation: "slide-in-up",
                dynamic: !0,
                backdropClickToClose: !1,
                siteOptions: o
            });
            desktop || preview || k.getXframeOptions({
                url: n
            }, function (e) {
                if (o.status = "loaded", u.$$phase || u.$apply(), e && e.success) {
                    var t = e.success.toLowerCase();
                    "sameorigin" != t && "deny" != t || (f.hide(a), setTimeout(function () {
                        f.show({
                            mode: "xFrameError",
                            errorOptions: {
                                url: n,
                                openInNativeBrowser: r
                            }
                        })
                    }, 500))
                }
            })
        } catch (e) { }
    }, k.createRequest = function (e, t, n) {
        t = t || {};
        var r = getFormData(k.getAuthVars());
        for (var o in t) r.append(o, t[o]);
        s.post(e, r, x).success(function (e) {
            n(e)
        }).error(function () {
            n(!1)
        })
    }, k.convertPDF = function (e, t) {
        if (e = e || {}, !preview) {
            var n = getFormData(k.getAuthVars());
            n.append("url", e.url), s.post(config.kotobee.liburl + "library/pdf", n, x).success(function (e) {
                t(e)
            }).error(function () { })
        }
    }, k.getXframeOptions = function (e, t) {
        var n = getFormData(k.getAuthVars());
        n.append("url", e.url), s.post(config.kotobee.liburl + "getxframe", n, x).success(function (e) {
            e.error ? t() : t(e)
        }).error(function (e) { })
    }, k.hasAccessTo = function (e, n) {
        if ("library" == config.kotobee.mode) {
            var t = getFormData(k.getAuthVars());
            for (var r in e = e || {}) t.append(r, e[r]);
            t.append("v", readerV), s.post(config.kotobee.liburl + "user/access", t, x).success(function (e) {
                n && n(e)
            }).error(function (e, t) {
                n && n({
                    error: ""
                })
            })
        } else n && n({
            success: !0
        })
    }, k.setLastChapter = function (n) {
        var e = getFormData(k.getAuthVars());
        e.append("chapter", currentIndex), s.post(config.kotobee.liburl + "user/lastchap", e, x).success(function (e) {
            n && n(e)
        }).error(function (e, t) {
            n && n({
                error: ""
            })
        })
    }, k.cloudParam = function (e) {
        return config.kotobee[e]
    }, k.getEbook = function (e, t) {
        var n = {
            func: arguments.callee,
            args: arguments
        },
            r = getFormData(k.getAuthVars());
        for (var o in e) r.append(o, e[o]);
        s.post(config.kotobee.liburl + "library/ebook/get", r, x).success(function (e) {
            e.error && l.update(e), t && t(e)
        }).error(function (e) {
            l.update({
                error: "networkError",
                ref: n
            }), t && t({
                error: "networkError"
            })
        })
    }, k.getEbooks = function (e, t, n) {
        var r = {
            func: arguments.callee,
            args: arguments
        },
            o = getFormData(k.getAuthVars());
        for (var i in e) o.append(i, e[i]);
        o.append("v", readerV), s.post(config.kotobee.liburl + "library/ebook/all", o, x).success(function (e) {
            e.error && l.update(e), n && n(e, t)
        }).error(function (e) {
            l.update({
                error: "networkError",
                ref: r
            }), n && n({
                error: "networkError"
            }, t)
        })
    }, k.sendRawPost = function (t, n, r, o, i) {
        i = i || 0;
        for (var e = "", a = [k.getAuthVars(), n], s = 0; s < a.length; s++)
            for (var l in a[s]) {
                var c = a[s][l];
                null != c && "object" != typeof c && ("string" == typeof c && (c = encodeURIComponent(c = ("." + c).substr(1))), "boolean" == typeof c && (c = c ? 1 : 0), e += "&" + l + "=" + c)
            }
        var d, u = new XMLHttpRequest;
        return u.open(r.method ? r.method : "POST", t, !0), "GET" != r.method && u.setRequestHeader("Content-type", "application/x-www-form-urlencoded"), r.onProgress && (u.onprogress = function (e) {
            e.lengthComputable && r.onProgress(e.loaded / e.total)
        }), r.responseType && (u.responseType = r.responseType), u.addEventListener("error", function (e) {
            o({
                error: "networkError"
            }), d = !0
        }, !1), u.onreadystatechange = function (e) {
            u.aborted || 4 == u.readyState && (200 == u.status ? m(function () {
                o(u.response)
            }) : u.onerror())
        }, u.onerror = function (e) {
            if (!d) {
                if (!(3 <= i)) return k.sendRawPost(t, n, r, o, ++i);
                m(function () {
                    o({
                        error: "d_connectionTimedOut"
                    })
                })
            }
        }, "GET" != r.method ? u.send(e) : u.send(), u
    }, k.abort = function (e) {
        e && 4 != e.readyState && (e.aborted = !0, e.abort())
    }, k
}]);

app.factory("book", ["$http", "error", "$timeout", "$rootScope", "crdv", "translit", "modals", "design", "$location", "library", "np", "sync", "cssInjector", "jsParser", "chapters", "cssParser", "$q", "status", "backend", "tinCan", "stg", "$sce", "$filter", function (y, r, e, w, o, k, x, i, a, t, s, l, n, c, d, u, f, C, p, I, E, h, T) {
    var g, S = {};
    return S.root = "", S.initialize = function () { }, S.getCoverImgPath = function (e, t) {
        if (t = t || e.path, !e.cover) return "";
        var n = "";
        return n = E.data.currentLibrary.cloud && E.data.currentLibrary.cloud.public ? E.getPublicRoot("book") + e.path + "/EPUB/" + e.cover : T("coverPath")(e.cover, t), e.exists && (native ? n = o.libEbooksDirectory.nativeURL + t + "/EPUB/" + e.cover.replace("_thumb", "") : desktop && (n = ph.normalize(s.getBooksPath() + t + "/" + e.cover.replace("_thumb", "")))), n
    }, S.getBgImgPath = function (e, t) {
        if (t = t || e.path, !e.cover) return "";
        var n = E.getPublicRoot("book");
        return n += e.path + "/" + e.bgimg
    }, S.setAllowedChapters = function (e) {
        if (S.allowedChapters = null, e && e.length) {
            for (var t = e.split(","), n = 0; n < t.length; n++) t[n] = Number(t[n]);
            S.allowedChapters = t
        }
    }, S.open = function (n) {
        if (preview) return x.hide(), void setTimeout(function () {
            x.show({
                mode: "generalError",
                errorOptions: {
                    msg: k.get("cantPreviewLibBook")
                }
            })
        }, 500);
        native ? (o.vibrate(), bookPath = o.libEbooksDirectory.nativeURL + n.path + "/EPUB/") : desktop ? (bookPath = s.getBooksPath() + n.path + "/", "mac" == os && (bookPath = "file://" + bookPath)) : (bookPath = E.data.currentLibrary.cloud.public ? E.getPublicRoot("book") : config.kotobee.liburl + "books/", bookPath += n.path + "/EPUB/"), x.hide(), setTimeout(function () {
            var e;
            C.update(k.get("pleaseWait"), null, {
                dots: !0
            }), e = function (e) {
                e.success ? (S.setAllowedChapters(e.chapters), S.loadBook().then(function () {
                    ! function (e, o) {
                        if (!e.dver || e.dver < 2) return o();
                        if (config.noBookOverride) return o();
                        y.get(cacheBust(ph.join(bookPath, "_kmeta/config.js"))).success(function (e) {
                            if (-1 != e.indexOf("var config")) {
                                e = e.replace("var config", "var bookConfig");
                                var t = document.createElement("script");
                                t.setAttribute("bookconfig", !0);
                                var n = document.getElementsByTagName("head");
                                for (var r in n.length && n[0].appendChild(t), t.innerHTML = e, g = g || clone(config), bookConfig) "kotobee" != r && (config[r] = bookConfig[r]);
                                i.injectUserCSS(config), i.convertTabs(), o && o()
                            } else o()
                        }).error(function () {
                            o && o()
                        })
                    }(n, function () {
                        if (E.data.book.accessmode = n.accessmode, E.data.book.id = n.id, E.data.epub.bookThumb = S.getCoverImgPath(n, E.data.book.path), chromeApp)
                            if (E.data.chromeAssets[E.data.book.bookThumb]) E.data.epub.bookThumb = E.data.chromeAssets[E.data.book.bookThumb];
                            else {
                                var t = E.data.epub.bookThumb;
                                delete E.data.epub.bookThumb, getImgDataSrc(t, function (e) {
                                    E.data.epub.bookThumb = e, E.data.chromeAssets[t] = e
                                })
                            }
                        C.update(), S.root = "/book/" + E.data.book.id, a.path(S.root + "/reader"), l.initialize(), setTimeout(function () {
                            d.initialize()
                        }, 1e3)
                    })
                })) : C.update()
            }, (native || desktop) && n.exists ? e({
                success: !0
            }) : p.hasAccessTo({
                testagainst: n.id,
                open: !0
            }, e)
        }, 200)
    }, S.removeBookConfigElem = function () {
        for (var e = document.getElementsByTagName("script"), t = 0; t < e.length; t++)
            if (e[t].hasAttribute("bookconfig")) {
                e[t].parentNode.removeChild(e[t]);
                break
            }
        if (g) {
            for (var n in config) "kotobee" != n && delete config[n];
            for (var n in g) config[n] = g[n];
            i.injectUserCSS(config)
        }
    }, S.loadBook = function () {
        var n = {
            func: arguments.callee,
            args: arguments
        },
            e = f.defer();

        function b() {
            p.addStat({
                action: "open"
            }), T("chapterHtml")(E.data.toc), E.data.book.chapters = E.data.toc.innerHTML, e.resolve()
        }
        return E.resetBook(), u.clearCache(), c.clearCache(), d.clearCache(), y.get(cacheBust(ph.join(bookPath, "META-INF/container.xml?v=" + readerV))).success(function (e) {
            config.kotobee.encodekey && (e = 1.1 <= config.v ? CryptoJS.AES.decrypt(e, config.kotobee.encodekey).toString(CryptoJS.enc.Utf8) : kInteractive.XORCipher().decode(config.kotobee.encodekey, e));
            var t = parser.parseFromString(e, "text/xml");
            E.data.epub = {};
            try {
                E.data.packageURL = t.getElementsByTagName("rootfile")[0].getAttribute("full-path")
            } catch (e) {
                return C.update(), void r.update({
                    error: "errorAndCallback",
                    msg: "cantReadConfigFile",
                    cb: o.exit
                })
            }
            var m = config.v,
                v = native || isKotobeeHosted();
            ! function (e) {
                window.location.origin;
                var t = Math.round(9999 * Math.random());
                "book" == config.kotobee.mode && !w.readerApp && 1.3 <= m && !v ? y.get("js/templates.js?c=" + t).success(e).error(function () {
                    r.update({
                        error: "networkError",
                        ref: n
                    })
                }) : e()
            }(function (g) {
                y.get(cacheBust(ph.join(bookPath, E.data.packageURL))).success(function (e) {
                    if ("book" == config.kotobee.mode && !w.readerApp)
                        if (1.3 <= m) {
                            if (v) {
                                if (0 != config.checksum.indexOf(CryptoJS.MD5(e.replace(/\s+/g, "")).toString())) return void t()
                            } else if (CryptoJS.MD5(e.replace(/\s+/g, "")).toString() + CryptoJS.MD5(g).toString() != config.checksum && (g = g.replace(/(\S)\n/g, "$1\r\n"), CryptoJS.MD5(e.replace(/\s+/g, "")).toString() + CryptoJS.MD5(g).toString() != config.checksum)) return t()
                        } else if (1.2 <= m && CryptoJS.MD5(e).toString() != config.checksum) return void t();

                    function t() {
                        var e = angular.element(document.getElementById("home"));
                        e && (e.scope().hideLoading = !0, w.$$phase || w.$apply()), C.update(), x.show({
                            mode: "generalError",
                            errorOptions: {
                                msg: k.get("errorLoadingPage")
                            }
                        })
                    }
                    config.kotobee.encodekey && (e = 1.1 <= config.v ? CryptoJS.AES.decrypt(e, config.kotobee.encodekey).toString(CryptoJS.enc.Utf8) : kInteractive.XORCipher().decode(config.kotobee.encodekey, e)), e = e.replace(/href="([^"]*?&[^"]*?)"/g, function (e, t) {
                        return 'href="' + t.replace(/&/g, "") + '"'
                    });
                    var n = parser.parseFromString(e, "text/xml");
                    if (E.data.epub.metadata = n.getElementsByTagName("metadata")[0], E.data.epub.manifest = n.getElementsByTagName("manifest")[0], E.data.epub.spine = n.getElementsByTagName("spine")[0], E.data.mediaList = [], E.data.book.meta = {}, E.data.book.version = 3, S.allowedChapters) {
                        for (var r = E.data.epub.spine.getElementsByTagName("itemref"), o = 0; o < S.allowedChapters.length; o++) r[S.allowedChapters[o]] && r[S.allowedChapters[o]].setAttribute("data-allow", !0);
                        for (o = 0; o < r.length; o++) r[o].hasAttribute("data-allow") ? r[o].removeAttribute("data-allow") : r[o].setAttribute("linear", "no")
                    }
                    try {
                        for (o = 0; o < E.data.epub.metadata.childNodes.length; o++) {
                            var i;
                            if (8 != (i = E.data.epub.metadata.childNodes[o]).nodeType && "#text" != i.nodeName)
                                if (i.hasAttribute("name") && "meta" == i.nodeName.toLowerCase()) E.data.book.meta[i.getAttribute("name")] = i.getAttribute("content");
                                else
                                    for (var a = (i.hasAttribute("property") ? i.getAttribute("property") : i.nodeName).split(":"), s = E.data.book.meta, l = 0; l < a.length; l++) l == a.length - 1 ? s[a[l].toLowerCase()] = i.innerHTML ? i.innerHTML : i.textContent : (s[a[l].toLowerCase()] || (s[a[l].toLowerCase()] = {}), s = s[a[l].toLowerCase()])
                        }
                    } catch (e) { }
                    E.initializeSettings();
                    var c = "image/.*",
                        d = getElementsByAttribute(E.data.epub.manifest, "item", "media-type", c);
                    for (o = 0; o < d.length; o++) {
                        var u = d[o].getAttribute("href");
                        if (0 != u.indexOf("js/widgets/") && (0 != u.indexOf("xhtml/pdf") || -1 == u.indexOf("/page"))) {
                            (s = {}).thumbnail = ph.join(bookPath, E.data.packageURL, d[o].getAttribute("href")), s.title = ph.removeExtension(ph.basename(s.thumbnail));
                            c = d[o].getAttribute("media-type");
                            s.type = c.split("image/")[1], E.data.mediaList.push(s), chromeApp && isAbsoluteURLAndNotDataPath(s.thumbnail) && (E.data.chromeAssets[s.thumbnail] ? s.thumbnail = E.data.chromeAssets[s.thumbnail] : f(s))
                        }
                    }

                    function f(t, n) {
                        getImgDataSrc(t.thumbnail, function (e) {
                            E.data.chromeAssets[t.thumbnail] = e, t.thumbnail = e, n && n()
                        })
                    }
                    var p = getElementsByAttribute(E.data.epub.manifest, "item", "properties", "cover-image");
                    0 < p.length ? E.data.epub.bookThumb = T("coverPath")(p[0].getAttribute("href")) : E.data.book.meta.cover && 0 < (p = getElementsByAttribute(E.data.epub.manifest, "item", "id", E.data.book.meta.cover)).length && (E.data.epub.bookThumb = T("coverPath")(p[0].getAttribute("href"))), chromeApp && E.data.epub.bookThumb && (E.data.chromeAssets[E.data.epub.bookThumb] ? E.data.epub.bookThumb = E.data.chromeAssets[E.data.epub.bookThumb] : getImgDataSrc(E.data.epub.bookThumb, function (e) {
                        E.data.chromeAssets[E.data.epub.bookThumb] = e, E.data.epub.bookThumb = e
                    }));
                    try {
                        E.data.tocPath = getElementsByAttribute(E.data.epub.manifest, "item", "properties", "[s]*nav[s]*")[0].getAttribute("href")
                    } catch (e) {
                        var h = E.data.epub.spine.getAttribute("toc");
                        if (!h) return void b();
                        try {
                            E.data.book.version = 2, E.data.tocPath = getElementsByAttribute(E.data.epub.manifest, "item", "id", h)[0].getAttribute("href")
                        } catch (e) {
                            return void b()
                        }
                    }
                    y.get(cacheBust(ph.join(bookPath, E.data.packageURL, E.data.tocPath))).success(function (e) {
                        if (config.kotobee.encodekey && (e = 1.1 <= config.v ? CryptoJS.AES.decrypt(e, config.kotobee.encodekey).toString(CryptoJS.enc.Utf8) : kInteractive.XORCipher().decode(config.kotobee.encodekey, e)), 2 == E.data.book.version) {
                            var t = parser.parseFromString(e, "text/xml").getElementsByTagName("navMap");
                            t.length && (E.data.toc = function e(t) {
                                var n = document.createElement("ol");
                                for (var r = 0; r < t.childNodes.length; r++)
                                    if (t.childNodes[r]) {
                                        var o = t.childNodes[r];
                                        if (8 != o.nodeType && "#text" != o.nodeName && "navpoint" == o.nodeName.toLowerCase()) {
                                            for (var i, a, s, l = 0; l < o.childNodes.length; l++)
                                                if (o.childNodes[l]) {
                                                    var c = o.childNodes[l].nodeName.toLowerCase();
                                                    "content" == c ? a = o.childNodes[l].getAttribute("src") : "navlabel" == c ? i = o.childNodes[l].textContent.trim() : "navpoint" == c && (s = s || e(o))
                                                }
                                            var d = document.createElement("li"),
                                                u = document.createElement("a");
                                            u.innerHTML = i || "", u.setAttribute("href", a ? ph.join(E.data.tocPath, a) : ""), d.appendChild(u), s && d.appendChild(s), n.appendChild(d)
                                        }
                                    }
                                return n
                            }(t[0]))
                        } else
                            for (var n = parser.parseFromString(e, "text/html").getElementsByTagName("ol"), r = 0, o = 0; o < n.length; o++) {
                                var i = n[o].getElementsByTagName("a");
                                if (!(i.length < r)) {
                                    r = i.length, E.data.toc = n[o];
                                    for (var a = i.length; a--;) i[a].getAttribute("href") ? i[a].setAttribute("href", ph.join(E.data.tocPath, i[a].getAttribute("href"))) : i[a].setAttribute("href", "")
                                }
                            }
                        b()
                    }), I.send({
                        verb: "opened",
                        activity: "Book: " + E.data.book.meta.dc.title
                    })
                }).error(function () {
                    r.update({
                        error: "networkError",
                        ref: n
                    })
                })
            })
        }).error(function (e) {
            C.update(), native || desktop || "file://" != window.location.origin ? r.update({
                error: "errorAndCallback",
                msg: "cantReadConfigFile",
                cb: o.exit
            }) : r.update({
                error: "This browser failed to run the ebook app locally. Please try a different browser, or upload the web files to your server and run the web app online"
            })
        }), e.promise
    }, S.getTitleByIndex = function (e) {
        if (E.data.toc) try {
            for (var t = E.data.toc.getElementsByTagName("li"), n = [], r = 0; r < t.length; r++)
                if (t[r]) {
                    var o = t[r].getElementsByTagName("a");
                    o.length && "true" != o[0].getAttribute("data-dontshow") && n.push(t[r])
                }
            return n[e].getElementsByTagName("a")[0].textContent
        } catch (e) { }
    }, S
}]);

app.factory("bookinfo", ["$http", "error", "$timeout", "auth", "$rootScope", "crdv", "np", "library", "book", "sync", "$location", "translit", "modals", "cssInjector", "jsParser", "chapters", "cssParser", "$q", "status", "backend", "tinCan", "stg", "$sce", "$filter", function (e, t, n, r, o, a, i, s, l, c, d, u, f, p, h, g, m, v, b, y, w, k, x, C) {
    var I, E, T, S, A = {};

    function O(e) {
        var t = {};
        return t.background = "url('" + e + "')", t["background-size"] = "cover", t["background-position"] = "center center", t.opacity = "1", t
    }

    function B() {
        if ("loading" == I) {
            if ("public" == E.accessmode) return;
            E.redirect = null, y.hasAccessTo({
                testagainst: E.id
            }, function (e) {
                e.success ? N() : (I = "denied", E.redirect = k.data.currentLibrary.unauthredirect, E.overrideunauthredirect && (E.redirect = E.unauthredirect)), M(), o.$$phase || o.$apply()
            })
        }
    }

    function L(e) {
        return k.data.info || (k.data.info = {}), k.data.info["book" + e.id] || (k.data.info["book" + e.id] = {}), k.data.info["book" + e.id]
    }

    function N() {
        E && (native || desktop ? I = E.exists ? "open" : L(E).loading ? "downloading" : L(E).extracting ? "extracting" : y.cloudParam("public") ? "download" : k.data.user.loggedIn ? "download" : "public" == E.accessmode ? "download" : y.cloudParam("entry") ? "login" : "download" : y.cloudParam("public") ? I = "open" : k.data.user.loggedIn ? I = "open" : "public" == E.accessmode ? I = "open" : y.cloudParam("entry") && (I = "login"))
    }

    function M() {
        T && T.ui && (T.ui.actionBtnIcon = function () {
            if ("open" == I) return "ion-eye size18";
            if ("login" == I) return "ion-person";
            if ("download" == I) return "ion-code-download";
            if ("downloading" == I) return null;
            if ("loading" == I) return null
        }(), T.ui.actionBtnSpinnerIcon = function () {
            {
                if ("downloading" == I) return "dots";
                if ("loading" == I) return "dots"
            }
        }(), T.ui.actionBtnLabel = function () {
            if ("open" == I) return "openBook";
            if ("login" == I) return "loginToRead";
            if ("download" == I) return "download";
            if ("downloading" == I) return "downloading";
            if ("extracting" == I) return "extracting";
            if ("loading" == I) return "pleaseWait";
            if ("denied" == I) return "bookAccessDenied"
        }(), T.ui.actionBtnStyle = "open" == I ? "openBtn" : "login" == I ? "openBtn" : "downloading" == I ? "downloadingBtn" : "extracting" == I ? "downloadingBtn" : "loading" == I ? "loadingBtn" : "denied" == I ? "deniedBtn" : "downloadBtn")
    }
    return A.redeemCode = function () {
        f.hide(), r.redeemCode({
            source: "bookinfo"
        }, function () {
            A.show(S)
        })
    }, A.show = function (t, e) {
        e = e || {}, E = (S = t).book, native && a.vibrate(), t.book.coverImg = l.getCoverImgPath(t.book), t.book.coverStyle = t.book.bannerStyle = {
            "background-image": "url('" + t.book.coverImg + "')"
        }, t.book.bgimg && (t.book.bannerStyle = O(l.getBgImgPath(t.book))), chromeApp && (k.data.chromeAssets[t.book.coverImg] ? t.book.coverStyle = t.book.bannerStyle = {
            "background-image": "url('" + k.data.chromeAssets[t.book.coverImg] + "')"
        } : getImgDataSrc(t.book.coverImg, function (e) {
            k.data.chromeAssets[t.book.coverImg] = e, t.book.coverStyle = {
                "background-image": "url('" + e + "')"
            }, t.book.bgimg || (t.book.bannerStyle = t.book.coverStyle)
        }), t.book.bgimg && (k.data.chromeAssets[t.book.bgImg] ? t.book.bannerStyle = O(k.data.chromeAssets[t.book.bgImg]) : getImgDataSrc(l.getBgImgPath(t.book), function (e) {
            k.data.chromeAssets[t.book.bgImg] = e, t.book.bannerStyle = O(e)
        })));
        var n = {};
        for (var r in t.book.meta || (t.book.meta = {}), t.book.meta) n[r] = t.book.meta[r];
        t.book.meta.dc = n;
        var o = {};
        o.book = t.book, o.categoryClicked = t.categoryClicked, o.ui = {}, T = o;
        var i = "slide-in-up";
        "none" == e.animation && (i = "none"), f.show({
            mode: "bookInfo",
            dynamic: !0,
            animation: i,
            bookInfoOptions: o
        }),
            function () {
                N(), !native && !desktop || "download" != I || "public" != E.accessmode && (I = "loading");
                native || desktop || "open" != I || "public" != E.accessmode && (I = "loading");
                "login" == I && (E.unauthredirect && (E.redirect = k.data.currentLibrary.unauthredirect), E.overrideunauthredirect && (E.redirect = E.unauthredirect));
                M()
            }(), setTimeout(B, mobile ? 1e3 : 500)
    }, A.download = function (t) {
        function e(e) {
            L(t).progress = Math.ceil(100 * e), o.$$phase || o.$apply()
        }
        L(t).loading || (a.vibrate(), a.toast(u.get("bookIsDownloading")), L(t).loading = !0, N(), M(), L(t).progress = 0, setTimeout(function () {
            y.downloadEbook(t, e, function (e) {
                L(t).loading = null, e && (L(t).extracting = !0, N(), M(), n(function () {
                    ! function (e, t, n, r) {
                        native ? a.writeEbook(e, t, n, r) : desktop && i.writeEbook(e, t, n, r)
                    }("src.zip", t, e, function (e) {
                        L(t).extracting = null, N(), M(), f.hide(), n(function () {
                            f.show({
                                mode: "bookDownloaded",
                                downloadedOptions: {
                                    book: t,
                                    open: l.open
                                },
                                apply: !0
                            }), a.applyExistingBooks(k.data.currentLibrary.books), y.addBookStat({
                                type: "download"
                            })
                        }, 1e3)
                    })
                }))
            })
        }, 500))
    }, A.action = function (e) {
        "loading" != I && "denied" != I && ("open" == I ? l.open(e) : "login" == I ? (f.hide(), setTimeout(y.login, 1e3)) : "download" == I && A.download(e), N(), M())
    }, A.delete = function (e) {
        a.toast(u.get("deletingBook")), a.vibrate(),
            function (e, t) {
                native ? a.deleteEbook(e, t) : desktop && i.deleteEbook(e, t)
            }(e, function () {
                N(), M(), o.$$phase || o.$apply()
            })
    }, A.toggleFav = function (e) {
        e.favorite ? A.removeFav(e, !0) : A.addToFav(e, !0)
    }, A.addToFav = function (e, t) {
        t ? e.favorite = !0 : b.update(u.get("addingToFavorites"), null, {
            dots: !0
        }), y.addtoFav(e, function () {
            e.favorite = !0, t || b.update()
        })
    }, A.removeFav = function (e, t) {
        t ? e.favorite = !1 : b.update(u.get("removingFromFavorites"), null, {
            dots: !0
        }), y.removeFav(e, function () {
            e.favorite = !1, t || b.update(), angular.element(document.getElementById("library")).scope().loadFavorites()
        })
    }, A
}]);

app.factory("cache", function () {
    var n = {},
        r = {};
    return n.initialize = function () {
        n.setOb("body", document.getElementsByTagName("body")[0]);
        var e = document.getElementsByTagName("title"),
            t = null;
        e.length ? t = e[0] : (t = document.createElement("title"), document.head.appendChild(t)), n.setOb("title", t)
    }, n.setOb = function (e, t) {
        return r[e] = t
    }, n.getOb = function (e) {
        return r[e]
    }, n
});

app.factory("chapterAPI", ["chapters", "stg", "$ionicSideMenuDelegate", "nav", "notes", "scorm", "selection", "$ionicScrollDelegate", "$rootScope", "$location", "backend", "tinCan", "settings", function (o, i, e, t, a, n, s, l, r, c, d, u, f) {
    var p = {},
        h = [];

    function g() {
        if (!(arguments.length < 2)) {
            var e, t = arguments[0],
                n = {};
            e = 2 == arguments.length ? arguments[1] : (n = arguments[1], arguments[2]), n.unique && v(t);
            var r = {
                event: t,
                cb: e
            };
            h.push(r)
        }
    }

    function m(e, t) {
        if (e) {
            var n = function (e) {
                if (!e) return;
                for (var t = h.length; t--;)
                    if (h[t].event == e) return h[t]
            }(e);
            n && n.cb && n.cb.apply(this, [t])
        }
    }

    function v(e) {
        for (var t = h.length; t--;) e && h[t].event != e || h.splice(t, 1)
    }

    function b() {
        var e = document.getElementById("tabMenu");
        if (e) return angular.element(e).scope()
    }

    function y() {
        var e = b();
        e && e.notebookClicked()
    }

    function w() {
        var e = b();
        e && e.pdfClicked()
    }

    function k() {
        var e = b();
        e && e.printClicked()
    }

    function x() {
        var e = b();
        e && e.settingsClicked()
    }

    function C() {
        var e = b();
        e && e.searchClicked()
    }

    function I() {
        var e = b();
        e && e.mediaClicked()
    }

    function E() {
        var e = b();
        e && e.chaptersClicked()
    }

    function T() {
        e.toggleRight(!1), e.toggleLeft(!1), t.fullscreen()
    }

    function S(e) {
        i.data.settings.language = e, f.langChanged()
    }

    function A() {
        o.nextChapter()
    }

    function O() {
        o.prevChapter()
    }

    function B(e) {
        o.loadChapterByIndex(e)
    }

    function L(e) {
        c.search("vi"), o.applyChapterStateFromURL(e, {
            vIndex: 0
        }), r.$$phase || r.$apply()
    }

    function N(e, t, n) {
        o.preloadChapterFromApi(e, t, n)
    }

    function M() {
        o.clearCache()
    }

    function P(e) {
        o.neverCache(e)
    }

    function R() {
        var e, t = l.$getByHandle("content");
        if (t && (e = t.getScrollPosition()), 1 == e.zoom) {
            var n = document.getElementById("epubContainer");
            if (hasClass(n.children[0], "scroll")) {
                var r = n.children[0].style.transform.replace(/.*?scale\((.*?)\).*?/g, "$1");
                r && (e.zoom = Number(r))
            }
        }
        return e
    }

    function D(e) {
        var t = l.$getByHandle("content");
        t && t.scrollTo(0, 0, e)
    }

    function F(e, t) {
        var n = l.$getByHandle("content");
        n && n.scrollTo(0, e, t)
    }

    function _(e, t) {
        var n = clone(t = t || {});
        n.content = e, kInteractive.alert(n)
    }

    function q(e, t) {
        var n = {};
        n.content = e, isFunction(t) ? (n.okBtn = "Yes", n.noBtn = "No", n.cb = t) : (t.okBtn && (n.okBtn = t.okBtn), t.noBtn && (n.noBtn = t.noBtn), n.cb = t.cb, n.noBackdrop = t.noBackdrop), kInteractive.confirm(n)
    }

    function H(e, t) {
        d.externalLink(e, t)
    }

    function z(e, t) {
        var n = s.convertElemsToLocation([t]),
            r = {};
        r.chapter = o.getElemChapterIndex(t), r.location = n.left ? n.left : n.right, r.type = "content", r.src = t.textContent, r.note = e, i.addNote(r, function () {
            a.addIcon(r)
        })
    }

    function $(e, t) {
        n.setManual(e, t)
    }

    function U(e) {
        e = e || {}, u.send(e)
    }

    function j(e, t) {
        if (!i.data.user.loggedIn) throw "User must be logged in.";
        if (!e.label) throw "'label' variable required.";
        if (!e.data) throw "'data' variable required.";
        d.setUserData(e, t)
    }

    function V(e, t) {
        if (!i.data.user.loggedIn) throw "User must be logged in.";
        if (!e.label && !e.category && !e.subcategory) throw "No variable passed to use as a keyword.";
        d.getUserData(e, t)
    }

    function W(e, t) {
        if (!i.data.user.loggedIn) throw "User must be logged in.";
        d.getUserList(e, t)
    }

    function G(e, t) {
        if (!i.data.user.loggedIn) throw "User must be logged in.";
        d.getUserDataArray({
            array: JSON.stringify(e)
        }, t)
    }

    function K(e, t) {
        if (!i.data.user.loggedIn) throw "User must be logged in.";
        if (!e.label) throw "'label' variable required.";
        d.deleteUserData(e, t)
    }
    return p.initialize = function () {
        kotobee.addEventListener = g, kotobee.dispatchEvent = m, kotobee.clearListeners = v, kotobee.openNotebook = y, kotobee.exportPDF = w, kotobee.print = k, kotobee.openSettings = x, kotobee.openSearch = C, kotobee.openMedia = I, kotobee.openChapters = E, kotobee.toggleFullscreen = T, kotobee.setLanguage = S, kotobee.nextChapter = A, kotobee.prevChapter = O, kotobee.gotoChapterIndex = B, kotobee.gotoChapterHref = L, kotobee.getChapter = N, kotobee.clearCache = M, kotobee.neverCache = P, kotobee.getScrollPosition = R, kotobee.scrollToTop = D, kotobee.scrollTo = F, kotobee.popup = _, kotobee.confirm = q, kotobee.openLink = H, kotobee.addToNotebook = z, kotobee.sendScorm = $, kotobee.sendTinCan = U, kotobee.setData = j, kotobee.getData = V, kotobee.getDataArray = G, kotobee.getUsers = W, kotobee.deleteData = K, Object.defineProperty(kotobee, "user", {
            get: function () {
                var e = clone(i.data.user);
                return delete e.pwd, delete e.rememberMe, delete e.signatures, e
            },
            set: function (e) {
                return null
            }
        }), Object.defineProperty(kotobee, "book", {
            get: function () {
                return i.data.book
            },
            set: function (e) {
                return null
            }
        }), Object.defineProperty(kotobee, "epub", {
            get: function () {
                return i.data.epub
            },
            set: function (e) {
                return null
            }
        }), Object.defineProperty(kotobee, "currentPage", {
            get: function () {
                return i.data.currentPageOb
            },
            set: function (e) {
                return null
            }
        }), Object.defineProperty(kotobee, "currentChapter", {
            get: function () {
                return i.data.currentPageOb
            },
            set: function (e) {
                return null
            }
        }), Object.defineProperty(kotobee, "documentation", {
            get: function () {
                var e = [];
                e.push({
                    method: "openNotebook()",
                    desc: "Opens the reader's notebook panel."
                }), e.push({
                    method: "exportPDF()",
                    desc: "Exports the current chapter as a PDF file."
                }), e.push({
                    method: "print()",
                    desc: "Prints the current chapter."
                }), e.push({
                    method: "openSettings()",
                    desc: "Opens the reader's settings panel."
                }), e.push({
                    method: "openSearch()",
                    desc: "Opens the reader's search panel."
                }), e.push({
                    method: "openMedia()",
                    desc: "Opens the reader's media panel."
                }), e.push({
                    method: "openChapters()",
                    desc: "Opens the reader's chapters panel (table of contents)."
                }), e.push({
                    method: "toggleFullscreen()",
                    desc: "Toggle reader into fullscreen mode."
                }), e.push({
                    method: "setLanguage(lang)",
                    desc: "Set the reader language. This method takes the language's 2 character abbreviation as input. Possible values: en,fr,es,pt,nl,de,hu,it,sw,ms,no,pl,ro,ru,tr,ar,zh,jp,ko."
                });
                var t = [];
                t.push({
                    method: "nextChapter()",
                    desc: "Jump to the next chapter, relative to the current chapter."
                }), t.push({
                    method: "prevChapter()",
                    desc: "Jump to the previous chapter, relative to the current chapter."
                }), t.push({
                    method: "gotoChapterIndex(index)",
                    desc: "Jump to a certain chapter, passing its index as input."
                }), t.push({
                    method: "gotoChapterHref(url)",
                    desc: "Jump to a certain chapter, passing its relative URL as input."
                }), t.push({
                    method: "getChapter(url, callback, errorCallback)",
                    desc: "Retrieve the chapter object, passing its relative URL as input. This method also takes 2 more arguments: a success callback with the chapter object, and an error callback."
                }), t.push({
                    method: "clearCache()",
                    desc: "Clear any cached chapter content."
                }), t.push({
                    method: "neverCache(chapterOb)",
                    desc: "Set the reader never to cache a specific chapter. This will cause chapters to be continuously loaded and never stored in memory. The chapter object is passed an input."
                }), t.push({
                    method: "getScrollPosition()",
                    desc: "Get the current chapter's scroll position."
                }), t.push({
                    method: "scrollToTop(animate)",
                    desc: "Scroll to the top of the chapter. This method also takes a second argument, to indicate whether scroll animation should be used."
                }), t.push({
                    method: "scrollTo(y, animate)",
                    desc: "Scroll to a certain y location. The y location is provided as an input. This method also takes a second argument, to indicate whether scroll animation should be used."
                });
                var n = [];
                n.push({
                    method: "popup(html, options)",
                    desc: "Display certain HTML content inside the reader's popup. Takes an optional object with the following properties:",
                    args: [{
                        Property: "title",
                        Description: "The text appearing in the title bar of the popup."
                    }, {
                        Property: "okBtn",
                        Description: "The label of the default OK button at the footer of the popup."
                    }, {
                        Property: "cb",
                        Description: "A callback function, called when the user clicks on OK."
                    }, {
                        Property: "raw",
                        Description: "Display only the passed HTML (raw) as a popup, without a frame and OK button. The above properties won't be effective if raw is set to true."
                    }, {
                        Property: "noBackdrop",
                        Description: "Don't display a backdrop when the popup is opened."
                    }]
                }), n.push({
                    method: "confirm(html,callback)",
                    desc: "Display certain content inside a confirmation box. The second argument represents the callback function in case the user confirms.",
                    args: [{
                        Property: "okBtn",
                        Description: "The label of the 'Yes' button at the footer of the popup. Defaults to 'Yes'"
                    }, {
                        Property: "noBtn",
                        Description: "The label of the 'No' button at the footer of the popup. Defaults to 'No'"
                    }, {
                        Property: "cb",
                        Description: "A callback function, called when the user clicks on the 'Yes' button."
                    }, {
                        Property: "noBackdrop",
                        Description: "Don't display a backdrop when the popup is opened."
                    }]
                }), n.push({
                    method: "openLink(url)",
                    desc: "Open a certain URL inside a sliding panel."
                }), n.push({
                    method: "addToNotebook(note, element)",
                    desc: "Add certain content to the notebook. The note is provided as the first argument, and the element to attach the note to is provided as the second argument."
                });
                var r = [];
                r.push({
                    method: "sendScorm(element,value)",
                    desc: "Set a SCORM run-time element with a value. This requires that the ebook is running as a SCORM package inside an LMS."
                }), r.push({
                    method: "sendTinCan(activityOb)",
                    desc: "Send a Tin Can activity to the LRS configured in the ebook/library settings. Available for cloud ebooks and libraries only."
                });
                var o = [];
                o.push({
                    method: "setData(dataOb, callback)",
                    desc: "Write data into the database for the currently logged in user. You will be able to fully label that data, for easy retrieval later. This methods takes an object with the following options:",
                    args: [{
                        Property: "label",
                        Description: "The label (key) representing this slot of data (required)."
                    }, {
                        Property: "data",
                        Description: "The data to write (required)."
                    }, {
                        Property: "overwrite",
                        Description: "In case this data exists, overwrite it"
                    }, {
                        Property: "category",
                        Description: "For better organization, set a category for this data. This will make queries more efficient."
                    }, {
                        Property: "subcategory",
                        Description: "For further organization, set a subcategory for this data. This will make queries more efficient."
                    }, {
                        Property: "options",
                        Description: "Set options for this data. This is treated as normal text, and can be used when querying for data."
                    }]
                }), o.push({
                    method: "getData(queryOb, callback)",
                    desc: "Retrieve data for the currently logged in user. It takes an object with the following options, and triggers a callback (the second argument) passing back the array of data.",
                    args: [{
                        Property: "label",
                        Description: "The label (key) representing this slot of data."
                    }, {
                        Property: "category",
                        Description: "The category assigned to this data, to use as a search parameter."
                    }, {
                        Property: "subcategory",
                        Description: "The subcategory assigned to this data, to use as a search parameter."
                    }, {
                        Property: "options",
                        Description: "The options for this data, to use as a search parameter."
                    }, {
                        Property: "allUsers",
                        Description: "Expand the scope of search to all users with access to this cloud ebook or library. To use this option, the user must have global data permissions."
                    }, {
                        Property: "role",
                        Description: "Search by user role."
                    }]
                }), o.push({
                    method: "getDataArray(array, callback)",
                    desc: "Send a collection of search queries for the currently logged in user, all in a single request. It takes an array of objects specified in the getData method, in addition to an 'id' object to label the results returned in an array."
                }), o.push({
                    method: "getUsers",
                    desc: "For logged in users with global data permissions, get an array of all users added to this cloud ebook/library.",
                    args: [{
                        Property: "role",
                        Description: "Limit the results to a specific user role."
                    }]
                }), o.push({
                    method: "deleteData(queryOb, callback)",
                    desc: "Delete a certain data slot for the currently logged in user. It takes an object with the following options:",
                    args: [{
                        Property: "label",
                        Description: "The label (key) representing this slot of data (required)."
                    }, {
                        Property: "category",
                        Description: "The category assigned to this data, to use as a search parameter."
                    }, {
                        Property: "subcategory",
                        Description: "The subcategory assigned to this data, to use as a search parameter."
                    }, {
                        Property: "options",
                        Description: "Set options for this data. This is treated as normal text, and can be used when querying for data."
                    }, {
                        Property: "all",
                        Description: "If this is true, all matching results will be deleted. It not, only the first result is deleted."
                    }]
                });
                var i = [];
                i.push({
                    prop: "epub",
                    desc: "The parsed epub object, showing the metadata, manifest, spine, and book thumbnail."
                }), i.push({
                    prop: "book",
                    desc: "The book object obtained from the epub, by the reader."
                }), i.push({
                    prop: "currentChapter",
                    desc: "The current chapter object, containing all its parsed properties."
                }), i.push({
                    prop: "currentPage",
                    desc: "In the case that a reflowable layout is paginated (displayed without a scrollbar, as single page or double page view), the currently displayed page(s) is(are) returned by this property."
                }), i.push({
                    prop: "user",
                    desc: "The currently logged in user. Return an object containing his id, name, email, and user role (if exists)."
                });
                var a = [];
                a.push({
                    event: "addEventListener(event, [options], callback)",
                    desc: "Listen to a certain event (string) and call a function once the event is fired. This function takes an optional second argument (options) with the following properties:",
                    args: [{
                        Property: "unique",
                        Description: "If an event listener already exists for this event, it will be removed, before adding the new one."
                    }]
                }), a.push({
                    event: "dispatchEvent(event)",
                    desc: "Fire a certain event. The function takes the event name (as a string)."
                }), a.push({
                    event: "clearListeners(event)",
                    desc: "Clear all listeners that are listening to a certain event (as a string). If no event is passed, then all listeners attached to the kotobee object are cleared."
                });
                var s = [];
                s.push({
                    event: "ready",
                    desc: "This event is fired from the document object, whenever the ebook or library initialization is complete. It is fired just once."
                }), s.push({
                    event: "chapterLoaded",
                    desc: "This event is fired whenever a new chapter is loaded and rendered (ready for use)."
                }), s.push({
                    event: "scrolled",
                    desc: "This event is fired whenever a chapter is scrolled. An object is passed back containing the scroll position."
                });
                var l = "color: #000; font-weight: bold; font-size:18px",
                    c = "color: #224e97; font-size:12px; font-weight:bold",
                    d = "color: #224e97; font-size:12px; font-weight:bold",
                    u = "margin-left:6px;",
                    f = "font-weight:bold;";
                setTimeout(console.clear.bind(console)), m("%cKotobee Reader API", "color: #000; font-weight: bold; font-size:22px;border:solid 1px #000;border-radius:8px;padding:5px 10px;"), m("%cThis documentation lists brief explanations of the different Kotobee Reader API methods, properties, and events, used to interact with the reader and build scalable ebook applications. A backend Developer API is also available for remotely controlling different aspects of your cloud ebooks and libraries, here: http://support.kotobee.com/en/support/solutions/folders/8000082574."), h(), m("%cUI Methods", l), m("%cThese methods are used to interact with the reader's different UI elements and components.", f);
                for (var p = 0; p < e.length; p++) m("%c" + e[p].method, c), m("%c" + e[p].desc, u);
                h(), m("%cContent Methods", l), m("%cThese methods are used to display/set content in different ways.", f);
                for (p = 0; p < n.length; p++) m("%c" + n[p].method, c), m("%c" + n[p].desc, u), n[p].args && g(n[p].args);
                h(), m("%cNavigation Methods", l), m("%cThese methods are used mainly to navigate to different chapters efficiently.", f);
                for (p = 0; p < t.length; p++) m("%c" + t[p].method, c), m("%c" + t[p].desc, u);
                h(), m("%cSCORM / Tin Can Methods", l), m("%cThese methods are used to send progress to an LMS or LRS.", f);
                for (p = 0; p < r.length; p++) m("%c" + r[p].method, c), m("%c" + r[p].desc, u);
                h(), m("%cCloud Methods", l), m("%cThese methods are used to store and retrieve user data into the database, for that cloud ebook or library. These methods are available for logged in users only.", f);
                for (p = 0; p < o.length; p++) m("%c" + o[p].method, c), m("%c" + o[p].desc, u), o[p].args && g(o[p].args);
                h(), m("%cProperties", l), m("%cThese properties are used to describe the book and user.", f);
                for (p = 0; p < i.length; p++) m("%c" + i[p].prop, "color: #224e97; font-size:12px; font-weight:bold"), m("%c" + i[p].desc, u);
                h(), m("%cEvents Methods", l), m("%cThese methods are used to listen to and manage events fired by kotobee.", f);
                for (p = 0; p < a.length; p++) m("%c" + a[p].event, d), m("%c" + a[p].desc, u), a[p].args && g(a[p].args);
                m("%cEvent types", f);
                for (p = 0; p < s.length; p++) m("%c" + s[p].event, d), m("%c" + s[p].desc, u);

                function h() {
                    m("")
                }

                function g(e) {
                    setTimeout(console.table.bind(console, e))
                }

                function m(e, t) {
                    setTimeout(console.log.bind(console, e, t || ""))
                }
                return null
            },
            set: function (e) {
                return null
            }
        })
    }, p
}]);

app.factory("chapters", ["$http", "tinCan", "backend", "context", "library", "error", "crdv", "skeleton", "$stateParams", "sync", "$ionicSideMenuDelegate", "scorm", "cache", "modals", "status", "$injector", "$timeout", "virt", "$ionicPopup", "translit", "selection", "$ionicScrollDelegate", "$filter", "$rootScope", "cssInjector", "cssParser", "jsParser", "$location", "$sce", "$q", "stg", function (r, p, h, g, e, i, o, s, l, a, t, m, v, n, c, d, u, b, f, y, w, k, x, C, I, E, T, S, A, O, B) {
    var L, N, M, P, R, D, F, _, q, H, z, $, U, j = {},
        V = [],
        W = [],
        G = [];

    function K(e) {
        for (var t = 0; t < G.length; t++)
            if (G[t] == e) return !0
    }

    function X(t, n) {
        var r = {
            leftOb: {}
        };
        j.getPageOb({
            page: t.index,
            source: "getNextIndex"
        }, function (e) {
            if (!e) return n();
            e.pages ? t.vIndex < e.pages.length - 1 ? (r.leftOb.index = t.index, r.leftOb.vIndex = t.vIndex + 1) : (r.leftOb.index = t.index + 1, r.leftOb.vIndex = 0) : r.leftOb.index = t.index + 1, n(r)
        })
    }

    function J(t, n) {
        var r = {
            leftOb: {}
        };
        if (j.shouldBePaged(t)) {
            if (!t.vIndex || 0 == t.vIndex) return void j.getPageOb({
                page: t.index - 1,
                source: "stepBack"
            }, function (e) {
                r.leftOb.index = t.index - 1, e.pages && e.pages.length && (r.leftOb.vIndex = e.pages.length - 1), n(r)
            });
            r.leftOb.index = t.index, r.leftOb.vIndex = t.vIndex - 1
        } else r.leftOb.index = t.index - 1;
        n(r)
    }

    function Y(e) {
        if (!(e < 0 || e >= B.data.epub.spine.getElementsByTagName("itemref").length - 1)) {
            var t = B.data.epub.spine.getElementsByTagName("itemref")[e];
            if (t) {
                var n = t.getAttribute("properties");
                if (n)
                    for (var r = n.split(" "), o = 0; o < r.length; o++)
                        if (0 < r[o].indexOf("-spread-")) return r[o].split("-spread-")[1]
            }
        }
    }

    function Z(e, t, n) {
        var r = j.cacheLookup(e);
        r ? n(r) : j.preloadChapter(e, t, n)
    }

    function Q() {
        "undefined" != typeof kotobeeReady && kotobeeReady(), void 0 !== kotobee && kotobee.ready && kotobee.ready()
    }

    function ee(e) {
        for (var t = 0; t < V.length; t++) V[t](e);
        if ("renderedChapter" == e) {
            try {
                document.dispatchEvent(new Event("kotobeeChapterLoaded"))
            } catch (e) { }
            kotobee && kotobee.dispatchEvent("chapterLoaded")
        }
    }

    function te() {
        s.isFirstChapter({
            first: R
        }, function (t) {
            s.isLastChapter({
                last: N
            }, function (e) {
                B.data.book.chapter.notLastChapter = !e, B.data.book.chapter.notFirstChapter = !t, C.$$phase || C.$apply()
            })
        })
    }

    function ne() {
        timeOut(function () {
            c.update(null)
        }, 1e3)
    }

    function re(e, t, n) {
        r.get(cacheBust(e)).success(function (e) {
            config.kotobee.encodekey && (e = 1.1 <= config.v ? CryptoJS.AES.decrypt(e, config.kotobee.encodekey).toString(CryptoJS.enc.Utf8) : kInteractive.XORCipher().decode(config.kotobee.encodekey, e)), e = (e = e.replace(/<br\s*?\/>/g, "<br>")).replace(/(<)([^ >]+)([^>]*?)(\/>)/g, "$1$2$3></$2>"), t && t(e)
        }).error(function () {
            n && n()
        })
    }

    function oe(e, t) {
        var n = (e = e || {}).first,
            r = e.page,
            o = e.vIndex;
        (null == r && (r = currentIndex), null == o && (o = currentVIndex), "single" == j.getCurrentView(r, {
            vIndex: o
        }).view) ? j.shouldBePaged(e.page ? e : B.data.currentPageOb) ? t(r <= n && 0 == currentVIndex) : t(r <= n) : B.data.currentPageOb.pages ? t(r <= n && 0 == currentVIndex) : t(r <= n)
    }

    function ie(e, t) {
        var n = (e = e || {}).max,
            r = e.page,
            o = e.vIndex,
            i = e.view;
        return null == r && (r = currentIndex, o = currentVIndex, i = j.getCurrentView(r, {
            returnType: "view"
        })), "single" == i ? (null == e.index && (e.index = e.page), j.shouldBePaged(null != e.page ? e : B.data.currentPageOb), a(i ? n - 1 <= r : n - 1 <= r && M - 1 <= o)) : a(n - 1 <= r ? !M || M - 2 <= o : n - 2 <= r && (!(M && P - 1 <= o) || M <= 1));

        function a(e) {
            return t && t(e), e
        }
    }

    function ae() {
        var e = ph.removeHash(window.location.href),
            t = document.getElementsByTagName("base");
        if (t.length)
            for (var n = 0; n < t.length; n++)
                if (t[n].getAttribute("href")) {
                    e = window.location.origin + "/" + t[n].getAttribute("href");
                    break
                }
        return isAbsoluteURLPath(B.data.book.chapter.absoluteURL) ? B.data.book.chapter.absoluteURL : "/" == B.data.book.chapter.absoluteURL[0] ? getDomainFromURL(e) + B.data.book.chapter.absoluteURL : ph.join(e, B.data.book.chapter.absoluteURL)
    }

    function se() {
        return "library" == config.kotobee.mode ? B.data.paths.private + "books/" + B.data.book.chapter.absoluteURL.split("/books/")[1] : B.data.paths.private + B.data.book.chapter.absoluteURL.substr(1)
    }
    return j.busy = !0, j.initialize = function () {
        if (document.getElementById("chaptersInner")) {
            config.firstDoublePage || (config.firstDoublePage = 0), null == B.data.settings.pageScroll && (B.data.settings.pageScroll = !0), g.initialize(), currentIndex = null, B.data.currentPageOb = null, L = B.data.epub.spine.getElementsByTagName("itemref").length, N = L;
            for (var e = B.data.epub.spine.getElementsByTagName("itemref"), t = e.length; t--;)
                if (!e[t].hasAttribute("linear") || "no" != e[t].getAttribute("linear")) {
                    N = t + 1;
                    break
                }
            for (t = R = 0; t < e.length; t++)
                if (!e[t].hasAttribute("linear") || "no" != e[t].getAttribute("linear")) {
                    R = t;
                    break
                }
            var n = B.data.book.chapters ? B.data.book.chapters : '<h5 class="panelEmpty"><i class="icon ion-alert-circled"></i> ' + y.get("noTOC") + "</h5>";
            replaceHTML(document.getElementById("chaptersInner"), n), v.setOb("title", replaceHTML(v.getOb("title"), B.data.book.meta.dc.title)), h.addBookStat({
                type: "view"
            }), D || (y.addListener(function () {
                var e = document.getElementById("chaptersInner");
                e && replaceHTML(e, B.data.book.chapters ? B.data.book.chapters : '<h5 class="panelEmpty"><i class="icon ion-alert-circled"></i> ' + y.get("noTOC") + "</h5>")
            }), D = !0), x("bookSetup")(), j.showLoader(), j.resetPages(), j.preloadEdgeChapters(function () {
                var o = 0,
                    i = 0;
                if (null != j.startChapter && "book" == config.kotobee.mode) {
                    var e = d.get("book").root + "/reader/chapter/" + j.startChapter;
                    return j.startHash && (e += "/" + j.startHash), S.path(e), j.hideLoader(), a.trigger(), void (C.$$phase || C.$apply())
                }
                B.readSlot("lastLoc" + B.getBookId(), function (r) {
                    B.readSlot("lastLocY" + B.getBookId(), function (e) {
                        config.preserveLoc && "app" != config.kotobee.mode && (o = r ? Number(r) : 0, i = e ? Number(e) : 0), B.data.user && null != B.data.user.lastchap && (o = Number(B.data.user.lastchap));
                        var t = m.getBookmark();
                        t && (o = Number(t));
                        var n = {};
                        i && (n.scroll = i), j.loadChapterByIndex(o, n).then(function () {
                            j.hideLoader(), a.trigger()
                        })
                    })
                })
            })
        } else setTimeout(j.initialize, 50)
    }, j.preloadEdgeChapters = function (t) {
        j.getPageOb({
            page: R,
            source: "preloadEdgeChapters"
        }, function (e) {
            j.getPageOb({
                page: N - 1,
                source: "preloadEdgeChapters"
            }, function (e) {
                P = M = null, t()
            })
        })
    }, j.displayChapters = function () {
        if (!C.widescreen || config.notebookTab) {
            var e = t.$getByHandle("menus");
            e._instances[0].left.width = e._instances[0].right.width = 330, e._instances[0].left.el.style.width = e._instances[0].right.el.style.width = "330px", C.widescreen && (B.data.settings.rtl ? (e._instances[0].right.width = 480, e._instances[0].right.el.style.width = "480px") : (e._instances[0].left.width = 480, e._instances[0].left.el.style.width = "480px"), d.get("notes").updateNotebookArray({}, function () {
                C.$$phase || C.$apply()
            })), mobile && d.get("nav").resize(), B.data.settings.rtl ? t.toggleRight() : t.toggleLeft(), native && o.vibrate()
        }
    }, j.clearCache = function () {
        W = []
    }, j.neverCache = function (e) {
        e && (K(e.url) || G.push(e.url))
    }, j.hideLoader = function () {
        var e = document.getElementById("ebookLoaderAnim");
        e && addClass(e, "hide")
    }, j.showLoader = function () {
        var e = document.getElementById("ebookLoaderAnim");
        e && removeClass(e, "hide")
    }, j.addListener = function (e) {
        V.push(e)
    }, j.removeListener = function (e) {
        for (var t = V.length; t--;) V[t] == e && V.splice(t, 1)
    }, j.redirectToHashPage = function (o, e) {
        var i = e.leftOb;
        if (i.pages && i.pages.length) {
            var a = parser.parseFromString(i.raw ? i.raw : i.content, "text/html");
            x("contentVirt")("parse", a.getElementById("epubContent"), function () {
                var e = a.getElementById(o);
                if (!e) {
                    var t = a.getElementsByName(o);
                    t.length && (e = t[0])
                }
                if (e) {
                    var n = w.convertElemsToLocation([e], a.getElementById("epubContent")).left,
                        r = w.getPageByLocation(i, n);
                    j.loadChapterByIndex(i.index, {
                        hash: o,
                        vIndex: r
                    })
                }
            })
        }
    }, j.redirect = function (e, t) {
        t = t || {};
        var n = d.get("book").root + "/reader/chapter/" + e;
        t.hash && (n += "/" + t.hash);
        var r = j.getCurrentHash();
        if (t.hash ? S.search("vi") : S.search({
            vi: t.vIndex
        }), S.path(n), currentIndex == e) {
            if (!r) return j.loadChapterByIndex(e, t);
            if (r == t.hash) return j.loadChapterByIndex(e, t)
        }
        C.$$phase || C.$apply()
    }, j.getCurrentHash = function () {
        var e = S.path(),
            t = (e = e.replace(/\?.*/g, "")).split("/" + currentIndex + "/");
        if (!(t.length < 2)) return t[1]
    }, j.loadLocation = function (a, e) {
        var s = O.defer();
        e = e || {};
        var n = !a.match(/[^0-9.]/g);
        return j.showLoader(), j.getPageOb({
            page: e.index
        }, function (o) {
            if (!o.pages || !o.pages.length) {
                if (n) {
                    var e = w.getElemsByLocation(a);
                    if (!e[0]) return s.resolve();
                    b.safeScrollToElem("content", e[0], !0)
                } else b.safeAnchorScroll("content", a, !0);
                return j.hideLoader(), s.resolve()
            }
            if (n) {
                var t = w.getPageByLocation(o, a);
                return j.loadChapterByIndex(o.index, {
                    vIndex: t
                }), s.resolve()
            }
            var i = parser.parseFromString(o.raw ? o.raw : o.content, "text/html");
            x("contentVirt")("parse", i.getElementById("epubContent"), function () {
                var e, t;
                if (!(e = i.getElementById(a))) {
                    var n = i.getElementsByName(a);
                    n.length && (e = n[0])
                }
                e && (t = w.convertElemsToLocation([e], i.getElementById("epubContent")).left);
                var r = 0;
                return t && (r = w.getPageByLocation(o, t)), j.loadChapterByIndex(o.index, {
                    vIndex: r
                }), s.resolve()
            })
        }), s.promise
    }, j.indexFromUrl = function (e, t) {
        var n;
        t = t || {};
        var r = ph.getHash(e);
        t.hash && (r = t.hash), e = ph.removeHash(ph.normalize(e));
        for (var o = B.data.epub.manifest.getElementsByTagName("item"), i = 0; i < o.length; i++)
            if (ph.removeHash(ph.normalize(o[i].getAttribute("href"))) == e) {
                n = o[i].getAttribute("id");
                break
            }
        o = B.data.epub.spine.getElementsByTagName("itemref");
        var a, s = t.order ? t.order : 1;
        for (i = 0; i < o.length; i++)
            if (o[i].getAttribute("idref") == n && o[a = i].getAttribute("id") == r) {
                if (--s) continue;
                return i
            }
        return a
    }, j.arePagesAppearingTogether = function (e, t, n) {
        var r = j.getCurrentView(t.index),
            o = e.vIndex;
        o < 0 && e.pages && e.pages.length && (o += e.pages.length), isNaN(o) && (o = null);
        var i = t.vIndex;
        if (i < 0 && t.pages && t.pages.length && (i += t.pages.length), isNaN(i) && (i = null), e.index == t.index && o == i) return n(!0);
        if ("double" != r.view) return n();
        if ("rfl" == e.layout) {
            if (e.index == t.index && o == i + 1) return n(!0);
            if (t.pages && 1 == t.pages.length && e.index == t.index + 1 && 0 == o) return n(!0)
        }
        return "fxl" == e.layout && e.index == t.index + 1 ? n(!0) : n()
    }, j.getNextIndexOld = function (t, n) {
        var r = {},
            e = j.getCurrentView(t.index);
        if (r.leftOb = {
            index: t.index + 1,
            vIndex: 0
        }, t.vIndex || (t.vIndex = 0), "double" == e.view)
            if (r.double = !0, "rfl" == t.layout)
                if (2 <= t.pages.length - 1 - t.vIndex) r.leftOb.index = t.index, r.leftOb.vIndex = t.vIndex + 2;
                else {
                    if (t.pages.length - 1 - t.vIndex != 1) return void j.getPageOb({
                        page: t.index + 1,
                        source: "getNextIndex"
                    }, function (e) {
                        1 == e.pages.length ? (r.leftOb.vIndex = 0, r.leftOb.index = t.index + 2) : (r.leftOb.vIndex = 1, r.leftOb.index = t.index + 1), n(r)
                    });
                    r.leftOb.index = t.index + 1, r.leftOb.vIndex = 0
                } else r.leftOb.index++;
        else "rfl" != t.layout || B.data.settings.pageScroll || (1 <= t.pages.length - 1 - t.vIndex ? (r.leftOb.index = t.index, r.leftOb.vIndex = t.vIndex + 1) : (r.leftOb.index = t.index + 1, r.leftOb.vIndex = 0));
        return n(r), r
    }, j.getElemChapterIndex = function (e) {
        return "single" == B.data.currentView ? currentIndex : traverseToTop(e, "spreadL") ? currentIndex : traverseToTop(e, "spreadR") ? B.data.currentRPageOb.index : currentIndex
    }, j.getNextIndex = function (e, t) {
        var n = j.getCurrentView(e.index, {
            vIndex: e.vIndex
        });
        X(e, function (e) {
            if (!e) return t();
            "double" == n.view ? e.leftOb.index >= N - 1 ? t(e) : X(e.leftOb, t) : t(e)
        })
    }, j.getPrevIndexOld = function (t, n) {
        var r = {};
        r.leftOb = {
            index: t.index - 1,
            vIndex: 0
        }, t.vIndex || (t.vIndex = 0);
        if ("double" == j.getCurrentView(t.index - 1, {}).view)
            if (r.double = !0, config.firstDoublePage || (config.firstDoublePage = 0), "rfl" == t.layout)
                if (1 < t.vIndex) r.leftOb.index = t.index, r.leftOb.vIndex = t.vIndex - 2, r.leftOb.index < config.firstDoublePage && r.leftOb.vIndex++;
                else {
                    if (1 != t.vIndex) return void j.getPageOb({
                        page: t.index - 1,
                        source: "getPrevIndex"
                    }, function (e) {
                        1 == e.pages.length ? (r.leftOb.index = t.index - 2, r.leftOb.vIndex = -1, r.leftOb.index < config.firstDoublePage && (r.leftOb.index = t.index - 1, r.leftOb.vIndex = 0)) : (r.leftOb.index = t.index - 1, r.leftOb.vIndex = t.vIndex - 2, r.leftOb.index < config.firstDoublePage && (r.leftOb.vIndex = t.vIndex - 1)), n(r)
                    });
                    r.leftOb.index = t.index - 1, r.leftOb.vIndex = -1, r.leftOb.index < config.firstDoublePage && (r.leftOb.index = t.index, r.leftOb.vIndex = 0)
                } else r.leftOb.index = t.index - 2, r.leftOb.index < config.firstDoublePage && (r.leftOb.index = t.index - 1);
        else "rfl" != t.layout || B.data.settings.pageScroll || (0 < t.vIndex ? (r.leftOb.index = t.index, r.leftOb.vIndex = t.vIndex - 1) : (r.leftOb.index = t.index - 1, r.leftOb.vIndex = -1));
        return n(r), r
    }, j.getPrevIndex = function (e, t) {
        J(e, function (e) {
            "double" == j.getCurrentView(e.leftOb.index, {
                vIndex: e.leftOb.vIndex
            }).view ? e.leftOb.index <= R ? t(e) : J(e.leftOb, t) : t(e)
        })
    }, j.nextChapter = function (e) {
        s.isLastChapter(null, function (e) {
            e ? o.toast(y.get("inLastChapter")) : (g.interruptAnimation(), s.getNextIndex({
                index: currentIndex,
                vIndex: currentVIndex
            }, function (e) {
                if (e) {
                    for (var t = B.data.epub.spine.getElementsByTagName("itemref")[e.leftOb.index]; t && t.hasAttribute("linear") && "no" == t.getAttribute("linear");) {
                        if (++e.leftOb.index >= t.length - 1) return void o.toast(y.get("inLastChapter"));
                        t = B.data.epub.spine.getElementsByTagName("itemref")[e.leftOb.index]
                    }
                    var n = t.getAttribute("id");
                    C.renderScope.applyHeader("default"), null != e.leftOb.vIndex && (n = null), j.redirect(e.leftOb.index, {
                        hash: n,
                        vIndex: e.leftOb.vIndex
                    }), C.$$phase || C.$apply()
                }
            }))
        })
    }, j.prevChapter = function (e) {
        s.isFirstChapter(null, function (e) {
            e ? o.toast(y.get("inFirstChapter")) : (g.interruptAnimation(), s.getPrevIndex({
                index: currentIndex,
                vIndex: currentVIndex
            }, function (e) {
                e.vIndex;
                e.leftOb.index < 0 && (e.leftOb.index = 0);
                for (var t = B.data.epub.spine.getElementsByTagName("itemref")[e.leftOb.index]; t && t.hasAttribute("linear") && "no" == t.getAttribute("linear");) {
                    if (--e.leftOb.index < 0) return e.leftOb.index = 0, void o.toast(y.get("inFirstChapter"));
                    t = B.data.epub.spine.getElementsByTagName("itemref")[e.leftOb.index]
                }
                C.renderScope.applyHeader("default");
                var n = t.getAttribute("id");
                null != e.leftOb.vIndex && (n = null), j.redirect(e.leftOb.index, {
                    hash: n,
                    vIndex: e.leftOb.vIndex
                }), C.$$phase || C.$apply()
            }))
        })
    }, j.getCurrentView = function (e, t) {
        var n = (t = t || {}).returnType;
        if (null != e) {
            var r = s.getCurrentView(e, t);
            if (r) return i(r);
            if (e < config.firstDoublePage) return i({
                view: null,
                index: e
            });
            var o = B.data.settings.viewMode;
            return "single" == o ? i({
                view: null,
                index: e,
                scroll: B.data.settings.pageScroll
            }) : "double" != o ? "auto" == o ? "left" == Y(e) && "right" == Y(e + 1) ? i({
                view: "double",
                index: e
            }) : "right" == Y(e) && "left" == Y(e - 1) ? i({
                view: "double",
                index: e - 1
            }) : i({
                view: null,
                index: e,
                scroll: B.data.settings.pageScroll
            }) : i({
                view: null,
                index: e,
                scroll: B.data.settings.pageScroll
            }) : config.singleViewOnPortrait && 1.3 * window.innerWidth < window.innerHeight ? i({
                view: null,
                index: e
            }) : i(s.isSingle(e, t.vIndex) ? {
                view: null,
                index: e
            } : {
                    view: "double",
                    index: e
                })
        }

        function i(e) {
            return e.view || (e.view = "single"), "view" == n ? (t.cb && t.cb(e.view), e.view) : "page" == n ? (t.cb && t.cb(e.page), e.page) : (t.cb && t.cb(e), e)
        }
    }, j.loadChapterByIndex = function (o, i) {
        var a = (i = i || {}).deferred;
        return a = a || O.defer(), null == o && (o = currentIndex), o < 0 ? a.resolve() : (L < o && (o = 0), j.getPageOb({
            page: o,
            source: "loadChapterByIndex"
        }, function (r) {
            j.getSurroundingPages({
                index: r.index,
                vIndex: i.vIndex
            }, function () {
                var e = {
                    index: o,
                    vIndex: i.vIndex
                };
                if (s.normalize(e), !i.hash && (e.index != o || null != e.vIndex && e.vIndex != i.vIndex)) return j.loadChapterByIndex(e.index, {
                    hash: i.hash,
                    vIndex: e.vIndex,
                    reload: i.reload,
                    deferred: a
                }), a.promise;
                var n = j.getChapterURL(o),
                    t = !1;
                if (0 == n.indexOf("#") && (t = !0), i.hash && r.pages && r.pages.length && (t = !0), t) return j.loadLocation(i.hash, {
                    index: o,
                    source: "from loadChapterByIndex"
                }).then(function (e) { });
                i.leftOb = r, i.leftOb.index = o, i.leftOb.currentVIndex = i.leftOb.vIndex, i.leftOb.vIndex = i.vIndex, asyncCheck(!(r.pages && i.leftOb.vIndex >= r.pages.length - 1), j.getPageOb, [{
                    page: o + 1,
                    source: "loadChapterByIndex"
                }], function () {
                    j.planPageMapping(i, function (e) {
                        if ((e = e || {}).redirect) return j.redirect(e.redirect.index, {
                            hash: i.hash,
                            vIndex: i.vIndex,
                            reload: i.reload,
                            deferred: a
                        }), a.promise;
                        if (i.deferred = null, j.urlMatchesIndex(n, currentIndex) && !i.reload && currentVIndex == i.vIndex) {
                            currentIndex = o, te();
                            var t = i.hash;
                            return (t = t || ph.getHash(i.leftOb.url)) ? j.loadLocation(t, {
                                index: currentIndex,
                                source: "from loadChapterByIndex2"
                            }).then(function () {
                                ee("renderedChapter"), j.highlightCurrentTocChapter()
                            }) : (k.$getByHandle("content").scrollTop(!0), ee("renderedChapter"), j.highlightCurrentTocChapter()), B.data.book.chapter.url = n, B.data.book.chapter.absoluteURL = ph.join(bookPath, B.data.packageURL, B.data.book.chapter.url), a.resolve(), a.promise
                        }
                        g.startTransition(i, a)
                    })
                })
            })
        })), a.promise
    }, j.getSurroundingPages = function (e, t) {
        if ("single" == B.data.settings.viewMode) return t();
        if (!e) return t();
        var n = s.getJoint(e.index, e.vIndex);
        if (n && 1 < n.length) return j.getPageOb({
            page: e.index - 1
        }, t);
        asyncCheck(n, j.getPageOb, [{
            page: e.index
        }], function () {
            j.getPageOb({
                page: e.index + 1
            }, function () {
                j.getPageOb({
                    page: e.index - 1
                }, t)
            })
        })
    }, j.planPageMapping = function (t, e) {
        var n = j.getCurrentView(t.leftOb.index, {
            vIndex: t.leftOb.vIndex
        });
        if (n.index == t.leftOb.index)
            if (j.shouldBePaged(t.leftOb) || "double" == n.view)
                if (t.view = "double", t.rightOb || (t.rightOb = {}), "fxl" == t.leftOb.layout) {
                    j.getChapterURL(t.leftOb.index + 1) ? (t.rightOb.index = t.leftOb.index + 1, t.rightOb.url = ph.join(bookPath, B.data.packageURL, j.getChapterURL(t.leftOb.index + 1))) : t.rightOb = null, i()
                } else {
                    if (t.rightOb.url = t.leftOb.url, t.leftOb.viewport = d.get("spread").getViewport(t.leftOb), t.vIndex = Number(t.vIndex), isNaN(t.vIndex) && (t.vIndex = 0), t.leftOb.vIndex = t.vIndex, t.vIndex < 0 || t.vIndex >= t.leftOb.pages.length) {
                        var r = 0;
                        return t.vIndex < 0 && (r = t.vIndex), t.vIndex = t.leftOb.pages.length + r, t.leftOb.vIndex = t.leftOb.pages.length + r, t.rightOb.vIndex = t.leftOb.vIndex + 1, t.rightOb.index = t.leftOb.index, void e({
                            redirect: {
                                index: n.index
                            }
                        })
                    }
                    if (t.leftOb.content = t.leftOb.pages[t.vIndex].content, "single" == n.view) return t.rightOb = null, void i();
                    if (t.vIndex + 1 < t.leftOb.pages.length) t.rightOb = clone(t.leftOb), t.rightOb.vIndex = t.vIndex + 1, t.rightOb.index = t.leftOb.index, t.rightOb.content = t.leftOb.pages[t.rightOb.vIndex].content, i();
                    else {
                        if (t.leftOb.index >= L - 1) return t.rightOb = null, void i();
                        t.rightOb.index = t.leftOb.index + 1;
                        var o = j.getChapterURL(t.rightOb.index);
                        j.getPageOb({
                            page: t.rightOb.index
                        }, function (e) {
                            t.rightOb = e, t.rightOb.vIndex = 0, t.rightOb.url = ph.join(bookPath, B.data.packageURL, o), t.rightOb.pages && t.rightOb.pages.length && (t.rightOb.content = t.rightOb.pages[0].content), t.rightOb.viewport = d.get("spread").getViewport(t.rightOb), i()
                        })
                    }
                } else t.view = "single", t.leftOb.viewport = null, t.leftOb.raw && (t.leftOb.content = t.leftOb.raw), i();
        else e({
            redirect: {
                index: n.index
            }
        });

        function i() {
            e && e({
                url: t.leftOb.url
            })
        }
    }, j.getCacheOrPreload = Z, j.currentUrlMatchesVIndex = function (e) {
        return getQueryStringValue("vi") == e
    }, j.urlMatchesIndex = function (e, t) {
        if (null != t) {
            e = ph.normalize(e);
            ph.getHash(e);
            var n, r = ph.removeHash(e),
                o = B.data.epub.spine.getElementsByTagName("itemref")[t];
            if (o) {
                for (var i = B.data.epub.manifest.getElementsByTagName("item"), a = 0; a < i.length; a++) i[a].getAttribute("id") == o.getAttribute("idref") && (n = i[a].getAttribute("href"));
                return !!n && ph.removeHash(ph.normalize(n)) == ph.removeHash(ph.normalize(r))
            }
        }
    }, j.applyChapterStateFromURL = function (e, t) {
        if (t = t || {}, 0 != e.indexOf("#")) {
            var n = ph.getHash(e),
                r = t.index ? t.index : j.indexFromUrl(e);
            null != r && (C.renderScope.applyHeader("default"), j.redirect(r, {
                hash: n,
                vIndex: t.vIndex
            }), C.$$phase || C.$apply())
        } else j.redirect(currentIndex, {
            hash: e.substr(1)
        })
    }, j.loadChapterByURL = function (t, n) {
        var e = ph.relative(ph.join(bookPath, B.data.packageURL), t.leftOb.url);
        t = t || {};
        var r = O.defer(),
            o = t.hash ? t.hash : ph.getHash(e),
            i = t.leftOb.index;
        if (0 == e.indexOf("#") && (o = e.substr(1)), null != i) {
            if (!t.reload && j.urlMatchesIndex(e, currentIndex) && currentVIndex == t.vIndex) return currentIndex = i, te(), o ? (n && n(), j.loadLocation(o, {
                index: currentIndex,
                source: "from loadChapterByUrl"
            })) : (l.hash && k.$getByHandle("content").scrollTop(!0), r.resolve(), n && n(), r.promise);
            if (desktop) require("nw.gui").App.clearCache();
            kInteractive.clearAudioVideo(document), currentVIndex = t.vIndex;
            var a = j.getCurrentView(i, {
                vIndex: currentVIndex
            });
            return B.data.currentView = a.view, currentIndex = i = a.index, te(), B.data.currentPageOb = t.leftOb, B.data.currentRPageOb = t.rightOb, delete t.leftOb.currentVIndex, mobile && k.$getByHandle("content").resize(), j.busy = !0, j.loadChapterByURLRaw(t).then(function (e) {
                j.busy = !1, t.scroll && k.$getByHandle("content").scrollTo(0, t.scroll, !1);
                config.kotobee.cloudid && (config.kotobee.public || h.setLastChapter()), setTimeout(function () {
                    b.reset(t.scroll ? t.scroll : 0, function () {
                        o ? j.loadLocation(o, {
                            index: currentIndex,
                            source: "loadChapterByURLRaw"
                        }).then(function () {
                            ee("renderedChapter"), j.highlightCurrentTocChapter(), ne(), Q(), n && n()
                        }) : (T.replaceScript(e || []), ee("renderedChapter"), j.highlightCurrentTocChapter(), ne(), Q(), n && n())
                    }, 30)
                })
            })
        }
        n && n()
    }, j.cacheLookup = function (e, t) {
        if (!K(e))
            for (var n = 0; n < W.length; n++)
                if (W[n].url == ph.removeHash(e)) {
                    var r = W[n];
                    return W.splice(n, 1), W.push(r), r
                }
    }, j.removeFromCache = function (e, t) {
        for (var n = 0; n < W.length; n++)
            if (W[n].url == ph.removeHash(e) && W[n].view == t) return void W.splice(n, 1)
    }, j.addToCache = function (e, t) {
        if (t = t || {}, e.url = ph.removeHash(e.url), !K(e.url)) {
            var n = j.cacheLookup(e.url, e.view);
            n ? t.update && (n.url = e.url, n.title = e.title, n.viewport = e.viewport, n.content = e.content, n.layout = e.layout, n.styles = e.styles, e.script && (n.script = e.script)) : (20 < W.length && W.shift(), t.lowPriority ? W = [e].concat() : W.push(e))
        }
    }, j.loadChapterByURLRaw = function (d) {
        d = d || {};
        var e = ph.relative(ph.join(bookPath, B.data.packageURL), d.leftOb.url),
            u = O.defer();
        B.data.book.chapter.url = e, B.data.book.chapter.absoluteURL = ph.join(bookPath, B.data.packageURL, B.data.book.chapter.url);
        ph.getHash(B.data.book.chapter.url);
        b.disable(), b.lock(), c.update(null);
        var f = [];
        return function (t) {
            var n;
            "double" == B.data.currentView && (n = j.cacheLookup(B.data.book.chapter.absoluteURL, "single"));
            if (n) return copyObjToObj(n, t.leftOb, {
                onlyIfNotExists: !0
            }), t.leftOb.cached = !0, o();
            var r = {
                showError: !0,
                deferred: u,
                view: t.view,
                id: t.id,
                view: null
            };

            function o() {
                if ("single" != t.view && t.rightOb) {
                    var e = t.rightOb.url;
                    (n = j.cacheLookup(e, "single")) ? (copyObjToObj(n, t.rightOb, {
                        onlyIfNotExists: !0
                    }), t.rightOb.cached = !0, i(t)) : j.preloadChapter(e, r, function (e) {
                        copyObjToObj(e, t.rightOb, {
                            onlyIfNotExists: !0
                        }), i(t)
                    })
                } else i(t)
            }
            j.preloadChapter(B.data.book.chapter.absoluteURL, r, function (e) {
                copyObjToObj(e, t.leftOb, {
                    onlyIfNotExists: !0
                }), o()
            })
        }(d), u.promise;

        function i(e) {
            var t = (e = e || {}).leftOb.title,
                n = (e.leftOb.content, e.leftOb.viewport),
                r = (e.leftOb.styles, e.leftOb.script, e.leftOb.layout),
                o = e.isCached,
                i = e.leftOb,
                a = e.rightOb;
            i && (f = f.concat(i.script)), a && (f = f.concat(a.script)), makeArrayUnique(f), t = t || i.title, B.data.book.chapter.title = t, p.send({
                verb: "navigated",
                activity: (currentIndex ? "Chapter " + currentIndex + ": " : "") + t
            }), h.addStat({
                action: "chapter",
                param: t
            });
            var s = document.getElementById("epubInner");
            e.source = "from chapters", pauseVideoAndAudio(s = g.populateSpreads(s, e)), v.setOb("epubContent", document.getElementById("epubContent")), v.getOb("epubContainer").focus(), o || g.setContentSize(v.getOb("epubContent"), d), B.data.book.chapter.viewport = n, B.data.book.chapter.layout = r, B.data.book.chapter.view = e.view, B.data.book.chapter.paged = j.shouldBePaged(i), te();
            var l = document.getElementById("chapters");
            removeClass(l, ["rfl", "fxl", "double", "single"]), addClass(l, B.data.book.chapter.viewport ? "fxl" : "rfl"), addClass(l, e.view), B.data.book.chapter.paged ? addClass(l, "paged") : removeClass(l, "paged"), kInteractive.postRender(document);
            o || (E.replaceStyles(""), E.replaceStyles(i.styles.replace(/#spreadR/g, "#spreadL"), {
                append: !0
            }), a && a.viewport && E.replaceStyles(a.styles.replace(/#spreadL/g, "#spreadR"), {
                append: !0
            })), E.replaceStyles("#epubContainer div#epubContent, #epubContainer.stylesEnabled div#epubContent { background-color:#e7e3d9 !important;}", {
                append: !0
            }), config.preserveLoc && "app" != config.kotobee.mode && B.writeSlot("lastLoc" + B.getBookId(), currentIndex), B.writeSlot("lastLocY" + B.getBookId(), 0);
            for (var c = 0; c < V.length; c++) V[c]("newChapter");
            x("contentJs")(o).then(function () {
                w.reset(), b.unlock(), m.setBookmark(currentIndex);
                var e = {};
                e.id = "chapter-" + currentIndex, e.description = "View chapter: " + t, e.learnerResponses = "Viewed", e.type = "other", e.timestamp = new Date, m.setInteractions([e]), u.resolve(f)
            })
        }
    }, j.preloadChapterFromApi = function (e, t) {
        return j.preloadChapter(ph.join(bookPath, B.data.packageURL, e), {}, t)
    }, j.loadAndDecrypt = function (e, t, n) {
        return re(ph.join(bookPath, B.data.packageURL, e), t, n)
    }, j.preloadChapter = function (f, p, h) {
        var e = {
            func: arguments.callee,
            args: arguments
        };
        p = p || {}, re(f, function (e) {
            for (var a = parser.parseFromString(e, "text/html"), s = x("contentHtml")(a, f), t = a.getElementsByTagName("meta"), l = null, n = 0; n < t.length; n++) {
                var r = t[n].getAttribute("name");
                if (r && "viewport" == r.toLowerCase()) {
                    l = {};
                    var o = t[n].getAttribute("content");
                    if (!o) break;
                    for (var i = o.split(","), c = 0; c < i.length; c++) {
                        var d = i[c].split("=");
                        d[1] && (d[1] = d[1].replace("px", ""), l[d[0].trim()] = Number(d[1].trim()))
                    }
                }
            }
            var u = l ? "fxl" : "rfl";
            x("js")(a).then(function (i) {
                x("styles")(a, {
                    path: f,
                    id: p.id
                }).then(function (e) {
                    var t, n = a.getElementsByTagName("title");
                    if (n.length) t = n[0].textContent.trim();
                    else {
                        var r = a.getElementsByTagName("h1");
                        r.length && (t = r[0].textContent.trim())
                    }
                    t = t || B.data.book.meta.dc.title;
                    var o = {
                        url: f,
                        title: t,
                        viewport: l,
                        layout: u,
                        content: s.outerHTML,
                        styles: e,
                        script: i
                    };
                    p.dontCache || j.addToCache(o, p), h && h(o)
                })
            })
        }, function () {
            p.showError && (i.update({
                error: "networkError",
                ref: e
            }), p.deferred && p.deferred.reject({}), ne())
        })
    }, j.clearContent = function () {
        replaceHTML("epubInner", "")
    }, j.getChapterURL = function (e) {
        var t;
        null == e && (e = currentIndex), e < 0 && (e = 0);
        try {
            var n = B.data.epub.spine.getElementsByTagName("itemref");
            if (e >= n.length) return;
            t = n[e]
        } catch (e) { }
        var r = getElementsByAttribute(B.data.epub.manifest, "item", "id", t.getAttribute("idref"))[0].getAttribute("href"),
            o = t.getAttribute("id");
        return o && (r += "#" + o), r
    }, j.isFirstChapter = function (e) {
        oe({
            first: 0
        }, e)
    }, j.isFirstVisibleChapter = function (e) {
        oe({
            first: R
        }, e)
    }, j.isLastChapter = function (e) {
        return ie({
            max: L
        }, e)
    }, j.isLastVisibleChapter = function (e) {
        return ie({
            max: N
        }, e)
    }, j.isLastVisibleSingleChapter = function (e, t, n) {
        return ie({
            max: N,
            page: e,
            vIndex: t,
            view: "single"
        }, n)
    }, j.getPageOb = function (e, n) {
        var r = (e = e || {}).page;
        null == r && (r = currentIndex);
        var t = j.getChapterURL(r);
        if (!t) return n ? n() : null;
        Z(ph.join(bookPath, B.data.packageURL, t), {}, function (t) {
            if (t.index = r, "fxl" == t.layout && (t.spread = Y(t.index)), t.nonlinear = function (e) {
                var t = B.data.epub.spine.getElementsByTagName("itemref")[e];
                if (t) return "no" == t.getAttribute("linear")
            }(t.index), t.pages || "fxl" == t.layout || !j.shouldBePaged(t) || e.dontParse) return s.add(t), n ? n(t) : null;
            d.get("spread").parseReflowableChapter(t, function (e) {
                return t.pages = e.pages, t.raw = e.raw, s.add(t), n ? n(t) : null
            })
        })
    }, j.shouldBePaged = function (e, t) {
        return (!e || "fxl" != e.layout) && ("double" == B.data.settings.viewMode || ("single" == B.data.settings.viewMode ? !B.data.settings.pageScroll : (!t && e && (t = j.getCurrentView(e.index).view), "single" != t || !B.data.settings.pageScroll)))
    }, j.pdfPrint = function () {
        var o = {
            func: arguments.callee,
            args: arguments
        };
        if (preview) i.update({
            error: "savePdfUnsupportedInPreview"
        });
        else if (!desktop && !native)
            if (config.pdfsavingOptions) {
                var e = {
                    scale: 100,
                    submit: function () {
                        n.hide(), t({
                            scale: e.scale
                        })
                    }
                };
                n.show({
                    mode: "pdfSave",
                    pdfSaveOptions: e
                })
            } else t();
        function t(n) {
            (n = n || {}).scale || (n.scale = 100);
            var r = ae();
            isKotobeeHosted() && config.kotobee.cloudid && !config.kotobee.public && (r = se());
            var e = B.data.book.chapter.title,
                t = {
                    url: r,
                    filename: e
                };
            c.update(y.get("pleaseWait"), null, {
                dots: !0
            }), h.createRequest(config.kotobee.liburl + "link/pdfgenerate/url", t, function (e) {
                if (!e) return c.update(), void i.update({
                    error: "networkError",
                    ref: o
                });
                var t = e;
                F && F.parentNode && F.parentNode.removeChild(F), (F = document.createElement("iframe")).setAttribute("style", "display:none"), F.setAttribute("name", "pdfFrame"), document.body.appendChild(F), _ && _.parentNode && _.parentNode.removeChild(_), (_ = document.createElement("form")).setAttribute("style", "visibility:hidden:position:absolute"), _.setAttribute("method", "post"), _.setAttribute("action", t), _.setAttribute("target", "pdfFrame"), addFormHiddenInput("url", r, _), addFormHiddenInput("filename", B.data.book.chapter.title, _), addFormHiddenInput("fileext", "pdf", _), addFormHiddenInput("scale", n.scale, _), addFormHiddenInput("format", config.pdfMode ? config.pdfMode : "imgpdf", _), asyncCheck(!config.kotobee.encodekey, re, [B.data.book.chapter.absoluteURL], function (e) {
                    function t() {
                        F && F.parentNode && F.parentNode.removeChild(F), _ && _.parentNode && _.parentNode.removeChild(_), c.update()
                    }
                    config.kotobee.encodekey && addFormHiddenInput("data", e, _), document.body.appendChild(_), _.submit(), setTimeout(function () {
                        F.addEventListener ? F.addEventListener("load", t, !0) : F.attachEvent && F.attachEvent("onload", t)
                    }), setTimeout(function () {
                        c.update()
                    }, 4e3)
                })
            })
        }
    }, j.pdfPrintOld = function () {
        preview ? i.update({
            error: "savePdfUnsupportedInPreview"
        }) : desktop || native || (c.update(y.get("pleaseWait"), null, {
            dots: !0
        }), window.location.href = "https://www.kotobee.com/link/pdfgenerate?url=" + ae(), setTimeout(function () {
            c.update()
        }, 4e3))
    }, j.printAsBitmap = function () {
        var r = {
            func: arguments.callee,
            args: arguments
        };
        if (!native && !mobile) {
            var o = ae();
            isKotobeeHosted() && config.kotobee.cloudid && !config.kotobee.public && (o = se());
            var e = B.data.book.chapter.title,
                t = {
                    url: o,
                    filename: e
                };
            c.update(y.get("pleaseWait"), null, {
                dots: !0
            }), h.createRequest(config.kotobee.liburl + "link/pdfgenerate/url", t, function (e) {
                if (!e) return c.update(), void i.update({
                    error: "networkError",
                    ref: r
                });
                var t = e;

                function n() {
                    try {
                        var e = this.contentWindow;
                        setTimeout(function () {
                            e.__container__ = this, e.focus(), e.print(), c.update()
                        }, 2500)
                    } catch (e) {
                        c.update()
                    }
                }
                q && q.parentNode && q.parentNode.removeChild(q), (q = document.createElement("iframe")).setAttribute("style", "display:none"), q.setAttribute("name", "printFrame"), q.id = "printContainer", q.style.visibility = "hidden", q.style.position = "fixed", q.style.right = "0", q.style.bottom = "0", document.body.appendChild(q), H && H.parentNode && H.parentNode.removeChild(H), (H = document.createElement("form")).setAttribute("style", "visibility:hidden:position:absolute"), H.setAttribute("method", "post"), H.setAttribute("action", t), H.setAttribute("target", "printFrame"), addFormHiddenInput("url", o, H), addFormHiddenInput("filename", B.data.book.chapter.title, H), addFormHiddenInput("fileext", "pdf", H), addFormHiddenInput("print", !0, H), addFormHiddenInput("format", "imghtml", H), asyncCheck(!config.kotobee.encodekey, re, [B.data.book.chapter.absoluteURL], function (e) {
                    config.kotobee.encodekey && addFormHiddenInput("data", e, H), document.body.appendChild(H), H.submit(), q.onload = n, setTimeout(function () {
                        c.update()
                    }, 8e3)
                })
            })
        }
    }, j.print = function () {
        native || mobile || ("fxl" == B.data.book.chapter.layout ? j.printAsBitmap() : (c.update(y.get("pleaseWait"), null, {
            dots: !0
        }), closePrintIframe(), re(ae(), function (e) {
            var t, n = ph.removeFilename(ae());
            return e = (e = (e = e.replace("<head>", "<head><meta http-equiv=\"Content-Security-Policy\" content=\"default-src * gap://ready file: filesystem: data:; style-src * 'self' 'unsafe-inline' filesystem: data:; script-src 'self' 'unsafe-inline' 'unsafe-eval' https://*.youtube.com https://*.kotobee.com http://*.youtube.com https://*.ytimg.com http://*.ytimg.com;img-src * filesystem: data: blob: android-webview-video-poster:\">")).replace("<head>", "<head><script> window.onload = function() { window.print(); parent.closePrintIframe(); }<\/script>")).replace("<head>", "<head><base href='" + n + "/'/>"), (t = document.createElement("iframe")).id = "printContainer", t.style.visibility = "hidden", t.style.position = "fixed", t.style.right = "0", t.style.bottom = "0", t.setAttribute("srcdoc", e), document.body.appendChild(t), void c.update()
        })))
    }, j.regenerateSpreads = function (e, n) {
        var r;
        e = e || {}, j.shouldBePaged(B.data.currentPageOb) && (j.regeneratingSpreads ? $ = !0 : ($ = !1, v.getOb("epubContainer").style.opacity = 0, j.showLoader(), B.data.currentPageOb.pages && B.data.currentPageOb.pages.length && (r = currentVIndex / B.data.currentPageOb.pages.length), z && clearTimeout(z), z = setTimeout(function () {
            j.regeneratingSpreads = !0, U && clearTimeout(U), U = setTimeout(function () {
                j.regeneratingSpreads = !1, j.regenerateSpreads(e, n)
            }, 2e4), j.resetPages(), j.preloadEdgeChapters(function () {
                j.getPageOb({
                    page: currentIndex
                }, function (e) {
                    var t = {};
                    t.viewMode = B.data.settings.viewMode, t.vIndex = currentVIndex, r && e.pages && (t.vIndex = Math.floor(e.pages.length * r), t.vIndex != currentVIndex && S.search({
                        vi: t.vIndex
                    })), t.reload = !0, j.loadChapterByIndex(currentIndex, t).then(function () {
                        j.regeneratingSpreads = !1, clearTimeout(U), $ ? setTimeout(function () {
                            $ = !1, j.regenerateSpreads(t, n)
                        }, 100) : (n && n(), v.getOb("epubContainer").style.opacity = 1, j.hideLoader())
                    })
                })
            })
        }, e.resize ? 600 : 30)))
    }, j.resetPages = function (e) {
        e = e || {};
        for (var t = 0; t < W.length; t++) W[t].pages = null;
        return s.reset(), e.buildChapterMode ? s.initialize({
            first: R,
            last: N
        }) : B.data.book.meta.rendition && "pre-paginated" == B.data.book.meta.rendition.layout ? s.initialize({
            first: R,
            last: N
        }) : "single" == B.data.settings.viewMode && B.data.settings.pageScroll ? s.initialize({
            first: R,
            last: N
        }) : void 0
    }, j.getIndexFromTocElem = function (e) {
        for (var t = e.getAttribute("href"), n = document.getElementById("chaptersInner").getElementsByTagName("a"), r = 0, o = 0; o < n.length && (n[o].getAttribute("href") != t || (r++ , e != n[o])); o++);
        return j.indexFromUrl(e.getAttribute("href"), {
            order: r
        })
    }, j.highlightCurrentTocChapter = function () {
        mobile && d.get("nav").resize();
        var e = B.data.currentPageOb.index;
        if (B.data.book) {
            for (var t = B.data.book.chapter.url, n = document.getElementById("chaptersInner").getElementsByTagName("a"), r = [], o = 0; o < n.length; o++) - 1 != n[o].getAttribute("href").indexOf(t) && j.getIndexFromTocElem(n[o]) == e && r.push({
                elem: n[o],
                id: ph.getHash(n[o].getAttribute("href"))
            });
            j.deApplySelectedChapter(), r.length && j.applySelectedChapter(r[0].elem.parentNode)
        }
    }, j.applySelectedChapter = function (e, t) {
        if (null == t && (t = !0), addClass(e = e || j.getElem, "selected"), addClass(e, "currentChapter"), t) {
            for (var n = e, r = v.getOb("chaptersContent");
                (e = e.parentNode) && e != r;) "li" == e.nodeName.toLowerCase() && addClass(e, "expanded");
            overflowScroll ? r.scrollTop = Math.round(n.offsetTop) : k.$getByHandle("chapters").scrollTo(0, Math.round(n.offsetTop - r.parentNode.offsetHeight / 2), !1)
        }
    }, j.deApplySelectedChapter = function () {
        for (var e = v.getOb("chaptersContent"), t = e.getElementsByClassName("selected"), n = t.length; n--;) removeClass(t[n], "selected");
        for (n = (t = e.getElementsByClassName("currentChapter")).length; n--;) t[n].removeAttribute("name")
    }, j
}]);

app.factory("context", ["$interpolate", "skeleton", "cssParser", "cache", "$rootScope", "stg", "$injector", "$http", function (e, i, s, A, O, B, v, l) {
    var b, L, N, y, M = {};

    function r(n, r) {
        var o;
        null != B.data.currentPageOb.currentVIndex && (isNaN(B.data.currentPageOb.currentVIndex) || (o = B.data.currentPageOb.vIndex, B.data.currentPageOb.vIndex = currentVIndex)), i.getNextIndex(B.data.currentPageOb, function (t) {
            var e;
            null != o && (B.data.currentPageOb.vIndex = o), t && (e = [n, t.leftOb]), asyncCheck(!t, b.arePagesAppearingTogether, e, function (e) {
                if (e && t) return r("next");
                null != currentVIndex && (B.data.currentPageOb.vIndex = currentVIndex), i.getPrevIndex(B.data.currentPageOb, function (e) {
                    null != o && (B.data.currentPageOb.vIndex = o);
                    b.getCurrentView(e.leftOb.index, {
                        vIndex: e.leftOb.vIndex
                    });
                    b.arePagesAppearingTogether(n, e.leftOb, function (e) {
                        return r(e ? "prev" : null)
                    })
                })
            })
        })
    }

    function o(e) {
        if (!(e = e || {}).reload) {
            var t = A.getOb("epubContainer");
            null != t.style.opacity && (t.style.opacity = null, b.hideLoader()), native && (t.scrollTop = 0);
            var n = document.getElementsByClassName("activeSlidePage");
            if (n.length) {
                var r = n[0];
                "navSlide" == B.data.settings.navAnim ? r.style.transform = null : "navFade" == B.data.settings.navAnim ? r.style.opacity = null : "navFlip" == B.data.settings.navAnim ? autoprefixTransform(r, null) : "navBookBlock" == B.data.settings.navAnim && (deleteTransform(r), deleteTransform(t), t.style.opacity = null);
                for (var o = 0; o < t.children.length; o++)
                    if ("shadow" == t.children[o].className) {
                        t.removeChild(t.children[o]);
                        break
                    }
                removeClass(r, "anim"), removeClass(r, "activeSlidePage");
                var i = '<h3 id="ebookLoaderAnim"><div class="splashSpinner large"><div class="dot1"></div><div class="dot2"></div></div></h3>';
                document.getElementById("nextPage").innerHTML = i, document.getElementById("prevPage").innerHTML = i
            }
        }
    }

    function a(r, o) {
        if (N) o(s.applyRoot(r, N));
        else {
            var i = isKotobeeHosted(),
                e = "lib/ionic/release/css/ionic.min.css";
            i && (e = "/bundles/vijualib/templates/reader/cloud/lib/ionic/release/css/ionic.min.css"), l.get(e).success(function (e) {
                if (L = e, i) l.get("/bundles/vijualib/templates/reader/cloud/css/styles.min.css").success(function (e) {
                    N = e, a()
                }).error(function () {
                    o(s.applyRoot(r, N))
                });
                else {
                    var t = "css/styles.min.css",
                        n = "http://localhost:8080" == window.location.origin;
                    n && (t = "css/style.css"), l.get(t).success(function (e) {
                        N = e, n ? l.get("css/base.css").success(function (e) {
                            N = e + " " + N, l.get("css/kotobeeInteractive.css").success(function (e) {
                                N += e, a()
                            }).error(function () {
                                a()
                            })
                        }).error(function () {
                            a()
                        }) : a()
                    }).error(function () {
                        o(s.applyRoot(r, N))
                    })
                }
            }).error(function () {
                o(s.applyRoot(r, N))
            })
        }

        function a() {
            o(N)
        }
    }

    function c(t, n) {
        t = t || {};
        t.container && (b.shouldBePaged(t.leftOb) && (t.leftOb && (t.leftOb.viewport = v.get("spread").getViewport(t.leftOb)), t.rightOb && (t.rightOb.viewport = v.get("spread").getViewport(t.rightOb))), t.simulate ? d(t, n) : b.getPageOb({
            page: t.leftOb.index
        }, function (e) {
            copyObjToObj(e, t.leftOb, {
                onlyIfNotExists: !0
            }), t.leftOb.pages && t.leftOb.vIndex < 0 && (t.leftOb.vIndex += t.leftOb.pages.length), t.rightOb ? b.getPageOb({
                page: t.rightOb.index
            }, function (e) {
                copyObjToObj(e, t.rightOb, {
                    onlyIfNotExists: !0
                }), t.rightOb.pages && t.rightOb.vIndex < 0 && (t.rightOb.vIndex += t.rightOb.pages.length), d(t, n)
            }) : d(t, n)
        }))
    }

    function d(t, e) {
        t.id = Math.round(999 * Math.random());
        var n = t.container,
            r = t.leftOb,
            o = t.rightOb,
            i = r.url,
            a = ph.removeHash(window.location.href),
            s = document.createElement("html"),
            l = document.createElement("head");
        ios && (l.innerHTML = "<meta http-equiv=\"Content-Security-Policy\" content=\"default-src * filesystem: data:; style-src 'self' 'unsafe-inline' filesystem: data:; script-src 'self' 'unsafe-inline' 'unsafe-eval' https://*.youtube.com http://*.youtube.com https://*.ytimg.com http://*.ytimg.com;img-src * filesystem: data: file:\">"), s.appendChild(l);
        var c = document.createElement("base"),
            d = document.getElementsByTagName("base");
        if (d.length)
            for (var u = 0; u < d.length; u++) {
                var f = d[u].getAttribute("href");
                if (f) {
                    a = 0 == f.indexOf("/") || isAbsoluteURLPath(f) ? f : ph.join(window.location.origin, f);
                    break
                }
            }

        function p(e) {
            var t = "";
            L && (t += L);
            var n = " body{margin:0; padding-top:0px;} #epubContainer{padding-top:0px;position:absolute;top:0;left:0;right:0;bottom:0 !important}";
            return mobile || (n += "body, #epubContainer{padding-top:1px;}"), t += n, t += "html,#epubContainer{overflow-y:" + (mobile ? "hidden" : "auto") + "}", t += "#epubScroller{position: static;height: 100%;width: 100%;box-sizing: border-box;}", !mobile && B.data.settings.rtl && (t += "#epubContainer{overflow:auto !important}"), mobile && (t += " #epubContent ::-webkit-scrollbar {display: none;} #epubContent {-ms-overflow-style: none;overflow: -moz-scrollbars-none;}"), t += "#epubScroller{padding:" + getElemPadding(A.getOb("epubContainer").getElementsByClassName("scroll")[0], {
                computed: !0
            }).left + "px}", r.viewport && r.viewport.width && (t += "#epubContent{width:" + r.viewport.width + "px;height:" + r.viewport.height + "px; overflow:hidden}", t += "html{overflow-x:" + (mobile ? "hidden" : "auto") + "}", t += " #epubInner{height:0;} ", t += " #spreadL,#spreadR{position:absolute !important; top:0 !important;font-size: 1em !important;overflow:hidden;}"), r.viewport || mobile || (t += "#epubScroller{overflow:auto}"), t += " #epubContent{padding:0px; line-height: 1.6;} ", t += " #epubContent{box-sizing:border-box; font-size:" + B.data.settings.textSize + " !important;}", t += " #epubContent.hide{display:block !important}", t += " #epubContent section{margin-top:0 !important}", B.data.settings.textSize && (t += " #epubContent{font-size: " + B.data.settings.textSize + "}"), t += N, t = (t += e = e || "").replace("background-color:#e7e3d9 !important;", "")
        }

        function h() {
            var e = arguments[2];
            return 0 == e.indexOf('"') && (e = e.split('"')[1]), 0 == e.indexOf("'") && (e = e.split("'")[1]), 0 == e.indexOf("&quot;") && (e = e.split("&quot;")[1]), -1 != e.indexOf("http://") ? arguments[0] : -1 != e.indexOf("https://") ? arguments[0] : 0 == e.indexOf("data:") ? arguments[0] : (e = ph.relative(ph.removeFilename(i), e), arguments[1] + e + arguments[3])
        }
        c.setAttribute("href", ph.join(a, ph.removeFilename(i)) + "/"), t.simulate || l.appendChild(c);
        var g = document.createElement("div");
        g.className = "stylesEnabled", g.id = "epubContainer";
        var m = document.createElement("div");
        m.id = "epubScroller", m.className = "scroll", g.appendChild(m);
        var v = document.createElement("div");

        function b(e) {
            e = (e = (e = (e = e || "").replace(/<div class="answer correct">/g, '<div class="answer">')).replace(/<div class="correct">/g, "<div>")).replace(/<p class="explanation">.*?<\/p>/g, ""), t.simulate || (e = e.replace(/(style=".*?url\()(.+?)(\).*?")/g, h));
            isKotobeeHosted();
            O.readerApp || t.simulate || (e = (e = e.replace(/src="(?!http[s]?\:\/\/)([^"]*)?"/g, function (e, t) {
                return 'src="' + ph.relative(i, t) + '"'
            })).replace(/xlink:href="(?!http[s]?\:\/\/)([^"]*)?"/g, function (e, t) {
                return 'xlink:href="' + ph.relative(i, t) + '"'
            }));
            r.title;
            return e
        }
        v.id = "epubInner", m.appendChild(v);
        var y = document.createElement("style");
        y.setAttribute("type", "text/css"), r.pages && null != r.vIndex ? r.content = r.pages[r.vIndex].content : !r.content && r.raw && (r.content = r.raw), r.content = b(r.content), o && (o.pages && null != o.vIndex ? o.content = o.pages[o.vIndex].content : !o.content && o.raw && (o.content = o.raw), o.content = b(o.content)), t.cached = !1, v = M.populateSpreads(v, t), t.cached || (r.styles = r.styles.replace(/#spreadR/g, "#spreadL"), t.simulate || (r.styles = r.styles.replace(/(url\()(.*?)(\))/g, h)), o && (o.styles = o.styles.replace(/#spreadL/g, "#spreadR"), t.simulate || (o.styles = o.styles.replace(/(url\()(.*?)(\))/g, h))));
        r.styles;
        var w = p(r.styles);
        !t.cached && o && (w += " " + p(o.styles), o.styles), y.innerHTML = w, l.appendChild(y);
        var k = v.getElementsByTagName("audio");
        for (u = k.length; u--;) {
            try {
                k[u].pause(), k[u].children.length || (k[u].src = "")
            } catch (e) { }
            var x = k[u].parentNode;
            x.previousSibling && hasClass(x.previousSibling, "playBtn") && removeClass(x.previousSibling, "hide"), x.removeChild(k[u])
        }
        var C = v.getElementsByTagName("video");
        for (u = C.length; u--;) {
            try {
                C[u].pause(), C[u].children.length || (C[u].src = "")
            } catch (e) { }
            var I = C[u].parentNode;
            I.previousSibling && hasClass(I.previousSibling, "playBtn") && removeClass(I.previousSibling, "hide"), I.removeChild(C[u])
        }
        var E = document.createElement("body");
        B.data.settings.rtl && addClass(E, "rtl"), addClass(E, r.layout), addClass(E, t.view), E.appendChild(g), s.appendChild(E);
        var T = document.createElement("iframe");
        if (T.setAttribute("srcdoc", s.outerHTML), T.style.visibility = "hidden", detectIE() || native && ios) {
            var S = "javascript: window.frameElement.getAttribute('srcdoc');";
            T.setAttribute("src", S), T.contentWindow && (T.contentWindow.location = S)
        }
        T.onload = function () {
            T.style.visibility = "visible", P(T, t)
        }, n && (n.innerHTML = "", n.appendChild(T), e && e(T))
    }

    function P(e, t) {
        var n = (t = t || {}).leftOb,
            r = {},
            o = !!t.rightOb,
            i = e.contentDocument,
            a = i.getElementById("epubContent"),
            s = i.getElementById("epubContainer");
        if (M.setContentSize(a, t), n.viewport) {
            r.epubContainer = s, r.epubContent = a, r.viewport = n.viewport, r.view = o ? "double" : "single", r.stylesFromEpubContent = !0;
            var l = v.get("nav").getFxlFit(r, "FROM CONTEXT"),
                c = l.scrollContainer;
            autoprefixTransform(c, "scale(" + l.bestFit + ")"), autoprefixTransformOrigin(c, "top left;"), c.style.height = "100%", v.get("nav").fxlAdjustAlignment(r)
        } else {
            deleteTransform(a);
            var d = i.getElementById("spreadL");
            d.style.width = d.style.height = null;
            var u = i.getElementById("spreadR");
            u && u.parentNode.removeChild(u)
        }
    }
    return M.initialize = function () {
        b = v.get("chapters")
    }, M.startTransition = function (t, n) {
        o(t), setTimeout(function () {
            var e = {
                animID: y = Math.round(1e4 * Math.random()),
                reload: t.reload,
                pageOb: t.leftOb
            };
            ! function p(h, g) {
                h = h || {};
                if (h.reload) return void (g && g());
                var m = A.getOb("epubContainer");
                if (null == currentIndex) return m.style.opacity = 0, b.showLoader(), void (g && g());
                r(h.pageOb, function (e) {
                    if (!e) return m.style.opacity = 0, h.pageOb.index != currentIndex && b.showLoader(), void (g && g());
                    var t = document.getElementById(e + "Page"),
                        n = t.getElementsByTagName("iframe");
                    if (n.length) {
                        var r = n[0].contentDocument.getElementById("epubContent"),
                            o = n[0].srcdoc;
                        if (r || !o) {
                            if (n[0].style.visibility = "visible", r) {
                                var i = v.get("nav").viewFromElem(r);
                                "fxl" == i.layout && P(n[0], i)
                            }
                        } else if (requestAnimationFrame) return void requestAnimationFrame(function () {
                            p(h, g)
                        })
                    }
                    if ("navSlide" == B.data.settings.navAnim) B.data.settings.rtl ? t.style.transform = "translate3d(" + ("next" == e ? "-" : "") + m.offsetWidth + "px,0,0)" : t.style.transform = "translate3d(" + ("next" == e ? "" : "-") + m.offsetWidth + "px,0,0)", setTimeout(function () {
                        h.animID == y && (B.data.settings.rtl ? autoprefixTransform(m, "translate3d(" + ("next" == e ? "" : "-") + m.offsetWidth + "px,0,0)") : autoprefixTransform(m, "translate3d(" + ("next" == e ? "-" : "") + m.offsetWidth + "px,0,0)"), t.style.transform = "translate3d(0,0,0)", addClass(m, "anim"), addClass(t, "anim"), addClass(t, "activeSlidePage"), setTimeout(function () {
                            h.animID == y && (autoprefixTransform(m, null), removeClass(m, "anim"), g && g())
                        }, 330))
                    }, 30);
                    else if ("navFade" == B.data.settings.navAnim) t.style.left = 0, t.style.opacity = 1, addClass(t, "anim"), addClass(t, "activeSlidePage"), setTimeout(function () {
                        h.animID == y && g && g()
                    }, 330);
                    else if ("navFlip" == B.data.settings.navAnim) addClass(m, "anim"), B.data.settings.rtl ? (autoprefixTransform(m, "next" == e ? "rotateY(90deg)" : "rotateY(-90deg)"), autoprefixTransform(t, "next" == e ? "rotateY(-90deg)" : "rotateY(90deg)")) : (autoprefixTransform(m, "next" == e ? "rotateY(-90deg)" : "rotateY(90deg)"), autoprefixTransform(t, "next" == e ? "rotateY(90deg)" : "rotateY(-90deg)")), m.style.opacity = 0, setTimeout(function () {
                        h.animID == y && (autoprefixTransform(t, null), addClass(t, "anim"), addClass(t, "activeSlidePage"), setTimeout(function () {
                            h.animID == y && (removeClass(m, "anim"), autoprefixTransform(m, null), g && g())
                        }, 630))
                    }, 30);
                    else if ("navBookBlock" == B.data.settings.navAnim) {
                        var a = document.createElement("div");
                        a.className = "shadow", a.style.opacity = 0, a.appendChild(document.createElement("div")), m.appendChild(a), addClass(m, "anim"), B.data.settings.rtl ? autoprefixTransform(m, "next" == e ? "rotateY(90deg)" : "rotateY(-90deg)") : autoprefixTransform(m, "next" == e ? "rotateY(-90deg)" : "rotateY(90deg)");
                        var s = m.style.transform || m.style.webkitTransform || m.style.mozTransform;
                        autoprefixTransformOrigin(m, "rotateY(90deg)" == s ? "right top" : "left top"), m.style.opacity = 0, addClass(t, "anim"), addClass(t, "activeSlidePage");
                        var l = document.createElement("div");
                        l.className = "shadow", l.style.opacity = .7, t.appendChild(l), a.style.opacity = 0, a.style.height = m.scrollHeight + "px", l.style.marginLeft = m.style.marginLeft, l.style.marginRight = m.style.marginRight, l.style.marginBottom = m.style.marginBottom;
                        var c = getElemMargin(m);
                        c.top || (c.top = 0);
                        var d = getComputedStyle(m).paddingTop.split("px");
                        l.style.marginTop = Number(c.top) + Number(d[0]) + "px";
                        var u = getComputedStyle(m).backgroundColor,
                            f = "rgba(0,0,0,0)" == (u = u.replace(/ /g, ""));
                        f && (m.style.backgroundColor = "#fff"), setTimeout(function () {
                            h.animID == y && (l.style.opacity = null, setTimeout(function () {
                                a.style.opacity = .4, setTimeout(function () {
                                    autoprefixTransformOrigin(m, null), l.parentNode && l.parentNode.removeChild(l), a.parentNode && a.parentNode.removeChild(a), h.animID == y && (removeClass(m, "anim"), m.style.opacity = null, f && (m.style.backgroundColor = null), autoprefixTransform(m, null), g && g())
                                }, 430)
                            }, 200))
                        }, 30)
                    }
                })
            }(e, function () {
                b.loadChapterByURL(t, function () {
                    setTimeout(function () {
                        e.animID == y && (o(t), function (n) {
                            function r(n, r, o) {
                                n && b.getPageOb({
                                    page: n.leftOb.index
                                }, function (e) {
                                    if (e) {
                                        var t = {};
                                        t.leftOb = clone(e), t.vIndex = n.leftOb.vIndex, t.vIndex < 0 && e.pages && e.pages.length && (t.vIndex += e.pages.length), b.planPageMapping(t, function (e) {
                                            a(t.leftOb.url, function (e) {
                                                t.styles = e, t.container = document.getElementById(r), t.view = b.getCurrentView(t.leftOb.index, t.leftOb).view, t.source = "from preloadchapters", c(t, o)
                                            })
                                        })
                                    }
                                })
                            }
                            b.isFirstChapter(function (t) {
                                b.isLastChapter(function (e) {
                                    e || i.getNextIndex(n.leftOb, function (e) {
                                        b.getSurroundingPages(e ? e.leftOb : null, function () {
                                            r(e, "nextPage")
                                        })
                                    }), t || i.getPrevIndex(n.leftOb, function (e) {
                                        b.getSurroundingPages(e ? e.leftOb : null, function () {
                                            r(e, "prevPage")
                                        })
                                    })
                                })
                            })
                        }(t), n && n.resolve())
                    }, mobile ? 500 : 0)
                })
            })
        })
    }, M.interruptAnimation = function () {
        o()
    }, M.simulateChapter = function (r, o) {
        a(r.url, function (e) {
            var t = document.createElement("div"),
                n = {};
            n.styles = e, n.styles += "#spreadL{left:0 !important;width:auto !important;height:auto !important;position:static !important;}", n.styles += "#epubContent{width:auto !important;height: auto !important;}", n.container = t, n.leftOb = clone(r), "double" == b.getCurrentView(n.leftOb.index).view && (n.rightOb = {}, n.rightOb.content = "<div><sections class='blankForSimulation'></sections></div>", n.rightOb.styles = "", n.rightOb.layout = "rfl"), n.simulate = !0, c(n, o)
        })
    }, M.populateSpreads = function (e, t) {
        var n = 0;
        "rfl" == (t = t || {}).leftOb.layout && "double" == t.view && (n = config.spreadSpacing ? config.spreadSpacing : 0);
        var r = t.leftContent ? t.leftContent : t.leftOb.content;
        if (!t.cached) {
            (c = document.createElement("div")).innerHTML = t.leftContent ? t.leftContent : t.leftOb.content;
            var o = c.children[0],
                i = o.children[0];
            copyAttributes(o, i), removeClass(i, "adjustedLineHeight"), i.id = "spreadL", i.style.transform = i.style.transformOrigin = i.style.height = null, t.leftOb.viewport ? (i.style.left = "0px", i.style.width = t.leftOb.viewport.width + "px", i.style.height = t.leftOb.viewport.height + "px", B.data.settings.rtl && (i.style.left = "auto", i.style.right = "0px")) : (i.style.left = null, i.style.width = null, i.style.height = null), addClass(i, t.leftOb.layout), r = c.innerHTML
        }
        var a = replaceHTML(e, r);
        if (t.rightOb) {
            var s;
            if (1 < a.children[0].children.length)
                for (var l = 0; l < a.children[0].children.length; l++)
                    if ("spreadR" == a.children[0].children[l].id) {
                        s = a.children[0].children[l];
                        break
                    }
            if (!s) {
                var c;
                (c = document.createElement("div")).innerHTML = t.rightContent ? t.rightContent : t.rightOb.content;
                var d = c.children[0];
                copyAttributes(d, s = d.children[0]), removeClass(s, "adjustedLineHeight"), s.id = "spreadR", s.style.transform = s.style.transformOrigin = s.style.height = null, a.children[0].appendChild(s)
            }
            t.rightOb.viewport && (t.leftOb.viewport && (s.style.left = t.leftOb.viewport.width + n + "px", B.data.settings.rtl && (s.style.right = s.style.left, s.style.left = "auto")), s.style.width = t.rightOb.viewport.width + "px", s.style.height = t.rightOb.viewport.height + "px", addClass(s, t.rightOb.layout))
        }
        return a
    }, M.test = function (e) { }, M.setContentSize = function (e, t) {
        var n = (t = t || {}).leftOb,
            r = t.rightOb;
        e.style.width = e.style.height = null;
        var o = 0;
        "rfl" == t.leftOb.layout && "double" == t.view && (o = config.spreadSpacing ? config.spreadSpacing : 0);
        var i = n.viewport;
        if (i) {
            if (i.width) {
                var a = i.width;
                r && r.viewport && (a += r.viewport.width + o), e.style.width = a + 0 + "px"
            }
            if (i.height) {
                var s = i.height;
                r && r.viewport && (s = Math.max(s, r.viewport.height)), e.style.height = s + 0 + 0 + "px"
            }
        }
    }, M
}]);

app.factory("crdv", ["stg", "status", "error", "translit", "$rootScope", "$http", "$ionicSideMenuDelegate", function (a, l, t, o, i, n, r) {
    var c = {};

    function s(e) {
        a.data.settings.rtl ? r.toggleLeft() : r.toggleRight()
    }

    function d() {
        try {
            return cordova, !0
        } catch (e) {
            return !1
        }
    }

    function u(o, i, a, s) {
        try {
            l.updateProgress(0), o.getEntries(function (e) {
                var r = 0;
                ! function t() {
                    if (r >= e.length) o.close(function () {
                        a && a()
                    });
                    else {
                        l.updateProgress(r / e.length);
                        var n = e[r];
                        r++ , n.getData(new zip.BlobWriter, function (e) {
                            try {
                                if ("/" == n.filename.substr(-1)) return void t();
                                c.writeFile(e, ph.join(i, n.filename), c.libEbooksDirectory, function () {
                                    t()
                                })
                            } catch (e) {
                                s && s()
                            }
                        }, function (e, t) { })
                    }
                }()
            })
        } catch (e) {
            s && s()
        }
    }

    function f(e) {
        switch (e.code) {
            case FileError.QUOTA_EXCEEDED_ERR:
                "QUOTA_EXCEEDED_ERR";
                break;
            case FileError.NOT_FOUND_ERR:
                "NOT_FOUND_ERR";
                break;
            case FileError.SECURITY_ERR:
                "SECURITY_ERR";
                break;
            case FileError.INVALID_MODIFICATION_ERR:
                "INVALID_MODIFICATION_ERR";
                break;
            case FileError.INVALID_STATE_ERR:
                "INVALID_STATE_ERR";
                break;
            default:
                "Unknown Error"
        }
    }
    return c.initialize = function (e, l) {
        putTest("in1"), d() ? (putTest("in3"), a.data.cordova = !0, putTest("in4"), putTest("in5"), e.$$phase || e.$apply(), a.copyLocalToNativeStorage(), c.initAds(), c.initDirectories(function () {
            n.get("permit.json").success(function (a) {
                function s() {
                    var e = {
                        error: "errorAndCallback",
                        msg: "noAppPermit",
                        cb: c.exit
                    };
                    t.update(e), i.$$phase || i.$apply()
                } ("udid" == a.type ? window.plugins.uniqueDeviceID.get : "mac" == a.type ? window.MacAddress.getMacAddress : window.plugins.imeiplugin.getImei)(function (e) {
                    e || s();
                    try {
                        for (var t, n = a.list.split("\n"), r = 0; r < n.length; r++) {
                            var o = n[r].trim().split(" ")[0].trim(),
                                i = n[r].trim().split("/")[0].trim();
                            if (o == e || i == e) {
                                t = !0;
                                break
                            }
                        }
                    } catch (e) {
                        return void s()
                    }
                    t ? l() : s()
                }, s)
            }).error(function () {
                l()
            })
        }), document.addEventListener("menubutton", s, !1), e.$$phase || e.$apply()) : l()
    }, c.showKeyboard = function () {
        try {
            if (!d()) return;
            cordova.plugins.Keyboard.show()
        } catch (e) { }
    }, c.hideKeyboard = function () {
        try {
            cordova.plugins.Keyboard.close()
        } catch (e) { }
    }, c.tts = function (e, t) {
        var n = {
            text: e,
            rate: 1
        };
        try {
            "iOS" == window.device.platform && ("9" == window.device.version.charAt(0) ? n.rate = 1.5 : "10" == window.device.version.substr(0, 2) ? n.rate = 1.5 : "11" == window.device.version.substr(0, 2) && (n.rate = 1.5))
        } catch (e) { }
        TTS.speak(n, function () {
            t(!0)
        }, function (e) {
            c.toast(o.get("ttsUnavailableMsg")), t(!1)
        })
    }, c.toast = function (e) {
        d() && window.plugins.toast.showShortBottom(e)
    }, c.copyToClipboard = function (e) {
        if (d()) try {
            cordova.plugins.clipboard ? cordova.plugins.clipboard.copy(e) : window.plugins.clipboard.copy(e), c.toast(o.get("copiedToClipboard"))
        } catch (e) { }
    }, c.share = function (e) {
        if (d()) try {
            window.plugins.socialsharing.share(e)
        } catch (e) { }
    }, c.createPDF = function (e, t, n) {
        try {
            var r = new jsPDF;
            margins = {
                top: 10,
                bottom: 0,
                left: 10,
                width: 180
            }, r.fromHTML(e, margins.left, margins.top, {
                width: margins.width
            }, function (e) {
                try {
                    cordova, c.docDirectory.getFile(t, {
                        create: !0
                    }, function (e) {
                        e.createWriter(function (e) {
                            e.onwriteend = function (e) { }, e.onerror = function (e) { }, e.write(r.output()), c.toast(o.get("pdfSaved"))
                        }, f)
                    }, f)
                } catch (e) {
                    try {
                        setTimeout(function () {
                            r.save(t)
                        }, 1e3)
                    } catch (e) { }
                }
            }, margins)
        } catch (e) { }
    }, c.initDirectories = function (n) {
        function r(e) { }
        window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem, window.requestFileSystem(window.TEMPORARY, 10485760, function (e) {
            c.cacheDirectory = e.root, window.requestFileSystem(window.PERSISTENT, 0, function (e) {
                c.docDirectory = e.root;
                var t = "kotobeeReader";
                i.readerApp ? t = "kotobeeReaderApp" : config.kotobee.cloudid && (t += config.kotobee.cloudid), c.docDirectory.getDirectory(t, {
                    create: !0
                }, function (e) {
                    c.appDirectory = e, c.appDirectory.getDirectory("library", {
                        create: !0
                    }, function (e) {
                        c.libEbooksDirectory = e, c.appDirectory.getDirectory("local", {
                            create: !0
                        }, function (e) {
                            c.localDirectory = e, c.localDirectory.getDirectory("books", {
                                create: !0
                            }, function (e) {
                                c.localBooksDirectory = e, c.localDirectory.getDirectory("thumbs", {
                                    create: !0
                                }, function (e) {
                                    c.localThumbsDirectory = e, c.libEbooksDirectory.getFile(".nomedia", {
                                        create: !0
                                    }, function () {
                                        c.localDirectory.getFile(".nomedia", {
                                            create: !0
                                        }, function () {
                                            n()
                                        }, r)
                                    }, r)
                                }, r)
                            }, r)
                        }, r)
                    }, r)
                }, r)
            }, r)
        }, r)
    }, c.exit = function () {
        d() && (navigator.app ? navigator.app.exitApp() : navigator.device && navigator.device.exitApp())
    }, c.applyExistingBooksOld = function (o, i) {
        if (d()) {
            var e = c.libEbooksDirectory.createReader(),
                a = [],
                s = function () {
                    e.readEntries(function (e) {
                        if (e.length) a = a.concat(function (e) {
                            return Array.prototype.slice.call(e || [], 0)
                        }(e)), s();
                        else {
                            for (var t = 0; t < a.length; t++)
                                for (var n = a[t].name, r = 0; r < o.length; r++) o[r].path == n && (o[r].exists = !0);
                            i()
                        }
                    }, function (e) { })
                };
            s()
        } else i()
    }, c.applyExistingBooks = function (e) {
        if ((native || desktop) && (e || a.data.currentLibrary && (e = a.data.currentLibrary.books), e)) {
            for (var t = 0; t < e.length; t++) e[t].exists = !1;
            for (var n = 0; n < a.data.downloaded.length; n++) a.data.downloaded[n].favorite = !1;
            for (n = 0; n < a.data.downloaded.length; n++) {
                var r = a.data.downloaded[n].path;
                a.data.downloaded[n].favorite = null;
                for (t = 0; t < e.length; t++)
                    if (e[t].path == r) {
                        e[t].exists = !0, a.data.downloaded[n].favorite = e[t].favorite;
                        break
                    }
            }
            i.$$phase || i.$apply()
        }
    }, c.vibrate = function (e) {
        d() && (config.noVibration || (e = e || 40, ios || (ios && (e /= 4), "high" == config.vibrationIntensity ? e = 40 : "medium" == config.vibrationIntensity ? e = 28 : "low" == config.vibrationIntensity && (e = 15), navigator && navigator.notification && navigator.notification.vibrate && navigator.notification.vibrate(e))))
    }, c.deleteEbook = function (t, n) {
        c.libEbooksDirectory.getDirectory(t.path, {}, function (e) {
            e.removeRecursively(function () {
                for (var e = 0; e < a.data.downloaded.length; e++) a.data.downloaded[e].path == t.path && (t.exists = !1, a.data.downloaded.splice(e, 1), e--);
                a.writeSlot("downloaded" + a.getBookId(!0), a.data.downloaded, function () {
                    n()
                })
            }, function (e) { })
        }, f)
    }, c.writeEbook = function (n, r, e, o) {
        var i = e;
        c.libEbooksDirectory.getDirectory(r.path, {
            create: !0
        }, function (e) {
            var t = ph.join(r.path, n);
            c.writeFile(i, t, c.libEbooksDirectory, function () {
                c.unzipViaPlugin(ph.join(c.libEbooksDirectory.nativeURL, t), ph.join(c.libEbooksDirectory.nativeURL, r.path, "EPUB"), function (e) {
                    r.exists = !0, a.data.downloaded.push(r), c.applyExistingBooks(a.data.currentLibrary.books), a.writeSlot("downloaded" + a.getBookId(!0), a.data.downloaded, function () {
                        o()
                    })
                }, function (e) { })
            })
        })
    }, c.unzip = function (e, t, n, r) {
        c.unzipBlob(e, t, n, r)
    }, c.unzipBlob = function (e, t, n, r) {
        zip.workerScriptsPath = "lib/zip/", zip.createReader(new zip.BlobReader(e), function (e) {
            u(e, t, n, r)
        })
    }, c.unzipViaPlugin = function (e, t, n, r) {
        zip.unzip(e, t, n, r)
    }, c.unzipData64 = function (e, t, n, r) {
        zip.workerScriptsPath = "lib/zip/", setTimeout(function () {
            zip.createReader(new zip.Data64URIReader(e), function (e) {
                setTimeout(function () {
                    u(e, t, n, r)
                }, 100)
            })
        }, 100)
    }, c.writeFile = function (a, e, t, s) {
        var n = ph.basename(e);
        try {
            c.getDirectoryRelativeToBooks(ph.normalize(e), t, function (e) {
                try {
                    e.getFile(n, {
                        create: !0
                    }, function (e) {
                        e.createWriter(function (r) {
                            var o = 0,
                                i = 4194304;
                            ! function t() {
                                var e = Math.min(i, a.size - o),
                                    n = a.slice(o, o + e);
                                r.write(n), o += e, r.onwrite = function (e) {
                                    o < a.size ? t() : s && s(!0)
                                }, r.onerror = function (e) {
                                    s && s(!1)
                                }
                            }()
                        }, f)
                    }, function (e, t) { })
                } catch (e) { }
            })
        } catch (e) { }
    }, c.getDirectoryRelativeToBooks = function (e, t, n) {
        t = t || c.libEbooksDirectory;
        var r = e.split("/");
        if (1 != r.length) {
            var o = r[0];
            t.getDirectory(o, {
                create: !0
            }, function (e) {
                r.shift();
                var t = r.join("/");
                c.getDirectoryRelativeToBooks(t, e, n)
            }, function (e) { })
        } else n(t)
    }, c.openInNativeBrowser = function (e, t) {
        e = e.toString();
        try {
            if (t) return void window.open(e, "_system");
            try {
                if (cordova.InAppBrowser) {
                    window.plugins.toast.show("  " + o.get("loadingExternalPage") + "  ", 4e4, "bottom");
                    var n = ios ? "yes" : "no",
                        r = cordova.InAppBrowser.open(e, "_blank", "location=no,hidden=no,clearcache=yes,closebuttoncaption=Go back to App,toolbar=" + n + ",presentationstyle=formsheet");
                    return r.addEventListener("loadstart", function () { }), r.addEventListener("loadstop", function () {
                        ios || window.plugins.toast.hide()
                    }), void r.addEventListener("loaderror", function () {
                        if (!ios) {
                            try {
                                r && r.close()
                            } catch (e) { }
                            window.plugins.toast.hide(), window.plugins.toast.showLongBottom("  " + o.get("errorLoadingPage") + "  ")
                        }
                    })
                }
            } catch (e) { }
            try {
                if (window.inAppBrowserXwalk) {
                    return void window.inAppBrowserXwalk.open(e, {
                        toolbarColor: "#FFFFFF",
                        toolbarHeight: "40",
                        closeButtonText: "< Close",
                        closeButtonSize: "25",
                        closeButtonColor: "#000000",
                        openHidden: !1
                    })
                }
            } catch (e) { }
            return void window.open(e, "_system")
        } catch (e) { }
    }, c.initAds = function () {
        try {
            if (!admob) return
        } catch (e) {
            return
        }
        config.kotobee.admobId && (admob.setOptions({
            publisherId: config.kotobee.admobId
        }), admob.createBannerView())
    }, c
}]);

app.factory("cssParser", ["$http", "cache", "stg", "workers", function (s, e, t, k) {
    var l, i, x = {},
        r = [];

    function C(e) {
        e = e.replace(/\r?\n|\r/g, " ");
        var t, n = [],
            r = "key",
            o = [0],
            i = n,
            a = [i];

        function s(e) {
            return e = e || 1, o[o.length - e]
        }
        for (var l = 0; l < e.length; l++) t = t || {}, "}" == e[l] ? ("properties" == r && (t.properties = e.substring(s(), l).trim(), t.keys && t.keys.length && (t.keys[t.keys.length - 1] = t.keys[t.keys.length - 1].trim()), i.push(t)), "key" == r && 1 < a.length && (a.pop(), i = a[a.length - 1]), t = null, r = "key", o.push(l + 1)) : "{" == e[l] ? ("properties" == r && (t.children || (t.children = []), t.keys && t.keys.length && (t.keys[t.keys.length - 1] = t.keys[t.keys.length - 1].trim()), n.push(t), i = t.children, a.push(i), (t = {
            keys: []
        }).keys.push(e.substring(s(), l).trim())), o.push(l + 1), r = "properties") : "key" == r && (t.keys || (t.keys = [""]), "," == e[l] ? t.keys.push("") : t.keys[t.keys.length - 1] += e[l]);
        return n
    }
    return x.clearCache = function () {
        r = []
    }, x.inject = function (o, b, e) {
        b = b || {};
        var i = "";

        function a(e, t) {
            for (var n = 0; n < r.length; n++)
                if (r[n].url == e && r[n].id == t) return r.push(r[n]), r[n]
        }

        function y(e) {
            7 < r.length && r.shift(), r.push(e)
        }

        function w(e) {
            var t = e;
            return t = (t = (t = (t = (t = t.replace(/\/\*(?:(?!\*\/)[\s\S])*\*\/|[\r\n\t]+/g, "")).replace(/ {2,}/g, " ")).replace(/ ([{:}]) /g, "$1")).replace(/([;,]) /g, "$1")).replace(/ !/g, "!")
        } (function t(g, n) {
            if (!g) return g = {}, void v();
            if ("path" == g.type) {
                var m = g.val;
                if (!m || 0 <= m.indexOf("EPUB/css/kotobeeInteractive.css")) return void v();
                var e = a(m, b.id);
                e ? v(e.val) : s.get(g.dontCache ? m : cacheBust(m)).success(function (e) {
                    r(e, m, "url")
                }).error(function () {
                    v()
                })
            } else r(g.val, g.root);

            function r(e, t, n) {
                if (k.supported()) {
                    var r = {};
                    return r.data = e, r.rootPath = t, r.cssOb = g, r.desktop = desktop, void k.call("cssParser", "parse", [r], function (e) {
                        "url" == n && y({
                            url: m,
                            val: e,
                            id: b.id
                        }), v(e)
                    })
                }
                e = (e = (e = (e = x.applyRoot(t, e)).replace(/\/\*[\s\S]*?\*\//g, "")).replace(/<!--/g, "")).replace(/-->/g, "");
                var o = "spreadL";
                b.id && (o = b.id);
                for (var i = (g.parentClass ? g.parentClass : "#epubContainer.stylesEnabled") + " #" + o, a = C(e), s = "", l = 0; l < a.length; l++) {
                    var c = h(a[l].keys);
                    if (s += c.joined, s += "{", a[l].children && a[l].children.length)
                        for (var d = 0; d < a[l].children.length; d++) {
                            var u = h(a[l].children[d].keys);
                            if (s += u.joined, s += "{" + a[l].children[d].properties + "}", u.hasBody) {
                                var f = p(a[l].properties);
                                f && (s += "#epubContainer{" + f + "}")
                            }
                        }
                    if (a[l].properties && (s += a[l].properties), s += "}", c.hasBody) {
                        var f = p(a[l].properties);
                        f && (s += "#epubContainer{" + f + "}")
                    }
                }

                function p(e) {
                    if (e) {
                        for (var t = e.split(";"), n = "", r = 0; r < t.length; r++) t[r] && -1 != t[r].trim().indexOf("background") && (n = t[r].trim().replace(/url\(.*?\)/g, "") + ";");
                        return n
                    }
                }

                function h(e) {
                    for (var t = {}, n = 0; n < e.length; n++)
                        if (e[n]) {
                            var r = e[n].trim();
                            0 != r.indexOf("body") ? 0 != r.indexOf("html") ? 0 != r.indexOf("@media") && 0 != r.indexOf("@") && isNaN(r[0]) && "from" != r && "to" != r && (e[n] = i + " " + r) : e[n] = r.replace("html", i) : (e[n] = r.replace("body", i), 0 == e[n].indexOf("html") && (e[n] = e[n].replace("html", i)), t.hasBody = !0)
                        }
                    return t.joined = e.join(","), t
                }
                e = w(e = s), "url" == n && y({
                    url: m,
                    val: e,
                    id: b.id
                }), v(e)
            }

            function v(e) {
                e && (i += e), ++l < o.length ? t(o[l], n) : n && n(i)
            }
        })(o[l = 0], e)
    }, x.applyRoot = function (r, e) {
        return e.replace(/url\((.*?)\)/g, function (e, t) {
            var n = "";
            return 0 == t.indexOf('"') ? (t = t.substr(1, t.length - 2), n = '"') : 0 == t.indexOf("'") && (t = t.substr(1, t.length - 2), n = "'"), -1 != t.indexOf("http://") ? e : -1 != t.indexOf("https://") ? e : 0 == t.indexOf("data:") ? e : "url(" + n + (t = ph.join(r, t)) + n + ")"
        })
    }, x.getStyles = function () {
        return i
    }, x.replaceStyles = function (e, t) {
        var n;
        t = t || {};
        for (var r = document.getElementsByTagName("style"), o = 0; o < r.length; o++)
            if (null != r[o].getAttribute("kotobee")) {
                n = r[o];
                break
            }
        i = e, t.append && (i = n.innerHTML + " " + e), n = replaceHTML(n, i)
    }, x.hideStyles = function () {
        removeClass(e.getOb("epubContainer"), "stylesEnabled")
    }, x.showStyles = function () {
        addClass(e.getOb("epubContainer"), "stylesEnabled")
    }, x.stylize = function (e, t) {
        for (var n = document.getElementById(e), r = n.getAttribute("style"), o = Object.keys(t), i = r ? r + ";" : "", a = 0; a < o.length; a++) i += o[a] + ":" + t[o[a]] + ";";
        n.setAttribute("style", i)
    }, x
}]);

app.factory("design", ["cssParser", "$interpolate", "stg", "$http", function (n, r, e, o) {
    var t, i, a = {};
    return a.getBgStyle = function () {
        if (!t) {
            var e = "";
            isKotobeeHosted() && (e = publicRoot), "stretch" == (t = {
                "background-image": 'url("' + (config.splashBg ? e + "imgUser/" + config.splashBg : "none") + '")',
                "background-color": config.splashBgColor ? config.splashBgColor : "#3f3248",
                color: (config.splashColor ? config.splashColor : "#fff") + " !important",
                "background-size": config.splashBgSize ? config.splashBgSize : "cover"
            })["background-size"] && (t["background-size"] = "100% 100%")
        }
        return t
    }, a.convertTabs = function () {
        if (config.tabs)
            for (var e = 0; e < config.tabs.length; e++) {
                var t = document.createElement("div");
                if (t.setAttribute("data-kotobee", config.tabs[e]), config.tabs[e] = kInteractive.readData(t), config.tabs[e].icon) {
                    var n = "";
                    isKotobeeHosted() && (n = publicRoot), config.tabs[e].iconStyle = {
                        "background-image": "url(" + n + "imgUser/" + config.tabs[e].icon + ")"
                    }
                }
            }
    }, a.injectUserCSS = function (n) {
        function t() {
            for (var e = document.getElementsByTagName("style"), t = 0; t < e.length; t++)
                if (e[t].hasAttribute("user")) {
                    e[t].innerHTML = r(i)(n);
                    break
                }
        }
        i ? t() : o.get(cacheBust(rootDir + "css/user.css")).success(function (e) {
            i = e, t()
        })
    }, a.stylize = function (e, t) {
        n.stylize(e, t)
    }, a
}]);

app.factory("error", ["modals", "translit", "$injector", "$location", "$ionicHistory", function (o, i, a, s, l) {
    var c = {},
        d = [];
    return c.modal = null, c.update = function (e) {
        var t, n = a.get("backend");
        if (e) switch (e.error) {
            case "s_authError":
                "library" == config.kotobee.mode && n.cloudParam("entry") ? t = {
                    mode: "login",
                    dynamic: !0,
                    animation: "slide-in-up",
                    loginOptions: {}
                } : (s.path("/login"), l.clearHistory());
                break;
            case "networkError":
                t = {
                    mode: "networkError",
                    errorOptions: {
                        ref: e.ref,
                        retry: c.retry
                    }
                };
                break;
            case "localonlyError":
                t = {
                    mode: "localOnlyError",
                    errorOptions: {
                        ref: e.ref,
                        retry: c.retry
                    }
                };
                break;
            case "errorAndCallback":
                t = {
                    mode: "generalError",
                    errorOptions: {
                        msg: i.get(e.msg),
                        cb: e.cb
                    }
                };
                break;
            default:
                t = {
                    mode: "generalError",
                    errorOptions: {
                        msg: i.get(e.error)
                    }
                }
        }
        for (var r = 0; r < d.length; r++) d[r].error == e.error && d[r].func(t);
        t && o.show(t)
    }, c.retry = function (e) {
        o.hide(), timeOut(function () {
            e.func.apply(null, e.args)
        }, 100)
    }, c.detect = function (e, t) {
        d.push({
            error: e,
            func: t
        })
    }, c.removeDetection = function (e, t) {
        for (var n = 0; n < d.length; n++)
            if (t == d[n].func && e == d[n].error) return void d.splice(n, 1)
    }, c
}]);

app.factory("initializer", ["$rootScope", "$injector", "chapterAPI", "translit", "autohide", "scorm", "workers", "cssParser", "tinCan", "book", "$location", "chapters", "error", "modals", "$ionicHistory", "$ionicPlatform", "crdv", "cache", "auth", "design", "stg", "backend", "settings", function (c, o, i, d, a, s, l, e, u, t, f, p, n, h, r, g, m, v, b, y, w, k, x) {
    var C, I, E, T = {},
        S = [];

    function A(e, t) {
        e.className = "", setTimeout(function () {
            e.className = "icon fa fa-cog fa-spin size16", setTimeout(t, 400)
        }, 500)
    }

    function O() {
        var r = require("nw.gui");
        C = C || new r.Menu({
            type: "menubar"
        });
        var e = new r.Menu;
        e.append(l({
            label: d.get("exit")
        }, a)), C.append(l({
            label: d.get("file")
        }, a, e));
        for (var t = new r.Menu, n = new r.Menu, o = w.data.languages, i = 0; i < o.length; i++) n.append(l({
            label: o[i].label
        }, s));

        function a(e) {
            this.label.toLowerCase() == d.get("exit").toLowerCase() && r.App.quit()
        }

        function s(e) {
            var t = this.label;
            setTimeout(function () {
                for (var e = 0; e < w.data.languages.length; e++)
                    if (w.data.languages[e].label == t) return w.data.settings.language = w.data.languages[e].val, x.langChanged(), void (c.$$phase || c.$apply())
            }, 300)
        }

        function l(e, t, n) {
            return t && (e.click = t), n && (e.submenu = n), new r.MenuItem(e)
        }
        t.append(l({
            label: d.get("language")
        }, a, n)), C.append(l({
            label: d.get("settings")
        }, a, t)), r.Window.get().menu = C
    }
    return T.preInitialize = function (e) {
        putTest("preInitialize"), T.initializing = !0, y.injectUserCSS(config);
        var t = T.getLogo();
        T.playAudio(), T.displayBg(), k.addListener("pathsChanged", T.playAudio), k.addListener("pathsChanged", T.displayBg);
        for (var n = document.getElementsByTagName("link"), r = 0; r < n.length; r++)
            if ("shortcut icon" == n[r].getAttribute("rel")) {
                n[r].getAttribute("href") || n[r].setAttribute("href", config.icon ? config.icon : t.logo);
                break
            }
        y.convertTabs(), putTest("preInitialize2"), v.initialize(), u.initialize(), l.initialize(), w.initialize(), s.initialize(), a.initialize(), i.initialize(), w.initializeSettings(function () {
            x.langChanged(), putTest("preInitialize3"), window.addEventListener("message", function (e) {
                var t = e.data.split(":");
                if ("next" == t[0]) p.nextChapter();
                else if ("prev" == t[0]) p.prevChapter();
                else if ("notebook" == t[0]) angular.element(document.getElementById("tabMenu")).scope().notebookClicked();
                else if ("goto" == t[0]) f.path(o.get("library").root + "/reader/chapter/" + Number(t[1]));
                else if ("zoom" == t[0])
                    for (var n = document.getElementsByTagName("meta"), r = 0; r < n.length; r++) "viewport" == n[r].getAttribute("name") && n[r].setAttribute("content", "initial-scale=" + t[1])
            }, !1), window.kbOpenChapterURL = function (e) {
                e && (-1 == e.indexOf(":/") && (e = ph.join(w.data.book.chapter.url, e)), p.applyChapterStateFromURL(e))
            }, setTimeout(function () {
                ionic.tap.requiresNativeClick = function (e) {
                    return !(!ionic.Platform.isWindowsPhone() || "A" != e.tagName && "BUTTON" != e.tagName && !e.hasAttribute("ng-click") && ("INPUT" != e.tagName || "button" != e.type && "submit" != e.type)) || !!(!e || e.disabled || /^(file|range)$/i.test(e.type) || /^(object|video|audio)$/i.test(e.tagName) || ionic.tap.isLabelContainingFileInput(e)) || ionic.tap.isElementTapDisabled(e)
                }
            }, 1e3), desktop && O(), native || (document.onkeydown = function (e) {
                "app" != config.kotobee.mode && document.activeElement && "epubContainer" == document.activeElement.id && (h.shown() || ("37" == (e = e || window.event).keyCode ? (w.data.settings.rtl ? p.nextChapter() : p.prevChapter(), c.$$phase || c.$apply()) : "39" == e.keyCode && (w.data.settings.rtl ? p.prevChapter() : p.nextChapter(), c.$$phase || c.$apply())))
            }), putTest("preInitialize4"), v.setOb("title", replaceHTML(v.getOb("title"), d.get("loading") + "..")), d.addListener(function () {
                desktop && function () {
                    if (C)
                        for (; C.items.length;) C.removeAt(0);
                    O()
                }(), I || w.data.book || w.data.book.meta || v.setOb("title", replaceHTML(v.getOb("title"), d.get("loading") + "..")), putTest("translit.addListener2"), I = !0
            }), putTest("preInitialize5"), config.kotobee.cloudid && k.getS3Paths()
        })
    }, T.displayBg = function () {
        y.stylize("home", y.getBgStyle())
    }, T.playAudio = function () {
        if (config.audio && !document.bgAudio) {
            var e = "";
            isKotobeeHosted() && (e = publicRoot);
            var t = e + "imgUser/" + config.audio,
                n = document.createElement("audio");
            n.setAttribute("data-dontclose", !0), n.setAttribute("controls", "true"), n.setAttribute("autoplay", "true"), n.setAttribute("src", t), config.audioLoop && n.setAttribute("loop", "true"), n.setAttribute("data-tap-disabled", "false");
            var r = document.createElement("source");
            r.src = t, n.appendChild(r), n.appendChild(document.createTextNode("Your browser does not support the audio element")), n.oncanplay = function () {
                kInteractive.tinCan({
                    verb: "played",
                    activity: "Audio: " + t
                })
            }, n.play(), document.bgAudio = n
        }
    }, T.getLogo = function () {
        var e = "";
        return isKotobeeHosted() && (e = publicRoot), {
            logo: config.logo ? e + "imgUser/" + config.logo : rootDir + "img/ui/defaultWelcomeLogoNew.png",
            slogan: config.slogan ? config.slogan : ""
        }
    }, T.addListener = function (e) {
        S.push(e)
    }, T.postInitialize = function (e) {
        putTest("postInitialize start"),
            function e(t) {
                return t();
                if ("localhost" == location.hostname) return t();
                var n = document.getElementById("iconDetector");
                if (preview && -1 != window.location.href.indexOf("&screenshot")) return void (-(n.style.opacity = 1) == window.location.href.indexOf("&startup") && t && t());
                A(n, function () {
                    E = E || setTimeout(function () {
                        n.style.opacity = 1, t && t()
                    }, 8e3), n.offsetWidth < 10 ? setTimeout(function () {
                        e(t)
                    }, 1e3) : (clearTimeout(E), n.style.opacity = 1, setTimeout(function () {
                        t && t()
                    }, 1e3))
                })
            }(function () {
                putTest("postInitialize start2"), k.initialize(e).then(function () {
                    if (putTest("MM"), e.$$phase || e.$apply(), "library" == config.kotobee.mode || c.readerApp) {
                        try {
                            w.readSlot("downloaded" + w.getBookId(!0), function (e) {
                                w.data.downloaded = e, putTest("MMMM")
                            })
                        } catch (e) {
                            w.data.downloaded = []
                        }
                        w.data.downloaded || (w.data.downloaded = [])
                    } else "book" == config.kotobee.mode ? (bookPath = "epub/", preview && (bookPath = config.kotobee.bookPath), bookPath = bookPath || "epub/") : "app" == config.kotobee.mode || (status.update(), n.update({
                        error: "errorAndCallback",
                        msg: "cantReadConfigFile",
                        cb: m.exit
                    }));
                    b.authenticate(e)
                }), g.registerBackButtonAction(function (e) {
                    var t = r.currentView();
                    "forgotPwd" == t.stateId ? f.path("/login") : "register" == t.stateId ? f.path("/login") : "library" == t.stateId && c.readerApp ? f.path("/app") : h.show({
                        mode: "exit",
                        backdropClickToClose: !1,
                        exitOptions: {
                            exit: function () {
                                m.exit()
                            }
                        }
                    }), c.$apply()
                }, 101), T.initialized = !0, T.initializing = !0,
                    function () {
                        for (var e = 0; e < S.length; e++) S[e].apply(this, arguments)
                    }()
            })
    }, T
}]);

app.factory("jsParser", ["$http", "cache", function (u, e) {
    var f, r, o, t = {},
        n = [];
    return t.clearCache = function () {
        n = []
    }, t.inject = function (a, e) {
        var s = [];

        function l(e) {
            for (var t = 0; t < n.length; t++)
                if (n[t].url == e) return n.push(n[t]), n[t]
        }

        function c(e) {
            5 < n.length && n.shift(), n.push(e)
        }

        function d() {
            for (var e = 0; e < s.length; e++) {
                var t;
                if (s[e]) 0 <= s[e].indexOf("window.location.href") && (t = /window\.location\.href[\s]*?=(.*?)[;\n]/g, s[e] = s[e].replace(t, "try { kbOpenChapterURL($1); } catch(er){};")), 0 <= s[e].indexOf("event.stopPropagation()") && (t = /event\.stopPropagation\(\)/g, s[e] = s[e].replace(t, "void(0)")), 0 <= s[e].indexOf("<![CDATA[") && (t = /<!\[CDATA\[/g, s[e] = s[e].replace(t, "//<![CDATA["), t = /\]\]>/g, s[e] = s[e].replace(t, "//]]>"))
            }
        } (function e(t, n) {
            if (!t) return void i();
            if ("code" == t.mode) s.push(t.val), i();
            else {
                var r = l(t.val);

                function o(e) {
                    e && s.push(e), i()
                }
                r ? o(r.val) : u.get(cacheBust(t.val)).success(function (e) {
                    c({
                        url: t.val,
                        val: e
                    }), o(e)
                }).error(function () {
                    o()
                })
            }

            function i() {
                ++f < a.length ? e(a[f], n) : (d(), n && n(s))
            }
        })(a[f = 0], e)
    }, t.getScript = function () {
        return r
    }, t.replaceScript = function (e) {
        if (r = e, o && o.length)
            for (var t = 0; t < o.length; t++) o[t].parentNode && o[t].parentNode.removeChild(o[t]);
        o = [];
        for (t = 0; t < e.length; t++) {
            var n = document.createElement("script");
            n.setAttribute("kotobee", ""), n.setAttribute("type", "text/javascript"), n.innerHTML = e[t], o.push(n);
            try {
                document.head.appendChild(n)
            } catch (e) { }
            try {
                try {
                    document.dispatchEvent(new Event("DOMContentLoaded"))
                } catch (e) { }
            } catch (e) { }
        }
    }, t
}]);

app.factory("library", ["stg", "translit", "cache", "$rootScope", "backend", "crdv", function (i, e, t, n, a, s) {
    var l, c = {},
        r = [];

    function d(e) {
        e = e || {}, c.maxResults = e.large ? 40 : 20
    }

    function u() {
        for (var e = 0; e < r.length; e++) r[e].apply(this, arguments)
    }
    return c.sortMethod = {}, c.root = "", c.initialize = function () {
        c.setBrowserTitle(), d(), r = [], u("initialized")
    }, c.setMax = function (e) {
        c.maxResults = e
    }, c.setBrowserTitle = function () {
        t.setOb("title", replaceHTML(t.getOb("title"), i.data.currentLibrary.name ? i.data.currentLibrary.name : e.get("library")))
    }, c.addListener = function (e) {
        r.push(e)
    }, c.getEbooks = function (r, o) {
        r = r || {}, l = Math.round(1e5 * Math.random()), u("preload", r), r.accumulated || addClass(document.getElementById("libraryThumbs"), "dim");
        r.more ? function (e) {
            e = e || {}, c.maxResults += e.large ? 20 : 6
        }(r.contentkey ? {
            large: !0
        } : {}) : d(r.contentkey ? {
            large: !0
        } : {}), r.sort = c.sortMethod.data, r.sortDir = c.sortMethod.dir, r.max = c.maxResults, a.getEbooks(r, l, function (e, t) {
            if (t == l) {
                removeClass(document.getElementById("libraryThumbs"), "dim"), e.error || (s.applyExistingBooks(e), function (e) {
                    try {
                        r.favs ? i.data.favorites = e : i.data.currentLibrary.books = e
                    } catch (e) { }
                }(e));
                var n = r.favs ? i.data.favorites : i.data.currentLibrary.books;
                n && n.length || (e.empty = !0), u("postload", r, e), o && o(e)
            }
        })
    }, c
}]);

app.factory("markers", ["$rootScope", "error", "cache", "stg", "$location", "$injector", "$ionicScrollDelegate", function (e, t, k, x, n, C, r) {
    var o = {},
        i = new Array,
        a = {};
    return o.addMarker = function (e) {
        i.push(e), o.refreshMarker(e)
    }, o.deleteMarker = function (e) {
        for (var t = 0; t < i.length; t++) i[t].id == e && i.splice(t, 1)
    }, o.windowResize = function () {
        (function () {
            if (n.path() && 0 <= n.path().indexOf("/reader")) return !1;
            return !0
        })() || a.width == window.innerWidth && a.height == window.innerHeight || (o.refreshMarkers(), a.width = window.innerWidth, a.height = window.innerHeight)
    }, o.refreshMarkers = function () {
        for (var e = 0; e < i.length; e++) o.refreshMarker(i[e])
    }, o.refreshMarkerById = function (e) {
        for (var t = 0; t < i.length; t++)
            if (i[t].id == e) return void o.refreshMarker(i[t])
    }, o.getMarker = function (e) {
        for (var t = 0; t < i.length; t++)
            if (i[t].id == e) return i[t]
    }, o.refreshMarker = function (e) {
        var t = e.target();
        if (t && e.elem) {
            var n = "",
                r = "page";
            (hasClass(e.elem, "contentSideNoteMarker") || hasClass(e.elem, "contentBookMarker")) && (r = "side");
            var o = k.getOb("epubContainer").getElementsByClassName("scroll")[0],
                i = k.getOb("epubContent");
            if (o) {
                var a, s, l = t.getBoundingClientRect(),
                    c = o.getBoundingClientRect(),
                    d = i.getBoundingClientRect(),
                    u = getElemPadding(k.getOb("epubContainer")),
                    f = 1;
                a = o.scrollTop, s = o.scrollLeft;
                var p = C.get("nav");
                x.data.book.chapter.viewport && (f = p.getFxlScale());
                var h, g, m, v = (l.top + a - (c.top + 0)) / f;
                if ("side" == r) {
                    h = 0;
                    var b = Number(u.left) + (d.left - c.left + s),
                        y = c.right - d.right + s;
                    (m = x.data.book.chapter.viewport ? b : b < y ? b : y) < 30 && (m = 30), x.data.settings.rtl && (g = 0, h = null), h -= b
                } else {
                    if (0 == e.elem.offsetWidth) return;
                    h = (l.left + s - c.left) / f
                }
                null != v && (n += "top: " + (v + e.offsetTop) + "px; "), null != h && (n += "left: " + (h + e.offsetLeft) + "px; "), null != g && (n += "right: " + (g - e.offsetLeft) + "px; "), null != m && (n += "width: " + m + "px; ");
                var w = 1 / f;
                "" != (n += "transform:translateZ(0) scale(" + w + ");-webkit-transform:translateZ(0) scale(" + w + ");-moz-transform:translateZ(0) scale(" + w + ");") && (e.elem.style.cssText = n), e.callback && e.callback()
            }
        }
    }, o.checkMarkerExistsAtSameLocation = function (o, i) {
        x.getNotes(function (e) {
            for (var t = 0; t < e.length; t++) {
                var n = e[t];
                if ("page" == n.type) {
                    var r = document.getElementById("note" + n.nid);
                    if (r && r != o && r.style.top == o.style.top) return i(!0)
                }
            }
            x.getBookmarks(function (e) {
                for (var t = 0; t < e.length; t++) {
                    var n = e[t],
                        r = document.getElementById("bookmark" + n.bmid);
                    if (r && (r != o && r.style.top == o.style.top)) return i(!0)
                }
                i()
            })
        })
    }, o.show = function () {
        for (var e = 0; e < i.length; e++) i[e].elem.style.visibility = "shown"
    }, o.hide = function () {
        for (var e = 0; e < i.length; e++) i[e].elem.style.visibility = "hidden"
    }, window.addEventListener("resize", o.windowResize, !0), o
}]);

app.factory("media", ["$ionicModal", "tinCan", "$rootScope", "modals", function (e, t, n, r) {
    var o = {};
    return o.show = function (e) {
        t.send({
            verb: "viewed image",
            activity: e.title ? e.title : e.thumbnail
        }), r.show({
            mode: "image",
            dynamic: !0,
            animation: "slide-in-up",
            image: e
        })
    }, o.hide = function () {
        r.hide()
    }, o
}]);

app.factory("modals", ["$ionicModal", "$q", "$injector", "$ionicPlatform", "$rootScope", function (n, r, o, e, t) {
    var i, a, s, l, c = {},
        d = [],
        u = {
            animation: "zoom-in",
            focusFirstInput: !1,
            backdropClickToClose: !1
        };
    c.setScope = function (e) {
        (a = e).hideModal = c.hide, i || f(), a.$on("modal.hidden", function (e) {
            l && clearTimeout(l), setTimeout(function () {
                d.shift(), p(),
                    function () {
                        for (var e = document.getElementsByClassName("modal-backdrop"), t = e.length; t--;) hasClass(e[t], "hide") && e[t].parentNode.removeChild(e[t])
                    }()
            }, 500)
        })
    };
    var f = function () {
        var t = r.defer(),
            e = clone(u);
        return e.scope = a, n.fromTemplateUrl(kTemplate("modals/modals.html"), e).then(function (e) {
            i = e, t.resolve()
        }), t.promise
    };

    function p() {
        if (s = null, d.length) {
            var e = d[0];
            e && ((s = e).hidden ? 1 < d.length && p() : (u.backdropClickToClose = !!e.backdropClickToClose && e.backdropClickToClose, u.animation = e.animation ? e.animation : "zoom-in", f().then(function () {
                a.modal = e, i && (i.show(), a.$$phase || a.$apply(), s.timeLimit && (l = setTimeout(function () {
                    var e = s;
                    c.hide(s);
                    var t = {
                        wait: function () {
                            c.hide(), c.show(e)
                        }
                    };
                    c.show({
                        mode: "waitTimeout",
                        timeoutOptions: t
                    })
                }, s.timeLimit)), setTimeout(function () {
                    e.cb && e.cb(i)
                }, 750))
            })))
        }
    }
    return c.show = function (e) {
        if (i) return d.push(e), 1 == d.length && p(), e;
        f().then(function () {
            c.show(e)
        })
    }, c.obShown = function (e) {
        return s == e
    }, c.shown = function () {
        return !!i && i.isShown()
    }, c.hide = function (e, t) {
        i && (e ? s == e ? (i.hide(), native && o.get("crdv").hideKeyboard()) : e.hidden = !0 : (i.hide(), native && o.get("crdv").hideKeyboard()), t && t())
    }, c
}]);

app.factory("nav", ["cache", "crdv", "$ionicGesture", "$location", "$rootScope", "$ionicSideMenuDelegate", "selection", "markers", "settings", "$timeout", "chapters", "$ionicScrollDelegate", "stg", function (I, t, n, e, r, o, l, c, s, i, a, d, E) {
    var u, f, p, h, T = {},
        g = 15,
        m = 0,
        v = {},
        b = [];

    function y(e, t) {
        for (var n = 0; n < b.length; n++) b[n].event == e && b[n].func(t)
    }

    function w(e) {
        if ("newChapter" == e) {
            var t = I.getOb("epubContainer"),
                n = I.getOb("epubContent");
            if (f = d.$getByHandle("content"), mobile) {
                var r = f.getScrollView().options;
                if (r.scrollbarFadeDelay = 0, r.scrollingY = !0, r.minZoom = .2, r.maxZoom = 3.5, r.zooming = !0, E.data.book.chapter.viewport) {
                    r.deceleration = .85, r.scrollingX = !0, r.scrollbarY = !1, f.scrollTo(0, 0, !1);
                    try {
                        T.getFxlFit(null, "from setChapterLayout").bestFit
                    } catch (e) { }
                    T.fxlZoomFit({
                        noAnim: !0,
                        immediateScrollRefresh: !0
                    })
                } else t.style.marginLeft = null, t.style.paddingLeft = null, t.style.paddingTop = null, f.scrollTo(0, 0, !1), r.zooming && f.zoomTo(1, !1, 0, 0), r.deceleration = .95, r.scrollbarY = !0, config.textSizeControls || (r.zooming = !1), setTimeout(function () {
                    r.scrollingX = !1, config.textSizeControls || (r.zooming = !1)
                }, 500);
                setTimeout(c.refreshMarkers, 1e3)
            } else {
                if (f.scrollTop(!1), E.data.book.chapter.viewport) f.scrollTo(0, 0, !1), T.fxlZoomFit({
                    noAnim: !0,
                    immediateScrollRefresh: !0
                });
                else {
                    t.style.marginLeft = null, t.style.paddingLeft = null, t.style.paddingTop = null, f.scrollTo(0, 0, !1), O(1, {
                        noAnim: !0
                    }), deleteTransform(n), deleteTransformOrigin(n);
                    var o = document.getElementById("spreadR");
                    o && o.parentNode.removeChild(o)
                }
            }
            var i = t.getElementsByClassName("scroll");
            i.length && (removeClass(i[0], "afterDelay"), setTimeout(function () {
                addClass(i[0], "afterDelay")
            }, 2e3))
        }
    }

    function k() {
        if (!config.hideTOCAtWidescreen) {
            var e = 1300 <= v.width;
            r.widescreen != e && (y("widescreenChanged"), r.widescreen = e), e ? addClass(document.body, "widescreen") : removeClass(document.body, "widescreen"), r.$$phase || r.$apply()
        }
    }

    function x(e) {
        if (mobile) {
            var t = I.getOb("epubContainer");
            "margin" == e ? (t.style.paddingLeft && (t.style.setProperty("margin-left", t.style.paddingLeft, "important"), t.style.paddingLeft = null), t.style.paddingTop && (t.style.setProperty("margin-top", t.style.paddingTop, "important"), t.style.paddingTop = null)) : (t.style.marginLeft && (t.style.setProperty("padding-left", t.style.marginLeft, "important"), t.style.marginLeft = null), t.style.marginTop && (t.style.setProperty("padding-top", t.style.marginTop, "important"), t.style.marginTop = null))
        }
    }

    function C(e) {
        if (mobile) {
            e = e || T.getFxlScale();
            var t = T.getFxlFit(null, "from fxlRefreshHorizontalVerticalScrolling"),
                n = f.getScrollView().options;
            e * t.widthFit <= t.widthFit ? n.scrollingX = !1 : n.scrollingX = !0, e * t.widthFit <= t.heightFit ? n.scrollingY = !1 : n.scrollingY = !0
        }
    }

    function S(e) {
        if (!(g < m || mobile && function () {
            var e = document.getElementById("readerBody").style.transform || document.getElementById("readerBody").style.webkitTransform;
            if (e && "translate3d(0px, 0px, 0px)" != e && "translate3d(0px, 0, 0)" != e) return !0
        }())) {
            var t = E.data.book.chapter.viewport ? 1 : .8,
                n = E.data.book.chapter.viewport ? 100 : 70;
            if (!(Math.abs(e.gesture.velocityX) < t || Math.abs(e.gesture.distance) < n || 1 < e.gesture.touches.length)) return !0
        }
    }

    function A() {
        return !(e.path() && 0 <= e.path().indexOf("/reader"))
    }

    function O(e, t) {
        t = t || {};
        var n = I.getOb("epubContainer").children[0];
        L(n) != "scale(" + e + ")" && (function (e, t) {
            autoprefixTransform(e, t)
        }(n, "scale(" + e + ")"), function (e, t) {
            e.style.perspective = e.style.msPerspective = e.style.webkitPerspective = e.style.mozPerspective = t
        }(n, 1 < e ? "none" : null), t.noAnim && (n.style.transition = "initial"), setTimeout(function () {
            t.noAnim && (n.style.transition = null), redrawForChrome(document.body)
        }, 500), setTimeout(c.refreshMarkers, 1e3), 1.1 < e ? addClass(I.getOb("epubContainer"), "zoomed") : removeClass(I.getOb("epubContainer"), "zoomed"))
    }

    function B() {
        if (document.fullscreenElement || document.webkitFullscreenElement || document.mozFullscreenElement || document.msFullscreenElement) return document.exitFullscreen ? document.exitFullscreen() : document.webkitExitFullscreen ? document.webkitExitFullscreen() : document.mozExitFullscreen ? document.mozExitFullscreen() : document.msExitFullscreen && document.msExitFullscreen(), !0
    }

    function L(e) {
        return e.style.webkitTransform || e.style.mozTransform || e.style.transform
    }
    return T.init = function (e) {
        mobile && (n.on("swipeleft", T.swipeLeft, e), n.on("swiperight", T.swipeRight, e)), n.on("dragstart", T.dragStart, e), n.on("drag", T.drag, e), n.on("tap", T.tap, e), n.on("doubletap", T.fxlToggleTapZoom, e), n.on("transformstart", T.transformstart, e), n.on("transformend", T.transformend, e), u || (window.addEventListener("resize", T.windowResize, !0), v.width = window.innerWidth, v.height = window.innerHeight, mobile || k(), a.addListener(w), u = !0, T.addListener("widescreenChanged", function () {
            setTimeout(T.fxlZoomFit, 400)
        }), document.body.addEventListener("keyup", function (e) {
            27 == e.keyCode && B()
        }))
    }, T.addListener = function (e, t) {
        b.push({
            event: e,
            func: t
        })
    }, T.windowResize = function () {
        A() || v.width == window.innerWidth && v.height == window.innerHeight || (T.fxlZoomFit(), v.width = window.innerWidth, v.height = window.innerHeight, mobile || k(), a.regenerateSpreads({
            resize: !0
        }))
    }, T.getScroller = function () {
        return f
    }, T.transformstart = function () {
        T.transforming = !0, E.data.book.chapter.viewport && (function (e) {
            if (!e) {
                var t = I.getOb("epubContainer");
                (e = t.getElementsByClassName("scroll")).length && (e = e[0])
            }
            e && deleteTransformOrigin(e)
        }(), p = (new Date).getTime(), h = T.getFxlScale(), x("margin"))
    }, T.transformend = function () {
        if (T.transforming = !1, !A())
            if (E.data.book.chapter.viewport) {
                var e = (new Date).getTime(),
                    t = T.getFxlScale();
                if (mobile && t < h && e - p < 800) return void T.fxlZoomFit();
                var n = T.getFxlFit(null, "from transformend").bestFit;
                t < n ? (d.$getByHandle("content").zoomTo(n, !0), t = n) : 3 < t && (d.$getByHandle("content").zoomTo(3, !0), t = n), C(3, "transformend"), setTimeout(c.refreshMarkers, 1e3), T.repaint()
            } else {
                if (!config.textSizeControls) return;
                var r = f.getScrollPosition().zoom,
                    o = s.getTextSize(),
                    i = Math.round(r * o * 10) / 10;
                i < .2 && (i = .2), E.data.settings.textSize = i + "em", s.updateTextSize(), s.writeTextSize(), f.zoomTo(1, !0);
                var a = angular.element(I.getOb("epubContent")).scope();
                a.header.mode = "textSize", a.$$phase || a.$apply(), setTimeout(c.refreshMarkers, 1e3)
            }
    }, T.repaint = function () {
        redrawForChrome(I.getOb("epubContent"))
    }, T.resize = function () {
        f.resize()
    }, T.swipeRight = function (e) {
        A() || S(e) && (f.resize(), native && t.vibrate(), E.data.settings.rtl ? a.nextChapter() : a.prevChapter())
    }, T.swipeLeft = function (e) {
        A() || S(e) && (f.resize(), native && t.vibrate(), E.data.settings.rtl ? a.prevChapter() : a.nextChapter())
    }, T.dragStart = function () {
        A() || (E.data.book.chapter.viewport || f.zoomTo(1, !1), m = 0, f.freezeScroll(!1))
    }, T.drag = function (e) {
        m++
    }, T.tap = function (e) {
        y("tap", e)
    }, T.fxlToggleTapZoom = function (e) {
        if (I.getOb("epubContent") && E.data.book.chapter.viewport && E.data.book.chapter.viewport.width) {
            var t = T.getFxlScale(),
                n = T.getFxlFit(null, "from fxlToggleTapZoom").bestFit,
                r = f.getScrollView().options.minZoom;
            if (null == r && (r = t - 1), Math.round(100 * t) != Math.round(100 * n) && r < t) T.fxlZoomFit(e);
            else {
                var o = 2 * T.getFxlFit(null, "from fxlToggleTapZoom").bestFit;
                mobile ? d.$getByHandle("content").zoomTo(o, !0, e.gesture.center.pageX, e.gesture.center.pageY) : O(o), l.reset(), x("margin"), C(o)
            }
            setTimeout(c.refreshMarkers, 1e3), setTimeout(T.repaint, 1e3)
        }
    }, T.fxlZoomIn = function (e) {
        l.reset();
        var t = 1.2 * T.getFxlScale(),
            n = T.getFxlFit(null, "from fxlZoomIn");
        n.bestFit;
        3 < t && (t = 3);
        var r = d.$getByHandle("content");
        mobile ? r.zoomTo(t, !0) : O(t), x("margin");
        var o = I.getOb("epubContent"),
            i = document.getElementById("readerBody");
        if (1 <= t);
        else if ("vertical" == n.bestFitType) {
            var a = getElemMargin(o).x,
                s = getElemPadding(o).x;
            "auto" == a && (a = 0), "auto" == s && (s = 0), a = a || 0, s = s || 0;
            a = (i.getBoundingClientRect().width - s - E.data.book.chapter.viewport * t) / 2;
            o.style.setProperty("margin-left", Math.round(a / t) + "px", "important"), o.style.setProperty("margin-right", Math.round(a / t) + "px", "important")
        } else o.style.marginLeft && (o.style.marginLeft = o.style.marginRight = null);
        C(t), setTimeout(c.refreshMarkers, 1e3)
    }, T.fxlZoomOut = function (e) {
        l.reset();
        var t = .8 * T.getFxlScale(),
            n = T.getFxlFit(null, "from fxlZoomOut").bestFit;
        t < n && (t = n), mobile ? d.$getByHandle("content").zoomTo(t, !0) : O(t), x("margin"), C(t), setTimeout(c.refreshMarkers, 1e3)
    }, T.fxlZoomFit = function (e) {
        if (e = e || {}, I.getOb("epubContent") && E.data.book && E.data.book.chapter.viewport && E.data.book.chapter.viewport.width) {
            l.reset();
            var t = T.getFxlFit(null, "from fxlZoomFit"),
                n = e.noAnim;
            mobile ? f.zoomTo(t.bestFit, !n) : O(t.bestFit, {
                noAnim: n
            }), T.fxlAdjustAlignment(), x("padding"), C(t.bestFit), setTimeout(c.refreshMarkers, 1e3)
        }
    }, T.fxlAdjustAlignment = function (e) {
        (e = e || {}).epubContent || (e.epubContent = I.getOb("epubContent")), e.epubContainer || (e.epubContainer = I.getOb("epubContainer"));
        var t = T.getFxlFit(e, "from fxlAdjustAlignment"),
            n = getElemMargin(e.epubContent, {
                computed: !0
            });
        "auto" == n.left && (n.left = 0), "auto" == n.top && (n.top = 0), autoprefixTransform(e.epubContent, "scale(" + t.widthFit + ")"), autoprefixTransformOrigin(e.epubContent, -n.left + "px " + -n.top + "px"), e.epubContainer.style.marginLeft = e.epubContainer.style.marginRight = null, e.epubContainer.style.paddingLeft = e.epubContainer.style.paddingRight = null, e.epubContainer.style.setProperty(E.data.settings.rtl ? "padding-right" : "padding-left", t.marginLeft + "px", "important"), e.epubContainer.style.setProperty("padding-top", t.marginTop + "px", "important")
    }, T.getSpreadHeight = function (e) {
        for (var t = 0, n = 0; n < e.children.length; n++) {
            var r = e.children[n];
            r.id && 0 == r.id.indexOf("spread") && (t = Math.max(t, Number(r.style.height.split("px")[0])))
        }
        return t
    }, T.getSpreadWidth = function (e) {
        for (var t = 0, n = 0; n < e.children.length; n++) {
            var r = e.children[n];
            r.id && 0 == r.id.indexOf("spread") && (t += Number(r.style.width.split("px")[0]))
        }
        return t
    }, T.viewFromElem = function (e) {
        for (var t = {
            maxHeight: 0,
            totalWidth: 0
        }, n = 0; n < e.children.length; n++) {
            var r = e.children[n];
            if (r.id && 0 == r.id.indexOf("spread")) {
                var o = Number(r.style.height.split("px")[0]),
                    i = Number(r.style.width.split("px")[0]),
                    a = {
                        width: i,
                        height: o
                    };
                "spreadL" == r.id ? (t.leftOb || (t.leftOb = {}), t.leftOb.viewport = a, t.leftOb.layout = hasClass(r, "fxl") ? "fxl" : "rfl", t.layout = t.leftOb.layout) : "spreadR" == r.id && (t.view = "double", t.rightOb || (t.rightOb = {}), t.rightOb.left = Number(r.style.left.split("px")[0]), t.rightOb.viewport = a, t.rightOb.layout = hasClass(r, "fxl") ? "fxl" : "rfl"), t.maxHeight = Math.max(o, t.maxHeight), t.totalWidth += i
            }
        }
        return t.leftOb && t.rightOb && t.rightOb.left && (t.totalWidth += t.rightOb.left - t.leftOb.viewport.width), t
    }, T.getFxlFit = function (e, t) {
        (e = e || {}).viewport || (e.viewport = E.data.book.chapter.viewport);
        var n = {};
        e.epubContent || (e.epubContent = I.getOb("epubContent"));
        var r = e.view;
        r = r || E.data.currentView;
        var o = {
            x: 0
        };
        e.epubContainer || (e.epubContainer = I.getOb("epubContainer"));
        var i = e.epubContainer.getElementsByClassName("scroll");
        i.length && (o = getElemPadding(i[0], {
            computed: !0
        }), n.scrollContainer = i[0]), "auto" == o.x && (o.x = 0), "auto" == o.y && (o.y = 0);
        var a = getElemMargin(e.epubContent, {
            computed: !0
        }),
            s = getElemPadding(e.epubContent, {
                computed: !0
            }),
            l = a.x,
            c = a.y;
        "auto" == l && (l = 0), "auto" == c && (c = 0);
        var d = s.x,
            u = s.y;
        "auto" == d && (d = 0), "auto" == u && (u = 0);
        var f = e.viewport.width + l,
            p = e.viewport.height + c;
        if ("double" == r) {
            var h = T.viewFromElem(e.epubContent);
            f = h.totalWidth, p = h.maxHeight
        }
        var g = document.getElementById("readerBody"),
            m = document.getElementById("readerHeader"),
            v = document.getElementById("tabMenu"),
            b = g.getBoundingClientRect().width,
            y = g.getBoundingClientRect().height;
        mobile && "alwaysShow" != config.autohideReaderMode && "showOnScroll" != config.autohideReaderMode && "showOnTap" != config.autohideReaderMode || (y -= m.getBoundingClientRect().height + v.getBoundingClientRect().height), e.includeScrollPadding || (b -= o.x, y -= o.y);
        var w = b / f,
            k = y / p;
        if (n.readerWidth = b, n.readerHeight = y, n.widthFit = w, n.heightFit = k, config.fitToScreen) n.bestFit = k < w ? k : w, n.bestFitType = k < w ? "vertical" : "horizontal";
        else {
            var x = w;
            1 <= x && (x = 1);
            var C = k / x;
            n.bestFitType = "vertical", C < 1 && .7 <= C && (x = k, n.bestFitType = "vertical"), n.bestFit = x
        }
        return n.bestFit = n.bestFit / n.widthFit, n.innerWidth = f, n.innerHeight = p, n.outerWidth = b, n.outerHeight = y, n.marginLeft = Math.round((n.outerWidth - n.widthFit * n.bestFit * n.innerWidth) / 2), n.marginTop = Math.round((n.outerHeight - n.widthFit * n.bestFit * n.innerHeight) / 2), n.marginTop < 0 && (n.marginTop = 0), n
    }, T.getFxlScale = function () {
        if (mobile) return d.$getByHandle("content").getScrollPosition().zoom;
        var e = L(I.getOb("epubContainer").children[0]);
        if (!e) return 1;
        if ("" == e) return 1;
        var t = e.split("scale");
        if (t.length <= 1) return 1;
        var n = t[1].split("(");
        if (n.length <= 1) return 1;
        var r = n[1].split(")");
        return Number(r[0])
    }, T.fullscreen = function () {
        var e = document.body;
        B() || (e.requestFullscreen ? e.requestFullscreen() : e.webkitRequestFullscreen ? e.webkitRequestFullscreen() : e.mozRequestFullScreen ? e.mozRequestFullScreen() : e.msRequestFullscreen && e.msRequestFullscreen())
    }, T
}]);

app.factory("np", ["stg", "status", "error", "translit", "$rootScope", "crdv", "modals", "$http", "$ionicSideMenuDelegate", function (a, e, t, n, r, s, o, i, l) {
    var c, d, u, f, p, h, g, m, v, b = {},
        y = kLog;
    return b.init = function (e) {
        if (desktop) {
            u = require("nw.gui"), require("child_process").exec, f = u.Window.get(), process.cwd(), (process.env.HOME || process.env.USERPROFILE) + "/Desktop", p = require("path").dirname(process.execPath), v = p + "/node_modules/", "mac" == os && (v = ""), r.readerApp && (v = ""), v = "", c = require(v + "fs.extra"), require(v + "async"), d = require(v + "buckle"), h = u.App.dataPath, g = h + "/temp", m = h + "/Books", b.createIfNotExists(m), b.createIfNotExists(g);
            try {
                f.removeAllListeners()
            } catch (e) {
                y("ERROR " + e, 1)
            }
            f.on("close", function () {
                b.exit()
            }), e && e()
        } else e()
    }, b.createIfNotExists = function (t, n) {
        desktop && c.exists(t, function (e) {
            e ? n && n() : b.createIfNotExists(ph.parent(t), function () {
                c.mkdirSync(t), n && n()
            })
        })
    }, b.createIfNoParentsExists = function (t, n) {
        desktop && c.exists(t, function (e) {
            e ? n && n() : b.createIfNoParentsExists(ph.parent(t), function () {
                c.mkdirSync(t), n && n()
            })
        })
    }, b.writeEbook = function (e, o, t, i) {
        desktop && b.createTempDir(function (n) {
            var r = new FileReader;
            r.onload = function () {
                var e = ph.join(n, "book.zip");
                "mac" == os && (e = n + "/book.zip"), c.writeFileSync(e, Buffer.from(new Uint8Array(r.result)));
                var t = ph.join(m, o.path);
                "mac" == os && (t = m + "/" + o.path), b.extractZip(e, t, function () {
                    o.exists = !0, a.data.downloaded.push(o), s.applyExistingBooks(a.data.currentLibrary.books), a.writeSlot("downloaded" + a.getBookId(!0), a.data.downloaded, function () {
                        b.removeDirectory(n, function () {
                            i && i()
                        })
                    })
                })
            }, r.readAsArrayBuffer(t)
        })
    }, b.deleteEbook = function (t, n) {
        b.removeDirectory(ph.join(m, t.path), function () {
            for (var e = 0; e < a.data.downloaded.length; e++) a.data.downloaded[e].path == t.path && (t.exists = !1, a.data.downloaded.splice(e, 1), e--);
            a.writeSlot("downloaded" + a.getBookId(!0), a.data.downloaded, function () {
                n && n()
            })
        })
    }, b.fileExists = function (e, t) {
        c.exists(e, function (e) {
            t(e)
        })
    }, b.removeFile = function (e, t) {
        b.removeDirectory(e, t)
    }, b.removeDirectory = function (e, t) {
        c.rmrf(e, function (e) {
            t && t(e)
        })
    }, b.extractZip = function (e, t, n) {
        d.open(e, t, function () {
            c.readdir(t, n)
        })
    }, b.createTempDir = function (t) {
        var n = g + "/f" + randStr(5);
        b.fileExists(n, function (e) {
            e ? b.createTempDir(t) : c.mkdir(n, function () {
                t(n)
            })
        })
    }, b.openLink = function (e) {
        u.Shell.openExternal(e)
    }, b.getBooksPath = function () {
        return m + "/"
    }, b.writeArrayBuffer = function (e, t, n, r) {
        var o = arrayBufferToBuffer(e);
        c.open(t, "w", function (e, t) {
            if (e) throw "error opening file: " + e;
            c.write(t, o, 0, o.length, null, function (e) {
                e ? r && r(!1) : c.close(t, function () {
                    r && r(!0)
                })
            })
        })
    }, b.copy = function (n, e, t, r) {
        if ((t = t || {}).replace)
            for (var o = 0; o < t.replace.length; o++) n = n.replace(t.replace[o].key, t.replace[o].val);
        t.header && (n = t.header + n), t.footer && (n += t.footer), t.encoding || (t.encoding = "ascii"), n = (n += "").replace(/\xA0/g, " "), n = utf8Encode(n), c.writeFile(e, n, t.encoding, function (e, t) {
            r && r(n)
        })
    }, b.exit = function () {
        a.getActions(function (e) {
            if (e && e.length) {
                o.hide();
                var t = {
                    title: "confirmation",
                    msg: "annotationsSyncExitWarning",
                    barStyle: "bar-dark"
                },
                    n = {
                        label: "delete",
                        style: "button-assertive",
                        func: function () {
                            u.App.quit()
                        }
                    },
                    r = {
                        label: "cancel",
                        func: function () {
                            o.hide()
                        }
                    };
                t.btns = [n, r], o.show({
                    mode: "confirm",
                    confirmOptions: t
                })
            } else u.App.quit()
        })
    }, b
}]);

app.factory("popovers", ["$ionicPopover", "$q", "$rootScope", function (e, n, r) {
    var o, i, t = {},
        a = [];
    t.setScope = function (e) {
        i = e, o || s()
    };
    var s = function () {
        var t = n.defer();
        return i = i || r, e.fromTemplateUrl(kTemplate("modals/popovers.html"), {
            scope: i
        }).then(function (e) {
            o = e, t.resolve()
        }), t.promise
    };

    function l() {
        if (0 != a.length) {
            var e = a.shift();
            e.apply ? i.$apply(function () {
                i.popover = e
            }) : i.popover = e, o && (o.isShown() || o.show(e.event))
        }
    }
    return t.show = function (e) {
        o ? (a.push(e), 1 == a.length && l()) : s().then(function () {
            t.show(e)
        })
    }, t.shown = function () {
        return !!o && o.isShown()
    }, t.hide = function () {
        o && (i.popover = null, o && o.hide(), l())
    }, t
}]);

app.factory("scorm", ["$injector", function (i) {
    var p, h, a, g = {};

    function s() {
        a.doLMSCommit("")
    }

    function m(e, t, n) {
        var r = v(e + "." + t + "." + n);
        return r && "" != r || (r = v(e + "_" + t + "." + n)), r
    }

    function v(e) {
        return a.doLMSGetValue(e)
    }

    function b(e, t) {
        return a.doLMSSetValue(e, t)
    }
    return g.initialize = function () {
        if (!(config.v < 1.2)) {
            kInteractive.scorm = this;
            try {
                if (!scormEnabled) return
            } catch (e) {
                return
            } (a = i.get("scormWrapper")).doLMSInitialize(""), 1.2 == (h = a.getAPIVersion()) ? "not attempted" == v("cmi.core.lesson_status") && (b("cmi.core.lesson_status", "browsed"), s()) : "not attempted" == v("cmi.completion_status") && (b("cmi.completion_status", "unknown"), s()), p = function () {
                try {
                    var e = v("cmi.suspend_data");
                    return JSON.parse(e)
                } catch (e) {
                    return []
                }
            }();
            try {
                for (var e = 0; e < config.scorm.objectives.length; e++) {
                    for (var t = Number(v("cmi.objectives._count")), n = t = t || 0, r = config.scorm.objectives[e], o = 0; o < t; o++) m("cmi.objectives", o, "id") == r.id && (n = o);
                    b("cmi.objectives." + n + ".id", r.id), 1.2 != h && b("cmi.objectives." + n + ".description", r.name)
                }
            } catch (e) { }
            s()
        }
    }, g.setBookmark = function (e, t) {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        b(1.2 == h ? "cmi.core.lesson_location" : "cmi.location", e), t || s()
    }, g.getBookmark = function () {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        return v(1.2 == h ? "cmi.core.lesson_location" : "cmi.location")
    }, g.setInteractions = function (e) {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        for (var t = 0; t < e.length; t++) {
            for (var n = p.length, r = 0; r < p.length; r++) p[r].id == e[t].id && (n = r);
            p[n] = e[t], g.setInteraction(n, e[t], !0)
        } ! function () {
            if (!p.length) return;
            for (var e = Number(v("cmi.objectives._count")), t = {}, n = 0; n < p.length; n++) t[p[n].qid] = p[n];
            for (n = 0; n < e; n++) {
                var r = v("cmi.objectives." + n + ".id");
                if (r) {
                    var o = 0;
                    for (var i in t) t[i].objective == r && (o += t[i].scoreMax);
                    g.setObjective({
                        id: r,
                        scoreMax: o
                    }, !0), g.setObjective({
                        id: r,
                        scoreRaw: 0
                    }, !0)
                }
            }
            o = 0;
            for (var i in t) o += t[i].scoreMax;
            b(1.2 == h ? "cmi.core.score.max" : "cmi.score.max", o), b(1.2 == h ? "cmi.core.score.min" : "cmi.score.min", 0), b(1.2 == h ? "cmi.core.score.raw" : "cmi.score.raw", 0);
            for (var a = 0; a < p.length; a++) {
                var s = p[a],
                    l = null,
                    c = null;
                for (n = 0; n < e; n++) s.objective == m("cmi.objectives", n, "id") && (l = s.objective, c = n);
                if (l) {
                    var d = Number(v("cmi.objectives." + c + ".score.raw"));
                    d = d || 0;
                    var u = {};
                    u.id = l, u.scoreRaw = s.result * s.weighting + d, u.scoreMin = 0, u.scoreScaled = u.scoreRaw / u.scoreMax, u.completionStatus = "unknown", u.successStatus = "unknown", g.setObjective(u, !0)
                }
                var f = Number(v(1.2 == h ? "cmi.core.score.raw" : "cmi.score.raw"));
                f = f || 0, b(1.2 == h ? "cmi.core.score.raw" : "cmi.score.raw", s.result * s.weighting + f)
            }
        }(),
            function (e) {
                try {
                    b("cmi.suspend_data", JSON.stringify(e))
                } catch (e) { }
            }(p), s()
    }, g.setInteraction = function (e, t, n) {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        t.id && b("cmi.interactions." + e + ".id", t.id), t.result && b("cmi.interactions." + e + ".result", t.result), t.correctResponses && b("cmi.interactions." + e + ".correct_responses.0.pattern", t.correctResponses), t.type && b("cmi.interactions." + e + ".type", t.type), t.weighting && b("cmi.interactions." + e + ".weighting", t.weighting), t.objective && b("cmi.interactions." + e + ".objectives.0.id", t.objective), 1.2 == h ? (t.timestamp && b("cmi.interactions." + e + ".time", function (e) {
            var t = e.getSeconds();
            t < 10 && (t = "0" + t);
            var n = e.getMinutes();
            n < 10 && (n = "0" + n);
            var r = e.getHours();
            return r < 10 && (r = "0" + r), r + ":" + n + ":" + t
        }(t.timestamp)), t.latency && b("cmi.interactions." + e + ".latency", function (e) {
            var t = Math.floor(e / 3600);
            e -= 60 * t * 60;
            var n = Math.floor(e / 60);
            return (e -= 60 * n) < 10 && (e = "0" + e), t < 10 && (t = "0" + t), n < 10 && (n = "0" + n), t + ":" + n + ":" + e
        }(t.latency)), t.learnerResponses && b("cmi.interactions." + e + ".student_response", t.learnerResponses)) : (t.timestamp && b("cmi.interactions." + e + ".timestamp", function (e) {
            var t = e.toISOString().split(".");
            return t.splice(t.length - 1, 1), t.join(".")
        }(t.timestamp)), t.latency && b("cmi.interactions." + e + ".latency", function (e) {
            var t = e,
                n = Math.floor(t / 31104e3);
            t = e - 31104e3 * n;
            var r = Math.floor(t / 2592e3);
            t = e - 2592e3 * r;
            var o = Math.floor(t / 86400);
            t = e - 86400 * o;
            var i = Math.floor(t / 3600);
            t = e - 3600 * i;
            var a = Math.floor(t / 60);
            return "P" + n + "Y" + r + "M" + o + "DT" + i + "H" + a + "M" + (t = e - 60 * a) + "S"
        }(t.latency)), t.learnerResponses && b("cmi.interactions." + e + ".learner_responses", t.learnerResponses), t.description && b("cmi.interactions." + e + ".description", t.description)), n || s()
    }, g.setObjective = function (e, t) {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        for (var n = Number(v("cmi.objectives._count")), r = n, o = 0; o < n; o++) m("cmi.objectives", o, "id") == e.id && (r = o);
        null != e.id && b("cmi.objectives." + r + ".id", e.id), null != e.scoreRaw && b("cmi.objectives." + r + ".score.raw", e.scoreRaw), null != e.scoreMin && b("cmi.objectives." + r + ".score.min", e.scoreMin), null != e.scoreMax && b("cmi.objectives." + r + ".score.max", e.scoreMax), 1.2 == h ? null != e.successStatus && b("cmi.objectives." + r + ".status", e.successStatus) : (null != e.description && b("cmi.objectives." + r + ".description", e.description), null != e.successStatus && b("cmi.objectives." + r + ".success_status", e.successStatus), null != e.completionStatus && b("cmi.objectives." + r + ".completion_status", e.completionStatus), null != e.scoreScaled && b("cmi.objectives." + r + ".score.scaled", e.scoreScaled)), t || s()
    }, g.setManual = function (e, t) {
        b(e, t), s()
    }, g.interactionExistsOld = function (e) {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        for (var t = Number(v("cmi.interactions._count")), n = 0; n < t; n++)
            if (m("cmi.interactions", n, "id") == e) return !0
    }, g.interactionExists = function (e) {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        for (var t = 0; t < p.length; t++)
            if (p[t].id == e) return !0
    }, g.objectiveExists = function (e) {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        for (var t = Number(v("cmi.objectives._count")), n = 0; n < t; n++)
            if (m("cmi.objectives", n, "id") == e) return !0
    }, g
}]);

app.factory("search", ["$http", "error", "book", "$rootScope", "modals", "status", "$location", "chapters", "cache", "$timeout", "$filter", "translit", "virt", "selection", "stg", function (e, t, n, l, c, d, r, u, o, i, a, f, s, p, h) {
    var g = {};

    function m(e, i, a, t) {
        if (0 != e.children.length) {
            for (var n = 0; n < e.children.length; n++) 0 != o(e.children[n]) && (t = !1, hasClass(e.children[n], "k-section") && s.checkIfRequiresParse(e.children[n]), m(e.children[n], i, a, !0));
            t && r(e)
        } else 0 < o(e) && r(e);

        function r(e) {
            for (var t = e, n = p.convertElemsToLocation([t], i).left, r = '<span class="searchItem">' + t.textContent + "</span>", o = (r.toLowerCase().indexOf(g.key.toLowerCase()), r.length + Math.round((70 - r.length) / 2)); t.previousSibling && !(r.length >= o);) t.previousSibling.textContent && (r = t.previousSibling.textContent + r), t = t.previousSibling;
            r.length;
            for (t = e; t.nextSibling && !(70 <= r.length);) t.nextSibling.textContent && (r += t.nextSibling.textContent), t = t.nextSibling;
            g.result[a].matches.push({
                chapter: a,
                elem: e,
                hint: r.trim(),
                location: n
            })
        }

        function o(e) {
            return (e.textContent.toLowerCase().match(new RegExp(g.key.toLowerCase(), "g")) || []).length
        }
    }

    function v() {
        for (var e = 0, t = g.result.length; t--;)
            if (g.result[t])
                for (var n = g.result[t].matches.length; n--;) e++;
        return e
    }

    function b(o, i) {
        var e = h.data.epub.spine.getElementsByTagName("itemref");
        if (!(o > e.length - 1)) return u.getPageOb({
            page: o,
            dontParse: !0
        }, function (e) {
            var t = document.createElement("div"),
                n = parser.parseFromString(e.raw ? e.raw : e.content, "text/html"),
                r = n.getElementById("epubContent");
            t.appendChild(r);
            n.getElementsByTagName("title");
            g.result[o] = [], g.result[o].title = e.title, g.result[o].matches = [], a("contentVirt")("parse", r, function (e) {
                m(e, e, o), i(!0)
            })
        });
        i(!1)
    }
    return g.start = function (e, t) {
        if (g.key = e.key, e.array ? g.result = e.array : g.result = new Array, "all" == e.mode) ! function (t, n) {
            var r = 0;
            t.start && (r = t.start);
            var o = v(),
                i = function (e) {
                    t.limit && v() - o >= t.limit ? n({
                        results: g.result,
                        more: !0,
                        key: g.key
                    }) : e ? b(r++, i) : n({
                        results: g.result,
                        key: g.key
                    })
                };
            b(r, i)
        }(e, t);
        else if ("chapter" == e.mode) {
            var n = o.getOb("epubContent");
            g.result[currentIndex] = [], g.result[currentIndex].matches = [], m(n, n, currentIndex), t({
                results: g.result,
                key: g.key
            })
        }
    }, g.removeHighlights = function () {
        for (var e = document.getElementsByClassName("searchItemHighlight"), t = e.length; t--;) removeClass(e[t], "searchItemHighlight")
    }, g.searchItemClicked = function (e, t, n) {
        t = t || {};
        var r = e.item,
            o = e.index,
            i = e.matches;

        function a() {
            d.update(f.get("pleaseWait"), null, {
                dots: !0
            }), u.addListener(s);
            var t = r.chapter,
                n = r.location;
            u.getPageOb({
                page: t
            }, function (e) {
                if (g.removeHighlights(), e.pages && e.pages.length);
                u.redirect(t, {
                    hash: n
                }), l.$$phase || l.$apply()
            })
        }

        function s(e) {
            "renderedChapter" == e && (d.update(), u.removeListener(s), i.index = o, l.renderScope.applyHeader("searchItems"), n && n())
        }
        c.hide(), t.timer ? timeOut(a, 300) : a()
    }, g
}]);

app.factory("selection", ["markers", "cache", "error", "stg", "virt", "$rootScope", "$injector", "$ionicScrollDelegate", "translit", function (a, p, e, u, f, t, n, h, r) {
    var o, s, l, c, d, g, m, v, b, y, w, k, x, C, I, E, T, S, A, O, B, L = {};

    function N(e) {
        return 0 < parents(e, {
            stopAtId: "spreadL",
            filterByClass: "ki-noHighlight"
        }).length
    }
    L.getLocation = function () {
        return I
    }, L.elementDown = function (e, t) {
        y = e;
        var n = !0;
        mobile || ("a" == e.nodeName.toLowerCase() && (n = !1), N(e) && (n = !1), e.parentNode && "a" == e.parentNode.nodeName.toLowerCase() && (n = !1), n && addClass(e, "highlightOver")), redrawForChrome(y.parentElement);
        var r = mobile && !mobileDebug ? 400 : 100;
        if (n || (r *= 4), k = setTimeout(L.elementHeld, r), mobile && !mobileDebug) {
            if (!t.touches || !t.touches.length) return;
            C = t.touches[0], A = C.clientX, O = C.clientY
        } else A = t.clientX, O = t.clientY, C = t;
        document.body.addEventListener(mobile && !mobileDebug ? "touchend" : "mouseup", L.elementUp), document.body.addEventListener(mobile && !mobileDebug ? "touchmove" : "mousemove", M)
    };
    var M = function (e) {
        C = mobile && !mobileDebug ? e.touches[0] : e, "activated" == E && e.stopPropagation()
    };

    function P(e, t) {
        for (t = t || e; !e.nextSibling;)
            if ((e = e.parentNode) == t) return;
        for (e = e.nextSibling; e.childNodes[0];) e = e.childNodes[0];
        return "#text" == e.nodeName && e.textContent.trim() ? e : P(e, t)
    }

    function R(e, t) {
        for (t = t || e; !e.previousSibling;)
            if ((e = e.parentNode) == t) return;
        for (e = e.previousSibling; e.childNodes.length;) e = e.childNodes[e.childNodes.length - 1];
        return "#text" == e.nodeName && e.textContent.trim() ? e : R(e, t)
    }

    function D(e) {
        return 8 == e.nodeType || "#text" == e.nodeName || "br" == e.nodeName.toLowerCase() || (0 == e.offsetWidth || void 0)
    }

    function F(e, t) {
        var n = e.split("."),
            r = t.split(".");
        if (e == t) return 0;
        for (var o = 0; o < n.length; o++) {
            if (Number(n[o]) > Number(r[o])) return 1;
            if (Number(n[o]) < Number(r[o])) return -1
        }
        return n.length > r.length ? 1 : -1
    }
    L.elementUp = function (e) {
        y && (mobile || removeClass(y, "highlightOver"), y = null, clearTimeout(k)), B && removeClass(p.getOb("epubContent"), "disableEvents"), B = !1, document.body.removeEventListener(mobile && !mobileDebug ? "touchend" : "mouseup", L.elementUp), document.body.removeEventListener(mobile && !mobileDebug ? "touchmove" : "mousemove", M), t.$$phase || t.$apply()
    }, L.elementHeld = function () {
        n.get("nav").transforming || C && (Math.abs(C.clientX - A) > (mobile ? 30 : 10) || Math.abs(C.clientY - O) > (mobile ? 30 : 10) || N(y) || (addClass(y, "highlightSelected"), Y(y), L.elementUp()))
    }, L.getSelectedText = function (e) {
        e = e || document.getElementsByClassName("highlightSelected");
        for (var t = "", n = 0; n < e.length; n++) t += e[n].textContent;
        return t
    }, L.getElemsByLocation = function (e, t) {
        t = t || p.getOb("epubContent");
        var n = new Array;
        if (!e) return n;
        var r = e.split(",");
        if ("double" == u.data.currentView) {
            for (var o = [], i = 0; i < r.length; i++) {
                var a = r[i];
                a = "1" + a.substr(1), o.push(a)
            }
            r = r.concat(o)
        }
        for (i = 0; i < r.length; i++)
            if (r[i]) {
                for (var s = r[i].split("."), l = t, c = 0; c < s.length; c++)
                    if (l) {
                        1 == c && f.checkIfRequiresParseAndAnnotations(l.children[Number(s[c])]);
                        var d = 0;
                        l.hasAttribute("kb-offset") && (d = Number(l.getAttribute("kb-offset"))), l = l.children[Number(s[c]) - d]
                    }
                l && n.push(l)
            }
        return n
    }, L.expand = function () {
        l = l.parentNode, $(), l = c = b, U(), mobile && H(), z()
    }, L.extendRight = function (e) {
        if (e || "rtl" != getComputedStyle(l).direction) {
            if (x) {
                var t = P(l, p.getOb("epubContent"));
                if (D(l = t.parentNode)) return void L.extendRight(e)
            } else {
                t = P(c, p.getOb("epubContent"));
                if (D(c = t.parentNode)) return void L.extendRight(e)
            }
            $(), U(), mobile && H(), z()
        } else L.extendLeft(!0)
    }, L.extendLeft = function (e) {
        if (e || "rtl" != getComputedStyle(l).direction) {
            if (x) {
                var t = R(l, p.getOb("epubContent"));
                if (D(c = t.parentNode)) return void L.extendLeft(e)
            } else {
                t = R(l, p.getOb("epubContent"));
                if (D(l = t.parentNode)) return void L.extendLeft(e)
            }
            $(), U(), mobile && H(), z()
        } else L.extendRight(!0)
    }, L.newChapter = function () {
        mobile && (a.deleteMarker("anchor1Marker"), a.deleteMarker("anchor2Marker"), o = L.createAnchorMarker("anchor1"), s = L.createAnchorMarker("anchor2"), addClass(o, "disable"), addClass(s, "disable"))
    }, L.getPageByLocation = function (e, t) {
        var n = e.pages;
        if (n && n.length) {
            for (var r = 0; r < n.length - 1; r++) {
                if (F(t, n[r].lastLocation) <= 0) return r
            }
            return n.length - 1
        }
    }, L.getTopCornerLocation = function () {
        var e = L.getTopCornerElement();
        return L.convertElemsToLocation([e]).left
    }, L.getTopCornerElement = function () {
        for (var e = p.getOb("epubContent"), t = p.getOb("epubContainer"), n = e.children[0], r = h.$getByHandle("content").getScrollPosition().top + 25, o = new Array, i = n.getElementsByClassName("k-section"), a = 0; a < i.length; a++) hasClass(i[a], "cNone") || o.push(i[a]);
        var s, l = document.getElementById("readerHeader").offsetHeight + 10;
        for (a = 0; a < o.length; a++)
            for (var c = o[a].getElementsByTagName("*"), d = 0; d < c.length; d++)
                if (!c[d].children.length) {
                    var u = c[d].getBoundingClientRect();
                    if (u.bottom > l) {
                        if (u.top > l && u.top < l + 10) return c[d];
                        var f = u.top - l;
                        s ? f < s.distance && u.bottom < l + t.offsetHeight && (s = {
                            elem: c[d],
                            bounds: u,
                            distance: f
                        }) : s = {
                            elem: c[d],
                            bounds: u,
                            distance: f
                        }
                    }
                }
        if (s) return s.elem;
        for (a = 0; a < o.length; a++)
            if (0 < o[a].offsetTop - r) return o[a];
        return o[0]
    }, L.getIdArray = function (e) {
        for (var t = L.getTopCornerElement(), n = []; t && t != p.getOb("epubContent");) t.getAttribute && t.getAttribute("id") && n.push(t.getAttribute("id")), t = t.previousSibling ? t.previousSibling : t.parentNode;
        return n
    };

    function _(e) {
        var t = 0;
        for (e && e.parentNode && e.parentNode.hasAttribute("kb-offset") && (t = Number(e.parentNode.getAttribute("kb-offset"))); e = e.previousSibling;) "#text" != e.nodeName && 8 != e.nodeType && t++;
        return t
    }

    function q(e, t) {
        t = t || p.getOb("epubContent");
        for (var n = ""; e != t;) {
            n = _(e) + "." + n, e = e.parentNode
        }
        var r = "left";
        return "" == n ? {
            index: "",
            side: r
        } : ("1" == n[0] && (n = "0" + n.substr(1), r = "right"), {
            index: n.substr(0, n.length - 1),
            side: r
        })
    }
    var H = function () {
        a.refreshMarkerById("anchor1Marker"), a.refreshMarkerById("anchor2Marker")
    },
        z = function () {
            return I = L.convertElemsToLocation(document.getElementsByClassName("highlightSelected"))
        };
    L.convertElemsToLocation = function (e, t) {
        t = t || p.getOb("epubContent");
        for (var n = "", r = "", o = 0; o < e.length; o++) {
            var i = q(e[o], t);
            "left" == i.side ? n += (n ? "," : "") + i.index : r += (r ? "," : "") + i.index
        }
        return {
            left: n,
            right: r
        }
    };
    var $ = function () {
        d = new Array, g = new Array;
        for (var e = p.getOb("epubContent").parentNode, t = l; d.push(t), (t = t.parentNode) != e;);
        for (t = c; g.push(t), (t = t.parentNode) != e;);
        var n = function () {
            for (i = 0; i < d.length; i++)
                for (j = 0; j < g.length; j++)
                    if (g[j] == d[i]) return [g[j], i, j]
        }();
        for (b = n[0], m = n[1], v = n[2], i = 0; i < b.childNodes.length; i++) {
            if (b.childNodes[i] == d[m - 1]) {
                x = !1;
                break
            }
            if (b.childNodes[i] == g[v - 1]) {
                x = !0;
                break
            }
        }
    },
        U = function () {
            try {
                if (W(), addClass(l, "highlightSelected"), addClass(c, "highlightSelected"), l == c) return void (I = {
                    left: q(l)
                });
                if (l == b) return void (I = {
                    left: q(l)
                });
                if (c == b) return void (I = {
                    left: q(c)
                });
                V(x ? c : l, x ? l : c)
            } catch (e) { }
        },
        V = function (e, t, n) {
            var r;
            if ("first" == (n = n || "first")) {
                if (e.parentNode == b) return void V(e, t, "second");
                for (r = e;
                    (r = r.nextSibling) && r;) "#text" != r.nodeName && 8 != r.nodeType && addClass(r, "highlightSelected");
                V(e.parentNode, t, "first")
            } else if ("second" == n) {
                if (t.parentNode == b) return void V(e, t, "third");
                for (r = t;
                    (r = r.previousSibling) && r;) "#text" != r.nodeName && 8 != r.nodeType && addClass(r, "highlightSelected");
                V(e, t.parentNode, n)
            } else if ("third" == n) {
                var o, a = x ? g[v - 1] : d[m - 1],
                    s = x ? d[m - 1] : g[v - 1];
                for (i = 0; i < b.childNodes.length; i++)
                    if (a != b.childNodes[i]) {
                        if (s == b.childNodes[i]) break;
                        o && "#text" != b.childNodes[i].nodeName && 8 != b.childNodes[i].nodeType && addClass(b.childNodes[i], "highlightSelected")
                    } else o = !0
            }
        },
        W = function () {
            try {
                var e = document.getElementsByClassName("highlightSelected");
                if (!e) return;
                for (var t = e.length; t--;) removeClass(e[t], "highlightSelected");
                mobile && f.forceRepaint()
            } catch (e) { }
        };

    function G(e) {
        var t = 1;
        return u.data.book.chapter.viewport && (t = n.get("nav").getFxlScale()), e.getBoundingClientRect().width / t
    }
    L.listener = null, L.addSpanAtCloseTag = function () {
        var e = document.createElement("span"),
            t = x ? l : c;
        return t.parentNode.insertBefore(e, t.nextSibling), e
    }, L.getSelectedNodes = function () {
        return document.getElementsByClassName("highlightSelected")
    }, L.reset = function () {
        W(), mobile && (o && (addClass(o, "disable"), addClass(s, "disable")), s && (removeClass(o, "tween"), removeClass(s, "tween"))), x = E = null, L.listener && L.listener("deactivated")
    }, L.deleteNoteMarker = function (e) {
        var t = document.getElementById("note" + e);
        t && t.parentNode && t.parentNode.removeChild(t), a.deleteMarker("noteMarker" + e)
    }, L.getNoteMarker = function (e) {
        return a.getMarker("noteMarker" + e)
    }, L.createAnchorMarker = function (r) {
        var t = document.createElement("span");
        return t.setAttribute("id", r + "Marker"), t.setAttribute("anchor", r), addClass(t, "anchorMarker"), p.getOb("epubMeta").appendChild(t), t.addEventListener(mobileDebug ? "mousedown" : "touchstart", function (e) {
            addClass(w = t, "selected"), S = null, document.addEventListener(mobileDebug ? "mousemove" : "touchmove", K), T && clearInterval(T), T = setInterval(X, 60), document.addEventListener(mobileDebug ? "mouseup" : "touchend", J), addClass(p.getOb("epubContent"), "defaultCursor"), E = "anchorDragged", e.stopPropagation()
        }), a.addMarker({
            id: r + "Marker",
            elem: t,
            offsetTop: -45,
            offsetLeft: -10,
            target: function () {
                var e, t = 0;
                if ("anchor1" == r) {
                    if (!(e = l)) return void (this.offsetLeft = -10);
                    try {
                        n = "rtl" == getComputedStyle(e).direction
                    } catch (e) { }
                    t = x ? n ? 0 : G(e) : n ? G(e) : 0
                } else {
                    if (!(e = c)) return void (this.offsetLeft = -10);
                    var n;
                    try {
                        n = "rtl" == getComputedStyle(e).direction
                    } catch (e) { }
                    t = x ? n ? G(e) : 0 : n ? 0 : G(e)
                }
                return this.offsetLeft = -10 + t, e
            }
        }), t
    }, L.createNoteMarker = function (n, e, t) {
        if (t = t || {}, !document.getElementById("note" + n.nid)) {
            var r = document.createElement("span");
            r.setAttribute("id", "note" + n.nid), addClass(r, t.class ? t.class : "contentNoteMarker"), p.getOb("epubMeta").appendChild(r);
            var o = 0;
            return o = o || 0, o = -18, a.addMarker({
                id: "noteMarker" + n.nid,
                elem: r,
                offsetTop: o,
                target: function () {
                    return hasClass(this.elem, "contentNoteMarker") ? this.offsetLeft = G(e) - 13 : this.offsetLeft = 0, hasClass(this.elem, "contentNoteMarker") || this.offsetTop < 10 && (this.offsetTop = 10), e
                },
                callback: function () {
                    var t = this;
                    a.checkMarkerExistsAtSameLocation(this.elem, function (e) {
                        return e ? (t.offsetTop += 39, void a.refreshMarker(t)) : n.callback ? n.callback() : void 0
                    })
                }
            }), r
        }
    }, L.deleteBookMarker = function (e) {
        var t = document.getElementById("bookmark" + e);
        t && t.parentNode && t.parentNode.removeChild(t), a.deleteMarker("bookMarker" + e)
    }, L.createBookMarker = function (n, e) {
        var t = document.createElement("span");
        t.setAttribute("id", "bookmark" + n.bmid), addClass(t, "contentBookMarker"), p.getOb("epubMeta").appendChild(t);
        var r = 0;
        return r = r || 0, a.addMarker({
            id: "bookMarker" + n.bmid,
            elem: t,
            offsetLeft: 0,
            offsetTop: r,
            target: function () {
                return this.offsetTop < 10 && (this.offsetTop = 10), e
            },
            callback: function () {
                var t = this;
                a.checkMarkerExistsAtSameLocation(this.elem, function (e) {
                    return e ? (t.offsetTop += 39, void a.refreshMarker(t)) : n.callback ? n.callback() : void 0
                })
            }
        }), t
    }, L.embedAnchorMarker = function (e) { };
    var K = function (e) {
        (S = e).stopPropagation()
    },
        X = function (e) {
            try {
                if (e && e.stopPropagation(), mobile) {
                    if ("anchorDragged" != E) return;
                    if (!S) return
                }
                var t;
                if (mobile) {
                    var n = mobileDebug ? S : S.targetTouches[0];
                    t = document.elementFromPoint(n.clientX, Math.round(n.clientY + o.offsetHeight / 2))
                } else t = e.target;
                if (p.getOb("epubContent") == t) return;
                if (!p.getOb("epubContent").contains(t)) return;
                if (1 < (t.textContent.match(new RegExp(" ", "g")) || []).length) return;
                if (mobile)
                    if ("anchor1" == w.getAttribute("anchor")) {
                        if (l == t) return;
                        l = t
                    } else {
                        if (c == t) return;
                        c = t
                    } else c = t;
                $(), U(), redrawForChrome(l.parentElement), redrawForChrome(c.parentElement), mobile && H()
            } catch (e) { }
        },
        J = function (e) {
            try {
                var t = p.getOb("epubContainer");
                document.removeEventListener(mobile && !mobileDebug ? "touchend" : "mouseup", J), mobile ? (document.removeEventListener(mobileDebug ? "mousemove" : "touchmove", K), removeClass(w, "selected"), clearInterval(T)) : t.removeEventListener("mousemove", X), removeClass(p.getOb("epubContent"), "defaultCursor"), w = null, I = z(), E = "selectionFixed", L.listener && L.listener(E), e.stopPropagation()
            } catch (e) { }
        },
        Y = function (e) {
            try {
                var t = p.getOb("epubContainer");
                l = c = e, mobile ? (removeClass(o, "disable"), removeClass(s, "disable"), timeOut(function () {
                    addClass(o, "tween"), addClass(s, "tween")
                }, 100), H()) : (t.addEventListener("mousemove", X), document.addEventListener("mouseup", J)), z(), E = "activated", L.listener && L.listener(E)
            } catch (e) { }
        };
    return L.active = function () {
        return "selectionFixed" == E || "activated" == E
    }, L.copyToClipboard = function () {
        if (config.clipboardCopy) {
            var t = L.getSelectedText();
            if (t)
                if (native) n.get("crdv").copyToClipboard(t);
                else if (desktop) {
                    require("nw.gui").Clipboard.get().set(t, "text"), status.showAndHide(r.get("copiedToClipboard"), 500)
                } else {
                    var e = document.createElement("textarea");
                    document.body.appendChild(e), e.value = t, e.focus(), e.select();
                    try {
                        document.execCommand("Copy"), n.get("status").showAndHide(r.get("copiedToClipboard"), 500)
                    } catch (e) {
                        n.get("modals").show({
                            mode: "flashUnsupported",
                            flashOb: {
                                text: t
                            }
                        }), setTimeout(function () {
                            document.getElementById("clipboardText").focus(), document.getElementById("clipboardText").select()
                        }, 200)
                    }
                    e.remove()
                }
        }
    }, L
}]);

app.factory("settings", ["$http", "error", "spread", "book", "cache", "modals", "$ionicScrollDelegate", "translit", "$timeout", "virt", "selection", "$injector", "stg", "markers", "sync", "crdv", "cssParser", function (e, t, n, r, d, o, u, i, a, f, s, l, p, c, h, g, m) {
    var v = {};
    return v.pinchActive = !1, v.stylesInitialize = function () { }, v.initialize = function () {
        v.stylingChanged(), v.lineAdjustChanged(), v.updateTextSize(), c.refreshMarkers()
    }, v.getTextSize = function () {
        return Number(p.data.settings.textSize.slice(0, -2))
    }, v.viewModeChanged = function () {
        n.applyMode(p.data.settings.viewMode)
    }, v.updateTextSize = function () {
        try {
            var e = d.getOb("epubContent"),
                t = d.getOb("epubContent").style;
            if (t.fontSize == p.data.settings.textSize) return;
            t.fontSize = p.data.settings.textSize;
            for (var n = 0; n < e.children.length; n++) try {
                e.children[n].style.fontSize = "1em"
            } catch (e) { }
            var r, o = d.getOb("epubContainer"),
                i = e.parentNode.parentNode,
                a = i.style.webkitTransform || i.style.mozTransform || i.style.transform;
            r = overflowScroll ? d.getOb("epubContainer").scrollTop : Number(a.split(",")[1].slice(0, -2));
            var s = e.offsetHeight,
                l = o.offsetTop,
                c = o.offsetHeight;
            s + r < c + l ? f.safeScroll("content", 0, l + s - c, !0) : s < c && u.$getByHandle("content").scrollTop(!1), f.reset()
        } catch (e) { }
    }, v.writeTextSize = function () {
        p.writeSlot("settings" + p.getBookId(!0), p.data.settings, function () {
            c.refreshMarkers()
        })
    }, v.stylingChanged = function () {
        p.data.settings.styling ? m.showStyles() : m.hideStyles(), c.refreshMarkers()
    }, v.saveSettings = function () {
        p.writeSlot("settings" + p.getBookId(!0), p.data.settings)
    }, v.langChanged = function () {
        p.langChanged(), l.get("nav").fxlZoomFit(), p.writeSlot("settings" + p.getBookId(!0), p.data.settings, function () {
            i.setLang(p.data.settings.language), c.refreshMarkers()
        })
    }, v.lineAdjustChanged = function () {
        p.data.settings.lineAdjust ? addClass(d.getOb("epubContent"), "adjustedLineHeight") : removeClass(d.getOb("epubContent"), "adjustedLineHeight"), c.refreshMarkers()
    }, v.sync = function () {
        h.onOnline(function () {
            native ? g.toast(i.get("syncComplete")) : o.show({
                mode: "success",
                successOptions: {
                    msg: i.get("syncComplete")
                }
            })
        })
    }, v
}]);

app.factory("skeleton", ["stg", function (t) {
    var d = {},
        i = [],
        l = [];

    function u() {
        var e = t.data.settings.viewMode;
        return "double" == e && config.singleViewOnPortrait && mobile && 1.3 * window.innerWidth < window.innerHeight && (e = "single"), e
    }

    function f(e) {
        i = [], (e = e || {}).firstBone || (e.firstBone = a());
        for (var t = e.firstBone, n = u(), r = e.lastJoint; t;) {
            var o = [];
            if (t.joint = o, "single" == n ? o.push(t) : "double" == n ? (o.push(t), r && delete t.alone, t.alone || t.index >= config.firstDoublePage && t.next && (o.push(t = t.next), t.joint = o)) : "auto" == n && o.push(t), r && ((o.prev = r).next = o), i.push(o), r = null, t.next && (r = o), e.lastBone == t) break;
            t = s(t, e)
        }
        return i[i.length - 1]
    }

    function a(e) {
        var t;
        e = e || 0;
        for (var n = 0; n < l.length; n++) l[n].index < e || (t ? l[n].index < t.index ? t = l[n] : l[n].index == t.index && l[n].vIndex < t.vIndex && (t = l[n]) : t = l[n]);
        return t
    }

    function o(e) {
        for (var t, n = 0; n < l.length; n++) l[n].index > e || (t ? l[n].index > t.index ? t = l[n] : l[n].index == t.index && l[n].vIndex > t.vIndex && (t = l[n]) : t = l[n]);
        return t
    }

    function s(e, t) {
        return t = t || {}, e.next ? e.next : t.connectedOnly ? void 0 : c(e.index + 1)
    }

    function c(e) {
        var t, n, r = o();
        if (r)
            for (; e <= r.index;) {
                if (t && (e = t.index + 1), n = d.getBone(e)) return n;
                t = null, e++
            }
    }

    function p(e) {
        var t = c(e);
        if (t) return t.joint
    }

    function h(e) {
        var t;
        for (""; e;) e.index + "." + e.vIndex, (e = (t = e).next) && "->";
        return t
    }

    function g(e, t) {
        var n;
        for (t = t || ""; e;) {
            t += " [";
            for (var r = 0; r < e.length; r++) t += e[r].index + "." + e[r].vIndex + ",";
            t = t.substr(0, t.length - 1) + "]", (e = (n = e).next) && (t += "->")
        }
        return n
    }
    return d.reset = function () {
        l = [], i = []
    }, d.initialize = function (e) {
        d.reset();
        for (var t = (e = e || {}).first; t < e.last; t++) d.add({
            index: t
        })
    }, d.trace = function () {
        var e = -1;
        for (; ;) {
            var t = c(e + 1);
            if (!t) break;
            var n = h(t);
            e = n.index
        }
        var r = -1;
        for (; ;) {
            var o = p(r + 1);
            if (!o) break;
            var i = g(o);
            r = i[i.length - 1].index
        }
    }, d.add = function (e) {
        var t, n, r, o, i = d.getBone(e.index);
        if (i) {
            var a = e.pages && e.pages.length;
            if (i.total && a) return;
            if (!i.total && !a) return;
            d.remove(i)
        }
        if (e.pages && e.pages.length)
            for (var s = 0; s < e.pages.length; s++)(o = {}).index = e.index, o.vIndex = s, o.total = e.pages.length, t && ((o.prev = t).next = o), l.push(o), t = o, 0 == s && (n = o), s == e.pages.length - 1 && (r = o);
        else (o = {}).index = e.index, o.vIndex = null, l.push(o), n = r = o;
        ! function (e, t) {
            var n = d.getBone(e.index - 1);
            if (n) {
                var r = n;
                1 < n.total && (r = d.getBone(e.index - 1, n.total - 1)), (r.next = e).prev = r
            }
            var o, i = d.getBone(t.index + 1);
            i && ((i.prev = t).next = i);
            if ("double" == u()) {
                var a = d.getJoint(e.index - 1),
                    s = d.getJoint(t.index + 1);
                if (a) return f();
                var l = t.total ? t.total : 1;
                if (s && 1 == s.length && s[0].index >= config.firstDoublePage && (o = !0), s) {
                    var c;
                    if (o || l % 2 == 0 && (c = !0), o && l % 2 == 1 && (c = !0), c) return f();
                    e.alone = !0
                }
            }
            f()
        }(n, r)
    }, d.remove = function (e) {
        e.prev && e.prev.next && (e.prev.next = null), e.next && e.next.prev && (e.next.prev = null);
        for (var t = 0; t < l.length; t++)
            if (l[t] == e) return void l.splice(t, 1)
    }, d.getBone = function (e, t) {
        for (var n = 0; n < l.length; n++)
            if (l[n].index == e) {
                if (null == t) return l[n];
                if (l[n].vIndex == t) return l[n]
            }
    }, d.normalize = function (e) {
        if (null != e.vIndex) {
            var t = d.getJoint(e.index, e.vIndex);
            if (t) {
                var n = t[0].index,
                    r = t[0].vIndex;
                (n != e.index || null != r && r != e.vIndex) && (e.index = n, null != r && (e.vIndex = r))
            }
        }
    }, d.getJoint = function (e, t) {
        for (var n = 0; n < i.length; n++)
            for (var r = 0; r < i[n].length; r++)
                if (i[n][r].index == e) {
                    if (t <= -1 && i[n][r].total && (t += i[n][r].total), null == t) return i[n];
                    if (null == i[n][r].vIndex) return i[n];
                    if (i[n][r].vIndex == t) return i[n]
                }
    }, d.nextJoint = function (e, t) {
        var n = d.getBone(e, t);
        if (n) {
            var r = n.joint.next;
            if (r) return {
                index: r.index,
                vIndex: r.vIndex
            }
        }
    }, d.prevJoint = function (e, t) {
        var n = d.getBone(e, t);
        if (n) {
            var r = n.joint.prev;
            if (r) return {
                index: r.index,
                vIndex: r.vIndex
            }
        }
    }, d.getNextIndex = function (e, t) {
        var n = {},
            r = d.getJoint(e.index, e.vIndex);
        return r && r.next ? n.leftOb = {
            index: r.next[0].index,
            vIndex: r.next[0].vIndex
        } : n.leftOb = {
            index: r[r.length - 1].index + 1,
            vIndex: 0
        }, t && t(n), n
    }, d.getPrevIndex = function (e, t) {
        var n = {},
            r = d.getJoint(e.index, e.vIndex);
        return r && r.prev ? n.leftOb = {
            index: r.prev[0].index,
            vIndex: r.prev[0].vIndex
        } : n.leftOb = {
            index: r[0].index - 1,
            vIndex: -1
        }, t && t(n), n
    }, d.isSingle = function (e, t) {
        var n = d.getJoint(e, t);
        if (n) return 1 == n.length
    }, d.getCurrentView = function (e, t) {
        var n = d.getJoint(e, t.vIndex);
        if (n) {
            var r = {};
            return r.view = 1 == n.length ? "single" : "double", r.index = e, r
        }
    }, d.isLastChapter = function (e, t) {
        var n = null == (e = e || {}).index ? currentIndex : e.index,
            r = null == e.vIndex ? currentVIndex : e.vIndex;
        return t(d.getJoint(n, r) == o(e.last).joint)
    }, d.isFirstChapter = function (e, t) {
        return e = e || {}, t(d.getJoint(currentIndex, currentVIndex) == a(e.first).joint)
    }, d.isNeighbor = function (e, t) {
        for (var n = d.getJoint(currentIndex, currentVIndex), r = n.next, o = n.prev, i = 0; i < 2; i++) {
            var a = [r, o][i],
                s = ["next", "prev"][i];
            if (a)
                for (var l = 0; l < a.length; l++)
                    if (a[l].index == e.index) {
                        if (null == a[l].vIndex) return t(s);
                        if (a[l].vIndex == e.vIndex) return t(s)
                    }
        }
        t()
    }, d
}]);

app.factory("spread", ["$document", "$injector", "cache", "selection", "skeleton", "chapters", "stg", "$filter", function (e, t, n, w, r, c, d, u) {
    var o = {};

    function k(e) {
        if (!e.childNodes.length) return !1;
        if (e.className.indexOf && 0 <= e.className.indexOf("kInteractive")) {
            if (hasClass(e, "video")) return !1;
            if (hasClass(e, "gallery")) return !1;
            if (hasClass(e, "audio")) return !1;
            if (hasClass(e, "widget")) return !1;
            if (hasClass(e, "widget")) return !1;
            if (hasClass(e, "threed")) return !1
        }
        return !0
    }

    function x(e, t) {
        if (e.style && ("always" == e.style.pageBreakBefore || "always" == getComputedStyle(e).pageBreakBefore)) return e;
        if (e.previousSibling) {
            if (e.previousSibling.nodeName && "span" == e.previousSibling.nodeName.toLowerCase()) return !1;
            if (e.previousSibling.style && ("always" == e.previousSibling.style.pageBreakAfter || "always" == getComputedStyle(e.previousSibling).pageBreakAfter)) return e;
            if (e.previousSibling.childNodes.length)
                for (var n = 0; n < e.previousSibling.childNodes.length; n++) {
                    var r = x(e.previousSibling.childNodes[n]);
                    if (r) return r
                }
        }
        return !(t || !e.nextSibling) && x(e.nextSibling)
    }

    function C(e, t) {
        var n = getFilteredChildIndex(t);
        if (0 < n) {
            var r = e.parentNode,
                o = getFilteredChildIndex(e);
            n != o && (r.getAttribute("kb-offset") || r.setAttribute("kb-offset", n - o))
        }
    }
    return o.initialize = function () { }, o.applyMode = function (e) {
        "double" == e ? o.activateDblPage() : "single" == e ? o.activateSinglePage() : "auto" == e && o.activateAutoPage()
    }, o.activateSinglePage = function () {
        if (c.shouldBePaged(d.data.currentPageOb)) c.regenerateSpreads();
        else {
            c.resetPages({
                buildChapterMode: !0
            });
            var e = {
                viewMode: "single",
                reload: !0
            };
            c.loadChapterByIndex(currentIndex, e, function () { })
        }
    }, o.activateDblPage = function () {
        if (c.shouldBePaged(d.data.currentPageOb)) c.regenerateSpreads();
        else {
            c.resetPages({
                buildChapterMode: !0
            });
            var e = {
                viewMode: "double",
                reload: !0
            };
            c.loadChapterByIndex(currentIndex, e, function () { })
        }
    }, o.activateAutoPage = function () {
        "double" == c.getCurrentView(currentIndex, {
            vIndex: currentVIndex
        }).view ? o.activateDblPage() : o.activateSinglePage()
    }, o.getViewport = function (e) {
        e = e || {};
        var t = document.getElementById("readerBody"),
            n = document.getElementById("readerHeader"),
            r = document.getElementById("tabMenu"),
            o = t.getBoundingClientRect().width,
            i = t.getBoundingClientRect().height;
        mobile && "alwaysShow" != config.autohideReaderMode && "showOnScroll" != config.autohideReaderMode && "showOnTap" != config.autohideReaderMode || (i -= n.getBoundingClientRect().height + r.getBoundingClientRect().height);
        var a = c.getCurrentView(e.index, {
            vIndex: e.vIndex
        });
        a = a || {
            view: "double"
        };
        var s = {},
            l = config.spreadSpacing ? config.spreadSpacing : 0;
        return "double" == a.view ? s.width = Math.round((o - l) / 2) : (s.width = o, "double" == d.data.settings.viewMode && (s.width = o / 2), 1.3 * window.innerWidth < window.innerHeight && (s.width = o)), s.height = i, s
    }, o.parseReflowableChapter = function (e, c) {
        e.raw && !e.pages && (e.content = e.raw), t.get("context").simulateChapter(e, function (a) {
            var s = o.getViewport(e),
                l = {
                    pages: []
                };
            l.pageWidth = s.width, l.pageHeight = s.height, l.url = e.url, l.index = e.index, a.style.width = "100%", a.style.height = "100%", a.style.position = "absolute", a.style.zIndex = -1, a.style.visibility = "hidden", a.setAttribute("data-index", l.index), document.body.appendChild(a), a.addEventListener("error", function (e) { }), a.addEventListener("load", function () {
                var e = a.contentDocument.documentElement,
                    n = e.getElementsByTagName("body")[0];
                n.style.width = s.width + "px", n.style.height = 2 * s.height + "px";
                var t = e.getElementsByTagName("style")[0].innerHTML;
                l.supportsPageBreakStyles = 0 <= t.indexOf("page-break-after") || 0 <= t.indexOf("page-break-before");
                var r = a.contentDocument.getElementById("spreadR");
                r && r.parentNode.removeChild(r), a.contentDocument.getElementById("epubScroller").style.overflowY = "hidden";
                var o = a.contentDocument.getElementById("epubContent"),
                    i = o.outerHTML;
                u("contentVirt")("parse", o, function () {
                    var e = function (e, r) {
                        r.pages || (r.pages = []);
                        r.index || (r.index = 0);
                        r.lastStop || (r.lastStop = document.createElement("div"));
                        var t = r.lastStop,
                            n = [];
                        e.style.width = r.pageWidth + "px";
                        var o = e,
                            i = r.pageHeight - 0,
                            a = getElemMargin(e, {
                                computed: !0
                            }),
                            s = getElemPadding(e, {
                                computed: !0
                            }),
                            l = a.top,
                            c = a.bottom,
                            d = s.top,
                            u = s.bottom;
                        "auto" == l && (l = 0);
                        "auto" == c && (c = 0);
                        "auto" == d && (d = 0);
                        "auto" == u && (u = 0);
                        l = l || 0;
                        c = c || 0;
                        d = d || 0;
                        u = u || 0;
                        a = Number(a), s = Number(s);
                        var f = l + d,
                            p = c + u,
                            h = traverseToBottom(e, "epubContent");
                        for (; ;) {
                            if (!o || 8 == o.nodeType) {
                                if ("continue" == y()) continue;
                                break
                            }
                            var g = getBoundingBox(o);
                            if (g.bottom < i - p) {
                                if (r.supportsPageBreakStyles && x(o, !0)) {
                                    if (b(o, {
                                        src: "breakStyle"
                                    }), i = g.bottom + r.pageHeight - f - p, "break" == y()) break;
                                    continue
                                }
                                var m = o.cloneNode(!0);
                                if (t.appendChild(m), n.length && C(m, o, "less"), "break" == y()) break
                            } else if (k(o)) {
                                m = cloneNodeAttributes(o);
                                t.appendChild(m), n.length && C(m, o, "canDigInside"), t = t.childNodes[t.childNodes.length - 1], o = o.childNodes[0]
                            } else if (b(o, {
                                src: "end"
                            }), "break" == y()) break
                        }

                        function v(e) {
                            if (!e) return "";
                            r.url;
                            for (var t = getElementsByAttribute(e, "*", "src"), n = 0; n < t.length; n++) {
                                t[n].getAttribute("src");
                                0
                            }
                            return e.outerHTML
                        }

                        function b(e) {
                            i = g.top + r.pageHeight - f - p, t && (n.length, n.push({
                                content: v(traverseToTop(t, "epubContent")),
                                lastLocation: w.convertElemsToLocation([e], h).left
                            })), (t = function (e) {
                                ({});
                                var t, n = parents(e);
                                for (var r = 0; r < n.length - 1; r++)
                                    if ("#document" != n[r].nodeName.toLowerCase() && "html" != n[r].nodeName.toLowerCase()) {
                                        var o = cloneNodeAttributes(n[r]);
                                        t = t ? (t.appendChild(o), C(o, n[r], "duplicate"), t.childNodes[0]) : o
                                    }
                                return t
                            }(e)).appendChild(e.cloneNode(!0))
                        }

                        function y() {
                            return o.nextSibling ? (o = o.nextSibling, "sibling") : o.parentNode && "body" != o.nodeName.toLowerCase() ? (o = o.parentNode, t = t.parentNode ? t.parentNode : traverseToBottom(t, "epubContent"), y()) : (t && n.push({
                                content: v(traverseToTop(t, "epubContent"))
                            }), "break")
                        }
                        if (1 == n.length) {
                            o = traverseToBottom(e, "epubContent");
                            n[0].content = v(o)
                        }
                        return n
                    }(n, l);
                    a.parentNode.removeChild(a);
                    var t = {};
                    t.pages = e, t.raw = i, t.viewport = s, c(t)
                })
            })
        })
    }, o
}]);

app.factory("status", ["$ionicLoading", "translit", "$q", function (a, e, s) {
    var t, l = {},
        c = 1e4;

    function d() {
        t && (clearTimeout(t), t = null)
    }

    function u() {
        d(), c && (t = setTimeout(function () {
            l.update().then(function () {
                l.showAndHide(e.get("statusTimeoutError"), 3e3)
            })
        }, c))
    }
    return l.update = function (e, t, n) {
        var r = s.defer();
        if ((n = n || {}).dots && (e += ' <ion-spinner icon="dots" class="loadingSpinner"></ion-spinner>'), n.progress && (e += '<div class="loadingProgress"></div>'), c = n.timeoutDuration ? n.timeoutDuration : 1e4, d(), e) {
            u();
            var o = {
                template: e
            };
            for (var i in n) o[i] = n[i];
            a.show(o), l.updateProgress()
        } else a.hide();
        return timeOut(function () {
            r.resolve()
        }, t || 250), r.promise
    }, l.updateProgress = function (e) {
        1 < (e = e || 0) && (e = 1);
        var t = document.getElementsByClassName("loadingProgress");
        t.length && (t[0].style.width = Math.round(100 * e) + "%"), 0 < e && u()
    }, l.showAndHide = function (e, t) {
        l.update(e, t).then(function () {
            l.update()
        })
    }, l
}]);

app.factory("stg", ["sync", "$rootScope", "serverapi", "modals", function (a, e, serverapi, modal) {
    var s = {
        data: {},
        view: {}
    };

    s.initialize = function () {
        e.data = s.data, e.view = s.view, s.reset(), s.setLanguages(), preview && s.clearSlots()
    };

    s.initializeSettings = function (t) {
        try {
            s.readSlot("settings" + s.getBookId(!0), function (e) {
                s.data.settings = e, 0 == s.data.settings.length && (s.data.settings = {}), n()
            })
        } catch (e) {
            s.data.settings = {}, n()
        }

        function n() {
            s.applyDefaultSettings();
            try {
                if (config.defaultLanguage && (s.data.settings.language = config.defaultLanguage, s.langChanged()), s.data.settings.navAnim = config.defaultNavAnim ? config.defaultNavAnim : "navBookBlock", s.data.settings.viewMode = config.defaultViewMode ? config.defaultViewMode : "auto", s.data.settings.pageScroll = null == config.defaultPageScroll || config.defaultPageScroll, !preview) return void s.readSlot("settings" + s.getBookId(!0), function (e) {
                    e && (e.language && (s.data.settings.language = e.language, s.langChanged()), e.navAnim && (s.data.settings.navAnim = e.navAnim), e.viewMode && (s.data.settings.viewMode = e.viewMode)), t && t()
                })
            } catch (e) { }
            t && t()
        }
    };

    s.langChanged = function () {
        "ar" == s.data.settings.language ? s.data.settings.rtl = !0 : s.data.settings.rtl = !1
    };

    s.reset = function () {
        s.resetBook(), s.resetLibrary()
    };

    s.setLanguages = function () {
        s.data.languages = [{
            label: "English",
            val: "en"
        }, {
            label: "Français",
            val: "fr"
        }, {
            label: "Español",
            val: "es"
        }, {
            label: "Português",
            val: "pt"
        }, {
            label: "Nederlands",
            val: "nl"
        }, {
            label: "Deutsch",
            val: "de"
        }, {
            label: "Magyar",
            val: "hu"
        }, {
            label: "Italiano",
            val: "it"
        }, {
            label: "Svenska",
            val: "sw"
        }, {
            label: "Melayu",
            val: "ms"
        }, {
            label: "Norsk",
            val: "no"
        }, {
            label: "Polski",
            val: "pl"
        }, {
            label: "Român",
            val: "ro"
        }, {
            label: "Pусский",
            val: "ru"
        }, {
            label: "Türkçe",
            val: "tr"
        }, {
            label: "العربية",
            val: "ar"
        }, {
            label: "汉语",
            val: "zh"
        }, {
            label: "日本語",
            val: "jp"
        }, {
            label: "한국어",
            val: "ko"
        }]
    };

    s.resetBook = function () {
        s.data.epub = {}, s.data.toc = "", s.data.mediaList = [], s.data.packageURL = "", s.data.book = {}, s.data.book.chapter = {}, s.data.chromeAssets = []
    };

    s.resetLibrary = function () {
        s.data.downloaded = [], s.data.favorites = [], s.data.currentLibrary = {}
    };

    s.applyDefaultSettings = function () {
        s.data.settings && (s.data.settings.styling || (s.data.settings.styling = !0), s.data.settings.textSize || (s.data.settings.textSize = "1em"), s.data.settings.lineAdjust || (s.data.settings.lineAdjust = !0), s.data.settings.language || (s.data.settings.language = "en"), null == config.viewModeOptional && (config.viewModeOptional = !0), null == config.pageScrollOptional && (config.pageScrollOptional = !0), null == config.navAnimOptional && (config.navAnimOptional = !0), null == config.languageOptional && (config.languageOptional = !0))
    };

    s.getBookId = function (e) {
        if (config.kotobee.cloudid) return "book" == config.kotobee.mode ? "_" + config.kotobee.cloudid : "library" == config.kotobee.mode ? "_" + config.kotobee.cloudid + "_" + (e ? "" : s.data.book.id) : void 0;
        var t, n = "",
            r = "";
        try {
            t = s.data.book.meta.dc.identifier
        } catch (e) { }
        if (desktop) {
            var o = require("nw.gui");
            r = t || o.App.manifest.name + (o.App.manifest.rand ? o.App.manifest.rand : "")
        } else if (native) r = t || (cordova.rand ? cordova.rand : "");
        else {
            r = (r = (r = (r = location.href.split("#")[0]).replace("https://", "http://")).replace("http://www.", "http://")).replace("http://", "");
            try {
                n = e ? "" : t || s.data.book.meta.dc.title
            } catch (e) { }
        }
        return "_" + (r + n).replace(/[\/\s:#]/g, "")
    };

    s.addBookmark = function (t, o) {
        s.readSlot("bookmark" + s.getBookId(), function (r) {
            function i(r) {
                r && (v = r),
                    t.chapter = currentIndex,
                    t.cloudid = config.kotobee.cloudid, v.push(t),
                    serverapi.SaveBookmark({ BookID: s.getBookId(), Chapter: t.chapter, Location: t.location, UserName: "1", Bmid: t.bmid }).then(function (data) {
                        t.BookmarkID = data;

                        s.writeSlot("bookmark" + s.getBookId(), a, function () {
                            return a.queue("bookmark/add", t), o && o(!0), !0
                        });
                        modal.hide();
                        console.log(t);
                    })
            }
            var v = r;
            null != t.bmid ? s.deleteBookmark(t.bmid, null, i) : s.uniqueId(v, "bmid", function (e) {
                t.bmid = e, i()
            })
        })
    };

    s.getBookmarks = function (e) {
        serverapi.GetBookmark(s.getBookId()).then(function (data) {
            delete localStorage["bookmark" + s.getBookId()];
            localStorage["bookmark" + s.getBookId()] = JSON.stringify(data);

            s.readSlot("bookmark" + s.getBookId(), function (t) {
                e && e(t)
            })
        });
    };

    s.deleteBookmark = function (t, o, r) {
        s.readSlot("bookmark" + s.getBookId(), function (i) {
            for (var c = 0; c < i.length; c++)
                if (i[c].bmid == t) {
                    var n = i[c];
                    serverapi.DeleteBookmark(n.BookmarkID);
                    return i.splice(c, 1), void s.writeSlot("bookmark" + s.getBookId(), i, function () {
                        o || a.queue("bookmark/delete", s), r && r(i)
                    })
                }
        })
    };

    s.deleteNote = function (r, o, i) {
        s.readSlot("note" + s.getBookId(), function (e) {
            for (var t = 0; t < e.length; t++)
                if (e[t].nid == r) {
                    var n = e[t];
                    serverapi.DeleteNote(n.NoteID);
                    return e.splice(t, 1), void s.writeSlot("note" + s.getBookId(), e, function () {
                        o || a.queue("note/delete", n), i && i(e)
                    })
                }
        });
    };

    s.addNote = function (o, i) {
        try {
            s.readSlot("note" + s.getBookId(), function (r) {
                function t(e) {
                    e && (r = e), null == o.chapter && (o.chapter = currentIndex), o.cloudid = config.kotobee.cloudid;
                    var t = {};
                    for (var n in o) "event" != n && (t[n] = o[n]);
                    r.push(t),
                        serverapi.SaveNote({ Chapter: o.chapter, Location: o.location, NoteText: o.note, Src: o.src, BookID: s.getBookId(), UserName: 1, Nid: o.nid }).then(function (data) {
                            t.NoteID = data;

                            s.writeSlot("note" + s.getBookId(), r, function () {
                                return a.queue("note/add", t), i && i(!0), !0
                            });
                            modal.hide();
                        })
                }
                null != o.nid ? s.deleteNote(o.nid, null, t) : s.uniqueId(r, "nid", function (e) {
                    o.nid = e, t()
                })
            })
        } catch (e) { }
        return !0
    };

    s.getNotes = function (e) {
        serverapi.GetNote(s.getBookId()).then(function (data) {
            delete localStorage["note" + s.getBookId()];
            localStorage["note" + s.getBookId()] = JSON.stringify(data);
            s.readSlot("note" + s.getBookId(), e);
        });
    };

    s.deleteHlight = function (r, o, i) {
        s.readSlot("hlight" + s.getBookId(), function (e) {
            for (var t = 0; t < e.length; t++)
                if (e[t].hid == r) {
                    var n = e[t];
                    serverapi.DeleteHighlight(n.HighlightID);
                    return e.splice(t, 1), void s.writeSlot("hlight" + s.getBookId(), e, function () {
                        o || a.queue("hlight/delete", n), i && i(e)
                    })
                }
        })
    };

    s.addHlight = function (r, o) {
        s.readSlot("hlight" + s.getBookId(), function (t) {
            function n(e) {
                e && (t = e), null == r.chapter && (r.chapter = currentIndex), r.cloudid = config.kotobee.cloudid, t.push(r),
                    serverapi.SaveHighlight({ Chapter: r.chapter, Location: r.location, BookID: s.getBookId(), ColorCode: r.color, Src: r.src, UserName: 1, Hid: r.hid }).then(function (data) {
                        r.HighlightID = data;

                        s.writeSlot("hlight" + s.getBookId(), t, function () {
                            return a.queue("hlight/add", r), o && o(!0), !0
                        })
                        modal.hide();
                    });
            }
            null != r.hid ? s.deleteHlight(r.hid, null, n) : s.uniqueId(t, "hid", function (e) {
                r.hid = e, n()
            })
        })
    };

    s.getHlightById = function (n, r) {
        s.getHlights(function (e) {
            for (var t = 0; t < e.length; t++)
                if (e[t].hid == n) return r && r(e[t]), e[t]
        })
    };

    s.getHlights = function (e) {
        serverapi.GetHighlight(s.getBookId()).then(function (data) {
            delete localStorage["hlight" + s.getBookId()];
            localStorage["hlight" + s.getBookId()] = JSON.stringify(data);
            s.readSlot("hlight" + s.getBookId(), e);
        });
    };

    s.addAction = function (t, n) {
        s.readSlot("actions" + s.getBookId(), function (e) {
            e.push(t), s.writeSlot("actions" + s.getBookId(), e, function () {
                return n && n(!0), !0
            })
        })
    };

    s.deleteAction = function (n, r) {
        s.readSlot("actions" + s.getBookId(), function (e) {
            for (var t = 0; t < e.length; t++)
                if (JSON.stringify(n) == JSON.stringify(e[t])) return e.splice(t, 1), void s.writeSlot("actions" + s.getBookId(), e, function () {
                    return r && r(e), e
                });
            r && r()
        })
    };

    s.getActions = function (e) {
        s.readSlot("actions" + s.getBookId(), e)
    };

    s.readSlot = function (t, n) {
        var r;
        if (chromeApp) chrome.storage.local.get(t, function (e) {
            e = e ? isEmpty(e) ? [] : e[t] : [], n && n(e)
        });
        else if (native) try {
            NativeStorage.getItem(t, function (e) {
                e || n([]);
                var t = JSON.parse(e);
                n && n(t)
            }, function () {
                n && n([])
            })
        } catch (e) {
            o()
        } else o();

        function o() {
            if (!(r = localStorage[t])) return n && n([]), [];
            var e = JSON.parse(r);
            return n && n(e), e
        }
    };

    s.writeSlot = function (e, t, n) {
        if (chromeApp) {
            var r = {};
            r[e] = t, chrome.storage.local.set(r, function () {
                n && n()
            })
        } else if (native) try {
            NativeStorage.setItem(e, JSON.stringify(t), function () {
                n && n()
            }, function (e) {
                o()
            })
        } catch (e) {
            o()
        } else o();

        function o() {
            localStorage[e] = JSON.stringify(t), n && n()
        }
    };

    s.clearSlot = function (e, t) {
        if (chromeApp) chrome.storage.local.remove(e, function () {
            t && t()
        });
        else if (native) try {
            NativeStorage.remove(e, function () {
                t && t()
            }, function () {
                n()
            })
        } catch (e) { } else n();

        function n() {
            delete localStorage[e], t && t()
        }
    };

    s.clearSlots = function (o) {
        if (chromeApp) chrome.storage.local.clear(o);
        else if (native) try {
            NativeStorage.clear(o, function () {
                e()
            })
        } catch (e) { } else e();

        function e() {
            var n = [];
            for (var e in localStorage) n.push(e);
            var r = -1;
            ! function e() {
                r++;
                if (r >= n.length) return void (o && o());
                var t = n[r];
                0 == t.indexOf("note_") ? s.clearSlot(t, e) : 0 == t.indexOf("bookmark_") ? s.clearSlot(t, e) : 0 == t.indexOf("hlight_") ? s.clearSlot(t, e) : 0 == t.indexOf("settings_") && s.clearSlot(t, e)
            }()
        }
    };

    s.copyLocalToNativeStorage = function () {
        if (native) try {
            for (var e in localStorage) NativeStorage.setItem(e, localStorage[e]), delete localStorage[e]
        } catch (e) { }
    };

    s.uniqueId = function (e, n, r) {
        var o = 0;
        n = n || "id";
        for (var t = 0; t < e.length; t++) Number(e[t][n]) > o && (o = Number(e[t][n]));
        s.readSlot("actions" + s.getBookId(), function (e) {
            for (var t = 0; t < e.length; t++) Number(e[t].data[n]) > o && (o = Number(e[t].data[n]));
            return r && r(o + 1), o + 1
        })
    };

    s.getPublicRoot = function (e) {
        var t = "";
        return s.data.paths && ("book" == config.kotobee.mode || "book" == e ? t = s.data.paths.books : "library" != config.kotobee.mode && "library" != e || (t = s.data.paths.libraries)), t = t || ""
    };

    return s;
}]);

app.factory("sync", ["$injector", "translit", "$rootScope", "$http", "error", function (i, e, t, a, s) {
    var l, c, d, u, f, n, p, h = {},
        g = {
            transformRequest: angular.identity,
            headers: {
                "Content-Type": void 0
            }
        };

    function m() {
        t.data.syncing || (t.data.syncing = !0, t.$$phase || t.$apply())
    }

    function v() {
        t.data.syncing && (t.data.syncing = !1, t.$$phase || t.$apply())
    }
    return h.initialize = function () {
        config.kotobee.cloudid && (c = c || i.get("stg"), d = d || i.get("bookmarks"), u = u || i.get("notes"), f = f || i.get("hlights"), n = n || i.get("chapters"), c.getActions(function (e) {
            l = e, window.addEventListener ? (window.addEventListener("offline", h.onOffline, !1), window.addEventListener("online", h.onOnline, !1)) : (document.addEventListener("offline", h.onOffline, !1), document.addEventListener("online", h.onOnline, !1))
        }))
    }, h.trigger = function () {
        h.offline() ? h.onOffline() : h.onOnline()
    }, h.offline = function () {
        if (i.get("backend").cloudParam("sync")) try {
            return "NONE" == navigator.network.connection.type.toUpperCase() && "UNKNOWN" == navigator.network.connection.type.toUpperCase()
        } catch (e) {
            return !1
        }
    }, h.onOffline = function () {
        i.get("backend").cloudParam("sync")
    }, h.onOnline = function (o) {
        i.get("backend").cloudParam("sync") && (p || h.applyChanges(function () {
            m();
            var e = getFormData(i.get("backend").getAuthVars());
            a.post(config.kotobee.liburl + "library/notebook/all", e, g).success(function (e) {
                if (v(), e.error) return s.update(e), void (o && o());
                var t = e.notes ? e.notes : [],
                    n = e.bookmarks ? e.bookmarks : [],
                    r = e.hlights ? e.hlights : [];
                u.deleteAll(!0, function () {
                    u.addNotesFromSync(t, function () {
                        d.deleteAll(!0, function () {
                            d.addBookmarksFromSync(n, function () {
                                f.deleteAll(!0, function () {
                                    f.addHlightsFromSync(r, function () {
                                        o && o()
                                    })
                                })
                            })
                        })
                    })
                })
            }).error(function (e) {
                t.data.syncing = !1, s.update({
                    error: "cantLoadNodes"
                }), o && o()
            })
        }))
    }, h.queue = function (e, t) {
        if (c && config.kotobee.cloudid && !config.kotobee.public) {
            var n = {
                type: e,
                data: clone(t)
            };
            l.push(n), c.addAction(n, function () {
                h.offline() || p || h.applyChanges()
            })
        }
    }, h.applyChanges = function (e) {
        if (v(), i.get("backend").cloudParam("sync") && c && c.data.user && (c.data.user.email || c.data.user.code)) try {
            var t = {
                func: arguments.callee,
                args: arguments
            };
            if (0 == l.length) return p = !1, void (e && e());
            m(), p = !0;
            var n = l[0],
                r = getFormData(i.get("backend").getAuthVars());
            for (var o in n.data) r.append(o, n.data[o]);
            a.post(config.kotobee.liburl + "library/" + n.type, r, g).success(function (e) {
                l.splice(0, 1), c.deleteAction(n, function () {
                    h.applyChanges()
                })
            }).error(function (e) {
                p = !1, setTimeout(v, 300), s.update({
                    error: "syncOnConnectionMsg",
                    ref: t
                })
            })
        } catch (e) { }
    }, h
}]);

app.factory("tinCan", ["stg", "backend", function (t, n) {
    var e = {};
    return e.initialize = function () { }, e.send = function (e) {
        ! function (e) {
            try {
                if (!config.gAnalyticsID) return;
                if (!ga) return
            } catch (e) {
                return
            }
            ga("send", "event", e.activity, e.verb, t.data.book.meta.dc.title)
        }(e), n.cloudParam("tincan") && (e.name = t.data.user.name ? t.data.user.name : "User", t.data.user.email ? e.email = t.data.user.email : t.data.user.code && (e.email = t.data.user.code + "@promocode"), n.tincan(e))
    }, e
}]);

app.factory("translit", ["$interpolate", "$http", "$injector", "$rootScope", "cache", function (r, e, o, i, a) {
    var s, l, t = {},
        c = {},
        d = [];
    return t.addListener = function (e) {
        d.push(e)
    }, t.setLangOld = function (e) {
        s = e
    }, t.setLang = function (t, n) {
        o.get("backend").getServerLang({
            lang: t
        }).success(function (e) {
            l = e, s = t, i.data && (i.data.lc || (i.data.lc = 0), i.data.lc = Number(i.data.lc) + 1),
                function () {
                    for (var e = 0; e < d.length; e++) d[e](s)
                }(), a.getOb("viewContainer") || a.setOb("viewContainer", document.getElementsByClassName("view-container")[0]), "ar" == t ? (a.getOb("body") && addClass(a.getOb("body"), "rtl"), a.getOb("viewContainer") && a.getOb("viewContainer").setAttribute("dir", "rtl")) : (a.getOb("body") && removeClass(a.getOb("body"), "rtl"), a.getOb("viewContainer") && a.getOb("viewContainer").setAttribute("dir", "ltr")), n && n()
        }).error(function (e, t) { })
    }, t.getLang = function () {
        return s
    }, t.get = function (e, t) {
        if (!l) return e;
        if (!s) return e;
        if (l[e] && (e = l[e]), t)
            for (var n = 0; n < t.length; n++) e = r(e)(t[n]);
        return e
    }, t.getOld = function (e, t) {
        if (c[s][e] && (e = c[s][e]), t)
            for (var n = 0; n < t.length; n++) e = r(e)(t[n]);
        return e
    }, t
}]);

app.factory("view", ["stg", function (r) {
    var e = {};
    return e.generate = function () {
        var e = r.data.currentLibrary,
            t = r.data.currentLibrary.cloud,
            n = r.view;
        n.registration = {}, n.registration.promocode = t && !t.public && e && !e.cloud.promocodesdisabled && e.promocodesettings.inregistration, n.topmenu = {}, n.topmenu.promocode = t && !t.public && e && !e.cloud.promocodesdisabled && e.promocodesettings.intopmenu, n.topmenu = {}, n.topmenu.promocode = t && !t.public && e && !e.cloud.promocodesdisabled && e.promocodesettings.intopmenu, n.bookinfo = {}, n.bookinfo.promocode = t && !t.public && e && !e.cloud.promocodesdisabled && e.promocodesettings.inbookinfo
    }, e
}]);

app.factory("virt", ["$ionicScrollDelegate", "$filter", "$location", "cache", "$q", function (s, n, e, l, i) {
    var t, c, r, o, d = {};
    d.lock = function () {
        c = !0
    }, d.unlock = function () {
        c = !1
    }, d.enable = function () {
        t || c || (t = !0, o && clearInterval(o), r = setInterval(a, 60), a())
    }, d.disableAfter = function (e) {
        o && clearInterval(o), o = setInterval(function () {
            d.disable(), clearInterval(o)
        }, e)
    }, d.disable = function () {
        t && (c || (t = !1, clearInterval(r)))
    };
    var a = function () {
        c || n("contentVirt")()
    };
    return d.reset = function (e, t) {
        c || (d.reveal(!0), n("contentVirt")("reset"), n("contentVirt")(null, e, function () {
            d.reveal(!1), t && t()
        }))
    }, d.checkIfRequiresParse = function (e) {
        hasClass(e, "parsed") || n("contentVirt")("parse", e)
    }, d.checkIfRequiresParseAndAnnotations = function (e) {
        hasClass(e, "parsed") || n("contentVirt")("parseAndAnnotations", e)
    }, d.reveal = function (e) {
        c || n("contentVirt")(e ? "reveal" : "undoReveal")
    }, d.safeAnchorScroll = function (e, t, n) {
        if (!c) {
            var r = i.defer();
            d.reveal(!0);
            var o = document.getElementById(t);
            return d.safeScrollToElem(e, o, n), timeOut(function () {
                d.reset(), r.resolve()
            }, 1300), r.promise
        }
    }, d.safeScroll = function (e, t, n, r) {
        if (!c) {
            var o = i.defer();
            return r = r || !0, d.reveal(!0), s.$getByHandle(e).scrollTo(Math.round(t), Math.round(n), r), timeOut(function () {
                d.reset(), o.resolve()
            }, 1300), o.promise
        }
    }, d.safeScrollToElem = function (e, t, n) {
        if (t && !c) {
            d.reveal(!0);
            var r = l.getOb("epubContainer"),
                o = 0,
                i = getComputedStyle(r).paddingTop;
            i && (o = Number(i.split("px")[0]));
            var a = s.$getByHandle(e).getScrollPosition().top - (r.getBoundingClientRect().top + o) - 25;
            return d.safeScroll(e, 0, t.getBoundingClientRect().top + a, n)
        }
    }, d.forceRepaint = function () {
        try {
            var e = document.createTextNode(" ");
            document.body.appendChild(e), setTimeout(function () { }, 0)
        } catch (e) { }
    }, d
}]);

app.factory("workers", ["stg", function (e) {
    var r, o, i, t, n = {},
        a = [],
        s = [],
        l = [];

    function c(e) {
        t = !0
    }
    n.initialize = function () {
        if (h()) try {
            r = new Worker(rootDir + "js/workers/cssParser.js"), o = new Worker(rootDir + "js/workers/htmlParser.js"), i = new Worker(rootDir + "js/workers/search.js"), r.addEventListener("error", c, !1), o.addEventListener("error", c, !1), i.addEventListener("error", c, !1), r.addEventListener("message", f, !1), o.addEventListener("message", f, !1), i.addEventListener("message", f, !1)
        } catch (e) {
            t = !0
        }
    };
    var d = 0;

    function u(e) {
        if (e.length) {
            var t = e[0].args,
                n = e[0].id;
            p(t[0]).postMessage({
                func: t[1],
                params: t[2],
                id: n
            })
        }
    }

    function f(e) {
        var t = function (e) {
            if (e == r) return a;
            if (e == o) return s;
            if (e == i) return l
        }(e.target);
        ! function (e) {
            for (var t = 0; t < e.length; t++);
        }(t);
        var n = t[0].args[3];
        n && n(e.data.data), t.shift(), u.apply(this, [t])
    }

    function p(e) {
        return "cssParser" == e ? r : "htmlParser" == e ? o : "search" == e ? i : void 0
    }

    function h() {
        return !t && "undefined" != typeof Worker
    }
    return n.call = function (e, t, n, r) {
        if (++d, p(e)) {
            var o = function (e) {
                if ("cssParser" == e) return a;
                if ("htmlParser" == e) return s;
                if ("search" == e) return l
            }(e),
                i = [e, t, n, r];
            o.push({
                args: i,
                id: d
            }), 1 == o.length && u(o)
        }
    }, n.supported = function () {
        return h()
    }, n
}]);

app.factory("bookmarks", ["selection", "translit", "crdv", "popovers", "stg", "cache", "modals", "virt", "chapters", function (r, i, t, a, s, e, l, c, o) {
    var d = {};
    d.addBookmark = function () {
        var e = {};
        e.chapter = currentIndex, e.location = r.getTopCornerLocation(), s.addBookmark(e, function () {
            d.addIcon(e), t.toast(i.get("bookmarked"))
        })
    };

    d.addBookmarksFromSync = function (t, n) {
        var r = -1;
        ! function e() {
            ++r >= t.length ? n && n() : function (t, n) {
                s.readSlot("bookmark" + s.getBookId(), function (e) {
                    e = e || [], t.chapter == currentIndex && (o.busy || arrayContainsProperty(e, "bmid", t.bmid) || d.addIcon(t)), e.push(t), s.writeSlot("bookmark" + s.getBookId(), e, n)
                })
            }(t[r], e)
        }()
    };

    d.addIconIfNotShown = function (e) {
        d.addIcon(e)
    };

    d.addIcon = function (o) {
        var e = r.getElemsByLocation(o.location);
        if (e[0]) {
            var n = r.createBookMarker(o, e[0]);
            if (n.className = "contentBookMarker", n.setAttribute("bmid", o.bmid), n.setAttribute("bloc", e[0].offsetTop), n.addEventListener("tap", function (e) {
                e.stopImmediatePropagation()
            }), n.addEventListener("click", function (e) {
                c.safeScrollToElem("content", n, !0);
                var t = {};
                t.bmid = this.getAttribute("bmid"), t.location = this.getAttribute("bloc")
            }), n.addEventListener("hold", function (e) {
                var t = [{
                    name: i.get("delete"),
                    func: function () {
                        d.delete(o.bmid), a.hide()
                    }
                }, {
                    name: i.get("cancel"),
                    func: function () {
                        a.hide()
                    }
                }];
                a.show({
                    mode: "list",
                    listOb: t,
                    backdropClickToClose: !1
                })
            }), !mobile) {
                var t = document.createElement("span");
                t.className = "editIcon icon ion-ios-close", t.addEventListener("click", function (e) {
                    var t = {
                        title: "confirmation",
                        msg: "bookmarkDeleteConfirm",
                        barStyle: "bar-dark"
                    },
                        n = {
                            label: "delete",
                            style: "button-assertive",
                            func: function () {
                                d.delete(o.bmid), l.hide()
                            }
                        },
                        r = {
                            label: "cancel",
                            func: function () {
                                l.hide()
                            }
                        };
                    t.btns = [n, r], l.show({
                        mode: "confirm",
                        confirmOptions: t
                    })
                }), t.addEventListener("tap", function (e) {
                    e.stopImmediatePropagation()
                }), n.appendChild(t), n.addEventListener("mouseover", function (e) {
                    addClass(n, "edit"), n.addEventListener("mouseout", function (e) {
                        removeClass(n, "edit")
                    })
                })
            }
        }
    };

    d.delete = function (e, t, n) {
        r.deleteBookMarker(e), s.deleteBookmark(e, t, n)
    };

    d.deleteAll = function (r, o) {
        s.getBookmarks(function (t) {
            var n = -1;
            ! function e() {
                n++;
                if (n >= t.length) return void (o && o());
                d.delete(t[n].bmid, r, e)
            }()
        })
    };
    return d;
}]);

app.factory("hlights", ["selection", "crdv", "backend", "stg", "$rootScope", "modals", "chapters", function (i, e, t, a, n, s, l) {
    var c = {};
    c.addHlightsFromSync = function (t, n) {
        var r = -1;
        ! function e() {
            ++r >= t.length ? n && n() : function (t, n) {
                a.readSlot("hlight" + a.getBookId(), function (e) {
                    e = e || [], t.chapter == currentIndex && (l.busy || arrayContainsProperty(e, "hid", t.hid) || c.addHlight(t)), e.push(t), a.writeSlot("hlight" + a.getBookId(), e, n)
                })
            }(t[r], e)
        }()
    };

    c.addHlight = function (e) {
        for (var t = document.getElementsByClassName("hlight" + e.hid), n = t.length; n--;) t[n].style.backgroundColor = null, removeClass(t[n], "contentHlightBG"), removeClass(t[n], "hlight" + e.hid);
        for (n = (t = i.getElemsByLocation(e.location)).length; n--;) t[n] && (addClass(t[n], "contentHlightBG"), addClass(t[n], "hlight" + e.hid), t[n].style.backgroundColor = e.color);
        return c
    };

    c.showPopup = function (r, o) {
        n.$new(!0).hlightOb = {}, r = r || [];
        var e = {
            save: function (e) {
                for (var t = 0; t < r.length; t++) {
                    var n = r[t];
                    null == n.chapter && (n.chapter = l.getElemChapterIndex(i.getSelectedNodes()[0])), n.color = e, null == n.src && (n.src = i.getSelectedText()), a.addHlight(n, function () {
                        c.addHlight(n), t == r.length - 1 && timeOut(function () {
                            s.hide(), o && o(!0)
                        }, 300)
                    })
                }
            }
        };
        e.color = r[0].color ? r[0].color : "#fffb85", r[0].hid && (e.delete = function () {
            for (var e = 0; e < r.length; e++) c.delete(r[0].hid, null, function () {
                e == r.length - 1 && timeOut(function () {
                    s.hide(), o && o()
                }, 300)
            })
        }), s.show({
            mode: "hlight",
            hlightOptions: e
        })
    };

    c.delete = function (e, t, n) {
        for (var r = document.getElementsByClassName("hlight" + e), o = r.length; o--;) o == r.length - 1 && removeClass(r[o], "last"), removeClass(r[o], "contentHlightBG"), r[o].style.backgroundColor = null, removeClass(r[o], "hlight" + e);
        a.deleteHlight(e, t, n)
    };

    c.deleteAll = function (r, o) {
        a.getHlights(function (t) {
            var n = -1;
            ! function e() {
                n++;
                if (n >= t.length) return void (o && o());
                c.delete(t[n].hid, r, e)
            }()
        })
    };
    return c;
}]);

app.factory("notes", ["selection", "translit", "crdv", "$ionicGesture", "stg", "$rootScope", "cache", "modals", "popovers", "$injector", "virt", "$timeout", "chapters", "book", "$sce", function (v, l, e, t, b, n, r, c, d, o, u, f, y, p, h) {
    var g = {};

    function w(e) {
        for (var t = e.split(" "), n = 0; n < t.length; n++)
            if (t[n] && 0 == t[n].trim().indexOf("note")) return t[n].trim().substr(4)
    }
    g.addNotesFromSync = function (t, n) {
        var r = -1;
        ! function e() {
            ++r >= t.length ? n && n() : function (t, n) {
                b.readSlot("note" + b.getBookId(), function (e) {
                    e = e || [], t.chapter == currentIndex && (y.busy || arrayContainsProperty(e, "nid", t.nid) || g.addIcon(t)), e.push(t), b.writeSlot("note" + b.getBookId(), e, n)
                })
            }(t[r], e)
        }()
    };

    g.addNotesFromQuestions = function (e, t) {
        for (var n = e.elem.getElementsByClassName("ques"), r = [], o = [], i = 0; i < n.length; i++)
            for (var a = n[i], s = a.getElementsByTagName("p")[0].textContent, l = a.getElementsByClassName("ans"), c = 0; c < l.length; c++) {
                var d, u = l[c],
                    f = u.className;
                f = f.split(" "), "radio" == u.type ? d = "mcq" : "checkbox" == u.type ? d = "mmcq" : "text" == u.type ? d = "sa" : "textarea" == u.type && (d = "sa");
                var p = w(u.className),
                    h = "";
                if ("mcq" == d || "mmcq" == d) {
                    if (!u.checked) {
                        p && o.push({
                            nid: p,
                            elem: u
                        });
                        continue
                    }
                    h = u.nextElementSibling.textContent
                }
                if ("sa" == d) {
                    if ("" == u.value) {
                        p && o.push({
                            nid: p,
                            elem: u
                        });
                        continue
                    }
                    h = u.value
                }
                var g = {};
                g.chapter || (g.chapter = y.getElemChapterIndex(u)), g.location || (g.location = v.convertElemsToLocation([u], e.root).left, e.rootIndex && (g.location = e.rootIndex + "." + g.location)), g.type || (g.type = "content"), g.src || (g.src = s), g.note = h, p && (g.nid = p), r.push({
                    ob: g,
                    elem: u
                })
            }

        function m(e) {
            e >= o.length ? t && t() : b.deleteNote(o[e].nid, null, function () {
                removeClass(o[e].elem, "note" + o[e].nid), m(++e)
            })
        } ! function e(t) {
            if (t >= r.length) return void m(0);
            b.addNote(r[t].ob, function () {
                addClass(r[t].elem, "note" + r[t].ob.nid), e(++t)
            })
        }(0)
    };

    g.addIcon = function (o, e) {
        if ("page" == o.type) {
            if (!(r = v.getElemsByLocation(o.location))[0]) return;
            var n = v.createNoteMarker(o, r[0], {
                class: "contentSideNoteMarker"
            });
            if (n.setAttribute("nid", o.nid), n.setAttribute("ncontent", o.note ? o.note : ""), n.setAttribute("nloc", o.location), n.addEventListener("click", function (e) {
                u.safeScrollToElem("content", n, !0);
                var t = {};
                t.note = this.getAttribute("ncontent"), t.nid = this.getAttribute("nid"), t.location = this.getAttribute("nloc"), t.type = "page", g.noteClicked([t]), e.stopImmediatePropagation()
            }), n.addEventListener("tap", function (e) {
                e.stopImmediatePropagation()
            }), n.addEventListener("hold", function (e) {
                var t = [{
                    name: l.get("edit"),
                    func: function () {
                        d.hide(), g.noteClicked([o])
                    }
                }, {
                    name: l.get("delete"),
                    func: function () {
                        b.deleteNote(o.nid, null, function () {
                            v.deleteNoteMarker(o.nid), d.hide()
                        })
                    }
                }, {
                    name: l.get("cancel"),
                    func: function () {
                        d.hide()
                    }
                }];
                d.show({
                    mode: "list",
                    listOb: t,
                    backdropClickToClose: !1
                })
            }), !mobile) {
                var t = document.createElement("span");
                t.className = "editIcon icon ion-ios-close", t.addEventListener("click", function (e) {
                    var t = {
                        title: "confirmation",
                        msg: "noteDeleteConfirm",
                        barStyle: "bar-dark"
                    },
                        n = {
                            label: "delete",
                            style: "button-assertive",
                            func: function () {
                                b.deleteNote(o.nid, null, function () {
                                    v.deleteNoteMarker(o.nid)
                                }), c.hide()
                            }
                        },
                        r = {
                            label: "cancel",
                            func: function () {
                                c.hide()
                            }
                        };
                    t.btns = [n, r], c.show({
                        mode: "confirm",
                        confirmOptions: t
                    }), e.stopImmediatePropagation()
                }), t.addEventListener("tap", function (e) {
                    e.stopImmediatePropagation()
                }), n.appendChild(t), n.addEventListener("mouseover", function (e) {
                    addClass(n, "edit"), n.addEventListener("mouseout", function (e) {
                        removeClass(n, "edit")
                    })
                })
            }
        }
        if ("content" == o.type) {
            for (var r = v.getElemsByLocation(o.location, e), i = 0; i < r.length; i++) r[i] && addClass(r[i], "note" + o.nid);
            if (1 == r.length && r[0]) {
                var a = r[0].type;
                if ("radio" == a || "checkbox" == a) return void (r[0].checked = !0);
                if ("text" == a || "textarea" == a) return void (r[0].value = o.note)
            }
            for (i = 0; i < r.length; i++) r[i] && addClass(r[i], "contentNoteBG");
            var s = r[r.length - 1];
            if (!s) return;
            s.setAttribute("ncontent", o.note ? o.note : ""), s.setAttribute("nloc", o.location), addClass(s, "last"), f(function () {
                try {
                    var e = v.createNoteMarker(o, s);
                    e && (e.onclick = function (e) {
                        var t = this.getAttribute("id"),
                            n = document.getElementsByClassName(t),
                            r = n[n.length - 1],
                            o = (r.getAttribute("ncontent"), {});
                        o.note = r.getAttribute("ncontent"), o.location = r.getAttribute("nloc"), o.nid = t.substr(4), o.type = "content", o.src = r.textContent, g.noteClicked([o]), e.stopImmediatePropagation(), e.preventDefault()
                    }, e.addEventListener("tap", function (e) {
                        e.stopImmediatePropagation()
                    }))
                } catch (e) { }
            })
        }
    };

    g.noteClicked = function (i, a) {
        try {
            var e = {
                save: function (e) {
                    for (var n = 0; n < i.length; n++) {
                        var r = i[n];
                        r.chapter || (r.chapter = y.getElemChapterIndex(v.getSelectedNodes()[0])), r.location || (r.location = v.getTopCornerLocation()), r.type || (r.type = "page"), r.src || (r.src = "content" == r.type ? v.getSelectedText() : "");
                        var o = null == r.nid;
                        r.note = e, b.addNote(r, function () {
                            if (o) g.addIcon(r);
                            else {
                                v.getNoteMarker(r.nid).elem.setAttribute("ncontent", r.note);
                                var e = v.getElemsByLocation(r.location),
                                    t = e[e.length - 1];
                                t && t.setAttribute("ncontent", r.note)
                            }
                            n == i.length - 1 && f(function () {
                                c.hide(), a && a(!0)
                            }, 300)
                        })
                    }
                }
            };
            (i = i || [{}])[0].note && (e.note = i[0].note, e.delete = function () {
                for (var e = 0; e < i.length; e++) g.delete(i[0].nid, null, function () {
                    e == i.length - 1 && f(function () {
                        c.hide(), a && a()
                    }, 300)
                })
            }), c.show({
                mode: "note",
                dynamic: !0,
                animation: "slide-in-up",
                noteOptions: e,
                event: i[0].event
            })
        } catch (e) { }
    };

    g.delete = function (e, t, n) {
        v.deleteNoteMarker(e);
        for (var r = document.getElementsByClassName("note" + e), o = r.length; o--;) o == r.length - 1 && (removeClass(r[o], "last"), r[o].removeAttribute("nloc"), r[o].removeAttribute("ncontent")), removeClass(r[o], "contentNoteBG"), removeClass(r[o], "note" + e);
        b.deleteNote(e, t, n)
    };

    g.deleteAll = function (r, o) {
        b.getNotes(function (t) {
            var n = -1;
            ! function e() {
                n++;
                if (n >= t.length) return void (o && o());
                g.delete(t[n].nid, r, e)
            }()
        })
    };

    g.updateNotebookArray = function (i, a) {
        var s = new Array;
        b.getBookmarks(function (e) {
            for (var t = 0; t < e.length; t++) {
                var n = e[t];
                s[n.chapter] || (s[n.chapter] = new Array), s[n.chapter].push(n)
            }
            b.getNotes(function (e) {
                for (var t = 0; t < e.length; t++) {
                    var n = e[t];
                    s[n.chapter] || (s[n.chapter] = new Array), n.noteTrusted = "", n.note && (n.noteTrusted = h.trustAsHtml(n.note.replace(new RegExp("\r?\n", "g"), "<br />"))), s[n.chapter].push(n)
                }
                b.getHlights(function (e) {
                    for (var t = 0; t < e.length; t++) {
                        var n = e[t];
                        s[n.chapter] || (s[n.chapter] = new Array), s[n.chapter].push(n)
                    }
                    for (var n in s) s[n].title = p.getTitleByIndex(n);
                    for (t = 0; t < s.length; t++)
                        if (s[t])
                            for (var r = 0; r < s[t].length; r++)
                                if (s[t][r].location) {
                                    var o = String(s[t][r].location).split(",");
                                    s[t][r].locationDec = Number("0." + o[0].split(".").join(""))
                                }
                    b.data.notebookContent = {
                        results: s,
                        showDelete: i.showDelete
                    }, a && a()
                })
            })
        })
    };

    return g;
}]);

app.factory("serverapi", ["$http", "$q", function ($http, $q) {

    var api = {}; var rootUrl = "http://localhost:54360/api/";

    api.SaveBookmark = function (paramObj) {
        return $http.post(rootUrl + "Bookmarks/SaveBookMark", paramObj).then(function (result) { return result.data; });
    };

    api.GetBookmark = function (BookId) {
        var UserId = 1;
        return $http.get(rootUrl + "Bookmarks/GetBookmarks/" + UserId + "/" + BookId).then(function (result) {
            var Bookmarks = [];
            angular.forEach(result.data, function (val, idx) {
                Bookmarks.push({ BookmarkID: val.BookmarkID, chapter: val.Chapter, location: val.Location, bmid: val.Bmid });
            });
            return Bookmarks;
        });
    };

    api.DeleteBookmark = function (bmId) {
        return $http.delete(rootUrl + "Bookmarks/DeleteBookMark/" + bmId).then(function (result) { return result.data; });
    };

    api.SaveHighlight = function (ParamObj) {
        return $http.post(rootUrl + "Highlight/SaveHighlight", ParamObj).then(function (result) { return result.data; });
    };

    api.GetHighlight = function (BookId) {
        var UserId = 1;
        return $http.get(rootUrl + "Highlight/GetHighlights/" + UserId + "/" + BookId).then(function (result) {
            var Highlights = [];
            angular.forEach(result.data, function (val, idx) {
                Highlights.push({ HighlightID: val.HighlightID, chapter: val.Chapter, location: val.Location, src: val.Src, color: val.ColorCode, hid: val.Hid });
            })
            return Highlights;
        });
    };

    api.DeleteHighlight = function (HId) {
        return $http.delete(rootUrl + "Highlight/DeleteHighlight/" + HId).then(function (result) { return result.data; });
    };

    api.SaveNote = function (ParamObj) {
        return $http.post(rootUrl + "Notes/SaveNotes", ParamObj).then(function (result) { return result.data; });
    }

    api.DeleteNote = function (NoteId) {
        return $http.delete(rootUrl + "Notes/DeleteNote/" + NoteId).then(function (result) { return result.data; });
    }

    api.GetNote = function (BookId) {
        var UserId = 1;
        return $http.get(rootUrl + "Notes/GetNotes/" + UserId + "/" + BookId).then(function (result) {
            var Notes = [];

            angular.forEach(result.data, function (val, idx) {
                Notes.push({ NoteID: val.NoteID, type: "content", chapter: val.Chapter, location: val.Location, note: val.NoteText, src: val.Src, nid: val.Nid });
            })
            return Notes;
        });
    }

    return api;
}]);

var kInteractive = {
    preRender: function (e, t) {
        var n, r = (t = t || {}).singleElem,
            o = e || document;
        r ? (n = [e], o = document) : n = o.getElementsByClassName("kInteractive"), void 0 !== isKotobee && (kInteractive.absoluteURL = t.chapterUrl ? t.chapterUrl : angular.element(document.body).scope().data.book.chapter.absoluteURL);
        for (var i = n.length; i--;) {
            var a = n[i];
            this.hasClass(a, "gallery") && kInteractive.gallery.preRender(a, o, i), this.hasClass(a, "questions") && kInteractive.questions.preRender(a, o, i), !this.hasClass(a, "image") && "img" != a.nodeName.toLowerCase() || kInteractive.image.preRender(a, o, i), this.hasClass(a, "link") && kInteractive.link.preRender(a, o, i), this.hasClass(a, "container") && kInteractive.container.preRender(a, o, i)
        }
    },
    postRender: function (t, e) {
        var n = (e = e || {}).singleElem;
        void 0 !== isKotobee && (kInteractive.absoluteURL = e.chapterUrl ? e.chapterUrl : angular.element(document.body).scope().data.book.chapter.absoluteURL), kInteractive.videoIsFullscreen = !1, kInteractive.timestamp = new Date, kInteractive.currentVideo = kInteractive.currentAudio = null, kInteractive.clearAudioVideo(t);
        var r, o = t || document;
        n ? (r = [t], o = document) : r = o.getElementsByClassName("kInteractive");
        for (var i = r.length; i--;) {
            var a = r[i];
            this.hasClass(a, "container") ? kInteractive.container.postRender(o, a, i, e) : this.hasClass(a, "questions") ? kInteractive.questions.postRender(o, a, i, e) : this.hasClass(a, "widget") ? kInteractive.widget.postRender(o, a, i, e) : this.hasClass(a, "video") ? kInteractive.video.postRender(o, a, i) : this.hasClass(a, "audio") ? kInteractive.audio.postRender(o, a, i) : this.hasClass(a, "threed") ? kInteractive.threed.postRender(o, a, i, e) : this.hasClass(a, "gallery") ? kInteractive.gallery.postRender(o, a, i) : this.hasClass(a, "image") && kInteractive.image.postRender(o, a, i)
        }
        this.firstRun && (window.addEventListener("resize", function (e) {
            kInteractive.resizeEvent(t, n)
        }), this.firstRun = !1), kInteractive.resizeEvent(t, n)
    },
    actionEvent: function (e) {
        kInteractive.action(e.target)
    },
    action: function (e, t) {
        if ("undefined" == typeof editorMode || !editorMode) {
            void 0 !== isKotobee && (kInteractive.absoluteURL = angular.element(document.body).scope().data.book.chapter.absoluteURL);
            for (var n = e; !this.hasClass(n, "kInteractive");) {
                if (!n.parentNode) return;
                if ((n = n.parentNode) == document.body) return
            }
            this.hasClass(n, "questions") ? kInteractive.questions.action(n, e) : this.hasClass(n, "widget") ? kInteractive.widget.action(n) : this.hasClass(e, "link") ? kInteractive.link.action(n, e) : this.hasClass(e, "image") ? kInteractive.image.action(n, e) : this.hasClass(n, "gallery") ? kInteractive.gallery.action(n, e) : this.hasClass(n, "audio") ? kInteractive.audio.action(n, e) : this.hasClass(n, "video") ? kInteractive.video.action(n, e) : this.hasClass(n, "threed") && kInteractive.threed.action(n, e)
        }
    },
    trigger: function (e, t) {
        this.action(t, e)
    },
    resizeEvent: function (e, t) {
        var n = e || document;
        t && (n = document);
        for (var r = n.getElementsByClassName("kInteractive"), o = r.length; o--;) {
            var i = r[o];
            this.hasClass(i, "image") ? kInteractive.image.resize(i) : this.hasClass(i, "gallery") ? kInteractive.gallery.resize(i) : this.hasClass(i, "container") ? kInteractive.container.resize(i) : this.hasClass(i, "video") ? kInteractive.video.resize(i) : this.hasClass(i, "widget") && kInteractive.widget.resize(i)
        }
    },
    vQueue: [],
    youTubeReady: !1,
    firstRun: !0
};

if (void 0 === isKotobee) {
    kotobee = {};
    var kotobeeListeners = [];

    function getKotobeeListener(e) {
        if (e)
            for (var t = kotobeeListeners.length; t--;)
                if (kotobeeListeners[t].event == e) return kotobeeListeners[t]
    }

    function clearKotobeeListeners(e) {
        for (var t = kotobeeListeners.length; t--;) e && kotobeeListeners[t].event != e || kotobeeListeners.splice(t, 1)
    }

    function dispatchKotobeeEvent() { }

    function kInteractionStart() {
        this.log = function (e) { }, kInteractive.setDOMParser(), kInteractive.preRender(), kInteractive.postRender()
    }

    function kInteractionStartSingleElem(e) {
        kInteractive.setDOMParser(), kInteractive.preRender(e, {
            singleElem: !0
        }), kInteractive.postRender(e, {
            singleElem: !0
        })
    }
    document.addEventListener("DOMContentLoaded", function (e) {
        "undefined" != typeof kotobeeReady && kotobeeReady(e), document.dispatchEvent(new Event("kotobeeReady")), document.dispatchEvent(new Event("kotobeeChapterLoaded")), kotobee.dispatchEvent("ready"), kotobee.dispatchEvent("chapterLoaded"), document.addEventListener("scroll", function (e) {
            kotobee.dispatchEvent("scrolled", e)
        })
    }), kotobee.addEventListener = function () {
        if (!(arguments.length < 2)) {
            var e, t = arguments[0],
                n = {};
            e = 2 == arguments.length ? arguments[1] : (n = arguments[1], arguments[2]), n.unique && clearKotobeeListeners(t);
            var r = {
                event: t,
                cb: e
            };
            kotobeeListeners.push(r)
        }
    }, kotobee.dispatchEvent = function (e, t) {
        if (e) {
            var n = getKotobeeListener(e);
            n && n.cb && n.cb.apply(this, [t])
        }
    };
    try {
        document.addEventListener("DOMContentLoaded", kInteractionStart, !1)
    } catch (e) {
        window.addEventListener("load", kInteractionStart, !1)
    }
}

kInteractive.audio = {
    preRender: function (e, t) { },
    postRender: function (e, t, n) {
        var r = e.createDocumentFragment(),
            o = kInteractive.readData(t);
        t.setAttribute("id", "ki-audio-" + n), t.innerHTML = "";
        var i = document.createElement("div");
        i.setAttribute("id", "ki-audio-" + n + "-container"), i.className = "container";
        var a = document.createElement("a");
        a.className = "playBtn ki-btn", a.appendChild(document.createElement("span")), void 0 === isKotobee && a.addEventListener("click", kInteractive.actionEvent);
        var s = document.createElement("div");
        o.style && kInteractive.c.addClass(s, o.style), r.appendChild(a), r.appendChild(i), s.appendChild(r), t.appendChild(s)
    },
    action: function (e) {
        kInteractive.stopCurrentMedia();
        var t = kInteractive.readData(e);
        if (t) {
            t.audioType || (t.audioType = t.type);
            e.getAttribute("id");
            e.getElementsByClassName("playBtn")[0].className = "playBtn ki-btn hide";
            var n = t.src;
            if ("file" == t.audioType && (n = void 0 === isKotobee ? t.audio : t.relToRoot ? ph.join(bookPath, t.audio) : ph.join(kInteractive.absoluteURL, t.audio)), kInteractive.scorm) {
                var r = {};
                r.id = kInteractive.getScormId(e.getAttribute("id"), n), r.description = "Played audio: " + n, r.type = "other", r.learnerResponses = "Played", r.objective = t.options ? t.options.objective : null, r.timestamp = new Date, kInteractive.scorm.setInteractions([r])
            }
            var o = document.createElement("audio");
            o.setAttribute("controls", "true"), o.setAttribute("autoplay", "true"), o.setAttribute("data-tap-disabled", "false");
            var i = document.createElement("source");
            i.src = n, o.appendChild(i), o.appendChild(document.createTextNode("Your browser does not support the audio element")), o.className = "ki-noHighlight", o.oncanplay = function () {
                kInteractive.currentAudio == e && kInteractive.tinCan({
                    verb: "played",
                    activity: "Audio: " + n
                })
            }, e.getElementsByClassName("container")[0].appendChild(o), o.play(), kInteractive.currentAudio = e, kInteractive.c.addClass(kInteractive.currentAudio, "playing")
        }
    }
};

var kInteractiveCommon = {
    checkResponsiveFloat: function (e, t) {
        if (e && e.disableWrapForMobile && "none" != e.float) {
            var n = t.parentNode;
            t.offsetWidth > .6 * n.offsetWidth && .4 * (n.offsetWidth - t.offsetWidth) < 100 ? kInteractive.c.addClass(t, "fullRow") : kInteractive.c.removeClass(t, "fullRow")
        }
    },
    openFrame: function (u) {
        var f = document.getElementById("kInteractiveFrame"),
            p = document.getElementById("kiDarkOverlay"),
            h = "";
        if (u.dict || (u.dict = {}), u.width && (h += "width:" + u.width + "px;max-width:" + u.width + "px;"), u.height && (h += "height:" + u.height + "px;max-height:" + u.height + "px;"), f || ((p = document.createElement("div")).id = "kiDarkOverlay", document.body.appendChild(p), p.style.display = "none", p.style.position = "fixed", p.addEventListener("click", kInteractive.closeFrame), (f = document.createElement("div")).id = "kInteractiveFrame"), u.pos || (u.pos = "top"), "none" != u.pos) {
            var e = document.createElement("a"),
                t = "closeBtn";
            u.pos && (t += " " + u.pos), e.className = t;
            var n = u.dict.close ? u.dict.close : "Close";
            try {
                n = angular.element(document.body).scope().translit("close")
            } catch (e) { }
            "side" != u.pos && (e.innerHTML = n), e.addEventListener("click", kInteractive.closeFrame), f.appendChild(e)
        }
        var g = u.class ? u.class : "";

        function r() {
            p.style.display = "block", p.className = "show";
            var e = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName("body")[0].clientHeight,
                t = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName("body")[0].clientWidth;
            e -= 20, t -= 20;
            var n = 1;
            if (u.width && u.height) {
                var r = t / u.width,
                    o = e / u.height;
                1 < (n = r < o ? r : o) && (n = 1)
            }
            var i = "display:block;";
            i += h;
            var a = u.width || u.height ? "-50%,-50%" : "0%,0%";
            i += "-webkit-transform:translate(" + a + ") scale(" + n + ");-moz-transform:translate(" + a + ") scale(" + n + ");transform:translate(" + a + ") scale(" + n + ");", i += "position:fixed;", void 0 === isKotobee && "-50%,-50%" == a && (i += "top:" + Math.round(window.innerHeight / 2) + "px;", i += "left:" + Math.round(window.innerWidth / 2) + "px;"), f.style.cssText = i, f.className = g + " ready";
            var s = f.getElementsByClassName("closeBtn");
            if (s.length) {
                var l = s[0],
                    c = 1 / n,
                    d = "-50%";
                kInteractive.hasClass(l, "side") && (d = "0%"), l.style.cssText = "-webkit-transform: translate(" + d + ",0%) scale(" + c + ");-webkit-transform-origin: 50% 50%;-moz-transform: translate(" + d + ",0%) scale(" + c + ");-moz-transform-origin: 50% 50%;transform: translate(" + d + ",0%) scale(" + c + ");transform-origin: 50% 50%;"
            }
        } (u.width || u.height) && (g += " fixed"), f.style.display = "block", f.className = g, document.body.appendChild(f), kInteractive.frameIsOpen = !0, kInteractive.frameOb = u, kInteractive.openFrame.resized = r, window.addEventListener("resize", r), setTimeout(function () {
            r(), u.cb && u.cb(f)
        }, 100)
    },
    closeAlert: function (e) {
        e && e.stopPropagation();
        var t = document.getElementById("kiSystemAlertBox"),
            n = document.getElementById("kiSystemAlertBackdrop");
        t.parentNode.removeChild(t), n.parentNode.removeChild(n), kInteractive.alertIsOpen = !1, window.removeEventListener("resize", kInteractive.alert.resized), kInteractive.alert.resized = null
    },
    closeFrame: function () {
        var e = document.getElementById("kInteractiveFrame"),
            t = document.getElementById("kiDarkOverlay");
        kInteractive.frameIsOpen = !1, window.removeEventListener("resize", kInteractive.openFrame.resized), kInteractive.openFrame.resized = null, kInteractive.c.removeClass(e, "ready");
        var n = kInteractive.frameOb,
            r = "";
        n.width && (r += "width:" + n.width + "px;max-width:" + n.width + "px;"), n.height && (r += "height:" + n.height + "px;max-height:" + n.height + "px;"), e.style.cssText = r, t.className = "", setTimeout(function () {
            e.innerHTML = "", e.style.display = t.style.display = "none", n.closed && n.closed(), kInteractive.frameOb = null
        }, 300)
    },
    stopCurrentMedia: function () {
        if (kInteractive.currentVideo) {
            var e = kInteractive.currentVideo.getAttribute("id") + "-container";
            if (t = document.getElementById(e)) t.parentNode.removeChild(t), (n = document.createElement("div")).setAttribute("id", e), n.className = "container", kInteractive.currentVideo.getElementsByClassName("playBtn")[0].className = "playBtn ki-btn", kInteractive.c.removeClass(kInteractive.currentVideo, "playing"), kInteractive.currentVideo.appendChild(n)
        }
        var t, n;
        if (kInteractive.currentAudio)
            if (e = kInteractive.currentAudio.getAttribute("id")) e += "-container", (t = document.getElementById(e)) && (t.parentNode.removeChild(t), (n = document.createElement("div")).setAttribute("id", e), n.className = "container", kInteractive.currentAudio.getElementsByClassName("playBtn")[0].className = "playBtn ki-btn", kInteractive.c.removeClass(kInteractive.currentAudio, "playing"), kInteractive.currentAudio.appendChild(n));
            else try {
                var r = kInteractive.currentAudio.getElementsByTagName("audio");
                r.length && (r[0].pause(), r[0].src = ""), kInteractive.currentAudio.pause && (kInteractive.currentAudio.pause(), kInteractive.currentAudio.src = "");
                for (var o = document.getElementsByClassName("kiAudioLoader"), i = o.length; i--;) o[i].parentNode.removeChild(o[i])
            } catch (e) { }
    },
    hasClass: function (e, t) {
        if (e && e.className)
            for (var n = e.className.trim().split(" "), r = 0; r < n.length; r++)
                if (n[r] && n[r].trim() == t) return !0
    },
    appendAfterDelay: function (e, t) {
        setTimeout(function () {
            e.appendChild(t)
        }, 30)
    },
    replaceHTML: function (e, t) {
        var n = "string" == typeof e ? document.getElementById(e) : e,
            r = n.cloneNode(!1);
        return r.innerHTML = t, n.parentNode.replaceChild(r, n), r
    },
    tinCan: function (e) {
        if (void 0 !== isKotobee) try {
            angular.element(document.body).scope().tinCanShortcut(e)
        } catch (e) { }
    },
    setDOMParser: function () {
        ! function (e) {
            "use strict";
            var t = e.prototype,
                i = t.parseFromString;
            try {
                if ((new e).parseFromString("", "text/html")) return
            } catch (e) { }
            t.parseFromString = function (e, t) {
                if (/^\s*text\/html\s*(?:;|$)/i.test(t)) {
                    var n, r = document.implementation.createHTMLDocument(""),
                        o = r.documentElement;
                    return o.innerHTML = e, n = o.firstElementChild, 1 === o.childElementCount && "html" === n.localName.toLowerCase() && r.replaceChild(n, o), r
                }
                return i.apply(this, arguments)
            }
        }(DOMParser)
    },
    readData: function (e) {
        try {
            var t = e.getAttribute("data-kotobee");
            return t = t || e.getAttribute("data"), JSON.parse(decodeURI(kInteractive.XORCipher().decode("kotobee%%author", t)))
        } catch (e) {
            try {
                return JSON.parse(kInteractive.XORCipher().decode("kotobee%%author", t))
            } catch (e) { }
        }
    },
    writeData: function (e, t) {
        var n = encodeURI(JSON.stringify(t)),
            r = e.hasAttribute("data-kotobee") ? "data-kotobee" : "data";
        e.setAttribute(r, kInteractive.XORCipher().encode("kotobee%%author", n))
    },
    XORCipher: function () {
        var u = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        return {
            encode: function (e, t) {
                return function (e) {
                    var t, n, r, o, i, a, s, l, c = 0,
                        d = "";
                    if (!e) return e;
                    for (; t = e[c++], n = e[c++], r = e[c++], o = (s = t << 16 | n << 8 | r) >> 12 & 63, i = s >> 6 & 63, a = 63 & s, d += u.charAt(s >> 18 & 63) + u.charAt(o) + u.charAt(i) + u.charAt(a), c < e.length;);
                    return ((l = e.length % 3) ? d.slice(0, l - 3) : d) + "===".slice(l || 3)
                }(t = function (n, e) {
                    return o(e, function (e, t) {
                        return e.charCodeAt(0) ^ r(n, t)
                    })
                }(e, t))
            },
            decode: function (e, t) {
                return function (n, e) {
                    return o(e, function (e, t) {
                        return String.fromCharCode(e ^ r(n, t))
                    }).join("")
                }(e, t = function (e) {
                    var t, n, r, o, i, a, s, l, c = 0,
                        d = [];
                    if (!e) return e;
                    e += "";
                    for (; o = u.indexOf(e.charAt(c++)), i = u.indexOf(e.charAt(c++)), a = u.indexOf(e.charAt(c++)), s = u.indexOf(e.charAt(c++)), t = (l = o << 18 | i << 12 | a << 6 | s) >> 16 & 255, n = l >> 8 & 255, r = 255 & l, d.push(t), 64 !== a && (d.push(n), 64 !== s && d.push(r)), c < e.length;);
                    return d
                }(t))
            }
        };

        function r(e, t) {
            return e.charCodeAt(Math.floor(t % e.length))
        }

        function o(e, t) {
            for (var n = [], r = 0; r < e.length; r++) n[r] = t(e[r], r);
            return n
        }
    },
    getScormId: function (e, t) {
        return e + (t = t ? "-" + t.replace(/[^a-zA-Z0-9]/g, "-").substr(0, 80) : "")
    },
    confirm: function (e) {
        kInteractive.alert(e)
    },
    alert: function (e) {
        if (e) {
            var t = document.createElement("div");
            if (t.setAttribute("id", "kiSystemAlertBox"), e.raw && (t.className = "raw"), t.style.position = "fixed", void 0 === isKotobee && (t.style.top = Math.round(window.innerHeight / 2) + "px", t.style.left = Math.round(window.innerWidth / 2) + "px"), e.raw) t.innerHTML = e.content.replace("&nbsp;", "&#160;");
            else {
                var n = document.createElement("div");
                n.innerHTML = e.content.replace("&nbsp;", "&#160;"), n.className = "content", t.appendChild(n)
            }
            if (e.title) {
                var r = document.createElement("div");
                r.innerHTML = e.title.replace("&nbsp;", "&#160;"), r.className = "header", t.insertBefore(r, t.firstChild)
            }
            kInteractive.alertIsOpen = !0;
            var o = document.createElement("a");
            if (!e.raw) {
                var i = document.createElement("div");
                i.className = "footer";
                var a = "OK";
                try {
                    a = angular.element(document.body).scope().translit("ok")
                } catch (e) { }
                if (e.okBtn && (a = e.okBtn), o.innerHTML = a.replace("&nbsp;", "&#160;"), o.className = "okBtn", o.addEventListener("click", kInteractive.closeAlert), e.cb && o.addEventListener("click", e.cb), i.appendChild(o), e.noBtn) {
                    var s = document.createElement("a");
                    s.innerHTML = e.noBtn, s.className = "cancelBtn", s.addEventListener("click", kInteractive.closeAlert), i.appendChild(s)
                }
                t.appendChild(i)
            }
            var l = document.createElement("div");
            e.noBackdrop || (l.setAttribute("id", "kiSystemAlertBackdrop"), l.addEventListener("click", kInteractive.closeAlert), l.style.position = "fixed", document.body.appendChild(l)), document.body.appendChild(t), o.focus(), kInteractive.alert.resized = c, window.addEventListener("resize", c), setTimeout(function () {
                kInteractive.c.addClass(t, "show"), kInteractive.c.addClass(l, "show")
            }, 50)
        }

        function c() {
            void 0 === isKotobee && (t.style.top = Math.round(window.innerHeight / 2) + "px", t.style.left = Math.round(window.innerWidth / 2) + "px")
        }
    },
    c: {
        addClass: function (e, t) {
            var n = e.className.trim();
            if (t instanceof Array) {
                for (var r = !0, o = 0; o < t.length; o++) - 1 == n.indexOf(t[o]) && (r = !1, n += ("" == n ? "" : " ") + t[o]);
                if (r) return
            } else {
                if (0 <= n.indexOf(t)) return;
                n += ("" == n ? "" : " ") + t
            }
            e.className = n
        },
        toggleClass: function (e, t) {
            this.strHasClass(e.className, t) ? this.removeClass(e, t) : this.addClass(e, t)
        },
        hasClass: function (e, t) {
            return this.strHasClass(e.className, t)
        },
        strHasClass: function (e, t) {
            for (var n = (e = e || "").trim().split(" "), r = 0; r < n.length; r++)
                if (n[r] == t) return !0;
            return !1
        },
        removeClass: function (e, t) {
            var n = e.className;
            if (t instanceof Array) {
                for (var r = !0, o = 0; o < t.length; o++) 0 <= n.indexOf(t[o]) && (r = !1, n = n.replace(t[o], ""));
                if (r) return
            } else {
                if (-1 == n.indexOf(t)) return;
                n = (n = n.replace(t, "")).replace(/ {2,}?/g, " ")
            }
            "" == (n = n.trim()) ? e.removeAttribute("class") : e.className = n
        },
        removeHash: function (e) {
            return -1 == e.indexOf("#") ? e : e.substring(0, e.lastIndexOf("#"))
        },
        removeFilename: function (e) {
            var t = kInteractive.c.normalizedArray(e);
            return -1 == t[t.length - 1].indexOf(".") ? e : (t.splice(t.length - 1, 1), t.join("/"))
        },
        normalizedArray: function (e) {
            for (var t = (e = e.replace(/\\/g, "/")).split("/"), n = t.length; n--;) t[n] ? ".." == t[n] && 0 < n && ".." != t[n - 1] && (t.splice(n, 1), t.splice(n - 1, 1)) : t.splice(n, 1);
            return t[0].match(/http[s]?:$/g) && (t[0] += "//" + t[1], t.splice(1, 1)), "file:" == t[0] && (t[0] += "///" + t[1], t.splice(1, 1)), t
        },
        shuffleArray: function (e) {
            e.sort(function () {
                return Math.random() - .5
            })
        },
        allowDrop: function (e) {
            e.preventDefault()
        },
        drag: function (e) {
            e.dataTransfer && e.dataTransfer.setData("text", e.target.id), dragged = e.target
        },
        drop: function (e) {
            e.preventDefault(), e.target.hasAttribute("ondragover") && e.target.appendChild(e.view.dragged)
        }
    },
    escapeHtml: function (e) {
        var t = {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#039;"
        };
        return e.replace(/[&<>"']/g, function (e) {
            return t[e]
        })
    },
    getChildIndex: function (e) {
        for (var t = 0; null != (e = e.previousSibling);) t++;
        return t
    },
    isAbsolutePath: function (e) {
        if (e) return 0 == e.trim().indexOf("http://") || (0 == e.trim().indexOf("https://") || (0 == e.trim().indexOf("//") || (0 == e.trim().indexOf("data:") || void 0)))
    },
    isMobile: function (e) {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) return !0;
        try {
            if (0 < window.location.href.indexOf("?preview")) {
                var t = window.location.href.split("?");
                t = t[1].split("&");
                for (var n = {}, r = 0; r < t.length; r++) {
                    var o = t[r].split("=");
                    n[o[0]] = o[1]
                }
                if (n.mobile) return !0
            }
        } catch (e) { }
    },
    closeFullscreenVideo: function () {
        kInteractive.currentVideo && (kInteractive.currentVideo.webkitExitFullscreen ? kInteractive.currentVideo.webkitExitFullscreen() : kInteractive.currentVideo.mozCancelFullScreen ? kInteractive.currentVideo.mozCancelFullScreen() : document.webkitExitFullscreen ? document.webkitExitFullscreen() : document.mozCancelFullScreen && document.mozCancelFullScreen(), kInteractive.videoIsFullscreen = !1)
    },
    getWidgetHome: function (e, t) {
        return {}, e.path ? void 0 === isKotobee ? e.path : ph.join(angular.element(document.body).scope().data.book.chapter.absoluteURL, e.path) : void 0 === isKotobee ? "../js/widgets/" : bookPath + "EPUB/js/widgets/"
    },
    getWidgetUrl: function (e, t) {
        var n = kInteractive.getWidgetHome(e, t) + "/" + e.name + "/" + e.src;
        if (void 0 !== isKotobee) {
            var r = angular.element(document.body).scope().data.user;
            r && r.email && r.signatures && r.signatures.bookwidgets && e.id && 0 == e.id.indexOf("com.kidimedia") && (n += "?oauth_signature=" + r.signatures.bookwidgets.signature, n += "&oauth_nonce=" + r.signatures.bookwidgets.nonce, n += "&oauth_timestamp=" + r.signatures.bookwidgets.timestamp, n += "&oauth_consumer_key=" + r.signatures.bookwidgets.key, n += "&lti_version=LTI-1p0", n += "&oauth_signature_method=HMAC-SHA1", n += "&oauth_version=1.0", n += "&tool_consumer_info_product_family_code=kotobee", n += "&lis_person_contact_email_primary=" + r.email, n += "&lis_person_name_full=" + (r.name ? r.name : ""), n += "&user_id=" + r.id, n += "&lti_message_type=basic-lti-launch-request", angular.element(document.body).scope().refreshSignatures && angular.element(document.body).scope().refreshSignatures())
        }
        return n
    },
    clearAudioVideo: function (e) {
        var t = e || document;
        kInteractive.stopCurrentMedia();
        for (var n = t.getElementsByTagName("audio"), r = n.length; r--;) try {
            if (n[r].hasAttribute("data-dontclose")) continue;
            n[r].pause(), n[r].children.length || (n[r].src = "")
        } catch (e) { }
        var o = t.getElementsByTagName("video");
        for (r = o.length; r--;) o[r].pause(), o[r].children.length || (o[r].src = "")
    }
};
for (var item in kInteractiveCommon) kInteractive[item] = kInteractiveCommon[item];

kInteractive.container = {
    preRender: function (e, t) {
        kInteractive.readData(e)
    },
    postRender: function (e, t, n) {
        var r = kInteractive.readData(t);
        if (r && (kInteractive.isMobile() && "cScroll" == r.type && void 0 !== isKotobee && angular.element(document.body).scope().applyNativeScroll(t), r.path)) {
            var o = r.path;
            void 0 !== isKotobee && (o = ph.join(kInteractive.absoluteURL, r.path));
            var i = t.getAttribute("style");
            t.setAttribute("style", i + ' background-image:url("' + o + '");')
        }
    },
    action: function (e) { },
    resize: function () {
        var e = kInteractive.readData(item);
        e && kInteractive.checkResponsiveFloat(e, item)
    }
};

kInteractive.gallery = {
    preRender: function (e, t) {
        var n = t.createDocumentFragment(),
            r = kInteractive.readData(e);
        if (r) {
            e.innerHTML = "";
            var o = document.createElement("div");
            o.className = "imgMask";
            var i = document.createElement("div");
            i.className = "images " + r.scale, o.appendChild(i);
            for (var a = 0; a < r.imgs.length; a++) {
                var s = document.createElement("div");
                s.className = "imgContainer" + (a ? "" : " selected");
                var l = void 0 === isKotobee ? r.imgs[a].path : ph.join(kInteractive.absoluteURL, r.imgs[a].path);
                s.setAttribute("style", "background-color:" + r.bgColor + ";background-image:url('" + l + "')" + (a ? "" : ";display:block")), r.imgs[a].caption && s.setAttribute("data-caption", r.imgs[a].caption), i.appendChild(s)
            }
            n.appendChild(o);
            var c = document.createElement("div");
            c.className = "imgCaption";
            var d = document.createElement("div");
            d.className = "inner", d.style.display = "none", r.imgs[0] && r.imgs[0].caption && (d.innerHTML = r.imgs[0].caption, d.style.display = null), c.appendChild(d), n.appendChild(c);
            var u = document.createElement("a");
            u.className = "next btn ki-btn", 1 == r.imgs.length && (u.className += " disable"), n.appendChild(u);
            var f = document.createElement("a");
            f.className = "prev btn ki-btn disable", n.appendChild(f), void 0 === isKotobee && (u.addEventListener("click", kInteractive.actionEvent), f.addEventListener("click", kInteractive.actionEvent));
            var p = document.createElement("div");
            r.style && kInteractive.c.addClass(p, r.style), p.appendChild(n), e.appendChild(p)
        }
    },
    postRender: function (e, t, n) { },
    action: function (e, t) {
        var n = e.getElementsByClassName("images")[0],
            r = (n.children.length, e.getElementsByClassName("selected")[0]),
            o = Number(r.style.left.slice(0, -2)),
            i = Number(n.style.left.slice(0, -2));
        if (kInteractive.hasClass(t, "next")) {
            var a = r.nextSibling;
            if (!a) return;
            kInteractive.c.removeClass(r, "selected"), kInteractive.c.addClass(a, "selected"), a.style.left = o + n.offsetWidth + "px", a.style.display = "block", n.style.left = i - n.offsetWidth + "px", a.nextSibling ? kInteractive.c.removeClass(t, "disable") : kInteractive.c.addClass(t, "disable"), kInteractive.c.removeClass(e.getElementsByClassName("prev")[0], "disable"), r = a
        } else if (kInteractive.hasClass(t, "prev")) {
            var s = r.previousSibling;
            if (!s) return;
            kInteractive.c.removeClass(r, "selected"), kInteractive.c.addClass(s, "selected"), s.style.left = o - n.offsetWidth + "px", s.style.display = "block", n.style.left = i + n.offsetWidth + "px", s.previousSibling ? kInteractive.c.removeClass(t, "disable") : kInteractive.c.addClass(t, "disable"), kInteractive.c.removeClass(e.getElementsByClassName("next")[0], "disable"), r = s
        }
        var l = e.getElementsByClassName("imgCaption");
        if (l.length) {
            var c = (l = l[0]).getElementsByClassName("inner");
            c.length && ((c = c[0]).style.display = "none", r.hasAttribute("data-caption") && (c.innerHTML = r.getAttribute("data-caption"), c.style.display = null))
        }
    },
    resize: function (e) {
        var t = kInteractive.readData(e);
        t && kInteractive.checkResponsiveFloat(t, e)
    }
};

kInteractive.image = {
    preRender: function (e, t) {
        var n = kInteractive.readData(e);
        n && (n.style && kInteractive.c.addClass(e, n.style), e.parentNode && kInteractive.hasClass(e.parentNode, "link") || void 0 === isKotobee && e.addEventListener("click", kInteractive.actionEvent))
    },
    postRender: function (e, t, n) {
        kInteractive.readData(t)
    },
    action: function (e, r, t) {
        if (r.parentNode && kInteractive.hasClass(r.parentNode, "kInteractive") && kInteractive.hasClass(r.parentNode, "link")) kInteractive.action(r.parentNode, event);
        else {
            var n = kInteractive.readData(r);
            if (n) {
                var o = n.behavior;
                if ("none" != o) s(r, "none"), l(r, "none"), c(r, "none"), setTimeout(function () {
                    "wiggle" == o ? (s(r, "wiggle"), l(r, "0.5s"), c(r, "1")) : "jump" == o ? (s(r, "jump"), l(r, "0.7s"), c(r, "1")) : "scale" == o && (s(r, "scale"), l(r, "0.5s"), c(r, "1"));
                    var n = function () {
                        var e, t = document.createElement("fakeelement"),
                            n = {
                                animation: "animationend",
                                OAnimation: "oAnimationEnd",
                                MozAnimation: "animationend",
                                WebkitAnimation: "webkitAnimationEnd"
                            };
                        for (e in n)
                            if (void 0 !== t.style[e]) return n[e]
                    }();
                    n && r.addEventListener(n, function e(t) {
                        s(r, "none"), l(r, "none"), c(r, "none"), r.removeEventListener(n, e)
                    })
                }), kInteractive.tinCan({
                    verb: "clicked",
                    activity: o + "image: " + r.getAttribute("src")
                });
                else if (n.popup) {
                    var i = {
                        class: "image"
                    },
                        a = document.createElement("img");
                    a.src = n.path, kInteractive.isAbsolutePath(n.path) || void 0 !== isKotobee && (a.src = ph.join(kInteractive.absoluteURL, n.path)), i.width = a.naturalWidth, i.height = a.naturalHeight, i.pos = "side", i.cb = function (e) {
                        e.appendChild(a)
                    }, i.closed = function () { }, kInteractive.openFrame(i)
                }
            }
        }

        function s(e, t) {
            e.style.webkitAnimation = e.style.mozAnimation = e.style.animation = t
        }

        function l(e, t) {
            e.style.webkitAnimationDuration = e.style.mozAnimationDuration = e.style.animationDuration = t
        }

        function c(e, t) {
            e.style.webkitAnimationIterationCount = e.style.mozAnimationIterationCount = e.style.animationIterationCount = t
        }
    },
    resize: function (e) {
        var t = kInteractive.readData(e);
        t && kInteractive.checkResponsiveFloat(t, e)
    }
};

kInteractive.link = {
    preRender: function (e, t) {
        var n = kInteractive.readData(e);
        if (n && ("undefined" == typeof editorMode || !editorMode)) {
            if ("div" == e.nodeName.toLowerCase()) {
                var r = document.createElement("a");
                r.setAttribute("href", "popup" == n.type ? "" : e.getAttribute("href")), r.setAttribute("target", n.target), r.setAttribute("style", e.getAttribute("style")), r.setAttribute("data-kotobee", e.getAttribute("data-kotobee")), r.className = e.className, r.id = e.id ? e.id : n.id, kInteractive.c.addClass(r, "btn"), r.style.opacity = Number(n.transparency) / 100, e.parentNode.replaceChild(r, e), e = r
            }
            void 0 === isKotobee && ("popup" != n.type && "audio" != n.type && "action" != n.type || e.setAttribute("onclick", "return false;"), e.addEventListener("click", kInteractive.actionEvent))
        }
    },
    postRender: function (e, t, n) { },
    action: function (e, t, n) {
        var r = kInteractive.readData(t);
        if (r) {
            if ("popup" == r.type) {
                var o = "<div class='kbAlertScroll'>" + r.msg + "</div>";
                if (o = o.replace(/src="(.*?)"/g, function () {
                    var e = arguments[1];
                    return void 0 !== isKotobee && (e = ph.join(kInteractive.absoluteURL, e)), 'src="' + e + '"'
                }), "image" == r.popupMode) {
                    var i = r.popupImg;
                    void 0 !== isKotobee && (i = ph.join(kInteractive.absoluteURL, i)), o = "<img src='" + i + "'/>"
                }
                kInteractive.alert({
                    content: o
                }), kInteractive.scorm && setTimeout(function () {
                    var e = {};
                    e.id = kInteractive.getScormId("popup", r.msg), e.description = "Shown popup message: " + r.msg, e.type = "other", e.learnerResponses = "Shown", e.timestamp = new Date, kInteractive.scorm.setInteractions([e])
                }, 800)
            } else if ("action" == r.type) {
                var a = r.asset,
                    s = r.action;
                if (a.remote) {
                    if ("widget" == a.type) {
                        var l = document.createElement("div");
                        l.className = "kInteractive widget kbHidden", l.setAttribute("data-kotobee", a.data), document.body.appendChild(l), kInteractive.trigger("click", l)
                    }
                    return
                }
                for (var c = document.getElementsByClassName(a.classname), d = 0; d < c.length; d++)
                    if (kInteractive.hasClass(c[d], "kInteractive") && kInteractive.hasClass(c[d], a.type))
                        if ("activate" == s)
                            if ("gallery" == a.type) {
                                var u = c[d].getElementsByClassName("next");
                                kInteractive.trigger("click", u[0])
                            } else if ("questions" == a.type) {
                                for (var f = c[d].getElementsByClassName("ki-btn"), p = 0; p < f.length; p++)
                                    if ("submit" == f[p].type) {
                                        kInteractive.trigger("click", f[p]);
                                        break
                                    }
                            } else kInteractive.trigger("click", c[d]);
                        else "visibility" == s && kInteractive.c.toggleClass(c[d], "kbHidden")
            } else if ("audio" == r.type) {
                kInteractive.stopCurrentMedia();
                var h, g = document.createElement("audio"),
                    m = r.src;

                function v() {
                    h || (kInteractive.c.addClass(y, "hide"), h = !0)
                }
                "file" == r.audioType && (m = void 0 === isKotobee ? r.audio : ph.join(kInteractive.absoluteURL, r.audio)), g.setAttribute("src", m);
                var b = document.createElement("div");
                b.className = "kiAudioLoader", b.style.transform = b.style.webkitTransform = b.style.mozTransform = "scale(1)";
                var y = document.createElement("div");
                if (y.className = "kiAudioSpinner", b.appendChild(y), t.parentNode.insertBefore(b, t), g.onplaying = v, setTimeout(v, 5e3), g.play(), kInteractive.currentAudio = g, kInteractive.tinCan({
                    verb: "played",
                    activity: "Audio: " + m
                }), kInteractive.scorm) {
                    var w = {};
                    w.id = kInteractive.getScormId(t.textContent, m), w.description = "Played audio link: " + m, w.type = "other", w.learnerResponses = "Played", w.objective = r.options ? r.options.objective : null, w.timestamp = new Date, kInteractive.scorm.setInteractions([w])
                }
            } else if ("attachment" == r.type) {
                var k = r.file;
                void 0 !== isKotobee && (k = ph.join(kInteractive.absoluteURL, r.file));
                var x = document.createElement("a");
                document.body.appendChild(x), x.setAttribute("style", "display: none"), x.href = k, x.target = "_blank", x.download = r.file.split("/")[r.file.split("/").length - 1], x.setAttribute("onclick", "");
                var C = document.createEvent("MouseEvents");
                C.initMouseEvent("click"), x.dispatchEvent(C), x.remove()
            }
            return !1
        }
    }
};

kInteractive.questions = {
    preRender: function (e, t, n, r) {
        var o = t.createDocumentFragment(),
            i = kInteractive.readData(e);
        if (i) {
            var a = i.dict,
                s = i.userDict;
            if (a = a || {}, s = s || a, i.options || (i.options = {}), e.id || (e.id = "ki-qs-" + n + "-" + Math.ceil(1e3 * Math.random())), e.innerHTML = "", i.layout && i.layout.popup && !kInteractive.hasClass(e, "inpopup")) {
                var l = document.createElement("img"),
                    c = i.layout.popupImg;
                return l.src = c, o.appendChild(l), void 0 === isKotobee && l.addEventListener("click", kInteractive.actionEvent), void e.appendChild(o)
            }
            if (i.options.randomize) {
                for (var d = 0; d < i.q.length; d++) i.q[d].origin = d;
                for (var u, f, p = i.q.length; 0 !== p;) f = Math.floor(Math.random() * p), p -= 1, u = i.q[p], i.q[p] = i.q[f], i.q[f] = u
            }
            if (i.options.questionsDisplayed) {
                var h = Number(i.options.questionsDisplayed);
                i.q = i.q.slice(0, h)
            }
            if (i.title) {
                var g = document.createElement("span");
                g.innerHTML = "<h4>" + kInteractive.escapeHtml(i.title) + "</h4>", o.appendChild(g)
            }
            for (d = 0; d < i.q.length; d++) {
                var m = i.q[d],
                    v = document.createElement("div");
                if (v.className = "ques", null != m.origin && v.setAttribute("data-o", m.origin), v.innerHTML = "<p><strong>" + (i.options.numbering ? "<span class='no'>" + (d + 1) + "</span>" : "") + kInteractive.escapeHtml(" " + m.q) + "</strong></p>", m.path) {
                    var b = document.createElement("img");
                    b.src = m.path, v.appendChild(b)
                }
                if (v.classList.add(m.type), "sa" == m.type) {
                    var y = m.lines ? m.lines : 1,
                        w = document.createElement(1 < y ? "textarea" : "input");
                    w.name = "sa-" + d, 1 == y ? w.type = "text" : w.rows = m.lines ? m.lines : 1, w.className = "ans txtfield";
                    var k = "ki-sa-" + n + "-" + d;
                    w.id = k, document[e.id] && document[e.id].preserve && document[e.id].q && document[e.id].q[d] && (w.value = document[e.id].q[d]), v.appendChild(w)
                } else if ("mcq" == m.type || "mmcq" == m.type)
                    for (var x = m.c, C = 0; C < x.length; C++) {
                        k = "ki-mcq-" + n + "-" + d + "-" + C;
                        var I = document.createElement("input");
                        "mmcq" == m.type ? (I.name = "name", I.type = "checkbox") : (I.name = "mcq-" + d, I.type = "radio"), I.className = "ans", I.id = k, (W = document.createElement("label")).htmlFor = k, W.innerHTML = " " + kInteractive.escapeHtml(x[C].t), W.className = "ki-noHighlight", (T = document.createElement("div")).className = "answer", document[e.id] && document[e.id].preserve && document[e.id].q && document[e.id].q[d] && (I.checked = document[e.id].q[d][C], i.options && i.options.highlightCorrect && i.q[d].c[C].a && kInteractive.c.addClass(T, "correct")), T.appendChild(I), T.appendChild(W), v.appendChild(T)
                    } else if ("tf" == m.type)
                    for (C = 0; C < 2; C++) {
                        k = "ki-tf-" + n + "-" + d + "-" + C;
                        var E = document.createElement("input");
                        E.type = "radio", E.name = "tf-" + d, E.className = "ans", E.id = k, (W = document.createElement("label")).htmlFor = k, m.choice1 && m.choice2 ? W.innerHTML = kInteractive.escapeHtml(" " + (0 == C ? m.choice1 : m.choice2)) : W.innerHTML = " " + (0 == C ? "True" : "False"), W.className = "ki-noHighlight";
                        var T = document.createElement("div");
                        document[e.id] && document[e.id].preserve && document[e.id].q && document[e.id].q[d] && (E.checked = document[e.id].q[d][C], i.options && i.options.highlightCorrect && (0 == C && i.q[d].a || 1 == C && !i.q[d].a) && kInteractive.c.addClass(T, "correct")), T.appendChild(E), T.appendChild(W), v.appendChild(T)
                    } else if ("dd" == m.type) {
                        var S = document.createElement("div");
                        S.className = "categoryContainer";
                        var A = [];
                        for (C = 0; C < m.k.length; C++) {
                            var O = m.k[C],
                                B = C,
                                L = document.createElement("div");
                            L.className = "dCategory ki-noHighlight ki-dd-category-" + B, L.setAttribute("ondragover", "kInteractive.c.allowDrop(event)"), L.setAttribute("ondrop", "kInteractive.c.drop(event)");
                            var N = document.createElement("div");
                            N.innerHTML = O.category.name, N.className = "qTitle", L.appendChild(N);
                            for (var M = 0; M < O.category.answers.length; M++) A.push(O.category.answers[M].t);
                            S.appendChild(L)
                        }
                        v.appendChild(S);
                        var P = document.createElement("div");
                        P.className = "ki-dd-clearfix", v.appendChild(P);
                        var R = document.createElement("div");
                        R.setAttribute("ondragover", "kInteractive.c.allowDrop(event)"), R.setAttribute("ondrop", "kInteractive.c.drop(event)"), R.className = "ki-dd-answers ki-noHighlight";
                        var D = [];
                        for (C = 0; C < A.length; C++) {
                            B = C, T = A[C];
                            var F = document.createElement("div");
                            k = "ki-dd-" + n + "-" + d + "-" + B;
                            F.id = k, F.innerHTML = "<span></span>", F.children[0].innerText = T, F.className = "ans ki-noHighlight", F.draggable = !0, F.setAttribute("ondragstart", "kInteractive.c.drag(event)"), D.push(F)
                        }
                        kInteractive.c.shuffleArray(D);
                        for (C = 0; C < D.length; C++) R.appendChild(D[C]);
                        v.appendChild(R)
                    }
                if ((U = document.createElement("p")).className = "separator", v.appendChild(U), document[e.id] && document[e.id].preserve && document[e.id].q && document[e.id].q[d]) {
                    if (i.q[d].exp)
                        if (!v.getElementsByClassName("explanation").length) {
                            var _ = document.createElement("p");
                            _.className = "explanation", _.innerHTML = kInteractive.escapeHtml(i.q[d].exp), kInteractive.appendAfterDelay(v, _)
                        }
                    if (i.q[d].ref)
                        if (!v.getElementsByClassName("reference").length) {
                            var q = document.createElement("a");
                            q.className = "reference", q.innerHTML = s.learnMode ? s.learnMode : "Learn more", q.setAttribute("href", kInteractive.escapeHtml(i.q[d].ref)), q.setAttribute("target", "_blank"), kInteractive.appendAfterDelay(v, q)
                        }
                }
                o.appendChild(v)
            }
            if (i.options.emailForm) {
                var H = document.createElement("div");
                H.setAttribute("class", "emailForm"),
                    function (e, t, n) {
                        var r = document.createElement("div");
                        r.className = "address", r.innerHTML = "<p><strong>" + e + "</strong><em>" + t + "</em></p>";
                        var o = document.createElement("input");
                        o.name = n, o.type = "text", o.className = "txtfield", r.appendChild(o);
                        var i = document.createElement("p");
                        i.className = "separator", r.appendChild(i), H.appendChild(r)
                    }(s.recipientEmail ? s.recipientEmail : "Recipient email", s.separateEmails ? s.separateEmails : "Separate multiple emails with commas", "recipientEmail"), o.appendChild(H)
            }
            var z = null;
            if ("none" != i.action) {
                var $ = "ki-" + n + "-btn";
                (z = document.createElement("input")).type = "submit", z.value = s.submit ? s.submit : "Submit Answers", z.id = $, z.className = "btn ki-btn ki-mcq"
            }
            var U, j = null,
                V = null;
            if (i.options && (i.options.clearAnswers && ((j = document.createElement("input")).type = "submit", j.value = s.clear ? s.clear : "Clear Answers", j.className = "btn ki-btn ki-questions questions-clear", void 0 === isKotobee && j.addEventListener("click", kInteractive.actionEvent)), i.options.addToNotebook && ((j = document.createElement("input")).type = "submit", j.value = s.notebook ? s.notebook : "Save to Notebook", j.className = "btn ki-btn ki-questions questions-notebook", void 0 === isKotobee && j.addEventListener("click", kInteractive.actionEvent)), i.options.preserveAnswers)) {
                (V = document.createElement("div")).style.marginTop = "5px";
                var W, G = document.createElement("input");
                G.type = "checkbox", G.name = "preserveAnswers", G.className = "ki-btn ki-questions questions-preserve", G.id = "ki-" + n + "-preserveAnswersBtn", document[e.id] && (G.checked = document[e.id].preserve), V.appendChild(G), (W = document.createElement("label")).htmlFor = G.id, W.innerHTML = " " + (s.preserve ? s.preserve : "Preserve submitted answers"), W.className = "ki-noHighlight", V.appendChild(W), void 0 === isKotobee && G.addEventListener("change", kInteractive.actionEvent)
            }
            void 0 === isKotobee && z && z.addEventListener("click", kInteractive.actionEvent), z && o.appendChild(z), o.appendChild(document.createTextNode(" ")), j && o.appendChild(j), V && o.appendChild(V), (U = document.createElement("p")).className = "separator", o.appendChild(U), i.options.rtl ? (e.style.direction = "rtl", kInteractive.c.addClass(e, "rtl")) : e.style.direction = "ltr";
            var K = document.createElement("div");
            i.style && kInteractive.c.addClass(K, i.style), K.appendChild(o), e.appendChild(K)
        }
    },
    postRender: function (e, t, n) {
        var r = kInteractive.readData(t);
        if (r) {
            var o = r.dict,
                i = r.userDict;
            o = o || {}, i = i || o;
            for (var a = t.getElementsByClassName("ques"), s = t.getElementsByClassName("explanation"), l = s.length; l--;) s[l].parentNode.removeChild(s[l]);
            var c = t.getElementsByClassName("reference");
            for (l = c.length; l--;) c[l].parentNode.removeChild(c[l]);
            for (var d = 0; d < a.length; d++) {
                var u = a[d].getElementsByClassName("ans"),
                    f = (u.length, d);
                if (r.options.randomize && (f = Number(a[d].getAttribute("data-o"))), document[t.id] && document[t.id].preserve && document[t.id].q && document[t.id].q[f]) {
                    if (r.q[f].exp)
                        if (!a[d].getElementsByClassName("explanation").length) {
                            var p = document.createElement("p");
                            p.className = "explanation", p.innerHTML = kInteractive.escapeHtml(r.q[f].exp), kInteractive.appendAfterDelay(a[d], p)
                        }
                    if (r.q[f].ref)
                        if (!a[d].getElementsByClassName("reference").length) {
                            var h = document.createElement("a");
                            h.className = "reference", h.innerHTML = i.learnMode ? i.learnMode : "Learn more", h.setAttribute("href", kInteractive.escapeHtml(r.q[f].ref)), h.setAttribute("target", "_blank"), kInteractive.appendAfterDelay(a[d], h)
                        }
                }
                for (l = 0; l < u.length; l++)
                    if (kInteractive.c.removeClass(u[l].parentNode, "correct"), 0 <= u[l].getAttribute("id").indexOf("ki-tf-")) document[t.id] && document[t.id].preserve && document[t.id].q && document[t.id].q[f] && (document[t.id].q[f][l] ? u[l].setAttribute("checked", !0) : u[l].removeAttribute("checked"), r.options && r.options.highlightCorrect && (0 == l && r.q[f].a || 1 == l && !r.q[f].a) && kInteractive.c.addClass(u[l].parentNode, "correct"));
                    else if (0 <= u[l].getAttribute("id").indexOf("ki-dd-")) {
                        if (document[t.id] && document[t.id].preserve && document[t.id].q && document[t.id].q[f])
                            for (var g = 0; g < document[t.id].q[f].length; g++) {
                                var m = document[t.id].q[f][g];
                                try {
                                    for (var v = document.getElementById(m.id), b = v; !kInteractive.c.hasClass(b, "ques");) b = b.parentNode;
                                    b.getElementsByClassName("categoryContainer")[0].children[m.category].appendChild(v)
                                } catch (e) { }
                            }
                    } else 0 <= u[l].getAttribute("id").indexOf("ki-sa-") ? document[t.id] && document[t.id].preserve && document[t.id].q && document[t.id].q[f] && (u[l].removeAttribute("value"), document[t.id].q[f] && u[l].setAttribute("value", document[t.id].q[f])) : document[t.id] && document[t.id].preserve && document[t.id].q && document[t.id].q[f] && (document[t.id].q[f][l] ? u[l].setAttribute("checked", !0) : u[l].removeAttribute("checked"), r.options && r.options.highlightCorrect && r.q[f].c[l].a && kInteractive.c.addClass(u[l].parentNode, "correct"));
                if (document[t.id]) {
                    var y = t.getElementsByClassName("questions-preserve");
                    y.length && (document[t.id].preserve ? y[0].setAttribute("checked", !0) : y[0].removeAttribute("checked"))
                }
            }
        }
    },
    action: function (p, t, e) {
        var n = kInteractive.readData(p);
        if (n) {
            var r = n.dict,
                o = n.userDict;
            if (r = r || {}, o = o || r, n.layout && n.layout.popup && !kInteractive.hasClass(p, "inpopup") && "img" == t.nodeName.toLowerCase()) {
                function i(e) {
                    for (var t = 0; e = e.previousSibling;) "#text" != e.nodeName && 8 != e.nodeType && t++;
                    return t
                }
                var a = document.getElementById("epubContent");
                a && p.setAttribute("data-loc", function (e, t) {
                    for (var n = ""; e != t;) {
                        n = i(e) + "." + n, e = e.parentNode
                    }
                    return "" == n ? "" : n.substr(0, n.length - 1)
                }(p, a));
                var h = document.createElement("iframe");
                return h.setAttribute("nwdisable", "true"), h.setAttribute("nwfaketop", "true"), n.cb = function (e) {
                    var t, n = document.createElement("head");
                    if (void 0 === isKotobee) {
                        for (var r = document.getElementsByTagName("link"), o = 0; o < r.length; o++) {
                            var i = r[o];
                            if (0 <= i.getAttribute("href").indexOf("base.css")) {
                                var a = i.getAttribute("href").split("/");
                                delete a[a.length - 1], t = a.join("/");
                                break
                            }
                        }
                        if (!t) return
                    }
                    var s = void 0 === isKotobee ? t + "../xhtml/" : bookPath + "EPUB/xhtml/";
                    n = "<head>";
                    if (n += '<base href="' + kInteractive.c.removeFilename(kInteractive.c.removeHash(window.location.href)) + "/" + s + '"/>', n += '<link rel="stylesheet" type="text/css" href="../css/base.css" />', n += '<link rel="stylesheet" type="text/css" href="../css/global.css" />', n += '<link rel="stylesheet" type="text/css" href="../css/kotobeeInteractive.css" />', n += '<script type="text/javascript" src="../js/kotobeeInteractive.js"><\/script>', n += '<script type="text/javascript" src="../js/global.js"><\/script>', void 0 !== isKotobee) {
                        var l = clone(config);
                        l.kotobee.email = angular.element(document.body).scope().data.user.email, n += '<script type="text/javascript">var config=' + JSON.stringify(l) + ";<\/script>"
                    }
                    n += '<meta charset="utf-8" />', n += "</head>";
                    var c = p.outerHTML;
                    c = (c = (c = (c = (c = (c = c.replace("<img", '<img style="display:none"')).replace(/([;\s]*?height:)(.*)?;/i, "$1auto")).replace(/([;\s]*?width:)(.*)?;/i, "$1auto")).replace(/([;\s]*?top:)(.*)?;/i, "$1auto")).replace(/([;\s]*?left:)(.*)?;/i, "$1auto")).replace('class="', 'class="inpopup ');
                    var d = "<html>" + n + "<body>" + (c += '<div class="vSpace20"></div>') + "</body></html>";
                    h.setAttribute("srcdoc", d);
                    var u = "javascript: window.frameElement.getAttribute('srcdoc');";

                    function f() {
                        if (void 0 !== isKotobee) {
                            var e = {};
                            e.root = h.contentDocument.documentElement.ownerDocument.body, e.rootIndex = p.getAttribute("data-loc"), angular.element(document.body).scope().getNotebookShortcut(e, function () { })
                        }
                    }
                    h.setAttribute("src", u), h.contentWindow && (h.contentWindow.location = u), e.appendChild(h), h.addEventListener ? h.addEventListener("load", f, !0) : h.attachEvent && h.attachEvent("onload", f)
                }, n.closed = function () { }, n.pos = n.layout.pos, kInteractive.openFrame(n), kInteractive.tinCan({
                    verb: "opened",
                    activity: "Questions: " + n.title
                }), void (kInteractive.scorm && setTimeout(function () {
                    var e = {};
                    e.id = kInteractive.getScormId("question", n.title), e.description = "Opened question popup: " + n.title, e.learnerResponses = "Opened", e.type = "other", e.timestamp = new Date, kInteractive.scorm.setInteractions([e])
                }, 800))
            }
            var s = "Untitled questions",
                l = p.getElementsByTagName("h4");
            l.length && (s = l[0].textContent || l[0].data);
            var c = 0,
                d = p.getElementsByClassName("ques"),
                u = d.length,
                f = "";
            if (!kInteractive.hasClass(t, "kInteractive"))
                if (kInteractive.hasClass(t, "questions-clear")) {
                    for (var g = p.getElementsByClassName("ans"), m = [], v = 0; v < g.length; v++) g[v].checked = !1, g[v].value = null, kInteractive.c.removeClass(g[v].parentNode, "correct"), kInteractive.c.removeClass(g[v], "incorrect"), kInteractive.c.hasClass(g[v].parentNode, "dCategory") && m.push(g[v]);
                    for (v = 0; v < m.length; v++) m[v].parentNode.parentNode.parentNode.getElementsByClassName("ki-dd-answers")[0].appendChild(m[v]);
                    for (v = (O = p.getElementsByClassName("explanation")).length; v--;) O[v].parentNode.removeChild(O[v]);
                    for (v = (B = p.getElementsByClassName("reference")).length; v--;) B[v].parentNode.removeChild(B[v]);
                    document[p.id] = null
                } else {
                    if (kInteractive.hasClass(t, "questions-notebook")) {
                        var b, y = n.layout && n.layout.popup && kInteractive.hasClass(p, "inpopup");
                        if (void 0 === isKotobee && !y) return void kInteractive.alert({
                            content: r.kotobeeOnly,
                            title: o.sorry ? o.sorry : "Sorry"
                        });
                        var w = {
                            elem: p
                        };
                        if (void 0 !== isKotobee) b = angular.element(document.body).scope();
                        else {
                            try {
                                b = window.parent.getGlobal()
                            } catch (e) {
                                return void kInteractive.alert({
                                    content: r.kotobeeOnly,
                                    title: o.sorry ? o.sorry : "Sorry"
                                })
                            }
                            w.rootIndex = p.getAttribute("data-loc"), w.root = p
                        }
                        return kInteractive.c.addClass(t, "busy"), void b.questionsAddToNotebookShortcut(w, function () {
                            kInteractive.c.removeClass(t, "busy"), kInteractive.alert({
                                content: o.savedToNotebook ? o.savedToNotebook : "Saved to notebook!"
                            })
                        })
                    }
                    if (kInteractive.hasClass(t, "questions-preserve")) return document[p.id] || (document[p.id] = {}), void (document[p.id].preserve = t.checked);
                    var k = 0,
                        x = 0,
                        C = [];
                    for (v = 0; v < d.length; v++) {
                        var I = 0 < (g = d[v].getElementsByClassName("ans")).length,
                            E = n.q[v];
                        if (n.options.randomize && (E = n.q[Number(d[v].getAttribute("data-o"))]), kInteractive.scorm && n.options && n.options.answerOnce && kInteractive.scorm.interactionExists(kInteractive.getScormId(p.id + "-" + (v + 1), E.q))) {
                            var T = "<p style='text-align:center'>" + (o.alreadySubmitted ? o.alreadySubmitted : "This test has already been submitted!") + "</p>";
                            return void kInteractive.alert({
                                content: T
                            })
                        }
                        var S = {},
                            A = g[0].getAttribute("id");
                        C.push(S), S.id = kInteractive.getScormId(p.id + "-" + (v + 1), E.q), S.qid = p.id, 0 <= A.indexOf("ki-tf-") ? S.type = "true-false" : 0 <= A.indexOf("ki-mcq-") || A.indexOf("ki-mmcq-") ? S.type = "choice" : 0 <= A.indexOf("ki-dd-") ? S.type = "drag-drop" : 0 <= A.indexOf("ki-sa-") && (S.type = "short-answer"), S.objective = n.options ? n.options.objective : null, S.timestamp = kInteractive.timestamp, S.latency = Math.round(((new Date).getTime() - kInteractive.timestamp.getTime()) / 1e3), S.description = E.q;
                        for (var O, B, L = [], N = 0; N < g.length; N++)
                            if (0 <= g[N].getAttribute("id").indexOf("ki-tf-")) 0 == N && E.a ? S.correctResponses = E.choice1 : 1 != N || E.a || (S.correctResponses = E.choice2), g[N].checked ? (S.learnerResponses = N ? E.choice2 : E.choice1, 0 != N || E.a ? 1 == N && E.a && (I = !1) : I = !1) : 0 == N && E.a ? I = !1 : 1 != N || E.a || (I = !1), n.options && n.options.highlightCorrect && (0 == N && E.a || 1 == N && !E.a) && kInteractive.c.addClass(g[N].parentNode, "correct");
                            else if (0 <= g[N].getAttribute("id").indexOf("ki-sa-")) {
                                se = (se = g[N].value.toLowerCase()) && se.trim(), I = !0;
                                for (var M = "", P = 0; P < E.k.length; P++) {
                                    var R = E.k[P].t;
                                    if (!R) {
                                        I = !1;
                                        break
                                    }
                                    M && (M += "+"), M += R = R.toLowerCase();
                                    for (var D = R.split(","), F = !1, _ = 0; _ < D.length; _++)
                                        if (0 <= se.indexOf(D[_].trim())) {
                                            F = !0;
                                            break
                                        }
                                    if (!F) {
                                        I = !1;
                                        break
                                    }
                                }
                                S.correctResponses = M, S.learnerResponses = se
                            } else if (0 <= g[N].getAttribute("id").indexOf("ki-dd-")) {
                                kInteractive.c.removeClass(g[N], "incorrect");
                                var q = g[N].id.split("-")[4],
                                    H = E.answers[q].forCategory,
                                    z = d[v].getElementsByClassName("dCategory")[H],
                                    $ = {};
                                if ($.correct = g[N].innerText + " -> " + z.children[0].innerText, kInteractive.c.hasClass(g[N].parentNode, "ki-dd-answers")) kInteractive.c.addClass(g[N], "incorrect"), I = !1, $.learner = g[N].innerText + " unassigned";
                                else {
                                    var U = g[N].parentNode,
                                        j = U.children[0].innerText,
                                        V = kInteractive.getChildIndex(U);
                                    $.learner = g[N].innerText + " -> " + j, V != H && n.options && n.options.highlightCorrect && (I = !1, kInteractive.c.addClass(g[N], "incorrect"))
                                }
                                L.push($)
                            } else E.c[N].a && (S.correctResponses = E.c[N].t), g[N].checked && (S.learnerResponses = E.c[N].t), g[N].checked && !E.c[N].a ? I = !1 : !g[N].checked && E.c[N].a && (I = !1), n.options && n.options.highlightCorrect && E.c[N].a && kInteractive.c.addClass(g[N].parentNode, "correct");
                        if (L.length)
                            for (var W = 0; W < L.length; W++) S.correctResponses && (S.correctResponses += "; "), S.learnerResponses && (S.learnerResponses += "; "), S.correctResponses += L[W].correct, S.learnerResponses += L[W].learner;
                        if (S.result = 0, null != E.weight && (k += E.weight, I && (x += E.weight, S.result = E.weight)), S.scoreMax = n.options ? Number(n.options.totalScore) : null, E.exp)
                            if (!d[v].getElementsByClassName("explanation").length) (O = document.createElement("p")).className = "explanation", O.innerHTML = kInteractive.escapeHtml(E.exp), kInteractive.appendAfterDelay(d[v], O);
                        if (E.ref)
                            if (!d[v].getElementsByClassName("reference").length) (B = document.createElement("a")).className = "reference", B.innerHTML = o.learnMode ? o.learnMode : "Learn more", B.setAttribute("href", kInteractive.escapeHtml(E.ref)), B.setAttribute("target", "_blank"), kInteractive.appendAfterDelay(d[v], B);
                        var G = o.question ? o.question.replace("[no]", v + 1) : "Question " + (v + 1);
                        I ? (f += "<span style='border-bottom: dotted 1px #aaa;' title='" + E.q.replace(/'/g, "&#39;") + "'>" + G + ".</span>  <span style='color:#366c20'>" + o.correct + "</span><br/>", c++) : f += "<span style='border-bottom: dotted 1px #aaa;' title='" + E.q.replace(/'/g, "&#39;") + "'>" + G + ".</span>  <span style='color:#7a1818'>" + o.incorrect + "</span><br/>"
                    }
                    for (v = 0; v < C.length; v++) C[v].weighting = Math.round(1e3 * n.options.totalScore / k) / 1e3;
                    var K, X = n.action;
                    if (n.options.totalScore && k && (K = Math.round(10 * n.options.totalScore * x / k) / 10), "selfAnswer" == X || "selfAnswerReport" == X) {
                        var J = "";
                        "ar" == n.options.language && (J = ' direction="rtl"');
                        T = "";
                        if (o.scoreWeight) {
                            if (null != K) {
                                var Y = "";
                                n.options.passScore && (K >= n.options.passScore || K == n.options.totalScore ? Y += o.pass : Y += o.fail), Y && (Y += ". "), T += "<h3" + J + ">" + (Y || "") + o.scoreWeight.replace("[score]", K).replace("[total]", n.options.totalScore) + "</h3>"
                            }
                            T += "<p" + J + ">" + o.score.replace("[correct]", c).replace("[total]", u) + "</p>"
                        } else T = "You scored " + c + " question(s) out of " + u;
                        "selfAnswerReport" == X && (T = "<p" + J + "><strong>" + T + "</strong></p><p><strong>" + (o.details ? o.details : "Details") + "</strong></p><p class='kbAlertScroll'>" + f + "</p>"), kInteractive.alert({
                            content: T
                        }), ue()
                    }
                    if ("email" == X || n.options.reportEmail || n.options.emailForm) {
                        var Z = "selfAnswer" != X && "selfAnswerReport" != X && "email" != X;
                        try {
                            if (!config.kotobee.cloudid) return void (Z && kInteractive.alert({
                                content: r.cloudOnly,
                                title: o.sorry ? o.sorry : "Sorry"
                            }))
                        } catch (e) {
                            return void (Z && kInteractive.alert({
                                content: r.cloudOnly,
                                title: o.sorry ? o.sorry : "Sorry"
                            }))
                        }
                        var Q = {};
                        Q.ans = new Array;
                        for (N = 0; N < d.length; N++)
                            for (g = d[N].getElementsByClassName("ans"), v = 0; v < g.length; v++) g[v].checked && Q.ans.push({
                                q: N,
                                index: v,
                                label: g[v].nextSibling.textContent || g[v].nextSibling.data
                            });
                        Q.title = s;
                        var ee = {};
                        if (ee.recipient = n.address, n.options.reportEmail && (ee.recipient = n.options.reportEmail), ee.content = JSON.stringify(Q), ee.mode = config.kotobee.mode, ee.cloudid = config.kotobee.cloudid, ee.email = config.kotobee.email ? config.kotobee.email : angular.element(document.body).scope().data.user.email, n.options.emailForm) {
                            var te = document.getElementsByName("recipientEmail");
                            te.length && (ee.recipient = ee.recipient ? ee.recipient + "," + te[0].value : te[0].value)
                        }
                        var ne = t.value;
                        t.value = o.submitting ? o.submitting : "Submitting ..", t.setAttribute("disabled", "true");
                        var re = new XMLHttpRequest;
                        re.onreadystatechange = function () {
                            try {
                                if (4 === re.readyState && 200 === re.status) {
                                    if (t.value = ne, t.removeAttribute("disabled"), !Z) return;
                                    JSON.parse(re.responseText).success ? kInteractive.alert({
                                        content: o.submitted ? o.submitted : "Answers submitted!"
                                    }) : kInteractive.alert({
                                        content: o.errorSubmitting ? o.errorSubmitting : "An error has occurred while sending answers to the server"
                                    })
                                }
                            } catch (e) {
                                if (t.value = ne, t.setAttribute("value", t.value), t.removeAttribute("disabled"), !Z) return;
                                kInteractive.alert({
                                    content: o.errorSubmitting ? o.errorSubmitting : "An error has occurred while sending answers to the server"
                                })
                            }
                        };
                        var oe = config.kotobee.liburl ? config.kotobee.liburl : "http://www.kotobee.com/";
                        oe += "library/report/questions", re.open("POST", oe), re.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        var ie = "a=a";
                        for (var ae in ee) {
                            var se;
                            null != (se = ee[ae]) && "object" != typeof se && ("string" == typeof se && (se = encodeURIComponent(se)), "boolean" == typeof se && (se = se ? 1 : 0), ie += "&" + ae + "=" + se)
                        }
                        re.send(ie), ue()
                    }
                    document[p.id] || (document[p.id] = {}), document[p.id].q = [];
                    var le = p.getElementsByClassName("ques");
                    for (N = 0; N < le.length; N++) {
                        var ce = N;
                        n.options.randomize && (ce = Number(le[N].getAttribute("data-o"))), document[p.id].q[ce] = [];
                        var de = le[N].getElementsByClassName("ans");
                        for (v = 0; v < de.length; v++) kInteractive.hasClass(de[v], "txtfield") ? document[p.id].q[ce][v] = de[v].value : de[v].type && "checkbox" == de[v].type.toLowerCase() ? document[p.id].q[ce][v] = de[v].checked : de[v].type && "radio" == de[v].type.toLowerCase() ? document[p.id].q[ce][v] = de[v].checked : kInteractive.c.hasClass(de[v].parentNode, "dCategory") && (document[p.id].q[ce][v] = {
                            id: de[v].id,
                            category: kInteractive.getChildIndex(de[v].parentNode)
                        })
                    }
                    kInteractive.scorm && setTimeout(function () {
                        kInteractive.scorm.setInteractions(C)
                    }, 500)
                }
        }

        function ue() {
            var e = "Questions: " + s + ". Score: " + c + " questions out of " + u;
            e += ". Action: " + X, "selfAnswerReport" == X && (e += ". Details: " + f), kInteractive.tinCan({
                verb: "solved",
                activity: e
            })
        }
    }
};

kInteractive.threed = {
    preRender: function (e, t) { },
    postRender: function (e, t, n, r) {
        var o = e.createDocumentFragment(),
            i = kInteractive.readData(t);
        if (i) {
            t.innerHTML = "";
            var a = t.getAttribute("style");
            if (i.inter.placeholderPath) a += "background-image:url('" + (void 0 === isKotobee ? i.inter.placeholderPath : ph.join(kInteractive.absoluteURL, i.inter.placeholderPath)) + "');";
            t.setAttribute("style", a);
            var s = document.createElement("a");
            i.inter.includeMsg ? (s.innerHTML = kInteractive.escapeHtml(i.inter.msg), s.className = "msgBtn ki-btn") : s.className = "invisibleBtn ki-btn", o.appendChild(s), void 0 === isKotobee && s && s.addEventListener("click", kInteractive.actionEvent), t.appendChild(o)
        }
    },
    action: function (r, e, t) {
        var m = kInteractive.readData(r);
        if (m) {
            "inPanel" == m.inter.target ? kInteractive.c.addClass(r, "running") : r.innerHTML = "";
            var v = document.createElement("div");
            if (v.className = "loading", r.appendChild(v), "undefined" == typeof THREE) {
                var n, o = document.getElementsByTagName("head")[0],
                    i = o.getElementsByTagName("script");
                if (void 0 !== isKotobee) n = bookPath + "EPUB/js/kotobeeInteractive3D.js";
                else
                    for (var a = 0; a < i.length; a++)
                        if (0 <= i[a].src.indexOf("kotobeeinteractive.js")) {
                            n = i[a].src.replace("kotobeeinteractive.js", "kotobeeinteractive3D.js");
                            break
                        } if (!n) return;
                var s = document.createElement("script");
                s.type = "text/javascript", s.src = n, s.onload = l, s.onreadystatechange = function () {
                    "complete" == this.readyState && l()
                }, o.appendChild(s)
            } else l()
        }

        function l() {
            var g, e = r;
            if ("inPanel" == m.inter.target) {
                var t = {
                    class: "threed",
                    cb: function (e) {
                        var t = document.createElement("div");
                        t.className = "container", e.appendChild(t), n(t)
                    },
                    closed: function () {
                        kInteractive.c.removeClass(r, "running")
                    }
                };
                kInteractive.openFrame(t)
            } else n(e);

            function n(t) {
                var n;
                kInteractive.scorm && setTimeout(function () {
                    var e = {};
                    e.id = kInteractive.getScormId("_3D", m.name ? m.name : "Model"), e.description = "Opened 3D model: " + (m.name ? m.name : "No name"), e.learnerResponses = "Viewed", e.type = "other", e.timestamp = new Date, kInteractive.scorm.setInteractions([e])
                }, 800), g && cancelAnimationFrame(g), n = function () {
                    try {
                        var e = document.createElement("canvas");
                        return !(!window.WebGLRenderingContext || !e.getContext("webgl") && !e.getContext("experimental-webgl"))
                    } catch (e) {
                        return !1
                    }
                }() ? new THREE.WebGLRenderer({
                    precision: "highp"
                }) : new THREE.CanvasRenderer;
                var e = t.offsetWidth,
                    r = t.offsetHeight,
                    o = new THREE.PerspectiveCamera(m.camera.viewAngle, e / r, m.camera.near, m.camera.far),
                    l = new THREE.Scene;
                null != m.scene.bgColor && n.setClearColor(m.scene.bgColor, 1), l.add(o), o.position.x = m.camera.x, o.position.y = m.camera.y, o.position.z = m.camera.z, n.setSize(e, r), m.scene.floor && (floor = new THREE.Mesh(new THREE.PlaneGeometry(500, 500, 1, 1), new THREE.MeshPhongMaterial({
                    color: m.scene.floorColor ? m.scene.floorColor : 10066329
                })), floor.applyMatrix((new THREE.Matrix4).makeRotationX(-Math.PI / 2)), floor.position.y = -80, floor.receiveShadow = !0, l.add(floor));
                var c = new THREE.LoadingManager;
                c.onProgress = function (e, t, n) { },
                    function i(a) {
                        if (!(a >= m.objects.length)) {
                            var s = m.objects[a],
                                e = void 0 === isKotobee ? s.path : ph.join(kInteractive.absoluteURL, s.path);
                            new THREE.OBJLoader(c).load(e, function (o) {
                                v && v.parentNode && v.parentNode.removeChild(v), n.domElement.parentNode || t.appendChild(n.domElement), o.position.set(s.x, s.y, s.z), o.traverse(function (n) {
                                    if (n instanceof THREE.Mesh) {
                                        var r = n == o.children[o.children.length - 1];
                                        if (s.textPath) {
                                            var e = new THREE.ImageLoader(c),
                                                t = void 0 === isKotobee ? s.textPath : ph.join(kInteractive.absoluteURL, s.textPath);
                                            e.load(t, function (e) {
                                                var t = new THREE.Texture;
                                                t.image = e, t.needsUpdate = !0, n.material.map = t, r && l.add(o), i(++a)
                                            }, function () { }, function (e) { })
                                        } else r && l.add(o), i(++a)
                                    }
                                })
                            }, function (e) {
                                e.lengthComputable && (e.loaded, e.total)
                            }, function (e) { })
                        }
                    }(0), new THREE.OrbitControls(o, n.domElement).target = new THREE.Vector3(0, 0, 0);
                for (var i = 0; i < m.lights.length; i++)
                    if ("pointLight" == m.lights[i].type) {
                        var a = new THREE.PointLight(m.lights[i].color);
                        a.position.set(m.lights[i].x, m.lights[i].y, m.lights[i].z), l.add(a)
                    } else if ("directionalLight" == m.lights[i].type) {
                        var s = new THREE.DirectionalLight(m.lights[i].color, m.lights[i].intensity);
                        s.position.set(m.lights[i].x, m.lights[i].y, m.lights[i].z), l.add(s)
                    } else if ("hemisphereLight" == m.lights[i].type) {
                        var d = new THREE.HemisphereLight(m.lights[i].sky, m.lights[i].ground, m.lights[i].intensity);
                        d.position.set(m.lights[i].x, m.lights[i].y, m.lights[i].z), l.add(d)
                    } else if ("spotLight" == m.lights[i].type) {
                        var u = new THREE.SpotLight(m.lights[i].color, m.lights[i].intensity, m.lights[i].distance, m.lights[i].angle, m.lights[i].exponent, m.lights[i].decay);
                        u.position.set(m.lights[i].x, m.lights[i].y, m.lights[i].z), l.add(u)
                    } else if ("ambientLight" == m.lights[i].type) {
                        var f = new THREE.AmbientLight(m.lights[i].color);
                        l.add(f)
                    } else if ("areaLight" == m.lights[i].type) {
                        var p = new THREE.AreaLight(m.lights[i].color, m.lights[i].intensity);
                        l.add(p)
                    }
                var h = new THREE.Clock;
                ! function e() {
                    try {
                        n.render(l, o)
                    } catch (e) {
                        return
                    }
                    h.getDelta();
                    g = requestAnimFrame(e)
                }()
            }
        }
    }
};

kInteractive.video = {
    preRender: function (e, t) { },
    postRender: function (e, t, n) {
        var r = e.createDocumentFragment(),
            o = kInteractive.readData(t);
        if (o) {
            var i = "";
            if (o.splash && (i = void 0 === isKotobee ? o.splash : ph.join(kInteractive.absoluteURL, o.splash)), i) {
                t.style.backgroundImage = null;
                var a = t.style.cssText;
                t.setAttribute("style", "background-image:url('" + i + "');" + a)
            }
            t.setAttribute("id", "ki-video-" + n), t.innerHTML = "";
            var s = document.createElement("div");
            s.setAttribute("id", "ki-video-" + n + "-container"), s.className = "container";
            var l = document.createElement("div");
            o.style && kInteractive.c.addClass(l, o.style);
            var c = document.createElement("a");
            c.className = "playBtn ki-btn", c.appendChild(document.createElement("span")), void 0 === isKotobee && c.addEventListener("click", kInteractive.actionEvent), r.appendChild(c), r.appendChild(s), l.appendChild(r), t.appendChild(l), o.autoplay && kInteractive.action(t)
        }
    },
    action: function (i) {
        var a = kInteractive.readData(i);
        if (a) {
            var e = i.getAttribute("id") + "-container",
                s = i.getElementsByClassName("playBtn")[0];
            if (!(0 <= s.className.indexOf("loading"))) {
                if (kInteractive.stopCurrentMedia(), s.className = "playBtn ki-btn loading", a.autoplay && (s.className = "playBtn ki-btn hide"), kInteractive.scorm) {
                    var t = {};
                    t.id = kInteractive.getScormId(i.getAttribute("id"), a.src), t.description = "Played video: " + a.src, t.type = "other", t.learnerResponses = "Played", t.objective = a.options ? a.options.objective : null, t.timestamp = new Date, kInteractive.scorm.setInteractions([t])
                }
                if ("file" == a.type) {
                    d(void 0 === isKotobee ? a.video : ph.join(kInteractive.absoluteURL, a.video))
                } else {
                    var n, r = new URL(a.src);
                    if (r.origin.includes("youtube.com") || r.origin.includes("youtu.be")) {
                        if (!(n = r.origin.includes("youtube.com") ? r.searchParams.get("v") : r.pathname.split("/")[1])) return;
                        if (window && window.iBooks) return setTimeout(function () {
                            s.className = "playBtn ki-btn"
                        }, 1500), void (window.location.href = a.src);
                        var o = {
                            videoId: n,
                            playerVars: {
                                autoplay: 1,
                                rel: "0"
                            },
                            events: {
                                onReady: function () { },
                                onStateChange: function (e) {
                                    e.data == YT.PlayerState.PLAYING && kInteractive.tinCan({
                                        verb: "played",
                                        activity: "Video: " + a.src
                                    })
                                }
                            }
                        };
                        if (kInteractive.youTubeStatus)
                            if ("loading" == kInteractive.youTubeStatus) {
                                if (kInteractive.vQueue) return;
                                kInteractive.vQueue = function () {
                                    s.className = "playBtn ki-btn hide", new YT.Player(e, o)
                                }
                            } else "ready" == kInteractive.youTubeStatus && (s.className = "playBtn ki-btn hide", new YT.Player(e, o));
                        else {
                            window.onYouTubePlayerAPIReady = function () {
                                kInteractive.youTubeStatus = "ready", kInteractive.vQueue(), kInteractive.vQueue = null
                            };
                            var l = document.createElement("script");
                            l.src = "https://www.youtube.com/player_api";
                            var c = document.getElementsByTagName("script")[0];
                            c.parentNode.insertBefore(l, c), kInteractive.youTubeStatus = "loading", kInteractive.vQueue = function () {
                                s.className = "playBtn ki-btn hide", new YT.Player(e, o)
                            }
                        }
                    } else d(a.src)
                }
                kInteractive.currentVideo = i, kInteractive.c.addClass(kInteractive.currentVideo, "playing")
            }
        }

        function d(e) {
            var t, n = document.createElement("video");
            n.setAttribute("width", "100%"), n.setAttribute("height", "100%"), n.setAttribute("src", e), n.setAttribute("autoplay", "true"), n.setAttribute("data-tap-disabled", "false"), 0 != e.indexOf("http://") && 0 != e.indexOf("https://") || (t = !0), t && n.setAttribute("crossorigin", "anonymous");
            var r = !1;
            try {
                r = -1 < navigator.userAgent.toLowerCase().indexOf("android")
            } catch (e) { }
            r || n.setAttribute("type", "video/mp4"), n.setAttribute("webkit-playsinline", "true"), n.setAttribute("controls", "true"), n.className = "ki-noHighlight", a.hideDownloadBtn && (n.className += " hideDownloadBtn"), n.innerHTML = "Your browser doesn't support HTML5 video.", t && (n.crossOrigin = "anonymous"), n.oncanplay = function () {
                kInteractive.currentVideo == i && (s.className = "playBtn ki-btn hide", kInteractive.tinCan({
                    verb: "played",
                    activity: "Video: " + e
                }))
            }, n.addEventListener("error", function (e) {
                kInteractive.stopCurrentMedia()
            }), n.addEventListener("webkitfullscreenchange", function (e) {
                var t = null !== document.webkitFullscreenElement;
                kInteractive.videoIsFullscreen = t
            });
            var o = i.getElementsByClassName("container")[0];
            o.setAttribute("data-tap-disabled", "true"), o.appendChild(n)
        }
    },
    resize: function (e) {
        var t = kInteractive.readData(e);
        t && (kInteractive.checkResponsiveFloat(t, e), t.maintainRatio && "px" == t.widthUnit && t.height && t.width && (e.style.height = e.offsetWidth * (t.height / t.width) + "px"))
    }
};

kInteractive.widget = {
    preRender: function (e, t) { },
    postRender: function (e, t, n, r) {
        r = r || {};
        var o = e.createDocumentFragment(),
            i = kInteractive.readData(t);
        if (i) {
            var a = i.name,
                s = (i.src, i.width),
                l = i.height;
            if ("page" == i.mode) {
                if (t.children.length) return;
                var c = document.createElement("div");
                c.className = "cover", o.appendChild(c), i.interaction && (c.style.pointerEvents = "none");
                var d = document.createElement("div");
                d.className = "iframeContainer";
                var u = document.createElement("iframe");
                u.setAttribute("nwdisable", "true"), u.setAttribute("nwfaketop", "true"), u.setAttribute("sandbox", "allow-same-origin allow-scripts allow-forms allow-modals"), u.setAttribute("width", t.style.width ? t.style.width : s + i.widthUnit), u.setAttribute("height", t.style.height ? t.style.height : l + "px"), u.src = kInteractive.getWidgetUrl(i, r), d.appendChild(u), o.appendChild(d);
                var f = t.style.width,
                    p = t.style.height;
                0 <= f.indexOf("px") && (i.widthUnit = "px"), 0 <= f.indexOf("%") && (i.widthUnit = "%");
                try {
                    i.widthUnit ? i.width = Number(f.split(i.widthUnit)[0]) : i.width = Number(f)
                } catch (e) {
                    i.width = Number(f)
                }
                try {
                    i.height = Number(p.split("px")[0])
                } catch (e) {
                    i.height = Number(p)
                }
                kInteractive.writeData(t, i)
            } else {
                if (!t.children.length && !t.innerHTML.trim()) {
                    var h = document.createElement("img");
                    h.src = kInteractive.getWidgetHome(i, r) + "/" + a + "/Icon.png", h.className = "wdgtIcon", o.appendChild(h)
                }
                void 0 === isKotobee && t.addEventListener("click", kInteractive.actionEvent)
            }
            t.appendChild(o)
        }
    },
    action: function (e) {
        var t = kInteractive.readData(e);
        if (t) {
            var n = kInteractive.getWidgetUrl(t),
                r = document.createElement("iframe");
            r.setAttribute("nwdisable", "true"), r.setAttribute("nwfaketop", "true"), r.setAttribute("sandbox", "allow-same-origin allow-scripts allow-forms allow-modals"), t.cb = function (e) {
                r.src = n, e.appendChild(r)
            }, t.cb1 = "yes", t.closed = function () { }, t.responsive && (t.width = t.height = null), kInteractive.openFrame(t), kInteractive.tinCan({
                verb: "opened",
                activity: "Widget: " + t.name
            }), kInteractive.scorm && setTimeout(function () {
                var e = {};
                e.id = kInteractive.getScormId("widget", t.name), e.description = "Opened popup widget: " + t.name, e.learnerResponses = "Opened", e.type = "other", e.timestamp = new Date, kInteractive.scorm.setInteractions([e])
            }, 800)
        }
    },
    resize: function (e) {
        var t = kInteractive.readData(e);
        if (t && "page" == t.mode && "px" == t.widthUnit) {
            var n = t.width,
                r = t.height,
                o = e.parentNode;
            if (o)
                if (n > o.offsetWidth) {
                    var i = o.offsetWidth / n,
                        a = e.children.length - 1;
                    e.children[a].style.transform = e.children[a].style.webkitTransform = e.children[a].style.mozTransform = "scale(" + i + ")", e.children[a].style.transformOrigin = e.children[a].style.webkitTransformOrigin = e.children[a].style.mozTransformOrigin = "0 0", e.style.maxWidth = e.style.width, e.style.height = Math.round(r * i) + "px"
                } else {
                    for (a = 0; a < e.children.length; a++) e.children[a].style.transform = e.children[a].style.webkitTransform = e.children[a].style.mozTransform = e.children[a].style.transformOrigin = e.children[a].style.webkitTransformOrigin = e.children[a].style.mozTransformOrigin = null;
                    e.style.maxWidth = null, e.style.height = r + "px"
                }
        }
    }
};