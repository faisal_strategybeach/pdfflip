window.onload = function (e) {
    CheckIfUserLoggedInforEBook();
};

function CheckIfUserLoggedInforEBook() {
    var request = new XMLHttpRequest()

    var apiUrl = window.location.origin + '/User/CheckIfUserLoggedInforEBook';

    request.open('GET', apiUrl, true);
    request.onload = function () {
        // Begin accessing JSON data here
        var data = JSON.parse(this.response)

        if (request.status >= 200 && request.status < 400) {
            if (!data.isUserLoggedIn) {
                window.location.href = window.location.origin;
            } 
        } else {
            console.log('error')
        }
    }

    request.send()
}