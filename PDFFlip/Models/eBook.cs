﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PDFFlip.Models
{
    public class eBook
    {
        public int BookID { get; set; }
        public string eBookTitle { get; set; }
        public string ThumbnailPath { get; set; }
        public string CategoryName { get; set; }
        public string PdfPath { get; set; }
    }
}