﻿using PDFFlip.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PDFFlip.Controllers
{
    public class HighlightController : ApiController
    {
        [HttpGet]
        [Route("~/api/Highlight/GetHighlights/{UserID}/{BookID}")]
        public IEnumerable<Highlight> GetHighlights(string UserID,string BookID)
        {
            using (var context = new SWUNMATHConnection())
            {
                return context.Highlights.Where(X => X.UserName == UserID && X.BookID == BookID).ToList();
            }
        }

        [HttpPost]
        [Route("~/api/Highlight/SaveHighlight")]
        public int SaveHighlight([FromBody]Highlight Hlight)
        {
            using (var context = new SWUNMATHConnection())
            {
                context.Highlights.Add(Hlight);
                context.SaveChanges();
                return context.Highlights.Max(X => X.HighlightID);
            }
        }

        [HttpDelete]
        [Route("~/api/Highlight/DeleteHighlight/{HId}")]
        public bool DeleteHighlight(int HId)
        {
            using (var context = new SWUNMATHConnection())
            {
                Highlight HLight = context.Highlights.Where(X => X.HighlightID == HId).SingleOrDefault();

                if(HLight!=null)
                {
                    context.Highlights.Remove(HLight);
                    context.SaveChanges();
                }
            }
            return true;
        }
    }
}