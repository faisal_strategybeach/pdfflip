﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PDFFlip.Controllers
{
    public class SSOLoginController : Controller
    {

        public async Task<string> GoogleClassroom()
        {
            string str = "";
            string responsestr = "";

            foreach (string str_loopVariable in Request.QueryString)
            {
                str = str_loopVariable;
                responsestr = responsestr + Environment.NewLine + str + " - " + (Convert.ToString(Request.QueryString[str]));
            }

            foreach (string s in Request.Params.Keys)
            {
                responsestr = responsestr + s.ToString() + ":" + Convert.ToString(Request.Params[s]) + Environment.NewLine;
            }

            string code = Convert.ToString(Request.QueryString["code"]);
            string codeerror = Convert.ToString(Request.QueryString["error"]);
            //string returnURL = "http://swunmath.pegasusone.online/SSOLogin/GoogleClassroom";


            string GoogleClientID = Convert.ToString(ConfigurationManager.AppSettings["GoogleClientID"]);
            string GoogleClientSecret = Convert.ToString(ConfigurationManager.AppSettings["GoogleClientSecret"]);
            string GoogleReturnURL = Convert.ToString(ConfigurationManager.AppSettings["GoogleReturnURL"]);

            


            RequestToken requestToken = new RequestToken();
            requestToken.code = code;
            requestToken.client_id = GoogleClientID;
            requestToken.client_secret = GoogleClientSecret;
            requestToken.grant_type = "authorization_code";
            requestToken.redirect_uri = GoogleReturnURL;

            var json = JsonConvert.SerializeObject(requestToken, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });

            //string json = "{\"code\":\""+ code + "\",\"grant_type\":\"authorization_code\",\"redirect_uri\":\""+ returnURL + "\"}";
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var url = "https://oauth2.googleapis.com/token";
            var client = new HttpClient();

         

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.PostAsync(url, data);

            string result = await response.Content.ReadAsStringAsync();


            Tokenresponse _dataResponse = JsonConvert.DeserializeObject<Tokenresponse>(result);
            string _accesstoken = Convert.ToString(_dataResponse.access_token);





            var clientuser = new HttpClient();
            clientuser.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", _accesstoken);

            var resultgetuser = await clientuser.GetAsync("https://www.googleapis.com/oauth2/v2/userinfo");


            string userdetails = await resultgetuser.Content.ReadAsStringAsync();


            GoogleUserObject GoogleUser = JsonConvert.DeserializeObject<GoogleUserObject>(userdetails);

            string name = GoogleUser.name;
            string email = GoogleUser.email;

            string welcomemessage = "Welcome " + name + " (" + email + ") from Google";


            



            return userdetails;
        }

        public async Task<string> ClassLink()
        {
            string str = "";
            string responsestr = "";

            foreach (string str_loopVariable in Request.QueryString)
            {
                str = str_loopVariable;
                responsestr = responsestr + Environment.NewLine + str + " - " + (Convert.ToString(Request.QueryString[str]));
            }

            foreach (string s in Request.Params.Keys)
            {
                responsestr = responsestr + s.ToString() + ":" + Convert.ToString(Request.Params[s]) + Environment.NewLine;
            }


            string code = Convert.ToString(Request.QueryString["code"]);
            string codeerror = Convert.ToString(Request.QueryString["error"]);
            //string returnURL = "http://swunmath.pegasusone.online/SSOLogin/ClassLink";


            //string clientid = "c1587133857461d2a0922bb140cb791481f0856749d231";
            //string clientsecret = "8b101a3e58eae7ac72dcad4e39631efa";


            string ClassLinkClientID = Convert.ToString(ConfigurationManager.AppSettings["ClassLinkClientID"]);
            string ClassLinkClientSecret = Convert.ToString(ConfigurationManager.AppSettings["ClassLinkClientSecret"]);
            string ClassLinkReturnURL = Convert.ToString(ConfigurationManager.AppSettings["ClassLinkReturnURL"]);


            RequestToken requestToken = new RequestToken();
            requestToken.code = code;
            requestToken.client_id = ClassLinkClientID;
            requestToken.client_secret = ClassLinkClientSecret;

            var json = JsonConvert.SerializeObject(requestToken, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });

            //string json = "{\"code\":\""+ code + "\",\"grant_type\":\"authorization_code\",\"redirect_uri\":\""+ returnURL + "\"}";
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var url = "https://launchpad.classlink.com/oauth2/v2/token";
            var client = new HttpClient();

            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Authorization",
            //            Convert.ToBase64String(Encoding.Default.GetBytes(clientid+":"+ clientsecret)));

            //string authstring = clientid + ":" + clientsecret;

            //var authToken = Encoding.ASCII.GetBytes(authstring);
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
            //        Convert.ToBase64String(authToken));

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.PostAsync(url, data);

            //string result = response.RequestMessage.();
            string result = await response.Content.ReadAsStringAsync();




            Tokenresponse _dataResponse = JsonConvert.DeserializeObject<Tokenresponse>(result);
            string _accesstoken = Convert.ToString(_dataResponse.access_token);


            var clientuser = new HttpClient();
            clientuser.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", _accesstoken);

            var resultgetuser = await clientuser.GetAsync("https://nodeapi.classlink.com/v2/my/info");


            string userdetails = await resultgetuser.Content.ReadAsStringAsync();

            ClassLinkUserObject classLinkUser = JsonConvert.DeserializeObject<ClassLinkUserObject>(userdetails);

            string fname = classLinkUser.FirstName;
            string lname = classLinkUser.LastName;
            string email = classLinkUser.Email;

            string welcomemessage = "Welcome " + fname + " " + lname + " (" + email + ") from classLink";


            //return userdetails + welcomemessage;
            return welcomemessage;
        }
            // GET: SSOLogin
            public async Task<string> CleverInstantReturn()
        {
            //var returnstr = "CleverInstantReturn Response";

            string str = "";
            string responsestr = "";

            foreach (string str_loopVariable in Request.QueryString)
            {
                str = str_loopVariable;
                responsestr = responsestr +Environment.NewLine + str + " - " + (Convert.ToString(Request.QueryString[str]));
            }

            foreach (string s in Request.Params.Keys)
            {
                responsestr = responsestr + s.ToString() + ":" + Convert.ToString(Request.Params[s]) + Environment.NewLine;
            }

            
            //string code = "d8695c3391cfe3c9eef7";// Convert.ToString(Request.QueryString["code"]);
            string code = Convert.ToString(Request.QueryString["code"]);
            //string returnURL = "http://swunmath.pegasusone.online/SSOLogin/CleverInstantReturn";
            //code = "d9bff49d71c751642fdf";
            //return code;
            //string clientid = "6c9b3640809a27970103";
            //string clientsecret = "0d58b334f29fa45a096b39e599df99376d356df6";

            string CleverClientID = Convert.ToString(ConfigurationManager.AppSettings["CleverClientID"]);
            string CleverClientSecret = Convert.ToString(ConfigurationManager.AppSettings["CleverClientSecret"]);
            string CleverReturnURL = Convert.ToString(ConfigurationManager.AppSettings["CleverReturnURL"]);


            RequestToken requestToken = new RequestToken();
            requestToken.code = code;
            requestToken.grant_type = "authorization_code";
            requestToken.redirect_uri = CleverReturnURL;

            var json = JsonConvert.SerializeObject(requestToken);

            //string json = "{\"code\":\""+ code + "\",\"grant_type\":\"authorization_code\",\"redirect_uri\":\""+ returnURL + "\"}";
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var url = "https://clever.com/oauth/tokens";
            var client = new HttpClient();

            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Authorization",
            //            Convert.ToBase64String(Encoding.Default.GetBytes(clientid+":"+ clientsecret)));

            string authstring = CleverClientID + ":" + CleverClientSecret;

            var authToken = Encoding.ASCII.GetBytes(authstring);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                    Convert.ToBase64String(authToken));

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response =await client.PostAsync(url, data);

            //string result = response.RequestMessage.();
            string result = await response.Content.ReadAsStringAsync();
            

            Tokenresponse _dataResponse = JsonConvert.DeserializeObject <Tokenresponse> (result);
            string _accesstoken = Convert.ToString(_dataResponse.access_token);




            var clientgetuser = new HttpClient();
            clientgetuser.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", _accesstoken);

            var resultgetuser = await clientgetuser.GetAsync("https://api.clever.com/v2.1/me");


            string userdetails = await resultgetuser.Content.ReadAsStringAsync();
            //Console.WriteLine(result);


            UserResponse _userResponse = JsonConvert.DeserializeObject<UserResponse>(userdetails);


            string userdetail = "";

            string fname = "";
            string lname = "";
            string email = "";
            string type = "";
            string GetUserDetailsApiUrl = "";
            if (_userResponse.type.ToUpper() == "STUDENT")
            {
                
                GetUserDetailsApiUrl = "https://api.clever.com/v2.1/students/"+ _userResponse.data.id;
               
            }

            if (_userResponse.type.ToUpper() == "TEACHER")
            {
                GetUserDetailsApiUrl = "https://api.clever.com/v2.1/teachers/" + _userResponse.data.id;
             }
                var clientUserdetails = new HttpClient();
            clientUserdetails.DefaultRequestHeaders.Authorization
                             = new AuthenticationHeaderValue("Bearer", _accesstoken);
                var resultgetuserdetails = await clientUserdetails.GetAsync(GetUserDetailsApiUrl);


                string userdetailresponse = await resultgetuserdetails.Content.ReadAsStringAsync();


                UserResponse _userdetailResponse = JsonConvert.DeserializeObject<UserResponse>(userdetailresponse);

                userdetail = userdetailresponse;

                fname = _userdetailResponse.data.name.first;
                lname = _userdetailResponse.data.name.last;
                email = _userdetailResponse.data.email;
                type = _userResponse.type;

            string welcomemessage = "Welcome "+ type + ": " + fname + " " + lname + " (" + email + ") from Clever";
            return welcomemessage;
            //return (userdetail + userdetails + Environment.NewLine + result + Environment.NewLine + responsestr + Environment.NewLine + _accesstoken);
        }

        public ActionResult CleverReceiveToken()
        {
            return null;
        }


    }

    public class Tokenresponse
    {
        public string error { get; set; }
        public string error_description { get; set; }
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string response_type { get; set; }

        public string expires_in { get; set; }
        public string refresh_token { get; set; }
        public string scope { get; set; }
    }

    class RequestToken
    {
        public string code { get; set; }
        public string grant_type { get; set; }
        public string redirect_uri { get; set; }


        public string client_secret  { get; set; }
        public string client_id { get; set; }
    }






    public class UserData
    {
        public string id { get; set; }
        public string district { get; set; }
        public string type { get; set; }
        public string authorized_by { get; set; }
        
        public string email { get; set; }
        public CleverName name { get; set; }

    }


    public class CleverName
    {
        public string first { get; set; }
        public string last { get; set; }
        public string middle { get; set; }
    }

    public class UserLink
    {
        public string rel { get; set; }
        public string uri { get; set; }
    }

    public class UserResponse
    {
        public string type { get; set; }
        public UserData data { get; set; }
        public List<UserLink> links { get; set; }
    }



    public class ClassLinkUserObject
    {
        public long UserId { get; set; }
        public long TenantId { get; set; }
        public long BuildingId { get; set; }
        public long AuthenticationType { get; set; }
        public string DisplayName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string LoginId { get; set; }
        public string ImagePath { get; set; }
        public long LanguageId { get; set; }
        public long DefaultTimeFormat { get; set; }
        public string Profile { get; set; }
        public long ProfileId { get; set; }
        public string Tenant { get; set; }
        public string Building { get; set; }
        public string Role { get; set; }
        public string LastAccessTime { get; set; }
        public string SourcedId { get; set; }
   }

}
public class GoogleUserObject
{
    public string family_name { get; set; }
    public string name { get; set; }
    public string picture { get; set; }
    public string locale { get; set; }
    public string email { get; set; }
    public string given_name { get; set; }
    public string id { get; set; }
    public bool verified_email { get; set; }
}