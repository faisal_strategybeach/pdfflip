﻿using PDFFlip.EntityFramework;
using PDFFlip.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;

namespace PDFFlip.Controllers
{
    public class NotesController : ApiController
    {
        [HttpGet]
        [Route("~/api/Notes/GetNotes/{UserID}/{BookId}")]
        public IEnumerable<Note> GetNotes(int UserId,string BookId)
        {
            try
            {
                var context = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                UserId = Convert.ToInt32(context.Session["UserID"].ToString());
            }
            catch (Exception) { }

            using (var dbContext = new SWUNMATHConnection())
            {
                var Notes= dbContext.Notes.Where(X => (X.UserName == UserId.ToString() && X.BookID==BookId)).ToList();
                Notes = Notes.OrderBy(x => x.NoteID).ToList();
                return Notes;
            }
        }

        [HttpPost]
        [Route("~/api/Notes/SaveNotes")]
        public int SaveNotes([FromBody]Note note)
        {
            int NoteID = 1;          
            try
            {
                var context = Request.Properties["MS_HttpContext"] as HttpContextWrapper;

                using (var dbContext = new SWUNMATHConnection())
                {
                    note.UserName = context.Session["UserID"].ToString();

                    dbContext.Notes.Add(note);
                    dbContext.SaveChanges();
                    NoteID = dbContext.Notes.Max(X => X.NoteID);
                }
            }
            catch(Exception ex)
            {
                Logger.WriteLog(ex.ToString(), "Error");
            }
            return NoteID;
        }

        [HttpDelete]
        [Route("~/api/Notes/DeleteNote/{NoteID}")]
        public bool DeleteNote(int NoteID)
        {           
            using (var context = new SWUNMATHConnection())
            {
                Note note = context.Notes.Where(X => X.NoteID == NoteID).SingleOrDefault();

                if(note!=null)
                {
                    context.Notes.Remove(note);
                    context.SaveChanges();
                }
            }
            return true;
        }
    }
}