﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;

namespace PDFFlip.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Login()
        {
            FormsAuthentication.SignOut();
            return View();
        }

        [HttpPost]
        public ActionResult LoginOld(string UserName,string Password)
        {
            if ((UserName.ToLower() == ConfigurationManager.AppSettings["StudentLogin"].ToLower()
                || UserName.ToLower() == ConfigurationManager.AppSettings["StudentLogin"].ToLower()
                || UserName.ToLower() == ConfigurationManager.AppSettings["StudentLogin2"].ToLower())
                && Password == ConfigurationManager.AppSettings["Password"])
            {
                HttpContext.Session["User"] = UserName;


                FormsAuthentication.SetAuthCookie(UserName, false);
                System.Web.HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(new FormsAuthenticationTicket(1, UserName, DateTime.Now, DateTime.Now.AddMinutes(20), false, "student,teacher"))));
                return RedirectToAction("eBook", "Home");
            }
            else
            {
                ViewBag.Error_Message = "Invalid username or password.";
                return View("Login");
            }
        }

        [HttpPost]
        public ActionResult Login(string UserName, string Password)
        {
            if ((UserName.ToLower() == ConfigurationManager.AppSettings["StudentLogin"].ToLower() || UserName.ToLower() == ConfigurationManager.AppSettings["StudentLogin"].ToLower()) && Password == ConfigurationManager.AppSettings["Password"])
            {
                FormsAuthentication.SetAuthCookie(UserName, false);
                System.Web.HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(new FormsAuthenticationTicket(1, UserName, DateTime.Now, DateTime.Now.AddMinutes(20), false, "student,teacher"))));

                //Hardcoded userId for testing as currently we do not have the user table
                Session["UserID"] = "3";
                return RedirectToAction("eBook", "Home");
            }
            else
            {
                ViewBag.Error_Message = "Invalid username or password.";
                return View("Login");
            }
        }


        [HttpPost]
        public ActionResult OtherLogin(string LogInMethod)
        {
            if (LogInMethod.Trim().ToUpper()== "GOOGLE")
            {
                string GoogleClientID = Convert.ToString(ConfigurationManager.AppSettings["GoogleClientID"]);
                string GoogleClientSecret = Convert.ToString(ConfigurationManager.AppSettings["GoogleClientSecret"]);
                string GoogleReturnURL = Convert.ToString(ConfigurationManager.AppSettings["GoogleReturnURL"]);

                string googleURL = "";
                googleURL = googleURL + "https://accounts.google.com/o/oauth2/v2/auth?";
                googleURL = googleURL + "scope="+ HttpUtility.UrlEncode("profile email openid");
                googleURL = googleURL + "&access_type=offline";
                googleURL = googleURL + "&include_granted_scopes=true";
                googleURL = googleURL + "&response_type=code";
                googleURL = googleURL + "&state =Sessiontemp";
                googleURL = googleURL + "&redirect_uri=" + HttpUtility.UrlEncode(GoogleReturnURL);
                googleURL = googleURL + "&client_id="+ GoogleClientID;

                Response.Redirect(googleURL);
            }

            if (LogInMethod.Trim().ToUpper() == "CLEVER")
            {
                string CleverClientID = Convert.ToString(ConfigurationManager.AppSettings["CleverClientID"]);
                string CleverClientSecret = Convert.ToString(ConfigurationManager.AppSettings["CleverClientSecret"]);
                string CleverReturnURL = Convert.ToString(ConfigurationManager.AppSettings["CleverReturnURL"]);
                string cleverURL = "";
                cleverURL = cleverURL + "https://clever.com/oauth/authorize?";
                cleverURL = cleverURL + "response_type=code";
                cleverURL = cleverURL + "&redirect_uri=" + HttpUtility.UrlEncode(CleverReturnURL);
                cleverURL = cleverURL + "&client_id="+ CleverClientID;


                Response.Redirect(cleverURL);
            }

            if (LogInMethod.Trim().ToUpper() == "CLASSLINK")
            {
                string ClassLinkClientID = Convert.ToString(ConfigurationManager.AppSettings["ClassLinkClientID"]);
                string ClassLinkClientSecret = Convert.ToString(ConfigurationManager.AppSettings["ClassLinkClientSecret"]);
                string ClassLinkReturnURL = Convert.ToString(ConfigurationManager.AppSettings["ClassLinkReturnURL"]);
                string ClassLinkURL = "";

                ClassLinkURL = ClassLinkURL + "https://launchpad.classlink.com/oauth2/v2/auth?";
                ClassLinkURL = ClassLinkURL + "scope=full";
                ClassLinkURL = ClassLinkURL + "&redirect_uri=" + HttpUtility.UrlEncode(ClassLinkReturnURL);
                ClassLinkURL = ClassLinkURL + "&client_id="+ ClassLinkClientID;
                ClassLinkURL = ClassLinkURL + "&response_type=code";
                //ClassLinkURL = ClassLinkURL + "&state=f3dhsde

                Response.Redirect(ClassLinkURL);

            }
            return null;
        }

        [HttpGet]
        public ActionResult CheckIfUserLoggedInforEBook()
        {
            bool result = false;

            try
            {
                if (HttpContext.Session["User"] != null && !string.IsNullOrEmpty(HttpContext.Session["User"].ToString()))
                {
                    string[] assignedEBooks = ConfigurationManager.AppSettings[HttpContext.Session["User"].ToString().ToLower()].ToString().Split(',');
                    string userAccessingGrade = GetGradeFromReferrerUrl(HttpContext.Request.UrlReferrer.ToString());
                    if (assignedEBooks.Contains(userAccessingGrade))
                    {
                        result = true;
                    }
                }
            }
            catch (Exception)
            {
            }

            return Json(new { isUserLoggedIn = result }, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public string GetGradeFromReferrerUrl(string url)
        {
            url = url.Remove(url.LastIndexOf('/'));
            string[] urlParts = url.Split('/');

            return urlParts[urlParts.Length - 1].Replace("grade", "");
        }
    }
}