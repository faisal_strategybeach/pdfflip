﻿using PDFFlip.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;

namespace PDFFlip.Controllers
{
    public class BookMarksController : ApiController
    {
        [HttpGet]
        [AllowAnonymous]
        [Route("~/api/Bookmarks/GetBookmarks/{UserID}/{BookID}")]
        public IEnumerable<Bookmark> GetBookmarks(string UserID, string BookID)
        {
            try
            {
                var context = Request.Properties["MS_HttpContext"] as HttpContextWrapper;
                UserID = context.Session["UserID"].ToString();
            }
            catch (Exception) { }

            using (var dbContext = new SWUNMATHConnection())
            {
                return dbContext.Bookmarks.Where(X => X.UserName == UserID && X.BookID == BookID).ToList();
            }
        }

        [HttpPost]
        [Route("~/api/Bookmarks/SaveBookMark")]
        public int SaveBookMark([FromBody]Bookmark bookmark)
        {
            int BookmarkID = 0;
            try
            {
                var context = Request.Properties["MS_HttpContext"] as HttpContextWrapper;

                using (var dbContext = new SWUNMATHConnection())
                {
                    bookmark.UserName = context.Session["UserID"].ToString();
                    dbContext.Bookmarks.Add(bookmark);
                    dbContext.SaveChanges();
                    BookmarkID= dbContext.Bookmarks.Max(X => X.BookmarkID);
                }
            }
            catch(Exception ex)
            {

            }
            return BookmarkID;
        }

        [HttpDelete]
        [Route("~/api/Bookmarks/DeleteBookMark/{BmID}")]
        public bool DeleteBookmark(int BmID)
        {
            using (var context = new SWUNMATHConnection())
            {
                Bookmark BM = context.Bookmarks.Where(X => X.BookmarkID == BmID).SingleOrDefault();

                if (BM != null)
                {
                    context.Bookmarks.Remove(BM);
                    context.SaveChanges();
                }
            }
            return true;
        }
    }
}