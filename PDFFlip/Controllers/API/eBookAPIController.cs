﻿using PDFFlip.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using PDFFlip.Model;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using WordPressPCL;
using WordPressPCL.Models;
using System.Threading.Tasks;

namespace PDFFlip.Controllers
{
    public class eBookController : ApiController
    {
        [HttpGet]
        [Route("~/api/eBook/GeteBooks/{UserID}")]
        public List<eBook> GeteBooks(int UserId)
        {
           // GetAllWordPressUsers().Wait();
            using (var context = new SWUNMATHConnection())
            {                
                var eBooks = context.eBooks.Where(x=>x.Status==true).ToList();                
                return eBooks;
            }
        }

        [HttpPost]
        [Route("~/api/eBook/Drawings/SaveScreen")]
        public Drawings SaveScreen([FromBody]Drawings drawings)
        {            
            try
            {
                int eBookId = GeteBookId(drawings.pdfPath);
                if (!string.IsNullOrEmpty(drawings.book))
                {
                    drawings.book = drawings.book.Replace('!', ':');
                }
                //drawings.user = "manjulaagnihotri19771@gmail.com";
                using (var context = new SWUNMATHConnection())
                {
                    var drwaings = context.DrawingScreens.Where(x => x.user == drawings.user && x.book == drawings.book && x.chapter == drawings.chapter && x.eBookId == eBookId && x.DrawingToolbarPath==drawings.DrawingToolbarPath).FirstOrDefault();
                    if (drwaings != null)
                    {
                        context.DrawingScreens.Remove(drwaings);
                        context.SaveChanges();
                    }
                    if (drawings != null && drawings.drawing != null)
                    {
                        var drwaingboard = new DrawingScreen();
                        drwaingboard.book = drawings.book;
                        drwaingboard.chapter = drawings.chapter;
                        drwaingboard.drawing = Convert.ToString(drawings.drawing);
                        //drwaingboard.Id = drawings.Id;
                        drwaingboard.eBookId = eBookId;
                        drwaingboard.user = drawings.user;
                        drwaingboard.DrawingToolbarPath = drawings.DrawingToolbarPath;
                        context.DrawingScreens.Add(drwaingboard);
                        context.SaveChanges();
                        //drwaings.Id= context.DrawingScreens.OrderByDescending(x => x.user == drawings.user && x.book == drawings.book && x.chapter == drawings.chapter && x.eBookId == eBookId).FirstOrDefault().Id;
                    }                    
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.ToString(), "Error");
            }           
            return drawings;
        }
        [HttpGet]
        [Route("~/api/eBook/Drawings/LoadScreen/{userMail}/{bookUUID}/{chapterPath}/{pdfPath}/{drawingToolbarPath}")]
        public Drawings LoadScreen(string userMail,string bookUUID,string chapterPath,string pdfPath,string drawingToolbarPath)
        {           
            var drwaingboard = new Drawings();
            try
            {                
                int eBookId = GeteBookId(pdfPath);
                if (!string.IsNullOrEmpty(userMail))
                {
                    userMail = userMail.Replace('~', '.');
                }
                if (!string.IsNullOrEmpty(drawingToolbarPath))
                {
                    drawingToolbarPath = drawingToolbarPath.Replace('~', '.');
                }
                if (!string.IsNullOrEmpty(bookUUID))
                {
                    bookUUID = bookUUID.Replace('!', ':');
                }
                if (!string.IsNullOrEmpty(chapterPath))
                {
                    chapterPath = chapterPath.Replace('$', '/').Replace('~', '.');
                }
                using (var context = new SWUNMATHConnection())
                {
                    var drwaings = context.DrawingScreens.Where(x => x.user == userMail && x.book == bookUUID && x.chapter == chapterPath && x.eBookId == eBookId && x.DrawingToolbarPath== drawingToolbarPath).FirstOrDefault();
                   
                    if (drwaings != null && drwaings.drawing != null)
                    {
                        drwaingboard.Id = drwaings.Id;
                        drwaingboard.book = drwaings.book;
                        drwaingboard.chapter = drwaings.chapter;                        
                        drwaingboard.drawing = JObject.Parse(drwaings.drawing);
                        drwaingboard.Id = drwaings.Id;
                    }                   
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.ToString(), "Error");
            }           
            return drwaingboard;
        }
        private int GeteBookId(string PdfPath)
        {           
            int eBookId = 0;
            using (var context = new SWUNMATHConnection())
            {
                var drwaings = context.eBooks.Where(x => x.PdfPath == PdfPath).FirstOrDefault();
                if(drwaings!=null)
                {
                    eBookId = drwaings.Id;
                }
            }          
            return eBookId;
        }

        private static async Task GetAllWordPressUsers()
        {
            try
            {
                WordPressClient client = await GetClient();
                if (await client.IsValidJWToken())
                {
                    await client.Users.GetAll();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error:" + e.Message);
            }
        }

        private static async Task<WordPressClient> GetClient()
        {
            // JWT authentication
            var client = new WordPressClient("https://swunmath.com/wp-json/");
            client.AuthMethod = AuthMethod.JWT;
            await client.RequestJWToken("root", "swunhts!_yt!24g");
            return client;
        }

        public class Drawings
        {
            public int Id { get; set; }
            public string book { get; set; }
            public string chapter { get; set; }
            public dynamic drawing { get; set; }           
            public Nullable<System.DateTime> createdon { get; set; }
            public Nullable<System.DateTime> updatedon { get; set; }
            public string user { get; set; }
            public string pdfPath { get; set; }
            public string DrawingToolbarPath { get; set; }
            
        }
    }
}