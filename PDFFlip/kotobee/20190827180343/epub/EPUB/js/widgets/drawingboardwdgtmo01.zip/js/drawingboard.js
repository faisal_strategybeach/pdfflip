/**
 * Drawing Board Widget
 * @version 1.5
 * @authors Ahmed Abbas <abbas@kotobee.com>, Abdullah Saleh <abdullaahsalleh@gmail.com>
 * 
 * IMPORTANT NOTES:
 * 1. Main script must run on window `load` event (_not any custom kotobee event_)
 * 2. Kotobee Reader must enable `allow-same-origin allow-scripts allow-forms allow-modals` flags on `sandbox` attribute (_@lines 36 and 109 in www/js/app/kotobeeinteractive/types/widget.js_)
 * 3. Access to ebook `kotobee` object always via `parent.kotobee` if on the same origin
 * 4. Access to ebook `book` and `chapter` via `postMessage` if not on the same origin. This case requires passwing both properties from `global.js` as follows:
document.addEventListener('kotobeeChapterLoaded', e => {
    // pass kotobee book identifier and chapter url to widget iframes
    var i,
        iframes = document.getElementsByTagName('iframe'),
        max = iframes.length,
        a = document.createElement('a');
    for (i = 0; i < max; i++) {
        if (iframes[i].src.indexOf('.amazonaws.com') !== -1) {
            a.href = iframes[i].src;
            iframes[i].addEventListener('load', e => {
                e.target.contentWindow.postMessage({
                    book: kotobee.book.meta.dc.identifier,
                    chapter: kotobee.currentChapter.url.split('EPUB/xhtml/')[1] // use internal chapter path for consistemcy with live ebooks
                }, `${a.protocol}//${a.hostname}`);
            });
        }
    }
});
 */

var drawingBoard,
    drawingTools,
    drawingStroke = 10,
    drawingColor = 'hsla(0, 0%, 0%, 1)',
    drawingState,
    userMail,
    bookUUID,
    chapterPath,
    //host = location.host.indexOf('localhost') !== -1 ? 'http://localhost:5000' : 'https://kotobee.herokuapp.com';
    host = 'https://kotobee.herokuapp.com';

const activateTool = t => {
    drawingBoard.setTool(t.tool);
    drawingBoard.tool.strokeWidth = drawingStroke;
    drawingBoard.colors.primary = drawingColor;
    drawingTools.map(t2 => {
        if (t == t2) {
            t2.el.classList.add('selected');
        } else {
            t2.el.classList.remove('selected');
        }
    });
};

const updateColor = jscolor => {
    drawingColor = drawingBoard.colors.primary = hexToHSLA(`#${jscolor}`);
};

const validateEmail = (e, action) => {
    if (e) e.preventDefault();
    userMail = document.querySelector("input[type=email]").value;
    var emailReg = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/g;
    if (userMail === '') {
        new Attention.Alert({
            title: 'Alert !',
            content: 'Please Input Your Email !'
        });
    } else if (!(userMail).match(emailReg)) {
        new Attention.Alert({
            title: 'Alert !',
            content: 'Invalid Email !'
        });
    } else {
        localStorage.setItem('userMail', userMail);
        eval(`${action}Screen()`);
    }
};

const loadScreen = () => {
    fetch(`${host}/api/v1/drawings/${userMail}/${bookUUID}/${chapterPath}`, {
        method: 'GET'
    }).then((res) => {
        if (res.status === 404) {
            document.querySelector('.btn-load').style.display = 'none';
            document.querySelector('.btn-save').style.display = 'block';
        }
        return res.json();
    }).then(res => {
        if (res.success) {
            if (drawingBoard.getSnapshot().shapes.length > 0) {
                new Attention.Confirm({
                    title: 'Caution!',
                    content: 'Replace your current drawing?',
                    onConfirm: component => {
                        drawingBoard.loadSnapshot(res.drawing);
                    }
                });
            } else {
                drawingBoard.loadSnapshot(res.drawing);
            }
        }
    });
};

const saveScreen = () => {
    fetch(`${host}/api/v1/drawings/${userMail}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            book: bookUUID,
            chapter: chapterPath,
            drawing: drawingBoard.getSnapshot()
        })
    }).then((res) => {
        return res.json();
    }).then(res => {
        new Attention.Alert({
            title: 'Alert !',
            content: "Drawing Updated Successfully."
        });
    });
};

const init = () => {
    document.querySelector('.drawing-widget').style.height = `${Math.min(window.innerHeight, window.outerHeight)}px`;
    // ref: http://literallycanvas.com
    let W = document.querySelector('.drawing-widget').clientWidth - 2;
    let H = document.querySelector('.drawing-widget').clientHeight - document.querySelector('.drawing-tools').clientHeight - 2;
    if (Math.min(window.innerWidth, window.outerWidth) <= 768) H -= document.querySelector('.user-form').clientHeight + 5;
    document.querySelector('.drawing-board').style.width = `${W}px`;
    document.querySelector('.drawing-board').style.height = `${H}px`;
    drawingBoard = LC.init(document.querySelector('.drawing-board'));
    drawingTools = [{
        name: 'pencil',
        el: document.getElementById('tool-pencil'),
        tool: new LC.tools.Pencil(drawingBoard)
    },
    {
        name: 'eraser',
        el: document.getElementById('tool-eraser'),
        tool: new LC.tools.Eraser(drawingBoard)
    }
    ];
    drawingTools.map(t => {
        t.el.style.cursor = "pointer";
        t.el.addEventListener('click', e => {
            e.preventDefault();
            activateTool(t);
        });
    });
    activateTool(drawingTools[0]);
    drawingBoard.on('snapshotLoad', args => {
        document.querySelector('.btn-load').style.display = 'none';
        document.querySelector('.btn-save').style.display = 'block';
        if (drawingBoard.getSnapshot().shapes.length > 0 && document.querySelector('#tool-undo').classList.contains('disabled')) {
            document.querySelector('#tool-undo').classList.remove('disabled');
        }
    });
    drawingBoard.on('shapeSave', args => {
        if (document.querySelector('#tool-undo').classList.contains('disabled')) {
            document.querySelector('#tool-undo').classList.remove('disabled');
        }
    });
    drawingBoard.on('clear', args => {
        if (document.querySelector('#tool-undo').classList.contains('disabled')) {
            document.querySelector('#tool-undo').classList.remove('disabled');
        }
    });
    drawingBoard.on('redo', args => {
        if (document.querySelector('#tool-undo').classList.contains('disabled')) {
            document.querySelector('#tool-undo').classList.remove('disabled');
        }
    });
    drawingBoard.on('undo', args => {
        if (document.querySelector('#tool-redo').classList.contains('disabled')) {
            document.querySelector('#tool-redo').classList.remove('disabled');
        }
    });
    document.querySelector('#tool-undo').addEventListener('click', e => {
        e.preventDefault();
        if (!e.target.classList.contains('disabled')) {
            drawingState = drawingBoard.undo();
            if (typeof drawingState === 'undefined') {
                document.querySelector('#tool-undo').classList.add('disabled');
            }
        }
    });
    document.querySelector('#tool-redo').addEventListener('click', e => {
        e.preventDefault();
        if (!e.target.classList.contains('disabled')) {
            drawingState = drawingBoard.redo();
            if (typeof drawingState === 'undefined') {
                document.querySelector('#tool-redo').classList.add('disabled');
            }
        }
    });
    document.getElementById('tool-stroke').querySelector('input[type="range"]').addEventListener('change', e => {
        drawingStroke = drawingBoard.tool.strokeWidth = Number(e.target.value);
    });
    document.querySelector('.jscolor').value = HSLAToHex(drawingColor);
    jscolor.installByClassName('jscolor'); // reinstall jscolor on chapter loaded!
    userMail = typeof Storage !== "undefined" ? localStorage.getItem('userMail') : null;
    if (userMail) {
        document.querySelector('.btn-load').style.display = 'block';
        document.querySelector('.btn-save').style.display = 'none';
        document.querySelector('input[type=email]').value = 'manjulaagnihotri19771@gmail.com';
        document.querySelector('input[type=email]').disabled = true;
        validateEmail(null, 'load');
    }
};

window.addEventListener("resize", e => {
    window.location.reload();
});

window.addEventListener("load", e => {
    try {
        if (parent && parent.kotobee) {
            bookUUID = parent.kotobee.book.meta.dc.identifier;
            chapterPath = parent.kotobee.currentChapter.url.split('EPUB/xhtml/')[1]; // use internal chapter path for consistemcy with live ebooks
        }
        init();
    } catch (exp) {
        //console.error(exp);
    }
});

window.addEventListener("message", e => {
    if (e.data && e.data.book && e.data.chapter) {
        bookUUID = e.data.book;
        chapterPath = e.data.chapter;
    }
    if (!window.loaded) {
        window.loaded = true;
        init();
    }
});
