﻿function getElementsByAttribute(e, t, n, o) {
    for (var r, i, a = "*" == t && e.all ? e.all : e.getElementsByTagName(t), s = new Array, l = void 0 !== o ? new RegExp("(^|\\s)" + o + "(\\s|$)", "i") : null, c = 0; c < a.length; c++) "string" == typeof (i = (r = a[c]).getAttribute && r.getAttribute(n)) && i.length > 0 && (void 0 === o || l && l.test(i)) && s.push(r);
    return s
}

function toggleClass(e, t) {
    strHasClass(e.className, t) ? removeClass(e, t) : addClass(e, t)
}

function addRemoveClass(e, t, n) {
    var o = e.className,
        r = !0;
    if (t)
        for (i = 0; i < t.length; i++) strHasClass(o, t[i]) || (r = !1, o += ("" == o ? "" : " ") + t[i]);
    if (n)
        for (var i = 0; i < n.length; i++) strHasClass(o, n[i]) && (r = !1, o = strRemoveClass(o, n[i]));
    r || (e.className = o.trim())
}

function addClass(e, t) {
    var n = e.className.trim();
    if (t instanceof Array) {
        for (var o = !0, r = 0; r < t.length; r++) strHasClass(n, t[r]) || (o = !1, n += ("" == n ? "" : " ") + t[r]);
        if (o) return
    } else {
        if (strHasClass(n, t)) return;
        n += ("" == n ? "" : " ") + t
    }
    e.className = n
}

function removeClass(e, t) {
    var n = e.className;
    if (t instanceof Array) {
        for (var o = !0, r = 0; r < t.length; r++) strHasClass(n, t[r]) && (o = !1, n = strRemoveClass(n, t[r]));
        if (o) return
    } else {
        if (!strHasClass(n, t)) return;
        n = strRemoveClass(n, t)
    }
    "" == (n = n.trim()) ? e.removeAttribute("class") : e.className = n
}

function getClassThatStartsWith(e, t) {
    for (var n = e.className.trim().split(" "), o = 0; o < n.length; o++)
        if (n[o].indexOf(t) > -1) return n[o]
}

function hasClass(e, t) {
    return strHasClass(e.className, t)
}

function strHasClass(e, t) {
    e || (e = "");
    for (var n = e.trim().split(" "), o = 0; o < n.length; o++)
        if (n[o] == t) return !0;
    return !1
}

function strRemoveClass(e, t) {
    e || (e = "");
    for (var n = e.trim().split(" "), o = 0; o < n.length; o++) n[o] == t && (n[o] = null);
    return n.join(" ")
}

function shouldParse(e) {
    if (!e.match(/^http[s]?:/g) && 0 != e.indexOf("/") && 0 != e.indexOf("#dontparse")) return !0
}

function removeElement(e) {
    e && e.parentNode && e.parentNode.removeChild(e)
}

function guid() {
    function e() {
        return Math.floor(65536 * (1 + Math.random())).toString(16).substring(1)
    }
    return e() + e() + "-" + e() + "-" + e() + "-" + e() + "-" + e() + e() + e()
}

function randStr(e, t) {
    return t || (t = "s"), t + Math.random().toString(36).substring(5).substr(0, e)
}

function getDateTime() {
    var e = new Date,
        t = e.getFullYear(),
        n = e.getMonth() + 1,
        o = e.getDate(),
        r = e.getHours(),
        i = e.getMinutes(),
        a = e.getSeconds();
    if (1 == n.toString().length) n = "0" + n;
    if (1 == o.toString().length) o = "0" + o;
    if (1 == r.toString().length) r = "0" + r;
    if (1 == i.toString().length) i = "0" + i;
    if (1 == a.toString().length) a = "0" + a;
    return t + "/" + n + "/" + o + " " + r + ":" + i + ":" + a
}

function getFormData(e) {
    e || (e = {});
    var t = new FormData;
    for (var n in e) e[n] && t.append(n, e[n]);
    return t
}

function newBlob(e, t) {
    try {
        var n = new Blob([e], {
            type: t
        });
        return n
    } catch (r) {
        if (window.BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder || window.MSBlobBuilder, "TypeError" == r.name && window.BlobBuilder) {
            var o = new BlobBuilder;
            return o.append(e.buffer), n = o.getBlob(t)
        }
        if ("InvalidStateError" == r.name) return n = new Blob([e.buffer], {
            type: t
        })
    }
}

function clone(e) {
    if (null == e || "object" != typeof e) return e;
    var t = e.constructor();
    for (var n in e) e.hasOwnProperty(n) && (t[n] = clone(e[n]));
    return t
}

function rgb2hex(e) {
    function t(e) {
        return ("0" + parseInt(e).toString(16)).slice(-2)
    }
    return /^#[0-9A-F]{6}$/i.test(e) ? e : (e = e.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/)) ? "#" + t(e[1]) + t(e[2]) + t(e[3]) : void 0
}

function timeOut(e, t) {
    var n = setInterval(function () {
        clearInterval(n), e()
    }, t);
    return n
}

function wrapNode(e, t) {
    var n = document.createRange();
    n.selectNode(e), n.surroundContents(document.createElement(t))
}

function wrapNodeFromTo(e, t, n, o) {
    var r = document.createRange();
    r.setStart(e, n), r.setEnd(e, o), r.surroundContents(document.createElement(t))
}

function XMLEscape(e, t) {
    for (var n = "", o = 0; o < e.length; o++) {
        var r = e.charAt(o);
        n += "&" == r ? "&amp;" : "'" == r ? t ? "&apos;" : "&#39;" : r
    }
    return n
}

function replaceHTML(elementID, t) {
    var node = "string" == typeof elementID ? document.getElementById(elementID) : elementID,
        o = node.cloneNode(!1);
    return o.innerHTML = t, node.parentNode.replaceChild(o, node), o
}

function kTemplate(route) {
    return mobile ? (config.overflowScroll ? "templates/mobileOverflow/" : "templates/mobile/") + route : "templates/web/" + route
}

function arrayContainsProperty(objArray, prop, checkVal) {
    for (var i = 0; i < objArray.length; i++)
        if (objArray[i][prop] == checkVal) return !0;
    return !1
}

function cacheBust(e) {
    if (desktop && (0 == e.indexOf("/") || 0 == e.indexOf("file://") || 0 == e.indexOf("chrome-extension://"))) return e;
    var t = "?";
    return e.indexOf("?") >= 0 && (t = "&"), e + t + Math.floor(9999 * Math.random())
}

function addPrefixedEvent(e, t, n) {
    for (var o = ["webkit", "moz", "MS", "o", ""], r = 0; r < o.length; r++) o[r] || (t = t.toLowerCase()), e.addEventListener(o[r] + t, n, !1)
}

function removePrefixedEvent(e, t, n) {
    for (var o = ["webkit", "moz", "MS", "o", ""], r = 0; r < o.length; r++) o[r] || (t = t.toLowerCase()), e.removeEventListener(o[r] + t, n, !1)
}

function redrawForChrome(e) {
    -1 != navigator.userAgent.toLowerCase().indexOf("chrome") && (e.style.display = "inline-block", e.offsetHeight, e.style.display = "")
}

function pauseVideoAndAudio(e) {
    for (var t = e.getElementsByTagName("audio"), n = t.length; n--;) try {
        t[n].pause()
    } catch (e) { }
    for (var o = e.getElementsByTagName("video"), n = o.length; n--;) try {
        o[n].pause()
    } catch (e) { }
}

function isEmpty(e) {
    for (var t in e)
        if (e.hasOwnProperty(t)) return !1;
    return JSON.stringify(e) === JSON.stringify({})
}

function asyncCheck(e, t, n, o) {
    return e ? o() : (n.push(o), t.apply(this, n))
}

function autoprefixTransform(e, t) {
    t || (t = ""), e.style.transform = e.style.msTransform = e.style.webkitTransform = e.style.mozTransform = t
}

function deleteTransform(e) {
    e && (e.style.transform = e.style.msTransform = e.style.webkitTransform = e.style.mozTransform = null)
}

function deleteTransformOrigin(e) {
    e && (e.style.transformOrigin = e.style.msTransformOrigin = e.style.webkitTransformOrigin = e.style.mozTransformOrigin = null)
}

function getChildIndex(e) {
    for (var t = 0; null != (e = e.previousSibling);) t++;
    return t
}

function getFilteredChildIndex(e) {
    for (var t = 0; e = e.previousSibling;) "#text" != e.nodeName && 8 != e.nodeType && t++;
    return t
}

function autoprefixTransformOrigin(e, t) {
    t || (t = ""), e.style.transformOrigin = e.style.msTransformOrigin = e.style.webkitTransformOrigin = e.style.mozTransformOrigin = t
}

function detectIE() {
    var e = window.navigator.userAgent,
        t = e.indexOf("MSIE ");
    if (t > 0) return parseInt(e.substring(t + 5, e.indexOf(".", t)), 10);
    if (e.indexOf("Trident/") > 0) {
        var n = e.indexOf("rv:");
        return parseInt(e.substring(n + 3, e.indexOf(".", n)), 10)
    }
    var o = e.indexOf("Edge/");
    return o > 0 && parseInt(e.substring(o + 5, e.indexOf(".", o)), 10)
}

function getImgDataSrc(e, t) {
    var n = new XMLHttpRequest;
    n.open("GET", e), n.responseType = "blob", n.onload = function () {
        var e = new FileReader;
        e.onloadend = function () {
            t && t(e.result)
        }, e.readAsDataURL(n.response)
    }, n.send()
}

function isAbsoluteURLPath(e) {
    return 0 == e.trim().indexOf("http://") || (0 == e.trim().indexOf("https://") || (0 == e.trim().indexOf("//") || (0 == e.trim().indexOf("data:") || void 0)))
}

function isAbsoluteURLAndNotDataPath(e) {
    return 0 == e.trim().indexOf("http://") || (0 == e.trim().indexOf("https://") || (0 == e.trim().indexOf("//") || void 0))
}

function isKotobeeHosted(e) {
    e || (e = window.location.href);
    var t = e.split("://");
    if (!(t.length <= 1)) {
        for (var n = t[1], o = ["kotobee.com"], r = 0; r < o.length; r++) {
            if (n.indexOf(o[r] + "/ebook") >= 0) return !0;
            if (n.indexOf(o[r] + "/library") >= 0) return !0;
            if (n.indexOf(o[r] + "/lti") >= 0) return !0
        }
        if (e.indexOf(".kotobee.com") >= 0) {
            var i = e.replace(/https?:\/\/(.*?\.)kotobee\.com.*/g, "$1");
            if (i) {
                var a = i.replace(/(.*?)\./g, "$1");
                return "www" != a && ("dev" != a && ("test" != a && "qa" != a))
            }
        }
    }
}

function decodeHTMLEntities(e) {
    for (var t = [
        ["amp", "&"],
        ["apos", "'"],
        ["#x27", "'"],
        ["#x2F", "/"],
        ["#39", "'"],
        ["#47", "/"],
        ["lt", "<"],
        ["gt", ">"],
        ["nbsp", " "],
        ["quot", '"']
    ], n = 0, o = t.length; n < o; ++n) e = e.replace(new RegExp("&" + t[n][0] + ";", "g"), t[n][1]);
    return e
}

function addFormHiddenInput(e, t, n) {
    var o = document.createElement("input");
    o.setAttribute("name", e), o.setAttribute("type", "hidden"), o.setAttribute("value", t), n.appendChild(o)
}

function getElemMargin(e, t) {
    return getElemDim(e, "margin", t)
}

function getElemPadding(e, t) {
    return getElemDim(e, "padding", t)
}

function getElemDim(e, t, n) {
    n || (n = {});
    var o = {},
        r = n.computed ? getComputedStyle(e) : e.style;
    return o.left = r[t + "Left"].split("px")[0], o.right = r[t + "Right"].split("px")[0], "auto" == o.left ? o.x = "auto" : "auto" == o.left ? o.x = "auto" : o.x = Math.round(o.left) + Math.round(o.right), o.top = r[t + "Top"].split("px")[0], o.bottom = r[t + "Bottom"].split("px")[0], "auto" == o.top ? o.y = "auto" : "auto" == o.bottom ? o.y = "auto" : o.y = Math.round(o.top) + Math.round(o.bottom), o
}

function makeArrayUnique(e) {
    for (var t = 0; t < e.length; t++)
        for (var n = e[t], o = e.length; o--;) t != o && e[o].length + "  " + n.length && e[o] == n && e.splice(o, 1)
}

function cloneNodeAttributes(e) {
    var t = document.createElement(e.nodeName);
    return copyAttributes(e, t), t
}

function copyAttributes(e, t) {
    for (var n = 0; n < e.attributes.length; n++) t.setAttribute(e.attributes[n].name, e.attributes[n].value)
}

function getBoundingBox(e) {
    if (3 == e.nodeType) {
        var t = document.createRange();
        t.selectNode(e);
        var n = t.getBoundingClientRect();
        return t.detach(), n
    }
    return e.getBoundingClientRect()
}

function parents(e, t) {
    t || (t = {});
    for (var n = []; e && (!t.stopAtId || e.id != t.stopAtId); e = e.parentNode) t.filterByClass && !e.classList.contains(t.filterByClass) || n.unshift(e);
    return n
}

function getCurrentTime() {
    return Math.floor(Date.now() / 1e3)
}

function getQueryStringValue(e) {
    var t = window.location.href.split("?");
    if (!(t.length <= 1))
        for (var n = t[1].split("&"), o = 0; o < n.length; o++) {
            var r = n[o].split("=");
            if (r[0] == e) return r[1]
        }
}

function copyObjToObj(e, t, n) {
    n || (n = {});
    for (var o in e) n.onlyIfNotExists && null != t[o] || (t[o] = e[o])
}

function traverseToBottom(e, t) {
    for (; e && e.children.length;) {
        if (t && e.id == t) return e;
        e = e.children[0]
    }
    return e
}

function traverseToTop(e, t) {
    for (; e && e.parentNode;) {
        if (t && e.id == t) return e;
        e = e.parentNode
    }
    return !e || e.parentNode ? null : e.id != t ? null : e
}

function getDomainFromURL(e) {
    var t = new URL(e);
    return t.protocol + "//" + t.hostname
}

function hasParent(e, t, n) {
    n || (n = 3);
    for (var o = 0; o < n; o++) {
        if ("body" == (e = e.parentNode).nodeName.toLowerCase()) return;
        if (hasClass(e, t)) return !0
    }
}

function XORCipher() {
    function e(e) {
        var t, n, o, r, i, s, l = 0,
            c = "";
        if (!e) return e;
        do {
            t = (i = e[l++] << 16 | e[l++] << 8 | e[l++]) >> 18 & 63, n = i >> 12 & 63, o = i >> 6 & 63, r = 63 & i, c += a.charAt(t) + a.charAt(n) + a.charAt(o) + a.charAt(r)
        } while (l < e.length);
        return ((s = e.length % 3) ? c.slice(0, s - 3) : c) + "===".slice(s || 3)
    }

    function t(e) {
        var t, n, o, r, i, s, l = 0,
            c = [];
        if (!e) return e;
        e += "";
        do {
            t = (s = a.indexOf(e.charAt(l++)) << 18 | a.indexOf(e.charAt(l++)) << 12 | (r = a.indexOf(e.charAt(l++))) << 6 | (i = a.indexOf(e.charAt(l++)))) >> 16 & 255, n = s >> 8 & 255, o = 255 & s, c.push(t), 64 !== r && (c.push(n), 64 !== i && c.push(o))
        } while (l < e.length);
        return c
    }

    function n(e, t) {
        return e.charCodeAt(Math.floor(t % e.length))
    }

    function o(e, t) {
        return i(t, function (t, o) {
            return t.charCodeAt(0) ^ n(e, o)
        })
    }

    function r(e, t) {
        return i(t, function (t, o) {
            return String.fromCharCode(t ^ n(e, o))
        }).join("")
    }

    function i(e, t) {
        for (var n = [], o = 0; o < e.length; o++) n[o] = t(e[o], o);
        return n
    }
    var a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    return {
        encode: function (t, n) {
            return n = o(t, n), e(n)
        },
        decode: function (e, n) {
            return n = t(n), r(e, n)
        }
    }
}

function isFunction(e) {
    return e && "[object Function]" === {}.toString.call(e)
}

function stopAllAudio() {
    for (var e = document.getElementsByTagName("audio"), t = 0; t < e.length; t++) e[t].pause(), e[t].src = "";
    try {
        document.bgAudio && (document.bgAudio.pause(), document.bgAudio.src = "")
    } catch (e) { }
}

function applyPreview(e) {
    var t = angular.element(document.body).scope();
    t.config = config = e, t.$apply(), angular.element(document.getElementById("home")).scope().injectUserCSS(e)
}

function previewPage(e, t) {
    var n = angular.element(document.body).scope();
    n.previewPage && n.previewPage(e, t)
}

function getCookie(e) {
    var t = ("; " + document.cookie).split("; " + e + "=");
    if (2 == t.length) return t.pop().split(";").shift()
}

function getVar(e) {
    return Modernizr.localstorage ? localStorage[e] : getCookie("continueWithoutHTML5")
}

function setVar(e, t) {
    Modernizr.localstorage ? localStorage[e] = t : document.cookie = e + "=" + t
}

function getGlobal() {
    return angular.element(document.body).scope()
}

function closePrintIframe() {
    var e = document.getElementById("printContainer");
    e && e.parentNode.removeChild(e)
}

function putTest(e) {
    return
}

function continueLocally() {
    setVar("continueLocally", "true"), location.reload()
}

function continueWithoutHTML5() {
    setVar("continueWithoutHTML5", "true"), location.reload()
}

function getKotobeeListener(e) {
    if (e)
        for (var t = kotobeeListeners.length; t--;)
            if (kotobeeListeners[t].event == e) return kotobeeListeners[t]
}

function clearKotobeeListeners(e) {
    for (var t = kotobeeListeners.length; t--;) e && kotobeeListeners[t].event != e || kotobeeListeners.splice(t, 1)
}

function dispatchKotobeeEvent() { }

function kInteractionStart() {
    this.log = function (e) { }, kInteractive.setDOMParser(), kInteractive.preRender(), kInteractive.postRender()
}

function kInteractionStartSingleElem(e) {
    kInteractive.setDOMParser(), kInteractive.preRender(e, {
        singleElem: !0
    }), kInteractive.postRender(e, {
        singleElem: !0
    })
} ! function (e) {
    "use strict";
    var t = e.prototype,
        n = t.parseFromString;
    try {
        if ((new e).parseFromString("", "text/html")) return
    } catch (e) { }
    t.parseFromString = function (e, t) {
        if (/^\s*text\/html\s*(?:;|$)/i.test(t)) {
            var o, r = document.implementation.createHTMLDocument(""),
                i = r.documentElement;
            return i.innerHTML = e, o = i.firstElementChild, 1 === i.childElementCount && "html" === o.localName.toLowerCase() && r.replaceChild(o, i), r
        }
        return n.apply(this, arguments)
    }
}(DOMParser);
ph = {};

ph.removeHash = function (e) {
    return -1 == e.indexOf("#") ? e : e.substring(0, e.lastIndexOf("#"))
};

ph.getHash = function (e) {
    if (e.indexOf("#") >= 0) return e.substr(e.lastIndexOf("#") + 1)
};

ph.normalize = function (e) {
    var t = ph.normalizedArray(e).join("/");
    return arguments.length && "/" == arguments[0].substr(0, 1) && (t = "file:///" + t), t
};

ph.normalizedArray = function (e) {
    for (var t = (e = e.replace(/\\/g, "/")).split("/"), n = t.length; n--;) t[n] ? ".." == t[n] && n > 0 && ".." != t[n - 1] && (t.splice(n, 1), t.splice(n - 1, 1)) : t.splice(n, 1);
    return t[0].match(/http[s]?:$/g) && (t[0] += "//" + t[1], t.splice(1, 1)), "file:" == t[0] && (t[0] += "///" + t[1], t.splice(1, 1)), t
};

ph.basename = function (e) {
    if (-1 == e.indexOf("/")) return e;
    var t = ph.normalizedArray(e);
    return t[t.length - 1]
};

ph.removeFilename = function (e) {
    var t = ph.normalizedArray(e);
    return -1 == t[t.length - 1].indexOf(".") ? e : (t.splice(t.length - 1, 1), t.join("/"))
};

ph.removeExtension = function (e) {
    if (-1 == e.indexOf(".")) return e;
    var t = e.split(".");
    return t.splice(t.length - 1, 1), t.join(".")
};

ph.getExtension = function (e) {
    if (-1 == e.indexOf(".")) return "";
    var t = e.split(".");
    return t[t.length - 1]
};

ph.dirname = function (e) {
    if (-1 == e.indexOf("/")) return e;
    var t = ph.normalizedArray(e);
    return t[t.length - 1].indexOf(".") >= 0 && t.splice(t.length - 1, 1), 1 == t.length ? t[0] : t[t.length - 1]
};

ph.relative = function (e, t) {
    var n, o, r = ph.normalizedArray(e),
        i = ph.normalizedArray(t),
        a = new Array;
    for (r[r.length - 1].indexOf(".") >= 0 && r.splice(r.length - 1, 1), n = 0; n < r.length && r[n] == i[n]; n++);
    for (o = n; o < r.length; o++) a.push("..");
    for (o = n; o < i.length; o++) a.push(i[o]);
    return a.join("/")
};

ph.join = function () {
    for (var e = new Array, t = 0; t < arguments.length; t++) {
        var n = ph.normalizedArray(arguments[t]);
        t < arguments.length - 1 && !isAbsoluteURLPath(n[n.length - 1]) && n[n.length - 1].indexOf(".") >= 0 && n.splice(n.length - 1, 1), e = e.concat(n)
    }
    var o = ph.normalize(e.join("/"));
    return "/" == arguments[0].substr(0, 1) && (o = desktop ? "file:////" + o : "/" + o), o
};

ph.parent = function (e) {
    e = ph.removeFilename(e);
    var t = ph.normalizedArray(e);
    t.splice(t.length - 1, 1);
    var n = t.join("/");
    return arguments.length && "/" == arguments[0].substr(0, 1) && (n = "/" + n), n
};
var startDate = Date.now();
/*LoggerWorker*/;
try {
    (loggerWorker = new Worker(rootDir + "js/app/workers/logger.js")).addEventListener("error", function () {
        loggerWorker = null
    }, !1)
} catch (e) { }
var kLog = function (e) {
    if (debug) {
        null == e && (e = "null"), debugDuration && (e = Date.now() - startDate + " " + e);
        try {
            if (loggerWorker) return void loggerWorker.postMessage({
                func: "log",
                params: arguments
            })
        } catch (e) { }
        console.log.apply(this, arguments)
    }
};

log = kLog;
Base64Binary = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    decodeArrayBuffer: function (e) {
        var t = e.length / 4 * 3,
            n = new ArrayBuffer(t);
        return this.decode(e, n), n
    },
    decode: function (e, t) {
        var n = this._keyStr.indexOf(e.charAt(e.length - 1)),
            o = this._keyStr.indexOf(e.charAt(e.length - 2)),
            r = e.length / 4 * 3;
        64 == n && r-- , 64 == o && r--;
        var i, a, s, l, c, d, u, p = 0,
            f = 0;
        for (i = t ? new Uint8Array(t) : new Uint8Array(r), e = e.replace(/[^A-Za-z0-9\+\/\=]/g, ""), p = 0; p < r; p += 3) a = this._keyStr.indexOf(e.charAt(f++)) << 2 | (c = this._keyStr.indexOf(e.charAt(f++))) >> 4, s = (15 & c) << 4 | (d = this._keyStr.indexOf(e.charAt(f++))) >> 2, l = (3 & d) << 6 | (u = this._keyStr.indexOf(e.charAt(f++))), i[p] = a, 64 != d && (i[p + 1] = s), 64 != u && (i[p + 2] = l);
        return i
    }
};
var currentIndex, currentVIndex, parser = new DOMParser, preview, previewParams, app, mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
var mobileDebug = !1;
var ios = /iPhone|iPad|iPod/i.test(navigator.userAgent);
var readerV = 1.4, debug = !1, native, bookPath, noFlash, desktop, chromeApp, os, uuid, kotobee = {},
    loggerWorker, partitionEnabled = "Android" == mobile && config.kotobee.segregate,
    isKotobee = !0,
    debugDuration = !1,
    overflowScroll = !mobile | config.overflowScroll,
    rootDir = "",
    postOptions = {
        transformRequest: angular.identity,
        headers: {
            "Content-Type": void 0
        }
    };
null == config.mediaTab && (config.mediaTab = !0), null == config.settingsTab && (config.settingsTab = !0), null == config.notebookTab && (config.notebookTab = !0), null == config.textSizeControls && (config.textSizeControls = !0), null == config.chaptersTab && (config.chaptersTab = !0), null == config.showCategories && (config.showCategories = !0), null == config.styleControls && (config.styleControls = !1),
    function () {
        function e() {
            for (var e = document.getElementsByTagName("head")[0], t = e.innerHTML, n = [], o = [], r = document.getElementsByTagName("script"), i = 0; i < r.length; i++)
                if (r[i].getAttribute("src") && r[i].getAttribute("src").indexOf("lib/ionic/release/js") >= 0) {
                    var a = r[i].getAttribute("src").split("lib/ionic/release/js");
                    rootDir = a[0];
                    break
                }
            try {
                (loggerWorker = new Worker(rootDir + "js/workers/logger.js")).addEventListener("error", function () {
                    loggerWorker = null
                }, !1)
            } catch (e) { }
            n = [rootDir + "lib/ionic/release/css/ionic.min.css"];
            for (i = 0; i < o.length; i++) t += '<script src="' + o[i] + '"><\/script>';
            for (i = 0; i < n.length; i++) t = '<link rel="stylesheet" href="' + n[i] + '">' + t;
            e.innerHTML = t
        }

        function t() {
            var e = document.getElementsByTagName("body");
            e.length ? addClass(e[0], "ie") : timeOut(t, 100)
        }
        if (detectIE() && t(), window.location.href.indexOf("?preview") > 0) {
            preview = !0, mobileDebug = !0;
            var n = window.location.href.split("?");
            n = n[1].split("&"), previewParams = {};
            for (var o = 0; o < n.length; o++) {
                var r = n[o].split("=");
                previewParams[r[0]] = r[1]
            }
            mobile = previewParams.mobile
        } else {
            if (config.gAnalyticsID) {
                var i = "";
                isKotobeeHosted() && (i = "/bundles/vijualib/templates/reader/public/");
                var a = "";
                (native || desktop) && (a = "ga('set','checkProtocolTask',null);ga('set','anonymizeIp',true);ga('set','forceSSL',true)");
                var s = "'auto'";
                native ? s = "{'storage':'none','clientId':device.uuid}" : desktop && (s = "{'storage':'none'}");
                var l = document.createElement("script");
                l.innerHTML = "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','" + i + "lib/gAnalytics/analytics.js','ga');ga('create', '" + config.gAnalyticsID + "', " + s + ");" + a;
                var c = document.getElementsByTagName("head");
                c.length && c[0].appendChild(l)
            }
            if ("file:" == window.location.protocol || "undefined" != typeof require) {
                if (mobile) native = !0;
                else if ("undefined" != typeof require) desktop = !0, os = "mac", process.env.OS && process.env.OS.toLowerCase().indexOf("windows") >= 0 && (os = process.env.PROCESSOR_ARCHITECTURE.indexOf("64") >= 0 ? "win64" : "win32");
                else if (window.chrome && chrome.app && chrome.app.runtime && chrome.storage) {
                    try {
                        if (!chromeEnabled) return
                    } catch (e) {
                        return
                    }
                    chromeApp = !0
                } else if ("true" != getVar("continueLocally")) return timeOut(function () {
                    document.getElementById("localFallback").style.display = "block", document.getElementById("kotobeeLink").style.display = "none", document.getElementsByTagName("body")[0].style.cssText = "overflow-y: scroll;"
                }, 100), void e()
            } else "chrome-extension:" == window.location.protocol && (chromeApp = !0)
        }
        if (!chromeApp) {
            if ("true" != getVar("continueWithoutHTML5") && !(Modernizr.audio && Modernizr.video && Modernizr.backgroundsize && Modernizr.borderradius && Modernizr.canvas && Modernizr.opacity && Modernizr.localstorage && Modernizr.rgba)) return timeOut(function () {
                document.getElementById("html5Fallback").style.display = "block", document.getElementById("kotobeeLink").style.display = "none", document.getElementsByTagName("body")[0].style.cssText = "overflow-y: scroll;"
            }, 100), void e();
            setVar("continueLocally", "false"), setVar("continueWithoutHTML5", "false")
        }
        e(),
            angular.module("ngIOS9UIWebViewPatch", ["ng"]).config(["$provide", function (e) {
                "use strict";
                e.decorator("$browser", ["$delegate", "$window", function (e, t) {
                    return function (e) {
                        return /(iPhone|iPad|iPod).* OS 9_\d/.test(e) && !/Version\/9\./.test(e)
                    }(t.navigator.userAgent) ? function (e) {
                        function t() {
                            n = null
                        }
                        var n = null,
                            o = e.url;
                        return e.url = function () {
                            return arguments.length ? (n = arguments[0], o.apply(e, arguments)) : n || o.apply(e, arguments)
                        }, window.addEventListener("popstate", t, !1), window.addEventListener("hashchange", t, !1), e
                    }(e) : e
                }])
            }]),
            app = angular.module("reader.Kotobee", ["templates", "ionic", "angular.css.injector", "scormwrapper", "ngIOS9UIWebViewPatch"]),
            ionic.Platform.isFullScreen = !1,
            app.config(["$stateProvider", "$urlRouterProvider", "$ionicConfigProvider", "$compileProvider", function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $compileProvider) {
                (chromeApp || desktop) && (o.imgSrcSanitizationWhitelist(/^\s*((https?|ftp|file|blob|chrome-extension):|data:image\/)/), $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|chrome-extension):/)), $ionicConfigProvider.views.swipeBackEnabled(!1), $urlRouterProvider.otherwise("/loading"), $stateProvider.state("home", {
                    url: "/loading",
                    templateUrl: kTemplate("views/home.html")
                }).state("login", {
                    url: "/login",
                    templateUrl: kTemplate("views/login.html")
                }).state("register", {
                    url: "/register",
                    templateUrl: kTemplate("views/register.html")
                }).state("forgotPwd", {
                    url: "/forgotpwd",
                    templateUrl: kTemplate("views/forgotPwd.html")
                }).state("redeemCode", {
                    url: "/redeemcode",
                    templateUrl: kTemplate("views/redeemCode.html")
                }).state("library", {
                    url: "/library",
                    templateUrl: kTemplate("views/library.html")
                }).state("library_tab", {
                    url: "/library/tab/:tab",
                    templateUrl: kTemplate("views/library.html")
                }).state("reader", {
                    url: "/reader",
                    templateUrl: kTemplate("views/reader.html")
                }).state("reader.chapter", {
                    url: "/chapter/:ch",
                    templateUrl: kTemplate("views/reader.html")
                }).state("reader.chapterhash", {
                    url: "/chapter/:ch/:hash",
                    templateUrl: kTemplate("views/reader.html")
                }).state("libraryreader", {
                    url: "/book/:book/reader",
                    templateUrl: kTemplate("views/reader.html")
                }).state("libraryreader.chapter", {
                    url: "/chapter/:ch",
                    templateUrl: kTemplate("views/reader.html")
                }).state("libraryreader.chapterhash", {
                    url: "/chapter/:ch/:hash",
                    templateUrl: kTemplate("views/reader.html")
                }).state("app", {
                    url: "/app",
                    templateUrl: kTemplate("views/app.html")
                })
            }]),
            putTest("prerun timeout start"),
            window.setTimeout(function () {
                putTest("prerun timeout found!!")
            }, 300),
            app.run(["$ionicPlatform", "$location", "chapters", "library", function ($ionicPlatform, $location, chapters, library) {
                var r = $location.path();
                if (0 == r.indexOf("/book/") && (a = (i = r.split("/book/")[1]).split("/")).length > 0 && (library.startBook = Number(a[0])), r.indexOf("/reader/chapter/") >= 0) {
                    var i = r.split("/reader/chapter/")[1],
                        a = i.split("/");
                    a.length > 0 && (chapters.startChapter = Number(a[0])), a.length > 1 && (chapters.startHash = a[1])
                }
                $location.path("/loading"), $ionicPlatform.ready(function () {
                    try {
                        try {
                            ios || StatusBar && (StatusBar.overlaysWebView(!1), StatusBar.backgroundColorByHexString("#2c1c3c"))
                        } catch (e) { }
                        try {
                            document.addEventListener("deviceready", function () { }, !1)
                        } catch (e) { }
                        try {
                            navigator.splashscreen && setTimeout(function () {
                                navigator.splashscreen.hide()
                            }, 1e3)
                        } catch (e) { }
                        putTest("app 1"), putTest(setTimeout), putTest(window.setTimeout), putTest(window.setTimeout == setTimeout), window.setTimeout(function () {
                            putTest("timeout A")
                        }, 100), putTest("app 1b"), window.setTimeout(function () {
                            putTest("timeout B")
                        }, 300), putTest("app 1c"), setTimeout(function () {
                            putTest("timeout C")
                        }, 600), putTest("app 1d"), window.setTimeout(function () {
                            putTest("timeout D")
                        }, 600), putTest("app 1e"), setTimeout(function () {
                            putTest("app 1a");
                            try {
                                cordova, window.cordova, cordova.plugins, window.cordova.plugins;
                                putTest("app 2")
                            } catch (e) {
                                putTest("app 3")
                            }
                            putTest("app 4"), window.cordova && window.cordova.plugins.Keyboard && cordova.plugins.Keyboard.hideKeyboardAccessoryBar(!0), putTest("app 5"), window.StatusBar && StatusBar.styleDefault(), putTest("app 6"), putTest(angular.element(document.body).scope().start), angular.element(document.body).scope().start(), putTest("app 7")
                        }, native ? 1e3 : 0), putTest("setTimeout called")
                    } catch (e) {
                        putTest("in catch"), putTest(e)
                    }
                })
            }])
    }();
var kInteractive = {
    preRender: function (e, t) {
        t || (t = {});
        var n, o = t.singleElem,
            r = e || document;
        o ? (n = [e], r = document) : n = r.getElementsByClassName("kInteractive"), void 0 !== isKotobee && (kInteractive.absoluteURL = t.chapterUrl ? t.chapterUrl : angular.element(document.body).scope().data.book.chapter.absoluteURL);
        for (var i = n.length; i--;) {
            var a = n[i];
            this.hasClass(a, "gallery") && kInteractive.gallery.preRender(a, r, i), this.hasClass(a, "questions") && kInteractive.questions.preRender(a, r, i), (this.hasClass(a, "image") || "img" == a.nodeName.toLowerCase()) && kInteractive.image.preRender(a, r, i), this.hasClass(a, "link") && kInteractive.link.preRender(a, r, i), this.hasClass(a, "container") && kInteractive.container.preRender(a, r, i)
        }
    },
    postRender: function (e, t) {
        t || (t = {});
        var n = t.singleElem;
        void 0 !== isKotobee && (kInteractive.absoluteURL = t.chapterUrl ? t.chapterUrl : angular.element(document.body).scope().data.book.chapter.absoluteURL), kInteractive.videoIsFullscreen = !1, kInteractive.timestamp = new Date, kInteractive.currentVideo = kInteractive.currentAudio = null, kInteractive.clearAudioVideo(e);
        var o, r = e || document;
        n ? (o = [e], r = document) : o = r.getElementsByClassName("kInteractive");
        for (var i = o.length; i--;) {
            var a = o[i];
            this.hasClass(a, "container") ? kInteractive.container.postRender(r, a, i, t) : this.hasClass(a, "questions") ? kInteractive.questions.postRender(r, a, i, t) : this.hasClass(a, "widget") ? kInteractive.widget.postRender(r, a, i, t) : this.hasClass(a, "video") ? kInteractive.video.postRender(r, a, i) : this.hasClass(a, "audio") ? kInteractive.audio.postRender(r, a, i) : this.hasClass(a, "threed") ? kInteractive.threed.postRender(r, a, i, t) : this.hasClass(a, "gallery") ? kInteractive.gallery.postRender(r, a, i) : this.hasClass(a, "image") && kInteractive.image.postRender(r, a, i)
        }
        this.firstRun && (window.addEventListener("resize", function (t) {
            kInteractive.resizeEvent(e, n)
        }), this.firstRun = !1), kInteractive.resizeEvent(e, n)
    },
    actionEvent: function (e) {
        kInteractive.action(e.target)
    },
    action: function (e, t) {
        if ("undefined" == typeof editorMode || !editorMode) {
            void 0 !== isKotobee && (kInteractive.absoluteURL = angular.element(document.body).scope().data.book.chapter.absoluteURL);
            for (var n = e; !this.hasClass(n, "kInteractive");) {
                if (!n.parentNode) return;
                if ((n = n.parentNode) == document.body) return
            }
            this.hasClass(n, "questions") ? kInteractive.questions.action(n, e) : this.hasClass(n, "widget") ? kInteractive.widget.action(n) : this.hasClass(e, "link") ? kInteractive.link.action(n, e) : this.hasClass(e, "image") ? kInteractive.image.action(n, e) : this.hasClass(n, "gallery") ? kInteractive.gallery.action(n, e) : this.hasClass(n, "audio") ? kInteractive.audio.action(n, e) : this.hasClass(n, "video") ? kInteractive.video.action(n, e) : this.hasClass(n, "threed") && kInteractive.threed.action(n, e)
        }
    },
    trigger: function (e, t) {
        this.action(t, e)
    },
    resizeEvent: function (e, t) {
        var n = e || document;
        t && (n = document);
        for (var o = n.getElementsByClassName("kInteractive"), r = o.length; r--;) {
            var i = o[r];
            this.hasClass(i, "image") ? kInteractive.image.resize(i) : this.hasClass(i, "gallery") ? kInteractive.gallery.resize(i) : this.hasClass(i, "container") ? kInteractive.container.resize(i) : this.hasClass(i, "video") ? kInteractive.video.resize(i) : this.hasClass(i, "widget") && kInteractive.widget.resize(i)
        }
    },
    vQueue: [],
    youTubeReady: !1,
    firstRun: !0
};

var kotobeeListeners = [];
if (void 0 === isKotobee) {
    kotobee = {
        addEventListener: function () {
            if (!(arguments.length < 2)) {
                var e, t = arguments[0],
                    n = {};
                2 == arguments.length ? e = arguments[1] : (n = arguments[1], e = arguments[2]), n.unique && clearKotobeeListeners(t);
                var o = {
                    event: t,
                    cb: e
                };
                kotobeeListeners.push(o)
            }
        },
        dispatchEvent: function (e, t) {
            if (e) {
                var n = getKotobeeListener(e);
                n && n.cb && n.cb.apply(this, [t])
            }
        }
    };

    document.addEventListener("DOMContentLoaded", function (e) {
        "undefined" != typeof kotobeeReady && kotobeeReady(e), document.dispatchEvent(new Event("kotobeeReady")), document.dispatchEvent(new Event("kotobeeChapterLoaded")), kotobee.dispatchEvent("ready"), kotobee.dispatchEvent("chapterLoaded"), document.addEventListener("scroll", function (e) {
            kotobee.dispatchEvent("scrolled", e)
        })
    });

    try {
        document.addEventListener("DOMContentLoaded", kInteractionStart, !1)
    } catch (e) {
        window.addEventListener("load", kInteractionStart, !1)
    }
}

kInteractive.audio = {
    preRender: function (e, t) { },
    postRender: function (e, t, n) {
        var o = e.createDocumentFragment(),
            r = kInteractive.readData(t);
        t.setAttribute("id", "ki-audio-" + n), t.innerHTML = "";
        var i = document.createElement("div");
        i.setAttribute("id", "ki-audio-" + n + "-container"), i.className = "container";
        var a = document.createElement("a");
        a.className = "playBtn ki-btn", a.appendChild(document.createElement("span")), void 0 === isKotobee && a.addEventListener("click", kInteractive.actionEvent);
        var s = document.createElement("div");
        r.style && kInteractive.c.addClass(s, r.style), o.appendChild(a), o.appendChild(i), s.appendChild(o), t.appendChild(s)
    },
    action: function (e) {
        kInteractive.stopCurrentMedia();
        var t = kInteractive.readData(e);
        if (t) {
            t.audioType || (t.audioType = t.type);
            e.getAttribute("id");
            e.getElementsByClassName("playBtn")[0].className = "playBtn ki-btn hide";
            var n = t.src;
            if ("file" == t.audioType && (n = void 0 === isKotobee ? t.audio : t.relToRoot ? ph.join(bookPath, t.audio) : ph.join(kInteractive.absoluteURL, t.audio)), kInteractive.scorm) {
                var o = {};
                o.id = kInteractive.getScormId(e.getAttribute("id"), n), o.description = "Played audio: " + n, o.type = "other", o.learnerResponses = "Played", o.objective = t.options ? t.options.objective : null, o.timestamp = new Date, kInteractive.scorm.setInteractions([o])
            }
            var r = document.createElement("audio");
            r.setAttribute("controls", "true"), r.setAttribute("autoplay", "true"), r.setAttribute("data-tap-disabled", "false");
            var i = document.createElement("source");
            i.src = n, r.appendChild(i), r.appendChild(document.createTextNode("Your browser does not support the audio element")), r.className = "ki-noHighlight", r.oncanplay = function () {
                kInteractive.currentAudio == e && kInteractive.tinCan({
                    verb: "played",
                    activity: "Audio: " + n
                })
            }, e.getElementsByClassName("container")[0].appendChild(r), r.play(), kInteractive.currentAudio = e, kInteractive.c.addClass(kInteractive.currentAudio, "playing")
        }
    }
}

var kInteractiveCommon = {
    checkResponsiveFloat: function (e, t) {
        if (e && e.disableWrapForMobile && "none" != e.float) {
            var n = t.parentNode;
            t.offsetWidth > .6 * n.offsetWidth && .4 * (n.offsetWidth - t.offsetWidth) < 100 ? kInteractive.c.addClass(t, "fullRow") : kInteractive.c.removeClass(t, "fullRow")
        }
    },
    openFrame: function (e) {
        function t() {
            o.style.display = "block", o.className = "show";
            var t = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName("body")[0].clientHeight,
                i = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName("body")[0].clientWidth;
            t -= 20, i -= 20;
            var a = 1;
            if (e.width && e.height) {
                var s = i / e.width,
                    c = t / e.height;
                (a = s < c ? s : c) > 1 && (a = 1)
            }
            var d = "display:block;";
            d += r;
            var u = e.width || e.height ? "-50%,-50%" : "0%,0%";
            d += "-webkit-transform:translate(" + u + ") scale(" + a + ");-moz-transform:translate(" + u + ") scale(" + a + ");transform:translate(" + u + ") scale(" + a + ");", d += "position:fixed;", void 0 === isKotobee && "-50%,-50%" == u && (d += "top:" + Math.round(window.innerHeight / 2) + "px;", d += "left:" + Math.round(window.innerWidth / 2) + "px;"), n.style.cssText = d, n.className = l + " ready";
            var p = n.getElementsByClassName("closeBtn");
            if (p.length) {
                var f = p[0],
                    g = 1 / a,
                    h = "-50%";
                kInteractive.hasClass(f, "side") && (h = "0%"), f.style.cssText = "-webkit-transform: translate(" + h + ",0%) scale(" + g + ");-webkit-transform-origin: 50% 50%;-moz-transform: translate(" + h + ",0%) scale(" + g + ");-moz-transform-origin: 50% 50%;transform: translate(" + h + ",0%) scale(" + g + ");transform-origin: 50% 50%;"
            }
        }
        var n = document.getElementById("kInteractiveFrame"),
            o = document.getElementById("kiDarkOverlay"),
            r = "";
        if (e.dict || (e.dict = {}), e.width && (r += "width:" + e.width + "px;max-width:" + e.width + "px;"), e.height && (r += "height:" + e.height + "px;max-height:" + e.height + "px;"), n || ((o = document.createElement("div")).id = "kiDarkOverlay", document.body.appendChild(o), o.style.display = "none", o.style.position = "fixed", o.addEventListener("click", kInteractive.closeFrame), (n = document.createElement("div")).id = "kInteractiveFrame"), e.pos || (e.pos = "top"), "none" != e.pos) {
            var i = document.createElement("a"),
                a = "closeBtn";
            e.pos && (a += " " + e.pos), i.className = a;
            var s = e.dict.close ? e.dict.close : "Close";
            try {
                s = angular.element(document.body).scope().translit("close")
            } catch (e) { }
            "side" != e.pos && (i.innerHTML = s), i.addEventListener("click", kInteractive.closeFrame), n.appendChild(i)
        }
        var l = e.class ? e.class : "";
        (e.width || e.height) && (l += " fixed"), n.style.display = "block", n.className = l, document.body.appendChild(n), kInteractive.frameIsOpen = !0, kInteractive.frameOb = e, kInteractive.openFrame.resized = t, window.addEventListener("resize", t), setTimeout(function () {
            t(), e.cb && e.cb(n)
        }, 100)
    },
    closeAlert: function (e) {
        e && e.stopPropagation();
        var t = document.getElementById("kiSystemAlertBox"),
            n = document.getElementById("kiSystemAlertBackdrop");
        t.parentNode.removeChild(t), n.parentNode.removeChild(n), kInteractive.alertIsOpen = !1, window.removeEventListener("resize", kInteractive.alert.resized), kInteractive.alert.resized = null
    },
    closeFrame: function () {
        var e = document.getElementById("kInteractiveFrame"),
            t = document.getElementById("kiDarkOverlay");
        kInteractive.frameIsOpen = !1, window.removeEventListener("resize", kInteractive.openFrame.resized), kInteractive.openFrame.resized = null, kInteractive.c.removeClass(e, "ready");
        var n = kInteractive.frameOb,
            o = "";
        n.width && (o += "width:" + n.width + "px;max-width:" + n.width + "px;"), n.height && (o += "height:" + n.height + "px;max-height:" + n.height + "px;"), e.style.cssText = o, t.className = "", setTimeout(function () {
            e.innerHTML = "", e.style.display = t.style.display = "none", n.closed && n.closed(), kInteractive.frameOb = null
        }, 300)
    },
    stopCurrentMedia: function () {
        if (kInteractive.currentVideo) {
            var e = kInteractive.currentVideo.getAttribute("id") + "-container";
            (t = document.getElementById(e)) && (t.parentNode.removeChild(t), (n = document.createElement("div")).setAttribute("id", e), n.className = "container", (o = kInteractive.currentVideo.getElementsByClassName("playBtn")[0]).className = "playBtn ki-btn", kInteractive.c.removeClass(kInteractive.currentVideo, "playing"), kInteractive.currentVideo.appendChild(n))
        }
        if (kInteractive.currentAudio)
            if (e = kInteractive.currentAudio.getAttribute("id")) {
                e += "-container";
                var t = document.getElementById(e);
                if (t) {
                    t.parentNode.removeChild(t);
                    var n = document.createElement("div");
                    n.setAttribute("id", e), n.className = "container";
                    var o = kInteractive.currentAudio.getElementsByClassName("playBtn")[0];
                    o.className = "playBtn ki-btn", kInteractive.c.removeClass(kInteractive.currentAudio, "playing"), kInteractive.currentAudio.appendChild(n)
                }
            } else try {
                var r = kInteractive.currentAudio.getElementsByTagName("audio");
                r.length && (r[0].pause(), r[0].src = ""), kInteractive.currentAudio.pause && (kInteractive.currentAudio.pause(), kInteractive.currentAudio.src = "");
                for (var i = document.getElementsByClassName("kiAudioLoader"), a = i.length; a--;) i[a].parentNode.removeChild(i[a])
            } catch (e) { }
    },
    hasClass: function (e, t) {
        if (e && e.className)
            for (var n = e.className.trim().split(" "), o = 0; o < n.length; o++)
                if (n[o] && n[o].trim() == t) return !0
    },
    appendAfterDelay: function (e, t) {
        setTimeout(function () {
            e.appendChild(t)
        }, 30)
    },
    replaceHTML: function (e, t) {
        var n = "string" == typeof e ? document.getElementById(e) : e,
            o = n.cloneNode(!1);
        return o.innerHTML = t, n.parentNode.replaceChild(o, n), o
    },
    tinCan: function (e) {
        if (void 0 !== isKotobee) try {
            angular.element(document.body).scope().tinCanShortcut(e)
        } catch (e) { }
    },
    setDOMParser: function () {
        ! function (e) {
            "use strict";
            var t = e.prototype,
                n = t.parseFromString;
            try {
                if ((new e).parseFromString("", "text/html")) return
            } catch (e) { }
            t.parseFromString = function (e, t) {
                if (/^\s*text\/html\s*(?:;|$)/i.test(t)) {
                    var o, r = document.implementation.createHTMLDocument(""),
                        i = r.documentElement;
                    return i.innerHTML = e, o = i.firstElementChild, 1 === i.childElementCount && "html" === o.localName.toLowerCase() && r.replaceChild(o, i), r
                }
                return n.apply(this, arguments)
            }
        }(DOMParser)
    },
    readData: function (e) {
        try {
            var t = e.getAttribute("data-kotobee");
            return t || (t = e.getAttribute("data")), JSON.parse(decodeURI(kInteractive.XORCipher().decode("kotobee%%author", t)))
        } catch (e) {
            try {
                return JSON.parse(kInteractive.XORCipher().decode("kotobee%%author", t))
            } catch (e) { }
        }
    },
    writeData: function (e, t) {
        var n = encodeURI(JSON.stringify(t)),
            o = e.hasAttribute("data-kotobee") ? "data-kotobee" : "data";
        e.setAttribute(o, kInteractive.XORCipher().encode("kotobee%%author", n))
    },
    XORCipher: function () {
        function e(e) {
            var t, n, o, r, i, s, l = 0,
                c = "";
            if (!e) return e;
            do {
                t = (i = e[l++] << 16 | e[l++] << 8 | e[l++]) >> 18 & 63, n = i >> 12 & 63, o = i >> 6 & 63, r = 63 & i, c += a.charAt(t) + a.charAt(n) + a.charAt(o) + a.charAt(r)
            } while (l < e.length);
            return ((s = e.length % 3) ? c.slice(0, s - 3) : c) + "===".slice(s || 3)
        }

        function t(e) {
            var t, n, o, r, i, s, l = 0,
                c = [];
            if (!e) return e;
            e += "";
            do {
                t = (s = a.indexOf(e.charAt(l++)) << 18 | a.indexOf(e.charAt(l++)) << 12 | (r = a.indexOf(e.charAt(l++))) << 6 | (i = a.indexOf(e.charAt(l++)))) >> 16 & 255, n = s >> 8 & 255, o = 255 & s, c.push(t), 64 !== r && (c.push(n), 64 !== i && c.push(o))
            } while (l < e.length);
            return c
        }

        function n(e, t) {
            return e.charCodeAt(Math.floor(t % e.length))
        }

        function o(e, t) {
            return i(t, function (t, o) {
                return t.charCodeAt(0) ^ n(e, o)
            })
        }

        function r(e, t) {
            return i(t, function (t, o) {
                return String.fromCharCode(t ^ n(e, o))
            }).join("")
        }

        function i(e, t) {
            for (var n = [], o = 0; o < e.length; o++) n[o] = t(e[o], o);
            return n
        }
        var a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        return {
            encode: function (t, n) {
                return n = o(t, n), e(n)
            },
            decode: function (e, n) {
                return n = t(n), r(e, n)
            }
        }
    },
    getScormId: function (e, t) {
        return t = t ? "-" + t.replace(/[^a-zA-Z0-9]/g, "-").substr(0, 80) : "", e + t
    },
    confirm: function (e) {
        kInteractive.alert(e)
    },
    alert: function (e) {
        function t() {
            void 0 === isKotobee && (n.style.top = Math.round(window.innerHeight / 2) + "px", n.style.left = Math.round(window.innerWidth / 2) + "px")
        }
        if (e) {
            var n = document.createElement("div");
            if (n.setAttribute("id", "kiSystemAlertBox"), e.raw && (n.className = "raw"), n.style.position = "fixed", void 0 === isKotobee && (n.style.top = Math.round(window.innerHeight / 2) + "px", n.style.left = Math.round(window.innerWidth / 2) + "px"), e.raw) n.innerHTML = e.content.replace("&nbsp;", "&#160;");
            else {
                var o = document.createElement("div");
                o.innerHTML = e.content.replace("&nbsp;", "&#160;"), o.className = "content", n.appendChild(o)
            }
            if (e.title) {
                var r = document.createElement("div");
                r.innerHTML = e.title.replace("&nbsp;", "&#160;"), r.className = "header", n.insertBefore(r, n.firstChild)
            }
            kInteractive.alertIsOpen = !0;
            var i = document.createElement("a");
            if (!e.raw) {
                var a = document.createElement("div");
                a.className = "footer";
                var s = "OK";
                try {
                    s = angular.element(document.body).scope().translit("ok")
                } catch (e) { }
                if (e.okBtn && (s = e.okBtn), i.innerHTML = s.replace("&nbsp;", "&#160;"), i.className = "okBtn", i.addEventListener("click", kInteractive.closeAlert), e.cb && i.addEventListener("click", e.cb), a.appendChild(i), e.noBtn) {
                    var l = document.createElement("a");
                    l.innerHTML = e.noBtn, l.className = "cancelBtn", l.addEventListener("click", kInteractive.closeAlert), a.appendChild(l)
                }
                n.appendChild(a)
            }
            var c = document.createElement("div");
            e.noBackdrop || (c.setAttribute("id", "kiSystemAlertBackdrop"), c.addEventListener("click", kInteractive.closeAlert), c.style.position = "fixed", document.body.appendChild(c)), document.body.appendChild(n), i.focus(), kInteractive.alert.resized = t, window.addEventListener("resize", t), setTimeout(function () {
                kInteractive.c.addClass(n, "show"), kInteractive.c.addClass(c, "show")
            }, 50)
        }
    },
    c: {
        addClass: function (e, t) {
            var n = e.className.trim();
            if (t instanceof Array) {
                for (var o = !0, r = 0; r < t.length; r++) - 1 == n.indexOf(t[r]) && (o = !1, n += ("" == n ? "" : " ") + t[r]);
                if (o) return
            } else {
                if (n.indexOf(t) >= 0) return;
                n += ("" == n ? "" : " ") + t
            }
            e.className = n
        },
        toggleClass: function (e, t) {
            this.strHasClass(e.className, t) ? this.removeClass(e, t) : this.addClass(e, t)
        },
        hasClass: function (e, t) {
            return this.strHasClass(e.className, t)
        },
        strHasClass: function (e, t) {
            e || (e = "");
            for (var n = e.trim().split(" "), o = 0; o < n.length; o++)
                if (n[o] == t) return !0;
            return !1
        },
        removeClass: function (e, t) {
            var n = e.className;
            if (t instanceof Array) {
                for (var o = !0, r = 0; r < t.length; r++) n.indexOf(t[r]) >= 0 && (o = !1, n = n.replace(t[r], ""));
                if (o) return
            } else {
                if (-1 == n.indexOf(t)) return;
                n = (n = n.replace(t, "")).replace(/ {2,}?/g, " ")
            }
            "" == (n = n.trim()) ? e.removeAttribute("class") : e.className = n
        },
        removeHash: function (e) {
            return -1 == e.indexOf("#") ? e : e.substring(0, e.lastIndexOf("#"))
        },
        removeFilename: function (e) {
            var t = kInteractive.c.normalizedArray(e);
            return -1 == t[t.length - 1].indexOf(".") ? e : (t.splice(t.length - 1, 1), t.join("/"))
        },
        normalizedArray: function (e) {
            for (var t = (e = e.replace(/\\/g, "/")).split("/"), n = t.length; n--;) t[n] ? ".." == t[n] && n > 0 && ".." != t[n - 1] && (t.splice(n, 1), t.splice(n - 1, 1)) : t.splice(n, 1);
            return t[0].match(/http[s]?:$/g) && (t[0] += "//" + t[1], t.splice(1, 1)), "file:" == t[0] && (t[0] += "///" + t[1], t.splice(1, 1)), t
        },
        shuffleArray: function (e) {
            e.sort(function () {
                return Math.random() - .5
            })
        },
        allowDrop: function (e) {
            e.preventDefault()
        },
        drag: function (e) {
            e.dataTransfer && e.dataTransfer.setData("text", e.target.id), dragged = e.target
        },
        drop: function (e) {
            e.preventDefault(), e.target.hasAttribute("ondragover") && e.target.appendChild(e.view.dragged)
        }
    },
    escapeHtml: function (e) {
        var t = {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#039;"
        };
        return e.replace(/[&<>"']/g, function (e) {
            return t[e]
        })
    },
    getChildIndex: function (e) {
        for (var t = 0; null != (e = e.previousSibling);) t++;
        return t
    },
    isAbsolutePath: function (e) {
        if (e) return 0 == e.trim().indexOf("http://") || (0 == e.trim().indexOf("https://") || (0 == e.trim().indexOf("//") || (0 == e.trim().indexOf("data:") || void 0)))
    },
    isMobile: function (e) {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) return !0;
        try {
            if (window.location.href.indexOf("?preview") > 0) {
                var t = window.location.href.split("?");
                t = t[1].split("&");
                for (var n = {}, o = 0; o < t.length; o++) {
                    var r = t[o].split("=");
                    n[r[0]] = r[1]
                }
                if (n.mobile) return !0
            }
        } catch (e) { }
    },
    closeFullscreenVideo: function () {
        kInteractive.currentVideo && (kInteractive.currentVideo.webkitExitFullscreen ? kInteractive.currentVideo.webkitExitFullscreen() : kInteractive.currentVideo.mozCancelFullScreen ? kInteractive.currentVideo.mozCancelFullScreen() : document.webkitExitFullscreen ? document.webkitExitFullscreen() : document.mozCancelFullScreen && document.mozCancelFullScreen(), kInteractive.videoIsFullscreen = !1)
    },
    getWidgetHome: function (e, t) {
        return t || (t = {}), e.path ? void 0 === isKotobee ? e.path : ph.join(angular.element(document.body).scope().data.book.chapter.absoluteURL, e.path) : void 0 === isKotobee ? "../js/widgets/" : bookPath + "EPUB/js/widgets/"
    },
    getWidgetUrl: function (e, t) {
        var n = kInteractive.getWidgetHome(e, t) + "/" + e.name + "/" + e.src;
        if (void 0 !== isKotobee) {
            var o = angular.element(document.body).scope().data.user;
            o && o.email && o.signatures && o.signatures.bookwidgets && e.id && 0 == e.id.indexOf("com.kidimedia") && (n += "?oauth_signature=" + o.signatures.bookwidgets.signature, n += "&oauth_nonce=" + o.signatures.bookwidgets.nonce, n += "&oauth_timestamp=" + o.signatures.bookwidgets.timestamp, n += "&oauth_consumer_key=" + o.signatures.bookwidgets.key, n += "&lti_version=LTI-1p0", n += "&oauth_signature_method=HMAC-SHA1", n += "&oauth_version=1.0", n += "&tool_consumer_info_product_family_code=kotobee", n += "&lis_person_contact_email_primary=" + o.email, n += "&lis_person_name_full=" + (o.name ? o.name : ""), n += "&user_id=" + o.id, n += "&lti_message_type=basic-lti-launch-request", angular.element(document.body).scope().refreshSignatures && angular.element(document.body).scope().refreshSignatures())
        }
        return n
    },
    clearAudioVideo: function (e) {
        var t = e || document;
        kInteractive.stopCurrentMedia();
        for (var n = t.getElementsByTagName("audio"), o = n.length; o--;) try {
            if (n[o].hasAttribute("data-dontclose")) continue;
            n[o].pause(), n[o].children.length || (n[o].src = "")
        } catch (e) { }
        for (var r = t.getElementsByTagName("video"), o = r.length; o--;) r[o].pause(), r[o].children.length || (r[o].src = "")
    }
};
for (var item in kInteractiveCommon)
    kInteractive[item] = kInteractiveCommon[item];

kInteractive.container = {
    preRender: function (e, t) {
        kInteractive.readData(e)
    },
    postRender: function (e, t, n) {
        var o = kInteractive.readData(t);
        if (o && (kInteractive.isMobile() && "cScroll" == o.type && void 0 !== isKotobee && angular.element(document.body).scope().applyNativeScroll(t), o.path)) {
            var r = o.path;
            void 0 !== isKotobee && (r = ph.join(kInteractive.absoluteURL, o.path));
            var i = t.getAttribute("style");
            t.setAttribute("style", i + ' background-image:url("' + r + '");')
        }
    },
    action: function (e) { },
    resize: function () {
        var e = kInteractive.readData(item);
        e && kInteractive.checkResponsiveFloat(e, item)
    }
};

kInteractive.gallery = {
    preRender: function (e, t) {
        var n = t.createDocumentFragment(),
            o = kInteractive.readData(e);
        if (o) {
            e.innerHTML = "";
            var r = document.createElement("div");
            r.className = "imgMask";
            var i = document.createElement("div");
            i.className = "images " + o.scale, r.appendChild(i);
            for (var a = 0; a < o.imgs.length; a++) {
                var s = document.createElement("div");
                s.className = "imgContainer" + (a ? "" : " selected");
                var l = void 0 === isKotobee ? o.imgs[a].path : ph.join(kInteractive.absoluteURL, o.imgs[a].path);
                s.setAttribute("style", "background-color:" + o.bgColor + ";background-image:url('" + l + "')" + (a ? "" : ";display:block")), o.imgs[a].caption && s.setAttribute("data-caption", o.imgs[a].caption), i.appendChild(s)
            }
            n.appendChild(r);
            var c = document.createElement("div");
            c.className = "imgCaption";
            var d = document.createElement("div");
            d.className = "inner", d.style.display = "none", o.imgs[0] && o.imgs[0].caption && (d.innerHTML = o.imgs[0].caption, d.style.display = null), c.appendChild(d), n.appendChild(c);
            var u = document.createElement("a");
            u.className = "next btn ki-btn", 1 == o.imgs.length && (u.className += " disable"), n.appendChild(u);
            var p = document.createElement("a");
            p.className = "prev btn ki-btn disable", n.appendChild(p), void 0 === isKotobee && (u.addEventListener("click", kInteractive.actionEvent), p.addEventListener("click", kInteractive.actionEvent));
            var f = document.createElement("div");
            o.style && kInteractive.c.addClass(f, o.style), f.appendChild(n), e.appendChild(f)
        }
    },
    postRender: function (e, t, n) { },
    action: function (e, t) {
        var n = e.getElementsByClassName("images")[0],
            o = (n.children.length, e.getElementsByClassName("selected")[0]),
            r = Number(o.style.left.slice(0, -2)),
            i = Number(n.style.left.slice(0, -2));
        if (kInteractive.hasClass(t, "next")) {
            var a = o.nextSibling;
            if (!a) return;
            kInteractive.c.removeClass(o, "selected"), kInteractive.c.addClass(a, "selected"), a.style.left = r + n.offsetWidth + "px", a.style.display = "block", n.style.left = i - n.offsetWidth + "px", a.nextSibling ? kInteractive.c.removeClass(t, "disable") : kInteractive.c.addClass(t, "disable"), kInteractive.c.removeClass(e.getElementsByClassName("prev")[0], "disable"), o = a
        } else if (kInteractive.hasClass(t, "prev")) {
            var s = o.previousSibling;
            if (!s) return;
            kInteractive.c.removeClass(o, "selected"), kInteractive.c.addClass(s, "selected"), s.style.left = r - n.offsetWidth + "px", s.style.display = "block", n.style.left = i + n.offsetWidth + "px", s.previousSibling ? kInteractive.c.removeClass(t, "disable") : kInteractive.c.addClass(t, "disable"), kInteractive.c.removeClass(e.getElementsByClassName("next")[0], "disable"), o = s
        }
        var l = e.getElementsByClassName("imgCaption");
        if (l.length) {
            var c = (l = l[0]).getElementsByClassName("inner");
            c.length && ((c = c[0]).style.display = "none", o.hasAttribute("data-caption") && (c.innerHTML = o.getAttribute("data-caption"), c.style.display = null))
        }
    },
    resize: function (e) {
        var t = kInteractive.readData(e);
        t && kInteractive.checkResponsiveFloat(t, e)
    }
};
kInteractive.image = {
    preRender: function (e, t) {
        var n = kInteractive.readData(e);
        n && (n.style && kInteractive.c.addClass(e, n.style), e.parentNode && kInteractive.hasClass(e.parentNode, "link") || void 0 === isKotobee && e.addEventListener("click", kInteractive.actionEvent))
    },
    postRender: function (e, t, n) {
        var o = kInteractive.readData(t);
        o && o && (o.behavior && "none" != o.behavior || (t.style.transition = t.style.webkitTransition = t.style.mozTransition = "none"))
    },
    action: function (e, t, n) {
        function o(e, t) {
            e.style.webkitAnimation = e.style.mozAnimation = e.style.animation = t
        }

        function r(e, t) {
            e.style.webkitAnimationDuration = e.style.mozAnimationDuration = e.style.animationDuration = t
        }

        function i(e, t) {
            e.style.webkitAnimationIterationCount = e.style.mozAnimationIterationCount = e.style.animationIterationCount = t
        }
        if (t.parentNode && kInteractive.hasClass(t.parentNode, "kInteractive") && kInteractive.hasClass(t.parentNode, "link")) kInteractive.action(t.parentNode, event);
        else {
            var a = kInteractive.readData(t);
            if (a) {
                var s = a.behavior;
                if ("none" != s) o(t, "none"), r(t, "none"), i(t, "none"), setTimeout(function () {
                    function e(a) {
                        o(t, "none"), r(t, "none"), i(t, "none"), t.removeEventListener(n, e)
                    }
                    "wiggle" == s ? (o(t, "wiggle"), r(t, "0.5s"), i(t, "1")) : "jump" == s ? (o(t, "jump"), r(t, "0.7s"), i(t, "1")) : "scale" == s && (o(t, "scale"), r(t, "0.5s"), i(t, "1"));
                    var n = function () {
                        var e, t = document.createElement("fakeelement"),
                            n = {
                                animation: "animationend",
                                OAnimation: "oAnimationEnd",
                                MozAnimation: "animationend",
                                WebkitAnimation: "webkitAnimationEnd"
                            };
                        for (e in n)
                            if (void 0 !== t.style[e]) return n[e]
                    }();
                    n && t.addEventListener(n, e)
                }),
                    kInteractive.tinCan({
                        verb: "clicked",
                        activity: s + "image: " + t.getAttribute("src")
                    });
                else if (a.popup) {
                    var l = {};
                    l.class = "image";
                    var c = document.createElement("img");
                    c.src = a.path, kInteractive.isAbsolutePath(a.path) || void 0 !== isKotobee && (c.src = ph.join(kInteractive.absoluteURL, a.path)), l.width = c.naturalWidth, l.height = c.naturalHeight, l.pos = "side", l.cb = function (e) {
                        e.appendChild(c)
                    }, l.closed = function () { }, kInteractive.openFrame(l)
                }
            }
        }
    },
    resize: function (e) {
        var t = kInteractive.readData(e);
        t && kInteractive.checkResponsiveFloat(t, e)
    }
};
kInteractive.link = {
    preRender: function (e, t) {
        var n = kInteractive.readData(e);
        if (n && ("undefined" == typeof editorMode || !editorMode)) {
            if ("div" == e.nodeName.toLowerCase()) {
                var o = document.createElement("a");
                o.setAttribute("href", "popup" == n.type ? "" : e.getAttribute("href")), o.setAttribute("target", n.target), o.setAttribute("style", e.getAttribute("style")), o.setAttribute("data-kotobee", e.getAttribute("data-kotobee")), o.className = e.className, o.id = e.id ? e.id : n.id, kInteractive.c.addClass(o, "btn"), o.style.opacity = Number(n.transparency) / 100, e.parentNode.replaceChild(o, e), e = o
            }
            void 0 === isKotobee && ("popup" != n.type && "audio" != n.type && "action" != n.type || e.setAttribute("onclick", "return false;"), e.addEventListener("click", kInteractive.actionEvent))
        }
    },
    postRender: function (e, t, n) { },
    action: function (e, t, n) {
        function o() {
            v || (kInteractive.c.addClass(y, "hide"), v = !0)
        }
        var r = kInteractive.readData(t);
        if (r) {
            if ("popup" == r.type) {
                var i = "<div class='kbAlertScroll'>" + r.msg + "</div>";
                if (i = i.replace(/src="(.*?)"/g, function () {
                    var e = arguments[1];
                    return void 0 !== isKotobee && (e = ph.join(kInteractive.absoluteURL, e)), 'src="' + e + '"'
                }), "image" == r.popupMode) {
                    var a = r.popupImg;
                    void 0 !== isKotobee && (a = ph.join(kInteractive.absoluteURL, a)), i = "<img src='" + a + "'/>"
                }
                kInteractive.alert({
                    content: i
                }), kInteractive.scorm && setTimeout(function () {
                    var e = {};
                    e.id = kInteractive.getScormId("popup", r.msg), e.description = "Shown popup message: " + r.msg, e.type = "other", e.learnerResponses = "Shown", e.timestamp = new Date, kInteractive.scorm.setInteractions([e])
                }, 800)
            } else if ("action" == r.type) {
                var s = r.asset,
                    l = r.action;
                if (s.remote) {
                    if ("widget" == s.type) {
                        var c = document.createElement("div");
                        c.className = "kInteractive widget kbHidden", c.setAttribute("data-kotobee", s.data), document.body.appendChild(c), kInteractive.trigger("click", c)
                    }
                    return
                }
                for (var d = document.getElementsByClassName(s.classname), u = 0; u < d.length; u++)
                    if (kInteractive.hasClass(d[u], "kInteractive") && kInteractive.hasClass(d[u], s.type))
                        if ("activate" == l)
                            if ("gallery" == s.type) {
                                var p = d[u].getElementsByClassName("next");
                                kInteractive.trigger("click", p[0])
                            } else if ("questions" == s.type) {
                                for (var f = d[u].getElementsByClassName("ki-btn"), g = 0; g < f.length; g++)
                                    if ("submit" == f[g].type) {
                                        kInteractive.trigger("click", f[g]);
                                        break
                                    }
                            } else kInteractive.trigger("click", d[u]);
                        else "visibility" == l && kInteractive.c.toggleClass(d[u], "kbHidden")
            } else if ("audio" == r.type) {
                kInteractive.stopCurrentMedia();
                var h = document.createElement("audio"),
                    m = r.src;
                "file" == r.audioType && (m = void 0 === isKotobee ? r.audio : ph.join(kInteractive.absoluteURL, r.audio)), h.setAttribute("src", m);
                var v, b = document.createElement("div");
                b.className = "kiAudioLoader", b.style.transform = b.style.webkitTransform = b.style.mozTransform = "scale(1)";
                var y = document.createElement("div");
                if (y.className = "kiAudioSpinner", b.appendChild(y), t.parentNode.insertBefore(b, t), h.onplaying = o, setTimeout(o, 5e3), h.play(), kInteractive.currentAudio = h, kInteractive.tinCan({
                    verb: "played",
                    activity: "Audio: " + m
                }), kInteractive.scorm) {
                    var k = {};
                    k.id = kInteractive.getScormId(t.textContent, m), k.description = "Played audio link: " + m, k.type = "other", k.learnerResponses = "Played", k.objective = r.options ? r.options.objective : null, k.timestamp = new Date, kInteractive.scorm.setInteractions([k])
                }
            } else if ("attachment" == r.type) {
                var w = r.file;
                void 0 !== isKotobee && (w = ph.join(kInteractive.absoluteURL, r.file));
                var C = document.createElement("a");
                document.body.appendChild(C), C.setAttribute("style", "display: none"), C.href = w, C.target = "_blank", C.download = r.file.split("/")[r.file.split("/").length - 1], C.setAttribute("onclick", "");
                var x = document.createEvent("MouseEvents");
                x.initMouseEvent("click"), C.dispatchEvent(x), C.remove()
            }
            return !1
        }
    }
};

kInteractive.questions = {
    preRender: function (e, t, n, o) {
        var r = t.createDocumentFragment(),
            i = kInteractive.readData(e);
        if (i) {
            var a = i.dict,
                s = i.userDict;
            a || (a = {}), s || (s = a), i.options || (i.options = {}), e.id || (e.id = "ki-qs-" + n + "-" + Math.ceil(1e3 * Math.random()));
            if (e.innerHTML = "", i.layout && i.layout.popup && !kInteractive.hasClass(e, "inpopup")) {
                var l = document.createElement("img"),
                    c = i.layout.popupImg;
                return l.src = c, r.appendChild(l), void 0 === isKotobee && l.addEventListener("click", kInteractive.actionEvent), void e.appendChild(r)
            }
            if (i.options.randomize) {
                for (h = 0; h < i.q.length; h++) i.q[h].origin = h;
                for (var d, u, p = i.q.length; 0 !== p;) u = Math.floor(Math.random() * p), p -= 1, d = i.q[p], i.q[p] = i.q[u], i.q[u] = d
            }
            if (i.options.questionsDisplayed) {
                var f = Number(i.options.questionsDisplayed);
                i.q = i.q.slice(0, f)
            }
            if (i.title) {
                var g = document.createElement("span");
                g.innerHTML = "<h4>" + kInteractive.escapeHtml(i.title) + "</h4>", r.appendChild(g)
            }
            for (var h = 0; h < i.q.length; h++) {
                var m = i.q[h],
                    v = document.createElement("div");
                if (v.className = "ques", null != m.origin && v.setAttribute("data-o", m.origin), v.innerHTML = "<p><strong>" + (i.options.numbering ? "<span class='no'>" + (h + 1) + "</span>" : "") + kInteractive.escapeHtml(" " + m.q) + "</strong></p>", m.path) {
                    var b = document.createElement("img");
                    b.src = m.path, v.appendChild(b)
                }
                if (v.classList.add(m.type), "sa" == m.type) {
                    var y = m.lines ? m.lines : 1,
                        k = document.createElement(y > 1 ? "textarea" : "input");
                    k.name = "sa-" + h, 1 == y ? k.type = "text" : k.rows = m.lines ? m.lines : 1, k.className = "ans txtfield";
                    x = "ki-sa-" + n + "-" + h;
                    k.id = x, document[e.id] && document[e.id].preserve && document[e.id].q && document[e.id].q[h] && (k.value = document[e.id].q[h]), v.appendChild(k)
                } else if ("mcq" == m.type || "mmcq" == m.type)
                    for (var w = m.c, C = 0; C < w.length; C++) {
                        var x = "ki-mcq-" + n + "-" + h + "-" + C,
                            I = document.createElement("input");
                        "mmcq" == m.type ? (I.name = "name", I.type = "checkbox") : (I.name = "mcq-" + h, I.type = "radio"), I.className = "ans", I.id = x, (_ = document.createElement("label")).htmlFor = x, _.innerHTML = " " + kInteractive.escapeHtml(w[C].t) + "<br/>", _.className = "ki-noHighlight", (H = document.createElement("div")).className = "answer", document[e.id] && document[e.id].preserve && document[e.id].q && document[e.id].q[h] && (I.checked = document[e.id].q[h][C], i.options && i.options.highlightCorrect && i.q[h].c[C].a && kInteractive.c.addClass(H, "correct")), H.appendChild(I), H.appendChild(_), v.appendChild(H)
                    } else if ("tf" == m.type)
                    for (C = 0; C < 2; C++) {
                        var x = "ki-tf-" + n + "-" + h + "-" + C,
                            E = document.createElement("input");
                        E.type = "radio", E.name = "tf-" + h, E.className = "ans", E.id = x, (_ = document.createElement("label")).htmlFor = x, m.choice1 && m.choice2 ? _.innerHTML = kInteractive.escapeHtml(" " + (0 == C ? m.choice1 : m.choice2)) : _.innerHTML = " " + (0 == C ? "True" : "False"), _.className = "ki-noHighlight";
                        H = document.createElement("div");
                        document[e.id] && document[e.id].preserve && document[e.id].q && document[e.id].q[h] && (E.checked = document[e.id].q[h][C], i.options && i.options.highlightCorrect && (0 == C && i.q[h].a || 1 == C && !i.q[h].a) && kInteractive.c.addClass(H, "correct")), H.appendChild(E), H.appendChild(_), v.appendChild(H)
                    } else if ("dd" == m.type) {
                        var T = document.createElement("div");
                        T.className = "categoryContainer";
                        for (var O = [], C = 0; C < m.k.length; C++) {
                            var S = m.k[C],
                                A = C,
                                L = document.createElement("div");
                            L.className = "dCategory ki-noHighlight ki-dd-category-" + A, L.setAttribute("ondragover", "kInteractive.c.allowDrop(event)"), L.setAttribute("ondrop", "kInteractive.c.drop(event)");
                            var B = document.createElement("div");
                            B.innerHTML = S.category.name, B.className = "qTitle", L.appendChild(B);
                            for (var N = 0; N < S.category.answers.length; N++) O.push(S.category.answers[N].t);
                            T.appendChild(L)
                        }
                        v.appendChild(T);
                        var M = document.createElement("div");
                        M.className = "ki-dd-clearfix", v.appendChild(M);
                        var P = document.createElement("div");
                        P.setAttribute("ondragover", "kInteractive.c.allowDrop(event)"), P.setAttribute("ondrop", "kInteractive.c.drop(event)"), P.className = "ki-dd-answers ki-noHighlight";
                        for (var $ = [], C = 0; C < O.length; C++) {
                            var A = C,
                                H = O[C],
                                R = document.createElement("div"),
                                x = "ki-dd-" + n + "-" + h + "-" + A;
                            R.id = x, R.innerHTML = "<span></span>", R.children[0].innerText = H, R.className = "ans ki-noHighlight", R.draggable = !0, R.setAttribute("ondragstart", "kInteractive.c.drag(event)"), $.push(R)
                        }
                        kInteractive.c.shuffleArray($);
                        for (C = 0; C < $.length; C++) P.appendChild($[C]);
                        v.appendChild(P)
                    }
                if ((K = document.createElement("p")).className = "separator", v.appendChild(K), document[e.id] && document[e.id].preserve && document[e.id].q && document[e.id].q[h]) {
                    if (i.q[h].exp && !v.getElementsByClassName("explanation").length) {
                        var D = document.createElement("p");
                        D.className = "explanation", D.innerHTML = kInteractive.escapeHtml(i.q[h].exp), kInteractive.appendAfterDelay(v, D)
                    }
                    if (i.q[h].ref && !v.getElementsByClassName("reference").length) {
                        var F = document.createElement("a");
                        F.className = "reference", F.innerHTML = s.learnMode ? s.learnMode : "Learn more", F.setAttribute("href", kInteractive.escapeHtml(i.q[h].ref)), F.setAttribute("target", "_blank"), kInteractive.appendAfterDelay(v, F)
                    }
                }
                r.appendChild(v)
            }
            if (i.options.emailForm) {
                var z = document.createElement("div");
                z.setAttribute("class", "emailForm"),
                    function (e, t, n) {
                        var o = document.createElement("div");
                        o.className = "address", o.innerHTML = "<p><strong>" + e + "</strong><em>" + t + "</em></p>";
                        var r = document.createElement("input");
                        r.name = n, r.type = "text", r.className = "txtfield", o.appendChild(r);
                        var i = document.createElement("p");
                        i.className = "separator", o.appendChild(i), z.appendChild(o)
                    }(s.recipientEmail ? s.recipientEmail : "Recipient email", s.separateEmails ? s.separateEmails : "Separate multiple emails with commas", "recipientEmail"), r.appendChild(z)
            }
            var j = null;
            if ("none" != i.action) {
                var U = "ki-" + n + "-btn";
                (j = document.createElement("input")).type = "submit", j.value = s.submit ? s.submit : "Submit Answers", j.id = U, j.className = "btn ki-btn ki-mcq"
            }
            var V = null,
                q = null;
            if (i.options && (i.options.clearAnswers && ((V = document.createElement("input")).type = "submit", V.value = s.clear ? s.clear : "Clear Answers", V.className = "btn ki-btn ki-questions questions-clear", void 0 === isKotobee && V.addEventListener("click", kInteractive.actionEvent)), i.options.addToNotebook && ((V = document.createElement("input")).type = "submit", V.value = s.notebook ? s.notebook : "Save to Notebook", V.className = "btn ki-btn ki-questions questions-notebook", void 0 === isKotobee && V.addEventListener("click", kInteractive.actionEvent)), i.options.preserveAnswers)) {
                (q = document.createElement("div")).style.marginTop = "5px";
                var W = document.createElement("input");
                W.type = "checkbox", W.name = "preserveAnswers", W.className = "ki-btn ki-questions questions-preserve", W.id = "ki-" + n + "-preserveAnswersBtn", document[e.id] && (W.checked = document[e.id].preserve), q.appendChild(W);
                var _ = document.createElement("label");
                _.htmlFor = W.id, _.innerHTML = " " + (s.preserve ? s.preserve : "Preserve submitted answers"), _.className = "ki-noHighlight", q.appendChild(_), void 0 === isKotobee && W.addEventListener("change", kInteractive.actionEvent)
            }
            void 0 === isKotobee && j && j.addEventListener("click", kInteractive.actionEvent), j && r.appendChild(j), r.appendChild(document.createTextNode(" ")), V && r.appendChild(V), q && r.appendChild(q);
            var K = document.createElement("p");
            K.className = "separator", r.appendChild(K), i.options.rtl ? (e.style.direction = "rtl", kInteractive.c.addClass(e, "rtl")) : e.style.direction = "ltr";
            var Y = document.createElement("div");
            i.style && kInteractive.c.addClass(Y, i.style), Y.appendChild(r), e.appendChild(Y)
        }
    },
    postRender: function (e, t, n) {
        var o = kInteractive.readData(t);
        if (o) {
            var r = o.dict,
                i = o.userDict;
            r || (r = {}), i || (i = r);
            for (var a = t.getElementsByClassName("ques"), s = t.getElementsByClassName("explanation"), l = s.length; l--;) s[l].parentNode.removeChild(s[l]);
            for (var c = t.getElementsByClassName("reference"), l = c.length; l--;) c[l].parentNode.removeChild(c[l]);
            for (var d = 0; d < a.length; d++) {
                var u = a[d].getElementsByClassName("ans"),
                    p = (u.length, d);
                if (o.options.randomize && (p = Number(a[d].getAttribute("data-o"))), document[t.id] && document[t.id].preserve && document[t.id].q && document[t.id].q[p]) {
                    if (o.q[p].exp && !a[d].getElementsByClassName("explanation").length) {
                        var f = document.createElement("p");
                        f.className = "explanation", f.innerHTML = kInteractive.escapeHtml(o.q[p].exp), kInteractive.appendAfterDelay(a[d], f)
                    }
                    if (o.q[p].ref && !a[d].getElementsByClassName("reference").length) {
                        var g = document.createElement("a");
                        g.className = "reference", g.innerHTML = i.learnMode ? i.learnMode : "Learn more", g.setAttribute("href", kInteractive.escapeHtml(o.q[p].ref)), g.setAttribute("target", "_blank"), kInteractive.appendAfterDelay(a[d], g)
                    }
                }
                for (l = 0; l < u.length; l++)
                    if (kInteractive.c.removeClass(u[l].parentNode, "correct"), u[l].getAttribute("id").indexOf("ki-tf-") >= 0) document[t.id] && document[t.id].preserve && document[t.id].q && document[t.id].q[p] && (document[t.id].q[p][l] ? u[l].setAttribute("checked", !0) : u[l].removeAttribute("checked"), o.options && o.options.highlightCorrect && (0 == l && o.q[p].a || 1 == l && !o.q[p].a) && kInteractive.c.addClass(u[l].parentNode, "correct"));
                    else if (u[l].getAttribute("id").indexOf("ki-dd-") >= 0) {
                        if (document[t.id] && document[t.id].preserve && document[t.id].q && document[t.id].q[p])
                            for (var h = 0; h < document[t.id].q[p].length; h++) {
                                var m = document[t.id].q[p][h];
                                try {
                                    for (var v = document.getElementById(m.id), b = v; !kInteractive.c.hasClass(b, "ques");) b = b.parentNode;
                                    b.getElementsByClassName("categoryContainer")[0].children[m.category].appendChild(v)
                                } catch (e) { }
                            }
                    } else u[l].getAttribute("id").indexOf("ki-sa-") >= 0 ? document[t.id] && document[t.id].preserve && document[t.id].q && document[t.id].q[p] && (u[l].removeAttribute("value"), document[t.id].q[p] && u[l].setAttribute("value", document[t.id].q[p])) : document[t.id] && document[t.id].preserve && document[t.id].q && document[t.id].q[p] && (document[t.id].q[p][l] ? u[l].setAttribute("checked", !0) : u[l].removeAttribute("checked"), o.options && o.options.highlightCorrect && o.q[p].c[l].a && kInteractive.c.addClass(u[l].parentNode, "correct"));
                if (document[t.id]) {
                    var y = t.getElementsByClassName("questions-preserve");
                    y.length && (document[t.id].preserve ? y[0].setAttribute("checked", !0) : y[0].removeAttribute("checked"))
                }
            }
        }
    },
    action: function (e, t, n) {
        function o(e) {
            for (var t = 0; e = e.previousSibling;) "#text" != e.nodeName && 8 != e.nodeType && t++;
            return t
        }

        function r() {
            var e = "Questions: " + d + ". Score: " + p + " questions out of " + g;
            e += ". Action: " + J, "selfAnswerReport" == J && (e += ". Details: " + h), kInteractive.tinCan({
                verb: "solved",
                activity: e
            })
        }
        var i = kInteractive.readData(e);
        if (i) {
            var a = i.dict,
                s = i.userDict;
            if (a || (a = {}), s || (s = a), i.layout && i.layout.popup && !kInteractive.hasClass(e, "inpopup") && "img" == t.nodeName.toLowerCase()) {
                var l = document.getElementById("epubContent");
                l && e.setAttribute("data-loc", function (e, t) {
                    for (var n = ""; e != t;) n = o(e) + "." + n, e = e.parentNode;
                    return "" == n ? "" : n.substr(0, n.length - 1)
                }(e, l));
                var c = document.createElement("iframe");
                return c.setAttribute("nwdisable", "true"), c.setAttribute("nwfaketop", "true"), i.cb = function (t) {
                    function n() {
                        if (void 0 !== isKotobee) {
                            var t = {};
                            t.root = c.contentDocument.documentElement.ownerDocument.body, t.rootIndex = e.getAttribute("data-loc"), angular.element(document.body).scope().getNotebookShortcut(t, function () { })
                        }
                    }
                    var o, r = document.createElement("head");
                    if (void 0 === isKotobee) {
                        for (var i = document.getElementsByTagName("link"), a = 0; a < i.length; a++) {
                            var s = i[a];
                            if (s.getAttribute("href").indexOf("base.css") >= 0) {
                                var l = s.getAttribute("href").split("/");
                                delete l[l.length - 1], o = l.join("/");
                                break
                            }
                        }
                        if (!o) return
                    }
                    var d = void 0 === isKotobee ? o + "../xhtml/" : bookPath + "EPUB/xhtml/",
                        r = "<head>";
                    if (r += '<base href="' + kInteractive.c.removeFilename(kInteractive.c.removeHash(window.location.href)) + "/" + d + '"/>', r += '<link rel="stylesheet" type="text/css" href="../css/base.css" />', r += '<link rel="stylesheet" type="text/css" href="../css/global.css" />', r += '<link rel="stylesheet" type="text/css" href="../css/kotobeeInteractive.css" />', r += '<script type="text/javascript" src="../js/kotobeeInteractive.js"><\/script>', r += '<script type="text/javascript" src="../js/global.js"><\/script>', void 0 !== isKotobee) {
                        var u = clone(config);
                        u.kotobee.email = angular.element(document.body).scope().data.user.email, r += '<script type="text/javascript">var config=' + JSON.stringify(u) + ";<\/script>"
                    }
                    r += '<meta charset="utf-8" />', r += "</head>";
                    var p = e.outerHTML;
                    p = (p = (p = (p = (p = (p = p.replace("<img", '<img style="display:none"')).replace(/([;\s]*?height:)(.*)?;/i, "$1auto")).replace(/([;\s]*?width:)(.*)?;/i, "$1auto")).replace(/([;\s]*?top:)(.*)?;/i, "$1auto")).replace(/([;\s]*?left:)(.*)?;/i, "$1auto")).replace('class="', 'class="inpopup ');
                    var f = "<html>" + r + "<body>" + (p += '<div class="vSpace20"></div>') + "</body></html>";
                    c.setAttribute("srcdoc", f);
                    var g = "javascript: window.frameElement.getAttribute('srcdoc');";
                    c.setAttribute("src", g), c.contentWindow && (c.contentWindow.location = g), t.appendChild(c), c.addEventListener ? c.addEventListener("load", n, !0) : c.attachEvent && c.attachEvent("onload", n)
                }, i.closed = function () { }, i.pos = i.layout.pos, kInteractive.openFrame(i), kInteractive.tinCan({
                    verb: "opened",
                    activity: "Questions: " + i.title
                }), void (kInteractive.scorm && setTimeout(function () {
                    var e = {};
                    e.id = kInteractive.getScormId("question", i.title), e.description = "Opened question popup: " + i.title, e.learnerResponses = "Opened", e.type = "other", e.timestamp = new Date, kInteractive.scorm.setInteractions([e])
                }, 800))
            }
            var d = "Untitled questions",
                u = e.getElementsByTagName("h4");
            u.length && (d = u[0].textContent || u[0].data);
            var p = 0,
                f = e.getElementsByClassName("ques"),
                g = f.length,
                h = "";
            if (!kInteractive.hasClass(t, "kInteractive"))
                if (kInteractive.hasClass(t, "questions-clear")) {
                    for (var m = e.getElementsByClassName("ans"), v = [], b = 0; b < m.length; b++) m[b].checked = !1, m[b].value = null, kInteractive.c.removeClass(m[b].parentNode, "correct"), kInteractive.c.removeClass(m[b], "incorrect"), kInteractive.c.hasClass(m[b].parentNode, "dCategory") && v.push(m[b]);
                    for (b = 0; b < v.length; b++) v[b].parentNode.parentNode.parentNode.getElementsByClassName("ki-dd-answers")[0].appendChild(v[b]);
                    for (b = (W = e.getElementsByClassName("explanation")).length; b--;) W[b].parentNode.removeChild(W[b]);
                    for (b = (_ = e.getElementsByClassName("reference")).length; b--;) _[b].parentNode.removeChild(_[b]);
                    document[e.id] = null
                } else {
                    if (kInteractive.hasClass(t, "questions-notebook")) {
                        var y = i.layout && i.layout.popup && kInteractive.hasClass(e, "inpopup");
                        if (void 0 === isKotobee && !y) return void kInteractive.alert({
                            content: a.kotobeeOnly,
                            title: s.sorry ? s.sorry : "Sorry"
                        });
                        var k, w = {
                            elem: e
                        };
                        if (void 0 !== isKotobee) k = angular.element(document.body).scope();
                        else {
                            try {
                                k = window.parent.getGlobal()
                            } catch (e) {
                                return void kInteractive.alert({
                                    content: a.kotobeeOnly,
                                    title: s.sorry ? s.sorry : "Sorry"
                                })
                            }
                            w.rootIndex = e.getAttribute("data-loc"), w.root = e
                        }
                        return kInteractive.c.addClass(t, "busy"), void k.questionsAddToNotebookShortcut(w, function () {
                            kInteractive.c.removeClass(t, "busy"), kInteractive.alert({
                                content: s.savedToNotebook ? s.savedToNotebook : "Saved to notebook!"
                            })
                        })
                    }
                    if (kInteractive.hasClass(t, "questions-preserve")) return document[e.id] || (document[e.id] = {}), void (document[e.id].preserve = t.checked);
                    for (var C = 0, x = 0, I = [], b = 0; b < f.length; b++) {
                        var E = (m = f[b].getElementsByClassName("ans")).length > 0,
                            T = i.q[b];
                        if (i.options.randomize && (T = i.q[Number(f[b].getAttribute("data-o"))]), kInteractive.scorm && i.options && i.options.answerOnce && kInteractive.scorm.interactionExists(kInteractive.getScormId(e.id + "-" + (b + 1), T.q))) {
                            G = "<p style='text-align:center'>" + (s.alreadySubmitted ? s.alreadySubmitted : "This test has already been submitted!") + "</p>";
                            return void kInteractive.alert({
                                content: G
                            })
                        }
                        var O = {},
                            S = m[0].getAttribute("id");
                        I.push(O), O.id = kInteractive.getScormId(e.id + "-" + (b + 1), T.q), O.qid = e.id, S.indexOf("ki-tf-") >= 0 ? O.type = "true-false" : S.indexOf("ki-mcq-") >= 0 || S.indexOf("ki-mmcq-") ? O.type = "choice" : S.indexOf("ki-dd-") >= 0 ? O.type = "drag-drop" : S.indexOf("ki-sa-") >= 0 && (O.type = "short-answer"), O.objective = i.options ? i.options.objective : null, O.timestamp = kInteractive.timestamp, O.latency = Math.round(((new Date).getTime() - kInteractive.timestamp.getTime()) / 1e3), O.description = T.q;
                        for (var A = [], L = 0; L < m.length; L++)
                            if (m[L].getAttribute("id").indexOf("ki-tf-") >= 0) 0 == L && T.a ? O.correctResponses = T.choice1 : 1 != L || T.a || (O.correctResponses = T.choice2), m[L].checked ? (O.learnerResponses = L ? T.choice2 : T.choice1, 0 != L || T.a ? 1 == L && T.a && (E = !1) : E = !1) : 0 == L && T.a ? E = !1 : 1 != L || T.a || (E = !1), i.options && i.options.highlightCorrect && (0 == L && T.a || 1 == L && !T.a) && kInteractive.c.addClass(m[L].parentNode, "correct");
                            else if (m[L].getAttribute("id").indexOf("ki-sa-") >= 0) {
                                (le = m[L].value.toLowerCase()) && (le = le.trim()), E = !0;
                                for (var B = "", N = 0; N < T.k.length; N++) {
                                    var M = T.k[N].t;
                                    if (!M) {
                                        E = !1;
                                        break
                                    }
                                    M = M.toLowerCase(), B && (B += "+"), B += M;
                                    for (var P = M.split(","), $ = !1, H = 0; H < P.length; H++)
                                        if (le.indexOf(P[H].trim()) >= 0) {
                                            $ = !0;
                                            break
                                        }
                                    if (!$) {
                                        E = !1;
                                        break
                                    }
                                }
                                O.correctResponses = B, O.learnerResponses = le
                            } else if (m[L].getAttribute("id").indexOf("ki-dd-") >= 0) {
                                kInteractive.c.removeClass(m[L], "incorrect");
                                var R = m[L].id.split("-")[4],
                                    D = T.answers[R].forCategory,
                                    F = f[b].getElementsByClassName("dCategory")[D],
                                    z = {};
                                if (z.correct = m[L].innerText + " -> " + F.children[0].innerText, kInteractive.c.hasClass(m[L].parentNode, "ki-dd-answers")) kInteractive.c.addClass(m[L], "incorrect"), E = !1, z.learner = m[L].innerText + " unassigned";
                                else {
                                    var j = m[L].parentNode,
                                        U = j.children[0].innerText,
                                        V = kInteractive.getChildIndex(j);
                                    z.learner = m[L].innerText + " -> " + U, V != D && i.options && i.options.highlightCorrect && (E = !1, kInteractive.c.addClass(m[L], "incorrect"))
                                }
                                A.push(z)
                            } else T.c[L].a && (O.correctResponses = T.c[L].t), m[L].checked && (O.learnerResponses = T.c[L].t), m[L].checked && !T.c[L].a ? E = !1 : !m[L].checked && T.c[L].a && (E = !1), i.options && i.options.highlightCorrect && T.c[L].a && kInteractive.c.addClass(m[L].parentNode, "correct");
                        if (A.length)
                            for (var q = 0; q < A.length; q++) O.correctResponses && (O.correctResponses += "; "), O.learnerResponses && (O.learnerResponses += "; "), O.correctResponses += A[q].correct, O.learnerResponses += A[q].learner;
                        if (O.result = 0, null != T.weight && (C += T.weight, E && (x += T.weight, O.result = T.weight)), O.scoreMax = i.options ? Number(i.options.totalScore) : null, T.exp && !f[b].getElementsByClassName("explanation").length) {
                            var W = document.createElement("p");
                            W.className = "explanation", W.innerHTML = kInteractive.escapeHtml(T.exp), kInteractive.appendAfterDelay(f[b], W)
                        }
                        if (T.ref && !f[b].getElementsByClassName("reference").length) {
                            var _ = document.createElement("a");
                            _.className = "reference", _.innerHTML = s.learnMode ? s.learnMode : "Learn more", _.setAttribute("href", kInteractive.escapeHtml(T.ref)), _.setAttribute("target", "_blank"), kInteractive.appendAfterDelay(f[b], _)
                        }
                        var K = s.question ? s.question.replace("[no]", b + 1) : "Question " + (b + 1);
                        E ? (h += "<span style='border-bottom: dotted 1px #aaa;' title='" + T.q.replace(/'/g, "&#39;") + "'>" + K + ".</span>  <span style='color:#366c20'>" + s.correct + "</span><br/>", p++) : h += "<span style='border-bottom: dotted 1px #aaa;' title='" + T.q.replace(/'/g, "&#39;") + "'>" + K + ".</span>  <span style='color:#7a1818'>" + s.incorrect + "</span><br/>"
                    }
                    for (b = 0; b < I.length; b++) I[b].weighting = Math.round(1e3 * i.options.totalScore / C) / 1e3;
                    var Y, J = i.action;
                    if (i.options.totalScore && C && (Y = Math.round(10 * i.options.totalScore * x / C) / 10), "selfAnswer" == J || "selfAnswerReport" == J) {
                        var Z = "";
                        "ar" == i.options.language && (Z = ' direction="rtl"');
                        var G = "";
                        if (s.scoreWeight) {
                            if (null != Y) {
                                var X = "";
                                i.options.passScore && (Y >= i.options.passScore || Y == i.options.totalScore ? X += s.pass : X += s.fail), X && (X += ". "), G += "<h3" + Z + ">" + (X || "") + s.scoreWeight.replace("[score]", Y).replace("[total]", i.options.totalScore) + "</h3>"
                            }
                            G += "<p" + Z + ">" + s.score.replace("[correct]", p).replace("[total]", g) + "</p>"
                        } else G = "You scored " + p + " question(s) out of " + g;
                        "selfAnswerReport" == J && (G = "<p" + Z + "><strong>" + G + "</strong></p><p><strong>" + (s.details ? s.details : "Details") + "</strong></p><p class='kbAlertScroll'>" + h + "</p>"), kInteractive.alert({
                            content: G
                        }), r()
                    }
                    if ("email" == J || i.options.reportEmail || i.options.emailForm) {
                        var Q = "selfAnswer" != J && "selfAnswerReport" != J && "email" != J;
                        try {
                            if (!config.kotobee.cloudid) return void (Q && kInteractive.alert({
                                content: a.cloudOnly,
                                title: s.sorry ? s.sorry : "Sorry"
                            }))
                        } catch (e) {
                            return void (Q && kInteractive.alert({
                                content: a.cloudOnly,
                                title: s.sorry ? s.sorry : "Sorry"
                            }))
                        }
                        var ee = {};
                        ee.ans = new Array;
                        for (L = 0; L < f.length; L++)
                            for (var m = f[L].getElementsByClassName("ans"), b = 0; b < m.length; b++) m[b].checked && ee.ans.push({
                                q: L,
                                index: b,
                                label: m[b].nextSibling.textContent || m[b].nextSibling.data
                            });
                        ee.title = d;
                        var te = {};
                        if (te.recipient = i.address, i.options.reportEmail && (te.recipient = i.options.reportEmail), te.content = JSON.stringify(ee), te.mode = config.kotobee.mode, te.cloudid = config.kotobee.cloudid, te.email = config.kotobee.email ? config.kotobee.email : angular.element(document.body).scope().data.user.email, i.options.emailForm) {
                            var ne = document.getElementsByName("recipientEmail");
                            ne.length && (te.recipient = te.recipient ? te.recipient + "," + ne[0].value : ne[0].value)
                        }
                        var oe = t.value;
                        t.value = s.submitting ? s.submitting : "Submitting ..", t.setAttribute("disabled", "true");
                        var re = new XMLHttpRequest;
                        re.onreadystatechange = function () {
                            try {
                                if (4 === re.readyState && 200 === re.status) {
                                    if (t.value = oe, t.removeAttribute("disabled"), !Q) return;
                                    JSON.parse(re.responseText).success ? kInteractive.alert({
                                        content: s.submitted ? s.submitted : "Answers submitted!"
                                    }) : kInteractive.alert({
                                        content: s.errorSubmitting ? s.errorSubmitting : "An error has occurred while sending answers to the server"
                                    })
                                }
                            } catch (e) {
                                if (t.value = oe, t.setAttribute("value", t.value), t.removeAttribute("disabled"), !Q) return;
                                kInteractive.alert({
                                    content: s.errorSubmitting ? s.errorSubmitting : "An error has occurred while sending answers to the server"
                                })
                            }
                        };
                        var ie = config.kotobee.liburl ? config.kotobee.liburl : "http://www.kotobee.com/";
                        ie += "library/report/questions", re.open("POST", ie), re.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        var ae = "a=a";
                        for (var se in te) {
                            var le = te[se];
                            void 0 != le && ("object" != typeof le && ("string" == typeof le && (le = encodeURIComponent(le)), "boolean" == typeof le && (le = le ? 1 : 0), ae += "&" + se + "=" + le))
                        }
                        re.send(ae), r()
                    }
                    document[e.id] || (document[e.id] = {}), document[e.id].q = [];
                    for (var ce = e.getElementsByClassName("ques"), L = 0; L < ce.length; L++) {
                        var de = L;
                        i.options.randomize && (de = Number(ce[L].getAttribute("data-o"))), document[e.id].q[de] = [];
                        for (var ue = ce[L].getElementsByClassName("ans"), b = 0; b < ue.length; b++) kInteractive.hasClass(ue[b], "txtfield") ? document[e.id].q[de][b] = ue[b].value : ue[b].type && "checkbox" == ue[b].type.toLowerCase() ? document[e.id].q[de][b] = ue[b].checked : ue[b].type && "radio" == ue[b].type.toLowerCase() ? document[e.id].q[de][b] = ue[b].checked : kInteractive.c.hasClass(ue[b].parentNode, "dCategory") && (document[e.id].q[de][b] = {
                            id: ue[b].id,
                            category: kInteractive.getChildIndex(ue[b].parentNode)
                        })
                    }
                    kInteractive.scorm && setTimeout(function () {
                        kInteractive.scorm.setInteractions(I)
                    }, 500)
                }
        }
    }
};

kInteractive.threed = {
    preRender: function (e, t) { },
    postRender: function (e, t, n, o) {
        var r = e.createDocumentFragment(),
            i = kInteractive.readData(t);
        if (i) {
            t.innerHTML = "";
            var a = t.getAttribute("style");
            i.inter.placeholderPath && (a += "background-image:url('" + (void 0 === isKotobee ? i.inter.placeholderPath : ph.join(kInteractive.absoluteURL, i.inter.placeholderPath)) + "');"), t.setAttribute("style", a);
            var s = document.createElement("a");
            i.inter.includeMsg ? (s.innerHTML = kInteractive.escapeHtml(i.inter.msg), s.className = "msgBtn ki-btn") : s.className = "invisibleBtn ki-btn", r.appendChild(s), void 0 === isKotobee && s && s.addEventListener("click", kInteractive.actionEvent), t.appendChild(r)
        }
    },
    action: function (e, t, n) {
        function o() {
            function t(e) {
                function t(n) {
                    if (!(n >= r.objects.length)) {
                        var o = r.objects[n],
                            s = void 0 === isKotobee ? o.path : ph.join(kInteractive.absoluteURL, o.path);
                        new THREE.OBJLoader(u).load(s, function (r) {
                            i && i.parentNode && i.parentNode.removeChild(i), a.domElement.parentNode || e.appendChild(a.domElement), r.position.set(o.x, o.y, o.z), r.traverse(function (e) {
                                if (e instanceof THREE.Mesh) {
                                    var i = e == r.children[r.children.length - 1];
                                    if (o.textPath) {
                                        var a = new THREE.ImageLoader(u),
                                            s = void 0 === isKotobee ? o.textPath : ph.join(kInteractive.absoluteURL, o.textPath);
                                        a.load(s, function (o) {
                                            var a = new THREE.Texture;
                                            a.image = o, a.needsUpdate = !0, e.material.map = a, i && d.add(r), t(++n)
                                        }, function () { }, function (e) { })
                                    } else i && d.add(r), t(++n)
                                }
                            })
                        }, function (e) {
                            e.lengthComputable && (e.loaded, e.total)
                        }, function (e) { })
                    }
                }

                function o() {
                    try {
                        a.render(d, c)
                    } catch (e) {
                        return
                    }
                    y.getDelta();
                    n = requestAnimFrame(o)
                }
                kInteractive.scorm && setTimeout(function () {
                    var e = {};
                    e.id = kInteractive.getScormId("_3D", r.name ? r.name : "Model"), e.description = "Opened 3D model: " + (r.name ? r.name : "No name"), e.learnerResponses = "Viewed", e.type = "other", e.timestamp = new Date, kInteractive.scorm.setInteractions([e])
                }, 800), n && cancelAnimationFrame(n);
                var a;
                a = function () {
                    try {
                        var e = document.createElement("canvas");
                        return !(!window.WebGLRenderingContext || !e.getContext("webgl") && !e.getContext("experimental-webgl"))
                    } catch (e) {
                        return !1
                    }
                }() ? new THREE.WebGLRenderer({
                    precision: "highp"
                }) : new THREE.CanvasRenderer;
                var s = e.offsetWidth,
                    l = e.offsetHeight,
                    c = new THREE.PerspectiveCamera(r.camera.viewAngle, s / l, r.camera.near, r.camera.far),
                    d = new THREE.Scene;
                null != r.scene.bgColor && a.setClearColor(r.scene.bgColor, 1), d.add(c), c.position.x = r.camera.x, c.position.y = r.camera.y, c.position.z = r.camera.z, a.setSize(s, l), r.scene.floor && (floor = new THREE.Mesh(new THREE.PlaneGeometry(500, 500, 1, 1), new THREE.MeshPhongMaterial({
                    color: r.scene.floorColor ? r.scene.floorColor : 10066329
                })), floor.applyMatrix((new THREE.Matrix4).makeRotationX(-Math.PI / 2)), floor.position.y = -80, floor.receiveShadow = !0, d.add(floor));
                var u = new THREE.LoadingManager;
                u.onProgress = function (e, t, n) { }, t(0), new THREE.OrbitControls(c, a.domElement).target = new THREE.Vector3(0, 0, 0);
                for (var p = 0; p < r.lights.length; p++)
                    if ("pointLight" == r.lights[p].type) {
                        var f = new THREE.PointLight(r.lights[p].color);
                        f.position.set(r.lights[p].x, r.lights[p].y, r.lights[p].z), d.add(f)
                    } else if ("directionalLight" == r.lights[p].type) {
                        var g = new THREE.DirectionalLight(r.lights[p].color, r.lights[p].intensity);
                        g.position.set(r.lights[p].x, r.lights[p].y, r.lights[p].z), d.add(g)
                    } else if ("hemisphereLight" == r.lights[p].type) {
                        var h = new THREE.HemisphereLight(r.lights[p].sky, r.lights[p].ground, r.lights[p].intensity);
                        h.position.set(r.lights[p].x, r.lights[p].y, r.lights[p].z), d.add(h)
                    } else if ("spotLight" == r.lights[p].type) {
                        var m = new THREE.SpotLight(r.lights[p].color, r.lights[p].intensity, r.lights[p].distance, r.lights[p].angle, r.lights[p].exponent, r.lights[p].decay);
                        m.position.set(r.lights[p].x, r.lights[p].y, r.lights[p].z), d.add(m)
                    } else if ("ambientLight" == r.lights[p].type) {
                        var v = new THREE.AmbientLight(r.lights[p].color);
                        d.add(v)
                    } else if ("areaLight" == r.lights[p].type) {
                        var b = new THREE.AreaLight(r.lights[p].color, r.lights[p].intensity);
                        d.add(b)
                    }
                var y = new THREE.Clock;
                o()
            }
            var n, o = e;
            if ("inPanel" == r.inter.target) {
                var a = {};
                a.class = "threed", a.cb = function (e) {
                    var n = document.createElement("div");
                    n.className = "container", e.appendChild(n), t(n)
                }, a.closed = function () {
                    kInteractive.c.removeClass(e, "running")
                }, kInteractive.openFrame(a)
            } else t(o)
        }
        var r = kInteractive.readData(e);
        if (r) {
            "inPanel" == r.inter.target ? kInteractive.c.addClass(e, "running") : e.innerHTML = "";
            var i = document.createElement("div");
            if (i.className = "loading", e.appendChild(i), "undefined" == typeof THREE) {
                var a, s = document.getElementsByTagName("head")[0],
                    l = s.getElementsByTagName("script");
                if (void 0 !== isKotobee) a = bookPath + "EPUB/js/kotobeeInteractive3D.js";
                else
                    for (var c = 0; c < l.length; c++)
                        if (l[c].src.indexOf("kotobeeinteractive.js") >= 0) {
                            a = l[c].src.replace("kotobeeinteractive.js", "kotobeeinteractive3D.js");
                            break
                        } if (!a) return;
                var d = document.createElement("script");
                d.type = "text/javascript", d.src = a, d.onload = o, d.onreadystatechange = function () {
                    "complete" == this.readyState && o()
                }, s.appendChild(d)
            } else o()
        }
    }
};

kInteractive.video = {
    preRender: function (e, t) { },
    postRender: function (e, t, n) {
        var o = e.createDocumentFragment(),
            r = kInteractive.readData(t);
        if (r) {
            var i = "";
            if (r.splash && (i = void 0 === isKotobee ? r.splash : ph.join(kInteractive.absoluteURL, r.splash)), i) {
                t.style.backgroundImage = null;
                var a = t.style.cssText;
                t.setAttribute("style", "background-image:url('" + i + "');" + a)
            }
            t.setAttribute("id", "ki-video-" + n), t.innerHTML = "";
            var s = document.createElement("div");
            s.setAttribute("id", "ki-video-" + n + "-container"), s.className = "container";
            var l = document.createElement("div");
            r.style && kInteractive.c.addClass(l, r.style);
            var c = document.createElement("a");
            c.className = "playBtn ki-btn", c.appendChild(document.createElement("span")), void 0 === isKotobee && c.addEventListener("click", kInteractive.actionEvent), o.appendChild(c), o.appendChild(s), l.appendChild(o), t.appendChild(l), r.autoplay && kInteractive.action(t)
        }
    },
    action: function (e) {
        function t(t) {
            var o = document.createElement("video");
            o.setAttribute("width", "100%"), o.setAttribute("height", "100%"), o.setAttribute("src", t), o.setAttribute("autoplay", "true"), o.setAttribute("data-tap-disabled", "false");
            var i;
            0 != t.indexOf("http://") && 0 != t.indexOf("https://") || (i = !0), i && o.setAttribute("crossorigin", "anonymous");
            var a = !1;
            try {
                a = navigator.userAgent.toLowerCase().indexOf("android") > -1
            } catch (e) { }
            a || o.setAttribute("type", "video/mp4"), o.setAttribute("webkit-playsinline", "true"), o.setAttribute("controls", "true"), o.className = "ki-noHighlight", n.hideDownloadBtn && (o.className += " hideDownloadBtn"), o.innerHTML = "Your browser doesn't support HTML5 video.", i && (o.crossOrigin = "anonymous"), o.oncanplay = function () {
                kInteractive.currentVideo == e && (r.className = "playBtn ki-btn hide", kInteractive.tinCan({
                    verb: "played",
                    activity: "Video: " + t
                }))
            }, o.addEventListener("error", function (e) {
                kInteractive.stopCurrentMedia()
            }), o.addEventListener("webkitfullscreenchange", function (e) {
                var t = null !== document.webkitFullscreenElement;
                kInteractive.videoIsFullscreen = t
            });
            var s = e.getElementsByClassName("container")[0];
            s.setAttribute("data-tap-disabled", "true"), s.appendChild(o)
        }
        var n = kInteractive.readData(e);
        if (n) {
            var o = e.getAttribute("id") + "-container",
                r = e.getElementsByClassName("playBtn")[0];
            if (!(r.className.indexOf("loading") >= 0)) {
                if (kInteractive.stopCurrentMedia(), r.className = "playBtn ki-btn loading", n.autoplay && (r.className = "playBtn ki-btn hide"), kInteractive.scorm) {
                    var i = {};
                    i.id = kInteractive.getScormId(e.getAttribute("id"), n.src), i.description = "Played video: " + n.src, i.type = "other", i.learnerResponses = "Played", i.objective = n.options ? n.options.objective : null, i.timestamp = new Date, kInteractive.scorm.setInteractions([i])
                }
                if ("file" == n.type) t(void 0 === isKotobee ? n.video : ph.join(kInteractive.absoluteURL, n.video));
                else {
                    var a, s = new URL(n.src);
                    if (s.origin.includes("youtube.com") || s.origin.includes("youtu.be")) {
                        if (!(a = s.origin.includes("youtube.com") ? s.searchParams.get("v") : s.pathname.split("/")[1])) return;
                        if (window && window.iBooks) return setTimeout(function () {
                            r.className = "playBtn ki-btn"
                        }, 1500), void (window.location.href = n.src);
                        var l = {
                            videoId: a,
                            playerVars: {
                                autoplay: 1,
                                rel: "0"
                            },
                            events: {
                                onReady: function () { },
                                onStateChange: function (e) {
                                    e.data == YT.PlayerState.PLAYING && kInteractive.tinCan({
                                        verb: "played",
                                        activity: "Video: " + n.src
                                    })
                                }
                            }
                        };
                        if (kInteractive.youTubeStatus)
                            if ("loading" == kInteractive.youTubeStatus) {
                                if (kInteractive.vQueue) return;
                                kInteractive.vQueue = function () {
                                    r.className = "playBtn ki-btn hide", new YT.Player(o, l)
                                }
                            } else "ready" == kInteractive.youTubeStatus && (r.className = "playBtn ki-btn hide", new YT.Player(o, l));
                        else {
                            window.onYouTubePlayerAPIReady = function () {
                                kInteractive.youTubeStatus = "ready", kInteractive.vQueue(), kInteractive.vQueue = null
                            };
                            var c = document.createElement("script");
                            c.src = "https://www.youtube.com/player_api";
                            var d = document.getElementsByTagName("script")[0];
                            d.parentNode.insertBefore(c, d), kInteractive.youTubeStatus = "loading", kInteractive.vQueue = function () {
                                r.className = "playBtn ki-btn hide", new YT.Player(o, l)
                            }
                        }
                    } else t(n.src)
                }
                kInteractive.currentVideo = e, kInteractive.c.addClass(kInteractive.currentVideo, "playing")
            }
        }
    },
    resize: function (e) {
        var t = kInteractive.readData(e);
        t && (kInteractive.checkResponsiveFloat(t, e), t.maintainRatio && "px" == t.widthUnit && t.height && t.width && (e.style.height = e.offsetWidth * (t.height / t.width) + "px"))
    }
};

kInteractive.widget = {
    preRender: function (e, t) { },
    postRender: function (e, t, n, o) {
        o || (o = {});
        var r = e.createDocumentFragment(),
            i = kInteractive.readData(t);
        if (i) {
            var a = i.name,
                s = (i.src, i.width),
                l = i.height;
            if ("page" == i.mode) {
                if (t.children.length) return;
                var c = document.createElement("div");
                c.className = "cover", r.appendChild(c), i.interaction && (c.style.pointerEvents = "none");
                var d = document.createElement("div");
                d.className = "iframeContainer";
                var u = document.createElement("iframe");
                u.setAttribute("nwdisable", "true"), u.setAttribute("nwfaketop", "true"), u.setAttribute("width", t.style.width ? t.style.width : s + i.widthUnit), u.setAttribute("height", t.style.height ? t.style.height : l + "px"), u.src = kInteractive.getWidgetUrl(i, o), d.appendChild(u), r.appendChild(d);
                var p = t.style.width,
                    f = t.style.height;
                p.indexOf("px") >= 0 && (i.widthUnit = "px"), p.indexOf("%") >= 0 && (i.widthUnit = "%");
                try {
                    i.widthUnit ? i.width = Number(p.split(i.widthUnit)[0]) : i.width = Number(p)
                } catch (e) {
                    i.width = Number(p)
                }
                try {
                    i.height = Number(f.split("px")[0])
                } catch (e) {
                    i.height = Number(f)
                }
                kInteractive.writeData(t, i)
            } else {
                if (!t.children.length && !t.innerHTML.trim()) {
                    var g = document.createElement("img");
                    g.src = kInteractive.getWidgetHome(i, o) + "/" + a + "/Icon.png", g.className = "wdgtIcon", r.appendChild(g)
                }
                void 0 === isKotobee && t.addEventListener("click", kInteractive.actionEvent)
            }
            t.appendChild(r)
        }
    },

    action: function (e) {
        var t = kInteractive.readData(e);
        if (t) {
            var n = kInteractive.getWidgetUrl(t),
                o = document.createElement("iframe");
            o.setAttribute("nwdisable", "true"), o.setAttribute("nwfaketop", "true"), t.cb = function (e) {
                o.src = n, e.appendChild(o)
            }, t.cb1 = "yes", t.closed = function () { }, kInteractive.openFrame(t), kInteractive.tinCan({
                verb: "opened",
                activity: "Widget: " + t.name
            }), kInteractive.scorm && setTimeout(function () {
                var e = {};
                e.id = kInteractive.getScormId("widget", t.name), e.description = "Opened popup widget: " + t.name, e.learnerResponses = "Opened", e.type = "other", e.timestamp = new Date, kInteractive.scorm.setInteractions([e])
            }, 800)
        }
    },

    resize: function (e) {
        var t = kInteractive.readData(e);
        if (t && "page" == t.mode && "px" == t.widthUnit) {
            var n = t.width,
                o = t.height,
                r = e.parentNode;
            if (r)
                if (n > r.offsetWidth) {
                    var i = r.offsetWidth / n,
                        a = e.children.length - 1;
                    e.children[a].style.transform = e.children[a].style.webkitTransform = e.children[a].style.mozTransform = "scale(" + i + ")", e.children[a].style.transformOrigin = e.children[a].style.webkitTransformOrigin = e.children[a].style.mozTransformOrigin = "0 0", e.style.maxWidth = e.style.width, e.style.height = Math.round(o * i) + "px"
                } else {
                    for (a = 0; a < e.children.length; a++) e.children[a].style.transform = e.children[a].style.webkitTransform = e.children[a].style.mozTransform = e.children[a].style.transformOrigin = e.children[a].style.webkitTransformOrigin = e.children[a].style.mozTransformOrigin = null;
                    e.style.maxWidth = null, e.style.height = o + "px"
                }
        }
    }
};

app.factory("bookmarks", ["selection", "translit", "crdv", "popovers", "stg", "cache", "modals", "virt", "chapters", function (selection, translit, crdv, popovers, stg, cache, modals, virt, chapters) {
    function c(e, t) {
        stg.readSlot("bookmark" + stg.getBookId(), function (n) {
            n || (n = []), e.chapter == currentIndex && (chapters.busy || arrayContainsProperty(n, "bmid", e.bmid) || d.addIcon(e)), n.push(e), stg.writeSlot("bookmark" + stg.getBookId(), n, t)
        })
    }
    var d = {};

    d.addBookmark = function () {
        var o = {};
        o.chapter = currentIndex;

        o.location = selection.getTopCornerLocation();

        stg.addBookmark(o, function () {
            d.addIcon(o), crdv.toast(translit.get("bookmarked"))
        });
    };

    d.addBookmarksFromSync = function (e, t) {
        function n() {
            ++o >= e.length ? t && t() : c(e[o], n)
        }
        var o = -1;
        n()
    };

    d.addIconIfNotShown = function (e) {
        d.addIcon(e)
    };

    d.addIcon = function (n) {
        var r = selection.getElemsByLocation(n.location);
        if (r[0]) {
            var i = selection.createBookMarker(n, r[0]);
            if (i.className = "contentBookMarker", i.setAttribute("bmid", n.bmid), i.setAttribute("bloc", r[0].offsetTop), i.addEventListener("tap", function (e) {
                e.stopImmediatePropagation()
            }), i.addEventListener("click", function (e) {
                virt.safeScrollToElem("content", i, !0);
                var t = {};
                t.bmid = this.getAttribute("bmid"), t.location = this.getAttribute("bloc")
            }), i.addEventListener("hold", function (e) {
                var r = [{
                    name: translit.get("delete"),
                    func: function () {
                        d.delete(n.bmid), popovers.hide()
                    }
                }, {
                    name: translit.get("cancel"),
                    func: function () {
                        popovers.hide()
                    }
                }];
                popovers.show({
                    mode: "list",
                    listOb: r,
                    backdropClickToClose: !1
                })
            }), !mobile) {
                var l = document.createElement("span");
                l.className = "editIcon icon ion-ios-close", l.addEventListener("click", function (e) {
                    var t = {
                        title: "confirmation",
                        msg: "bookmarkDeleteConfirm",
                        barStyle: "bar-dark"
                    },
                        o = {
                            label: "delete",
                            style: "button-assertive"
                        };
                    o.func = function () {
                        d.delete(n.bmid), modals.hide()
                    };
                    var r = {
                        label: "cancel"
                    };
                    r.func = function () {
                        modals.hide()
                    }, t.btns = [o, r], modals.show({
                        mode: "confirm",
                        confirmOptions: t
                    })
                }), l.addEventListener("tap", function (e) {
                    e.stopImmediatePropagation()
                }), i.appendChild(l), i.addEventListener("mouseover", function (e) {
                    addClass(i, "edit"), i.addEventListener("mouseout", function (e) {
                        removeClass(i, "edit")
                    })
                })
            }
        }
    };

    d.delete = function (t, n, o) {
        selection.deleteBookMarker(t);

        stg.deleteBookmark(t, n, o);
    };

    d.deleteAll = function (e, t) {
        stg.getBookmarks(function (n) {
            function o() {
                ++r >= n.length ? t && t() : d.delete(n[r].bmid, e, o)
            }
            var r = -1;
            o()
        })
    };

    return d;
}]);

app.factory("hlights", ["selection", "stg", "$rootScope", "modals", "chapters", function (selection, stg, $rootScope, modals, chapters) {
    function s(e, t) {
        stg.readSlot("hlight" + stg.getBookId(), function (n) {
            n || (n = []), e.chapter == currentIndex && (chapters.busy || arrayContainsProperty(n, "hid", e.hid) || l.addHlight(e)), n.push(e), stg.writeSlot("hlight" + stg.getBookId(), n, t)
        })
    }

    var l = {};

    l.addHlightsFromSync = function (e, t) {
        function n() {
            ++o >= e.length ? t && t() : s(e[o], n)
        }
        var o = -1;
        n()
    };

    l.addHlight = function (t) {
        for (var n = document.getElementsByClassName("hlight" + t.hid), o = n.length; o--;) n[o].style.backgroundColor = null, removeClass(n[o], "contentHlightBG"), removeClass(n[o], "hlight" + t.hid);
        for (o = (n = selection.getElemsByLocation(t.location)).length; o--;) n[o] && (addClass(n[o], "contentHlightBG"), addClass(n[o], "hlight" + t.hid), n[o].style.backgroundColor = t.color);
        return l
    };

    l.showPopup = function (t, n) {
        $rootScope.$new(!0).hlightOb = {}, t || (t = []);
        var s = {};
        s.save = function (r) {
            for (var s = 0; s < t.length; s++) {
                var c = t[s];
                null == c.chapter && (c.chapter = chapters.getElemChapterIndex(selection.getSelectedNodes()[0])), c.color = r, null == c.src && (c.src = selection.getSelectedText()), stg.addHlight(c, function () {
                    l.addHlight(c), s == t.length - 1 && timeOut(function () {
                        modals.hide(), n && n(!0)
                    }, 300)
                })
            }
        }, s.color = t[0].color ? t[0].color : "#fffb85", t[0].hid && (s.delete = function () {
            for (var e = 0; e < t.length; e++) l.delete(t[0].hid, null, function () {
                e == t.length - 1 && timeOut(function () {
                    modals.hide(), n && n()
                }, 300)
            })
        }), modals.show({
            mode: "hlight",
            hlightOptions: s
        })
    };

    l.delete = function (e, t, n) {
        for (var r = document.getElementsByClassName("hlight" + e), i = r.length; i--;) i == r.length - 1 && removeClass(r[i], "last"), removeClass(r[i], "contentHlightBG"), r[i].style.backgroundColor = null, removeClass(r[i], "hlight" + e);
        stg.deleteHlight(e, t, n)
    };

    l.deleteAll = function (e, t) {
        stg.getHlights(function (n) {
            function o() {
                ++r >= n.length ? t && t() : l.delete(n[r].hid, e, o)
            }
            var r = -1;
            o()
        })
    };

    return l;
}]);

app.factory("notes", ["selection", "translit", "stg", "modals", "popovers", "virt", "$timeout", "chapters", "book", "$sce", function (selection, translit, stg, modals, popovers, virt, $timeout, chapters, book, $sce) {
    function h(e) {
        for (var t = e.split(" "), n = 0; n < t.length; n++)
            if (t[n] && 0 == t[n].trim().indexOf("note")) return t[n].trim().substr(4)
    }

    function m(e, t) {
        stg.readSlot("note" + stg.getBookId(), function (n) {
            n || (n = []), e.chapter == currentIndex && (chapters.busy || arrayContainsProperty(n, "nid", e.nid) || v.addIcon(e)), n.push(e), stg.writeSlot("note" + stg.getBookId(), n, t)
        })
    }

    var v = {};

    v.addNotesFromSync = function (e, t) {
        function n() {
            ++o >= e.length ? t && t() : m(e[o], n)
        }
        var o = -1;
        n()
    };

    v.addNotesFromQuestions = function (t, n) {
        function o(e) {
            e >= s.length ? i(0) : stg.addNote(s[e].ob, function () {
                addClass(s[e].elem, "note" + s[e].ob.nid), o(++e)
            })
        }

        function i(e) {
            e >= l.length ? n && n() : stg.deleteNote(l[e].nid, null, function () {
                removeClass(l[e].elem, "note" + l[e].nid), i(++e)
            })
        }
        for (var a = t.elem.getElementsByClassName("ques"), s = [], l = [], c = 0; c < a.length; c++)
            for (var d = a[c], u = d.getElementsByTagName("p")[0].textContent, f = d.getElementsByClassName("ans"), g = 0; g < f.length; g++) {
                var m = f[g],
                    v = m.className;
                v = v.split(" ");
                var b;
                "radio" == m.type ? b = "mcq" : "checkbox" == m.type ? b = "mmcq" : "text" == m.type ? b = "sa" : "textarea" == m.type && (b = "sa");
                var y = h(m.className),
                    k = "";
                if ("mcq" == b || "mmcq" == b) {
                    if (!m.checked) {
                        y && l.push({
                            nid: y,
                            elem: m
                        });
                        continue
                    }
                    k = m.nextElementSibling.textContent
                }
                if ("sa" == b) {
                    if ("" == m.value) {
                        y && l.push({
                            nid: y,
                            elem: m
                        });
                        continue
                    }
                    k = m.value
                }
                var w = {};
                w.chapter || (w.chapter = chapters.getElemChapterIndex(m)), w.location || (w.location = selection.convertElemsToLocation([m], t.root).left, t.rootIndex && (w.location = t.rootIndex + "." + w.location)), w.type || (w.type = "content"), w.src || (w.src = u), w.note = k, y && (w.nid = y), s.push({
                    ob: w,
                    elem: m
                })
            }
        o(0)
    };

    v.addIcon = function (n, o) {
        if ("page" == n.type) {
            if (!(c = selection.getElemsByLocation(n.location))[0]) return;
            var i = selection.createNoteMarker(n, c[0], {
                class: "contentSideNoteMarker"
            });
            if (i.setAttribute("nid", n.nid), i.setAttribute("ncontent", n.note ? n.note : ""), i.setAttribute("nloc", n.location), i.addEventListener("click", function (e) {
                virt.safeScrollToElem("content", i, !0);
                var t = {};
                t.note = this.getAttribute("ncontent"), t.nid = this.getAttribute("nid"), t.location = this.getAttribute("nloc"), t.type = "page", v.noteClicked([t]), e.stopImmediatePropagation()
            }), i.addEventListener("tap", function (e) {
                e.stopImmediatePropagation()
            }), i.addEventListener("hold", function (o) {
                var i = [{
                    name: translit.get("edit"),
                    func: function () {
                        popovers.hide(), v.noteClicked([n])
                    }
                }, {
                    name: translit.get("delete"),
                    func: function () {
                        stg.deleteNote(n.nid, null, function () {
                            selection.deleteNoteMarker(n.nid), popovers.hide()
                        })
                    }
                }, {
                    name: translit.get("cancel"),
                    func: function () {
                        popovers.hide()
                    }
                }];
                popovers.show({
                    mode: "list",
                    listOb: i,
                    backdropClickToClose: !1
                })
            }), !mobile) {
                var a = document.createElement("span");
                a.className = "editIcon icon ion-ios-close", a.addEventListener("click", function (t) {
                    var o = {
                        title: "confirmation",
                        msg: "noteDeleteConfirm",
                        barStyle: "bar-dark"
                    },
                        i = {
                            label: "delete",
                            style: "button-assertive"
                        };
                    i.func = function () {
                        stg.deleteNote(n.nid, null, function () {
                            selection.deleteNoteMarker(n.nid)
                        }), modals.hide()
                    };
                    var a = {
                        label: "cancel"
                    };
                    a.func = function () {
                        modals.hide()
                    }, o.btns = [i, a], modals.show({
                        mode: "confirm",
                        confirmOptions: o
                    }), t.stopImmediatePropagation()
                }), a.addEventListener("tap", function (e) {
                    e.stopImmediatePropagation()
                }), i.appendChild(a), i.addEventListener("mouseover", function (e) {
                    addClass(i, "edit"), i.addEventListener("mouseout", function (e) {
                        removeClass(i, "edit")
                    })
                })
            }
        }
        if ("content" == n.type) {
            for (var c = selection.getElemsByLocation(n.location, o), p = 0; p < c.length; p++) c[p] && addClass(c[p], "note" + n.nid);
            if (1 == c.length && c[0]) {
                var f = c[0].type;
                if ("radio" == f || "checkbox" == f) return void (c[0].checked = !0);
                if ("text" == f || "textarea" == f) return void (c[0].value = n.note)
            }
            for (p = 0; p < c.length; p++) c[p] && addClass(c[p], "contentNoteBG");
            var g = c[c.length - 1];
            if (!g) return;
            g.setAttribute("ncontent", n.note ? n.note : ""), g.setAttribute("nloc", n.location), addClass(g, "last"), $timeout(function () {
                try {
                    var t = selection.createNoteMarker(n, g);
                    t && (t.onclick = function (e) {
                        var t = this.getAttribute("id"),
                            n = document.getElementsByClassName(t),
                            o = n[n.length - 1],
                            r = (o.getAttribute("ncontent"), {});
                        r.note = o.getAttribute("ncontent"), r.location = o.getAttribute("nloc"), r.nid = t.substr(4), r.type = "content", r.src = o.textContent, v.noteClicked([r]), e.stopImmediatePropagation(), e.preventDefault()
                    }, t.addEventListener("tap", function (e) {
                        e.stopImmediatePropagation()
                    }))
                } catch (e) { }
            })
        }
    };

    v.noteClicked = function (t, n) {
        try {
            t || (t = [{}]);
            var o = {};
            o.save = function (o) {
                for (var i = 0; i < t.length; i++) {
                    var a = t[i];
                    a.chapter || (a.chapter = chapters.getElemChapterIndex(selection.getSelectedNodes()[0])), a.location || (a.location = selection.getTopCornerLocation()), a.type || (a.type = "page"), a.src || (a.src = "content" == a.type ? selection.getSelectedText() : "");
                    var l = null == a.nid;
                    a.note = o, stg.addNote(a, function () {
                        if (l) v.addIcon(a);
                        else {
                            selection.getNoteMarker(a.nid).elem.setAttribute("ncontent", a.note);
                            var o = selection.getElemsByLocation(a.location),
                                r = o[o.length - 1];
                            r && r.setAttribute("ncontent", a.note)
                        }
                        i == t.length - 1 && $timeout(function () {
                            modals.hide(), n && n(!0)
                        }, 300)
                    })
                }
            }, t[0].note && (o.note = t[0].note, o.delete = function () {
                for (var e = 0; e < t.length; e++) v.delete(t[0].nid, null, function () {
                    e == t.length - 1 && $timeout(function () {
                        modals.hide(), n && n()
                    }, 300)
                })
            }), modals.show({
                mode: "note",
                dynamic: !0,
                animation: "slide-in-up",
                noteOptions: o,
                event: t[0].event
            })
        } catch (e) { }
    };

    v.delete = function (t, n, o) {
        selection.deleteNoteMarker(t);
        for (var i = document.getElementsByClassName("note" + t), a = i.length; a--;) a == i.length - 1 && (removeClass(i[a], "last"), i[a].removeAttribute("nloc"), i[a].removeAttribute("ncontent")), removeClass(i[a], "contentNoteBG"), removeClass(i[a], "note" + t);
        stg.deleteNote(t, n, o)
    };

    v.deleteAll = function (e, t) {
        stg.getNotes(function (n) {
            function o() {
                ++r >= n.length ? t && t() : v.delete(n[r].nid, e, o)
            }
            var r = -1;
            o()
        })
    };

    v.updateNotebookArray = function (e, t) {
        var n = new Array;
        stg.getBookmarks(function (o) {
            for (var i = 0; i < o.length; i++) {
                var a = o[i];
                n[a.chapter] || (n[a.chapter] = new Array), n[a.chapter].push(a)
            }
            stg.getNotes(function (o) {
                for (var i = 0; i < o.length; i++) {
                    var a = o[i];
                    n[a.chapter] || (n[a.chapter] = new Array), a.noteTrusted = "", a.note && (a.noteTrusted = $sce.trustAsHtml(a.note.replace(new RegExp("\r?\n", "g"), "<br />"))), n[a.chapter].push(a)
                }
                stg.getHlights(function (o) {
                    for (a = 0; a < o.length; a++) {
                        i = o[a];
                        n[i.chapter] || (n[i.chapter] = new Array), n[i.chapter].push(i)
                    }
                    for (var i in n) n[i].title = book.getTitleByIndex(i);
                    for (var a = 0; a < n.length; a++)
                        if (n[a])
                            for (var s = 0; s < n[a].length; s++)
                                if (n[a][s].location) {
                                    var l = String(n[a][s].location).split(",");
                                    n[a][s].locationDec = Number("0." + l[0].split(".").join(""))
                                }
                    stg.data.notebookContent = {
                        results: n,
                        showDelete: e.showDelete
                    }, t && t()
                })
            })
        })
    };

    return v;
}]);

app.controller("BookInfoCtrl", ["$scope", "np", "bookinfo", "crdv", "book", "modals", function ($scope, np, bookinfo, crdv, book, modals) {
    var m;
    $scope.init = function () { };
    $scope.open = function (e) {
        m = e, book.open(e)
    };
    $scope.download = function (e) {
        bookinfo.download(e)
    };
    $scope.actionBtnClicked = function (e) {
        e.redirect ? desktop ? np.openLink(e.redirect) : native && crdv.openInNativeBrowser(e.redirect) : bookinfo.action(e)
    };
    $scope.delete = function (e) {
        bookinfo.delete(e)
    };
    $scope.addToFav = function (e) {
        bookinfo.addToFav(e)
    };
    $scope.toggleFav = function (e) {
        bookinfo.toggleFav(e)
    };
    $scope.removeFav = function (e) {
        bookinfo.removeFav(e)
    };
    $scope.categoryClicked = function (t) {
        modals.hide(), $scope.modal.bookInfoOptions.categoryClicked(t)
    };
    $scope.redeemCodeClicked = function () {
        bookinfo.redeemCode()
    }
}]);

app.controller("BookSettingsCtrl", ["$scope", "stg", "crdv", "nav", "backend", "$ionicSideMenuDelegate", "modals", "settings", "$timeout", function ($scope, stg, crdv, nav, backend, $ionicSideMenuDelegate, modals, settings, $timeout) {
    $scope.init = function () {
        $scope.navAnims = [
            {
                label: "flip",
                val: "navBookBlock"
            },
            {
                label: "card",
                val: "navFlip"
            },
            {
                label: "slide",
                val: "navSlide"
            },
            {
                label: "fade",
                val: "navFade"
            }];
        $scope.navModes = [
            {
                label: "vertical",
                val: "vertical"
            },
            {
                label: "horizontal",
                val: "horizontal"
            }];
        $scope.viewModes = [{
            label: "auto",
            val: "auto"
        },
        {
            label: "singlePage",
            val: "single"
        },
        {
            label: "doublePage",
            val: "double"
        }];
    };

    $scope.stylingChanged = function () {
        $timeout(settings.stylingChanged, 300)
    };

    $scope.navAnimChanged = function () {
        settings.saveSettings()
    };

    $scope.viewModeChanged = function () {
        $timeout(settings.viewModeChanged, 300), stg.writeSlot("settings" + stg.getBookId(!0), stg.data.settings)
    };

    $scope.navModeChanged = function () {
        settings.saveSettings()
    };

    $scope.lineAdjustChanged = function () {
        $timeout(settings.lineAdjustChanged, 300)
    };

    $scope.singlePageScrollChanged = function () {
        $timeout(settings.viewModeChanged, 300)
    };

    $scope.editFontSize = function () {
        $scope.header.mode = "textSize", $ionicSideMenuDelegate.toggleRight(!1), $ionicSideMenuDelegate.toggleLeft(!1)
    };

    $scope.langChanged = function () {
        $ionicSideMenuDelegate.toggleLeft(!1), $ionicSideMenuDelegate.toggleRight(!1), $timeout(settings.langChanged, 500)
    };

    $scope.sync = function () {
        $ionicSideMenuDelegate.toggleRight(!1), $ionicSideMenuDelegate.toggleLeft(!1), settings.sync()
    };

    $scope.fullscreen = function () {
        $ionicSideMenuDelegate.toggleRight(!1), $ionicSideMenuDelegate.toggleLeft(!1), nav.fullscreen()
    };

    $scope.logout = function () {
        $ionicSideMenuDelegate.toggleRight(!1), $ionicSideMenuDelegate.toggleLeft(!1), backend.logout()
    };

    $scope.about = function () {
        $scope.openAuthorWebsite()
    };

    $scope.exit = function () {
        modals.show({
            mode: "exit",
            backdropClickToClose: !1,
            exitOptions: {
                exit: function () {
                    crdv.exit()
                }
            }
        })
    };
}]);

app.controller("CatMenuCtrl", ["$scope", "backend", "translit", "$ionicScrollDelegate", "$ionicModal", "$timeout", "stg", function (e, t, n, o, r, i, a) { }]);

app.controller("ChaptersCtrl", ["$scope", "initializer", "stg", "modals", "$ionicHistory", "crdv", "chapters", "$location", function ($scope, initializer, stg, modals, $ionicHistory, crdv, chapters, $location) {
    $scope.init = function () {
        putTest("ChaptersCtrl init");
        $scope.$on("$stateChangeStart", function (e, t, n) {
            if (0 == t.name.indexOf("reader") || 0 == t.name.indexOf("libraryreader")) {
                var o = n.ch ? n.ch : 0,
                    r = n.hash ? n.hash : null,
                    i = getQueryStringValue("vi");
                chapters.loadChapterByIndex(Number(o), {
                    hash: r,
                    vIndex: i
                })
            }
        }), $scope.native || $scope.desktop || $scope.$on("$ionicView.beforeEnter", function () {
            if (!initializer.initialized || !config.kotobee.public && !stg.data.user.loggedIn) {
                var e = "loading";
                $ionicHistory.forwardView() ? e = $ionicHistory.forwardView().stateId : $ionicHistory.backView() && (e = $ionicHistory.backView().stateId), $ionicHistory.nextViewOptions({
                    disableAnimate: !0
                }), $location.path("/" + e)
            }
        });
    };

    $scope.nextChapter = function () {
        return chapters.nextChapter()
    };

    $scope.prevChapter = function () {
        return chapters.prevChapter()
    };

    $scope.pinched = function (e) { };

    $scope.showBookInfo = function () {
        native && crdv.vibrate();
        var e = stg.data.book;
        e.coverImg = stg.data.epub.bookThumb, e.coverStyle = {
            "background-image": "url('" + e.coverImg + "')"
        }, e.name = e.meta.dc.title;
        var t = {};
        t.book = e, config.kotobee.cloudid && "book" == config.kotobee.mode && (t.book.stats = config.kotobee.stats), t.noAction = !0, modals.show({
            mode: "bookInfo",
            dynamic: !0,
            animation: "slide-in-up",
            bookInfoOptions: t
        })
    };
}]);

app.controller("ContentCtrl", ["nav", "virt", "stg", "$scope", "$ionicScrollDelegate", function (nav, virt, stg, $scope, $ionicScrollDelegate) {
    $scope.init = function () { };
    $scope.mouseDown = function () {
        mobile && nav.resize(), virt.enable()
    };
    $scope.mouseUp = function () {
        virt.disableAfter(2e3)
    };
    var y;
    $scope.scroll = function () {
        if (stg.data.book) {
            var e = $ionicScrollDelegate.$getByHandle("content"),
                t = new Event("kotobeeScrolled");
            if (e) {
                var o = e.getScrollPosition();
                t.left = o.left, t.top = o.top, t.zoom = o.zoom
            }
            document.dispatchEvent(t), kotobee && kotobee.dispatchEvent("scrolled", o), config.preserveLoc && "app" != config.kotobee.mode && (y && clearTimeout(y), y = setTimeout(function () {
                if ("app" != config.kotobee.mode) {
                    var e = $ionicScrollDelegate.$getByHandle("content");
                    if (e) {
                        var t = e.getScrollPosition();
                        t && stg.writeSlot("lastLocY" + stg.getBookId(), t.top)
                    }
                }
            }, 2e3)), mobile || (virt.enable(), virt.disableAfter(2e3))
        }
    };
}]);

app.controller("ForgotPwdCtrl", ["$scope", "initializer", "popovers", "crdv", "design", "stg", "$location", "modals", "$ionicHistory", "error", "backend", "auth", "cache", "translit", "$window", function (e, t, n, o, r, i, a, s, l, c, d, u, p, f, g) {
    e.init = function () {
        mobile || (r.stylize("forgotPwd", r.getBgStyle()), d.addListener("pathsChanged", function () {
            r.stylize("forgotPwd", r.getBgStyle())
        })), e.$on("$ionicView.afterEnter", function () {
            p.setOb("title", replaceHTML(p.getOb("title"), f.get("forgotPwd")))
        }), e.native || e.desktop || e.$on("$ionicView.beforeEnter", function () {
            if (!t.initialized || d.cloudParam("public") || i.data.user && i.data.user.loggedIn) {
                var e = "loading";
                l.forwardView() ? e = l.forwardView().stateId : l.backView() && (e = l.backView().stateId), l.nextViewOptions({
                    disableAnimate: !0
                }), a.path("/" + e)
            }
        })
    }, e.showMenu = function () {
        var e = [{
            name: f.get("exit"),
            func: function () {
                n.hide(), s.show({
                    mode: "exit",
                    backdropClickToClose: !1,
                    exitOptions: {
                        exit: function () {
                            o.exit()
                        }
                    }
                })
            }
        }];
        n.show({
            mode: "cornerList",
            listOb: e,
            backdropClickToClose: !1
        })
    }, e.submit = function () {
        e.user && e.user.email && d.forgotPwd(e.user, function (e) {
            e.error ? c.update({
                error: "s_authError" == e.error ? "accessDenied" : e.error
            }) : (s.show({
                mode: "success",
                successOptions: {
                    msg: f.get("pwdEmailSent")
                }
            }), g.history.back())
        })
    }, e.backClicked = function () {
        g.history.back()
    }
}]);

app.controller("GlobalCtrl", ["modals", "popovers", "np", "$compile", "initializer", "translit", "notes", "$sce", "tinCan", "$scope", "backend", "stg", "$location", function (modals, popovers, np, $compile, initializer, translit, notes, $sce, tinCan, $scope, backend, stg, $location) {
    putTest = function (e) {
        return
    };

    $scope.init = function () {
        putTest("Global init");
        $scope.mobile = mobile; $scope.native = native;
        $scope.desktop = desktop;
        $scope.ios = ios;
        $scope.preview = preview;
        $scope.config = config;
        putTest("Global init good"), np.init(function () { })
    };

    $scope.start = function () {
        putTest("Global Start");
        putTest("TO HOME!");
        $scope.cloudParam = backend.cloudParam;
        modals.setScope($scope);
        popovers.setScope($scope);
        $scope.started = !0;
        putTest("A!");
        setTimeout(function () {
            putTest("timeout postInitialize"), initializer.postInitialize($scope)
        }, 700), $scope.$$phase || $scope.$apply()
    };

    $scope.tinCanShortcut = function (e) {
        tinCan.send(e)
    };

    $scope.questionsAddToNotebookShortcut = function (e, t) {
        notes.addNotesFromQuestions(e, t || angular.noop)
    };

    $scope.getNotebookShortcut = function (e, t) {
        stg.getNotes(function (n) {
            var o = [currentIndex];
            stg.data.currentRPageOb && o.push(stg.data.currentRPageOb.index);
            for (var r = 0; r < o.length; r++)
                for (var i = 0; i < n.length; i++) n[i].chapter == o[r] && 0 == n[i].location.indexOf(e.rootIndex) && (n[i].location = n[i].location.split(e.rootIndex)[1], notes.addIcon(n[i], e.root));
            t && t()
        })
    };

    $scope.location = function (e) {
        $location.path(e)
    };

    $scope.kTemplate = function (e) {
        return kTemplate(e)
    };

    $scope.translit = function (e) {
        return translit.get(e)
    };

    $scope.htmlSafe = function (e) {
        return $sce.trustAsHtml(e)
    };

    $scope.openAuthorWebsite = function () {
        var e = stg.data.settings.language;
        "en" != e && "es" != e && "ar" != e && "fr" != e && (e = "en"), window.open("https://www.kotobee.com/" + e + "/products/author", "_system")
    };

    $scope.applyNativeScroll = function (e) {
        var t, n = e.getElementsByClassName("kbContent");
        if (n.length && !(n[0].scrollHeight <= e.offsetHeight)) {
            e.children.length && "ion-scroll" == e.children[0].nodeName.toLowerCase() && (t = e.children[0], e.appendChild(n[0]), e.removeChild(t), e.scrollTop = 0), (t = document.createElement("ion-scroll")).setAttribute("scrollY", "true"), t.setAttribute("overflow-scroll", "false"), t.setAttribute("style", "width:100%;height:100%"), t.setAttribute("container-scroll", "true");
            for (var o = 0; o < e.children.length; o++) t.appendChild(e.children[o]);
            setTimeout(function () {
                var n = $compile(t.outerHTML)($scope);
                angular.element(e).append(n)
            }, 0)
        }
    };

    $scope.globalClick = function () { };

    $scope.globalTouch = function () { };

    $scope.refreshSignatures = function () {
        backend.refreshSignatures()
    };

    $scope.testModals = function () {
        modals.show({
            mode: "loading",
            loadingMsg: translit.get("pleaseWait")
        });
        modals.show({
            mode: "login",
            dynamic: !0,
            animation: "slide-in-up"
        });
        modals.show({
            mode: "login",
            dynamic: !0,
            animation: "slide-in-up"
        });
        modals.show({
            mode: "login",
            dynamic: !0,
            animation: "slide-in-up"
        });
        modals.show({
            mode: "bookInfo",
            dynamic: !0,
            animation: "slide-in-up"
        });
        modals.show({
            mode: "bookInfo",
            dynamic: !0,
            animation: "slide-in-up"
        });
        modals.show({
            mode: "update",
            dynamic: !0,
            animation: "slide-in-up",
            backdropClickToClose: !0
        });
        modals.show({
            mode: "bookInfo",
            dynamic: !0,
            animation: "slide-in-up"
        });
        modals.show({
            mode: "generalError"
        });
        modals.show({
            mode: "loading",
            loadingMsg: translit.get("pleaseWait"),
            backdropClickToClose: !0
        });
        modals.show({
            mode: "note",
            dynamic: !0,
            animation: "slide-in-up",
            backdropClickToClose: !0
        });
        modals.show({
            mode: "hlight"
        });
        modals.show({
            mode: "externalSite",
            dynamic: !0,
            animation: "slide-in-up",
            backdropClickToClose: !0
        });
        modals.show({
            mode: "bookInfo",
            dynamic: !0,
            animation: "slide-in-up"
        });
        modals.show({
            mode: "flashUnsupported",
            flashOb: {
                text: "Text for flash unsupported site"
            }
        });
        modals.show({
            mode: "success",
            successOptions: {
                msg: "SUCCESS MSG"
            }
        });
        modals.show({
            mode: "localOnlyError"
        });
        modals.show({
            mode: "xFrameError"
        });
        modals.show({
            mode: "networkError"
        });
        modals.show({
            mode: "exit"
        });
        modals.show({
            mode: "waitTimeout"
        });
        modals.show({
            mode: "backToLib"
        });
        modals.show({
            mode: "notebook",
            dynamic: !0,
            animation: "slide-in-up",
            scope: {
                notebookClose: t.hide
            }
        });
        modals.show({
            mode: "search",
            dynamic: !0,
            animation: "slide-in-up"
        });
        modals.show({
            mode: "image",
            dynamic: !0,
            animation: "slide-in-up"
        });
        modals.show({
            mode: "libraryInfo",
            dynamic: !0,
            animation: "slide-in-up"
        });
        modals.show({
            mode: "bookInfo",
            dynamic: !0,
            animation: "slide-in-up"
        });
        modals.show({
            mode: "encryptionKey",
            backdropClickToClose: !0
        });
        modals.show({
            mode: "loading",
            loadingMsg: translit.get("pleaseWait"),
            timeLimit: 3e3
        });
        return
    };
}]);

app.controller("HomeCtrl", ["$scope", "design", "backend", "initializer", "$ionicHistory", "$location", function ($scope, design, backend, initializer, $ionicHistory, $location) {
    function l() {
        var t = initializer.getLogo();
        $scope.$parent.logo = t.logo;
        $scope.$parent.slogan = t.slogan;
        var n = document.getElementById("home").getElementsByClassName("logo")[0],
            o = n.getElementsByTagName("img")[0],
            i = n.style.backgroundImage;
        if (!i) {
            var l = "";
            config.hideLogo ? l += "display:none" : l += "background-image:url('" + t.logo + "');", n.style.cssText = l, putTest("HomeCtrl1")
        }
        o.onload = function () {
            if (!i) {
                var e = window,
                    t = document,
                    r = t.documentElement,
                    a = t.getElementsByTagName("body")[0],
                    s = e.innerWidth || r.clientWidth || a.clientWidth,
                    c = e.innerHeight || r.clientHeight || a.clientHeight;
                s || (s = e.screen.width), c || (c = e.screen.height);
                var d = {};
                d.width = s - 80, d.height = .5 * c - 40, l += "opacity:1;", d.height < o.height || d.width < o.width ? l += "background-size:contain;" : l += "background-size:auto;", n.style.cssText = l
            }
        };
        putTest("HomeCtrl2");
        initializer.initialized || initializer.initializing || initializer.preInitialize($scope); putTest("k"), $scope.native || $scope.desktop || (putTest("k2"), $scope.$on("$ionicView.beforeEnter", function () {
            putTest("be4 enter"), initializer.initialized && (putTest("obbaa"), $ionicHistory.forwardView() && (putTest("yup"), $ionicHistory.nextViewOptions({
                disableAnimate: !0
            }), $location.path("/" + $ionicHistory.forwardView().stateId)))
        }))
    }
    $scope.init = function () {
        putTest("HomeCtrl!"), l(), backend.addListener("pathsChanged", l)
    };

    $scope.injectUserCSS = function (e) {
        design.injectUserCSS(e)
    };
}]);

app.controller("LibraryCtrl", ["$scope", "error", "bookinfo", "autohide", "$location", "$ionicPlatform", "$rootScope", "$ionicHistory", "auth", "modals", "popovers", "library", "book", "$timeout", "$ionicScrollDelegate", "backend", "crdv", "$filter", "stg", "settings", function ($scope, error, bookinfo, autohide, $location, $ionicPlatform, $rootScope, $ionicHistory, auth, modals, popovers, library, book, $timeout, $ionicScrollDelegate, backend, crdv, $filter, stg, settings) {
    function O(t) {
        for (var n = 0; n < $scope.sortOb.options.length; n++)
            if ($scope.sortOb.options[n].val == t) return $scope.sortOb.options[n]
    }

    function S() {
        if (library.initialize(), library.addListener(A), autohide.libInitialized(), $scope.initialized = !0, $scope.catList = stg.data.currentLibrary.categories, stg.data.user.loggedIn && ("account" != stg.data.currentLibrary.authbookview || backend.cloudParam("public") || ($scope.supportsAccountView = !0)), "categories" == stg.data.currentLibrary.startingpage) P(), N("categories");
        else if (0 == $location.path().indexOf("/library/tab/downloads")) $scope.downloadedClicked();
        else if ($scope.supportsAccountView) N("account"), $timeout(function () {
            M({
                account: !0
            })
        });
        else if (!native && !desktop || stg.data.user.loggedIn || config.kotobee.public || !config.kotobee.offline) {
            if (N("all"), !backend.cloudParam("public") && "hide" == stg.data.currentLibrary.authbookview && !stg.data.user.loggedIn) return $scope.paneMsg = "noEbooksAvailable", void P();
            $timeout(M)
        } else N("downloaded"), $scope.paneMsg = null, 0 == stg.data.downloaded.length && ($scope.paneMsg = "noEbooksDownloaded"), P();
        library.startBook && (L(library.startBook), library.startBook = null)
    }

    function A() {
        var t, n = arguments[0];
        if ("preload" == n && (arguments[1].accumulated || (t = !0), mobile || (t = !0), $scope.paneMsg = null, t && $()), "postload" == n) {
            arguments[1];
            var o = arguments[2];
            t = !1, $scope.stopInfiniteScroll(), D = !1, $scope.stopRefresher();
            var r = B(o);
            $scope.moreData = r >= library.maxResults, $scope.moreData && library.setMax(r)
        }
    }

    function L(e) {
        if (stg.data.user.loggedIn) {
            if (stg.data.currentLibrary && stg.data.currentLibrary.books)
                for (var t = 0; t < stg.data.currentLibrary.books.length; t++)
                    if (stg.data.currentLibrary.books[t].id == e) return void book.open(stg.data.currentLibrary.books[t]);
            backend.getEbook({
                bid: e
            }, function (e) {
                book.open(e)
            })
        }
    }

    function B(e) {
        if (U) {
            for (var t = 0, n = 0; n < e.length; n++)
                if (e[n].searchresults)
                    for (var o = 0; o < e[n].searchresults.length; o++) e[n].searchresults[o].results && (t += e[n].searchresults[o].results.length);
            return t
        }
        return e.length
    }

    function N(t) {
        $scope.tab = t, mobile ? $ionicScrollDelegate.$getByHandle("thumbs").scrollTop(!1) : document.getElementById("libraryThumbs").scrollTop = 0
    }

    function M(t) {
        t || (t = {}), $scope.paneMsg = null, z && (t.catid = z.id, t.listmode = z.listmode), F && (t.key = F), U && (t.contentkey = U), library.getEbooks(t, function (t) {
            "all" != $scope.tab && "account" != $scope.tab || (t.empty && ($scope.paneMsg = "noEbooksAvailable"), $scope.spaceAvailable() || P(), $scope.$$phase || $scope.$apply())
        })
    }

    function P() {
        for (var e = document.getElementsByClassName("libLoaderAnim"), t = 0; t < e.length; t++) addClass(e[t], "hide")
    }

    function $() {
        for (var e = document.getElementsByClassName("libLoaderAnim"), t = 0; t < e.length; t++) removeClass(e[t], "hide")
    }

    function H(e, t) {
        t || (t = stg.data.currentLibrary.categories);
        for (var n = 0; n < t.length; n++) {
            if (t[n].id == e) return t[n];
            if (t[n].children) {
                var o = H(e, t[n].children);
                if (o) return o
            }
        }
    }

    function R() {
        j && j(), j = $ionicPlatform.registerBackButtonAction(function (t) {
            $scope.searchOb.enabled ? $scope.disableSearch() : "categories" == $scope.tab && $scope.catList != stg.data.currentLibrary.categories ? $scope.categoryBackClicked() : z ? $scope.back() : F ? $scope.back() : U && $scope.back(), $scope.$apply()
        }, 102)
    }
    var D, F, z, j, U;
    $scope.initialized = !1;

    $scope.init = function () {
        document.getElementsByClassName("libraryView");
        backend.setLibraryLoginCallback(function () {
            S()
        });

        backend.setLibraryLogoutCallback(function () {
            $scope.supportsAccountView && "all" != $scope.tab && (N("all"), $timeout(M)), $scope.supportsAccountView = !1, P()
        });

        $scope.sortOb = {};
        $scope.sortOb.options = [
            {
                name: "dateAddedNewestFirst",
                data: "date",
                dir: "desc",
                val: "dateDesc"
            }, {
                name: "dateAddedOldestFirst",
                data: "date",
                dir: "asc",
                val: "dateAsc"
            }, {
                name: "titleA-Z",
                data: "name",
                dir: "asc",
                val: "titleAsc"
            }, {
                name: "titleZ-A",
                data: "name",
                dir: "desc",
                val: "titleDesc"
            }, {
                name: "categoryA-Z",
                data: "cat",
                dir: "asc",
                val: "catAsc"
            }, {
                name: "categoryZ-A",
                data: "cat",
                dir: "desc",
                val: "catDesc"
            }];
        $scope.sortOb.method = O("dateDesc"), stg.data.currentLibrary && stg.data.currentLibrary.defaultsort && ($scope.sortOb.method = O(stg.data.currentLibrary.defaultsort)), library.sortMethod = $scope.sortOb.method;
        var t;

        $scope.$on("$ionicView.beforeEnter", function (n, o) {
            if (t = null, !($scope.native || $scope.desktop || auth.authenticated && "library" == config.kotobee.mode)) {
                var r = "loading";
                $ionicHistory.forwardView() ? r = $ionicHistory.forwardView().stateId : $ionicHistory.backView() && (r = $ionicHistory.backView().stateId), $ionicHistory.nextViewOptions({
                    disableAnimate: !0
                }), $location.path("/" + r)
            }
            $ionicHistory.forwardView() && (t = $ionicHistory.forwardView().stateId), $ionicHistory.backView() && (t = $ionicHistory.backView().stateId), "forward" == o.direction && (stg.data.currentLibrary.books = [], stg.data.favorites = [])
        });

        $scope.$on("$ionicView.afterEnter", function (e, t) {
            if ("back" == t.direction) return P(), void library.setBrowserTitle();

            backend.cloudParam("entry") ? backend.login().then(function (e) {
                e.error && S()
            }) : (backend.cloudParam("public"), S())
        });
    };

    $scope.userLogin = function () {
        $scope.data.user.loggedIn ? backend.logout() : error.update({
            error: "s_authError"
        })
    };

    $scope.langSelected = function (t) {
        $scope.langDropdownExpanded = !1, stg.data.settings.language = t, settings.langChanged()
    };

    $scope.showLibraryMenu = function () {
        popovers.show({
            mode: "libraryMenu",
            menuItem: $scope.menuItemClicked,
            backdropClickToClose: !1
        })
    };

    $scope.spaceAvailable = function () {
        var t = document.getElementById("libraryThumbs"),
            n = document.getElementById("libraryThumbsContent");

        if (t.offsetHeight > n.offsetHeight) {
            if (D) return;
            if (!$scope.moreDataExists()) return;
            return D = !0, $scope.loadMore(), !0
        }
    };

    $scope.scroll = function (t) {
        var n = U ? .95 : .8;
        if (t || (t = document.getElementById("libraryThumbs")), t.scrollTop / (document.getElementById("libraryThumbsContent").offsetHeight - t.offsetHeight) > n) {
            if (D) return;
            if (!$scope.moreDataExists()) return;
            D = !0, $scope.loadMore()
        }
    };

    $scope.bookClicked = function (t) {
        bookinfo.show({
            book: t,
            categoryClicked: $scope.categoryClicked
        })
    };

    $scope.refresherExists = function () {
        try {
            if (!stg.data.currentLibrary) return !1;
            if ("categories" == $scope.tab) return !1;
            if ("downloaded" == $scope.tab) return !1;
            if ("favorites" == $scope.tab) return !0;
            if ("account" == $scope.tab) return !0;
            if ("all" == $scope.tab) return !0
        } catch (e) {
            return !1
        }
        return !0
    };

    $scope.doRefresh = function () {
        var t = {
            accumulated: !0
        };
        "all" == $scope.tab ? M(t) : "account" == $scope.tab ? (t.account = !0, M(t)) : "favorites" == $scope.tab ? $scope.loadFavorites(t) : $scope.stopRefresher()
    };

    $scope.sort = function () {
        library.sortMethod = $scopee.sortOb.method, "downloaded" == $scope.tab ? $filter("orderBy")($scope.data.downloaded, $scope.sortOb.method.data) : "account" == $scope.tab ? M({
            account: !0
        }) : M()
    };

    $scope.menuItemClicked = function (n) {
        "language" == n && settings.langChanged(), "redeemCode" == n && (popovers.hide(), $scope.redeemCodeClicked()), "info" == n && (popovers.hide(), modals.show({
            mode: "libraryInfo",
            dynamic: !0,
            animation: "slide-in-up",
            libraryInfo: stg.data.currentLibrary.libraryinfo
        })), "logout" == n && (popovers.hide(), $scope.data.user.pwd || $scope.data.user.code ? backend.logout() : error.update({
            error: "s_authError"
        })), "exit" == n && (popovers.hide(), modals.show({
            mode: "exit",
            backdropClickToClose: !1,
            exitOptions: {
                exit: function () {
                    crdv.exit()
                }
            }
        }))
    };
    $scope.loadFavorites = function (t) {
        t || (t = {}), $scope.paneMsg = null, z && (t.catid = z.id), F && (t.key = F), U && (t.contentkey = U), t.favs = !0, library.getEbooks(t, function (t) {
            "favorites" == $scope.tab && (t.error && ($scope.paneMsg = "failedToGetFavs"), 0 == t.length && ($scope.paneMsg = "noFavsAvailable"), $scope.spaceAvailable() || P())
        })
    };

    $scope.searchEbooksOld = function (t) {
        $scope.subMode = "search", $scope.title = "searchResults", F = library.currentKey = t, z = library.currentCat = null, $scope.supportsAccountView && "all" != $scope.tab ? (N("account"), $timeout(function () {
            M({
                account: !0
            })
        })) : (N("all"), $timeout(M)), R()
    };

    $scope.searchEbooks = function (n) {
        if (n && n.length && 0 != n.length) {
            if (n.length < 3) return error.update({
                error: "lessThan3Characters"
            }), void modals.hide();

            $scope.subMode = "search";
            $scope.title = "searchResults";
            config.searchLibContent ? U = library.currentContentKey = n : F = library.currentKey = n, z = library.currentCat = null;
            $scope.supportsAccountView && "all" != $scope.tab ? (N("account"), $timeout(function () {
                M({ account: !0 })
            })) : (N("all"), $timeout(M)), R()
        }
    };

    $scope.enableSearch = function () {
        $scope.searchOb.enabled = !0, R()
    };

    $scope.disableSearch = function () {
        $scope.searchOb.enabled = !1, j && j()
    };

    $scope.loadMore = function () {
        var t = {
            more: !0,
            accumulated: !0
        };
        "all" == $scope.tab ? M(t) : "account" == $scope.tab ? (t.account = !0, M(t)) : "favorites" == $scope.tab ? $scope.loadFavorites(t) : ($scope.stopInfiniteScroll(), D = !0)
    };

    $scope.stopInfiniteScroll = function () {
        $rootScope.$broadcast("scroll.infiniteScrollComplete")
    };

    $scope.stopRefresher = function () {
        $rootScope.$broadcast("scroll.refreshComplete")
    };

    $scope.moreDataExists = function () {
        try {
            if (!stg.data.currentLibrary) return !1;
            if ("categories" == $scope.tab) return !1;
            if ("downloaded" == $scope.tab) return !1;
            var t;
            if ("favorites" == $scope.tab && (t = $scope.data.favorites), "all" == $scope.tab && (t = stg.data.currentLibrary.books), "account" == $scope.tab && (t = stg.data.currentLibrary.books), 0 == t.length) return !1
        } catch (e) {
            return !1
        }
        return $scope.moreData
    };

    $scope.back = function () {
        $scope.subMode = !1, $scope.title = null, F = library.currentKey = null, U = library.currentContentKey = null, z = library.currentCat = null, $scope.supportsAccountView && "all" != $scope.tab ? (N("account"), $timeout(function () {
            M({
                account: !0
            })
        })) : (N("all"), $timeout(M)), j && j()
    };

    $scope.categoryBackClicked = function () {
        if ($scope.catList && $scope.catList.length) {
            var t = $scope.catList[0].parent;
            for (t = H(t.id); ;) {
                if (!(t = t.parent)) return void ($scope.catList = stg.data.currentLibrary.categories);
                if ("separate" == (t = H(t.id)).submode) return void ($scope.catList = t.children)
            }
        }
    };

    $scope.categoryClicked = function (t) {
        if (native && crdv.vibrate(), "expand" != t.submode)
            if ("separate" != t.submode) {
                var n = {};
                z = library.currentCat = t, F = library.currentKey = null, U = library.currentContentKey = null, n.catid = z.id, $scope.subMode = "category", $scope.title = z.name, $scope.supportsAccountView ? (N("account"), $timeout(function () {
                    M({
                        account: !0
                    })
                })) : (N("all"), $timeout(M)), R()
            } else $scope.catList = t.children;
        else t.expand = !t.expand
    };

    $scope.categoriesClicked = function () {
        native && crdv.vibrate(), N("categories"), $scope.paneMsg = null, P()
    };

    $scope.accountClicked = function () {
        native && crdv.vibrate(), N("account"), P(), $timeout(function () {
            M({
                account: !0
            })
        })
    };

    $scope.allClicked = function () {
        native && crdv.vibrate(), N("all"), $timeout(M)
    };

    $scope.downloadedClicked = function () {
        native && crdv.vibrate(), N("downloaded"), $scope.paneMsg = null, 0 == stg.data.downloaded.length && ($scope.paneMsg = "noEbooksDownloaded"), P()
    };

    $scope.favoritesClicked = function () {
        native && crdv.vibrate(), N("favorites"), $scope.loadFavorites()
    };

    $scope.redeemCodeClicked = function () {
        auth.redeemCode({
            source: "topmenu"
        }, function () {
            $scope.allClicked()
        })
    };
}]);

app.controller("LoginCtrl", ["$scope", "initializer", "crdv", "$ionicHistory", "popovers", "design", "modals", "error", "backend", "auth", "stg", "cache", "translit", "$location", function ($scope, initializer, crdv, $ionicHistory, popovers, design, modals, error, backend, auth, stg, cache, translit, $location) {
    $scope.model = {};

    $scope.init = function () {
        mobile || (design.stylize("login", design.getBgStyle()), backend.addListener("pathsChanged", function () {
            design.stylize("login", design.getBgStyle())
        })), "email" == config.kotobee.loginmode ? $scope.canLoginByEmail = !0 : "code" == config.kotobee.loginmode ? $scope.canLoginByCode = !0 : $scope.canLoginByEmail = $scope.canLoginByCode = !0, $scope.mode = $scope.canLoginByEmail ? "email" : "code", $scope.$on("$ionicView.afterEnter", function () {
            cache.setOb("title", replaceHTML(cache.getOb("title"), translit.get("login")))
        }), $scope.native || $scope.desktop || $scope.$on("$ionicView.beforeEnter", function () {
            if (!initializer.initialized || config.kotobee.public || stg.data.user.loggedIn) {
                var e = "loading";
                $ionicHistory.forwardView() ? e = $ionicHistory.forwardView().stateId : $ionicHistory.backView() && (e = $ionicHistory.backView().stateId), $ionicHistory.nextViewOptions({
                    disableAnimate: !0
                }), $location.path("/" + e)
            }
        })
    };

    $scope.showMenu = function () {
        var e = [{
            name: translit.get("exit"),
            func: function () {
                popovers.hide(), modals.show({
                    mode: "exit",
                    backdropClickToClose: !1,
                    exitOptions: {
                        exit: function () {
                            crdv.exit()
                        }
                    }
                })
            }
        }];
        popovers.show({
            mode: "cornerList",
            listOb: e,
            backdropClickToClose: !1
        })
    };

    $scope.login = function () {
        function t() {
            backend.login({
                dontShowErrorDialog: !0,
                userInput: !0
            }).then(function (t) {
                t.error ? error.update({
                    error: "s_authError" == t.error ? "accessDenied" : t.error
                }) : ($scope.model.pwd = null, $scope.model.code = null, auth.startup())
            })
        }
        if ($scope.model) {
            if ("code" == $scope.mode) {
                if (!$scope.model.code) return void error.update({
                    error: "enterYourCode"
                });
                stg.data.user.code = $scope.model.code, stg.data.user.email = stg.data.user.pwd = null
            } else {
                if (!stg.data.user.email) return void error.update({
                    error: "enterYourEmail"
                });
                if (!$scope.model.pwd) return void error.update({
                    error: "enterYourPwd"
                });
                stg.data.user.pwd = $scope.model.pwd, stg.data.user.code = null
            }
            config.kotobee.permsLoaded ? t() : auth.getPermissions(t)
        }
    };

    $scope.register = function () {
        modals.hide(), $location.path("/register")
    }

    $scope.forgotPassword = function () {
        $location.path("/forgotpwd")
    };

    $scope.signInByEmail = function () {
        $scope.mode = "email"
    };

    $scope.redeemCode = function () {
        $scope.mode = "code"
    };
}]);

app.controller("MediaCtrl", ["media", "$scope", function (media, $scope) {
    $scope.init = function () { };

    $scope.mediaClicked = function (e) {
        media.show(e)
    };

    $scope.hide = function () {
        media.hide()
    };
}]);

app.controller("NotebookCtrl", ["$scope", "$ionicScrollDelegate", "$timeout", "crdv", "translit", "$rootScope", "selection", "book", "chapters", "modals", "hlights", "stg", function ($scope, $ionicScrollDelegate, $timeout, crdv, translit, $rootScope, selection, book, chapters, modals, hlights, stg) {
    $scope.model = {}, $scope.ui = {}, $scope.noteBtns = {
        notes: !0,
        bookmarks: !0,
        hlights: !0
    };

    $scope.init = function () { };

    $scope.notebookClose = function () {
        modals.hide(), $scope.bookmarkContent = null, $scope.noteContent = null, $scope.hlightContent = null, $scope.notebook = {}
    };

    $scope.noteBtnClicked = function (e, n) {
        "all" == e ? n.notes = n.bookmarks = n.hlights = !(n.notes && n.bookmarks && n.hlights) : (n.notes = n.bookmarks = n.hlights = !1, n[e] = !0), overflowScroll ? document.getElementById("notebookContent").scrollTop = 0 : $ionicScrollDelegate.$getByHandle("notebookScroll").scrollTop()
    };

    $scope.pdfExport = function () {
        var t = $scope.serializeNotebookHtml(),
            i = (stg.data.book.meta.dc.title ? stg.data.book.meta.dc.title : translit.get("book")) + ".pdf";
        $timeout(function () {
            crdv.createPDF(t, i, function (e) { })
        }, 500)
    };

    $scope.serializeNotebookHtml = function () {
        function e(e, t, n) {
            for (var o = "", r = 0; r < e.length; r++) {
                var i = e[r],
                    a = /^[\u0000-\u007fñáéíóúü]*$/.test(i);
                if (a) o += a ? i : "<em>Non-unicode text</em>";
                else
                    for (var s = 0; s < i.length; s++) o += (a = /^[\u0000-\u007fñáéíóúü]*$/.test(i[s])) ? i[s] : "?"
            }
            return "<" + t + (n ? " " + n : "") + ">" + o + "</" + t + ">"
        }
        var t = "<br/>",
            n = '<html><head><meta charset="UTF-8" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body>';
        n += e([translit.get("notebookFor"), ' "' + stg.data.book.meta.dc.title + '"'], "h1"), n += t + t;
        for (var o = stg.data.notebookContent.results, i = 0; i < o.length; i++)
            if (o[i]) {
                var a = "";
                a += e(book.getTitleByIndex(i), "h2") + t;
                var l = "..................................................................................";
                a += e([l + l], "p");
                for (var c = 0; c < o[i].length; c++) {
                    var d = o[i][c],
                        u = "";
                    null != d.nid ? "page" == d.type ? u += e([translit.get("pageNote") + ": ", d.note], "p") + t : "content" == d.type && (u += e([translit.get("noteFor"), ' "' + d.src + '"'], "p") + t, u += e([translit.get("note") + ": ", d.note], "p") + t) : null != d.hid ? (u += e([translit.get("highlightFor"), ' "' + d.src + '"'], "p") + t, u += e([translit.get("highlightColor") + ": " + d.color + t], "p", 'style="background-color:' + d.color + '"')) : null != d.bmid && (u += e([translit.get("contentBookmark")], "p") + t), a += e([u], "p")
                }
                n += e([a], "p", 'style="border:solid 3px #000;background-color:#ddd"')
            }
        return n += "</body></html>"
    };

    $scope.shareNotebook = function () {
        var t = $scope.serializeNotebookTxt();
        crdv.share(t)
    };

    $scope.serializeNotebookTxt = function () {
        for (var e = "\n", t = translit.get("notebookFor") + ' "' + stg.data.book.meta.dc.title + '"' + e, n = t.length; n--;) t += "#";
        t += e + e;
        for (var o = stg.data.notebookContent.results, i = 0; i < o.length; i++)
            if (o[i]) {
                var a = translit.get("chapter") + " " + (i + 1);
                t += a + e;
                for (n = a.length; n--;) t += "_";
                t += e + e;
                for (var s = 0; s < o[i].length; s++) {
                    var l = o[i][s];
                    null != l.nid ? "page" == l.type ? t += translit.get("pageNote") + ": " + l.note + e : "content" == l.type && (l.src ? t += translit.get("noteFor") + ' "' + l.src + '"' + e : t += translit.get("note") + e, t += translit.get("note") + ": " + l.note + e) : null != l.hid ? (t += translit.get("highlightFor") + ' "' + l.src + '"' + e, t += translit.get("highlightColor") + " " + l.color + e) : null != l.bmid && (t += translit.get("contentBookmark") + e), t += "........................\n\n"
                }
            }
        return t
    };

    $scope.deleteFromNotebookContent = function (e, t) {
        for (var n = stg.data.notebookContent.results, o = 0; o < n.length; o++)
            if (n[o])
                for (var r = 0; r < n[o].length; r++)
                    if (n[o][r][e] == t) return n[o].splice(r, 1), void (n[o].length || n.splice(o, 1))
    };

    $scope.noteItemClicked = function (e) {
        var t = e.chapter,
            o = e.location;
        o = String(o).split(",")[0], modals.hide(), $timeout(function () {
            chapters.redirect(t, {
                hash: o
            })
        }, 100)
    };

    $scope.deleteNote = function (t, n) {
        stg.deleteNote(t, null, function (n) {
            $scope.noteContent = n, selection.deleteNoteMarker(t), $scope.deleteFromNotebookContent("nid", t);
            for (var o = document.getElementsByClassName("note" + t), r = o.length; r--;) r == o.length - 1 && (removeClass(o[r], "last"), o[r].removeAttribute("nloc"), o[r].removeAttribute("ncontent")), removeClass(o[r], "contentNoteBG"), removeClass(o[r], "note" + t);
            $rootScope.$$phase || $rootScope.$apply()
        }), n.stopPropagation()
    };

    $scope.deleteBookmark = function (t, n) {
        stg.deleteBookmark(t, null, function (n) {
            $scope.bookmarkContent = n, selection.deleteBookMarker(t), $scope.deleteFromNotebookContent("bmid", t), $rootScope.$$phase || $rootScope.$apply()
        }), n.stopPropagation()
    };

    $scope.deleteHlight = function (t, n) {
        hlights.delete(t, null, function (o) {
            $scope.hlightContent = o, $scope.deleteFromNotebookContent("hid", t), n.stopPropagation(), $rootScope.$$phase || $rootScope.$apply()
        })
    };
}]);

app.controller("PreviewCtrl", ["modals", "popovers", "bookinfo", "np", "$rootScope", "translit", "$scope", "backend", "stg", function (modals, popovers, bookinfo, np, $rootScope, translit, $scope, backend, stg) {
    $scope.init = function () {
        putTest("Preview init"), $scope.mobile = mobile, $scope.native = native, $scope.desktop = desktop, $scope.ios = ios, $scope.preview = preview, $scope.config = config, putTest("Global init good"),
            np.init(function () { });
    };

    $scope.start = function () {
        $rootScope.data = stg.data, $rootScope.view = stg.view, $scope.cloudParam = backend.cloudParam, modals.setScope($scope), popovers.setScope($scope), $scope.started = !0
    };

    $scope.previewPage = function (e, t) {
        $scope.started ? (e || (e = "bookInfo"), t || (t = {
            lang: "en",
            book: {
                name: "Alice",
                description: "A great book indeed"
            }
        }), "bookInfo" == e && (config = t.config ? t.config : {
            kotobee: {
                entry: 1,
                mode: "library"
            }
        }, stg.data.user = t.user ? t.user : {
            loggedIn: !0
        }, stg.data.currentLibrary = t.library ? t.library : {}, translit.setLang(t.lang ? t.lang : "en"), bookinfo.show({
            book: t.book
        }, {
                animation: "none"
            }))) : setTimeout(function () {
                $scope.previewPage(e, t)
            }, 100)
    };

    $scope.kTemplate = function (e) {
        return kTemplate(e)
    };

    $scope.translit = function (e) {
        return translit.get(e)
    };

    $scope.htmlSafe = function (e) {
        return f.trustAsHtml(e)
    };
}]);

app.controller("ReaderCtrl", ["modals", "library", "$location", "$scope", "chapters", "book", "stg", function (modals, library, $location, $scope, chapters, book, stg) {
    $scope.init = function () {
        $scope.mode = "reading"
    };

    $scope.chapterClicked = function (e) {
        chapters.displayChapters()
    };

    $scope.backClicked = function (e) {
        l.isOpenLeft() || modals.show({
            mode: "backToLib",
            backdropClickToClose: !1,
            backToLibOptions: {
                back: function () {
                    modals.hide(), book.removeBookConfigElem(), chapters.clearContent(), setTimeout(library.setBrowserTitle, 600), setTimeout(stg.resetBook, 600), $location.path("/library")
                }
            }
        })
    };

    $scope.searchClicked = function (e) {
        $scope.applyHeader("search")
    };

    $scope.menuClicked = function (e) { };

    $scope.setting1Clicked = function (e) { };
    $scope.setting2Clicked = function (e) { };
    $scope.setting3Clicked = function (e) { };
    $scope.linkClicked = function () { }
}]);

app.controller("RedeemCodeCtrl", ["$scope", "initializer", "popovers", "crdv", "design", "stg", "$location", "modals", "$ionicHistory", "error", "backend", "auth", "cache", "translit", "$window", function ($scope, initializer, popovers, crdv, design, stg, $location, modals, $ionicHistory, error, backend, auth, cache, translit, $window) {
    $scope.init = function () {
        mobile || (design.stylize("redeemCode", design.getBgStyle()), backend.addListener("pathsChanged", function () {
            design.stylize("redeemCode", design.getBgStyle())
        })), $scope.$on("$ionicView.afterEnter", function () {
            cache.setOb("title", replaceHTML(cache.getOb("title"), translit.get("redeemCode")))
        }), $scope.native || $scope.desktop || $scope.$on("$ionicView.beforeEnter", function () {
            if (!initializer.initialized || backend.cloudParam("public") || stg.data.user && stg.data.user.loggedIn) {
                var e = "loading";
                $ionicHistory.forwardView() ? e = $ionicHistory.forwardView().stateId : $ionicHistory.backView() && (e = $ionicHistory.backView().stateId), $ionicHistory.nextViewOptions({
                    disableAnimate: !0
                }), $location.path("/" + e)
            }
        })
    };

    $scopee.showMenu = function () {
        var e = [{
            name: translit.get("exit"),
            func: function () {
                popovers.hide(), modals.show({
                    mode: "exit",
                    backdropClickToClose: !1,
                    exitOptions: {
                        exit: function () {
                            crdv.exit()
                        }
                    }
                })
            }
        }];
        popovers.show({
            mode: "cornerList",
            listOb: e,
            backdropClickToClose: !1
        })
    };

    $scope.submit = function () {
        function t() {
            backend.cloudParam("entry") ? ($window.history.back(), backend.login()) : backend.login().then(function (e) {
                e.error ? error.update({
                    error: "s_authError" == e.error ? "accessDenied" : e.error
                }) : auth.startup()
            })
        }
        $scope.user && $scope.user.code && (stg.data.user.code = $scope.user.code, stg.data.user.email = stg.data.user.pwd = null, config.kotobee.permsLoaded ? t() : auth.getPermissions(t))
    };

    $scope.backClicked = function () {
        $window.history.back()
    };
}]);

app.controller("RegisterCtrl", ["$scope", "initializer", "popovers", "crdv", "modals", "design", "stg", "$ionicHistory", "$location", "translit", "backend", "backend", "translit", "cache", "error", "$window", function ($scope, initializer, popovers, crdv, modals, design, stg, $ionicHistory, $location, c, d, d, c, cache, error, $window) {
    function g() {
        var t = $scope.user;
        return -1 == t.email1.indexOf("@") ? (error.update({
            error: "emailFormatError"
        }), !1) : /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test(t.email1) ? t.email1 == t.email2 || (error.update({
            error: "emailsDontMatch"
        }), !1) : (error.update({
            error: "emailFormatError"
        }), !1)
    };

    $scope.init = function () {
        $scope.user = {
            email1: "",
            email2: ""
        }, mobile || (design.stylize("register", design.getBgStyle()), d.addListener("pathsChanged", function () {
            design.stylize("register", design.getBgStyle())
        })), $scope.$on("$ionicView.afterEnter", function () {
            cache.setOb("title", replaceHTML(cache.getOb("title"), c.get("register")))
        }), $scope.native || $scope.desktop || $scope.$on("$ionicView.beforeEnter", function () {
            if (!initializer.initialized || config.kotobee.public || stg.data.user.loggedIn) {
                var e = "loading";
                $ionicHistory.forwardView() ? e = $ionicHistory.forwardView().stateId : $ionicHistory.backView() && (e = $ionicHistory.backView().stateId), $ionicHistory.nextViewOptions({
                    disableAnimate: !0
                }), $location.path("/" + e)
            }
        })
    };

    $scope.showMenu = function () {
        var e = [{
            name: c.get("exit"),
            func: function () {
                popovers.hide(), modals.show({
                    mode: "exit",
                    backdropClickToClose: !1,
                    exitOptions: {
                        exit: function () {
                            crdv.exit()
                        }
                    }
                })
            }
        }];
        popovers.show({
            mode: "cornerList",
            listOb: e,
            backdropClickToClose: !1
        })
    };

    $scope.create = function () {
        g() && d.register($scope.user, function (e) {
            if (e.error) error.update(e);
            else {
                var t = "d_accountCreated";
                e.success && e.success.approvalRequired && (t = "d_accountCreatedApprovalRequired"), modals.show({
                    mode: "success",
                    successOptions: {
                        msg: c.get(t)
                    }
                }), $window.history.back()
            }
        })
    };

    $scope.backClicked = function () {
        $window.history.back()
    };
}]);

app.controller("RendererCtrl", ["$rootScope", "$ionicHistory", "modals", "nav", "backend", "book", "auth", "status", "error", "translit", "$ionicPlatform", "bookmarks", "notes", "$ionicSideMenuDelegate", "$scope", "$ionicScrollDelegate", "chapters", "$location", "$timeout", "selection", "crdv", "stg", "search", function ($rootScope, $ionicHistory, modals, nav, backend, book, auth, status, error, translit, $ionicPlatform, bookmarks, notes, $ionicSideMenuDelegate, $scope, $ionicScrollDelegate, chapters, $location, $timeout, selection, crdv, stg, search) {
    $scope.init = function () {
        $rootScope.renderScope = $scope, selection.listener = $scope.selection, $scope.searchContent = {}, $scope.sideMenuMode = "bookSettings";
        var r = $ionicPlatform.registerBackButtonAction(function (t) {
            kInteractive.frameIsOpen ? kInteractive.closeFrame() : kInteractive.alertIsOpen ? kInteractive.closeAlert() : kInteractive.videoIsFullscreen ? kInteractive.closeFullscreenVideo() : "default" != $scope.header.mode ? "selection" == $scope.header.mode ? selection.reset() : $scope.applyHeader("default") : stg.data.book.chapter.viewport && nav.getFxlScale() != nav.getFxlFit().bestFit ? o.fxlZoomFit() : "library" == config.kotobee.mode ? $location.path("/library") : $rootScope.readerApp ? $location.path("/app") : modals.show({
                mode: "exit",
                backdropClickToClose: !1,
                exitOptions: {
                    exit: function () {
                        crdv.exit()
                    }
                }
            }), $scope.$apply()
        }, 103);

        if ($scope.$on("$destroy", r), !desktop && !chromeApp && !preview && ZeroClipboard) {
            ZeroClipboard.config({
                trustedDomains: ["*"],
                trustedOrigins: ["*"]
            });
            var i = new ZeroClipboard(document.getElementById("copyBtn"));
            i.on("ready", function (e) {
                i.on("aftercopy", function (e) {
                    e.target.style.display = "none", status.showAndHide(translit.get("copiedToClipboard"), 500)
                })
            }), i.on("error", function (e) {
                noFlash = !0
            })
        };

        $scope.native || $scope.desktop || $scope.$on("$ionicView.beforeEnter", function () {
            if (!auth.authenticated) {
                var e = "loading";
                $ionicHistory.forwardView() ? e = $ionicHistory.forwardView().stateId : $ionicHistory.backView() && (e = $ionicHistory.backView().stateId), $ionicHistory.nextViewOptions({
                    disableAnimate: !0
                }), $location.path("/" + e)
            }
        });

    };

    $scope.setSideMenuMode = function (e) {
        $scope.sideMenuMode = e
    };

    $scope.applyHeader = function (e) {
        try {
            $scope.header.mode = e
        } catch (e) { }
    };

    $scope.header = {
        mode: "default"
    };

    $scope.selection = function (e) {
        mobile ? "activated" == e || "selectionFixed" == e ? $scope.applyHeader("selection") : "deactivated" == e && $scope.applyHeader("default") : "selectionFixed" == e ? (addClass(document.getElementById("selectionOptions"), "showBar"), document.getElementById("copyBtn").setAttribute("data-clipboard-text", selection.getSelectedText())) : "deactivated" == e && removeClass(document.getElementById("selectionOptions"), "showBar")
    };

    $scope.contentLink = function (e) {
        chapters.applyChapterStateFromURL(e.getAttribute("href"))
    };

    $scope.chapterLink = function (e) {
        if (e.parentElement.getElementsByTagName("ol").length > 0) {
            var t = e.offsetHeight + 20;
            if (overflowScroll || $ionicScrollDelegate.$getByHandle("chapters").scrollBy(0, t, !0), !e.getAttribute("href") || e.getAttribute("data-dontshow")) return void (hasClass(e.parentElement, "expanded") ? removeClass(e.parentElement, "expanded") : addClass(e.parentElement, "expanded"));
            addClass(e.parentElement, "expanded")
        }
        $ionicSideMenuDelegate.toggleLeft(!1), $ionicSideMenuDelegate.toggleRight(!1), $location.search("vi");
        var n = chapters.getIndexFromTocElem(e);
        chapters.applyChapterStateFromURL(e.getAttribute("href"), {
            vIndex: 0,
            index: n
        }), $scope.$$phase || $scope.$apply()
    };

    $scope.targetCurrentChapter = function (e) {
        chapters.highlightCurrentTocChapter()
    };

    $scope.externalLink = function (e) {
        var t = e.getAttribute("href"),
            n = e.getAttribute("target");
        return void r.backend(t, {
            target: n
        })
    };

    $scope.bookmarkClicked = function (e) {
        bookmarks.addBookmark()
    };

    $scope.noteClicked = function (e, t) {
        notes.noteClicked(e ? [e] : null, t)
    };

    $scope.search = function (e, t, o, r) {
        if (e && e.length && 0 != e.length) {
            if (e.length < 3) return error.update({
                error: "lessThan3Characters"
            }), void modals.hide();
            $scope.applyHeader("default"), void 0 == o && ($scope.searchStack = [0]), status.update(translit.get("searching"), null, {
                dots: !0,
                timeoutDuration: 6e4
            }), t || (t = "all", o || (o = 0));
            var i = {
                key: e,
                mode: t,
                limit: 40,
                start: o,
                array: r
            };
            $timeout(function () {
                search.start(i, function (e) {
                    status.update(null), $scope.searchContent = e;
                    for (var n = 0; n < $scope.searchContent.results.length; n++)
                        if ($scope.searchContent.results[n] && $scope.searchContent.results[n].matches.length) {
                            $scope.searchContent.notEmpty = !0;
                            break
                        }
                    "all" == t ? ($scope.$broadcast("scroll.infiniteScrollComplete"), overflowScroll ? document.getElementById("searchScroll").scrollTop = 0 : $ionicScrollDelegate.$getByHandle("searchScroll").scrollTop()) : "chapter" == t && $timeout(function () {
                        $scope.applyHeader("searchItems")
                    }, 500)
                })
            }, 500)
        }
    };

    $scope.searchMore = function () {
        for (var e, t = $scope.searchContent.results.length; t--;)
            if ($scope.searchContent.results[t] && $scope.searchContent.results[t].matches[0]) {
                e = $scope.searchContent.results[t].matches[0];
                break
            }
        e ? ($scope.searchStack.push(e.chapter + 1), overflowScroll ? document.getElementById("searchScroll").scrollTop = 0 : $ionicScrollDelegate.$getByHandle("searchScroll").scrollTop(), $scope.search($scope.searchContent.key, "all", e.chapter + 1)) : $scope.searchContent.more = !1
    };

    $scope.searchBack = function () {
        if (!($scope.searchStack.length <= 1)) {
            $scope.searchStack.pop();
            var e = $scope.searchStack.pop();
            overflowScroll ? document.getElementById("searchScroll").scrollTop = 0 : $ionicScrollDelegate.$getByHandle("searchScroll").scrollTop(), $scope.search($scope.searchContent.key, "all", e)
        }
    };

    $scope.locateInChapter = function (e) {
        modals.hide(), $scope.search(e, "chapter")
    };

    $scope.clearResults = function () {
        $scope.searchContent = {}, $scope.applyHeader("default"), overflowScroll ? document.getElementById("searchScroll").scrollTop = 0 : $ionicScrollDelegate.$getByHandle("searchScroll").scrollTop()
    };

    $scope.searchItemClickedVeryOld = function (e) {
        var t = e.item;
        e.index, e.matches;
        modals.hide(), timeOut(function () {
            function e(n) {
                if ("newChapter" == n) {
                    chapters.removeListener(e);
                    var o = selection.getElemsByLocation(t.location)[0];
                    timeOut(function () {
                        addClass(o, "searchItemHighlightFade"), timeOut(function () {
                            removeClass(o, "searchItemHighlightFade")
                        }, 2200)
                    }, 1200)
                }
            }
            chapters.addListener(e), $location.path(book.root + "/reader/chapter/" + t.chapter + "/" + t.location)
        }, 300)
    };

    $scope.searchItemClicked = function (t) {
        search.searchItemClicked(t, {
            timer: !0
        }, function () {
            $rootScope.renderScope.applyHeader(""), $timeout(function () {
                $rootScope.renderScope.applyHeader("searchItems"), $rootScope.$$phase || $rootScope.$apply()
            })
        })
    };

    $scope.searchItemClickedOld = function (t, o) {
        function r(e) {
            "renderedChapter" == e && (status.update(), chapters.removeListener(r), l.index = a, $scope.applyHeader("searchItems"))
        }
        o || (o = {});
        var i = t.item,
            a = t.index,
            l = t.matches;
        modals.hide(), timeOut(function () {
            status.update(translit.get("pleaseWait"), null, {
                dots: !0
            }), chapters.addListener(r);
            var t = i.chapter,
                n = i.location,
                o = {
                    hash: n
                };
            chapters.getPageOb({
                page: t
            }, function (i) {
                if (chapters.shouldBePaged(i)) {
                    var a = selection.getPageByLocation(i, n);
                    null != a && (o.vIndex = a, $location.search({
                        vi: a
                    }))
                }
                if ($location.path(book.root + "/reader/chapter/" + t + "/" + n, o), i.index == currentIndex) {
                    if (o.vIndex && o.vIndex == currentVIndex) return void r("renderedChapter");
                    chapters.loadChapterByIndex(currentIndex, o)
                }
                $rootScope.$$phase || $rootScope.$apply()
            })
        }, 300)
    };

    $scope.fxlZoomIn = function (e) {
        nav.fxlZoomIn(e)
    };

    $scope.fxlZoomOut = function (e) {
        nav.fxlZoomOut(e)
    };

    $scope.fxlZoomFit = function (e) {
        nav.fxlZoomFit(e)
    };
}]);

app.controller("SearchItemsCtrl", ["search", "chapters", "virt", "$scope", "selection", function (search, chapters, virt, $scope, selection) {
    $scope.index = 0;

    $scope.init = function () {
        $scope.index = 0, $scope.titleMode = 3, $scope.results = [];
        var e = $scope.searchContent.results[currentIndex];
        e && ($scope.results = e.matches), $scope.results.index && ($scope.index = $scope.results.index), 0 == $scope.results.length ? $scope.titleMode = "empty" : $scope.highlightCurrentItem({
            initial: !0
        }),
            $scope.$on("$destroy", function () {
                search.removeHighlights()
            });
    };

    $scope.backClicked = function () {
        $scope.applyHeader("default")
    };

    $scope.up = function () {
        0 == $scope.index && ($scope.index = $scope.results.length), $scope.index-- , $scope.highlightCurrentItem()
    };

    $scope.down = function () {
        $scope.index == $scope.results.length - 1 && ($scope.index = -1), $scope.index++ , $scope.highlightCurrentItem()
    };

    $scope.highlightCurrentItem = function (e) {
        function n(e) {
            if ("renderedChapter" == e) {
                chapters.removeListener(n), chapters.hideLoader();
                var t = chapters.getCurrentHash(),
                    r = selection.getElemsByLocation(t);
                r[0] && (r[0].offsetParent ? ($scope.titleMode = "title", addClass(r[0], "searchItemHighlight"), virt.safeScrollToElem("content", r[0], !0)) : $scope.titleMode = "hidden")
            }
        }
        if (e || (e = {}), $scope.results) {
            var r;
            chapters.getPageOb({
                page: currentIndex
            }, function (e) {
                search.removeHighlights(), r = $scope.results[$scope.index].location, chapters.addListener(n), chapters.redirect(currentIndex, {
                    hash: r
                })
            })
        }
    };
}]);

app.controller("SelectionCtrl", ["chapters", "translit", "error", "status", "modals", "notes", "backend", "hlights", "$scope", "selection", "crdv", "stg", function (chapters, translit, error, status, modals, notes, backend, hlights, $scope, selection, crdv, stg) {
    $scope.init = function () { };

    $scope.tts = function () {
        if (config.tts) {
            var e = selection.getSelectedText();
            if (e)
                if (desktop) error.update({
                    error: "speechUnsupportedInDesktop"
                });
                else if (preview) error.update({
                    error: "speechUnsupportedInPreview"
                });
                else if (native && "undefined" != typeof TTS) crdv.tts(e, function (e) { });
                else try {
                    if ("speechSynthesis" in window && SpeechSynthesisUtterance) {
                        status.showAndHide(translit.get("preparingVoice"), 600, {
                            dots: !0
                        }), window.speechSynthesis.cancel();
                        var t = new SpeechSynthesisUtterance,
                            i = window.speechSynthesis.getVoices();
                        if (t.lang = config.defaultTtsLanguage ? config.defaultTtsLanguage : "en-US", t.voice = i.filter(function (e) {
                            return e.lang == t.lang
                        })[0], !t.voice) return;
                        t.text = e, window.speechSynthesis.speak(t)
                    } else error.update({
                        error: "speechUnsupported"
                    })
                } catch (e) {
                    error.update({
                        error: "speechUnsupported"
                    })
                }
        }
    };

    $scope.copy = function () {
        if (config.clipboardCopy) {
            var e = selection.getSelectedText();
            if (e)
                if (native) crdv.copyToClipboard(e);
                else if (desktop) require("nw.gui").Clipboard.get().set(e, "text"), status.showAndHide(translit.get("copiedToClipboard"), 500);
                else {
                    var t = document.createElement("textarea");
                    document.body.appendChild(t), t.value = e, t.focus(), t.select();
                    try {
                        document.execCommand("Copy"), status.showAndHide(translit.get("copiedToClipboard"), 500)
                    } catch (t) {
                        modals.show({
                            mode: "flashUnsupported",
                            flashOb: {
                                text: e
                            }
                        }), setTimeout(function () {
                            document.getElementById("clipboardText").focus(), document.getElementById("clipboardText").select()
                        }, 200)
                    }
                    t.remove()
                }
        }
    };

    $scope.share = function () {
        if (config.share) {
            var e = selection.getSelectedText();
            e && crdv.share(e)
        }
    };

    $scope.note = function (e) {
        if (config.annotations) {
            var n, o, r = selection.getLocation(),
                i = [];
            r.left && ((n = {}).type = "content", n.location = r.left, o = selection.getElemsByLocation(n.location), n.chapter = chapters.getElemChapterIndex(o[0]), n.src = selection.getSelectedText(o), n.event = e, i.push(n)),
                r.right && ((n = {}).type = "content", n.location = r.right, o = selection.getElemsByLocation(n.location), n.chapter = chapters.getElemChapterIndex(o[0]), n.src = g.getSelectedText(o), n.event = e, i.push(n)),
                notes.noteClicked(i, function (e) {
                    selection.reset()
                })
        }
    };

    $scope.highlight = function () {
        function e() {
            hlights.showPopup(i, function (e) {
                selection.reset()
            })
        }
        if (config.annotations) {
            var n, o = selection.getSelectedNodes(),
                r = selection.getLocation(),
                i = [];
            r.left && ((n = {}).location = r.left, o = selection.getElemsByLocation(n.location), n.chapter = chapters.getElemChapterIndex(o[0]), n.src = selection.getSelectedText(o), i.push(n)), r.right && ((n = {}).location = r.right, o = selection.getElemsByLocation(n.location), n.chapter = chapters.getElemChapterIndex(o[0]), n.src = selection.getSelectedText(o), i.push(n));
            for (var a = 0; a < o.length; a++)
                if (hasClass(o[a], "contentHlightBG")) {
                    for (var s = getClassThatStartsWith(o[a], "hlight"), l = 0; l < i.length; l++) i[l].hid = s.substr(6);
                    return void stg.getHlightById(i[0].hid, function (t) {
                        for (var n = 0; n < i.length; n++) i[n].color = t.color;
                        e()
                    })
                }
            e()
        }
    };

    $scope.google = function () {
        if (config.googleLookup) {
            var e = selection.getSelectedText();
            if (e) try {
                var t = "http://www.google.com/custom?q=" + e;
                return void backend.externalLink(t, {
                    secure: !0
                })
            } catch (e) { }
        }
    };

    $scope.wikipedia = function () {
        if (config.wikipediaLookup) {
            var e = selection.getSelectedText();
            if (e) try {
                var t = "https://" + (config.wikipediaLang ? config.wikipediaLang : "en") + ".wikipedia.org/wiki/" + e;
                backend.externalLink(t, {
                    secure: !0
                })
            } catch (e) { }
        }
    };

    $scope.backClicked = function () {
        selection.reset()
    };
}]);

app.controller("SelectionOptionsCtrl", ["$scope", "selection", function ($scope, selection) {
    $scope.init = function () { };

    $scope.expand = function () {
        selection.expand()
    };

    $scope.extendRight = function () {
        selection.extendRight()
    };

    $scope.extendLeft = function () {
        selection.extendLeft()
    }
}]);

app.controller("TabMenuCtrl", ["$scope", "modals", "nav", "notes", "$rootScope", "$ionicSideMenuDelegate", "$ionicScrollDelegate", "chapters", "$timeout", "stg", "crdv", function ($scope, modals, nav, notes, $rootScope, $ionicSideMenuDelegate, $ionicScrollDelegate, chapters, $timeout, stg, crdv) {
    $scope.init = function () {
        $scope.$watch(function () {
            return $ionicSideMenuDelegate.isOpenRight()
        }, function (t) {
            t || $timeout(function () {
                $scope.setSideMenuMode("bookSettings")
            }, 100)
        })
    };

    $scope.chaptersClicked = function () {
        chapters.displayChapters()
    };

    $scope.mediaClicked = function () {
        if (stg.data.settings.rtl) {
            if ($ionicSideMenuDelegate.isOpenLeft() && "media" != $scope.sideMenuMode) return $ionicSideMenuDelegate.toggleLeft(), void $timeout($scope.mediaClicked, 300)
        } else if ($ionicSideMenuDelegate.isOpenRight() && "media" != $scope.sideMenuMode) return $ionicSideMenuDelegate.toggleRight(), void $timeout($scope.mediaClicked, 300);
        native && crdv.vibrate(), $scope.setSideMenuMode("media"), mobile && nav.resize(), stg.data.settings.rtl ? $ionicSideMenuDelegate.toggleLeft() : $ionicSideMenuDelegate.toggleRight()
    };

    $scope.notebookClicked = function () {
        $rootScope.widescreen ? $scope.chaptersClicked() : (native && crdv.vibrate(), notes.updateNotebookArray({}, function () {
            overflowScroll || nav.resize(), modals.show({
                mode: "notebook",
                dynamic: !0,
                scope: $scope,
                animation: "slide-in-up",
                cb: function () {
                    overflowScroll ? document.getElementById("notebookContent").scrollTop = 0 : $ionicScrollDelegate.$getByHandle("notebookScroll").scrollTop()
                }
            })
        }))
    };

    $scope.pdfClicked = function () {
        chapters.pdfPrint()
    };

    $scope.printClicked = function () {
        chapters.print()
    };

    $scope.settingsClicked = function () {
        if (stg.data.settings.rtl) {
            if ($ionicSideMenuDelegate.isOpenLeft() && "bookSettings" != $scope.sideMenuMode) return $ionicSideMenuDelegate.toggleLeft(), void $timeout($scope.settingsClicked, 300)
        } else if ($ionicSideMenuDelegate.isOpenRight() && "bookSettings" != $scope.sideMenuMode) return $ionicSideMenuDelegate.toggleRight(), void $timeout($scope.settingsClicked, 300);
        native && crdv.vibrate(), $scope.setSideMenuMode("bookSettings"), mobile && nav.resize(), stg.data.settings.rtl ? $ionicSideMenuDelegate.toggleLeft() : $ionicSideMenuDelegate.toggleRight()
    };

    $scope.searchClicked = function () {
        native && crdv.vibrate(), mobile && nav.resize(), modals.show({
            mode: "search",
            dynamic: !0,
            animation: "slide-in-up",
            scope: $scope,
            cb: function () {
                $scope.searchContent.notEmpty || $timeout(function () {
                    crdv.showKeyboard()
                }, 200)
            }
        })
    };

    $scope.settingsClose = function () {
        modals.hide(), stg.writeSlot("settings" + stg.getBookId(!0), $scope.data.settings)
    };
    var I = [];

    $scope.customTabClicked = function (e, t) {
        var n = document.createElement("div");
        if (e.relToRoot = !0, "audio" == e.type) {
            var o = ph.join(bookPath, e.audio);
            e.id || (e.id = Math.round(1e4 * Math.random()));
            for (var r, i = 0; i < I.length; i++)
                if (I[i].getAttribute("id") == "audio-" + e.id && !(r = I[i]).paused) return r.pause(), void (r.src = "");
            if (!r) {
                (r = document.createElement("audio")).setAttribute("data-dontclose", !0), r.setAttribute("id", "audio-" + e.id), r.setAttribute("controls", "true"), r.setAttribute("autoplay", "true"), r.setAttribute("data-tap-disabled", "false");
                var a = document.createElement("source");
                a.src = o, r.appendChild(a), r.appendChild(document.createTextNode("Your browser does not support the audio element")), r.className = "ki-noHighlight", r.oncanplay = function () {
                    kInteractive.tinCan({
                        verb: "played",
                        activity: "Audio: " + o
                    })
                }
            }
            if (r.setAttribute("src", o), r.play(), I.push(r), kInteractive.scorm) {
                var l = {};
                l.id = kInteractive.getScormId(e.title, o), l.description = "Played audio link: " + o, l.type = "other", l.learnerResponses = "Played", l.objective = e.options ? e.options.objective : null, l.timestamp = new Date, kInteractive.scorm.setInteractions([l])
            }
        } else {
            kInteractive.writeData(n, e);
            var c = e.url;
            if (e.hash && (c += e.hash), n.className = "kInteractive link", !c) return kInteractive.action(n, t), void t.stopPropagation();
            n.setAttribute("href", c), e.target && "_blank" == e.target.toLowerCase() && n.setAttribute("target", "_BLANK"), c.match(/http[s]?:/g) ? $rootScope.renderScope.externalLink(n) : 0 == c.indexOf("/") ? window.open(c) : 0 == c.indexOf("mailto:") ? window.location.href = c : (0 != c.indexOf("#") && (c = ph.join(stg.data.book.chapter.url, c)), chapters.applyChapterStateFromURL(c))
        }
    };
}]);

app.controller("TextSizeCtrl", ["$scope", "stg", "settings", "$injector", function ($scope, stg, settings, $injector) {
    function s() {
        "rfl" == stg.data.book.chapter.layout && ("double" != stg.data.book.chapter.view && stg.data.settings.pageScroll || $injector.get("chapters").regenerateSpreads(null, function () { }))
    };

    $scope.backClicked = function () {
        $scope.applyHeader("default")
    };

    $scope.plus = function () {
        var t = settings.getTextSize();
        t > 30 || (t = Math.round(10 * (t + .1)) / 10, $scope.data.settings.textSize = t + "em", settings.updateTextSize(), settings.writeTextSize(), s())
    };

    $scope.minus = function () {
        var t = settings.getTextSize();
        t <= .2 || (t = Math.round(10 * (t - .1)) / 10, $scope.data.settings.textSize = t + "em", settings.updateTextSize(), settings.writeTextSize(), s())
    };
}]);

app.controller("ThumbCtrl", ["$scope", "stg", "book", function ($scope, stg, book) {
    $scope.init = function () {
        if ($scope.book.coverImg = book.getCoverImgPath($scope.book), chromeApp)
            if (stg.data.chromeAssets[$scope.book.coverImg]) $scope.book.coverImg = stg.data.chromeAssets[$scope.book.coverImg];
            else {
                var t = $scope.book.coverImg;
                delete $scope.book.coverImg, getImgDataSrc(t, function (n) {
                    $scope.book.coverImg = n, stg.data.chromeAssets[t] = n, $scope.$apply()
                })
            }
    }
}]);

app.filter("bookSetup", ["$rootScope", "stg", "selection", "media", "chapters", "cache", function ($rootScope, stg, selection, media, chapters, cache) {
    return function () {
        function a(e) {
            selection.active() && selection.reset();
            var t = e.target;
            if ("sections" != t.nodeName.toLowerCase() && !hasClass(t, "k-section") && "video" != t.nodeName.toLowerCase() && "audio" != t.nodeName.toLowerCase())
                for (var o = t.childNodes.length; o--;)
                    if ("#text" == t.childNodes[o].nodeName && "" != t.childNodes[o].nodeValue.replace(/\s+/g, " ").trim()) return void selection.elementDown(t, e)
        }
        var s = document.getElementById("chaptersContent"),
            l = document.getElementById("epubContainer");
        cache.setOb("epubContainer", l), cache.setOb("chaptersContent", s), document.getElementById("chapters").addEventListener("click", function (t) {
            function n(e) {
                for (var t = e.parentNode; t && t != document.body;) {
                    if (hasClass(t, "menu") && (hasClass(t, "menu-left") || hasClass(t, "menu-right"))) return !0;
                    t = t.parentNode
                }
            }
            var o = t.target;
            if ("a" == o.nodeName.toLowerCase()) {
                if (!n(o)) return;
                var r = o.getAttribute("href");
                if (!r) return;
                if (r.match(/http[s]?:/g)) return;
                if (0 == r.indexOf("/")) return;
                $rootScope.renderScope.chapterLink(o)
            } else if ("i" == o.nodeName.toLowerCase()) {
                if (!n(o)) return;
                if (o.parentElement.getElementsByTagName("ol").length > 0) o.offsetHeight, o.parentElement.classList.toggle("expanded")
            }
        }), l.addEventListener("touchstart", a), l.addEventListener("mousedown", a), l.addEventListener("click", function (n) {
            var i = n.target;
            if (i) {
                for (var a = i; a && a != l;) {
                    if ("a" == a.nodeName.toLowerCase()) {
                        if (hasClass(a, "ki-btn")) kInteractive.action(a, n);
                        else {
                            var s = a.getAttribute("href");
                            if (!s) return kInteractive.action(a, n), void n.stopPropagation();
                            s.match(/http[s]?:/g) ? $rootScope.renderScope.externalLink(a) : 0 == s.indexOf("/") ? window.open(s) : 0 == s.indexOf("mailto:") ? window.location.href = s : (0 != s.indexOf("#") && (s = ph.join(stg.data.book.chapter.url, s)), chapters.applyChapterStateFromURL(s))
                        }
                        return void n.stopPropagation()
                    }
                    a = a.parentNode
                }
                if ("video" != i.nodeName.toLowerCase() && "audio" != i.nodeName.toLowerCase()) {
                    if ("img" == i.nodeName.toLowerCase()) {
                        if (hasClass(i, "kInteractive") && "none" != kInteractive.readData(i).behavior) return kInteractive.action(i, n), void n.stopPropagation();
                        var c = i.parentNode;
                        if (hasClass(c, "kInteractive")) {
                            if (hasClass(c, "widget")) return kInteractive.action(i, n), void n.stopPropagation();
                            if (hasClass(c, "questions")) return kInteractive.action(i, n), void n.stopPropagation()
                        }
                        var d = !0,
                            u = kInteractive.readData(i);
                        if (u ? u.popup || (d = !1) : d = !1, i.hasAttribute("static") && (d = !1), i.hasAttribute("data-static") && (d = !1), d) {
                            var p = {
                                thumbnail: i.getAttribute("src"),
                                title: i.getAttribute("alt")
                            };
                            return media.show(p), void n.stopPropagation()
                        }
                    }
                    return hasClass(i, "ki-btn") ? (kInteractive.action(i, n), void n.stopPropagation()) : hasClass(i, "kInteractive") ? (kInteractive.action(i, n), void n.stopPropagation()) : void ("input" != i.nodeName.toLowerCase() || n.stopPropagation())
                }
            }
        })
    }
}]);

app.filter("chapterHtml", function () {
    return function (e) {
        if (e) {
            for (var t = e.getElementsByTagName("li"), n = t.length; n--;) {
                addClass(t[n], "item");
                var o = t[n].getElementsByTagName("ol").length ? " parent" : "";
                t[n].innerHTML = '<i class="icon' + o + '"> </i> ' + t[n].innerHTML
            }
            e.innerHTML = e.innerHTML.replace(/(<a .*?)(>)/g, '$1 onclick="return false;" $2')
        }
    }
});

app.filter("contentHtml", ["$rootScope", "$filter", "selection", "stg", function (e, t, n, o) {
    return function (e, t) {
        var n = document.createElement("div");
        n.setAttribute("id", "epubContent"), t || (t = o.data.book.chapter.absoluteURL);
        for (var r = e.getElementsByTagName("body")[0], i = [], a = 0; a < r.childNodes.length; a++) 3 == r.childNodes[a].nodeType && i.push(r.childNodes[a]);
        for (a = 0; a < i.length; a++) {
            var s = i[a],
                l = document.createElement("span");
            l.textContent = s.textContent, s.nextSibling ? r.insertBefore(l, s.nextSibling) : r.appendChild(l), r.removeChild(s)
        }
        kInteractive && kInteractive.preRender(e, {
            chapterUrl: t
        });
        for (var c = getElementsByAttribute(e, "*", "src"), a = 0; a < c.length; a++)
            if (shouldParse(h = c[a].getAttribute("src")) && c[a].setAttribute("src", ph.join(t, h)), "img" == c[a].nodeName.toLowerCase() && (hasClass(c[a], "kInteractive") || hasClass(c[a].parentNode, "kInteractive") || c[a].style.height || c[a].getAttribute("height") || (c[a].style.maxWidth = "100%"), chromeApp && c[a].getAttribute("src"))) {
                var d = c[a],
                    u = d.getAttribute("src");
                if (isAbsoluteURLAndNotDataPath(u)) {
                    var p = d.getAttribute("chrome");
                    o.data.chromeAssets[u] ? d.setAttribute("src", o.data.chromeAssets[u]) : p && o.data.chromeAssets[p] ? d.setAttribute("src", o.data.chromeAssets[p]) : (p = Math.round(1e5 * Math.random()), d.setAttribute("src", ""), d.setAttribute("chrome", p), getImgDataSrc(u, function (e) {
                        setTimeout(function () {
                            o.data.chromeAssets[p] = e;
                            var t = getElementsByAttribute(document, "img", "chrome", p);
                            t.length && t[0].setAttribute("src", e)
                        }, 300)
                    }))
                }
            }
        for (var f = getElementsByAttribute(e, "*", "poster"), a = 0; a < f.length; a++) shouldParse(h = f[a].getAttribute("poster")) && f[a].setAttribute("poster", ph.join(t, h));
        for (var g = getElementsByAttribute(e, "image", "xlink:href"), a = 0; a < g.length; a++) {
            var h = g[a].getAttribute("xlink:href");
            shouldParse(h) && g[a].setAttribute("xlink:href", ph.join(t, h))
        }
        for (var m = getElementsByAttribute(e, "*", "style"), a = 0; a < m.length; a++) {
            var v = m[a].getAttribute("style"); - 1 != v.toLowerCase().indexOf("url(") && (hasParent(m[a], "kInteractive", 5) || m[a].setAttribute("style", v.replace(/(.*?url\()(.+?)(\).*?)/g, function () {
                var e = arguments[2];
                return 0 == e.indexOf('"') && (e = e.split('"')[1]), 0 == e.indexOf("'") && (e = e.split("'")[1]), 0 == e.indexOf("&quot;") && (e = e.split("&quot;")[1]), -1 != e.indexOf("http://") ? arguments[0] : -1 != e.indexOf("https://") ? arguments[0] : 0 == e.indexOf("data:") ? arguments[0] : (e = ph.join(t, e), arguments[1] + e + arguments[3])
            })))
        }
        var b = document.createElement("sections"),
            y = e.getElementsByTagName("body")[0];
        if (partitionEnabled) {
            for (var k = new Array, w = 0; ;) {
                var C = !0,
                    x = !1,
                    I = 0;
                k[w] = document.createElement("section"), k[w].setAttribute("sec", w), addClass(k[w], "k-section");
                for (a = y.childNodes.length; a--;)
                    if (!y.childNodes[a].style || "absolute" != y.childNodes[a].style.position) {
                        if (!((I += (null != y.childNodes[a].textContent ? y.childNodes[a].textContent : y.childNodes[a].data).length) < 4e3 || C)) {
                            x = !0;
                            break
                        }
                        C = !1, k[w].insertBefore(y.childNodes[a], k[w].firstChild)
                    }
                if (!x) break;
                w++
            }
            for (a = 0; a < k.length; a++) b.insertBefore(k[a], b.firstChild)
        } else {
            var E = document.createElement("section");
            E.setAttribute("sec", "0"), addClass(E, "k-section");
            for (a = y.childNodes.length; a--;) E.insertBefore(y.childNodes[a], E.firstChild);
            b.appendChild(E)
        }
        return y.appendChild(b), n.innerHTML = e.getElementsByTagName("body")[0].innerHTML, pauseVideoAndAudio(n), n
    }
}]);

app.filter("contentJs", ["cache", "selection", "stg", "$q", "settings", "bookmarks", "hlights", "notes", function (cache, selection, stg, $q, settings, bookmarks, hlights, notes) {
    return function (e) {
        function n() {
            function e() {
                if (r++ , "#e7e3d9" == rgb2hex(window.getComputedStyle(cache.getOb("epubContent")).getPropertyValue("background-color"))) {
                    clearInterval(n), removeClass(o, "hide");
                    for (var e = document.getElementsByTagName("style"), r = 0; r < e.length; r++)
                        if (e[r].hasAttribute("kotobee")) {
                            var i = e[r].innerHTML;
                            i = i.replace(/[^}]*?{[^}]*?background-color:#e7e3d9 !important;.*?}/g, ""), e[r].innerHTML = i
                        }
                    settings.initialize(), l.resolve({})
                }
            }
            removeClass(cache.getOb("epubContent"), "bgInherit");
            var n = setInterval(e, 30);
            e()
        }
        var o, l = $q.defer(), p = cache.getOb("epubContent");
        return document.getElementById("epubMeta") ? (document.getElementById("epubMeta").innerHTML = "", o = document.getElementById("epubMeta")) : ((o = document.createElement("div")).setAttribute("id", "epubMeta"), o.className = "hide", p.parentNode.insertBefore(o, p.nextSibling)), cache.setOb("epubMeta", o), selection.newChapter(), e ? stg.getBookmarks(function (e) {
            stg.getNotes(function (t) {
                stg.getHlights(function (o) {
                    for (r = 0; r < e.length; r++) e[r].chapter == currentIndex && bookmarks.addIcon(e[r]);
                    for (r = 0; r < t.length; r++) t[r].chapter == currentIndex && notes.addIcon(t[r]);
                    for (var r = 0; r < o.length; r++) o[r].chapter == currentIndex && hlights.addHlight(o[r]);
                    n()
                })
            })
        }) : n(), l.promise
    }
}]);

app.filter("contentVirt", ["stg", "bookmarks", "notes", "workers", "hlights", "$timeout", "cache", function (stg, bookmarks, notes, workers, hlights, $timeout, cache) {
    var c, d, u, p, f = [];
    return function (a, l, g) {
        function h(e) {
            if (e) {
                for (var t = f.length; t--;)
                    if (f[t] == e) return;
                var n = cache.getOb("epubContent").className;
                addClass(cache.getOb("epubContent"), "reveal");
                var o = e.offsetTop - cache.getOb("epubContent").offsetTop;
                0 != o && (e.style.marginTop = o + "px", f.push(e), cache.getOb("epubContent").className = n)
            }
        }

        function m(e, t) {
            function n() {
                i.html = b[0].html, workers.call("htmlParser", "parse", [i], function (e) {
                    var t = replaceHTML(b[0].el, e),
                        o = b[0].options;
                    o.includeAnnotations && v(t), b.shift(), o.cb && o.cb(t), b.length && n()
                })
            }
            t || (t = {}), t.id = Math.round(9999 * Math.random());
            t.cb;
            if (null == t.includeAnnotations && (t.includeAnnotations = !0), hasClass(e, "parsed")) return t.cb && t.cb(e), e;
            addClass(e, "parsed");
            var r = e.innerHTML;
            if (workers.supported() && t.allowWorkers) {
                var i = {};
                return b.push({
                    html: r,
                    el: e,
                    options: t
                }), 1 == b.length && n(), !0
            }
            var a, s = [];
            r.indexOf("<pre"), r.indexOf("<code"), r.indexOf("<script");
            s.push(/([< \/][^>]*?>)((\s*[^<\s]+\s+?)+)([^<\s]+\s*)(<)/g), a = /(>)([^<\n]*?[^<]+?)(<[^\/])/g;
            for (var l = 0; l < s.length; l++) {
                var c = s[l];
                r = r.replace(c, function (e, t) {
                    if (arguments[1].indexOf('class="parsed"') >= 0) return e;
                    if (0 == arguments[1].indexOf("<pre")) return e;
                    if (0 == arguments[1].indexOf("<code")) return e;
                    if (0 == arguments[1].indexOf("<script")) return e;
                    var n = "",
                        o = arguments[2].split(" ");
                    "" == o[o.length - 1] && o.splice(-1, 1), o.push(arguments[4]);
                    for (var r = 0; r < o.length; r++) {
                        var i = r == o.length - 1 ? "" : " ";
                        n += "<span>" + o[r] + i + "</span>"
                    }
                    return arguments[1] + n + "<"
                })
            }
            a && (r = r.replace(a, function (e, t) {
                if (!arguments[2].trim()) return e;
                var n = "<span>" + arguments[2] + "</span>";
                return arguments[1] + n + arguments[3]
            }));
            var d = replaceHTML(e, r = r.replace(/(<a [\s\S]*?)(>)/g, '$1 onclick="return false;" $2'));
            return t.includeAnnotations && v(d), g && g(), d
        }

        function v(o) {
            for (var a = 0, s = o; null != (s = s.previousSibling);) a++;
            var l = [currentIndex];
            stg.data.currentRPageOb && l.push(stg.data.currentRPageOb.index), $timeout(function () {
                stg.getBookmarks(function (o) {
                    stg.getNotes(function (i) {
                        stg.getHlights(function (e) {
                            for (var s = 0; s < l.length; s++) {
                                for (c = 0; c < o.length; c++) o[c].chapter == l[s] && String(o[c].location).split(".")[1] == a && bookmarks.addIcon(o[c]);
                                for (c = 0; c < i.length; c++) i[c].chapter == l[s] && String(i[c].location).split(".")[1] == a && notes.addIcon(i[c]);
                                for (var c = 0; c < e.length; c++) e[c].chapter == l[s] && String(e[c].location).split(".")[1] == a && hlights.addHlight(e[c])
                            }
                        })
                    })
                })
            }, 1e3)
        }
        try {
            var b = [];
            if ("reset" == a) return c = d = null, u = !0, void (f = []);
            var y = cache.getOb("epubContent");
            if ("reveal" == a) return void addClass(y, "reveal");
            if ("undoReveal" == a) return void removeClass(y, "reveal");
            if ("parse" == a) return void m(l, {
                includeAnnotations: !1,
                allowWorkers: !0,
                source: "parse mode",
                cb: g
            });
            if ("parseAndAnnotations" == a) return void m(l, {
                allowWorkers: !0,
                source: "parseAndAnnotations mode"
            });
            var k, w = y.parentNode.parentNode,
                C = w.style.webkitTransform || w.style.mozTransform || w.style.transform;
            k = null == a && null != l ? l : !overflowScroll || !config.overflowScroll && preview && mobile ? -Number(C.split(",")[1].slice(0, -2)) : cache.getOb("epubContainer").scrollTop;
            var x = p > k ? "up" : "down";
            p = k;
            var I, E, T = k + w.parentNode.offsetHeight,
                O = y.children;
            if (u) {
                for (var S = [], A = 0, L = 0; L < O.length; L++) {
                    for (var B = [], N = O[L], M = 0; M < N.childNodes.length; M++) B[M] = {}, N.childNodes[M].offsetTop + N.childNodes[M].offsetHeight < k - 200 ? B[M].add = ["cNone", "noMargin"] : N.childNodes[M].offsetTop > T + 200 ? B[M].add = ["cNone", "noMargin"] : (0 == A && (B[M].setMargin = !0, A = 1, I = M), B[M].remove = ["cNone", "noMargin"], B[M].setParse = !0, E = M);
                    for (M = 0; M < B.length; M++) B[M].setMargin && h(N.childNodes[M]), B[M].setParse && S.push([N.childNodes[M], {
                        source: "from clean",
                        allowWorkers: !0
                    }]), addRemoveClass(N.childNodes[M], B[M].add, B[M].remove);
                    for (M = 0; M < B.length; M++);
                    null != I && (c = N.childNodes[I]), null != E && (d = N.childNodes[E])
                }
                if (u = !1, S.length)
                    for (M = 0; M < S.length; M++) {
                        var P = S[M];
                        M == S.length - 1 && (P[1].cb = g), m(P[0], P[1])
                    } else g && g();
                return
            }
            if ("fxl" == stg.data.book.chapter.layout) return;
            if (O.length > 1) return;
            if (1 == O[0].childNodes.length) return;
            N = O[0];
            if ("up" == x) {
                if (!(c.offsetTop + c.offsetHeight) < k - 0 && !(c.offsetTop > k - 200)) return;
                if (d == N.childNodes[0]) return
            } else if ("down" == x) {
                if (!(d.offsetTop > T + 0 || d.offsetTop + d.offsetHeight < T + 200)) return;
                if (c == N.childNodes[N.childNodes.length - 1]) return
            }
            c && (c.offsetTop + c.offsetHeight < k - 0 ? (addClass(c, "cNone"), addClass(c, "noMargin"), c.nextSibling && (removeClass(c = c.nextSibling, "noMargin"), h(c), c = m(c, {
                source: "topElem1"
            }))) : c.offsetTop > k - 200 && c.previousSibling && (addClass(c, "noMargin"), removeClass(c = c.previousSibling, "cNone"), removeClass(c, "noMargin"), h(c), c = m(c, {
                source: "topElem2"
            }))), d && (d.offsetTop > T + 0 ? (addClass(d, "cNone"), d.previousSibling && (removeClass(d = d.previousSibling, "cNone"), d = m(d, {
                source: "bottomElem1"
            }))) : d.offsetTop + d.offsetHeight < T + 200 && d.nextSibling && (removeClass(d = d.nextSibling, "cNone"), d = m(d, {
                source: "bottomElem2"
            })))
        } catch (e) { }
    }
}]);

app.filter("js", ["$q", "jsParser", function ($q, jsParser) {
    return function (e, o) {
        var r = $q.defer();
        o || (o = {});
        for (var i = [], a = e.getElementsByTagName("script"), s = 0; s < a.length; s++)
            if (a[s].getAttribute("src")) {
                var l = a[s].getAttribute("src");
                if (l.indexOf("/kotobeeInteractive.js") >= 0) continue;
                l.indexOf(":/"), i.push({
                    mode: "url",
                    val: l
                })
            } else i.push({
                mode: "code",
                val: a[s].innerHTML
            });

        var c = e.getElementsByTagName("body")[0];

        return c.getAttribute("onload") && i.push({
            mode: "code",
            val: c.getAttribute("onload")
        }), o.noJsParse ? r.resolve() : jsParser.inject(i, function (e) {
            r.resolve(e)
        }), r.promise
    }
}]);

app.filter("coverPath", ["stg", function (stg) {
    return function (t, n) {
        var o = (config.kotobee.liburl ? config.kotobee.liburl : "") + "img/ui/defaultCover.png";
        return t ? isAbsoluteURLPath(t) ? t : n ? (config.kotobee.liburl ? config.kotobee.liburl : "../../") + "books/" + n + "/EPUB/" + t : ph.join(bookPath, stg.data.packageURL, t) : o
    }
}]);

app.filter("categoryStyle", ["stg", function (e) {
    return function (e) {
        var t = "";
        isKotobeeHosted() && (t = publicRoot);
        var n = "";
        return "image" != e.display && "imagetext" != e.display || e.img && (n += "background-image:url(" + (t + "imgUser/" + e.img) + ");"), "image" == e.display && (n += "color:transparent;"), e.css && (n += e.css), n
    }
}]);

app.filter("catImgPath", ["stg", function (e) {
    return function (e) {
        var t = (config.kotobee.liburl ? config.kotobee.liburl : "") + "imgUser/" + e;
        return config.kotobee.liburl + "/" + ph.join(bookPath, t)
    }
}]);

app.filter("decodeHtmlEntities", function () {
    return function (e) {
        return decodeHTMLEntities(e)
    }
});

app.filter("nativeLinks", function () {
    return function (e) {
        var t = document.createElement("div");
        t.innerHTML = e;
        for (var n = t.getElementsByTagName("a"), o = 0; o < n.length; o++) {
            var r = n[o].getAttribute("href");
            n[o].setAttribute("onclick", "window.open('" + r + "', '_system');"), n[o].setAttribute("href", "#")
        }
        return t.innerHTML
    }
});

app.filter("moreDataExists", ["stg", function (e) {
    return function (t, n) {
        try {
            if (!e.currentLibrary) return !1;
            if ("categories" == n) return !1;
            if ("downloaded" == n) return !1;
            var o;
            if ("favorites" == n && (o = e.favorites), "all" == n && (o = e.currentLibrary.books), 0 == o.length) return !1
        } catch (e) {
            return !1
        }
        return t
    }
}]);

app.filter("round", function () {
    return function (e) {
        return Math.round(e)
    }
});

app.filter("arrayEmpty", ["$filter", function ($filter) {
    return function (t) {
        if (!t) return !0;
        for (var n = 0; n < t.length; n++)
            if (Array.isArray(t[n])) {
                if (!$filter("arrayEmpty")(t[n])) return !1
            } else if (t[n]) return !1;
        return !0
    }
}]);

app.filter("langFromId", ["stg", function (stg) {
    return function (t) {
        for (var n = 0; n < stg.data.languages.length; n++)
            if (stg.data.languages[n].val == t) return stg.data.languages[n].label;
        return t
    }
}]);

app.filter("unsafe", ["$sce", function ($sce) {
    return $sce.trustAsHtml
}]);

app.filter("nl2br", function () {
    return function (e) {
        return e ? e.indexOf("</") >= 0 ? e : e.replace(/\n/g, "<br>") : ""
    }
});

app.filter("releasePlatform", function () {
    return function (e) {
        switch (e) {
            case "win32":
                return "Windows 32-bit";
            case "win64":
                return "Windows 64-bit";
            case "win":
                return "Windows";
            case "mac":
                return "Macintosh"
        }
    }
});

app.filter("styles", ["$q", "cssParser", function ($q, cssParser) {
    return function (e, o) {
        var r = $q.defer();
        o || (o = {});
        for (var i, a = [], s = e.getElementsByTagName("link"), l = 0; l < s.length; l++)
            if ("text/css" == s[l].getAttribute("type") && s[l].getAttribute("href").length) {
                var c = ph.join(o.path, s[l].getAttribute("href")),
                    d = !1;
                if (isAbsoluteURLPath(s[l].getAttribute("href")) && (c = s[l].getAttribute("href"), d = !0), function (e) {
                    if (e.indexOf("/css/kotobeeInteractive.css") >= 0) return !0
                }(c)) continue;
                c == bookPath + "EPUB/css/base.css" && (i = !0), a.push({
                    type: "path",
                    val: c,
                    dontCache: d
                })
            }
        i || a.unshift({
            type: "path",
            val: bookPath + "EPUB/css/base.css",
            dontCache: !1
        });
        for (var u = e.getElementsByTagName("style"), l = 0; l < u.length; l++) a.push({
            type: "local",
            val: u[l].innerHTML,
            root: o.path
        });
        return o.noCssParse ? r.resolve() : cssParser.inject(a, o, function (e) {
            r.resolve(e)
        }), r.promise
    }
}]);

app.filter("t", ["translit", function (translit) {
    return function (t, n) {
        return translit.getLang() ? translit.get(t, Array.prototype.slice.call(arguments, 2)) : ""
    }
}]);
var log = kLog;

app.directive("iframeOnload", [function () {
    return {
        scope: !1,
        link: function (e, t, n) {
            t.on("load", function () {
                return e.modal.siteOptions.loaded()
            })
        }
    }
}]);

app.directive("containerScroll", ["$timeout", function ($timeout) {
    return {
        scope: !1,
        link: function (t, n, o) {
            function r() {
                $timeout(function () {
                    var e = document.getElementsByClassName("containerScroll" + i);
                    e.length ? e[0].addEventListener("touchstart", function (t) {
                        e[0].getElementsByClassName("kbContent");
                        t.touches && t.touches.length > 1 || t.stopPropagation()
                    }) : ++a < 50 && r()
                }, 40)
            }
            var i = Math.round(99999 * Math.random());
            addClass(n[0], "containerMobileScroll containerScroll" + i), r();
            var a = 0
        }
    }
}]);

app.directive("enterWithoutShift", function () {
    return function (e, t, n) {
        var o;
        t.bind("keydown keypress", function (t) {
            (o = t.shiftKey) || 13 === t.which && (e.$apply(function () {
                e.$eval(n.enterWithoutShift)
            }), t.preventDefault())
        })
    }
});

app.directive("enterWithCtrl", function () {
    return function (e, t, n) {
        var o;
        t.bind("keydown keypress", function (t) {
            (o = t.ctrlKey) && 13 === t.which && (e.$apply(function () {
                e.$eval(n.enterWithCtrl)
            }), t.preventDefault())
        })
    }
});

app.directive("searchheader", ["$timeout", "$parse", function ($timeout, $parse) {
    return {
        restrict: "E",
        replace: !1,
        scope: {
            obj: "=obj",
            prop: "=prop",
            kotobeeGradient: "=kotobeeGradient",
            onValue: "=onValue",
            offValue: "=offValue",
            noCancel: "=noCancel",
            searchKeyOb: "=searchKeyOb",
            onsearch: "&"
        },
        templateUrl: kTemplate("headers/headerSearch.html"),
        link: function (n, o, r) {
            n.$watch("obj[prop]", function (i, a) {
                i === $parse(r.onValue)(n) && $timeout(function () {
                    o[0].getElementsByTagName("input")[0].focus()
                })
            }), n.hide = function () {
                n.obj[n.prop] = n.offValue
            }
        }
    }
}]);

app.directive("ngEnter", function () {
    return function (e, t, n) {
        t.bind("keydown keypress", function (t) {
            13 === t.which && (e.$apply(function () {
                e.$eval(n.ngEnter)
            }), t.preventDefault())
        })
    }
});

app.directive("nextchapterscroll", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("components/nextChapterScroll.html")
    }
});

app.directive("autofocusme", ["$timeout", function ($timeout) {
    return {
        link: function (t, n, o) {
            $timeout(function () {
                n[0].focus()
            }, 350)
        }
    }
}]);

app.directive("readerheader", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("headers/readerHeader.html"),
        scope: !1
    }
});

app.directive("headerdefault", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("headers/headerDefault.html"),
        scope: !1
    }
});

app.directive("headerselection", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("headers/headerSelection.html"),
        scope: !1
    }
});

app.directive("headersearchitems", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("headers/headerSearchItems.html"),
        scope: !1
    }
});

app.directive("headertextsize", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("headers/headerTextSize.html"),
        scope: !1
    }
});

app.directive("mouseEvents", ["nav", function (nav) {
    return {
        restrict: "A",
        link: function (e, t, o) {
            nav.init(t)
        }
    }
}]);

app.directive("vanillaScroll", function () {
    return {
        restrict: "A",
        link: function (e, t, n) {
            t[0].addEventListener("scroll", function () {
                e.scroll(this)
            })
        }
    }
});

app.directive("selectOptions", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("headers/selectOptions.html")
    }
});

app.directive("media", function () {
    return {
        restrict: "E",
        replace: !0,
        templateUrl: kTemplate("panels/media.html"),
        scope: !1
    }
});

app.directive("noteitem", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("components/noteItem.html")
    }
});

app.directive("bookmarkitem", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("components/bookmarkItem.html")
    }
});

app.directive("hlightitem", function () {
    return {
        restrict: "E",
        templateUrl: kTemplate("components/hlightItem.html")
    }
});

app.directive("bookThumb", function () {
    return {
        restrict: "E",
        replace: !0,
        templateUrl: kTemplate("panels/bookThumb.html")
    }
});

app.directive("thumbnail", function () {
    return {
        restrict: "A",
        link: function (e, t, n) {
            t.attr("style", "background-size:cover,width:50px,height:50px")
        }
    }
});

app.directive("showafterimageload", function () {
    return {
        restrict: "A",
        link: function (e, t, n) {
            t.bind("load", function (e) {
                e.currentTarget.style.opacity = 1, autoprefixTransform(e.currentTarget, "translate(-50%, -50%) scale(1)")
            })
        }
    }
});

app.directive("headerShrink", ["autohide", function (autohide) {
    return {
        restrict: "A",
        link: function (t, n, o) {
            autohide.setup(t, n, o)
        }
    }
}]);

app.directive("categoryList", ["autohide", function (e) {
    return {
        restrict: "E",
        templateUrl: kTemplate("components/categoryList.html"),
        scope: {
            rtl: "=rtl",
            list: "=list",
            parent: "=parent",
            backlist: "=backlist",
            odd: "=odd",
            categoryClicked: "=clicked",
            categoryBackClicked: "=backClicked"
        }
    }
}]);

app.factory("app", function () { return 1; });

app.factory("auth", ["$q", "backend", "modals", "$location", "sync", "$ionicHistory", "status", "book", "view", "chapters", "app", "translit", function ($q, backend, modals, $location, sync, $ionicHistory, status, book, view, chapters, app, translit) {
    function g() {
        if ({}.watermarkPlaceholder) {
            var e = '<div id="watermark"><span>' + translit.get("createdUsing") + '</span><a href="#" class="inlineBlock" style="vertical-align: sub" onclick="window.open(\'https://www.kotobee.com\', \'_system\');return false;"><img width="163" src="' + rootDir + 'img/ui/authorLogo.png"></a></div>',
                t = document.createElement("div");
            t.innerHTML = e, document.body.insertBefore(t, document.getElementById("html5Fallback")), setTimeout(function () {
                addClass(document.getElementById("watermark"), "show")
            }, 4e3)
        }
    }
    var h = {};
    h.authenticate = function () {
        var r = $q.defer();
        return putTest("inside"), h.getPermissions(function (e) {
            view.generate();
            try {
                if ("book" == config.kotobee.mode && isKotobeeHosted())
                    if (config.kotobee.public) bookPath = publicRoot + "epub/";
                    else {
                        var r = publicRoot.split("/books/")[1].split("/")[0];
                        bookPath = "/books/" + r + "/EPUB/epub/"
                    }
                if ((native || desktop) && e && backend.cloudParam("offline") && "offline" == e.error) return void h.startup({
                    tab: "downloads"
                })
            } catch (e) { }
            backend.cloudParam("public") || backend.cloudParam("entry") ? h.startup() : backend.login().then(function (e) {
                e.error ? (modals.hide(), $location.path("/login")) : h.startup()
            })
        }), r.promise
    };

    h.getPermissions = function (e) {
        "library" == config.kotobee.mode ? backend.getUnauthLibrary(function (t) {
            if (t || (t = {}), "offline" != t.error) {
                if (t.cloud)
                    for (var n in t.cloud) config.kotobee[n] = t.cloud[n];
                config.kotobee.permsLoaded = !0, e()
            } else e(t)
        }) : "book" == config.kotobee.mode ? config.kotobee.cloudid ? backend.getPermissions(function (t) {
            for (var n in t) config.kotobee[n] = t[n];
            config.kotobee.permsLoaded = !0, e()
        }) : (config.kotobee.public = !0, e()) : app.getPermissions(e)
    };

    h.startup = function (e) {
        if (e || (e = {}), native && g(), "library" == config.kotobee.mode) {
            h.authenticated = !0;
            var t = "";
            e.tab && (t = "/tab/" + e.tab);
            try {
                $location.path("/library" + t)
            } catch (e) { }
            $ionicHistory.clearHistory()
        } else "book" == config.kotobee.mode ? book.loadBook().then(function () {
            h.authenticated = !0, $location.path("/reader"), $ionicHistory.clearHistory(), sync.initialize(), setTimeout(function () {
                chapters.initialize()
            }, 1e3)
        }) : "app" == config.kotobee.mode && (h.authenticated = !0, $location.path("/app"), $ionicHistory.clearHistory());
        kotobee && kotobee.dispatchEvent("ready")
    };

    h.redeemCode = function (e, o) {
        var r = clone(e),
            i = {
                model: {}
            };
        i.submit = function () {
            modals.hide(), r.code = i.model.code, r.code && (status.update(translit.get("pleaseWait"), null, {
                dots: !0
            }), backend.redeemCode(r, function (e) {
                e && !e.error ? (status.update(), o()) : status.showAndHide(translit.get(e ? e.error : "unexpectedError"), 3e3)
            }))
        }, modals.show({
            mode: "redeemCode",
            redeemCodeInfo: i
        })
    };

    return h;
}]);

app.factory("autohide", ["$document", "$injector", "selection", "chapters", "stg", function ($document, $injector, selection, chapters, stg) {
    function s() {
        var t = {},
            n = document.getElementById("libraryThumbs");
        if (n && "none" != getComputedStyle(n).display) t.mode = "library", t.header = $document[0].getElementById("libraryHeader"), t.footer = $document[0].getElementById("libraryFooter"), t.footer && (t.footerHeight = t.footer.offsetHeight), t.scope = angular.element(n).scope(), t.libTabs = $document[0].getElementById("libraryTabs"), t.libTabs && (t.libTabHeight = t.libTabs.offsetHeight);
        else {
            t.mode = "reader", t.header = $document[0].getElementById("readerHeader"), t.scope = angular.element(t.header).scope(), t.tabMenu = $document[0].getElementById("tabMenu"), t.tabMenuHeight = t.tabMenu.offsetHeight;
            var o = $document[0].body.getElementsByClassName("navControls");
            if (o.length) {
                var r = (o = o[o.length - 1]).getElementsByClassName("next");
                r.length && (t.navNext = r[0]);
                var i = o.getElementsByClassName("prev");
                i.length && (t.navPrev = i[0]), t.navWidth = t.navNext.offsetWidth
            }
        }
        return t.headerHeight = t.header.offsetHeight, ios && (t.headerHeight -= 20), t
    }

    function l(e) {
        m = !0, selection.active() ? selection.reset() : "hidden" == h ? g("buttons") : "buttons" == h ? g("hidden") : "midway" == h && g("buttons")
    }

    function c(e) {
        function t(t, n) {
            Math.abs(e.clientY - w.y) < 15 || (e.clientY < w.y ? w.y > .85 * document.body.clientHeight && g(t) : e.clientY > .85 * document.body.clientHeight && g(n), e.clientY > w.y ? w.y < .15 * document.body.clientHeight && g(t) : e.clientY < .15 * document.body.clientHeight && g(n))
        }
        k = null;
        var n = s();
        if ("reader" == n.mode) "neverShow" == config.autohideReaderMode && w && (new Date).getTime() - C < 800 && Math.abs(e.clientX - w.x) < 50 && t("tab", "hidden");
        else if ("library" == n.mode) {
            if ("showOnScroll" == config.autohideLibMode) {
                var o = document.getElementsByClassName("bookThumbsContainer")[0],
                    r = document.getElementById("libraryThumbs"),
                    i = Number(getElemPadding(r).top);
                i || (i = 0), o.offsetHeight < r.offsetHeight - i && g("buttons")
            }
            "neverShow" == config.autohideLibMode && w && (new Date).getTime() - C < 800 && Math.abs(e.clientX - w.x) < 50 && t("header", "hidden")
        }
        w = null
    }

    function d(e) {
        w = {
            x: e.clientX,
            y: e.clientY
        }, C = (new Date).getTime()
    }

    function u(e) {
        var n = 40;
        if (w) {
            k || (k = s());
            var o = k;
            if ("library" == o.mode && (n = 5), !("reader" == o.mode && "showOnTap" == config.autohideReaderMode || "library" == o.mode && "showOnTap" == config.autohideLibMode)) {
                if (y || (y = Math.max(n, e.detail.scrollTop)), m) return m = !1, y = Math.max(n, e.detail.scrollTop), void ("hidden" == h && (y -= o.headerHeight, b = o.headerHeight));
                if ("reader" == o.mode) {
                    if (I) return;
                    if ("default" != o.scope.header.mode) return;
                    if ($injector.get("nav").transforming) return
                } else if ("library" == o.mode) {
                    if (o.scope.subMode) return;
                    if (o.scope.searchOb.enabled) return
                } (v = o.headerHeight - (o.headerHeight - (e.detail.scrollTop - y))) >= o.headerHeight ? (y = e.detail.scrollTop - o.headerHeight, v = o.headerHeight, h = "hidden") : v < 0 ? (y = Math.max(n, e.detail.scrollTop), v = 0, h = "buttons") : h = "midway", null == b && (b = 0);
                var r = removeClass;
                if (b == o.headerHeight) {
                    if (v == o.headerHeight) return;
                    if (v > 14) return;
                    y = Math.max(n, e.detail.scrollTop), v = 0, r = addClass
                }
                v + b != 0 && (b = v, p(r, {
                    dims: o
                }))
            }
        }
    }

    function p(e, t) {
        function n() {
            e(o.tabMenu, "ease"), f(o.tabMenu, v * o.tabMenuHeight / o.headerHeight, o.tabMenuHeight, "down", !!ios)
        }
        t || (t = {});
        var o = t.dims;
        if (o || (o = s()), e(o.header, "ease"), f(o.header, v, o.headerHeight, "up", !!ios), "reader" == o.mode) {
            if (t.headerOnly) return;
            if (t.tabOnly) return void n();
            e(o.navNext, "ease"), e(o.navPrev, "ease"), f(o.navNext, v * o.navWidth / o.headerHeight, o.navWidth, stg.data.settings.rtl ? "left" : "right"), f(o.navPrev, v * o.navWidth / o.headerHeight, o.navWidth, stg.data.settings.rtl ? "right" : "left"), n()
        } else if ("library" == o.mode) {
            if (o.libTabs || (o = s()), o.libTabs && (e(o.libTabs, "ease"), f(o.libTabs, v, o.libTabHeight, "up")), t.headerOnly) return;
            o.footer && (e(o.footer, "ease"), f(o.footer, v * o.footerHeight / o.headerHeight, o.footerHeight, "down", !!ios))
        }
    }

    function f(e, t, n, o, r) {
        var i = 1 - (t = Math.min(n, t)) / 44;
        ionic.requestAnimationFrame(function () {
            if (e) {
                var n;
                if ("up" == o ? n = "translate3d(0, -" + t + "px, 0)" : "down" == o ? n = "translate3d(0, " + t + "px, 0)" : "right" == o ? n = "translate3d(" + t + "px, 0, 0)" : "left" == o && (n = "translate3d(-" + t + "px, 0, 0)"), e.style[ionic.CSS.TRANSFORM] = n, r)
                    for (var a = 0, s = e.children.length; a < s; a++) e.children[a].style.opacity = i
            }
        })
    }

    function g(e) {
        var t = s();
        "buttons" == e ? (b = v = 0, p(addClass)) : "header" == e ? (b = v = 0, p(addClass, {
            headerOnly: !0
        })) : "tab" == e ? (b = v = 0, p(addClass, {
            tabOnly: !0
        })) : "hidden" == e && (b = v = t.headerHeight, p(addClass, {
            dims: t
        })), h = e
    }
    var h, m, v, b, y, k, w, C, x = {},
        I = !0;
    return x.initialize = function () {
        mobile && (document.body.addEventListener(mobile && !mobileDebug ? "touchend" : "mouseup", c), document.body.addEventListener(mobile && !mobileDebug ? "touchstart" : "mousedown", d), config.autohideHeader && !config.autohideReaderMode && (config.autohideReaderMode = "showOnScroll"), config.autohideHeader || (config.autohideReaderMode = "alwaysShow"), config.autohideLibHeader && !config.autohideLibMode && (config.autohideLibMode = "showOnScroll"), config.autohideLibHeader || (config.autohideLibMode = "alwaysShow"))
    }, x.setup = function (e, n, o) {
        if (h = "buttons", "reader" == o.headerShrink) {
            if ("alwaysShow" == config.autohideReaderMode) return;
            if ("neverShow" == config.autohideReaderMode) return g("hidden"), addClass(document.getElementById("chapters"), "fillSpace"), e.$watch("header.mode", function (e, t) {
                e != t && g("selection" == e || "searchItems" == e || "textSize" == e ? "header" : "hidden")
            }), void addClass(document.getElementById("tabMenu"), "transparent");
            "showOnTap" == config.autohideReaderMode && (g("hidden"), addClass(document.getElementById("chapters"), "transparentBtns"), addClass(document.getElementById("chapters"), "fillSpace"))
        }
        if ("library" == o.headerShrink) {
            if ("alwaysShow" == config.autohideLibMode) return;
            if ("neverShow" == config.autohideLibMode) return;
            config.autohideLibMode
        }
        if ($injector.get("nav").addListener("tap", l), "reader" == o.headerShrink) {
            var i;
            e.$watch("data.settings.textSize", function (e, t) {
                (i || (i = !0, "showOnTap" != config.autohideReaderMode)) && g("buttons")
            });
            var a;
            e.$watch("header.mode", function (e, t) {
                (a || (a = !0, "showOnTap" != config.autohideReaderMode)) && g("buttons")
            }), chapters.addListener(function (e) {
                "newChapter" == e && setTimeout(function () {
                    I = !1
                }, 1500)
            }), "showOnTap" == config.autohideReaderMode && g("hidden")
        } else "library" == o.headerShrink && (e.$watch("subMode", function (e, t) {
            g("buttons")
        }), e.$watch("searchOb.enabled", function (e, t) {
            g("buttons")
        }));
        n.bind("scroll", u)
    }, x.libInitialized = function () {
        setTimeout(function () {
            "neverShow" == config.autohideLibMode && g("hidden"), "showOnTap" == config.autohideLibMode && g("hidden")
        })
    }, x
}]);

app.factory("backend", ["$http", "error", "translit", "$location", "$injector", "$ionicHistory", "$rootScope", "modals", "$window", "crdv", "$sce", "$timeout", "stg", "$q", function ($http, error, translit, $location, $injector, $ionicHistory, $rootScope, modals, $window, crdv, $sce, $timeout, stg, $q) {
    function v(e) {
        for (var t = 0; t < C.length; t++) C[t].event == e && C[t].func()
    }

    function b() {
        try {
            if ("undefined" != typeof lti && lti && email && hash) {
                stg.data.user.email = email, stg.data.user.pwd = hash, stg.data.user.rememberMe = !0;
                var e = document.getElementsByTagName("head")[0];
                if (e.length) {
                    e[0];
                    for (var t = document.getElementsByTagName("script"), n = 0; n < t.length; n++)
                        if (t[n].hasAttribute("lti")) {
                            t[n].parentNode.removeChild(t[n]);
                            break
                        }
                }
            }
        } catch (e) { }
    }

    function y(e) {
        if (stg.data.currentLibrary = e, e && e.categories) {
            for (var t = [], n = 0; n < e.categories.length; n++) t.push(e.categories[n]);
            for (n = 0; n < e.categories.length; n++)
                for (var o = e.categories[n], r = 0; r < t.length; r++) t[r].parent && t[r].parent.id == o.id && (o.children || (o.children = []), o.children.push(t[r]), t[r].nested = !0);
            for (n = 0; n < e.categories.length; n++) e.categories[n].nested && (e.categories.splice(n, 1), n--)
        }
    }
    var k, w = {},
        C = [];
    w.getAuthVarsOld = function () {
        return {
            libid: config.kotobee.libid,
            email: stg.data.user.email,
            pwd: stg.data.user.pwd
        }
    };

    w.getAuthVars = function () {
        var e = {
            email: stg.data.user.email,
            pwd: stg.data.user.pwd,
            code: stg.data.user.code,
            env: config.kotobee.mode
        };
        return b(), e.cloudid = config.kotobee.cloudid, "library" == e.env && stg.data.book && (e.bid = stg.data.book.id), uuid && (e.fingerprint = uuid), e
    };

    w.addListener = function (e, t) {
        C.push({
            event: e,
            func: t
        })
    };

    w.initialize = function (e) {
        e || (e = {
            putTest: function () { },
            $$phase: !0
        });
        arguments.callee;
        putTest("X");
        var n = $q.defer();
        return putTest("Y"), putTest("Z"), putTest("J"), error.detect("s_authError", w.loginErrorDetection), putTest("K"), stg.readSlot("user" + config.kotobee.cloudid, function (t) {
            if (config.kotobee.cloudid) {
                var o = t;
                o && (stg.data.user = o)
            }
            stg.data.user || (stg.data.user = {}), 0 == stg.data.user.length && (stg.data.user = {}), stg.data.user.loggedIn = !1, putTest("L"), crdv.initialize(e, function () {
                preview ? n.resolve() : w.getFingerprint(function (e) {
                    uuid = e, n.resolve()
                })
            })
        }), n.promise
    };

    w.getFingerprint = function (e) {
        function t() {
            var t = {
                excludeUserAgent: !0,
                excludeScreenResolution: !0,
                excludeDoNotTrack: !0,
                excludeAvailableScreenResolution: !0,
                excludeOpenDatabase: !0,
                excludePlugins: !0,
                excludeCanvas: !0,
                excludeWebGL: !0,
                excludeJsFonts: !0
            };
            t.excludeCanvas = !1, t.excludeWebGL = !1, new Fingerprint2(t).get(function (t, n) {
                try {
                    t || (t = process.env.USERNAME), stg.writeSlot("fingerprint", t, function () {
                        e(t)
                    })
                } catch (e) { }
            })
        }
        native && window.plugins && window.plugins.uniqueDeviceID ? window.plugins.uniqueDeviceID.get(function (n) {
            n && (n.length ? e(n) : t())
        }, t) : desktop ? "mac" == os ? t() : require("child_process").exec("wmic CPU get ProcessorId", {
            cwd: "."
        }, function (n, o, r) {
            if (o) {
                var i = o.toUpperCase().split("PROCESSORID");
                if (i.length) {
                    var a = i[1].trim();
                    a.length ? e(a) : t()
                } else t()
            } else t()
        }) : t()
    };
    var x = null,
        I = null;
    w.setLibraryLoginCallback = function (e) {
        x = e
    };

    w.setLibraryLogoutCallback = function (e) {
        I = e
    };

    w.loginErrorDetection = function (e) {
        stg.data.user.loggedIn = !1, e && ("email" == config.kotobee.loginmode ? e.loginOptions.canLoginByEmail = !0 : "code" == config.kotobee.loginmode ? e.loginOptions.canLoginByCode = !0 : e.loginOptions.canLoginByEmail = e.loginOptions.canLoginByCode = !0, e.loginOptions.mode = e.loginOptions.canLoginByEmail ? "email" : "code", e.loginOptions.signIn = function () {
            modals.hide(), setTimeout(function () {
                function n() {
                    w.login({
                        userInput: !0
                    }).then(function (t) {
                        t.error || e.model && (e.model.pwd = e.model.code = null)
                    })
                }
                if ("code" == e.loginOptions.mode) {
                    if (!e.model || !e.model.code) return void error.update({
                        error: "enterYourCode"
                    });
                    stg.data.user.code = e.model.code, stg.data.user.email = stg.data.user.pwd = null
                } else {
                    if (!stg.data.user.email) return void error.update({
                        error: "enterYourEmail"
                    });
                    if (!e.model || !e.model.pwd) return void error.update({
                        error: "enterYourPwd"
                    });
                    stg.data.user.pwd = e.model.pwd, stg.data.user.code = null
                }
                config.kotobee.permsLoaded ? n() : $injector.get("auth").getPermissions(n)
            }, 800)
        }, e.loginOptions.register = function () {
            modals.hide(), $location.path("/register")
        }, e.loginOptions.forgotPassword = function () {
            modals.hide(), $location.path("/forgotpwd")
        }, e.loginOptions.redeemCode = function () {
            e.loginOptions.mode = "code"
        }, e.loginOptions.signInByEmail = function () {
            e.loginOptions.mode = "email"
        })
    };
    var E = null;

    w.refreshSignatures = function (t) {
        var n = config.kotobee.liburl + "user/signatures";
        $http.post(n, getFormData(w.getAuthVars()), postOptions).success(function (e) {
            e.error || (stg.data.user.signatures = e.signatures, t && t())
        })
    };

    var T = {
        ping: !0
    };

    w.login = function (o, r) {
        o || (o = {});
        var i = {
            func: arguments.callee,
            args: arguments
        };
        document.body.focus = null;
        var a, s = $q.defer();
        o.ping || (a = modals.show({
            mode: "loading",
            loadingMsg: translit.get("pleaseWait"),
            timeLimit: 1e4
        }), stg.data.user.loggedIn = !1), r && (E = r);
        var c;
        "book" == config.kotobee.mode && (c = config.kotobee.liburl + "library/auth"), "library" == config.kotobee.mode && (c = config.kotobee.liburl + "library/get"), b(), preview || (w.fingerprint = uuid);
        var d = w.getAuthVars();
        return o.userInput && (d.input = !0), d.platformdesc = navigator.userAgent, native ? (d.platform = "mobile", device && (d.platformdesc = device.platform + " " + device.model + " " + device.version + " " + (device.manufacturer ? device.manufacturer : ""))) : d.platform = desktop ? "desktop" : "web", d.v = readerV, o.ping && (d.ping = !0, d.pwd = o.pwd), d.v = config.v, $http.post(c, getFormData(d), postOptions).success(function (e) {
            function n() {
                if (!e.cloudid && !e.success) return o.dontShowErrorDialog || error.update({
                    error: "s_authError"
                }), void s.resolve({
                    error: "s_authError"
                });
                stg.data.user.loggedIn = !0, "library" == config.kotobee.mode && (y(e), x && x()), s.resolve(e), E && (E(e), E = null)
            }
            if (clearTimeout(void 0), a && modals.hide(a), e.error) return o.dontShowErrorDialog || error.update(e), stg.data.user.pwd = null, stg.data.user.code = null, o.ping && w.logout(), void s.resolve(e);
            var r = 3e5;
            if (void 0 === r && (r = 3e5), k && clearTimeout(k), k = setTimeout(function () {
                e.pwd && (T.pwd = e.pwd), w.login(T)
            }, r), !o.ping)
                if (stg.data.user.pwd = e.pwd, stg.data.user.id = e.userid, stg.data.user.name = e.username, stg.data.user.lastchap = e.lastchap, stg.data.user.signatures = e.signatures, stg.data.user.role = e.role, stg.data.user.rememberMe) {
                    var i = {
                        rememberMe: !0
                    };
                    stg.data.user.email && stg.data.user.pwd ? (i.email = stg.data.user.email, i.pwd = stg.data.user.pwd) : stg.data.user.code && (i.code = stg.data.user.code), stg.writeSlot("user" + config.kotobee.cloudid, i, n)
                } else stg.clearSlot("user" + config.kotobee.cloudid, n)
        }).error(function (e) {
            clearTimeout(void 0), modals.hide(), error.update({
                error: "networkError",
                ref: i
            })
        }), s.promise
    };

    w.logout = function () {
        stg.data.user.pwd = null, stg.data.user.loggedIn = !1, stg.data.user.email = null, stg.data.user.code = null, stg.writeSlot("user" + config.kotobee.cloudid, stg.data.user, function () {
            "library" == config.kotobee.mode ? $timeout(function () {
                if (error.update({
                    error: "s_authError"
                }), w.cloudParam("entry")) {
                    for (var e = 0; e < stg.data.currentLibrary.books.length; e++) stg.data.currentLibrary.books[e].favorite = !1;
                    crdv.applyExistingBooks()
                } else stg.data.currentLibrary = {};
                I && I()
            }, 200) : ($location.path("/login"), $ionicHistory.clearHistory())
        })
    };

    w.getPermissions = function (n) {
        var o = {
            func: arguments.callee,
            args: arguments
        },
            r = {};
        return r.env = config.kotobee.mode, r.cloudid = config.kotobee.cloudid, $http.post(config.kotobee.liburl + "library/permissions", getFormData(r), postOptions).success(function (e) {
            if (e.error) return error.update({
                error: "errorAndCallback",
                msg: e.error,
                cb: crdv.exit
            }), angular.element(document.getElementById("home")).scope().hideLoading = !0, void ($rootScope.$$phase || $rootScope.$apply());
            n && n(e)
        }).error(function (e, r) {
            (native || desktop) && w.cloudParam("offline") ? n && n({
                error: "offline"
            }) : error.update({
                error: "networkError",
                ref: o
            })
        })
    };

    w.register = function (o, r) {
        var i = {
            func: arguments.callee,
            args: arguments
        },
            a = modals.show({
                mode: "loading",
                loadingMsg: translit.get("registeringAccount")
            }),
            s = w.getAuthVars();
        s.email = o.email1, s.promocode = o.promocode, s.root = config.kotobee.liburl;
        var c = getFormData(s);
        return c.append("platform", navigator.userAgent), $http.post(config.kotobee.liburl + "user/register", c, postOptions).success(function (e) {
            modals.hide(a), e.error ? error.update(e) : r && r(e)
        }).error(function (e) {
            error.update({
                error: "networkError",
                ref: i
            })
        })
    };

    w.forgotPwd = function (o, r) {
        var i = {
            func: arguments.callee,
            args: arguments
        },
            a = modals.show({
                mode: "loading",
                loadingMsg: translit.get("retrieving")
            }),
            s = w.getAuthVars();
        s.email = o.email, s.root = config.kotobee.liburl;
        var c = getFormData(s);
        return $http.post(config.kotobee.liburl + "user/forgotpwd", c, postOptions).success(function (e) {
            modals.hide(a), e.error ? error.update(e) : r && r(e)
        }).error(function (e) {
            error.update({
                error: "networkError",
                ref: i
            })
        })
    };

    w.redeemCode = function (n, o) {
        var r = {
            func: arguments.callee,
            args: arguments
        },
            i = getFormData(w.getAuthVars());
        i.append("redeemcode", n.code), i.append("platform", navigator.userAgent), $http.post(config.kotobee.liburl + "user/redeemcode", i, postOptions).success(function (e) {
            o(e)
        }).error(function () {
            error.update({
                error: "networkError",
                ref: r
            })
        })
    };

    w.downloadEbookTest = function (e, n) {
        var o = {
            responseType: "blob",
            onProgress: function (e) { }
        },
            r = {
                id: e.id,
                mode: e.binary
            };
        return w.sendRawPost("http://192.168.1.5/files/test.zip", r, o, function (e) {
            if (e.error) return error.update(e), void n();
            n(e)
        })
    };

    w.downloadEbook = function (e, n, o) {
        try {
            var r = {
                responseType: "blob",
                onProgress: n
            },
                i = {
                    id: e.id,
                    mode: "binary",
                    requester: "reader"
                };
            return w.sendRawPost(config.kotobee.liburl + "library/ebook/download", i, r, function (e) {
                if (e.error) return error.update(e), void o();
                o(e)
            })
        } catch (e) { }
        return
    };

    w.tincan = function (t, n) {
        var o = getFormData(w.getAuthVars());
        for (var r in t) o.append(r, t[r]);
        $http.post(config.kotobee.liburl + "tincan", o, postOptions).success(function (e) {
            n && n()
        }).error(function (e, t) { })
    };

    w.getUnauthLibrary = function (n) {
        if ("library" == config.kotobee.mode) {
            var o = {
                func: arguments.callee,
                args: arguments
            },
                r = getFormData(w.getAuthVars());
            $http.post(config.kotobee.liburl + "library/getbasic", r, postOptions).success(function (e) {
                y(e), n && n(e)
            }).error(function (e, r) {
                (native || desktop) && w.cloudParam("offline") ? n && n({
                    error: "offline"
                }) : error.update({
                    error: "networkError",
                    ref: o
                })
            })
        }
    };

    w.addtoFav = function (n, o) {
        var r = {
            func: arguments.callee,
            args: arguments
        },
            i = getFormData(w.getAuthVars());
        i.append("bid", n.id), i.append("cloudid", w.cloudParam("cloudid")), i.append("type", "book"), $http.post(config.kotobee.liburl + "library/favorite/add", i, postOptions).success(function (e) {
            if (e.error) return error.update(e), void o();
            o(e)
        }).error(function () {
            error.update({
                error: "networkError",
                ref: r
            })
        })
    };

    w.removeFav = function (n, o) {
        var r = {
            func: arguments.callee,
            args: arguments
        },
            i = getFormData(w.getAuthVars());
        i.append("bid", n.id), i.append("cloudid", w.cloudParam("cloudid")), i.append("type", "book"), $http.post(config.kotobee.liburl + "library/favorite/remove", i, postOptions).success(function (e) {
            if (e.error) return error.update(e), void o();
            o(e)
        }).error(function (e) {
            error.update({
                error: "networkError",
                ref: r
            })
        })
    };

    w.getServerLang = function (t, n) {
        if (isKotobeeHosted()) {
            var o = getFormData();
            return o.append("lang", t.lang), o.append("root", rootDir), o.append("v", readerV), o.append("base", publicRoot), $http.post(config.kotobee.liburl + "library/getlang", o, postOptions)
        }
        return $http.get(cacheBust(rootDir + "locales/" + t.lang + ".js"))
    };

    w.getS3Paths = function (t) {
        var n = getFormData();
        $http.post(config.kotobee.liburl + "library/s3paths", n, postOptions).success(function (e) {
            stg.data.paths = e, v("pathsChanged"), t && t(e)
        }).error(function () { })
    };

    w.addBookStat = function (t, n) {
        if (t || (t = {}), !preview && config.kotobee.cloudid) {
            window.location.href;
            var o = getFormData(w.getAuthVars());
            o.append("type", t.type), $http.post(config.kotobee.liburl + "library/ebook/stat", o, postOptions).success(function (e) { }).error(function () { })
        }
    };

    w.addStat = function (t, n) {
        if (t || (t = {}), !preview) {
            var o = window.location.href;
            if (!(o.indexOf("localhost/") >= 0 || o.indexOf("localhost:") >= 0)) {
                var r = getFormData(w.getAuthVars());
                r.append("software", $rootScope.readerApp ? "readerapp" : "reader"), r.append("platform", navigator.userAgent), r.append("assetType", config.kotobee.mode);
                try {
                    r.append("assetName", "book" == config.kotobee.mode ? stg.data.book.meta.dc.title : stg.data.currentLibrary.name)
                } catch (e) { }
                r.append("assetPath", o.split("#")[0]), config.kotobee.cloudid && r.append("assetId", config.kotobee.cloudid), stg.data.user.email && r.append("email", stg.data.user.email), stg.data.user.code && r.append("code", stg.data.user.code), t.action && r.append("action", t.action), t.param && r.append("param", t.param), $http.post(config.kotobee.liburl + "stat/add", r, postOptions).success(function (e) { }).error(function () { })
            }
        }
    };

    w.setUserData = function (t, n) {
        var o = getFormData(w.getAuthVars());
        o.append("label", t.label), o.append("data", t.data), t.overwrite && o.append("overwrite", t.overwrite), t.category && o.append("category", t.category), t.subcategory && o.append("subcategory", t.subcategory), t.options && o.append("options", t.options), $http.post(config.kotobee.liburl + "cloud/set", o, postOptions).success(function (e) {
            if (e.error) throw e;
            n(e)
        }).error(function () {
            throw "Network Error. Please try again"
        })
    };

    w.getUserData = function (t, n) {
        var o = getFormData(w.getAuthVars());
        t.label && o.append("label", t.label), t.category && o.append("category", t.category), t.subcategory && o.append("subcategory", t.subcategory), t.allUsers && o.append("allUsers", !0), t.options && o.append("options", t.options), $http.post(config.kotobee.liburl + "cloud/get", o, postOptions).success(function (e) {
            if (e.error) throw e;
            n(e)
        }).error(function () {
            throw "Network Error. Please try again"
        })
    };

    w.getUserList = function (t, n) {
        var o = getFormData(w.getAuthVars());
        t.role && o.append("role", t.role), $http.post(config.kotobee.liburl + "cloud/users", o, postOptions).success(function (e) {
            if (e.error) throw e;
            n(e)
        }).error(function () {
            throw "Network Error. Please try again"
        })
    };

    w.getUserDataArray = function (t, n) {
        var o = getFormData(w.getAuthVars());
        t.array && o.append("array", t.array), $http.post(config.kotobee.liburl + "cloud/getarray", o, postOptions).success(function (e) {
            if (e.error) throw e;
            n(e)
        }).error(function () {
            throw "Network Error. Please try again"
        })
    };

    w.deleteUserData = function (t, n) {
        var o = getFormData(w.getAuthVars());
        o.append("label", t.label), o.append("data", t.data), t.all && o.append("all", t.all), t.category && o.append("category", t.category), t.subcategory && o.append("subcategory", t.subcategory), t.options && o.append("options", t.options), $http.post(config.kotobee.liburl + "cloud/delete", o, postOptions).success(function (e) {
            if (e.error) throw e;
            n(e)
        }).error(function () {
            throw "Network Error. Please try again"
        })
    };

    w.externalLink = function (e, t) {
        try {
            if (t || (t = {}), t.secure && !native && !desktop && !preview) return void $window.open(e);
            var o = function (e, t) {
                native ? crdv.openInNativeBrowser(e, t) : desktop ? require("nw.gui").Shell.openExternal(e) : window.open(e, "_blank")
            };
            if (t.target && "_blank" == t.target.toLowerCase()) return void o(e, !0);
            if (native) return void o(e);
            var r = {
                title: translit.get("externalLink"),
                src: $sce.trustAsResourceUrl(e),
                openInNativeBrowser: o
            };
            r.status = "loading";
            var i = setTimeout(function () {
                r.status = "", r.error = translit.get("errorLoadingPage"), $rootScope.$$phase || $rootScope.$apply()
            }, 1e4);
            r.loaded = function (e, t, n) {
                r.status = "loaded", clearTimeout(i), $rootScope.$$phase || $rootScope.$apply()
            };
            var a = modals.show({
                mode: "externalSite",
                animation: "slide-in-up",
                dynamic: !0,
                backdropClickToClose: !1,
                siteOptions: r
            });
            desktop || preview || w.getXframeOptions({
                url: e
            }, function (t) {
                if (r.status = "loaded", $rootScope.$$phase || $rootScope.$apply(), t && t.success) {
                    var n = t.success.toLowerCase();
                    "sameorigin" != n && "deny" != n || (modals.hide(a), setTimeout(function () {
                        modals.show({
                            mode: "xFrameError",
                            errorOptions: {
                                url: e,
                                openInNativeBrowser: o
                            }
                        })
                    }, 500))
                }
            })
        } catch (e) { }
    };

    w.createRequest = function (t, n, o) {
        n || (n = {});
        var r = getFormData(w.getAuthVars());
        for (var i in n) r.append(i, n[i]);
        $http.post(t, r, postOptions).success(function (e) {
            o(e)
        }).error(function () {
            o(!1)
        })
    };

    w.convertPDF = function (t, n) {
        if (t || (t = {}), !preview) {
            var o = getFormData(w.getAuthVars());
            o.append("url", t.url), $http.post(config.kotobee.liburl + "library/pdf", o, postOptions).success(function (e) {
                n(e)
            }).error(function () { })
        }
    };

    w.getXframeOptions = function (t, n) {
        arguments.callee;
        var o = getFormData(w.getAuthVars());
        o.append("url", t.url), $http.post(config.kotobee.liburl + "getxframe", o, postOptions).success(function (e) {
            e.error ? n() : n(e)
        }).error(function (e) { })
    };

    w.hasAccessTo = function (t, n) {
        if ("library" == config.kotobee.mode) {
            var o = getFormData(w.getAuthVars());
            t || (t = {});
            for (var r in t) o.append(r, t[r]);
            o.append("v", readerV), $http.post(config.kotobee.liburl + "user/access", o, postOptions).success(function (e) {
                n && n(e)
            }).error(function (e, t) {
                n && n({
                    error: ""
                })
            })
        } else n && n({
            success: !0
        })
    };

    w.setLastChapter = function (t) {
        var n = getFormData(w.getAuthVars());
        n.append("chapter", currentIndex), $http.post(config.kotobee.liburl + "user/lastchap", n, postOptions).success(function (e) {
            t && t(e)
        }).error(function (e, n) {
            t && t({
                error: ""
            })
        })
    };

    w.cloudParam = function (e) {
        return config.kotobee[e]
    };

    w.getEbook = function (n, o) {
        var r = {
            func: arguments.callee,
            args: arguments
        },
            i = getFormData(w.getAuthVars());
        for (var a in n) i.append(a, n[a]);
        $http.post(config.kotobee.liburl + "library/ebook/get", i, postOptions).success(function (e) {
            e.error && error.update(e), o && o(e)
        }).error(function (e) {
            error.update({
                error: "networkError",
                ref: r
            }), o && o({
                error: "networkError"
            })
        })
    };

    w.getEbooks = function (n, o, r) {
        var i = {
            func: arguments.callee,
            args: arguments
        },
            a = getFormData(w.getAuthVars());
        for (var s in n) a.append(s, n[s]);
        a.append("v", readerV), $http.post(config.kotobee.liburl + "library/ebook/all", a, postOptions).success(function (e) {
            e.error && error.update(e), r && r(e, o)
        }).error(function (e) {
            error.update({
                error: "networkError",
                ref: i
            }), r && r({
                error: "networkError"
            }, o)
        })
    };

    w.sendRawPost = function (e, t, n, o, r) {
        r || (r = 0);
        for (var i = "", a = [w.getAuthVars(), t], s = 0; s < a.length; s++)
            for (var l in a[s]) {
                var c = a[s][l];
                void 0 != c && ("object" != typeof c && ("string" == typeof c && (c = encodeURIComponent(c = ("." + c).substr(1))), "boolean" == typeof c && (c = c ? 1 : 0), i += "&" + l + "=" + c))
            }
        var d = new XMLHttpRequest;
        d.open(n.method ? n.method : "POST", e, !0), "GET" != n.method && d.setRequestHeader("Content-type", "application/x-www-form-urlencoded"), n.onProgress && (d.onprogress = function (e) {
            e.lengthComputable && n.onProgress(e.loaded / e.total)
        }), n.responseType && (d.responseType = n.responseType);
        var u;
        return d.addEventListener("error", function (e) {
            o({
                error: "networkError"
            }), u = !0
        }, !1), d.onreadystatechange = function (e) {
            d.aborted || 4 == d.readyState && (200 == d.status ? $timeout(function () {
                o(d.response)
            }) : d.onerror())
        }, d.onerror = function (i) {
            if (!u) {
                if (!(r >= 3)) return w.sendRawPost(e, t, n, o, ++r);
                $timeout(function () {
                    o({
                        error: "d_connectionTimedOut"
                    })
                })
            }
        }, "GET" != n.method ? d.send(i) : d.send(), d
    };

    w.abort = function (e) {
        e && 4 != e.readyState && (e.aborted = !0, e.abort())
    };

    return w;
}]);

app.factory("book", ["$http", "error", "$rootScope", "crdv", "translit", "modals", "design", "$location", "np", "sync", "jsParser", "chapters", "cssParser", "$q", "status", "backend", "tinCan", "stg", "$filter", function ($http, error, $rootScope, crdv, translit, modals, design, $location, np, sync, jsParser, chapters, cssParser, $q, status, backend, tinCan, stg, $filter) {
    function x(t, n) {
        return !t.dver || t.dver < 2 ? n() : config.noBookOverride ? n() : void $http.get(cacheBust(ph.join(bookPath, "_kmeta/config.js"))).success(function (e) {
            if (-1 != e.indexOf("var config")) {
                e = e.replace("var config", "var bookConfig");
                var t = document.createElement("script");
                t.setAttribute("bookconfig", !0);
                var o = document.getElementsByTagName("head");
                o.length && o[0].appendChild(t), t.innerHTML = e, T || (T = clone(config));
                for (var r in bookConfig) "kotobee" != r && (config[r] = bookConfig[r]);
                design.injectUserCSS(config), design.convertTabs(), n && n()
            } else n()
        }).error(function () {
            n && n()
        })
    }

    function I(e) {
        for (var t = document.createElement("ol"), n = 0; n < e.childNodes.length; n++)
            if (e.childNodes[n]) {
                var o = e.childNodes[n];
                if (8 != o.nodeType && "#text" != o.nodeName && "navpoint" == o.nodeName.toLowerCase()) {
                    for (var r, i, a, s = 0; s < o.childNodes.length; s++)
                        if (o.childNodes[s]) {
                            var l = o.childNodes[s].nodeName.toLowerCase();
                            "content" == l ? i = o.childNodes[s].getAttribute("src") : "navlabel" == l ? r = o.childNodes[s].textContent.trim() : "navpoint" == l && (a || (a = I(o)))
                        }
                    var c = document.createElement("li"),
                        d = document.createElement("a");
                    d.innerHTML = r || "", d.setAttribute("href", i ? ph.join(stg.data.tocPath, i) : ""), c.appendChild(d), a && c.appendChild(a), t.appendChild(c)
                }
            }
        return t
    }
    var E = {};
    E.root = ""; E.initialize = function () { };

    E.getCoverImgPath = function (e, t) {
        if (t || (t = e.path), !e.cover) return "";
        var n = "";
        return n = stg.data.currentLibrary.cloud.public ? stg.getPublicRoot("book") + e.path + "/EPUB/" + e.cover : $filter("coverPath")(e.cover, t), e.exists && (native ? n = crdv.libEbooksDirectory.nativeURL + t + "/EPUB/" + e.cover.replace("_thumb", "") : desktop && (n = ph.normalize(np.getBooksPath() + t + "/" + e.cover.replace("_thumb", "")))), n
    };

    E.getBgImgPath = function (e, t) {
        if (t || (t = e.path), !e.cover) return "";
        var n = stg.getPublicRoot("book");
        return n += e.path + "/" + e.bgimg
    };

    E.open = function (e) {
        if (preview) return modals.hide(), void setTimeout(function () {
            modals.show({
                mode: "generalError",
                errorOptions: {
                    msg: translit.get("cantPreviewLibBook")
                }
            })
        }, 500);
        native ? (crdv.vibrate(), bookPath = crdv.libEbooksDirectory.nativeURL + e.path + "/EPUB/") : desktop ? (bookPath = np.getBooksPath() + e.path + "/", "mac" == os && (bookPath = "file://" + bookPath)) : (bookPath = stg.data.currentLibrary.cloud.public ? stg.getPublicRoot("book") : config.kotobee.liburl + "books/", bookPath += e.path + "/EPUB/"), modals.hide(), setTimeout(function () {
            status.update(translit.get("pleaseWait"), null, {
                dots: !0
            }),
                function (t) {
                    (native || desktop) && e.exists ? t({
                        success: !0
                    }) : backend.hasAccessTo({
                        testagainst: e.id,
                        open: !0
                    }, t)
                }(function (t) {
                    t.success ? E.loadBook().then(function () {
                        x(e, function () {
                            if (stg.data.book.id = e.id, stg.data.epub.bookThumb = E.getCoverImgPath(e, stg.data.book.path), chromeApp)
                                if (stg.data.chromeAssets[stg.data.book.bookThumb]) stg.data.epub.bookThumb = stg.data.chromeAssets[stg.data.book.bookThumb];
                                else {
                                    var t = stg.data.epub.bookThumb;
                                    delete stg.data.epub.bookThumb, getImgDataSrc(t, function (e) {
                                        stg.data.epub.bookThumb = e, stg.data.chromeAssets[t] = e
                                    })
                                }
                            status.update(), E.root = "/book/" + stg.data.book.id, $location.path(E.root + "/reader"), sync.initialize(), setTimeout(function () {
                                chapters.initialize()
                            }, 1e3)
                        })
                    }) : status.update()
                })
        }, 200)
    };

    E.removeBookConfigElem = function () {
        for (var e = document.getElementsByTagName("script"), t = 0; t < e.length; t++)
            if (e[t].hasAttribute("bookconfig")) {
                e[t].parentNode.removeChild(e[t]);
                break
            }
        if (T) {
            for (var n in config) "kotobee" != n && delete config[n];
            for (var n in T) config[n] = T[n];
            design.injectUserCSS(config)
        }
    };
    var T;
    E.loadBook = function () {
        function n() {
            backend.addStat({
                action: "open"
            }), $filter("chapterHtml")(stg.data.toc), stg.data.book.chapters = stg.data.toc.innerHTML, l.resolve()
        };
        var s = {
            func: arguments.callee,
            args: arguments
        },
            l = $q.defer();
        return stg.resetBook(), cssParser.clearCache(), jsParser.clearCache(), chapters.clearCache(), $http.get(cacheBust(ph.join(bookPath, "META-INF/container.xml?v=" + readerV))).success(function (l) {
            config.kotobee.encodekey && (l = config.v >= 1.1 ? CryptoJS.AES.decrypt(l, config.kotobee.encodekey).toString(CryptoJS.enc.Utf8) : kInteractive.XORCipher().decode(config.kotobee.encodekey, l));
            var c = parser.parseFromString(l, "text/xml");
            stg.data.epub = {};
            try {
                stg.data.packageURL = c.getElementsByTagName("rootfile")[0].getAttribute("full-path")
            } catch (e) {
                return status.update(), void error.update({
                    error: "errorAndCallback",
                    msg: "cantReadConfigFile",
                    cb: crdv.exit
                })
            }
            var d = config.v,
                u = native || isKotobeeHosted();
            ! function (n) {
                window.location.origin;
                var r = Math.round(9999 * Math.random());
                "book" == config.kotobee.mode && !$rootScope.readerApp && d >= 1.3 && !u ? $http.get("js/templates.js?c=" + r).success(n).error(function () {
                    error.update({
                        error: "networkError",
                        ref: s
                    })
                }) : n()
            }(function (r) {
                $http.get(cacheBust(ph.join(bookPath, stg.data.packageURL))).success(function (t) {
                    function s() {
                        var e = angular.element(document.getElementById("home"));
                        e && (e.scope().hideLoading = !0, $rootScope.$$phase || $rootScope.$apply()), status.update(), modals.show({
                            mode: "generalError",
                            errorOptions: {
                                msg: translit.get("errorLoadingPage")
                            }
                        })
                    }
                    if ("book" == config.kotobee.mode && !$rootScope.readerApp)
                        if (d >= 1.3) {
                            if (u) {
                                if (0 != config.checksum.indexOf(CryptoJS.MD5(t.replace(/\s+/g, "")).toString())) return void s()
                            } else if (CryptoJS.MD5(t.replace(/\s+/g, "")).toString() + CryptoJS.MD5(r).toString() != config.checksum && (r = r.replace(/(\S)\n/g, "$1\r\n"), CryptoJS.MD5(t.replace(/\s+/g, "")).toString() + CryptoJS.MD5(r).toString() != config.checksum)) return s()
                        } else if (d >= 1.2 && CryptoJS.MD5(t).toString() != config.checksum) return void s();
                    config.kotobee.encodekey && (t = config.v >= 1.1 ? CryptoJS.AES.decrypt(t, config.kotobee.encodekey).toString(CryptoJS.enc.Utf8) : kInteractive.XORCipher().decode(config.kotobee.encodekey, t)), t = t.replace(/href="([^"]*?&[^"]*?)"/g, function (e, t) {
                        return 'href="' + t.replace(/&/g, "") + '"'
                    });
                    var l = parser.parseFromString(t, "text/xml");
                    stg.data.epub.metadata = l.getElementsByTagName("metadata")[0], stg.data.epub.manifest = l.getElementsByTagName("manifest")[0], stg.data.epub.spine = l.getElementsByTagName("spine")[0];
                    stg.data.mediaList = [], stg.data.book.meta = {}, stg.data.book.version = 3;
                    try {
                        for (w = 0; w < stg.data.epub.metadata.childNodes.length; w++) {
                            var c = stg.data.epub.metadata.childNodes[w];
                            if (8 != c.nodeType && "#text" != c.nodeName)
                                if (c.hasAttribute("name") && "meta" == c.nodeName.toLowerCase()) stg.data.book.meta[c.getAttribute("name")] = c.getAttribute("content");
                                else
                                    for (var p, f = (p = c.hasAttribute("property") ? c.getAttribute("property") : c.nodeName).split(":"), g = stg.data.book.meta, h = 0; h < f.length; h++) h == f.length - 1 ? g[f[h].toLowerCase()] = c.innerHTML ? c.innerHTML : c.textContent : (g[f[h].toLowerCase()] || (g[f[h].toLowerCase()] = {}), g = g[f[h].toLowerCase()])
                        }
                    } catch (e) { }
                    stg.initializeSettings();
                    for (var m = "image/.*", b = getElementsByAttribute(stg.data.epub.manifest, "item", "media-type", m), w = 0; w < b.length; w++) {
                        var x = b[w].getAttribute("href");
                        if (0 != x.indexOf("js/widgets/") && (0 != x.indexOf("xhtml/pdf") || -1 == x.indexOf("/page"))) {
                            (g = {}).thumbnail = ph.join(bookPath, stg.data.packageURL, b[w].getAttribute("href")), g.title = ph.removeExtension(ph.basename(g.thumbnail));
                            m = b[w].getAttribute("media-type");
                            g.type = m.split("image/")[1], stg.data.mediaList.push(g), chromeApp && isAbsoluteURLAndNotDataPath(g.thumbnail) && (stg.data.chromeAssets[g.thumbnail] ? g.thumbnail = stg.data.chromeAssets[g.thumbnail] : function (e, t) {
                                getImgDataSrc(e.thumbnail, function (n) {
                                    stg.data.chromeAssets[e.thumbnail] = n, e.thumbnail = n, t && t()
                                })
                            }(g))
                        }
                    }
                    var E = getElementsByAttribute(stg.data.epub.manifest, "item", "properties", "cover-image");
                    E.length > 0 ? stg.data.epub.bookThumb = $filter("coverPath")(E[0].getAttribute("href")) : stg.data.book.meta.cover && (E = getElementsByAttribute(stg.data.epub.manifest, "item", "id", stg.data.book.meta.cover)).length > 0 && (stg.data.epub.bookThumb = $filter("coverPath")(E[0].getAttribute("href"))), chromeApp && stg.data.epub.bookThumb && (stg.data.chromeAssets[stg.data.epub.bookThumb] ? stg.data.epub.bookThumb = stg.data.chromeAssets[stg.data.epub.bookThumb] : getImgDataSrc(stg.data.epub.bookThumb, function (e) {
                        stg.data.chromeAssets[stg.data.epub.bookThumb] = e, stg.data.epub.bookThumb = e
                    }));
                    try {
                        stg.data.tocPath = getElementsByAttribute(stg.data.epub.manifest, "item", "properties", "[s]*nav[s]*")[0].getAttribute("href")
                    } catch (e) {
                        var T = stg.data.epub.spine.getAttribute("toc");
                        if (!T) return void n();
                        try {
                            stg.data.book.version = 2, stg.data.tocPath = getElementsByAttribute(stg.data.epub.manifest, "item", "id", T)[0].getAttribute("href")
                        } catch (e) {
                            return void n()
                        }
                    }
                    $http.get(cacheBust(ph.join(bookPath, stg.data.packageURL, stg.data.tocPath))).success(function (e) {
                        if (config.kotobee.encodekey && (e = config.v >= 1.1 ? CryptoJS.AES.decrypt(e, config.kotobee.encodekey).toString(CryptoJS.enc.Utf8) : kInteractive.XORCipher().decode(config.kotobee.encodekey, e)), 2 == stg.data.book.version) {
                            var t = (o = parser.parseFromString(e, "text/xml")).getElementsByTagName("navMap");
                            t.length && (stg.data.toc = I(t[0]))
                        } else
                            for (var o = parser.parseFromString(e, "text/html"), r = o.getElementsByTagName("ol"), i = 0, a = 0; a < r.length; a++) {
                                var s = r[a].getElementsByTagName("a");
                                if (!(s.length < i)) {
                                    i = s.length, stg.data.toc = r[a];
                                    for (var l = s.length; l--;) s[l].getAttribute("href") ? s[l].setAttribute("href", ph.join(stg.data.tocPath, s[l].getAttribute("href"))) : s[l].setAttribute("href", "")
                                }
                            }
                        n()
                    }), tinCan.send({
                        verb: "opened",
                        activity: "Book: " + stg.data.book.meta.dc.title
                    })
                }).error(function () {
                    error.update({
                        error: "networkError",
                        ref: s
                    })
                })
            })
        }).error(function (e) {
            status.update(), native || desktop || "file://" != window.location.origin ? error.update({
                error: "errorAndCallback",
                msg: "cantReadConfigFile",
                cb: crdv.exit
            }) : error.update({
                error: "This browser failed to run the ebook app locally. Please try a different browser, or upload the web files to your server and run the web app online"
            })
        }), l.promise
    };

    E.getTitleByIndex = function (e) {
        if (stg.data.toc) try {
            for (var t = stg.data.toc.getElementsByTagName("li"), n = [], o = 0; o < t.length; o++)
                if (t[o]) {
                    var r = t[o].getElementsByTagName("a");
                    r.length && "true" != r[0].getAttribute("data-dontshow") && n.push(t[o])
                }
            return n[e].getElementsByTagName("a")[0].textContent
        } catch (e) { }
    };

    return E;
}]);

app.factory("bookinfo", ["$timeout", "auth", "$rootScope", "crdv", "np", "book", "translit", "modals", "status", "backend", "stg", function ($timeout, auth, $rootScope, crdv, np, book, translit, modals, status, backend, stg) {
    function I(e) {
        var t = {};
        return t.background = "url('" + e + "')", t["background-size"] = "cover", t["background-position"] = "center center", t.opacity = "1", t
    }

    function E() {
        A(), (native || desktop) && "download" == H && (H = "loading"), native || desktop || "open" != H || (H = "loading"), "login" == H && (R.unauthredirect && (R.redirect = stg.data.currentLibrary.unauthredirect), R.overrideunauthredirect && (R.redirect = R.unauthredirect)), L()
    }

    function T() {
        "loading" == H && (R.redirect = null, backend.hasAccessTo({
            testagainst: R.id
        }, function (e) {
            e.success ? A() : (H = "denied", R.redirect = stg.data.currentLibrary.unauthredirect, R.overrideunauthredirect && (R.redirect = R.unauthredirect)), L(), $rootScope.$$phase || $rootScope.$apply()
        }))
    }

    function O(e, t, n, o) {
        native ? crdv.writeEbook(e, t, n, o) : desktop && np.writeEbook(e, t, n, o)
    }

    function S(e) {
        return stg.data.info || (stg.data.info = {}), stg.data.info["book" + e.id] || (stg.data.info["book" + e.id] = {}), stg.data.info["book" + e.id]
    }

    function A() {
        R && (native || desktop ? H = R.exists ? "open" : S(R).loading ? "downloading" : S(R).extracting ? "extracting" : backend.cloudParam("public") ? "download" : stg.data.user.loggedIn ? "download" : backend.cloudParam("entry") ? "login" : "download" : backend.cloudParam("public") ? H = "open" : stg.data.user.loggedIn ? H = "open" : backend.cloudParam("entry") && (H = "login"))
    }

    function L() {
        D && D.ui && (D.ui.actionBtnIcon = N(), D.ui.actionBtnSpinnerIcon = M(), D.ui.actionBtnLabel = P(), D.ui.actionBtnStyle = B())
    }

    function B() {
        return "open" == H ? "openBtn" : "login" == H ? "openBtn" : "downloading" == H ? "downloadingBtn" : "extracting" == H ? "downloadingBtn" : "loading" == H ? "loadingBtn" : "denied" == H ? "deniedBtn" : "downloadBtn"
    }

    function N() {
        return "open" == H ? "ion-eye size18" : "login" == H ? "ion-person" : "download" == H ? "ion-code-download" : "downloading" == H ? null : "loading" == H ? null : void 0
    }

    function M() {
        return "downloading" == H ? "dots" : "loading" == H ? "dots" : void 0
    }

    function P() {
        return "open" == H ? "openBook" : "login" == H ? "loginToRead" : "download" == H ? "download" : "downloading" == H ? "downloading" : "extracting" == H ? "extracting" : "loading" == H ? "pleaseWait" : "denied" == H ? "bookAccessDenied" : void 0
    }

    function $(e, t) {
        native ? crdv.deleteEbook(e, t) : desktop && np.deleteEbook(e, t)
    }

    var H, R, D, F, z = {};

    z.redeemCode = function () {
        modals.hide(), auth.redeemCode({
            source: "bookinfo"
        }, function () {
            z.show(F)
        })
    };

    z.show = function (e, t) {
        t || (t = {}), F = e, R = e.book, native && crdv.vibrate(), e.book.coverImg = book.getCoverImgPath(e.book), e.book.coverStyle = e.book.bannerStyle = {
            "background-image": "url('" + e.book.coverImg + "')"
        }, e.book.bgimg && (e.book.bannerStyle = I(book.getBgImgPath(e.book))), chromeApp && (stg.data.chromeAssets[e.book.coverImg] ? e.book.coverStyle = e.book.bannerStyle = {
            "background-image": "url('" + stg.data.chromeAssets[e.book.coverImg] + "')"
        } : getImgDataSrc(e.book.coverImg, function (t) {
            stg.data.chromeAssets[e.book.coverImg] = t, e.book.coverStyle = {
                "background-image": "url('" + t + "')"
            }, e.book.bgimg || (e.book.bannerStyle = e.book.coverStyle)
        }), e.book.bgimg && (stg.data.chromeAssets[e.book.bgImg] ? e.book.bannerStyle = I(stg.data.chromeAssets[e.book.bgImg]) : getImgDataSrc(book.getBgImgPath(e.book), function (t) {
            stg.data.chromeAssets[e.book.bgImg] = t, e.book.bannerStyle = I(t)
        })));
        var n = {};
        e.book.meta || (e.book.meta = {});
        for (var o in e.book.meta) n[o] = e.book.meta[o];
        e.book.meta.dc = n;
        var r = {};
        r.book = e.book, r.categoryClicked = e.categoryClicked, r.ui = {}, D = r;
        var a = "slide-in-up";
        "none" == t.animation && (a = "none"), modals.show({
            mode: "bookInfo",
            dynamic: !0,
            animation: a,
            bookInfoOptions: r
        }), E(), setTimeout(T, mobile ? 1e3 : 500)
    };

    z.download = function (e) {
        function t(t) {
            S(e).progress = Math.ceil(100 * t), $rootScope.$$phase || $rootScope.$apply()
        }
        S(e).loading || (crdv.vibrate(), crdv.toast(translit.get("bookIsDownloading")), S(e).loading = !0, A(), L(), S(e).progress = 0, setTimeout(function () {
            backend.downloadEbook(e, t, function (t) {
                S(e).loading = null, t && (S(e).extracting = !0, A(), L(), $timeout(function () {
                    O("src.zip", e, t, function (t) {
                        S(e).extracting = null, A(), L(), modals.hide(), $timeout(function () {
                            modals.show({
                                mode: "bookDownloaded",
                                downloadedOptions: {
                                    book: e,
                                    open: book.open
                                },
                                apply: !0
                            }), crdv.applyExistingBooks(stg.data.currentLibrary.books), backend.addBookStat({
                                type: "download"
                            })
                        }, 1e3)
                    })
                }))
            })
        }, 500))
    };

    z.action = function (e) {
        "loading" != H && "denied" != H && ("open" == H ? book.open(e) : "login" == H ? (modals.hide(), setTimeout(backend.login, 1e3)) : "download" == H && z.download(e), A(), L())
    };

    z.delete = function (e) {
        crdv.toast(translit.get("deletingBook")), crdv.vibrate(), $(e, function () {
            A(), L(), $rootScope.$$phase || $rootScope.$apply()
        })
    };

    z.toggleFav = function (e) {
        e.favorite ? z.removeFav(e, !0) : z.addToFav(e, !0)
    };

    z.addToFav = function (e, t) {
        t ? e.favorite = !0 : status.update(translit.get("addingToFavorites"), null, {
            dots: !0
        }), backend.addtoFav(e, function () {
            e.favorite = !0, t || status.update()
        })
    };

    z.removeFav = function (e, t) {
        t ? e.favorite = !1 : status.update(translit.get("removingFromFavorites"), null, {
            dots: !0
        }), backend.removeFav(e, function () {
            e.favorite = !1, t || status.update(), angular.element(document.getElementById("library")).scope().loadFavorites()
        })
    };
    return z;
}]);

app.factory("cache", function () {
    var e = {},
        t = {};
    return e.initialize = function () {
        e.setOb("body", document.getElementsByTagName("body")[0]);
        var t = document.getElementsByTagName("title"),
            n = null;
        t.length ? n = t[0] : (n = document.createElement("title"), document.head.appendChild(n)), e.setOb("title", n)
    }, e.setOb = function (e, n) {
        return t[e] = n, n
    }, e.getOb = function (e) {
        return t[e]
    }, e
});

app.factory("chapterAPI", ["chapters", "stg", "$ionicSideMenuDelegate", "nav", "notes", "scorm", "selection", "$ionicScrollDelegate", "$rootScope", "$location", "backend", "tinCan", "settings", function (chapters, stg, $ionicSideMenuDelegate, nav, notes, scorm, selection, $ionicScrollDelegate, $rootScope, $location, backend, tinCan, settings) {
    function f() {
        if (!(arguments.length < 2)) {
            var e, t = arguments[0],
                n = {};
            2 == arguments.length ? e = arguments[1] : (n = arguments[1], e = arguments[2]), n.unique && m(t);
            var o = {
                event: t,
                cb: e
            };
            J.push(o)
        }
    }

    function g(e, t) {
        if (e) {
            var n = h(e);
            n && n.cb && n.cb.apply(this, [t])
        }
    }

    function h(e) {
        if (e)
            for (var t = J.length; t--;)
                if (J[t].event == e) return J[t]
    }

    function m(e) {
        for (var t = J.length; t--;) e && J[t].event != e || J.splice(t, 1)
    }

    function v() {
        var e = document.getElementById("tabMenu");
        if (e) return angular.element(e).scope()
    }

    function b() {
        var e = v();
        e && e.notebookClicked()
    }

    function y() {
        var e = v();
        e && e.pdfClicked()
    }

    function k() {
        var e = v();
        e && e.printClicked()
    }

    function w() {
        var e = v();
        e && e.settingsClicked()
    }

    function C() {
        var e = v();
        e && e.searchClicked()
    }

    function x() {
        var e = v();
        e && e.mediaClicked()
    }

    function I() {
        var e = v();
        e && e.chaptersClicked()
    }

    function E() {
        $ionicSideMenuDelegate.toggleRight(!1), $ionicSideMenuDelegate.toggleLeft(!1), nav.fullscreen()
    }

    function T(e) {
        stg.data.settings.language = e, settings.langChanged()
    }

    function O() {
        chapters.nextChapter()
    }

    function S() {
        chapters.prevChapter()
    }

    function A(t) {
        chapters.loadChapterByIndex(t)
    }

    function L(t) {
        $location.search("vi"), chapters.applyChapterStateFromURL(t, {
            vIndex: 0
        }), $rootScope.$$phase || $rootScope.$apply()
    }

    function B(t, n, o) {
        chapters.preloadChapterFromApi(t, n, o)
    }

    function N() {
        chapters.clearCache()
    }

    function M(t) {
        chapters.neverCache(t)
    }

    function P() {
        var e, t = $ionicScrollDelegate.$getByHandle("content");
        if (t && (e = t.getScrollPosition()), 1 == e.zoom) {
            var n = document.getElementById("epubContainer");
            if (hasClass(n.children[0], "scroll")) {
                var o = n.children[0].style.transform.replace(/.*?scale\((.*?)\).*?/g, "$1");
                o && (e.zoom = Number(o))
            }
        }
        return e
    }

    function $(e) {
        var t = $ionicScrollDelegate.$getByHandle("content");
        t && t.scrollTo(0, 0, e)
    }

    function H(e, t) {
        var n = $ionicScrollDelegate.$getByHandle("content");
        n && n.scrollTo(0, e, t)
    }

    function R(e, t) {
        t || (t = {});
        var n = clone(t);
        n.content = e, kInteractive.alert(n)
    }

    function D(e, t) {
        var n = {};
        n.content = e, isFunction(t) ? (n.okBtn = "Yes", n.noBtn = "No", n.cb = t) : (t.okBtn && (n.okBtn = t.okBtn), t.noBtn && (n.noBtn = t.noBtn), n.cb = t.cb, n.noBackdrop = t.noBackdrop), kInteractive.confirm(n)
    }

    function F(e, t) {
        backend.externalLink(e, t)
    }

    function z(n, o) {
        var i = selection.convertElemsToLocation([o]),
            s = {};
        s.chapter = chapters.getElemChapterIndex(o), s.location = i.left ? i.left : i.right, s.type = "content", s.src = o.textContent, s.note = n, stg.addNote(s, function () {
            notes.addIcon(s)
        })
    }

    function j(e) {
        e || (e = {}), e.id = kInteractive.getScormId(e.id), scorm.setInteractions([e])
    }

    function U(e) {
        e || (e = {}), tinCan.send(e)
    }

    function V(e, n) {
        if (!stg.data.user.loggedIn) throw "User must be logged in.";
        if (!e.label) throw "'label' variable required.";
        if (!e.data) throw "'data' variable required.";
        backend.setUserData(e, n)
    }

    function q(e, n) {
        if (!stg.data.user.loggedIn) throw "User must be logged in.";
        if (!e.label && !e.category && !e.subcategory) throw "No variable passed to use as a keyword.";
        backend.getUserData(e, n)
    }

    function W(e, n) {
        if (!stg.data.user.loggedIn) throw "User must be logged in.";
        backend.getUserList(e, n)
    }

    function _(e, n) {
        if (!stg.data.user.loggedIn) throw "User must be logged in.";
        backend.getUserDataArray({
            array: JSON.stringify(e)
        }, n)
    }

    function K(e, n) {
        if (!stg.data.user.loggedIn) throw "User must be logged in.";
        if (!e.label) throw "'label' variable required.";
        backend.deleteUserData(e, n)
    }
    var Y = {},
        J = [];
    Y.initialize = function () {
        kotobee.addEventListener = f,
            kotobee.dispatchEvent = g,
            kotobee.clearListeners = m,
            kotobee.openNotebook = b,
            kotobee.exportPDF = y,
            kotobee.print = k,
            kotobee.openSettings = w,
            kotobee.openSearch = C,
            kotobee.openMedia = x,
            kotobee.openChapters = I,
            kotobee.toggleFullscreen = E,
            kotobee.setLanguage = T,
            kotobee.nextChapter = O,
            kotobee.prevChapter = S,
            kotobee.gotoChapterIndex = A,
            kotobee.gotoChapterHref = L,
            kotobee.getChapter = B,
            kotobee.clearCache = N,
            kotobee.neverCache = M,
            kotobee.getScrollPosition = P,
            kotobee.scrollToTop = $,
            kotobee.scrollTo = H,
            kotobee.popup = R,
            kotobee.confirm = D,
            kotobee.openLink = F,
            kotobee.addToNotebook = z,
            kotobee.sendScorm = j,
            kotobee.sendTinCan = U,
            kotobee.setData = V,
            kotobee.getData = q,
            kotobee.getDataArray = _,
            kotobee.getUsers = W,
            kotobee.deleteData = K,
            Object.defineProperty(kotobee, "user", {
                get: function () {
                    var e = clone(stg.data.user);
                    return delete e.pwd, delete e.rememberMe, delete e.signatures, e
                },
                set: function (e) {
                    return null
                }
            }), Object.defineProperty(kotobee, "book", {
                get: function () {
                    return stg.data.book
                },
                set: function (e) {
                    return null
                }
            }), Object.defineProperty(kotobee, "epub", {
                get: function () {
                    return stg.data.epub
                },
                set: function (e) {
                    return null
                }
            }), Object.defineProperty(kotobee, "currentPage", {
                get: function () {
                    return stg.data.currentPageOb
                },
                set: function (e) {
                    return null
                }
            }), Object.defineProperty(kotobee, "currentChapter", {
                get: function () {
                    return stg.data.currentPageOb
                },
                set: function (e) {
                    return null
                }
            }), Object.defineProperty(kotobee, "documentation", {
                get: function () {
                    function e() {
                        n("")
                    }

                    function t(e) {
                        setTimeout(console.table.bind(console, e))
                    }

                    function n(e, t) {
                        setTimeout(console.log.bind(console, e, t || ""))
                    }
                    var o = [];
                    o.push({
                        method: "openNotebook()",
                        desc: "Opens the reader's notebook panel."
                    }), o.push({
                        method: "exportPDF()",
                        desc: "Exports the current chapter as a PDF file."
                    }), o.push({
                        method: "print()",
                        desc: "Prints the current chapter."
                    }), o.push({
                        method: "openSettings()",
                        desc: "Opens the reader's settings panel."
                    }), o.push({
                        method: "openSearch()",
                        desc: "Opens the reader's search panel."
                    }), o.push({
                        method: "openMedia()",
                        desc: "Opens the reader's media panel."
                    }), o.push({
                        method: "openChapters()",
                        desc: "Opens the reader's chapters panel (table of contents)."
                    }), o.push({
                        method: "toggleFullscreen()",
                        desc: "Toggle reader into fullscreen mode."
                    }), o.push({
                        method: "setLanguage(lang)",
                        desc: "Set the reader language. This method takes the language's 2 character abbreviation as input. Possible values: en,fr,es,pt,nl,de,hu,it,sw,ms,no,pl,ro,ru,tr,ar,zh,jp,ko."
                    });
                    var r = [];
                    r.push({
                        method: "nextChapter()",
                        desc: "Jump to the next chapter, relative to the current chapter."
                    }), r.push({
                        method: "prevChapter()",
                        desc: "Jump to the previous chapter, relative to the current chapter."
                    }), r.push({
                        method: "gotoChapterIndex(index)",
                        desc: "Jump to a certain chapter, passing its index as input."
                    }), r.push({
                        method: "gotoChapterHref(url)",
                        desc: "Jump to a certain chapter, passing its relative URL as input."
                    }), r.push({
                        method: "getChapter(url, callback, errorCallback)",
                        desc: "Retrieve the chapter object, passing its relative URL as input. This method also takes 2 more arguments: a success callback with the chapter object, and an error callback."
                    }), r.push({
                        method: "clearCache()",
                        desc: "Clear any cached chapter content."
                    }), r.push({
                        method: "neverCache(chapterOb)",
                        desc: "Set the reader never to cache a specific chapter. This will cause chapters to be continuously loaded and never stored in memory. The chapter object is passed an input."
                    }), r.push({
                        method: "getScrollPosition()",
                        desc: "Get the current chapter's scroll position."
                    }), r.push({
                        method: "scrollToTop(animate)",
                        desc: "Scroll to the top of the chapter. This method also takes a second argument, to indicate whether scroll animation should be used."
                    }), r.push({
                        method: "scrollTo(y, animate)",
                        desc: "Scroll to a certain y location. The y location is provided as an input. This method also takes a second argument, to indicate whether scroll animation should be used."
                    });
                    var i = [];
                    i.push({
                        method: "popup(html, options)",
                        desc: "Display certain HTML content inside the reader's popup. Takes an optional object with the following properties:",
                        args: [{
                            Property: "title",
                            Description: "The text appearing in the title bar of the popup."
                        }, {
                            Property: "okBtn",
                            Description: "The label of the default OK button at the footer of the popup."
                        }, {
                            Property: "cb",
                            Description: "A callback function, called when the user clicks on OK."
                        }, {
                            Property: "raw",
                            Description: "Display only the passed HTML (raw) as a popup, without a frame and OK button. The above properties won't be effective if raw is set to true."
                        }, {
                            Property: "noBackdrop",
                            Description: "Don't display a backdrop when the popup is opened."
                        }]
                    }), i.push({
                        method: "confirm(html,callback)",
                        desc: "Display certain content inside a confirmation box. The second argument represents the callback function in case the user confirms.",
                        args: [{
                            Property: "okBtn",
                            Description: "The label of the 'Yes' button at the footer of the popup. Defaults to 'Yes'"
                        }, {
                            Property: "noBtn",
                            Description: "The label of the 'No' button at the footer of the popup. Defaults to 'No'"
                        }, {
                            Property: "cb",
                            Description: "A callback function, called when the user clicks on the 'Yes' button."
                        }, {
                            Property: "noBackdrop",
                            Description: "Don't display a backdrop when the popup is opened."
                        }]
                    }), i.push({
                        method: "openLink(url)",
                        desc: "Open a certain URL inside a sliding panel."
                    }), i.push({
                        method: "addToNotebook(note, element)",
                        desc: "Add certain content to the notebook. The note is provided as the first argument, and the element to attach the note to is provided as the second argument."
                    });
                    var a = [];
                    a.push({
                        method: "sendScorm(interactionOb)",
                        desc: "Send a certain scorm interaction object to the hosting LMS."
                    }), a.push({
                        method: "sendTinCan(activityOb)",
                        desc: "Send a Tin Can activity to the LRS configured in the ebook/library settings. Available for cloud ebooks and libraries only."
                    });
                    var s = [];
                    s.push({
                        method: "setData(dataOb, callback)",
                        desc: "Write data into the database for the currently logged in user. You will be able to fully label that data, for easy retrieval later. This methods takes an object with the following options:",
                        args: [{
                            Property: "label",
                            Description: "The label (key) representing this slot of data (required)."
                        }, {
                            Property: "data",
                            Description: "The data to write (required)."
                        }, {
                            Property: "overwrite",
                            Description: "In case this data exists, overwrite it"
                        }, {
                            Property: "category",
                            Description: "For better organization, set a category for this data. This will make queries more efficient."
                        }, {
                            Property: "subcategory",
                            Description: "For further organization, set a subcategory for this data. This will make queries more efficient."
                        }, {
                            Property: "options",
                            Description: "Set options for this data. This is treated as normal text, and can be used when querying for data."
                        }]
                    }), s.push({
                        method: "getData(queryOb, callback)",
                        desc: "Retrieve data for the currently logged in user. It takes an object with the following options, and triggers a callback (the second argument) passing back the array of data.",
                        args: [{
                            Property: "label",
                            Description: "The label (key) representing this slot of data."
                        }, {
                            Property: "category",
                            Description: "The category assigned to this data, to use as a search parameter."
                        }, {
                            Property: "subcategory",
                            Description: "The subcategory assigned to this data, to use as a search parameter."
                        }, {
                            Property: "options",
                            Description: "The options for this data, to use as a search parameter."
                        }, {
                            Property: "allUsers",
                            Description: "Expand the scope of search to all users with access to this cloud ebook or library. To use this option, the user must have global data permissions."
                        }, {
                            Property: "role",
                            Description: "Search by user role."
                        }]
                    }), s.push({
                        method: "getDataArray(array, callback)",
                        desc: "Send a collection of search queries for the currently logged in user, all in a single request. It takes an array of objects specified in the getData method, in addition to an 'id' object to label the results returned in an array."
                    }), s.push({
                        method: "getUsers",
                        desc: "For logged in users with global data permissions, get an array of all users added to this cloud ebook/library.",
                        args: [{
                            Property: "role",
                            Description: "Limit the results to a specific user role."
                        }]
                    }), s.push({
                        method: "deleteData(queryOb, callback)",
                        desc: "Delete a certain data slot for the currently logged in user. It takes an object with the following options:",
                        args: [{
                            Property: "label",
                            Description: "The label (key) representing this slot of data (required)."
                        }, {
                            Property: "category",
                            Description: "The category assigned to this data, to use as a search parameter."
                        }, {
                            Property: "subcategory",
                            Description: "The subcategory assigned to this data, to use as a search parameter."
                        }, {
                            Property: "options",
                            Description: "Set options for this data. This is treated as normal text, and can be used when querying for data."
                        }, {
                            Property: "all",
                            Description: "If this is true, all matching results will be deleted. It not, only the first result is deleted."
                        }]
                    });
                    var l = [];
                    l.push({
                        prop: "epub",
                        desc: "The parsed epub object, showing the metadata, manifest, spine, and book thumbnail."
                    }), l.push({
                        prop: "book",
                        desc: "The book object obtained from the epub, by the reader."
                    }), l.push({
                        prop: "currentChapter",
                        desc: "The current chapter object, containing all its parsed properties."
                    }), l.push({
                        prop: "currentPage",
                        desc: "In the case that a reflowable layout is paginated (displayed without a scrollbar, as single page or double page view), the currently displayed page(s) is(are) returned by this property."
                    }), l.push({
                        prop: "user",
                        desc: "The currently logged in user. Return an object containing his id, name, email, and user role (if exists)."
                    });
                    var c = [];
                    c.push({
                        event: "addEventListener(event, [options], callback)",
                        desc: "Listen to a certain event (string) and call a function once the event is fired. This function takes an optional second argument (options) with the following properties:",
                        args: [{
                            Property: "unique",
                            Description: "If an event listener already exists for this event, it will be removed, before adding the new one."
                        }]
                    }), c.push({
                        event: "dispatchEvent(event)",
                        desc: "Fire a certain event. The function takes the event name (as a string)."
                    }), c.push({
                        event: "clearListeners(event)",
                        desc: "Clear all listeners that are listening to a certain event (as a string). If no event is passed, then all listeners attached to the kotobee object are cleared."
                    });
                    var d = [];
                    d.push({
                        event: "ready",
                        desc: "This event is fired from the document object, whenever the ebook or library initialization is complete. It is fired just once."
                    }), d.push({
                        event: "chapterLoaded",
                        desc: "This event is fired whenever a new chapter is loaded and rendered (ready for use)."
                    }), d.push({
                        event: "scrolled",
                        desc: "This event is fired whenever a chapter is scrolled. An object is passed back containing the scroll position."
                    });
                    var u = "color: #000; font-weight: bold; font-size:18px",
                        p = "color: #224e97; font-size:12px; font-weight:bold",
                        f = "color: #224e97; font-size:12px; font-weight:bold",
                        g = "margin-left:6px;",
                        h = "font-weight:bold;";
                    setTimeout(console.clear.bind(console)), n("%cKotobee Reader API", "color: #000; font-weight: bold; font-size:22px;border:solid 1px #000;border-radius:8px;padding:5px 10px;"), n("%cThis documentation lists brief explanations of the different Kotobee Reader API methods, properties, and events, used to interact with the reader and build scalable ebook applications. A backend Developer API is also available for remotely controlling different aspects of your cloud ebooks and libraries, here: http://support.kotobee.com/en/support/solutions/folders/8000082574."), e(), n("%cUI Methods", u), n("%cThese methods are used to interact with the reader's different UI elements and components.", h);
                    for (m = 0; m < o.length; m++) n("%c" + o[m].method, p), n("%c" + o[m].desc, g);
                    e(), n("%cContent Methods", u), n("%cThese methods are used to display/set content in different ways.", h);
                    for (m = 0; m < i.length; m++) n("%c" + i[m].method, p), n("%c" + i[m].desc, g), i[m].args && t(i[m].args);
                    e(), n("%cNavigation Methods", u), n("%cThese methods are used mainly to navigate to different chapters efficiently.", h);
                    for (m = 0; m < r.length; m++) n("%c" + r[m].method, p), n("%c" + r[m].desc, g);
                    e(), n("%cSCORM / Tin Can Methods", u), n("%cThese methods are used to send progress to an LMS or LRS.", h);
                    for (m = 0; m < a.length; m++) n("%c" + a[m].method, p), n("%c" + a[m].desc, g);
                    e(), n("%cCloud Methods", u), n("%cThese methods are used to store and retrieve user data into the database, for that cloud ebook or library. These methods are available for logged in users only.", h);
                    for (m = 0; m < s.length; m++) n("%c" + s[m].method, p), n("%c" + s[m].desc, g), s[m].args && t(s[m].args);
                    e(), n("%cProperties", u), n("%cThese properties are used to describe the book and user.", h);
                    for (m = 0; m < l.length; m++) n("%c" + l[m].prop, "color: #224e97; font-size:12px; font-weight:bold"), n("%c" + l[m].desc, g);
                    e(), n("%cEvents Methods", u), n("%cThese methods are used to listen to and manage events fired by kotobee.", h);
                    for (m = 0; m < c.length; m++) n("%c" + c[m].event, f), n("%c" + c[m].desc, g), c[m].args && t(c[m].args);
                    n("%cEvent types", h);
                    for (var m = 0; m < d.length; m++) n("%c" + d[m].event, f), n("%c" + d[m].desc, g);
                    return null
                },
                set: function (e) {
                    return null
                }
            })
    };

    return Y;
}]);

app.factory("chapters", ["$http", "tinCan", "backend", "context", "error", "crdv", "skeleton", "$stateParams", "sync", "$ionicSideMenuDelegate", "scorm", "cache", "modals", "status", "$injector", "virt", "translit", "selection", "$ionicScrollDelegate", "$filter", "$rootScope", "cssParser", "jsParser", "$location", "$q", "stg", function ($http, tinCan, backend, context, error, crdv, skeleton, $stateParams, sync, $ionicSideMenuDelegate, scorm, cache, modals, status, $injector, virt, translit, selection, $ionicScrollDelegate, $filter, $rootScope, cssParser, jsParser, $location, $q, stg) {
    function B(e) {
        for (var t = 0; t < ie.length; t++)
            if (ie[t] == e) return !0
    }

    function N(e, t) {
        var n = {
            leftOb: {}
        };
        ne.getPageOb({
            page: e.index,
            source: "getNextIndex"
        }, function (o) {
            if (!o) return t();
            o.pages ? e.vIndex < o.pages.length - 1 ? (n.leftOb.index = e.index, n.leftOb.vIndex = e.vIndex + 1) : (n.leftOb.index = e.index + 1, n.leftOb.vIndex = 0) : n.leftOb.index = e.index + 1, t(n)
        })
    }

    function M(e, t) {
        var n = {
            leftOb: {}
        };
        if (ne.shouldBePaged(e)) {
            if (!e.vIndex || 0 == e.vIndex) return void ne.getPageOb({
                page: e.index - 1,
                source: "stepBack"
            }, function (o) {
                n.leftOb.index = e.index - 1, o.pages && o.pages.length && (n.leftOb.vIndex = o.pages.length - 1), t(n)
            });
            n.leftOb.index = e.index, n.leftOb.vIndex = e.vIndex - 1
        } else n.leftOb.index = e.index - 1;
        t(n)
    }

    function P(e) {
        var t = stg.data.epub.spine.getElementsByTagName("itemref")[e];
        if (t) return "no" == t.getAttribute("linear")
    }

    function $(e) {
        if (!(e < 0 || e >= stg.data.epub.spine.getElementsByTagName("itemref").length - 1)) {
            var t = stg.data.epub.spine.getElementsByTagName("itemref")[e];
            if (t) {
                var n = t.getAttribute("properties");
                if (n)
                    for (var o = n.split(" "), r = 0; r < o.length; r++)
                        if (o[r].indexOf("-spread-") > 0) return o[r].split("-spread-")[1]
            }
        }
    }

    function H(e, t, n) {
        var o = ne.cacheLookup(e);
        o ? n(o) : ne.preloadChapter(e, t, n)
    }

    function R() {
        "undefined" != typeof kotobeeReady && kotobeeReady(), void 0 !== kotobee && kotobee.ready && kotobee.ready()
    }

    function D(e) {
        for (var t = 0; t < oe.length; t++) oe[t](e);
        "renderedChapter" == e && document.dispatchEvent(new Event("kotobeeChapterLoaded")), kotobee && kotobee.dispatchEvent("chapterLoaded")
    }

    function F() {
        skeleton.isFirstChapter({
            first: Z
        }, function (e) {
            skeleton.isLastChapter({
                last: K
            }, function (t) {
                stg.data.book.chapter.notLastChapter = !t, stg.data.book.chapter.notFirstChapter = !e, $rootScope.$$phase || $rootScope.$apply()
            })
        })
    }

    function z() {
        timeOut(function () {
            status.update(null)
        }, 1e3)
    }

    function j(t, n, o) {
        $http.get(cacheBust(t)).success(function (e) {
            config.kotobee.encodekey && (e = config.v >= 1.1 ? CryptoJS.AES.decrypt(e, config.kotobee.encodekey).toString(CryptoJS.enc.Utf8) : kInteractive.XORCipher().decode(config.kotobee.encodekey, e)), e = (e = e.replace(/<br\s*?\/>/g, "<br>")).replace(/(<)([^ >]+)([^>]*?)(\/>)/g, "$1$2$3></$2>"), n && n(e)
        }).error(function () {
            o && o()
        })
    }

    function U(e, t) {
        e || (e = {});
        var n = e.first,
            o = e.page,
            r = e.vIndex;
        null == o && (o = currentIndex), null == r && (r = currentVIndex), t("single" == ne.getCurrentView(o, {
            vIndex: r
        }).view ? ne.shouldBePaged(e.page ? e : stg.data.currentPageOb) ? o <= n ? 0 == currentVIndex : !1 : o <= n : stg.data.currentPageOb.pages ? o <= n ? 0 == currentVIndex : !1 : o <= n)
    }

    function V(e, t) {
        function n(e) {
            return t && t(e), e
        }
        e || (e = {});
        var o = e.max,
            r = e.page,
            i = e.vIndex,
            a = e.view;
        return null == r && (r = currentIndex, i = currentVIndex, a = ne.getCurrentView(r, {
            returnType: "view"
        })), "single" == a ? (null == e.index && (e.index = e.page), ne.shouldBePaged(null != e.page ? e : stg.data.currentPageOb), n(a ? r >= o - 1 : r >= o - 1 ? i >= Y - 1 : !1)) : n(r >= o - 1 ? Y ? i >= Y - 2 : !0 : r >= o - 2 ? Y && i >= J - 1 ? Y <= 1 : !0 : !1)
    }

    function q() {
        var e = ph.removeHash(window.location.href),
            t = document.getElementsByTagName("base");
        if (t.length)
            for (var n = 0; n < t.length; n++)
                if (t[n].getAttribute("href")) {
                    e = window.location.origin + "/" + t[n].getAttribute("href");
                    break
                }
        return isAbsoluteURLPath(stg.data.book.chapter.absoluteURL) ? stg.data.book.chapter.absoluteURL : "/" == stg.data.book.chapter.absoluteURL[0] ? getDomainFromURL(e) + stg.data.book.chapter.absoluteURL : ph.join(e, stg.data.book.chapter.absoluteURL)
    }

    function W() {
        return "library" == config.kotobee.mode ? stg.data.paths.private + "books/" + stg.data.book.chapter.absoluteURL.split("/books/")[1] : stg.data.paths.private + stg.data.book.chapter.absoluteURL.substr(1)
    }

    var _, K, Y, J, Z, G, X, Q, ee, te, ne = {},
        oe = [],
        re = [],
        ie = [];

    ne.busy = !0, ne.initialize = function () {
        if (document.getElementById("chaptersInner")) {
            config.firstDoublePage || (config.firstDoublePage = 0), null == stg.data.settings.pageScroll && (stg.data.settings.pageScroll = !0), context.initialize(), currentIndex = null, stg.data.currentPageOb = null, _ = stg.data.epub.spine.getElementsByTagName("itemref").length, K = _;
            for (var e = stg.data.epub.spine.getElementsByTagName("itemref"), t = e.length; t--;)
                if (!e[t].hasAttribute("linear") || "no" != e[t].getAttribute("linear")) {
                    K = t + 1;
                    break
                }
            Z = 0;
            for (t = 0; t < e.length; t++)
                if (!e[t].hasAttribute("linear") || "no" != e[t].getAttribute("linear")) {
                    Z = t;
                    break
                }
            var r = stg.data.book.chapters ? stg.data.book.chapters : '<h5 class="panelEmpty"><i class="icon ion-alert-circled"></i> ' + translit.get("noTOC") + "</h5>";
            replaceHTML(document.getElementById("chaptersInner"), r), cache.setOb("title", replaceHTML(cache.getOb("title"), stg.data.book.meta.dc.title)), backend.addBookStat({
                type: "view"
            }), G || (translit.addListener(function () {
                var e = document.getElementById("chaptersInner");
                e && replaceHTML(e, stg.data.book.chapters ? stg.data.book.chapters : '<h5 class="panelEmpty"><i class="icon ion-alert-circled"></i> ' + translit.get("noTOC") + "</h5>")
            }), G = !0), $filter("bookSetup")(), ne.showLoader(), ne.resetPages(), ne.preloadEdgeChapters(function () {
                var e = 0,
                    t = 0;
                if (null != ne.startChapter && "book" == config.kotobee.mode) {
                    var n = $injector.get("book").root + "/reader/chapter/" + ne.startChapter;
                    return ne.startHash && (n += "/" + ne.startHash), $location.path(n), ne.hideLoader(), sync.trigger(), void ($rootScope.$$phase || $rootScope.$apply())
                }
                stg.readSlot("lastLoc" + stg.getBookId(), function (n) {
                    stg.readSlot("lastLocY" + stg.getBookId(), function (o) {
                        config.preserveLoc && "app" != config.kotobee.mode && (e = n ? Number(n) : 0, t = o ? Number(o) : 0), stg.data.user && null != stg.data.user.lastchap && (e = Number(stg.data.user.lastchap));
                        var r = scorm.getBookmark();
                        r && (e = Number(r));
                        var i = {};
                        t && (i.scroll = t), ne.loadChapterByIndex(e, i).then(function () {
                            ne.hideLoader(), sync.trigger()
                        })
                    })
                })
            })
        } else setTimeout(ne.initialize, 50)
    };

    ne.preloadEdgeChapters = function (e) {
        ne.getPageOb({
            page: Z,
            source: "preloadEdgeChapters"
        }, function (t) {
            ne.getPageOb({
                page: K - 1,
                source: "preloadEdgeChapters"
            }, function (t) {
                Y = null, J = null, e()
            })
        })
    };

    ne.displayChapters = function () {
        if (!$rootScope.widescreen || config.notebookTab) {
            var e = $ionicSideMenuDelegate.$getByHandle("menus");
            e._instances[0].left.width = e._instances[0].right.width = 330, e._instances[0].left.el.style.width = e._instances[0].right.el.style.width = "330px", $rootScope.widescreen && (stg.data.settings.rtl ? (e._instances[0].right.width = 480, e._instances[0].right.el.style.width = "480px") : (e._instances[0].left.width = 480, e._instances[0].left.el.style.width = "480px"), $injector.get("notes").updateNotebookArray({}, function () {
                $rootScope.$$phase || $rootScope.$apply()
            })), mobile && $injector.get("nav").resize(), stg.data.settings.rtl ? $ionicSideMenuDelegate.toggleRight() : $ionicSideMenuDelegate.toggleLeft(), native && crdv.vibrate()
        }
    };

    ne.clearCache = function () {
        re = []
    };

    ne.neverCache = function (e) {
        e && (B(e.url) || ie.push(e.url))
    };

    ne.hideLoader = function () {
        var e = document.getElementById("ebookLoaderAnim");
        e && addClass(e, "hide")
    };

    ne.showLoader = function () {
        var e = document.getElementById("ebookLoaderAnim");
        e && removeClass(e, "hide")
    };

    ne.addListener = function (e) {
        oe.push(e)
    };

    ne.removeListener = function (e) {
        for (var t = oe.length; t--;) oe[t] == e && oe.splice(t, 1)
    };

    ne.redirectToHashPage = function (e, t) {
        var n = t.leftOb;
        if (n.pages && n.pages.length) {
            var o = parser.parseFromString(n.raw ? n.raw : n.content, "text/html");
            $filter("contentVirt")("parse", o.getElementById("epubContent"), function () {
                var t = o.getElementById(e);
                if (!t) {
                    var r = o.getElementsByName(e);
                    r.length && (t = r[0])
                }
                if (t) {
                    var i = selection.convertElemsToLocation([t], o.getElementById("epubContent")).left,
                        a = selection.getPageByLocation(n, i);
                    ne.loadChapterByIndex(n.index, {
                        hash: e,
                        vIndex: a
                    })
                }
            })
        }
    };

    ne.redirect = function (e, t) {
        t || (t = {});
        var n = $injector.get("book").root + "/reader/chapter/" + e;
        t.hash && (n += "/" + t.hash);
        var o = ne.getCurrentHash();
        if (t.hash ? $location.search("vi") : $location.search({
            vi: t.vIndex
        }), $location.path(n), currentIndex == e) {
            if (!o) return ne.loadChapterByIndex(e, t);
            if (o == t.hash) return ne.loadChapterByIndex(e, t)
        }
        $rootScope.$$phase || $rootScope.$apply()
    };

    ne.getCurrentHash = function () {
        var e = $location.path(),
            t = (e = e.replace(/\?.*/g, "")).split("/" + currentIndex + "/");
        if (!(t.length < 2)) return t[1]
    };

    ne.loadLocation = function (e, t) {
        var n = $q.defer();
        t || (t = {});
        var o = !e.match(/[^0-9.]/g);
        return ne.showLoader(), ne.getPageOb({
            page: t.index
        }, function (t) {
            if (!t.pages || !t.pages.length) {
                if (o) {
                    var r = selection.getElemsByLocation(e);
                    if (!r[0]) return n.resolve();
                    virt.safeScrollToElem("content", r[0], !0)
                } else virt.safeAnchorScroll("content", e, !0);
                return ne.hideLoader(), n.resolve()
            }
            if (o) {
                var i = selection.getPageByLocation(t, e);
                return ne.loadChapterByIndex(t.index, {
                    vIndex: i
                }), n.resolve()
            }
            var a = parser.parseFromString(t.raw ? t.raw : t.content, "text/html");
            $filter("contentVirt")("parse", a.getElementById("epubContent"), function () {
                var o, r;
                if (!(o = a.getElementById(e))) {
                    var i = a.getElementsByName(e);
                    i.length && (o = i[0])
                }
                o && (r = selection.convertElemsToLocation([o], a.getElementById("epubContent")).left);
                var s = 0;
                return r && (s = selection.getPageByLocation(t, r)), ne.loadChapterByIndex(t.index, {
                    vIndex: s
                }), n.resolve()
            })
        }), n.promise
    };

    ne.indexFromUrl = function (e, t) {
        t || (t = {});
        var n, o = ph.getHash(e);
        t.hash && (o = t.hash), e = ph.removeHash(ph.normalize(e));
        for (var r = stg.data.epub.manifest.getElementsByTagName("item"), i = 0; i < r.length; i++)
            if (ph.removeHash(ph.normalize(r[i].getAttribute("href"))) == e) {
                n = r[i].getAttribute("id");
                break
            }
        r = stg.data.epub.spine.getElementsByTagName("itemref");
        var a, s = t.order ? t.order : 1;
        for (i = 0; i < r.length; i++)
            if (r[i].getAttribute("idref") == n && (a = i, r[i].getAttribute("id") == o)) {
                if (--s) continue;
                return i
            }
        return a
    };

    ne.arePagesAppearingTogether = function (e, t, n) {
        var o = ne.getCurrentView(t.index),
            r = e.vIndex;
        r < 0 && e.pages && e.pages.length && (r += e.pages.length), isNaN(r) && (r = null);
        var i = t.vIndex;
        if (i < 0 && t.pages && t.pages.length && (i += t.pages.length), isNaN(i) && (i = null), e.index == t.index && r == i) return n(!0);
        if ("double" == o.view) {
            if ("rfl" == e.layout) {
                if (e.index == t.index && r == i + 1) return n(!0);
                if (t.pages && 1 == t.pages.length && e.index == t.index + 1 && 0 == r) return n(!0)
            }
            return "fxl" == e.layout && e.index == t.index + 1 ? n(!0) : n()
        }
        return n()
    };

    ne.getNextIndexOld = function (e, t) {
        var n = {},
            o = ne.getCurrentView(e.index);
        if (n.leftOb = {
            index: e.index + 1,
            vIndex: 0
        }, e.vIndex || (e.vIndex = 0), "double" == o.view)
            if (n.double = !0, "rfl" == e.layout)
                if (e.pages.length - 1 - e.vIndex >= 2) n.leftOb.index = e.index, n.leftOb.vIndex = e.vIndex + 2;
                else {
                    if (e.pages.length - 1 - e.vIndex != 1) return void ne.getPageOb({
                        page: e.index + 1,
                        source: "getNextIndex"
                    }, function (o) {
                        1 == o.pages.length ? (n.leftOb.vIndex = 0, n.leftOb.index = e.index + 2) : (n.leftOb.vIndex = 1, n.leftOb.index = e.index + 1), t(n)
                    });
                    n.leftOb.index = e.index + 1, n.leftOb.vIndex = 0
                } else n.leftOb.index++;
        else "rfl" != e.layout || stg.data.settings.pageScroll || (e.pages.length - 1 - e.vIndex >= 1 ? (n.leftOb.index = e.index, n.leftOb.vIndex = e.vIndex + 1) : (n.leftOb.index = e.index + 1, n.leftOb.vIndex = 0));
        return t(n), n
    };

    ne.getElemChapterIndex = function (e) {
        return "single" == stg.data.currentView ? currentIndex : traverseToTop(e, "spreadL") ? currentIndex : traverseToTop(e, "spreadR") ? stg.data.currentRPageOb.index : currentIndex
    };

    ne.getNextIndex = function (e, t) {
        var n = ne.getCurrentView(e.index, {
            vIndex: e.vIndex
        });
        N(e, function (e) {
            if (!e) return t();
            "double" == n.view ? e.leftOb.index >= K - 1 ? t(e) : N(e.leftOb, t) : t(e)
        })
    };

    ne.getPrevIndexOld = function (e, t) {
        var n = {};
        n.leftOb = {
            index: e.index - 1,
            vIndex: 0
        }, e.vIndex || (e.vIndex = 0);
        var o = {};
        if ("double" == ne.getCurrentView(e.index - 1, o).view)
            if (n.double = !0, config.firstDoublePage || (config.firstDoublePage = 0), "rfl" == e.layout)
                if (e.vIndex > 1) n.leftOb.index = e.index, n.leftOb.vIndex = e.vIndex - 2, n.leftOb.index < config.firstDoublePage && n.leftOb.vIndex++;
                else {
                    if (1 != e.vIndex) return void ne.getPageOb({
                        page: e.index - 1,
                        source: "getPrevIndex"
                    }, function (o) {
                        1 == o.pages.length ? (n.leftOb.index = e.index - 2, n.leftOb.vIndex = -1, n.leftOb.index < config.firstDoublePage && (n.leftOb.index = e.index - 1, n.leftOb.vIndex = 0)) : (n.leftOb.index = e.index - 1, n.leftOb.vIndex = e.vIndex - 2, n.leftOb.index < config.firstDoublePage && (n.leftOb.vIndex = e.vIndex - 1)), t(n)
                    });
                    n.leftOb.index = e.index - 1, n.leftOb.vIndex = -1, n.leftOb.index < config.firstDoublePage && (n.leftOb.index = e.index, n.leftOb.vIndex = 0)
                } else n.leftOb.index = e.index - 2, n.leftOb.index < config.firstDoublePage && (n.leftOb.index = e.index - 1);
        else "rfl" != e.layout || stg.data.settings.pageScroll || (e.vIndex > 0 ? (n.leftOb.index = e.index, n.leftOb.vIndex = e.vIndex - 1) : (n.leftOb.index = e.index - 1, n.leftOb.vIndex = -1));
        return t(n), n
    };

    ne.getPrevIndex = function (e, t) {
        M(e, function (e) {
            "double" == ne.getCurrentView(e.leftOb.index, {
                vIndex: e.leftOb.vIndex
            }).view ? e.leftOb.index <= Z ? t(e) : M(e.leftOb, t) : t(e)
        })
    };

    ne.nextChapter = function (e) {
        skeleton.isLastChapter(null, function (e) {
            e ? crdv.toast(translit.get("inLastChapter")) : (context.interruptAnimation(), skeleton.getNextIndex({
                index: currentIndex,
                vIndex: currentVIndex
            }, function (e) {
                if (e) {
                    for (var t = stg.data.epub.spine.getElementsByTagName("itemref")[e.leftOb.index]; t && t.hasAttribute("linear") && "no" == t.getAttribute("linear");) {
                        if (++e.leftOb.index >= t.length - 1) return void crdv.toast(translit.get("inLastChapter"));
                        t = stg.data.epub.spine.getElementsByTagName("itemref")[e.leftOb.index]
                    }
                    var n = t.getAttribute("id");
                    $rootScope.renderScope.applyHeader("default"), null != e.leftOb.vIndex && (n = null), ne.redirect(e.leftOb.index, {
                        hash: n,
                        vIndex: e.leftOb.vIndex
                    }), $rootScope.$$phase || $rootScope.$apply()
                }
            }))
        })
    };

    ne.prevChapter = function (e) {
        skeleton.isFirstChapter(null, function (e) {
            e ? crdv.toast(translit.get("inFirstChapter")) : (context.interruptAnimation(), skeleton.getPrevIndex({
                index: currentIndex,
                vIndex: currentVIndex
            }, function (e) {
                e.vIndex;
                e.leftOb.index < 0 && (e.leftOb.index = 0);
                for (var t = stg.data.epub.spine.getElementsByTagName("itemref")[e.leftOb.index]; t && t.hasAttribute("linear") && "no" == t.getAttribute("linear");) {
                    if (--e.leftOb.index < 0) return e.leftOb.index = 0, void crdv.toast(translit.get("inFirstChapter"));
                    t = stg.data.epub.spine.getElementsByTagName("itemref")[e.leftOb.index]
                }
                $rootScope.renderScope.applyHeader("default");
                var n = t.getAttribute("id");
                null != e.leftOb.vIndex && (n = null), ne.redirect(e.leftOb.index, {
                    hash: n,
                    vIndex: e.leftOb.vIndex
                }), $rootScope.$$phase || $rootScope.$apply()
            }))
        })
    };

    ne.getCurrentView = function (e, t) {
        function n(e) {
            return e.view || (e.view = "single"), "view" == o ? (t.cb && t.cb(e.view), e.view) : "page" == o ? (t.cb && t.cb(e.page), e.page) : (t.cb && t.cb(e), e)
        }
        t || (t = {});
        var o = t.returnType;
        if (void 0 != e) {
            var r = skeleton.getCurrentView(e, t);
            if (r) return n(r);
            if (e < config.firstDoublePage) return n({
                view: null,
                index: e
            });
            var i = stg.data.settings.viewMode;
            return n("single" == i ? {
                view: null,
                index: e,
                scroll: stg.data.settings.pageScroll
            } : "double" == i ? config.singleViewOnPortrait && 1.3 * window.innerWidth < window.innerHeight ? {
                view: null,
                index: e
            } : skeleton.isSingle(e, t.vIndex) ? {
                view: null,
                index: e
            } : {
                        view: "double",
                        index: e
                    } : "auto" == i ? "left" == $(e) && "right" == $(e + 1) ? {
                        view: "double",
                        index: e
                    } : "right" == $(e) && "left" == $(e - 1) ? {
                        view: "double",
                        index: e - 1
                    } : {
                                view: null,
                                index: e,
                                scroll: stg.data.settings.pageScroll
                            } : {
                            view: null,
                            index: e,
                            scroll: stg.data.settings.pageScroll
                        })
        }
    };

    ne.loadChapterByIndex = function (e, t) {
        t || (t = {});
        var n = t.deferred;
        return n || (n = $q.defer()), void 0 == e && (e = currentIndex), e < 0 ? (n.resolve(), n.promise) : (e > _ && (e = 0), ne.getPageOb({
            page: e,
            source: "loadChapterByIndex"
        }, function (r) {
            ne.getSurroundingPages({
                index: r.index,
                vIndex: t.vIndex
            }, function () {
                var i = {
                    index: e,
                    vIndex: t.vIndex
                };
                if (skeleton.normalize(i), !t.hash && (i.index != e || null != i.vIndex && i.vIndex != t.vIndex)) return ne.loadChapterByIndex(i.index, {
                    hash: t.hash,
                    vIndex: i.vIndex,
                    reload: t.reload,
                    deferred: n
                }), n.promise;
                var a = ne.getChapterURL(e),
                    l = !1;
                if (0 == a.indexOf("#") && (l = !0), t.hash && r.pages && r.pages.length && (l = !0), l) return ne.loadLocation(t.hash, {
                    index: e,
                    source: "from loadChapterByIndex"
                }).then(function (e) { });
                t.leftOb = r, t.leftOb.index = e, t.leftOb.currentVIndex = t.leftOb.vIndex, t.leftOb.vIndex = t.vIndex, asyncCheck(!(r.pages && t.leftOb.vIndex >= r.pages.length - 1), ne.getPageOb, [{
                    page: e + 1,
                    source: "loadChapterByIndex"
                }], function () {
                    ne.planPageMapping(t, function (r) {
                        if (r || (r = {}), r.redirect) return ne.redirect(r.redirect.index, {
                            hash: t.hash,
                            vIndex: t.vIndex,
                            reload: t.reload,
                            deferred: n
                        }), n.promise;
                        if (t.deferred = null, ne.urlMatchesIndex(a, currentIndex) && !t.reload && currentVIndex == t.vIndex) {
                            currentIndex = e, F();
                            var i = t.hash;
                            return i || (i = ph.getHash(t.leftOb.url)), i ? ne.loadLocation(i, {
                                index: currentIndex,
                                source: "from loadChapterByIndex2"
                            }).then(function () {
                                D("renderedChapter"), ne.highlightCurrentTocChapter()
                            }) : ($ionicScrollDelegate.$getByHandle("content").scrollTop(!0), D("renderedChapter"), ne.highlightCurrentTocChapter()), stg.data.book.chapter.url = a, stg.data.book.chapter.absoluteURL = ph.join(bookPath, stg.data.packageURL, stg.data.book.chapter.url), n.resolve(), n.promise
                        }
                        context.startTransition(t, n)
                    })
                })
            })
        }), n.promise)
    };

    ne.getSurroundingPages = function (e, t) {
        if ("single" == stg.data.settings.viewMode) return t();
        if (!e) return t();
        var n = skeleton.getJoint(e.index, e.vIndex);
        if (n && n.length > 1) return ne.getPageOb({
            page: e.index - 1
        }, t);
        asyncCheck(n, ne.getPageOb, [{
            page: e.index
        }], function () {
            ne.getPageOb({
                page: e.index + 1
            }, function () {
                ne.getPageOb({
                    page: e.index - 1
                }, t)
            })
        })
    };

    ne.planPageMapping = function (e, t) {
        function n() {
            t && t({
                url: e.leftOb.url
            })
        }
        var o = ne.getCurrentView(e.leftOb.index, {
            vIndex: e.leftOb.vIndex
        });
        if (o.index == e.leftOb.index)
            if (ne.shouldBePaged(e.leftOb) || "double" == o.view)
                if (e.view = "double", e.rightOb || (e.rightOb = {}), "fxl" == e.leftOb.layout) ne.getChapterURL(e.leftOb.index + 1) ? (e.rightOb.index = e.leftOb.index + 1, e.rightOb.url = ph.join(bookPath, stg.data.packageURL, ne.getChapterURL(e.leftOb.index + 1))) : e.rightOb = null, n();
                else {
                    if (e.rightOb.url = e.leftOb.url, e.leftOb.viewport = $injector.get("spread").getViewport(e.leftOb), e.vIndex = Number(e.vIndex), isNaN(e.vIndex) && (e.vIndex = 0), e.leftOb.vIndex = e.vIndex, e.vIndex < 0 || e.vIndex >= e.leftOb.pages.length) {
                        var r = 0;
                        return e.vIndex < 0 && (r = e.vIndex), e.vIndex = e.leftOb.pages.length + r, e.leftOb.vIndex = e.leftOb.pages.length + r, e.rightOb.vIndex = e.leftOb.vIndex + 1, e.rightOb.index = e.leftOb.index, void t({
                            redirect: {
                                index: o.index
                            }
                        })
                    }
                    if (e.leftOb.content = e.leftOb.pages[e.vIndex].content, "single" == o.view) return e.rightOb = null, void n();
                    if (e.vIndex + 1 < e.leftOb.pages.length) e.rightOb = clone(e.leftOb), e.rightOb.vIndex = e.vIndex + 1, e.rightOb.index = e.leftOb.index, e.rightOb.content = e.leftOb.pages[e.rightOb.vIndex].content, n();
                    else {
                        if (e.leftOb.index >= _ - 1) return e.rightOb = null, void n();
                        e.rightOb.index = e.leftOb.index + 1;
                        var i = ne.getChapterURL(e.rightOb.index);
                        ne.getPageOb({
                            page: e.rightOb.index
                        }, function (t) {
                            e.rightOb = t, e.rightOb.vIndex = 0, e.rightOb.url = ph.join(bookPath, stg.data.packageURL, i), e.rightOb.pages && e.rightOb.pages.length && (e.rightOb.content = e.rightOb.pages[0].content), e.rightOb.viewport = $injector.get("spread").getViewport(e.rightOb), n()
                        })
                    }
                } else e.view = "single", e.leftOb.viewport = null, e.leftOb.raw && (e.leftOb.content = e.leftOb.raw), n();
        else t({
            redirect: {
                index: o.index
            }
        })
    };

    ne.getCacheOrPreload = H, ne.currentUrlMatchesVIndex = function (e) {
        return getQueryStringValue("vi") == e
    };

    ne.urlMatchesIndex = function (e, t) {
        if (null != t) {
            e = ph.normalize(e);
            ph.getHash(e);
            var n, o = ph.removeHash(e),
                r = stg.data.epub.spine.getElementsByTagName("itemref")[t];
            if (r) {
                for (var i = stg.data.epub.manifest.getElementsByTagName("item"), a = 0; a < i.length; a++) i[a].getAttribute("id") == r.getAttribute("idref") && (n = i[a].getAttribute("href"));
                return !!n && ph.removeHash(ph.normalize(n)) == ph.removeHash(ph.normalize(o))
            }
        }
    };

    ne.applyChapterStateFromURL = function (e, t) {
        if (t || (t = {}), 0 != e.indexOf("#")) {
            var n = ph.getHash(e),
                o = t.index ? t.index : ne.indexFromUrl(e);
            null != o && ($rootScope.renderScope.applyHeader("default"), ne.redirect(o, {
                hash: n,
                vIndex: t.vIndex
            }), $rootScope.$$phase || $rootScope.$apply())
        } else ne.redirect(currentIndex, {
            hash: e.substr(1)
        })
    };

    ne.loadChapterByURL = function (e, t) {
        var o = ph.relative(ph.join(bookPath, stg.data.packageURL), e.leftOb.url);
        e || (e = {});
        var r = $q.defer(),
            i = e.hash ? e.hash : ph.getHash(o),
            a = e.leftOb.index;
        if (0 == o.indexOf("#") && (i = o.substr(1)), null != a) {
            if (!e.reload && ne.urlMatchesIndex(o, currentIndex) && currentVIndex == e.vIndex) return currentIndex = a, F(), i ? (t && t(), ne.loadLocation(i, {
                index: currentIndex,
                source: "from loadChapterByUrl"
            })) : ($stateParams.hash ? ($ionicScrollDelegate.$getByHandle("content").scrollTop(!0), r.resolve()) : r.resolve(), t && t(), r.promise);
            desktop && require("nw.gui").App.clearCache(), kInteractive.clearAudioVideo(document), currentVIndex = e.vIndex;
            var s = ne.getCurrentView(a, {
                vIndex: currentVIndex
            });
            return stg.data.currentView = s.view, currentIndex = a = s.index, F(), stg.data.currentPageOb = e.leftOb, stg.data.currentRPageOb = e.rightOb, delete e.leftOb.currentVIndex, mobile && $ionicScrollDelegate.$getByHandle("content").resize(), ne.busy = !0, ne.loadChapterByURLRaw(e).then(function (o) {
                ne.busy = !1, e.scroll && $ionicScrollDelegate.$getByHandle("content").scrollTo(0, e.scroll, !1), config.kotobee.cloudid && (config.kotobee.public || backend.setLastChapter()), setTimeout(function () {
                    virt.reset(e.scroll ? e.scroll : 0, function () {
                        i ? ne.loadLocation(i, {
                            index: currentIndex,
                            source: "loadChapterByURLRaw"
                        }).then(function () {
                            D("renderedChapter"), ne.highlightCurrentTocChapter(), z(), R(), t && t()
                        }) : (jsParser.replaceScript(o || []), D("renderedChapter"), ne.highlightCurrentTocChapter(), z(), R(), t && t())
                    }, 30)
                })
            })
        }
        t && t()
    };

    ne.cacheLookup = function (e, t) {
        if (!B(e))
            for (var n = 0; n < re.length; n++)
                if (re[n].url == ph.removeHash(e)) {
                    var o = re[n];
                    return re.splice(n, 1), re.push(o), o
                }
    };

    ne.removeFromCache = function (e, t) {
        for (var n = 0; n < re.length; n++)
            if (re[n].url == ph.removeHash(e) && re[n].view == t) return void re.splice(n, 1)
    };

    ne.addToCache = function (e, t) {
        if (t || (t = {}), e.url = ph.removeHash(e.url), !B(e.url)) {
            var n = ne.cacheLookup(e.url, e.view);
            n ? t.update && (n.url = e.url, n.title = e.title, n.viewport = e.viewport, n.content = e.content, n.layout = e.layout, n.styles = e.styles, e.script && (n.script = e.script)) : (re.length > 20 && re.shift(), t.lowPriority ? re = [e].concat() : re.push(e))
        }
    };

    ne.loadChapterByURLRaw = function (e) {
        function r(r, i) {
            r || (r = {});
            var l = r.leftOb.title,
                c = (r.leftOb.content, r.leftOb.viewport),
                d = (r.leftOb.styles, r.leftOb.script, r.leftOb.layout),
                f = r.isCached,
                g = r.leftOb,
                h = r.rightOb;
            g && (s = s.concat(g.script)), h && (s = s.concat(h.script)), makeArrayUnique(s), l || (l = g.title), stg.data.book.chapter.title = l, tinCan.send({
                verb: "navigated",
                activity: (currentIndex ? "Chapter " + currentIndex + ": " : "") + l
            }), backend.addStat({
                action: "chapter",
                param: l
            });
            var m = document.getElementById("epubInner");
            r.source = "from chapters", pauseVideoAndAudio(m = context.populateSpreads(m, r)), cache.setOb("epubContent", document.getElementById("epubContent")), cache.getOb("epubContainer").focus(), f || context.setContentSize(cache.getOb("epubContent"), e), stg.data.book.chapter.viewport = c, stg.data.book.chapter.layout = d, stg.data.book.chapter.view = r.view, stg.data.book.chapter.paged = ne.shouldBePaged(g), F();
            var b = document.getElementById("chapters");
            removeClass(b, ["rfl", "fxl", "double", "single"]), addClass(b, stg.data.book.chapter.viewport ? "fxl" : "rfl"), addClass(b, r.view), stg.data.book.chapter.paged ? addClass(b, "paged") : removeClass(b, "paged"), kInteractive.postRender(document);
            f || (cssParser.replaceStyles(""), cssParser.replaceStyles(g.styles.replace(/#spreadR/g, "#spreadL"), {
                append: !0
            }), h && h.viewport && cssParser.replaceStyles(h.styles.replace(/#spreadL/g, "#spreadR"), {
                append: !0
            })), cssParser.replaceStyles("#epubContainer div#epubContent, #epubContainer.stylesEnabled div#epubContent { background-color:#e7e3d9 !important;}", {
                append: !0
            }), config.preserveLoc && "app" != config.kotobee.mode && stg.writeSlot("lastLoc" + stg.getBookId(), currentIndex), stg.writeSlot("lastLocY" + stg.getBookId(), 0);
            for (var y = 0; y < oe.length; y++) oe[y]("newChapter");
            $filter("contentJs")(f).then(function () {
                selection.reset(), virt.unlock(), scorm.setBookmark(currentIndex);
                var e = {};
                e.id = "chapter-" + currentIndex, e.description = "View chapter: " + l, e.learnerResponses = "Viewed", e.type = "other", e.timestamp = new Date, scorm.setInteractions([e]), a.resolve(s)
            })
        }
        e || (e = {});
        var i = ph.relative(ph.join(bookPath, stg.data.packageURL), e.leftOb.url),
            a = (arguments.callee, $q.defer());
        stg.data.book.chapter.url = i, stg.data.book.chapter.absoluteURL = ph.join(bookPath, stg.data.packageURL, stg.data.book.chapter.url);
        ph.getHash(stg.data.book.chapter.url);
        virt.disable(), virt.lock(), status.update(null);
        var s = [];
        return function (e, t) {
            function n() {
                if ("single" != e.view && e.rightOb) {
                    var t = e.rightOb.url;
                    (o = ne.cacheLookup(t, "single")) ? (copyObjToObj(o, e.rightOb, {
                        onlyIfNotExists: !0
                    }), e.rightOb.cached = !0, r(e)) : ne.preloadChapter(t, i, function (t) {
                        copyObjToObj(t, e.rightOb, {
                            onlyIfNotExists: !0
                        }), r(e)
                    })
                } else r(e)
            }
            var o;
            if ("double" == stg.data.currentView && (o = ne.cacheLookup(stg.data.book.chapter.absoluteURL, "single")), o) return copyObjToObj(o, e.leftOb, {
                onlyIfNotExists: !0
            }), e.leftOb.cached = !0, void n();
            var i = {
                showError: !0,
                deferred: a,
                view: e.view,
                id: e.id,
                view: null
            };
            ne.preloadChapter(stg.data.book.chapter.absoluteURL, i, function (t) {
                copyObjToObj(t, e.leftOb, {
                    onlyIfNotExists: !0
                }), n()
            })
        }(e), a.promise
    };

    ne.preloadChapterFromApi = function (e, t) {
        return ne.preloadChapter(ph.join(bookPath, stg.data.packageURL, e), {}, t)
    };

    ne.loadAndDecrypt = function (e, t, n) {
        return j(ph.join(bookPath, stg.data.packageURL, e), t, n)
    };

    ne.preloadChapter = function (e, t, n) {
        var o = {
            func: arguments.callee,
            args: arguments
        };
        t || (t = {}), j(e, function (o) {
            for (var r = parser.parseFromString(o, "text/html"), i = $filter("contentHtml")(r, e), a = r.getElementsByTagName("meta"), s = null, l = 0; l < a.length; l++) {
                var c = a[l].getAttribute("name");
                if (c && "viewport" == c.toLowerCase()) {
                    s = {};
                    var d = a[l].getAttribute("content");
                    if (!d) break;
                    for (var u = d.split(","), p = 0; p < u.length; p++) {
                        var f = u[p].split("=");
                        f[1] && (f[1] = f[1].replace("px", ""), s[f[0].trim()] = Number(f[1].trim()))
                    }
                }
            }
            var g = s ? "fxl" : "rfl";
            $filter("js")(r).then(function (o) {
                $filter("styles")(r, {
                    path: e,
                    id: t.id
                }).then(function (a) {
                    var l, c = r.getElementsByTagName("title");
                    if (c.length) l = c[0].textContent.trim();
                    else {
                        var d = r.getElementsByTagName("h1");
                        d.length && (l = d[0].textContent.trim())
                    }
                    l || (l = stg.data.book.meta.dc.title);
                    var u = {
                        url: e,
                        title: l,
                        viewport: s,
                        layout: g,
                        content: i.outerHTML,
                        styles: a,
                        script: o
                    };
                    t.dontCache || ne.addToCache(u, t), n && n(u)
                })
            })
        }, function () {
            t.showError && (error.update({
                error: "networkError",
                ref: o
            }), t.deferred && t.deferred.reject({}), z())
        })
    };

    ne.clearContent = function () {
        replaceHTML("epubInner", "")
    };

    ne.getChapterURL = function (e) {
        void 0 == e && (e = currentIndex), e < 0 && (e = 0);
        var t;
        try {
            var n = stg.data.epub.spine.getElementsByTagName("itemref");
            if (e >= n.length) return;
            t = n[e]
        } catch (e) { }
        var o = getElementsByAttribute(stg.data.epub.manifest, "item", "id", t.getAttribute("idref"))[0].getAttribute("href"),
            r = t.getAttribute("id");
        return r && (o += "#" + r), o
    };

    ne.isFirstChapter = function (e) {
        U({
            first: 0
        }, e)
    };

    ne.isFirstVisibleChapter = function (e) {
        U({
            first: Z
        }, e)
    };

    ne.isLastChapter = function (e) {
        return V({
            max: _
        }, e)
    };

    ne.isLastVisibleChapter = function (e) {
        return V({
            max: K
        }, e)
    };

    ne.isLastVisibleSingleChapter = function (e, t, n) {
        return V({
            max: K,
            page: e,
            vIndex: t,
            view: "single"
        }, n)
    };

    ne.getPageOb = function (e, t) {
        e || (e = {});
        var n = e.page;
        null == n && (n = currentIndex);
        var o = ne.getChapterURL(n);
        if (!o) return t ? t() : null;
        H(ph.join(bookPath, stg.data.packageURL, o), {}, function (o) {
            if (o.index = n, "fxl" == o.layout && (o.spread = $(o.index)), o.nonlinear = P(o.index), o.pages || "fxl" == o.layout || !ne.shouldBePaged(o) || e.dontParse) return skeleton.add(o), t ? t(o) : null;
            $injector.get("spread").parseReflowableChapter(o, function (e) {
                return o.pages = e.pages, o.raw = e.raw, skeleton.add(o), t ? t(o) : null
            })
        })
    };

    ne.shouldBePaged = function (e, t) {
        return (!e || "fxl" != e.layout) && ("double" == stg.data.settings.viewMode || ("single" == stg.data.settings.viewMode ? !stg.data.settings.pageScroll : (!t && e && (t = ne.getCurrentView(e.index).view), "single" != t || !stg.data.settings.pageScroll)))
    };

    ne.pdfPrint = function () {
        function e(e) {
            e || (e = {}), e.scale || (e.scale = 100);
            var o = q();
            isKotobeeHosted() && config.kotobee.cloudid && !config.kotobee.public && (o = W());
            var r = stg.data.book.chapter.title,
                a = {
                    url: o,
                    filename: r
                };
            status.update(translit.get("pleaseWait"), null, {
                dots: !0
            }), backend.createRequest(config.kotobee.liburl + "link/pdfgenerate/url", a, function (n) {
                if (!n) return status.update(), void error.update({
                    error: "networkError",
                    ref: t
                });
                var r = n;
                X && X.parentNode && X.parentNode.removeChild(X), (X = document.createElement("iframe")).setAttribute("style", "display:none"), X.setAttribute("name", "pdfFrame"), document.body.appendChild(X), Q && Q.parentNode && Q.parentNode.removeChild(Q), (Q = document.createElement("form")).setAttribute("style", "visibility:hidden:position:absolute"), Q.setAttribute("method", "post"), Q.setAttribute("action", r), Q.setAttribute("target", "pdfFrame"), addFormHiddenInput("url", o, Q), addFormHiddenInput("filename", stg.data.book.chapter.title, Q), addFormHiddenInput("fileext", "pdf", Q), addFormHiddenInput("scale", e.scale, Q), addFormHiddenInput("format", config.pdfMode ? config.pdfMode : "imgpdf", Q), asyncCheck(!config.kotobee.encodekey, j, [stg.data.book.chapter.absoluteURL], function (e) {
                    function t() {
                        X && X.parentNode && X.parentNode.removeChild(X), Q && Q.parentNode && Q.parentNode.removeChild(Q), status.update()
                    }
                    config.kotobee.encodekey && addFormHiddenInput("data", e, Q), document.body.appendChild(Q), Q.submit(), setTimeout(function () {
                        X.addEventListener ? X.addEventListener("load", t, !0) : X.attachEvent && X.attachEvent("onload", t)
                    }), setTimeout(function () {
                        status.update()
                    }, 4e3)
                })
            })
        }
        var t = {
            func: arguments.callee,
            args: arguments
        };
        if (preview) error.update({
            error: "savePdfUnsupportedInPreview"
        });
        else if (!desktop && !native)
            if (config.pdfsavingOptions) {
                var o = {
                    scale: 100
                };
                o.submit = function () {
                    modals.hide(), e({
                        scale: o.scale
                    })
                }, modals.show({
                    mode: "pdfSave",
                    pdfSaveOptions: o
                })
            } else e()
    };

    ne.pdfPrintOld = function () {
        preview ? error.update({
            error: "savePdfUnsupportedInPreview"
        }) : desktop || native || (status.update(translit.get("pleaseWait"), null, {
            dots: !0
        }), window.location.href = "https://www.kotobee.com/link/pdfgenerate?url=" + q(), setTimeout(function () {
            status.update()
        }, 4e3))
    };

    ne.printAsBitmap = function () {
        var e = {
            func: arguments.callee,
            args: arguments
        };
        if (!native && !mobile) {
            var t = q();
            isKotobeeHosted() && config.kotobee.cloudid && !config.kotobee.public && (t = W());
            var o = stg.data.book.chapter.title,
                r = {
                    url: t,
                    filename: o
                };
            status.update(translit.get("pleaseWait"), null, {
                dots: !0
            }), backend.createRequest(config.kotobee.liburl + "link/pdfgenerate/url", r, function (n) {
                function o() {
                    try {
                        var e = this.contentWindow;
                        setTimeout(function () {
                            e.__container__ = this, e.focus(), e.print(), status.update()
                        }, 2500)
                    } catch (e) {
                        status.update()
                    }
                }
                if (!n) return status.update(), void error.update({
                    error: "networkError",
                    ref: e
                });
                var r = n;
                ee && ee.parentNode && ee.parentNode.removeChild(ee), (ee = document.createElement("iframe")).setAttribute("style", "display:none"), ee.setAttribute("name", "printFrame"), ee.id = "printContainer", ee.style.visibility = "hidden", ee.style.position = "fixed", ee.style.right = "0", ee.style.bottom = "0", document.body.appendChild(ee), te && te.parentNode && te.parentNode.removeChild(te), (te = document.createElement("form")).setAttribute("style", "visibility:hidden:position:absolute"), te.setAttribute("method", "post"), te.setAttribute("action", r), te.setAttribute("target", "printFrame"), addFormHiddenInput("url", t, te), addFormHiddenInput("filename", stg.data.book.chapter.title, te), addFormHiddenInput("fileext", "pdf", te), addFormHiddenInput("print", !0, te), addFormHiddenInput("format", "imghtml", te), asyncCheck(!config.kotobee.encodekey, j, [stg.data.book.chapter.absoluteURL], function (e) {
                    config.kotobee.encodekey && addFormHiddenInput("data", e, te), document.body.appendChild(te), te.submit(), ee.onload = o, setTimeout(function () {
                        status.update()
                    }, 8e3)
                })
            })
        }
    };

    ne.print = function () {
        native || mobile || ("fxl" == stg.data.book.chapter.layout ? ne.printAsBitmap() : (status.update(translit.get("pleaseWait"), null, {
            dots: !0
        }), closePrintIframe(), j(q(), function (e) {
            var t = ph.removeFilename(q());
            return e = (e = (e = e.replace("<head>", "<head><meta http-equiv=\"Content-Security-Policy\" content=\"default-src * gap://ready file: filesystem: data:; style-src * 'self' 'unsafe-inline' filesystem: data:; script-src 'self' 'unsafe-inline' 'unsafe-eval' https://*.youtube.com https://*.kotobee.com http://*.youtube.com https://*.ytimg.com http://*.ytimg.com;img-src * filesystem: data: blob: android-webview-video-poster:\">")).replace("<head>", "<head><script> window.onload = function() { window.print(); parent.closePrintIframe(); }<\/script>")).replace("<head>", "<head><base href='" + t + "/'/>"), (n = document.createElement("iframe")).id = "printContainer", n.style.visibility = "hidden", n.style.position = "fixed", n.style.right = "0", n.style.bottom = "0", n.setAttribute("srcdoc", e), document.body.appendChild(n), void status.update();
            var n
        })))
    };
    var ae, se, le;

    ne.regenerateSpreads = function (e, t) {
        if (e || (e = {}), ne.shouldBePaged(stg.data.currentPageOb))
            if (ne.regeneratingSpreads) se = !0;
            else {
                se = !1, cache.getOb("epubContainer").style.opacity = 0, ne.showLoader();
                var n;
                stg.data.currentPageOb.pages && stg.data.currentPageOb.pages.length && (n = currentVIndex / stg.data.currentPageOb.pages.length), ae && clearTimeout(ae), ae = setTimeout(function () {
                    ne.regeneratingSpreads = !0, le && clearTimeout(le), le = setTimeout(function () {
                        ne.regeneratingSpreads = !1, ne.regenerateSpreads(e, t)
                    }, 2e4), ne.resetPages(), ne.preloadEdgeChapters(function () {
                        ne.getPageOb({
                            page: currentIndex
                        }, function (e) {
                            var o = {};
                            o.viewMode = stg.data.settings.viewMode, o.vIndex = currentVIndex, n && e.pages && (o.vIndex = Math.floor(e.pages.length * n), o.vIndex != currentVIndex && $location.search({
                                vi: o.vIndex
                            })), o.reload = !0, ne.loadChapterByIndex(currentIndex, o).then(function () {
                                ne.regeneratingSpreads = !1, clearTimeout(le), se ? setTimeout(function () {
                                    se = !1, ne.regenerateSpreads(o, t)
                                }, 100) : (t && t(), cache.getOb("epubContainer").style.opacity = 1, ne.hideLoader())
                            })
                        })
                    })
                }, e.resize ? 600 : 30)
            }
    };

    ne.resetPages = function (e) {
        e || (e = {});
        for (var t = 0; t < re.length; t++) re[t].pages = null;
        return skeleton.reset(), e.buildChapterMode ? skeleton.initialize({
            first: Z,
            last: K
        }) : stg.data.book.meta.rendition && "pre-paginated" == stg.data.book.meta.rendition.layout ? skeleton.initialize({
            first: Z,
            last: K
        }) : "single" == stg.data.settings.viewMode && stg.data.settings.pageScroll ? skeleton.initialize({
            first: Z,
            last: K
        }) : void 0
    };

    ne.getIndexFromTocElem = function (e) {
        for (var t = e.getAttribute("href"), n = document.getElementById("chaptersInner").getElementsByTagName("a"), o = 0, r = 0; r < n.length && (n[r].getAttribute("href") != t || (o++ , e != n[r])); r++);
        return ne.indexFromUrl(e.getAttribute("href"), {
            order: o
        })
    };

    ne.highlightCurrentTocChapter = function () {
        mobile && $injector.get("nav").resize();
        for (var e = stg.data.currentPageOb.index, t = stg.data.book.chapter.url, n = document.getElementById("chaptersInner").getElementsByTagName("a"), o = [], r = 0; r < n.length; r++) - 1 != n[r].getAttribute("href").indexOf(t) && ne.getIndexFromTocElem(n[r]) == e && o.push({
            elem: n[r],
            id: ph.getHash(n[r].getAttribute("href"))
        });
        ne.deApplySelectedChapter(), o.length && ne.applySelectedChapter(o[0].elem.parentNode)
    };

    ne.applySelectedChapter = function (e, t) {
        if (e || (e = ne.getElem), null == t && (t = !0), addClass(e, "selected"), addClass(e, "currentChapter"), t) {
            for (var n = e, o = cache.getOb("chaptersContent"); ;) {
                if (!(e = e.parentNode)) break;
                if (e == o) break;
                "li" == e.nodeName.toLowerCase() && addClass(e, "expanded")
            }
            overflowScroll ? o.scrollTop = Math.round(n.offsetTop) : $ionicScrollDelegate.$getByHandle("chapters").scrollTo(0, Math.round(n.offsetTop - o.parentNode.offsetHeight / 2), !1)
        }
    };

    ne.deApplySelectedChapter = function () {
        for (var e = cache.getOb("chaptersContent"), t = e.getElementsByClassName("selected"), n = t.length; n--;) removeClass(t[n], "selected");
        for (n = (t = e.getElementsByClassName("currentChapter")).length; n--;) t[n].removeAttribute("name")
    };

    return ne;
}]);

app.factory("context", ["skeleton", "cssParser", "cache", "$rootScope", "stg", "$injector", "$http", function (skeleton, cssParser, cache, $rootScope, stg, $injector, $http) {
    function l(e, n) {
        var o;
        null != stg.data.currentPageOb.currentVIndex && (isNaN(stg.data.currentPageOb.currentVIndex) || (o = stg.data.currentPageOb.vIndex, stg.data.currentPageOb.vIndex = currentVIndex)), skeleton.getNextIndex(stg.data.currentPageOb, function (r) {
            null != o && (stg.data.currentPageOb.vIndex = o);
            var a;
            r && (a = [e, r.leftOb]), asyncCheck(!r, m.arePagesAppearingTogether, a, function (a) {
                if (a && r) return n("next");
                null != currentVIndex && (stg.data.currentPageOb.vIndex = currentVIndex), skeleton.getPrevIndex(stg.data.currentPageOb, function (t) {
                    null != o && (stg.data.currentPageOb.vIndex = o);
                    m.getCurrentView(t.leftOb.index, {
                        vIndex: t.leftOb.vIndex
                    });
                    m.arePagesAppearingTogether(e, t.leftOb, function (e) {
                        return n(e ? "prev" : null)
                    })
                })
            })
        })
    }

    function c(e, t) {
        if (e || (e = {}), e.reload) t && t();
        else {
            var n = cache.getOb("epubContainer");
            if (null == currentIndex) return n.style.opacity = 0, m.showLoader(), void (t && t());
            l(e.pageOb, function (o) {
                if (!o) return n.style.opacity = 0, e.pageOb.index != currentIndex && m.showLoader(), void (t && t());
                var r = document.getElementById(o + "Page"),
                    s = r.getElementsByTagName("iframe");
                if (s.length) {
                    var l = s[0].contentDocument.getElementById("epubContent"),
                        d = s[0].srcdoc;
                    if (l || !d) {
                        if (s[0].style.visibility = "visible", l) {
                            var u = $injector.get("nav").viewFromElem(l);
                            "fxl" == u.layout && h(s[0], u)
                        }
                    } else if (requestAnimationFrame) return void requestAnimationFrame(function () {
                        c(e, t)
                    })
                }
                if ("navSlide" == stg.data.settings.navAnim) stg.data.settings.rtl ? r.style.transform = "translate3d(" + ("next" == o ? "-" : "") + n.offsetWidth + "px,0,0)" : r.style.transform = "translate3d(" + ("next" == o ? "" : "-") + n.offsetWidth + "px,0,0)", setTimeout(function () {
                    e.animID == y && (stg.data.settings.rtl ? autoprefixTransform(n, "translate3d(" + ("next" == o ? "" : "-") + n.offsetWidth + "px,0,0)") : autoprefixTransform(n, "translate3d(" + ("next" == o ? "-" : "") + n.offsetWidth + "px,0,0)"), r.style.transform = "translate3d(0,0,0)", addClass(n, "anim"), addClass(r, "anim"), addClass(r, "activeSlidePage"), setTimeout(function () {
                        e.animID == y && (autoprefixTransform(n, null), removeClass(n, "anim"), t && t())
                    }, 330))
                }, 30);
                else if ("navFade" == stg.data.settings.navAnim) r.style.left = 0, r.style.opacity = 1, addClass(r, "anim"), addClass(r, "activeSlidePage"), setTimeout(function () {
                    e.animID == y && t && t()
                }, 330);
                else if ("navFlip" == stg.data.settings.navAnim) addClass(n, "anim"), stg.data.settings.rtl ? (autoprefixTransform(n, "next" == o ? "rotateY(90deg)" : "rotateY(-90deg)"), autoprefixTransform(r, "next" == o ? "rotateY(-90deg)" : "rotateY(90deg)")) : (autoprefixTransform(n, "next" == o ? "rotateY(-90deg)" : "rotateY(90deg)"), autoprefixTransform(r, "next" == o ? "rotateY(90deg)" : "rotateY(-90deg)")), n.style.opacity = 0, setTimeout(function () {
                    e.animID == y && (autoprefixTransform(r, null), addClass(r, "anim"), addClass(r, "activeSlidePage"), setTimeout(function () {
                        e.animID == y && (removeClass(n, "anim"), autoprefixTransform(n, null), t && t())
                    }, 630))
                }, 30);
                else if ("navBookBlock" == stg.data.settings.navAnim) {
                    var p = document.createElement("div");
                    p.className = "shadow", p.style.opacity = 0, p.appendChild(document.createElement("div")), n.appendChild(p), addClass(n, "anim"), stg.data.settings.rtl ? autoprefixTransform(n, "next" == o ? "rotateY(90deg)" : "rotateY(-90deg)") : autoprefixTransform(n, "next" == o ? "rotateY(-90deg)" : "rotateY(90deg)");
                    var f = n.style.transform || n.style.webkitTransform || n.style.mozTransform;
                    autoprefixTransformOrigin(n, "rotateY(90deg)" == f ? "right top" : "left top"), n.style.opacity = 0, addClass(r, "anim"), addClass(r, "activeSlidePage");
                    var g = document.createElement("div");
                    g.className = "shadow", g.style.opacity = .7, r.appendChild(g), p.style.opacity = 0, p.style.height = n.scrollHeight + "px", g.style.marginLeft = n.style.marginLeft, g.style.marginRight = n.style.marginRight, g.style.marginBottom = n.style.marginBottom;
                    var v = getElemMargin(n);
                    v.top || (v.top = 0);
                    var b = getComputedStyle(n).paddingTop.split("px");
                    g.style.marginTop = Number(v.top) + Number(b[0]) + "px";
                    var k = getComputedStyle(n).backgroundColor,
                        w = "rgba(0,0,0,0)" == (k = k.replace(/ /g, ""));
                    w && (n.style.backgroundColor = "#fff"), setTimeout(function () {
                        e.animID == y && (g.style.opacity = null, setTimeout(function () {
                            p.style.opacity = .4, setTimeout(function () {
                                autoprefixTransformOrigin(n, null), g.parentNode && g.parentNode.removeChild(g), p.parentNode && p.parentNode.removeChild(p), e.animID == y && (removeClass(n, "anim"), n.style.opacity = null, w && (n.style.backgroundColor = null), autoprefixTransform(n, null), t && t())
                            }, 430)
                        }, 200))
                    }, 30)
                }
            })
        }
    }

    function d(e) {
        if (e || (e = {}), !e.reload) {
            var t = cache.getOb("epubContainer");
            null != t.style.opacity && (t.style.opacity = null, m.hideLoader()), native && (t.scrollTop = 0);
            var n = document.getElementsByClassName("activeSlidePage");
            if (n.length) {
                var r = n[0];
                "navSlide" == stg.data.settings.navAnim ? r.style.transform = null : "navFade" == stg.data.settings.navAnim ? r.style.opacity = null : "navFlip" == stg.data.settings.navAnim ? autoprefixTransform(r, null) : "navBookBlock" == stg.data.settings.navAnim && (deleteTransform(r), deleteTransform(t), t.style.opacity = null);
                for (var a = 0; a < t.children.length; a++)
                    if ("shadow" == t.children[a].className) {
                        t.removeChild(t.children[a]);
                        break
                    }
                removeClass(r, "anim"), removeClass(r, "activeSlidePage");
                var s = '<h3 id="ebookLoaderAnim"><div class="splashSpinner large"><div class="dot1"></div><div class="dot2"></div></div></h3>';
                document.getElementById("nextPage").innerHTML = s, document.getElementById("prevPage").innerHTML = s
            }
        }
    }

    function u(e) {
        function n(e, t, n) {
            e && m.getPageOb({
                page: e.leftOb.index
            }, function (o) {
                if (o) {
                    var r = {};
                    r.leftOb = clone(o), r.vIndex = e.leftOb.vIndex, r.vIndex < 0 && o.pages && o.pages.length && (r.vIndex += o.pages.length), m.planPageMapping(r, function (e) {
                        p(r.leftOb.url, function (e) {
                            r.styles = e, r.container = document.getElementById(t), r.view = m.getCurrentView(r.leftOb.index, r.leftOb).view, r.source = "from preloadchapters", f(r, n)
                        })
                    })
                }
            })
        }
        m.isFirstChapter(function (o) {
            m.isLastChapter(function (r) {
                r || skeleton.getNextIndex(e.leftOb, function (e) {
                    m.getSurroundingPages(e ? e.leftOb : null, function () {
                        n(e, "nextPage")
                    })
                }), o || skeleton.getPrevIndex(e.leftOb, function (e) {
                    m.getSurroundingPages(e ? e.leftOb : null, function () {
                        n(e, "prevPage")
                    })
                })
            })
        })
    }

    function p(e, t) {
        function o() {
            t(b)
        }
        if (b) t(cssParser.applyRoot(e, b));
        else {
            var r = isKotobeeHosted(),
                i = "lib/ionic/release/css/ionic.min.css";
            r && (i = "/bundles/vijualib/templates/reader/cloud/lib/ionic/release/css/ionic.min.css"), $http.get(i).success(function (i) {
                if (v = i, r) $http.get("/bundles/vijualib/templates/reader/cloud/css/styles.min.css").success(function (e) {
                    b = e, o()
                }).error(function () {
                    t(cssParser.applyRoot(e, b))
                });
                else {
                    var a = "css/styles.min.css",
                        l = "http://localhost:8080" == window.location.origin;
                    l && (a = "css/style.css"), $http.get(a).success(function (e) {
                        b = e, l ? $http.get("css/base.css").success(function (e) {
                            b = e + " " + b, $http.get("css/kotobeeInteractive.css").success(function (e) {
                                b += e, o()
                            }).error(function () {
                                o()
                            })
                        }).error(function () {
                            o()
                        }) : o()
                    }).error(function () {
                        t(cssParser.applyRoot(e, b))
                    })
                }
            }).error(function () {
                t(cssParser.applyRoot(e, b))
            })
        }
    }

    function f(e, t) {
        e || (e = {});
        var n = e.styles;
        e.container && (n || (n = ""), m.shouldBePaged(e.leftOb) && (e.leftOb && (e.leftOb.viewport = $injector.get("spread").getViewport(e.leftOb)), e.rightOb && (e.rightOb.viewport = $injector.get("spread").getViewport(e.rightOb))), e.simulate ? g(e, t) : m.getPageOb({
            page: e.leftOb.index
        }, function (n) {
            copyObjToObj(n, e.leftOb, {
                onlyIfNotExists: !0
            }), e.leftOb.pages && e.leftOb.vIndex < 0 && (e.leftOb.vIndex += e.leftOb.pages.length), e.rightOb ? m.getPageOb({
                page: e.rightOb.index
            }, function (n) {
                copyObjToObj(n, e.rightOb, {
                    onlyIfNotExists: !0
                }), e.rightOb.pages && e.rightOb.vIndex < 0 && (e.rightOb.vIndex += e.rightOb.pages.length), g(e, t)
            }) : g(e, t)
        }))
    }

    function g(e, t) {
        function n(e) {
            var t = "";
            v && (t += v);
            var n = " body{margin:0; padding-top:0px;} #epubContainer{padding-top:0px;position:absolute;top:0;left:0;right:0;bottom:0 !important}";
            return mobile || (n += "body, #epubContainer{padding-top:1px;}"), t += n, t += "html,#epubContainer{overflow-y:" + (mobile ? "hidden" : "auto") + "}", t += "#epubScroller{position: static;height: 100%;width: 100%;box-sizing: border-box;}", !mobile && stg.data.settings.rtl && (t += "#epubContainer{overflow:auto !important}"), mobile && (t += " #epubContent ::-webkit-scrollbar {display: none;} #epubContent {-ms-overflow-style: none;overflow: -moz-scrollbars-none;}"), t += "#epubScroller{padding:" + getElemPadding(cache.getOb("epubContainer").getElementsByClassName("scroll")[0], {
                computed: !0
            }).left + "px}", c.viewport && c.viewport.width && (t += "#epubContent{width:" + c.viewport.width + "px;height:" + c.viewport.height + "px; overflow:hidden}", t += "html{overflow-x:" + (mobile ? "hidden" : "auto") + "}", t += " #epubInner{height:0;} ", t += " #spreadL,#spreadR{position:absolute !important; top:0 !important;font-size: 1em !important;overflow:hidden;}"), c.viewport || mobile || (t += "#epubScroller{overflow:auto}"), t += " #epubContent{padding:0px; line-height: 1.6;} ", t += " #epubContent{box-sizing:border-box; font-size:" + stg.data.settings.textSize + " !important;}", t += " #epubContent.hide{display:block !important}", t += " #epubContent section{margin-top:0 !important}", stg.data.settings.textSize && (t += " #epubContent{font-size: " + stg.data.settings.textSize + "}"), t += b, e || (e = ""), t += e, t = t.replace("background-color:#e7e3d9 !important;", "")
        }

        function a() {
            var e = arguments[2];
            return 0 == e.indexOf('"') && (e = e.split('"')[1]), 0 == e.indexOf("'") && (e = e.split("'")[1]), 0 == e.indexOf("&quot;") && (e = e.split("&quot;")[1]), -1 != e.indexOf("http://") ? arguments[0] : -1 != e.indexOf("https://") ? arguments[0] : 0 == e.indexOf("data:") ? arguments[0] : (e = ph.relative(ph.removeFilename(u), e), arguments[1] + e + arguments[3])
        }

        function s(t) {
            t || (t = ""), t = (t = (t = t.replace(/<div class="answer correct">/g, '<div class="answer">')).replace(/<div class="correct">/g, "<div>")).replace(/<p class="explanation">.*?<\/p>/g, ""), e.simulate || (t = t.replace(/(style=".*?url\()(.+?)(\).*?")/g, a));
            isKotobeeHosted();
            $rootScope.readerApp || e.simulate || (t = (t = t.replace(/src="(?!http[s]?\:\/\/)([^"]*)?"/g, function (e, t) {
                return 'src="' + ph.relative(u, t) + '"'
            })).replace(/xlink:href="(?!http[s]?\:\/\/)([^"]*)?"/g, function (e, t) {
                return 'xlink:href="' + ph.relative(u, t) + '"'
            }));
            c.title;
            return t
        }
        e.id = Math.round(999 * Math.random());
        var l = e.container,
            c = e.leftOb,
            d = e.rightOb,
            u = c.url,
            p = ph.removeHash(window.location.href),
            f = document.createElement("html"),
            g = document.createElement("head");
        ios && (g.innerHTML = "<meta http-equiv=\"Content-Security-Policy\" content=\"default-src * filesystem: data:; style-src 'self' 'unsafe-inline' filesystem: data:; script-src 'self' 'unsafe-inline' 'unsafe-eval' https://*.youtube.com http://*.youtube.com https://*.ytimg.com http://*.ytimg.com;img-src * filesystem: data: file:\">"), f.appendChild(g);
        var m = document.createElement("base"),
            y = document.getElementsByTagName("base");
        if (y.length)
            for (S = 0; S < y.length; S++) {
                var w = y[S].getAttribute("href");
                if (w) {
                    p = 0 == w.indexOf("/") || isAbsoluteURLPath(w) ? w : ph.join(window.location.origin, w);
                    break
                }
            }
        m.setAttribute("href", ph.join(p, ph.removeFilename(u)) + "/"), e.simulate || g.appendChild(m);
        var C = document.createElement("div");
        C.className = "stylesEnabled", C.id = "epubContainer";
        var x = document.createElement("div");
        x.id = "epubScroller", x.className = "scroll", C.appendChild(x);
        var I = document.createElement("div");
        I.id = "epubInner", x.appendChild(I);
        var E = document.createElement("style");
        E.setAttribute("type", "text/css"), c.pages && null != c.vIndex ? c.content = c.pages[c.vIndex].content : !c.content && c.raw && (c.content = c.raw), c.content = s(c.content), d && (d.pages && null != d.vIndex ? d.content = d.pages[d.vIndex].content : !d.content && d.raw && (d.content = d.raw), d.content = s(d.content)), e.cached = !1, I = k.populateSpreads(I, e), e.cached || (c.styles = c.styles.replace(/#spreadR/g, "#spreadL"), e.simulate || (c.styles = c.styles.replace(/(url\()(.*?)(\))/g, a)), d && (d.styles = d.styles.replace(/#spreadL/g, "#spreadR"), e.simulate || (d.styles = d.styles.replace(/(url\()(.*?)(\))/g, a))));
        c.styles;
        var T = n(c.styles);
        !e.cached && d && (T += " " + n(d.styles), d.styles), E.innerHTML = T, g.appendChild(E);
        for (var O = I.getElementsByTagName("audio"), S = O.length; S--;) {
            try {
                O[S].pause(), O[S].children.length || (O[S].src = "")
            } catch (e) { }
            var A = O[S].parentNode;
            A.previousSibling && hasClass(A.previousSibling, "playBtn") && removeClass(A.previousSibling, "hide"), A.removeChild(O[S])
        }
        for (var L = I.getElementsByTagName("video"), S = L.length; S--;) {
            try {
                L[S].pause(), L[S].children.length || (L[S].src = "")
            } catch (e) { }
            var B = L[S].parentNode;
            B.previousSibling && hasClass(B.previousSibling, "playBtn") && removeClass(B.previousSibling, "hide"), B.removeChild(L[S])
        }
        var N = document.createElement("body");
        stg.data.settings.rtl && addClass(N, "rtl"), addClass(N, c.layout), addClass(N, e.view), N.appendChild(C), f.appendChild(N);
        var M = document.createElement("iframe");
        if (M.setAttribute("srcdoc", f.outerHTML), M.style.visibility = "hidden", detectIE() || native && ios) {
            var P = "javascript: window.frameElement.getAttribute('srcdoc');";
            M.setAttribute("src", P), M.contentWindow && (M.contentWindow.location = P)
        }
        M.onload = function () {
            M.style.visibility = "visible", h(M, e)
        }, l && (l.innerHTML = "", l.appendChild(M), t && t(M))
    }

    function h(e, t) {
        t || (t = {});
        var n = t.leftOb,
            o = {},
            r = !!t.rightOb,
            i = e.contentDocument,
            s = i.getElementById("epubContent"),
            l = i.getElementById("epubContainer");
        if (k.setContentSize(s, t), n.viewport) {
            o.epubContainer = l, o.epubContent = s, o.viewport = n.viewport, o.view = r ? "double" : "single", o.stylesFromEpubContent = !0;
            var c = $injector.get("nav").getFxlFit(o, "FROM CONTEXT"),
                d = c.scrollContainer;
            autoprefixTransform(d, "scale(" + c.bestFit + ")"), autoprefixTransformOrigin(d, "top left;"), d.style.height = "100%", $injector.get("nav").fxlAdjustAlignment(o)
        } else {
            deleteTransform(s);
            var u = i.getElementById("spreadL");
            u.style.width = u.style.height = null;
            var p = i.getElementById("spreadR");
            p && p.parentNode.removeChild(p)
        }
    }
    var m, v, b, y, k = {};

    k.initialize = function () {
        m = $injector.get("chapters")
    };

    k.startTransition = function (e, t) {
        d(e), setTimeout(function () {
            var n = {
                animID: y = Math.round(1e4 * Math.random()),
                reload: e.reload,
                pageOb: e.leftOb
            };
            c(n, function () {
                m.loadChapterByURL(e, function () {
                    setTimeout(function () {
                        n.animID == y && (d(e), u(e), t && t.resolve())
                    }, mobile ? 500 : 0)
                })
            })
        })
    };

    k.interruptAnimation = function () {
        d()
    };

    k.simulateChapter = function (e, t) {
        p(e.url, function (n) {
            var o = document.createElement("div"),
                r = {};
            r.styles = n, r.styles += "#spreadL{left:0 !important;width:auto !important;height:auto !important;position:static !important;}", r.styles += "#epubContent{width:auto !important;height: auto !important;}", r.container = o, r.leftOb = clone(e), "double" == m.getCurrentView(r.leftOb.index).view && (r.rightOb = {}, r.rightOb.content = "<div><sections class='blankForSimulation'></sections></div>", r.rightOb.styles = "", r.rightOb.layout = "rfl"), r.simulate = !0, f(r, t)
        })
    };

    k.populateSpreads = function (e, t) {
        t || (t = {});
        var n = 0;
        "rfl" == t.leftOb.layout && "double" == t.view && (n = config.spreadSpacing ? config.spreadSpacing : 0);
        var o = t.leftContent ? t.leftContent : t.leftOb.content;
        if (!t.cached) {
            (d = document.createElement("div")).innerHTML = t.leftContent ? t.leftContent : t.leftOb.content;
            var r = d.children[0],
                a = r.children[0];
            copyAttributes(r, a), removeClass(a, "adjustedLineHeight"), a.id = "spreadL", a.style.transform = a.style.transformOrigin = a.style.height = null, t.leftOb.viewport ? (a.style.left = "0px", a.style.width = t.leftOb.viewport.width + "px", a.style.height = t.leftOb.viewport.height + "px", stg.data.settings.rtl && (a.style.left = "auto", a.style.right = "0px")) : (a.style.left = null, a.style.width = null, a.style.height = null), addClass(a, t.leftOb.layout), o = d.innerHTML
        }
        var s = replaceHTML(e, o);
        if (t.rightOb) {
            var l;
            if (s.children[0].children.length > 1)
                for (var c = 0; c < s.children[0].children.length; c++)
                    if ("spreadR" == s.children[0].children[c].id) {
                        l = s.children[0].children[c];
                        break
                    }
            if (!l) {
                var d = document.createElement("div");
                d.innerHTML = t.rightContent ? t.rightContent : t.rightOb.content;
                var u = d.children[0];
                copyAttributes(u, l = u.children[0]), removeClass(l, "adjustedLineHeight"), l.id = "spreadR", l.style.transform = l.style.transformOrigin = l.style.height = null, s.children[0].appendChild(l)
            }
            t.rightOb.viewport && (t.leftOb.viewport && (l.style.left = t.leftOb.viewport.width + n + "px", stg.data.settings.rtl && (l.style.right = l.style.left, l.style.left = "auto")), l.style.width = t.rightOb.viewport.width + "px", l.style.height = t.rightOb.viewport.height + "px", addClass(l, t.rightOb.layout))
        }
        return s
    };

    k.test = function (e) { };

    k.setContentSize = function (e, t) {
        t || (t = {});
        var n = t.leftOb,
            o = t.rightOb;
        e.style.width = e.style.height = null;
        var r = 0;
        "rfl" == t.leftOb.layout && "double" == t.view && (r = config.spreadSpacing ? config.spreadSpacing : 0);
        var i = n.viewport;
        if (i) {
            if (i.width) {
                var a = i.width;
                o && o.viewport && (a += o.viewport.width + r), e.style.width = a + 0 + "px"
            }
            if (i.height) {
                var s = i.height;
                o && o.viewport && (s = Math.max(s, o.viewport.height)), e.style.height = s + 0 + 0 + "px"
            }
        }
    };

    return k;
}]);

app.factory("crdv", ["stg", "status", "error", "translit", "$rootScope", "$http", "$ionicSideMenuDelegate", function (stg, status, error, translit, $rootScope, $http, $ionicSideMenuDelegate) {
    function s(t) {
        stg.data.settings.rtl ? $ionicSideMenuDelegate.toggleLeft() : $ionicSideMenuDelegate.toggleRight()
    }

    function l() {
        try {
            return cordova, !0
        } catch (e) {
            return !1
        }
    }

    function c(e, n, o, r) {
        try {
            status.updateProgress(0), e.getEntries(function (i) {
                function a() {
                    if (s >= i.length) e.close(function () {
                        o && o()
                    });
                    else {
                        status.updateProgress(s / i.length);
                        var l = i[s];
                        s++ , l.getData(new zip.BlobWriter, function (e) {
                            try {
                                if ("/" == l.filename.substr(-1)) return void a();
                                u.writeFile(e, ph.join(n, l.filename), u.libEbooksDirectory, function () {
                                    a()
                                })
                            } catch (e) {
                                r && r()
                            }
                        }, function (e, t) { })
                    }
                }
                var s = 0;
                a()
            })
        } catch (e) {
            r && r()
        }
    }

    function d(e) {
        switch (e.code) {
            case FileError.QUOTA_EXCEEDED_ERR:
                "QUOTA_EXCEEDED_ERR";
                break;
            case FileError.NOT_FOUND_ERR:
                "NOT_FOUND_ERR";
                break;
            case FileError.SECURITY_ERR:
                "SECURITY_ERR";
                break;
            case FileError.INVALID_MODIFICATION_ERR:
                "INVALID_MODIFICATION_ERR";
                break;
            case FileError.INVALID_STATE_ERR:
                "INVALID_STATE_ERR";
                break;
            default:
                "Unknown Error"
        }
    }
    var u = {};
    u.initialize = function (t, o) {
        putTest("in1"), l() ? (putTest("in3"), stg.data.cordova = !0, putTest("in4"), putTest("in5"), t.$$phase || t.$apply(), stg.copyLocalToNativeStorage(), u.initAds(), u.initDirectories(function () {
            $http.get("permit.json").success(function (e) {
                function t() {
                    var e = {
                        error: "errorAndCallback",
                        msg: "noAppPermit",
                        cb: u.exit
                    };
                    error.update(e), $rootScope.$$phase || $rootScope.$apply()
                } ("udid" == e.type ? window.plugins.uniqueDeviceID.get : "mac" == e.type ? window.MacAddress.getMacAddress : window.plugins.imeiplugin.getImei)(function (n) {
                    n || t();
                    try {
                        for (var r, i = e.list.split("\n"), a = 0; a < i.length; a++) {
                            var s = i[a].trim().split(" ")[0].trim(),
                                l = i[a].trim().split("/")[0].trim();
                            if (s == n || l == n) {
                                r = !0;
                                break
                            }
                        }
                    } catch (e) {
                        return void t()
                    }
                    r ? o() : t()
                }, t)
            }).error(function () {
                o()
            })
        }), document.addEventListener("menubutton", s, !1), t.$$phase || t.$apply()) : o()
    };

    u.showKeyboard = function () {
        try {
            if (!l()) return;
            cordova.plugins.Keyboard.show()
        } catch (e) { }
    };

    u.hideKeyboard = function () {
        try {
            cordova.plugins.Keyboard.close()
        } catch (e) { }
    };

    u.tts = function (e, t) {
        var n = {
            text: e,
            rate: 1
        };
        try {
            "iOS" == window.device.platform && ("9" == window.device.version.charAt(0) ? n.rate = 1.5 : "10" == window.device.version.substr(0, 2) ? n.rate = 1.5 : "11" == window.device.version.substr(0, 2) && (n.rate = 1.5))
        } catch (e) { }
        TTS.speak(n, function () {
            t(!0)
        }, function (e) {
            u.toast(translit.get("ttsUnavailableMsg")), t(!1)
        })
    };

    u.toast = function (e) {
        l() && window.plugins.toast.showShortBottom(e)
    };

    u.copyToClipboard = function (e) {
        if (l()) try {
            cordova.plugins.clipboard ? cordova.plugins.clipboard.copy(e) : window.plugins.clipboard.copy(e), u.toast(translit.get("copiedToClipboard"))
        } catch (e) { }
    };

    u.share = function (e) {
        if (l()) try {
            window.plugins.socialsharing.share(e)
        } catch (e) { }
    };

    u.createPDF = function (e, t, n) {
        try {
            var r = new jsPDF;
            margins = {
                top: 10,
                bottom: 0,
                left: 10,
                width: 180
            }, r.fromHTML(e, margins.left, margins.top, {
                width: margins.width
            }, function (e) {
                try {
                    cordova, u.docDirectory.getFile(t, {
                        create: !0
                    }, function (e) {
                        e.createWriter(function (e) {
                            e.onwriteend = function (e) { }, e.onerror = function (e) { }, e.write(r.output()), u.toast(translit.get("pdfSaved"))
                        }, d)
                    }, d)
                } catch (e) {
                    try {
                        setTimeout(function () {
                            r.save(t)
                        }, 1e3)
                    } catch (e) { }
                }
            }, margins)
        } catch (e) { }
    };

    u.initDirectories = function (e) {
        function t(e) { }
        window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem, window.requestFileSystem(window.TEMPORARY, 10485760, function (n) {
            u.cacheDirectory = n.root, window.requestFileSystem(window.PERSISTENT, 0, function (n) {
                u.docDirectory = n.root;
                var o = "kotobeeReader";
                $rootScope.readerApp ? o = "kotobeeReaderApp" : config.kotobee.cloudid && (o += config.kotobee.cloudid), u.docDirectory.getDirectory(o, {
                    create: !0
                }, function (n) {
                    u.appDirectory = n, u.appDirectory.getDirectory("library", {
                        create: !0
                    }, function (n) {
                        u.libEbooksDirectory = n, u.appDirectory.getDirectory("local", {
                            create: !0
                        }, function (n) {
                            u.localDirectory = n, u.localDirectory.getDirectory("books", {
                                create: !0
                            }, function (n) {
                                u.localBooksDirectory = n, u.localDirectory.getDirectory("thumbs", {
                                    create: !0
                                }, function (n) {
                                    u.localThumbsDirectory = n, u.libEbooksDirectory.getFile(".nomedia", {
                                        create: !0
                                    }, function () {
                                        u.localDirectory.getFile(".nomedia", {
                                            create: !0
                                        }, function () {
                                            e()
                                        }, t)
                                    }, t)
                                }, t)
                            }, t)
                        }, t)
                    }, t)
                }, t)
            }, t)
        }, t)
    };

    u.exit = function () {
        l() && (navigator.app ? navigator.app.exitApp() : navigator.device && navigator.device.exitApp())
    };

    u.applyExistingBooksOld = function (e, t) {
        function n(e) {
            return Array.prototype.slice.call(e || [], 0)
        }
        if (l()) {
            var o = u.libEbooksDirectory.createReader(),
                r = [],
                i = function () {
                    o.readEntries(function (o) {
                        if (o.length) r = r.concat(n(o)), i();
                        else {
                            for (var a = 0; a < r.length; a++)
                                for (var s = r[a].name, l = 0; l < e.length; l++) e[l].path == s && (e[l].exists = !0);
                            t()
                        }
                    }, function (e) { })
                };
            i()
        } else t()
    };

    u.applyExistingBooks = function (t) {
        if ((native || desktop) && (t || stg.data.currentLibrary && (t = stg.data.currentLibrary.books), t)) {
            for (i = 0; i < t.length; i++) t[i].exists = !1;
            for (n = 0; n < stg.data.downloaded.length; n++) stg.data.downloaded[n].favorite = !1;
            for (var n = 0; n < stg.data.downloaded.length; n++) {
                var o = stg.data.downloaded[n].path;
                stg.data.downloaded[n].favorite = null;
                for (var i = 0; i < t.length; i++)
                    if (t[i].path == o) {
                        t[i].exists = !0, stg.data.downloaded[n].favorite = t[i].favorite;
                        break
                    }
            }
            $rootScope.$$phase || $rootScope.$apply()
        }
    };

    u.vibrate = function (e) {
        l() && (config.noVibration || (e || (e = 40), ios || (ios && (e /= 4), "high" == config.vibrationIntensity ? e = 40 : "medium" == config.vibrationIntensity ? e = 28 : "low" == config.vibrationIntensity && (e = 15), navigator && navigator.notification && navigator.notification.vibrate && navigator.notification.vibrate(e))))
    };

    u.deleteEbook = function (t, n) {
        u.libEbooksDirectory.getDirectory(t.path, {}, function (o) {
            o.removeRecursively(function () {
                for (var o = 0; o < stg.data.downloaded.length; o++) stg.data.downloaded[o].path == t.path && (t.exists = !1, stg.data.downloaded.splice(o, 1), o--);
                stg.writeSlot("downloaded" + stg.getBookId(!0), stg.data.downloaded, function () {
                    n()
                })
            }, function (e) { })
        }, d)
    };

    u.writeEbook = function (t, n, o, r) {
        var i = o;
        u.libEbooksDirectory.getDirectory(n.path, {
            create: !0
        }, function (o) {
            var a = ph.join(n.path, t);
            u.writeFile(i, a, u.libEbooksDirectory, function () {
                u.unzipViaPlugin(ph.join(u.libEbooksDirectory.nativeURL, a), ph.join(u.libEbooksDirectory.nativeURL, n.path, "EPUB"), function (t) {
                    n.exists = !0, stg.data.downloaded.push(n), u.applyExistingBooks(stg.data.currentLibrary.books), stg.writeSlot("downloaded" + stg.getBookId(!0), stg.data.downloaded, function () {
                        r()
                    })
                })
            })
        })
    };

    u.unzip = function (e, t, n, o) {
        u.unzipBlob(e, t, n, o)
    };

    u.unzipBlob = function (e, t, n, o) {
        zip.workerScriptsPath = "lib/zip/", zip.createReader(new zip.BlobReader(e), function (e) {
            c(e, t, n, o)
        })
    };

    u.unzipViaPlugin = function (e, t, n, o) {
        zip.unzip(e, t, n, o)
    };

    u.unzipURLOld = function (e, t, n, o) {
        zip.workerScriptsPath = "lib/zip/", zip.createReader(new zip.HttpReader(e), function (e) {
            c(e, t, n, o)
        })
    };

    u.unzipData64 = function (e, t, n, o) {
        zip.workerScriptsPath = "lib/zip/", setTimeout(function () {
            zip.createReader(new zip.Data64URIReader(e), function (e) {
                setTimeout(function () {
                    c(e, t, n, o)
                }, 100)
            })
        }, 100)
    };

    u.writeFile = function (e, t, n, o) {
        var r = ph.basename(t);
        try {
            u.getDirectoryRelativeToBooks(ph.normalize(t), n, function (t) {
                try {
                    t.getFile(r, {
                        create: !0
                    }, function (t) {
                        t.createWriter(function (t) {
                            function n() {
                                var a = Math.min(i, e.size - r),
                                    s = e.slice(r, r + a);
                                t.write(s), r += a, t.onwrite = function (t) {
                                    r < e.size ? n() : o && o(!0)
                                }, t.onerror = function (e) {
                                    o && o(!1)
                                }
                            }
                            var r = 0,
                                i = 4194304;
                            n()
                        }, d)
                    }, function (e, t) { })
                } catch (e) { }
            })
        } catch (e) { }
    };

    u.getDirectoryRelativeToBooks = function (e, t, n) {
        t || (t = u.libEbooksDirectory);
        var o = e.split("/");
        if (1 != o.length) {
            var r = o[0];
            t.getDirectory(r, {
                create: !0
            }, function (e) {
                o.shift();
                var t = o.join("/");
                u.getDirectoryRelativeToBooks(t, e, n)
            }, function (e) { })
        } else n(t)
    };

    u.openInNativeBrowser = function (e, t) {
        e = e.toString();
        try {
            if (t) return void window.open(e, "_system");
            try {
                if (cordova.InAppBrowser) {
                    window.plugins.toast.show("  " + translit.get("loadingExternalPage") + "  ", 4e4, "bottom");
                    var n = ios ? "yes" : "no",
                        r = cordova.InAppBrowser.open(e, "_blank", "location=no,hidden=no,clearcache=yes,closebuttoncaption=Go back to App,toolbar=" + n + ",presentationstyle=formsheet");
                    return r.addEventListener("loadstart", function () { }), r.addEventListener("loadstop", function () {
                        ios || window.plugins.toast.hide()
                    }), void r.addEventListener("loaderror", function () {
                        if (!ios) {
                            try {
                                r && r.close()
                            } catch (e) { }
                            window.plugins.toast.hide(), window.plugins.toast.showLongBottom("  " + translit.get("errorLoadingPage") + "  ")
                        }
                    })
                }
            } catch (e) { }
            try {
                if (window.inAppBrowserXwalk) {
                    var i = {
                        toolbarColor: "#FFFFFF",
                        toolbarHeight: "40",
                        closeButtonText: "< Close",
                        closeButtonSize: "25",
                        closeButtonColor: "#000000",
                        openHidden: !1
                    };
                    return void window.inAppBrowserXwalk.open(e, i)
                }
            } catch (e) { }
            return void window.open(e, "_system")
        } catch (e) { }
    };

    u.initAds = function () {
        try {
            if (!admob) return
        } catch (e) {
            return
        }
        config.kotobee.admobId && (admob.setOptions({
            publisherId: config.kotobee.admobId
        }), admob.createBannerView())
    };

    return u;
}]);

app.factory("cssParser", ["$http", "cache", "workers", function ($http, cache, workers) {
    function r(e) {
        function t(e) {
            return e || (e = 1), i[i.length - e]
        }
        e = e.replace(/\r?\n|\r/g, " ");
        for (var n, o = [], r = "key", i = [0], a = o, s = [a], l = 0; l < e.length; l++) n || (n = {}), "}" == e[l] ? ("properties" == r && (n.properties = e.substring(t(), l).trim(), n.keys && n.keys.length && (n.keys[n.keys.length - 1] = n.keys[n.keys.length - 1].trim()), a.push(n)), "key" == r && s.length > 1 && (s.pop(), a = s[s.length - 1]), n = null, r = "key", i.push(l + 1)) : "{" == e[l] ? ("properties" == r && (n.children || (n.children = []), n.keys && n.keys.length && (n.keys[n.keys.length - 1] = n.keys[n.keys.length - 1].trim()), o.push(n), a = n.children, s.push(a), (n = {
            keys: []
        }).keys.push(e.substring(t(), l).trim())), i.push(l + 1), r = "properties") : "key" == r && (n.keys || (n.keys = [""]), "," == e[l] ? n.keys.push("") : n.keys[n.keys.length - 1] += e[l]);
        return o
    }
    var i, a, s = {}, l = [];

    s.clearCache = function () {
        l = []
    };

    s.inject = function (t, n, a) {
        function c(e, t) {
            for (var n = 0; n < l.length; n++)
                if (l[n].url == e && l[n].id == t) return l.push(l[n]), l[n]
        }

        function d(e) {
            l.length > 7 && l.shift(), l.push(e)
        }

        function u(a, l) {
            function g(e, t, i) {
                function l(e) {
                    if (e) {
                        for (var t = e.split(";"), n = "", o = 0; o < t.length; o++) t[o] && -1 != t[o].trim().indexOf("background") && (n = t[o].trim().replace(/url\(.*?\)/g, "") + ";");
                        return n
                    }
                }

                function c(e) {
                    for (var t = {}, n = 0; n < e.length; n++)
                        if (e[n]) {
                            var o = e[n].trim();
                            0 != o.indexOf("body") ? 0 != o.indexOf("html") ? 0 != o.indexOf("@media") && 0 != o.indexOf("@") && isNaN(o[0]) && "from" != o && "to" != o && (e[n] = g + " " + o) : e[n] = o.replace("html", g) : (e[n] = o.replace("body", g), 0 == e[n].indexOf("html") && (e[n] = e[n].replace("html", g)), t.hasBody = !0)
                        }
                    return t.joined = e.join(","), t
                }
                if (workers.supported()) {
                    var u = {};
                    return u.data = e, u.rootPath = t, u.cssOb = a, u.desktop = desktop, void workers.call("cssParser", "parse", [u], function (e) {
                        "url" == i && d({
                            url: m,
                            val: e,
                            id: n.id
                        }), h(e)
                    })
                }
                e = (e = (e = (e = s.applyRoot(t, e)).replace(/\/\*[\s\S]*?\*\//g, "")).replace(/<!--/g, "")).replace(/-->/g, "");
                var f = "spreadL";
                n.id && (f = n.id);
                for (var g = (a.parentClass ? a.parentClass : "#epubContainer.stylesEnabled") + " #" + f, v = r(e), b = "", y = 0; y < v.length; y++) {
                    var k = c(v[y].keys);
                    if (b += k.joined, b += "{", v[y].children && v[y].children.length)
                        for (var w = 0; w < v[y].children.length; w++) {
                            var C = c(v[y].children[w].keys);
                            b += C.joined, b += "{" + v[y].children[w].properties + "}", C.hasBody && (x = l(v[y].properties)) && (b += "#epubContainer{" + x + "}")
                        }
                    if (v[y].properties && (b += v[y].properties), b += "}", k.hasBody) {
                        var x = l(v[y].properties);
                        x && (b += "#epubContainer{" + x + "}")
                    }
                }
                e = p(e = b), "url" == i && d({
                    url: m,
                    val: e,
                    id: n.id
                }), h(e)
            }

            function h(e) {
                e && (f += e), ++i < t.length ? u(t[i], l) : l && l(f)
            }
            if (!a) return a = {}, void h();
            if ("path" == a.type) {
                var m = a.val;
                if (!m || m.indexOf("EPUB/css/kotobeeInteractive.css") >= 0) return void h();
                var v = c(m, n.id);
                v ? h(v.val) : $http.get(a.dontCache ? m : cacheBust(m)).success(function (e) {
                    g(e, m, "url")
                }).error(function () {
                    h()
                })
            } else g(a.val, a.root)
        }

        function p(e) {
            var t = e;
            return t = t.replace(/\/\*(?:(?!\*\/)[\s\S])*\*\/|[\r\n\t]+/g, ""), t = t.replace(/ {2,}/g, " "), t = t.replace(/ ([{:}]) /g, "$1"), t = t.replace(/([;,]) /g, "$1"), t = t.replace(/ !/g, "!")
        }
        n || (n = {});
        var f = "";
        u(t[i = 0], a)
    };

    s.applyRoot = function (e, t) {
        var n = /url\((.*?)\)/g;
        return t.replace(n, function (t, n) {
            var o = "";
            return 0 == n.indexOf('"') ? (n = n.substr(1, n.length - 2), o = '"') : 0 == n.indexOf("'") && (n = n.substr(1, n.length - 2), o = "'"), -1 != n.indexOf("http://") ? arguments[0] : -1 != n.indexOf("https://") ? arguments[0] : 0 == n.indexOf("data:") ? arguments[0] : (n = ph.join(e, n), "url(" + o + n + o + ")")
        })
    };

    s.getStyles = function () {
        return a
    };

    s.replaceStyles = function (e, t) {
        t || (t = {});
        for (var n, o = document.getElementsByTagName("style"), r = 0; r < o.length; r++)
            if (void 0 != o[r].getAttribute("kotobee")) {
                n = o[r];
                break
            }
        a = e, t.append && (a = n.innerHTML + " " + e), n = replaceHTML(n, a)
    };

    s.hideStyles = function () {
        removeClass(cache.getOb("epubContainer"), "stylesEnabled")
    };

    s.showStyles = function () {
        addClass(cache.getOb("epubContainer"), "stylesEnabled")
    };

    s.stylize = function (e, t) {
        for (var n = document.getElementById(e), o = n.getAttribute("style"), r = Object.keys(t), i = o ? o + ";" : "", a = 0; a < r.length; a++) i += r[a] + ":" + t[r[a]] + ";";
        n.setAttribute("style", i)
    };

    return s;
}]);

app.factory("design", ["cssParser", "$interpolate", "$http", function (cssParser, $interpolate, $http) {
    var r, i, a = {};
    a.getBgStyle = function () {
        if (!r) {
            var e = "";
            isKotobeeHosted() && (e = publicRoot), "stretch" == (r = {
                "background-image": 'url("' + (config.splashBg ? e + "imgUser/" + config.splashBg : "none") + '")',
                "background-color": config.splashBgColor ? config.splashBgColor : "#3f3248",
                color: (config.splashColor ? config.splashColor : "#fff") + " !important",
                "background-size": config.splashBgSize ? config.splashBgSize : "cover"
            })["background-size"] && (r["background-size"] = "100% 100%")
        }
        return r
    };

    a.convertTabs = function () {
        if (config.tabs)
            for (var e = 0; e < config.tabs.length; e++) {
                var t = document.createElement("div");
                t.setAttribute("data-kotobee", config.tabs[e]), config.tabs[e] = kInteractive.readData(t)
            }
    };

    a.injectUserCSS = function (e) {
        function n() {
            for (var n = document.getElementsByTagName("style"), o = 0; o < n.length; o++)
                if (n[o].hasAttribute("user")) {
                    n[o].innerHTML = $interpolate(i)(e);
                    break
                }
        }
        i ? n() : $http.get(cacheBust(rootDir + "css/user.css")).success(function (e) {
            i = e, n()
        })
    };

    a.stylize = function (t, n) {
        cssParser.stylize(t, n)
    };

    return a;
}]);

app.factory("error", ["modals", "translit", "$injector", "$location", "$ionicHistory", function (modals, translit, $injector, $location, $ionicHistory) {
    var i = {},
        a = [];
    i.modal = null;

    i.update = function (s) {
        var l, c = $injector.get("backend");
        if (s) switch (s.error) {
            case "s_authError":
                "library" == config.kotobee.mode && c.cloudParam("entry") ? l = {
                    mode: "login",
                    dynamic: !0,
                    animation: "slide-in-up",
                    loginOptions: {}
                } : ($location.path("/login"), $ionicHistory.clearHistory());
                break;
            case "networkError":
                l = {
                    mode: "networkError",
                    errorOptions: {
                        ref: s.ref,
                        retry: i.retry
                    }
                };
                break;
            case "localonlyError":
                l = {
                    mode: "localOnlyError",
                    errorOptions: {
                        ref: s.ref,
                        retry: i.retry
                    }
                };
                break;
            case "errorAndCallback":
                l = {
                    mode: "generalError",
                    errorOptions: {
                        msg: translit.get(s.msg),
                        cb: s.cb
                    }
                };
                break;
            default:
                l = {
                    mode: "generalError",
                    errorOptions: {
                        msg: translit.get(s.error)
                    }
                }
        }
        for (var d = 0; d < a.length; d++) a[d].error == s.error && a[d].func(l);
        l && modals.show(l)
    };

    i.retry = function (t) {
        modals.hide(), timeOut(function () {
            t.func.apply(null, t.args)
        }, 100)
    };

    i.detect = function (e, t) {
        a.push({
            error: e,
            func: t
        })
    };

    i.removeDetection = function (e, t) {
        for (var n = 0; n < a.length; n++)
            if (t == a[n].func && e == a[n].error) return void a.splice(n, 1)
    };

    return i
}]);

app.factory("initializer", ["$rootScope", "$injector", "chapterAPI", "translit", "autohide", "scorm", "workers", "tinCan", "$location", "chapters", "error", "modals", "$ionicHistory", "$ionicPlatform", "crdv", "cache", "auth", "design", "stg", "backend", "settings", function ($rootScope, $injector, chapterAPI, translit, autohide, scorm, workers, tinCan, $location, chapters, error, modals, $ionicHistory, $ionicPlatform, crdv, cache, auth, design, stg, backend, settings) {
    function x() {
        document.onkeydown = function (t) {
            "app" != config.kotobee.mode && document.activeElement && "epubContainer" == document.activeElement.id && (modals.shown() || ("37" == (t = t || window.event).keyCode ? (stg.data.settings.rtl ? chapters.nextChapter() : chapters.prevChapter(), $rootScope.$$phase || $rootScope.$apply()) : "39" == t.keyCode && (stg.data.settings.rtl ? chapters.prevChapter() : chapters.nextChapter(), $rootScope.$$phase || $rootScope.$apply())))
        }
    }

    function I() {
        setTimeout(function () {
            ionic.tap.requiresNativeClick = function (e) {
                return !(!ionic.Platform.isWindowsPhone() || "A" != e.tagName && "BUTTON" != e.tagName && !e.hasAttribute("ng-click") && ("INPUT" != e.tagName || "button" != e.type && "submit" != e.type)) || (!!(!e || e.disabled || /^(file|range)$/i.test(e.type) || /^(object|video|audio)$/i.test(e.tagName) || ionic.tap.isLabelContainingFileInput(e)) || ionic.tap.isElementTapDisabled(e))
            }
        }, 1e3)
    }

    function E() {
        window.addEventListener("message", function (e) {
            var n = e.data.split(":");
            if ("next" == n[0]) chapters.nextChapter();
            else if ("prev" == n[0]) chapters.prevChapter();
            else if ("notebook" == n[0]) angular.element(document.getElementById("tabMenu")).scope().notebookClicked();
            else if ("goto" == n[0]) $location.path($injector.get("library").root + "/reader/chapter/" + Number(n[1]));
            else if ("zoom" == n[0])
                for (var o = document.getElementsByTagName("meta"), r = 0; r < o.length; r++) "viewport" == o[r].getAttribute("name") && o[r].setAttribute("content", "initial-scale=" + n[1])
        }, !1), window.kbOpenChapterURL = function (e) {
            e && (-1 == e.indexOf(":/") && (e = ph.join(stg.data.book.chapter.url, e)), chapters.applyChapterStateFromURL(e))
        }
    }

    function T() {
        for (var e = 0; e < P.length; e++) P[e].apply(this, arguments)
    }

    function O(e) {
        if ("localhost" == location.hostname) return e();
        var t = document.getElementById("iconDetector");
        if (preview && -1 != window.location.href.indexOf("&screenshot")) return t.style.opacity = 1, void (-1 == window.location.href.indexOf("&startup") && e && e());
        S(t, function () {
            $ || ($ = setTimeout(function () {
                t.style.opacity = 1, e && e()
            }, 8e3)), t.offsetWidth < 10 ? setTimeout(function () {
                O(e)
            }, 1e3) : (clearTimeout($), t.style.opacity = 1, setTimeout(function () {
                e && e()
            }, 1e3))
        })
    }

    function S(e, t) {
        e.className = "", setTimeout(function () {
            e.className = "icon fa fa-cog fa-spin size16", setTimeout(t, 400)
        }, 500)
    }

    function A() {
        if (B)
            for (; B.items.length;) B.removeAt(0);
        L()
    }

    function L() {
        function t(e) {
            this.label.toLowerCase() == translit.get("exit").toLowerCase() && r.App.quit()
        }

        function n(e, t, n) {
            return t && (e.click = t), n && (e.submenu = n), new r.MenuItem(e)
        }
        var r = require("nw.gui");
        B || (B = new r.Menu({
            type: "menubar"
        }));
        var i = new r.Menu;
        i.append(n({
            label: translit.get("exit")
        }, t)), B.append(n({
            label: translit.get("file")
        }, t, i));
        for (var a = new r.Menu, s = new r.Menu, l = stg.data.languages, c = 0; c < l.length; c++) s.append(n({
            label: l[c].label
        }, function (t) {
            var n = this.label;
            setTimeout(function () {
                for (var t = 0; t < stg.data.languages.length; t++)
                    if (stg.data.languages[t].label == n)
                        return stg.data.settings.language = stg.data.languages[t].val, settings.langChanged(), void ($rootScope.$$phase || $rootScope.$apply())
            }, 300)
        }));
        a.append(n({
            label: translit.get("language")
        }, t, s)), B.append(n({
            label: translit.get("settings")
        }, t, a)), r.Window.get().menu = B
    }
    var B, N, M = {},
        P = [];
    M.preInitialize = function (e) {
        putTest("preInitialize"), M.initializing = !0, design.injectUserCSS(config);
        var t = M.getLogo();
        M.playAudio(), M.displayBg(), backend.addListener("pathsChanged", M.playAudio), backend.addListener("pathsChanged", M.displayBg);
        for (var s = document.getElementsByTagName("link"), c = 0; c < s.length; c++)
            if ("shortcut icon" == s[c].getAttribute("rel")) {
                s[c].getAttribute("href") || s[c].setAttribute("href", config.icon ? config.icon : t.logo);
                break
            }
        design.convertTabs(), putTest("preInitialize2"), cache.initialize(), tinCan.initialize(), workers.initialize(), stg.initialize(), scorm.initialize(), autohide.initialize(), chapterAPI.initialize(), stg.initializeSettings(function () {
            settings.langChanged(), putTest("preInitialize3"), E(), I(), desktop && L(), native || x(), putTest("preInitialize4"), cache.setOb("title", replaceHTML(cache.getOb("title"), translit.get("loading") + "..")), translit.addListener(function () {
                desktop && A(), N || stg.data.book || stg.data.book.meta || cache.setOb("title", replaceHTML(cache.getOb("title"), translit.get("loading") + "..")), putTest("translit.addListener2"), N = !0
            }), putTest("preInitialize5"), config.kotobee.cloudid && backend.getS3Paths()
        })
    }, M.displayBg = function () {
        design.stylize("home", design.getBgStyle())
    }, M.playAudio = function () {
        if (config.audio && !document.bgAudio) {
            var e = "";
            isKotobeeHosted() && (e = publicRoot);
            var t = e + "imgUser/" + config.audio,
                n = document.createElement("audio");
            n.setAttribute("data-dontclose", !0), n.setAttribute("controls", "true"), n.setAttribute("autoplay", "true"), n.setAttribute("src", t), config.audioLoop && n.setAttribute("loop", "true"), n.setAttribute("data-tap-disabled", "false");
            var o = document.createElement("source");
            o.src = t, n.appendChild(o), n.appendChild(document.createTextNode("Your browser does not support the audio element")), n.oncanplay = function () {
                kInteractive.tinCan({
                    verb: "played",
                    activity: "Audio: " + t
                })
            }, n.play(), document.bgAudio = n
        }
    }, M.getLogo = function () {
        var e = "";
        return isKotobeeHosted() && (e = publicRoot), {
            logo: config.logo ? e + "imgUser/" + config.logo : rootDir + "img/ui/defaultWelcomeLogoNew.png",
            slogan: config.slogan ? config.slogan : ""
        }
    }, M.addListener = function (e) {
        P.push(e)
    }, M.postInitialize = function (t) {
        putTest("postInitialize start"), O(function () {
            putTest("postInitialize start2"), backend.initialize(t).then(function () {
                if (putTest("MM"), t.$$phase || t.$apply(), "library" == config.kotobee.mode || $rootScope.readerApp) {
                    try {
                        stg.readSlot("downloaded" + stg.getBookId(!0), function (e) {
                            stg.data.downloaded = e, putTest("MMMM")
                        })
                    } catch (e) {
                        stg.data.downloaded = []
                    }
                    stg.data.downloaded || (stg.data.downloaded = [])
                } else "book" == config.kotobee.mode ? (bookPath = "epub/", preview && (bookPath = config.kotobee.bookPath), bookPath || (bookPath = "epub/")) : "app" == config.kotobee.mode || (status.update(), error.update({
                    error: "errorAndCallback",
                    msg: "cantReadConfigFile",
                    cb: crdv.exit
                }));
                auth.authenticate(t)
            }), $ionicPlatform.registerBackButtonAction(function (t) {
                var n = $ionicHistory.currentView();
                "forgotPwd" == n.stateId ? $location.path("/login") : "register" == n.stateId ? $location.path("/login") : "library" == n.stateId && $rootScope.readerApp ? $location.path("/app") : modals.show({
                    mode: "exit",
                    backdropClickToClose: !1,
                    exitOptions: {
                        exit: function () {
                            crdv.exit()
                        }
                    }
                }), $rootScope.$apply()
            }, 101), M.initialized = !0, M.initializing = !0, T()
        })
    };
    var $;
    return M
}]);

app.factory("jsParser", ["$http", function ($http) {
    var n, o, r, i = {},
        a = [];
    return i.clearCache = function () {
        a = []
    }, i.inject = function (t, o) {
        function r(e) {
            for (var t = 0; t < a.length; t++)
                if (a[t].url == e) return a.push(a[t]), a[t]
        }

        function i(e) {
            a.length > 5 && a.shift(), a.push(e)
        }

        function s(o, a) {
            function d(e) {
                e && c.push(e), u()
            }

            function u() {
                ++n < t.length ? s(t[n], a) : (l(), a && a(c))
            }
            if (o)
                if ("code" == o.mode) c.push(o.val), u();
                else {
                    var p = r(o.val);
                    p ? d(p.val) : $http.get(cacheBust(o.val)).success(function (e) {
                        i({
                            url: o.val,
                            val: e
                        }), d(e)
                    }).error(function () {
                        d()
                    })
                } else u()
        }

        function l() {
            for (var e = 0; e < c.length; e++)
                if (c[e]) {
                    var t;
                    c[e].indexOf("window.location.href") >= 0 && (t = /window\.location\.href[\s]*?=(.*?)[;\n]/g, c[e] = c[e].replace(t, "try { kbOpenChapterURL($1); } catch(er){};")), c[e].indexOf("event.stopPropagation()") >= 0 && (t = /event\.stopPropagation\(\)/g, c[e] = c[e].replace(t, "void(0)")), c[e].indexOf("<![CDATA[") >= 0 && (t = /<!\[CDATA\[/g, c[e] = c[e].replace(t, "//<![CDATA["), t = /\]\]>/g, c[e] = c[e].replace(t, "//]]>"))
                }
        }
        var c = [];
        s(t[n = 0], o)
    }, i.getScript = function () {
        return o
    }, i.replaceScript = function (e) {
        if (o = e, r && r.length)
            for (t = 0; t < r.length; t++) r[t].parentNode && r[t].parentNode.removeChild(r[t]);
        r = [];
        for (var t = 0; t < e.length; t++) {
            var n = document.createElement("script");
            n.setAttribute("kotobee", ""), n.setAttribute("type", "text/javascript"), n.innerHTML = e[t], r.push(n);
            try {
                document.head.appendChild(n)
            } catch (e) { }
            try {
                document.dispatchEvent(new Event("DOMContentLoaded"))
            } catch (e) { }
        }
    }, i
}]);

app.factory("library", ["stg", "translit", "cache", "backend", "crdv", function (stg, translit, cache, backend, crdv) {
    function a(e) {
        e || (e = {}), d.maxResults = e.large ? 40 : 20
    }

    function s(e) {
        e || (e = {}), d.maxResults += e.large ? 20 : 6
    }

    function l() {
        for (var e = 0; e < u.length; e++) u[e].apply(this, arguments)
    }
    var c, d = {}, u = [];

    d.sortMethod = {}; d.root = "";

    d.initialize = function () {
        d.setBrowserTitle(), a(), u = [], l("initialized")
    };

    d.setMax = function (e) {
        d.maxResults = e
    };

    d.setBrowserTitle = function () {
        cache.setOb("title", replaceHTML(cache.getOb("title"), stg.data.currentLibrary.name ? stg.data.currentLibrary.name : translit.get("library")))
    };

    d.addListener = function (e) {
        u.push(e)
    };

    d.getEbooks = function (t, n) {
        function o(n) {
            try {
                t.favs ? stg.data.favorites = n : stg.data.currentLibrary.books = n
            } catch (e) { }
        }
        t || (t = {}), c = Math.round(1e5 * Math.random()), l("preload", t), t.accumulated || addClass(document.getElementById("libraryThumbs"), "dim"), t.more ? s(t.contentkey ? {
            large: !0
        } : {}) : a(t.contentkey ? {
            large: !0
        } : {}), t.sort = d.sortMethod.data, t.sortDir = d.sortMethod.dir, t.max = d.maxResults, backend.getEbooks(t, c, function (r, a) {
            if (a == c) {
                removeClass(document.getElementById("libraryThumbs"), "dim"), r.error || (crdv.applyExistingBooks(r), o(r));
                var s = t.favs ? stg.data.favorites : stg.data.currentLibrary.books;
                s && s.length || (r.empty = !0), l("postload", t, r), n && n(r)
            }
        })
    };

    return d;
}]);

app.factory("markers", ["cache", "stg", "$location", "$injector", function (cache, stg, $location, $injector) {
    function s() {
        return !($location.path() && $location.path().indexOf("/reader") >= 0)
    }
    var l = {},
        c = new Array,
        d = {};

    l.addMarker = function (e) {
        c.push(e), l.refreshMarker(e)
    };

    l.deleteMarker = function (e) {
        for (var t = 0; t < c.length; t++) c[t].id == e && c.splice(t, 1)
    };

    l.windowResize = function () {
        s() || d.width == window.innerWidth && d.height == window.innerHeight || (l.refreshMarkers(), d.width = window.innerWidth, d.height = window.innerHeight)
    };

    l.refreshMarkers = function () {
        for (var e = 0; e < c.length; e++) l.refreshMarker(c[e])
    };

    l.refreshMarkerById = function (e) {
        for (var t = 0; t < c.length; t++)
            if (c[t].id == e) return void l.refreshMarker(c[t])
    };

    l.getMarker = function (e) {
        for (var t = 0; t < c.length; t++)
            if (c[t].id == e) return c[t]
    };

    l.refreshMarker = function (e) {
        var t = e.target();
        if (t && e.elem) {
            var r = "",
                a = "page";
            (hasClass(e.elem, "contentSideNoteMarker") || hasClass(e.elem, "contentBookMarker")) && (a = "side");
            var s = cache.getOb("epubContainer").getElementsByClassName("scroll")[0],
                l = cache.getOb("epubContent");
            if (s) {
                var c = t.getBoundingClientRect(),
                    d = s.getBoundingClientRect(),
                    u = l.getBoundingClientRect(),
                    p = getElemPadding(cache.getOb("epubContainer")),
                    f = 0,
                    g = 0,
                    h = 1;
                f = s.scrollTop, g = s.scrollLeft;
                var m = $injector.get("nav");
                stg.data.book.chapter.viewport && (h = m.getFxlScale());
                var v, b, y, k = (c.top + f - (d.top + 0)) / h;
                if ("side" == a) {
                    v = 0;
                    var w = Number(p.left) + (u.left - d.left + g),
                        C = d.right - u.right + g;
                    (y = stg.data.book.chapter.viewport ? w : w < C ? w : C) < 30 && (y = 30), stg.data.settings.rtl && (b = 0, v = null), v -= w
                } else {
                    if (0 == e.elem.offsetWidth) return;
                    v = (c.left + g - d.left) / h
                }
                null != k && (r += "top: " + (k + e.offsetTop) + "px; "), null != v && (r += "left: " + (v + e.offsetLeft) + "px; "), null != b && (r += "right: " + (b - e.offsetLeft) + "px; "), null != y && (r += "width: " + y + "px; ");
                var x = 1 / h;
                "" != (r += "transform:translateZ(0) scale(" + x + ");-webkit-transform:translateZ(0) scale(" + x + ");-moz-transform:translateZ(0) scale(" + x + ");") && (e.elem.style.cssText = r), e.callback && e.callback()
            }
        }
    };

    l.checkMarkerExistsAtSameLocation = function (e, t) {
        stg.getNotes(function (n) {
            for (var r = 0; r < n.length; r++) {
                var i = n[r];
                if ("page" == i.type) {
                    var a = document.getElementById("note" + i.nid);
                    if (a && a != e && a.style.top == e.style.top) return t(!0)
                }
            }
            stg.getBookmarks(function (n) {
                for (var o = 0; o < n.length; o++) {
                    var r = n[o],
                        i = document.getElementById("bookmark" + r.bmid);
                    if (i && (i != e && i.style.top == e.style.top)) return t(!0)
                }
                t()
            })
        })
    };

    l.show = function () {
        for (var e = 0; e < c.length; e++) c[e].elem.style.visibility = "shown"
    };

    l.hide = function () {
        for (var e = 0; e < c.length; e++) c[e].elem.style.visibility = "hidden"
    };

    window.addEventListener("resize", l.windowResize, !0);

    return l;
}]);

app.factory("media", ["tinCan", "modals", function (tinCan, modals) {
    var r = {};
    r.show = function (e) {
        tinCan.send({
            verb: "viewed image",
            activity: e.title ? e.title : e.thumbnail
        }), modals.show({
            mode: "image",
            dynamic: !0,
            animation: "slide-in-up",
            image: e
        })
    };

    r.hide = function () {
        modals.hide()
    };

    return r
}]);

app.factory("modals", ["$ionicModal", "$q", "$injector", function ($ionicModal, $q, $injector) {
    function i() {
        if (l = null, u.length) {
            var e = u[0];
            e && ((l = e).hidden ? u.length > 1 && i() : (p.backdropClickToClose = !!e.backdropClickToClose && e.backdropClickToClose, p.animation = e.animation ? e.animation : "zoom-in", f().then(function () {
                s.modal = e, a && (a.show(), s.$$phase || s.$apply(), l.timeLimit && (c = setTimeout(function () {
                    var e = l;
                    d.hide(l);
                    var t = {
                        wait: function () {
                            d.hide(), d.show(e)
                        }
                    };
                    d.show({
                        mode: "waitTimeout",
                        timeoutOptions: t
                    })
                }, l.timeLimit)), setTimeout(function () {
                    e.cb && e.cb(a)
                }, 750))
            })))
        }
    }
    var a, s, l, c, d = {},
        u = [],
        p = {
            animation: "zoom-in",
            focusFirstInput: !1,
            backdropClickToClose: !1
        };
    d.setScope = function (e) {
        function t() {
            for (var e = document.getElementsByClassName("modal-backdrop"), t = e.length; t--;) hasClass(e[t], "hide") && e[t].parentNode.removeChild(e[t])
        } (s = e).hideModal = d.hide, a || f(), s.$on("modal.hidden", function (e) {
            c && clearTimeout(c), setTimeout(function () {
                u.shift(), i(), t()
            }, 500)
        })
    };

    var f = function () {
        var n = $q.defer(),
            o = clone(p);
        return o.scope = s, $ionicModal.fromTemplateUrl(kTemplate("modals/modals.html"), o).then(function (e) {
            a = e, n.resolve()
        }), n.promise
    };

    d.show = function (e) {
        if (a) return u.push(e), 1 == u.length && i(), e;
        f().then(function () {
            d.show(e)
        })
    };

    d.obShown = function (e) {
        return l == e
    };

    d.shown = function () {
        return !!a && a.isShown()
    };

    d.hide = function (e, t) {
        a && (e ? l == e ? (a.hide(), native && $injector.get("crdv").hideKeyboard()) : e.hidden = !0 : (a.hide(), native && $injector.get("crdv").hideKeyboard()), t && t())
    };

    return d;
}]);

app.factory("nav", ["cache", "crdv", "$ionicGesture", "$location", "$rootScope", "selection", "markers", "settings", "chapters", "$ionicScrollDelegate", "stg", function (cache, crdv, $ionicGesture, $location, $rootScope, selection, markers, settings, chapters, $ionicScrollDelegate, stg) {
    function f(e, t) {
        for (var n = 0; n < M.length; n++) M[n].event == e && M[n].func(t)
    }

    function g(t) {
        if ("newChapter" == t) {
            var n = cache.getOb("epubContainer"),
                o = cache.getOb("epubContent");
            if (P = $ionicScrollDelegate.$getByHandle("content"), mobile) {
                var r = P.getScrollView().options;
                if (r.scrollbarFadeDelay = 0, r.scrollingY = !0, r.minZoom = .2, r.maxZoom = 3.5, r.zooming = !0, stg.data.book.chapter.viewport) {
                    r.deceleration = .85, r.scrollingX = !0, r.scrollbarY = !1, P.scrollTo(0, 0, !1);
                    try {
                        A.getFxlFit(null, "from setChapterLayout").bestFit
                    } catch (e) { }
                    A.fxlZoomFit({
                        noAnim: !0,
                        immediateScrollRefresh: !0
                    })
                } else n.style.marginLeft = null, n.style.paddingLeft = null, n.style.paddingTop = null, P.scrollTo(0, 0, !1), r.zooming && P.zoomTo(1, !1, 0, 0), r.deceleration = .95, r.scrollbarY = !0, config.textSizeControls || (r.zooming = !1), setTimeout(function () {
                    r.scrollingX = !1, config.textSizeControls || (r.zooming = !1)
                }, 500);
                setTimeout(markers.refreshMarkers, 1e3)
            } else {
                P.scrollTop(!1);
                if (stg.data.book.chapter.viewport) P.scrollTo(0, 0, !1), A.fxlZoomFit({
                    noAnim: !0,
                    immediateScrollRefresh: !0
                });
                else {
                    n.style.marginLeft = null, n.style.paddingLeft = null, n.style.paddingTop = null, P.scrollTo(0, 0, !1), w(1, {
                        noAnim: !0
                    }), deleteTransform(o), deleteTransformOrigin(o);
                    var i = document.getElementById("spreadR");
                    i && i.parentNode.removeChild(i)
                }
            }
            var a = n.getElementsByClassName("scroll");
            a.length && (removeClass(a[0], "afterDelay"), setTimeout(function () {
                addClass(a[0], "afterDelay")
            }, 2e3))
        }
    }

    function h() {
        if (!config.hideTOCAtWidescreen) {
            var e = N.width >= 1300;
            $rootScope.widescreen != e && (f("widescreenChanged"), $rootScope.widescreen = e), e ? addClass(document.body, "widescreen") : removeClass(document.body, "widescreen"), $rootScope.$$phase || $rootScope.$apply()
        }
    }

    function m(t) {
        if (mobile) {
            var n = cache.getOb("epubContainer");
            "margin" == t ? (n.style.paddingLeft && (n.style.setProperty("margin-left", n.style.paddingLeft, "important"), n.style.paddingLeft = null), n.style.paddingTop && (n.style.setProperty("margin-top", n.style.paddingTop, "important"), n.style.paddingTop = null)) : (n.style.marginLeft && (n.style.setProperty("padding-left", n.style.marginLeft, "important"), n.style.marginLeft = null), n.style.marginTop && (n.style.setProperty("padding-top", n.style.marginTop, "important"), n.style.marginTop = null))
        }
    }

    function v(e, t) {
        if (mobile) {
            e || (e = A.getFxlScale());
            var n = A.getFxlFit(null, "from fxlRefreshHorizontalVerticalScrolling"),
                o = P.getScrollView().options;
            e * n.widthFit <= n.widthFit ? o.scrollingX = !1 : o.scrollingX = !0, e * n.widthFit <= n.heightFit ? o.scrollingY = !1 : o.scrollingY = !0
        }
    }

    function b(e) {
        if (!(O() || mobile && x())) {
            var t = stg.data.book.chapter.viewport ? 1 : .8,
                n = stg.data.book.chapter.viewport ? 100 : 70;
            if (!(Math.abs(e.gesture.velocityX) < t || Math.abs(e.gesture.distance) < n || e.gesture.touches.length > 1)) return !0
        }
    }

    function y() {
        return !($location.path() && $location.path().indexOf("/reader") >= 0)
    }

    function k(t) {
        t || (t = cache.getOb("epubContainer").getElementsByClassName("scroll")).length && (t = t[0]), t && deleteTransformOrigin(t)
    }

    function w(t, n) {
        n || (n = {});
        n.noAnim;
        var o = cache.getOb("epubContainer").children[0];
        I(o) != "scale(" + t + ")" && (E(o, "scale(" + t + ")"), T(o, t > 1 ? "none" : null), n.noAnim && (o.style.transition = "initial"), setTimeout(function () {
            n.noAnim && (o.style.transition = null), redrawForChrome(document.body)
        }, 500), setTimeout(markers.refreshMarkers, 1e3), t > 1.1 ? addClass(cache.getOb("epubContainer"), "zoomed") : removeClass(cache.getOb("epubContainer"), "zoomed"))
    }

    function C() {
        if (document.fullscreenElement || document.webkitFullscreenElement || document.mozFullscreenElement || document.msFullscreenElement) return document.exitFullscreen ? document.exitFullscreen() : document.webkitExitFullscreen ? document.webkitExitFullscreen() : document.mozExitFullscreen ? document.mozExitFullscreen() : document.msExitFullscreen && document.msExitFullscreen(), !0
    }

    function x() {
        var e = document.getElementById("readerBody").style.transform || document.getElementById("readerBody").style.webkitTransform;
        if (e && "translate3d(0px, 0px, 0px)" != e && "translate3d(0px, 0, 0)" != e) return !0
    }

    function I(e) {
        return e.style.webkitTransform || e.style.mozTransform || e.style.transform
    }

    function E(e, t) {
        autoprefixTransform(e, t)
    }

    function T(e, t) {
        e.style.perspective = e.style.msPerspective = e.style.webkitPerspective = e.style.mozPerspective = t
    }

    function O() {
        return B > L
    }
    var S, A = {},
        L = 15,
        B = 0,
        N = {},
        M = [];

    A.init = function (e) {
        mobile && ($ionicGesture.on("swipeleft", A.swipeLeft, e), $ionicGesture.on("swiperight", A.swipeRight, e)), $ionicGesture.on("dragstart", A.dragStart, e), $ionicGesture.on("drag", A.drag, e), $ionicGesture.on("tap", A.tap, e), $ionicGesture.on("doubletap", A.fxlToggleTapZoom, e), $ionicGesture.on("transformstart", A.transformstart, e), $ionicGesture.on("transformend", A.transformend, e), S || (window.addEventListener("resize", A.windowResize, !0), N.width = window.innerWidth, N.height = window.innerHeight, mobile || h(), chapters.addListener(g), S = !0, A.addListener("widescreenChanged", function () {
            setTimeout(A.fxlZoomFit, 400)
        }), document.body.addEventListener("keyup", function (e) {
            27 == e.keyCode && C()
        }))
    };

    A.addListener = function (e, t) {
        M.push({
            event: e,
            func: t
        })
    };

    var P;
    A.windowResize = function () {
        y() || N.width == window.innerWidth && N.height == window.innerHeight || (A.fxlZoomFit(), N.width = window.innerWidth, N.height = window.innerHeight, mobile || h(), chapters.regenerateSpreads({
            resize: !0
        }))
    };

    A.getScroller = function () {
        return P
    };

    var $, H;

    A.transformstart = function () {
        A.transforming = !0, stg.data.book.chapter.viewport && (k(), $ = (new Date).getTime(), H = A.getFxlScale(), m("margin"))
    };

    A.transformend = function () {
        if (A.transforming = !1, !y())
            if (stg.data.book.chapter.viewport) {
                var t = (new Date).getTime(),
                    n = A.getFxlScale();
                if (mobile && n < H && t - $ < 800) return void A.fxlZoomFit();
                var o = A.getFxlFit(null, "from transformend").bestFit;
                n < o ? ($ionicScrollDelegate.$getByHandle("content").zoomTo(o, !0), n = o) : n > 3 && ($ionicScrollDelegate.$getByHandle("content").zoomTo(3, !0), n = o), v(3), setTimeout(markers.refreshMarkers, 1e3), A.repaint()
            } else {
                if (!config.textSizeControls) return;
                var r = P.getScrollPosition().zoom,
                    i = settings.getTextSize(),
                    a = Math.round(r * i * 10) / 10;
                a < .2 && (a = .2), stg.data.settings.textSize = a + "em", settings.updateTextSize(), settings.writeTextSize(), P.zoomTo(1, !0);
                var c = angular.element(cache.getOb("epubContent")).scope();
                c.header.mode = "textSize", c.$$phase || c.$apply(), setTimeout(markers.refreshMarkers, 1e3)
            }
    };

    A.repaint = function () {
        redrawForChrome(cache.getOb("epubContent"))
    };

    A.resize = function () {
        P.resize()
    };

    A.swipeRight = function (e) {
        y() || b(e) && (P.resize(), native && crdv.vibrate(), stg.data.settings.rtl ? chapters.nextChapter() : chapters.prevChapter())
    };

    A.swipeLeft = function (e) {
        y() || b(e) && (P.resize(), native && crdv.vibrate(), stg.data.settings.rtl ? chapters.prevChapter() : chapters.nextChapter())
    };

    A.dragStart = function () {
        y() || (stg.data.book.chapter.viewport || P.zoomTo(1, !1), B = 0, P.freezeScroll(!1))
    };

    A.drag = function (e) {
        B++
    };

    A.tap = function (e) {
        f("tap", e)
    };

    A.fxlToggleTapZoom = function (t) {
        if (cache.getOb("epubContent") && stg.data.book.chapter.viewport && stg.data.book.chapter.viewport.width) {
            var n = A.getFxlScale(),
                o = A.getFxlFit(null, "from fxlToggleTapZoom").bestFit,
                r = P.getScrollView().options.minZoom;
            if (null == r && (r = n - 1), Math.round(100 * n) != Math.round(100 * o) && n > r) A.fxlZoomFit(t);
            else {
                var i = 2 * A.getFxlFit(null, "from fxlToggleTapZoom").bestFit;
                mobile ? $ionicScrollDelegate.$getByHandle("content").zoomTo(i, !0, t.gesture.center.pageX, t.gesture.center.pageY) : w(i), selection.reset(), m("margin"), v(i)
            }
            setTimeout(markers.refreshMarkers, 1e3), setTimeout(A.repaint, 1e3)
        }
    };

    A.fxlZoomIn = function (t) {
        selection.reset();
        var n = 1.2 * A.getFxlScale(),
            o = A.getFxlFit(null, "from fxlZoomIn");
        o.bestFit;
        n > 3 && (n = 3);
        var r = $ionicScrollDelegate.$getByHandle("content");
        mobile ? r.zoomTo(n, !0) : w(n), m("margin");
        var i = cache.getOb("epubContent"),
            l = document.getElementById("readerBody");
        if (n >= 1);
        else if ("vertical" == o.bestFitType) {
            var c = getElemMargin(i).x,
                d = getElemPadding(i).x;
            "auto" == c && (c = 0), "auto" == d && (d = 0), c || (c = 0), d || (d = 0);
            c = (l.getBoundingClientRect().width - d - stg.data.book.chapter.viewport * n) / 2;
            i.style.setProperty("margin-left", Math.round(c / n) + "px", "important"), i.style.setProperty("margin-right", Math.round(c / n) + "px", "important")
        } else i.style.marginLeft && (i.style.marginLeft = i.style.marginRight = null);
        v(n), setTimeout(markers.refreshMarkers, 1e3)
    };

    A.fxlZoomOut = function (e) {
        selection.reset();
        var t = .8 * A.getFxlScale(),
            n = A.getFxlFit(null, "from fxlZoomOut").bestFit;
        t < n && (t = n), mobile ? $ionicScrollDelegate.$getByHandle("content").zoomTo(t, !0) : w(t), m("margin"), v(t), setTimeout(markers.refreshMarkers, 1e3)
    };

    A.fxlZoomFit = function (t) {
        if (t || (t = {}), cache.getOb("epubContent") && stg.data.book.chapter.viewport && stg.data.book.chapter.viewport.width) {
            selection.reset();
            var n = A.getFxlFit(null, "from fxlZoomFit"),
                o = t.noAnim;
            mobile ? P.zoomTo(n.bestFit, !o) : w(n.bestFit, {
                noAnim: o
            }), A.fxlAdjustAlignment(), m("padding"), v(n.bestFit), setTimeout(markers.refreshMarkers, 1e3)
        }
    };

    A.fxlAdjustAlignment = function (t) {
        t || (t = {}), t.epubContent || (t.epubContent = cache.getOb("epubContent")), t.epubContainer || (t.epubContainer = cache.getOb("epubContainer"));
        var n = A.getFxlFit(t, "from fxlAdjustAlignment"),
            o = getElemMargin(t.epubContent, {
                computed: !0
            });
        "auto" == o.left && (o.left = 0), "auto" == o.top && (o.top = 0), autoprefixTransform(t.epubContent, "scale(" + n.widthFit + ")"), autoprefixTransformOrigin(t.epubContent, -o.left + "px " + -o.top + "px"), t.epubContainer.style.marginLeft = t.epubContainer.style.marginRight = null, t.epubContainer.style.paddingLeft = t.epubContainer.style.paddingRight = null, t.epubContainer.style.setProperty(stg.data.settings.rtl ? "padding-right" : "padding-left", n.marginLeft + "px", "important"), t.epubContainer.style.setProperty("padding-top", n.marginTop + "px", "important")
    };

    A.getSpreadHeight = function (e) {
        for (var t = 0, n = 0; n < e.children.length; n++) {
            var o = e.children[n];
            o.id && 0 == o.id.indexOf("spread") && (t = Math.max(t, Number(o.style.height.split("px")[0])))
        }
        return t
    };

    A.getSpreadWidth = function (e) {
        for (var t = 0, n = 0; n < e.children.length; n++) {
            var o = e.children[n];
            o.id && 0 == o.id.indexOf("spread") && (t += Number(o.style.width.split("px")[0]))
        }
        return t
    };

    A.viewFromElem = function (e) {
        var t = {};
        t.maxHeight = 0, t.totalWidth = 0;
        for (var n = 0; n < e.children.length; n++) {
            var o = e.children[n];
            if (o.id && 0 == o.id.indexOf("spread")) {
                var r = Number(o.style.height.split("px")[0]),
                    i = Number(o.style.width.split("px")[0]),
                    a = {
                        width: i,
                        height: r
                    };
                "spreadL" == o.id ? (t.leftOb || (t.leftOb = {}), t.leftOb.viewport = a, t.leftOb.layout = hasClass(o, "fxl") ? "fxl" : "rfl", t.layout = t.leftOb.layout) : "spreadR" == o.id && (t.view = "double", t.rightOb || (t.rightOb = {}), t.rightOb.left = Number(o.style.left.split("px")[0]), t.rightOb.viewport = a, t.rightOb.layout = hasClass(o, "fxl") ? "fxl" : "rfl"), t.maxHeight = Math.max(r, t.maxHeight), t.totalWidth += i
            }
        }
        return t.leftOb && t.rightOb && t.rightOb.left && (t.totalWidth += t.rightOb.left - t.leftOb.viewport.width), t
    };

    A.getFxlFit = function (t, n) {
        t || (t = {}), t.viewport || (t.viewport = stg.data.book.chapter.viewport);
        var o = {};
        t.epubContent || (t.epubContent = cache.getOb("epubContent"));
        var r = t.view;
        r || (r = stg.data.currentView);
        var i = {
            x: 0
        };
        t.epubContainer || (t.epubContainer = cache.getOb("epubContainer"));
        var a = t.epubContainer.getElementsByClassName("scroll");
        a.length && (i = getElemPadding(a[0], {
            computed: !0
        }), o.scrollContainer = a[0]), "auto" == i.x && (i.x = 0), "auto" == i.y && (i.y = 0);
        var s = getElemMargin(t.epubContent, {
            computed: !0
        }),
            l = getElemPadding(t.epubContent, {
                computed: !0
            }),
            c = s.x,
            d = s.y;
        "auto" == c && (c = 0), "auto" == d && (d = 0);
        var u = l.x,
            f = l.y;
        "auto" == u && (u = 0), "auto" == f && (f = 0);
        var g = t.viewport.width + c,
            h = t.viewport.height + d;
        if ("double" == r) {
            var m = A.viewFromElem(t.epubContent);
            g = m.totalWidth, h = m.maxHeight
        }
        var v = document.getElementById("readerBody"),
            b = document.getElementById("readerHeader"),
            y = document.getElementById("tabMenu"),
            k = v.getBoundingClientRect().width,
            w = v.getBoundingClientRect().height;
        mobile && "alwaysShow" != config.autohideReaderMode && "showOnScroll" != config.autohideReaderMode && "showOnTap" != config.autohideReaderMode || (w -= b.getBoundingClientRect().height + y.getBoundingClientRect().height), t.includeScrollPadding || (k -= i.x, w -= i.y);
        var C = k / g,
            x = w / h;
        if (o.readerWidth = k, o.readerHeight = w, o.widthFit = C, o.heightFit = x, config.fitToScreen) o.bestFit = x < C ? x : C, o.bestFitType = x < C ? "vertical" : "horizontal";
        else {
            var I = C;
            I >= 1 && (I = 1);
            var E = x / I;
            o.bestFitType = "vertical", E < 1 && E >= .7 && (I = x, o.bestFitType = "vertical"), o.bestFit = I
        }
        return o.bestFit = o.bestFit / o.widthFit, o.innerWidth = g, o.innerHeight = h, o.outerWidth = k, o.outerHeight = w, o.marginLeft = Math.round((o.outerWidth - o.widthFit * o.bestFit * o.innerWidth) / 2), o.marginTop = Math.round((o.outerHeight - o.widthFit * o.bestFit * o.innerHeight) / 2), o.marginTop < 0 && (o.marginTop = 0), o
    };

    A.getFxlScale = function () {
        if (mobile) return $ionicScrollDelegate.$getByHandle("content").getScrollPosition().zoom;
        var t = I(cache.getOb("epubContainer").children[0]);
        if (!t) return 1;
        if ("" == t) return 1;
        var n = t.split("scale");
        if (n.length <= 1) return 1;
        var o = n[1].split("(");
        if (o.length <= 1) return 1;
        var r = o[1].split(")");
        return Number(r[0])
    };

    A.fullscreen = function () {
        var e = document.body;
        C() || (e.requestFullscreen ? e.requestFullscreen() : e.webkitRequestFullscreen ? e.webkitRequestFullscreen() : e.mozRequestFullScreen ? e.mozRequestFullScreen() : e.msRequestFullscreen && e.msRequestFullscreen())
    };

    return A
}]);

app.factory("np", ["stg", "$rootScope", "crdv", "modals", function (stg, $rootScope, crdv, modals) {
    var c, d, u, p, f, g, h, m, v, b, y, k, w, C = {};

    C.init = function (e) {
        desktop ? (p = require("nw.gui"), f = require("child_process").exec, g = p.Window.get(), h = process.cwd(), v = (process.env.HOME || process.env.USERPROFILE) + "/Desktop", m = require("path").dirname(process.execPath), w = m + "/node_modules/", "mac" == os && (w = ""), $rootScope.readerApp && (w = ""), w = "", c = require(w + "fs.extra"), d = require(w + "async"), u = require(w + "buckle"), b = p.App.dataPath, y = b + "/temp", k = b + "/Books", C.createIfNotExists(k), C.createIfNotExists(y), g.removeAllListeners(), g.on("close", function () {
            C.exit()
        }), e && e()) : e()
    };

    C.createIfNotExists = function (e, t) {
        desktop && c.exists(e, function (n) {
            n ? t && t() : C.createIfNotExists(ph.parent(e), function () {
                c.mkdirSync(e), t && t()
            })
        })
    };

    C.createIfNoParentsExists = function (e, t) {
        desktop && c.exists(e, function (n) {
            n ? t && t() : C.createIfNoParentsExists(ph.parent(e), function () {
                c.mkdirSync(e), t && t()
            })
        })
    };

    C.writeEbook = function (t, n, o, r) {
        desktop && C.createTempDir(function (t) {
            var a = new FileReader;
            a.onload = function () {
                var o = ph.join(t, "book.zip");
                "mac" == os && (o = t + "/book.zip"), c.writeFileSync(o, Buffer.from(new Uint8Array(a.result)));
                var s = ph.join(k, n.path);
                "mac" == os && (s = k + "/" + n.path), C.extractZip(o, s, function () {
                    n.exists = !0, stg.data.downloaded.push(n), crdv.applyExistingBooks(stg.data.currentLibrary.books), stg.writeSlot("downloaded" + stg.getBookId(!0), stg.data.downloaded, function () {
                        C.removeDirectory(t, function () {
                            r && r()
                        })
                    })
                })
            }, a.readAsArrayBuffer(o)
        })
    };

    C.deleteEbook = function (t, n) {
        C.removeDirectory(ph.join(k, t.path), function () {
            for (var o = 0; o < stg.data.downloaded.length; o++) stg.data.downloaded[o].path == t.path && (t.exists = !1, stg.data.downloaded.splice(o, 1), o--);
            stg.writeSlot("downloaded" + stg.getBookId(!0), stg.data.downloaded, function () {
                n && n()
            })
        })
    };

    C.fileExists = function (e, t) {
        c.exists(e, function (e) {
            t(e)
        })
    };

    C.removeFile = function (e, t) {
        C.removeDirectory(e, t)
    };

    C.removeDirectory = function (e, t) {
        c.rmrf(e, function (e) {
            t && t(e)
        })
    };

    C.extractZip = function (e, t, n) {
        u.open(e, t, function () {
            c.readdir(t, n)
        })
    };

    C.createTempDir = function (e) {
        var t = y + "/f" + randStr(5);
        C.fileExists(t, function (n) {
            n ? C.createTempDir(e) : c.mkdir(t, function () {
                e(t)
            })
        })
    };

    C.openLink = function (e) {
        p.Shell.openExternal(e)
    };

    C.getBooksPath = function () {
        return k + "/"
    };

    C.writeArrayBuffer = function (e, t, n, o) {
        var r = arrayBufferToBuffer(e);
        c.open(t, "w", function (e, t) {
            if (e) throw "error opening file: " + e;
            c.write(t, r, 0, r.length, null, function (e) {
                e ? o && o(!1) : c.close(t, function () {
                    o && o(!0)
                })
            })
        })
    };

    C.copy = function (e, t, n, o) {
        if (n || (n = {}), n.replace)
            for (var r = 0; r < n.replace.length; r++) e = e.replace(n.replace[r].key, n.replace[r].val);
        n.header && (e = n.header + e), n.footer && (e += n.footer), n.encoding || (n.encoding = "ascii"), e = (e += "").replace(/\xA0/g, " "), e = utf8Encode(e), c.writeFile(t, e, n.encoding, function (t, n) {
            o && o(e)
        })
    };

    C.exit = function () {
        stg.getActions(function (e) {
            if (e && e.length) {
                modals.hide();
                var t = {
                    title: "confirmation",
                    msg: "annotationsSyncExitWarning",
                    barStyle: "bar-dark"
                },
                    n = {
                        label: "delete",
                        style: "button-assertive"
                    };
                n.func = function () {
                    p.App.quit()
                };
                var o = {
                    label: "cancel"
                };
                o.func = function () {
                    modals.hide()
                }, t.btns = [n, o], modals.show({
                    mode: "confirm",
                    confirmOptions: t
                })
            } else p.App.quit()
        })
    };

    return C;
}]);

app.factory("popovers", ["$ionicPopover", "$q", "$rootScope", function ($ionicPopover, $q, $rootScope) {
    function o() {
        if (0 != s.length) {
            var e = s.shift();
            e.apply ? i.$apply(function () {
                i.popover = e
            }) : i.popover = e, r && (r.isShown() || r.show(e.event))
        }
    }
    var r, i, a = {},
        s = [];

    a.setScope = function (e) {
        i = e, r || l()
    };

    var l = function () {
        var o = $q.defer();
        return i || (i = $rootScope), $ionicPopover.fromTemplateUrl(kTemplate("modals/popovers.html"), {
            scope: i
        }).then(function (e) {
            r = e, o.resolve()
        }), o.promise
    };

    a.show = function (e) {
        r ? (s.push(e), 1 == s.length && o()) : l().then(function () {
            a.show(e)
        })
    };

    a.shown = function () {
        return !!r && r.isShown()
    };

    a.hide = function () {
        r && (i.popover = null, r && r.hide(), o())
    };

    return a;
}]);

app.factory("scorm", ["scormWrapper", function (scormWrapper) {
    function t(e) {
        var t = e.getSeconds();
        t < 10 && (t = "0" + t);
        var n = e.getMinutes();
        n < 10 && (n = "0" + n);
        var o = e.getHours();
        return o < 10 && (o = "0" + o), o + ":" + n + ":" + t
    }

    function n(e) {
        var t = e.toISOString().split(".");
        return t.splice(t.length - 1, 1), t.join(".")
    }

    function o(e) {
        var t = Math.floor(e / 3600);
        e -= 60 * t * 60;
        var n = Math.floor(e / 60);
        return (e -= 60 * n) < 10 && (e = "0" + e), t < 10 && (t = "0" + t), n < 10 && (n = "0" + n), t + ":" + n + ":" + e
    }

    function r(e) {
        var t = e,
            n = Math.floor(t / 31104e3);
        t = e - 31104e3 * n;
        var o = Math.floor(t / 2592e3);
        t = e - 2592e3 * o;
        var r = Math.floor(t / 86400);
        t = e - 86400 * r;
        var i = Math.floor(t / 3600);
        t = e - 3600 * i;
        var a = Math.floor(t / 60);
        return "P" + n + "Y" + o + "M" + r + "DT" + i + "H" + a + "M" + (t = e - 60 * a) + "S"
    }

    function i() {
        if (p.length) {
            for (var e = Number(l("cmi.objectives._count")), t = {}, n = 0; n < p.length; n++) t[p[n].qid] = p[n];
            for (n = 0; n < e; n++) {
                var o = l("cmi.objectives." + n + ".id");
                if (o) {
                    i = 0;
                    for (var r in t) t[r].objective == o && (i += t[r].scoreMax);
                    g.setObjective({
                        id: o,
                        scoreMax: i
                    }, !0), g.setObjective({
                        id: o,
                        scoreRaw: 0
                    }, !0)
                }
            }
            var i = 0;
            for (var r in t) i += t[r].scoreMax;
            c(1.2 == f ? "cmi.core.score.max" : "cmi.score.max", i), c(1.2 == f ? "cmi.core.score.min" : "cmi.score.min", 0), c(1.2 == f ? "cmi.core.score.raw" : "cmi.score.raw", 0);
            for (var a = 0; a < p.length; a++) {
                for (var d = p[a], u = null, h = null, n = 0; n < e; n++) d.objective == s("cmi.objectives", n, "id") && (u = d.objective, h = n);
                if (u) {
                    var m = Number(l("cmi.objectives." + h + ".score.raw"));
                    m || (m = 0);
                    var v = {};
                    v.id = u, v.scoreRaw = d.result * d.weighting + m, v.scoreMin = 0, v.scoreScaled = v.scoreRaw / v.scoreMax, v.completionStatus = "unknown", v.successStatus = "unknown", g.setObjective(v, !0)
                }
                var b = Number(l(1.2 == f ? "cmi.core.score.raw" : "cmi.score.raw"));
                b || (b = 0), c(1.2 == f ? "cmi.core.score.raw" : "cmi.score.raw", d.result * d.weighting + b)
            }
        }
    }

    function a() {
        scormWrapper.doLMSCommit("")
    }

    function s(e, t, n) {
        var o = l(e + "." + t + "." + n);
        return o && "" != o || (o = l(e + "_" + t + "." + n)), o
    }

    function l(t) {
        return scormWrapper.doLMSGetValue(t)
    }

    function c(t, n) {
        return scormWrapper.doLMSSetValue(t, n)
    }

    function d() {
        try {
            var e = l("cmi.suspend_data");
            return JSON.parse(e)
        } catch (e) {
            return []
        }
    }

    function u(e) {
        try {
            c("cmi.suspend_data", JSON.stringify(e))
        } catch (e) { }
    }
    var p, f, g = {};
    return g.initialize = function () {
        if (!(config.v < 1.2)) {
            kInteractive.scorm = this;
            try {
                if (!scormEnabled) return
            } catch (e) {
                return
            }
            scormWrapper.doLMSInitialize(""), 1.2 == (f = scormWrapper.getAPIVersion()) ? "not attempted" == l("cmi.core.lesson_status") && (c("cmi.core.lesson_status", "browsed"), a()) : "not attempted" == l("cmi.completion_status") && (c("cmi.completion_status", "unknown"), a()), p = d();
            try {
                for (var t = 0; t < config.scorm.objectives.length; t++) {
                    var n = Number(l("cmi.objectives._count"));
                    n || (n = 0);
                    for (var o = n, r = config.scorm.objectives[t], i = 0; i < n; i++) s("cmi.objectives", i, "id") == r.id && (o = i);
                    c("cmi.objectives." + o + ".id", r.id), 1.2 != f && c("cmi.objectives." + o + ".description", r.name)
                }
            } catch (e) { }
            a()
        }
    }, g.setBookmark = function (e, t) {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        c(1.2 == f ? "cmi.core.lesson_location" : "cmi.location", e), t || a()
    }, g.getBookmark = function () {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        return l(1.2 == f ? "cmi.core.lesson_location" : "cmi.location")
    }, g.setInteractions = function (e) {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        for (var t = 0; t < e.length; t++) {
            for (var n = p.length, o = 0; o < p.length; o++) p[o].id == e[t].id && (n = o);
            p[n] = e[t], g.setInteraction(n, e[t], !0)
        }
        i(), u(p), a()
    }, g.setInteraction = function (e, i, s) {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        i.id && c("cmi.interactions." + e + ".id", i.id), i.result && c("cmi.interactions." + e + ".result", i.result), i.correctResponses && c("cmi.interactions." + e + ".correct_responses.0.pattern", i.correctResponses), i.type && c("cmi.interactions." + e + ".type", i.type), i.weighting && c("cmi.interactions." + e + ".weighting", i.weighting), i.objective && c("cmi.interactions." + e + ".objectives.0.id", i.objective), 1.2 == f ? (i.timestamp && c("cmi.interactions." + e + ".time", t(i.timestamp)), i.latency && c("cmi.interactions." + e + ".latency", o(i.latency)), i.learnerResponses && c("cmi.interactions." + e + ".student_response", i.learnerResponses)) : (i.timestamp && c("cmi.interactions." + e + ".timestamp", n(i.timestamp)), i.latency && c("cmi.interactions." + e + ".latency", r(i.latency)), i.learnerResponses && c("cmi.interactions." + e + ".learner_responses", i.learnerResponses), i.description && c("cmi.interactions." + e + ".description", i.description)), s || a()
    }, g.setObjective = function (e, t) {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        for (var n = Number(l("cmi.objectives._count")), o = n, r = 0; r < n; r++) s("cmi.objectives", r, "id") == e.id && (o = r);
        null != e.id && c("cmi.objectives." + o + ".id", e.id), null != e.scoreRaw && c("cmi.objectives." + o + ".score.raw", e.scoreRaw), null != e.scoreMin && c("cmi.objectives." + o + ".score.min", e.scoreMin), null != e.scoreMax && c("cmi.objectives." + o + ".score.max", e.scoreMax), 1.2 == f ? null != e.successStatus && c("cmi.objectives." + o + ".status", e.successStatus) : (null != e.description && c("cmi.objectives." + o + ".description", e.description), null != e.successStatus && c("cmi.objectives." + o + ".success_status", e.successStatus), null != e.completionStatus && c("cmi.objectives." + o + ".completion_status", e.completionStatus), null != e.scoreScaled && c("cmi.objectives." + o + ".score.scaled", e.scoreScaled)), t || a()
    }, g.interactionExistsOld = function (e) {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        for (var t = Number(l("cmi.interactions._count")), n = 0; n < t; n++)
            if (s("cmi.interactions", n, "id") == e) return !0
    }, g.interactionExists = function (e) {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        for (var t = 0; t < p.length; t++)
            if (p[t].id == e) return !0
    }, g.objectiveExists = function (e) {
        try {
            if (!scormEnabled) return
        } catch (e) {
            return
        }
        for (var t = Number(l("cmi.objectives._count")), n = 0; n < t; n++)
            if (s("cmi.objectives", n, "id") == e) return !0
    }, g
}]);

app.factory("search", ["$rootScope", "modals", "status", "chapters", "cache", "$filter", "translit", "virt", "selection", "stg", function ($rootScope, modals, status, chapters, cache, $filter, translit, virt, selection, stg) {
    function h(e, t, n, o) {
        function r(e) {
            for (var o = e, r = selection.convertElemsToLocation([o], t).left, i = '<span class="searchItem">' + o.textContent + "</span>", a = (i.toLowerCase().indexOf(y.key.toLowerCase()), i.length + Math.round((70 - i.length) / 2)); ;) {
                if (!o.previousSibling) break;
                if (i.length >= a) break;
                o.previousSibling.textContent && (i = o.previousSibling.textContent + i), o = o.previousSibling
            }
            i.length;
            for (o = e; ;) {
                if (!o.nextSibling) break;
                if (i.length >= 70) break;
                o.nextSibling.textContent && (i += o.nextSibling.textContent), o = o.nextSibling
            }
            y.result[n].matches.push({
                chapter: n,
                elem: e,
                hint: i.trim(),
                location: r
            })
        }

        function i(e) {
            return (e.textContent.toLowerCase().match(new RegExp(y.key.toLowerCase(), "g")) || []).length
        }
        if (0 != e.children.length) {
            for (var a = 0; a < e.children.length; a++) 0 != i(e.children[a]) && (o = !1, hasClass(e.children[a], "k-section") && virt.checkIfRequiresParse(e.children[a]), h(e.children[a], t, n, !0));
            o && r(e)
        } else i(e) > 0 && r(e)
    }

    function m(e, t) {
        var n = 0;
        e.start && (n = e.start);
        var o = v(),
            r = function (i) {
                e.limit && v() - o >= e.limit ? t({
                    results: y.result,
                    more: !0,
                    key: y.key
                }) : i ? b(n++, r) : t({
                    results: y.result,
                    key: y.key
                })
            };
        b(n, r)
    }

    function v() {
        for (var e = 0, t = y.result.length; t--;)
            if (y.result[t])
                for (var n = y.result[t].matches.length; n--;) e++;
        return e
    }

    function b(e, t) {
        var n = stg.data.epub.spine.getElementsByTagName("itemref");
        {
            if (!(e > n.length - 1)) return chapters.getPageOb({
                page: e,
                dontParse: !0
            },
                function (n) {
                    var o = document.createElement("div"),
                        r = parser.parseFromString(n.raw ? n.raw : n.content, "text/html"),
                        i = r.getElementById("epubContent");
                    o.appendChild(i);
                    r.getElementsByTagName("title");
                    y.result[e] = [], y.result[e].title = n.title, y.result[e].matches = [], $filter("contentVirt")("parse", i, function (n) {
                        h(n, n, e), t(!0)
                    })
                });
            t(!1)
        }
    }

    var y = {};

    y.start = function (e, t) {
        if (y.key = e.key, e.array ? y.result = e.array : y.result = new Array, "all" == e.mode) m(e, t);
        else if ("chapter" == e.mode) {
            var n = cache.getOb("epubContent");
            y.result[currentIndex] = [], y.result[currentIndex].matches = [], h(n, n, currentIndex), t({
                results: y.result,
                key: y.key
            })
        }
    };

    y.removeHighlights = function () {
        for (var e = document.getElementsByClassName("searchItemHighlight"), t = e.length; t--;) removeClass(e[t], "searchItemHighlight")
    };

    y.searchItemClicked = function (e, t, n) {
        function a() {
            status.update(translit.get("pleaseWait"), null, {
                dots: !0
            }), chapters.addListener(l);
            var e = c.chapter,
                t = c.location;
            chapters.getPageOb({
                page: e
            }, function (n) {
                if (y.removeHighlights(), n.pages && n.pages.length);
                chapters.redirect(e, {
                    hash: t
                }), $rootScope.$$phase || $rootScope.$apply()
            })
        }

        function l(e) {
            "renderedChapter" == e && (status.update(), chapters.removeListener(l), p.index = d, $rootScope.renderScope.applyHeader("searchItems"), n && n())
        }
        t || (t = {});
        var c = e.item,
            d = e.index,
            p = e.matches;
        modals.hide(), t.timer ? timeOut(a, 300) : a()
    };

    return y;
}]);

app.factory("selection", ["markers", "cache", "stg", "virt", "$rootScope", "$injector", "$ionicScrollDelegate", function (markers, cache, stg, virt, $rootScope, $injector, $ionicScrollDelegate) {
    function c(e) {
        return parents(e, {
            stopAtId: "spreadL",
            filterByClass: "ki-noHighlight"
        }).length > 0
    }

    function d(e, t) {
        for (t || (t = e); !e.nextSibling;)
            if ((e = e.parentNode) == t) return;
        for (e = e.nextSibling; e.childNodes[0];) e = e.childNodes[0];
        return "#text" == e.nodeName && e.textContent.trim() ? e : d(e, t)
    }

    function u(e, t) {
        for (t || (t = e); !e.previousSibling;)
            if ((e = e.parentNode) == t) return;
        for (e = e.previousSibling; e.childNodes.length;) e = e.childNodes[e.childNodes.length - 1];
        return "#text" == e.nodeName && e.textContent.trim() ? e : u(e, t)
    }

    function p(e) {
        return 8 == e.nodeType || "#text" == e.nodeName || "br" == e.nodeName.toLowerCase() || (0 == e.offsetWidth || void 0)
    }

    function f(e, t) {
        var n = e.split("."),
            o = t.split(".");
        if (e == t) return 0;
        for (var r = 0; r < n.length; r++) {
            if (Number(n[r]) > Number(o[r])) return 1;
            if (Number(n[r]) < Number(o[r])) return -1
        }
        return n.length > o.length ? 1 : -1
    }

    function g(e) {
        var t = 1;
        return stg.data.book.chapter.viewport && (t = $injector.get("nav").getFxlScale()), e.getBoundingClientRect().width / t
    }
    var h, m, v, b, y, k, w, C, x, I, E, T, O, S, A, L, B, N, M, P, $ = {};

    $.getLocation = function () {
        return A
    };

    var H;

    $.elementDown = function (e, t) {
        I = e;
        var n = !0;
        mobile || ("a" == e.nodeName.toLowerCase() && (n = !1), c(e) && (n = !1), e.parentNode && "a" == e.parentNode.nodeName.toLowerCase() && (n = !1), n && addClass(e, "highlightOver")), redrawForChrome(I.parentElement);
        var o = mobile && !mobileDebug ? 400 : 100;
        if (n || (o *= 4), T = setTimeout($.elementHeld, o), mobile && !mobileDebug) {
            if (!t.touches || !t.touches.length) return;
            S = t.touches[0], M = S.clientX, P = S.clientY
        } else M = t.clientX, P = t.clientY, S = t;
        document.body.addEventListener(mobile && !mobileDebug ? "touchend" : "mouseup", $.elementUp), document.body.addEventListener(mobile && !mobileDebug ? "touchmove" : "mousemove", R)
    };

    var R = function (e) {
        S = mobile && !mobileDebug ? e.touches[0] : e, "activated" == L && e.stopPropagation()
    };

    $.elementUp = function (e) {
        I && (mobile || removeClass(I, "highlightOver"), I = null, clearTimeout(T)), H && removeClass(cache.getOb("epubContent"), "disableEvents"), H = !1, document.body.removeEventListener(mobile && !mobileDebug ? "touchend" : "mouseup", $.elementUp), document.body.removeEventListener(mobile && !mobileDebug ? "touchmove" : "mousemove", R), $rootScope.$$phase || $rootScope.$apply()
    };

    $.elementHeld = function () {
        $injector.get("nav").transforming || S && (Math.abs(S.clientX - M) > (mobile ? 30 : 10) || Math.abs(S.clientY - P) > (mobile ? 30 : 10) || c(I) || (addClass(I, "highlightSelected"), G(I), $.elementUp()))
    };

    $.getSelectedText = function (e) {
        e || (e = document.getElementsByClassName("highlightSelected"));
        for (var t = "", n = 0; n < e.length; n++) t += e[n].textContent;
        return t
    };

    $.getElemsByLocation = function (e, n) {
        n || (n = cache.getOb("epubContent"));
        var i = new Array;
        if (!e) return i;
        var a = e.split(",");
        if ("double" == stg.data.currentView) {
            for (var s = [], l = 0; l < a.length; l++) {
                var c = a[l];
                c = "1" + c.substr(1), s.push(c)
            }
            a = a.concat(s)
        }
        for (l = 0; l < a.length; l++)
            if (a[l]) {
                for (var d = a[l].split("."), u = n, p = 0; p < d.length; p++)
                    if (u) {
                        1 == p && virt.checkIfRequiresParseAndAnnotations(u.children[Number(d[p])]);
                        var f = 0;
                        u.hasAttribute("kb-offset") && (f = Number(u.getAttribute("kb-offset"))), u = u.children[Number(d[p]) - f]
                    }
                u && i.push(u)
            }
        return i
    };

    $.expand = function () {
        v = v.parentNode, V(), v = b = x, q(), mobile && D(), U()
    };

    $.extendRight = function (e) {
        if (e || "rtl" != getComputedStyle(v).direction) {
            if (O) {
                n = d(v, cache.getOb("epubContent"));
                if (v = n.parentNode, p(v)) return void $.extendRight(e)
            } else {
                var n = d(b, cache.getOb("epubContent"));
                if (b = n.parentNode, p(b)) return void $.extendRight(e)
            }
            V(), q(), mobile && D(), U()
        } else $.extendLeft(!0)
    };

    $.extendLeft = function (e) {
        if (e || "rtl" != getComputedStyle(v).direction) {
            if (O) {
                n = u(v, cache.getOb("epubContent"));
                if (b = n.parentNode, p(b)) return void $.extendLeft(e)
            } else {
                var n = u(v, cache.getOb("epubContent"));
                if (v = n.parentNode, p(v)) return void $.extendLeft(e)
            }
            V(), q(), mobile && D(), U()
        } else $.extendRight(!0)
    };

    $.newChapter = function () {
        mobile && (markers.deleteMarker("anchor1Marker"), markers.deleteMarker("anchor2Marker"), h = $.createAnchorMarker("anchor1"), m = $.createAnchorMarker("anchor2"), addClass(h, "disable"), addClass(m, "disable"))
    };

    $.getPageByLocation = function (e, t) {
        var n = e.pages;
        if (n && n.length) {
            for (var o = 0; o < n.length - 1; o++)
                if (f(t, n[o].lastLocation) <= 0) return o;
            return n.length - 1
        }
    };

    $.getTopCornerLocation = function () {
        var e = $.getTopCornerElement();
        return $.convertElemsToLocation([e]).left
    };

    $.getTopCornerElement = function () {
        for (var e = cache.getOb("epubContent"), n = cache.getOb("epubContainer"), o = e.children[0], r = $ionicScrollDelegate.$getByHandle("content").getScrollPosition().top + 25, i = new Array, a = o.getElementsByClassName("k-section"), s = 0; s < a.length; s++) hasClass(a[s], "cNone") || i.push(a[s]);
        for (var c, d = document.getElementById("readerHeader").offsetHeight + 10, s = 0; s < i.length; s++)
            for (var u = i[s].getElementsByTagName("*"), p = 0; p < u.length; p++)
                if (!u[p].children.length) {
                    var f = u[p].getBoundingClientRect();
                    if (f.bottom > d) {
                        if (f.top > d && f.top < d + 10) return u[p];
                        var g = f.top - d;
                        c ? g < c.distance && f.bottom < d + n.offsetHeight && (c = {
                            elem: u[p],
                            bounds: f,
                            distance: g
                        }) : c = {
                            elem: u[p],
                            bounds: f,
                            distance: g
                        }
                    }
                }
        if (c) return c.elem;
        for (s = 0; s < i.length; s++)
            if (i[s].offsetTop - r > 0) return i[s];
        return i[0]
    };

    $.getIdArray = function (e) {
        for (var n = $.getTopCornerElement(), o = []; ;) {
            if (!n) break;
            if (n == cache.getOb("epubContent")) break;
            n.getAttribute && n.getAttribute("id") && o.push(n.getAttribute("id")), n = n.previousSibling ? n.previousSibling : n.parentNode
        }
        return o
    };

    var D = function () {
        markers.refreshMarkerById("anchor1Marker"), markers.refreshMarkerById("anchor2Marker")
    },
        F = function (e) {
            var t = 0;
            for (e && e.parentNode && e.parentNode.hasAttribute("kb-offset") && (t = Number(e.parentNode.getAttribute("kb-offset"))); e = e.previousSibling;) "#text" != e.nodeName && 8 != e.nodeType && t++;
            return t
        },
        z = function (e, n) {
            n || (n = cache.getOb("epubContent"));
            for (var o = ""; e != n;) o = F(e) + "." + o, e = e.parentNode;
            var r = "left";
            return "" == o ? {
                index: "",
                side: r
            } : ("1" == o[0] && (o = "0" + o.substr(1), r = "right"), {
                index: o.substr(0, o.length - 1),
                side: r
            })
        },
        U = function () {
            return A = $.convertElemsToLocation(document.getElementsByClassName("highlightSelected"))
        };

    $.convertElemsToLocation = function (e, n) {
        n || (n = cache.getOb("epubContent"));
        for (var o = "", r = "", i = 0; i < e.length; i++) {
            var a = z(e[i], n);
            "left" == a.side ? o += (o ? "," : "") + a.index : r += (r ? "," : "") + a.index
        }
        return {
            left: o,
            right: r
        }
    };

    var V = function () {
        y = new Array, k = new Array;
        for (var e = cache.getOb("epubContent").parentNode, n = v; ;)
            if (y.push(n), (n = n.parentNode) == e) break;
        for (n = b; ;)
            if (k.push(n), (n = n.parentNode) == e) break;
        var o = function () {
            for (i = 0; i < y.length; i++)
                for (j = 0; j < k.length; j++)
                    if (k[j] == y[i]) return [k[j], i, j]
        }();
        for (x = o[0], w = o[1], C = o[2], i = 0; i < x.childNodes.length; i++) {
            if (x.childNodes[i] == y[w - 1]) {
                O = !1;
                break
            }
            if (x.childNodes[i] == k[C - 1]) {
                O = !0;
                break
            }
        }
    },
        q = function () {
            try {
                if (K(), addClass(v, "highlightSelected"), addClass(b, "highlightSelected"), v == b) return void (A = {
                    left: z(v)
                });
                if (v == x) return void (A = {
                    left: z(v)
                });
                if (b == x) return void (A = {
                    left: z(b)
                });
                W(O ? b : v, O ? v : b)
            } catch (e) { }
        },
        W = function (e, t, n) {
            var o;
            if (n || (n = "first"), "first" == n) {
                if (e.parentNode == x) return void W(e, t, "second");
                for (o = e;
                    (o = o.nextSibling) && o;) "#text" != o.nodeName && 8 != o.nodeType && addClass(o, "highlightSelected");
                W(e.parentNode, t, "first")
            } else if ("second" == n) {
                if (t.parentNode == x) return void W(e, t, "third");
                for (o = t;
                    (o = o.previousSibling) && o;) "#text" != o.nodeName && 8 != o.nodeType && addClass(o, "highlightSelected");
                W(e, t.parentNode, n)
            } else if ("third" == n) {
                var r, a = O ? k[C - 1] : y[w - 1],
                    s = O ? y[w - 1] : k[C - 1];
                for (i = 0; i < x.childNodes.length; i++)
                    if (a != x.childNodes[i]) {
                        if (s == x.childNodes[i]) break;
                        r && "#text" != x.childNodes[i].nodeName && 8 != x.childNodes[i].nodeType && addClass(x.childNodes[i], "highlightSelected")
                    } else r = !0
            }
        },
        _ = function () {
            K(), mobile && (h && (addClass(h, "disable"), addClass(m, "disable")), m && (removeClass(h, "tween"), removeClass(m, "tween"))), L = null, O = null, $.listener && $.listener("deactivated")
        },
        K = function () {
            try {
                var e = document.getElementsByClassName("highlightSelected");
                if (!e) return;
                for (var t = e.length; t--;) removeClass(e[t], "highlightSelected");
                mobile && virt.forceRepaint()
            } catch (e) { }
        };

    $.listener = null;

    $.addSpanAtCloseTag = function () {
        var e = document.createElement("span"),
            t = O ? v : b;
        return t.parentNode.insertBefore(e, t.nextSibling), e
    };

    $.getSelectedNodes = function () {
        return document.getElementsByClassName("highlightSelected")
    };

    $.reset = function () {
        _()
    };

    $.deleteNoteMarker = function (t) {
        var n = document.getElementById("note" + t);
        n && n.parentNode && n.parentNode.removeChild(n), markers.deleteMarker("noteMarker" + t)
    };

    $.getNoteMarker = function (t) {
        return markers.getMarker("noteMarker" + t)
    };

    $.createAnchorMarker = function (n) {
        var o = document.createElement("span");
        return o.setAttribute("id", n + "Marker"), o.setAttribute("anchor", n), addClass(o, "anchorMarker"), cache.getOb("epubMeta").appendChild(o), o.addEventListener(mobileDebug ? "mousedown" : "touchstart", function (e) {
            addClass(E = o, "selected"), N = null, document.addEventListener(mobileDebug ? "mousemove" : "touchmove", Y), B && clearInterval(B), B = setInterval(J, 60), document.addEventListener(mobileDebug ? "mouseup" : "touchend", Z), addClass(cache.getOb("epubContent"), "defaultCursor"), L = "anchorDragged", e.stopPropagation()
        }), markers.addMarker({
            id: n + "Marker",
            elem: o,
            offsetTop: -45,
            offsetLeft: -10,
            target: function () {
                var e, t = 0;
                if ("anchor1" == n) {
                    if (!(e = v)) return void (this.offsetLeft = -10);
                    try {
                        o = "rtl" == getComputedStyle(e).direction
                    } catch (e) { }
                    t = O ? o ? 0 : g(e) : o ? g(e) : 0
                } else {
                    if (!(e = b)) return void (this.offsetLeft = -10);
                    var o;
                    try {
                        o = "rtl" == getComputedStyle(e).direction
                    } catch (e) { }
                    t = O ? o ? g(e) : 0 : o ? 0 : g(e)
                }
                return this.offsetLeft = -10 + t, e
            }
        }), o
    };

    $.createNoteMarker = function (n, o, r) {
        if (r || (r = {}), !document.getElementById("note" + n.nid)) {
            var i = document.createElement("span");
            i.setAttribute("id", "note" + n.nid), addClass(i, r.class ? r.class : "contentNoteMarker"), cache.getOb("epubMeta").appendChild(i);
            var a = 0;
            return a || (a = 0), markers.addMarker({
                id: "noteMarker" + n.nid,
                elem: i,
                offsetTop: a,
                target: function () {
                    return hasClass(this.elem, "contentNoteMarker") ? this.offsetLeft = g(o) - 13 : this.offsetLeft = 0, this.offsetTop < 10 && (this.offsetTop = 10), o
                },
                callback: function () {
                    var t = this;
                    markers.checkMarkerExistsAtSameLocation(this.elem, function (o) {
                        return o ? (t.offsetTop += 39, void markers.refreshMarker(t)) : n.callback ? n.callback() : void 0
                    })
                }
            }), i
        }
    };

    $.deleteBookMarker = function (t) {
        var n = document.getElementById("bookmark" + t);
        n && n.parentNode && n.parentNode.removeChild(n), markers.deleteMarker("bookMarker" + t)
    };

    $.createBookMarker = function (n, o) {
        var r = document.createElement("span");
        r.setAttribute("id", "bookmark" + n.bmid), addClass(r, "contentBookMarker"), cache.getOb("epubMeta").appendChild(r);
        var i = 0;
        return i || (i = 0), markers.addMarker({
            id: "bookMarker" + n.bmid,
            elem: r,
            offsetLeft: 0,
            offsetTop: i,
            target: function () {
                return this.offsetTop < 10 && (this.offsetTop = 10), o
            },
            callback: function () {
                var t = this;
                markers.checkMarkerExistsAtSameLocation(this.elem, function (o) {
                    return o ? (t.offsetTop += 39, void markers.refreshMarker(t)) : n.callback ? n.callback() : void 0
                })
            }
        }), r
    };

    $.embedAnchorMarker = function (e) { };

    var Y = function (e) {
        N = e, e.stopPropagation()
    },
        J = function (e) {
            try {
                if (e && e.stopPropagation(), mobile) {
                    if ("anchorDragged" != L) return;
                    if (!N) return
                }
                var n;
                if (mobile) {
                    var o = mobileDebug ? N : N.targetTouches[0];
                    n = document.elementFromPoint(o.clientX, Math.round(o.clientY + h.offsetHeight / 2))
                } else n = e.target;
                if (cache.getOb("epubContent") == n) return;
                if (!cache.getOb("epubContent").contains(n)) return;
                if ((n.textContent.match(new RegExp(" ", "g")) || []).length > 1) return;
                if (mobile)
                    if ("anchor1" == E.getAttribute("anchor")) {
                        if (v == n) return;
                        v = n
                    } else {
                        if (b == n) return;
                        b = n
                    } else b = n;
                V(), q(), redrawForChrome(v.parentElement), redrawForChrome(b.parentElement), mobile && D()
            } catch (e) { }
        },
        Z = function (e) {
            try {
                var n = cache.getOb("epubContainer");
                document.removeEventListener(mobile && !mobileDebug ? "touchend" : "mouseup", Z), mobile ? (document.removeEventListener(mobileDebug ? "mousemove" : "touchmove", Y), removeClass(E, "selected"), clearInterval(B)) : n.removeEventListener("mousemove", J), removeClass(cache.getOb("epubContent"), "defaultCursor"), E = null, A = U(), L = "selectionFixed", $.listener && $.listener(L), e.stopPropagation()
            } catch (e) { }
        },
        G = function (e) {
            try {
                var n = cache.getOb("epubContainer");
                v = b = e, mobile ? (removeClass(h, "disable"), removeClass(m, "disable"), timeOut(function () {
                    addClass(h, "tween"), addClass(m, "tween")
                }, 100), D()) : (n.addEventListener("mousemove", J), document.addEventListener("mouseup", Z)), U(), L = "activated", $.listener && $.listener(L)
            } catch (e) { }
        };

    $.active = function () {
        return "selectionFixed" == L || "activated" == L
    };

    return $;
}]);

app.factory("settings", ["spread", "cache", "modals", "$ionicScrollDelegate", "translit", "virt", "$injector", "stg", "markers", "sync", "crdv", "cssParser", function (spread, cache, modals, $ionicScrollDelegate, translit, virt, $injector, stg, markers, sync, crdv, cssParser) {
    var v = {};
    v.pinchActive = !1;

    v.stylesInitialize = function () { };

    v.initialize = function () {
        v.stylingChanged(), v.lineAdjustChanged(), v.updateTextSize(), markers.refreshMarkers()
    };

    v.getTextSize = function () {
        return Number(stg.data.settings.textSize.slice(0, -2))
    };

    v.viewModeChanged = function () {
        spread.applyMode(stg.data.settings.viewMode)
    };

    v.updateTextSize = function () {
        try {
            var e = cache.getOb("epubContent"),
                t = cache.getOb("epubContent").style;
            if (t.fontSize == stg.data.settings.textSize) return;
            t.fontSize = stg.data.settings.textSize;
            for (var n = 0; n < e.children.length; n++) try {
                e.children[n].style.fontSize = "1em"
            } catch (e) { }
            var o, i = cache.getOb("epubContainer"),
                s = e.parentNode.parentNode,
                l = s.style.webkitTransform || s.style.mozTransform || s.style.transform;
            o = overflowScroll ? cache.getOb("epubContainer").scrollTop : Number(l.split(",")[1].slice(0, -2));
            var d = e.offsetHeight,
                u = i.offsetTop,
                f = i.offsetHeight;
            d + o < f + u ? virt.safeScroll("content", 0, u + d - f, !0) : d < f && $ionicScrollDelegate.$getByHandle("content").scrollTop(!1), virt.reset()
        } catch (e) { }
    };

    v.writeTextSize = function () {
        stg.writeSlot("settings" + stg.getBookId(!0), stg.data.settings, function () {
            markers.refreshMarkers()
        })
    };

    v.stylingChanged = function () {
        stg.data.settings.styling ? cssParser.showStyles() : cssParser.hideStyles(), markers.refreshMarkers()
    };

    v.saveSettings = function () {
        stg.writeSlot("settings" + stg.getBookId(!0), stg.data.settings)
    };

    v.langChanged = function () {
        "ar" == stg.data.settings.language ? stg.data.settings.rtl = !0 : stg.data.settings.rtl = !1, $injector.get("nav").fxlZoomFit(), stg.writeSlot("settings" + stg.getBookId(!0), stg.data.settings, function () {
            translit.setLang(stg.data.settings.language), markers.refreshMarkers()
        })
    };

    v.lineAdjustChanged = function () {
        stg.data.settings.lineAdjust ? addClass(cache.getOb("epubContent"), "adjustedLineHeight") : removeClass(cache.getOb("epubContent"), "adjustedLineHeight"), markers.refreshMarkers()
    };

    v.sync = function () {
        sync.onOnline(function () {
            native ? crdv.toast(translit.get("syncComplete")) : modals.show({
                mode: "success",
                successOptions: {
                    msg: translit.get("syncComplete")
                }
            })
        })
    };

    return v;
}]);

app.factory("skeleton", ["stg", function (stg) {
    function t(e, t) {
        var r = u.getBone(e.index - 1);
        if (r) {
            var i = r;
            r.total > 1 && (i = u.getBone(e.index - 1, r.total - 1)), i.next = e, e.prev = i
        }
        var a = u.getBone(t.index + 1);
        a && (a.prev = t, t.next = a);
        var s;
        if ("double" == n()) {
            var l = u.getJoint(e.index - 1),
                c = u.getJoint(t.index + 1);
            if (l) return void o();
            var d = t.total ? t.total : 1;
            if (c && 1 == c.length && c[0].index >= config.firstDoublePage && (s = !0), c) {
                var p;
                if (s || d % 2 == 0 && (p = !0), s && d % 2 == 1 && (p = !0), p) return void o();
                e.alone = !0
            }
        }
        o()
    }

    function n() {
        var t = stg.data.settings.viewMode;
        return "double" == t && config.singleViewOnPortrait && mobile && 1.3 * window.innerWidth < window.innerHeight && (t = "single"), t
    }

    function o(e) {
        e || (e = {}), p = [], e.firstBone || (e.firstBone = r());
        for (var t = e.firstBone, o = n(), i = e.lastJoint; t;) {
            var s = [];
            if (t.joint = s, "single" == o ? s.push(t) : "double" == o ? (s.push(t), i && delete t.alone, t.alone || t.index >= config.firstDoublePage && t.next && (s.push(t = t.next), t.joint = s)) : "auto" == o && s.push(t), i && (s.prev = i, i.next = s), p.push(s), i = null, t.next && (i = s), e.lastBone == t) break;
            t = a(t, e)
        }
        return p[p.length - 1]
    }

    function r(e) {
        var t;
        e || (e = 0);
        for (var n = 0; n < f.length; n++) f[n].index < e || (t ? f[n].index < t.index ? t = f[n] : f[n].index == t.index && f[n].vIndex < t.vIndex && (t = f[n]) : t = f[n]);
        return t
    }

    function i(e) {
        for (var t, n = 0; n < f.length; n++) f[n].index > e || (t ? f[n].index > t.index ? t = f[n] : f[n].index == t.index && f[n].vIndex > t.vIndex && (t = f[n]) : t = f[n]);
        return t
    }

    function a(e, t) {
        return t || (t = {}), e.next ? e.next : t.connectedOnly ? void 0 : s(e.index + 1)
    }

    function s(e) {
        var t, n, o = i();
        if (o)
            for (; e <= o.index;) {
                if (t && (e = t.index + 1), n = u.getBone(e)) return n;
                t = null, e++
            }
    }

    function l(e) {
        var t = s(e);
        if (t) return t.joint
    }

    function c(e, t) {
        t || (t = "");
        for (var n; e;) t += e.index + "." + e.vIndex, n = e, (e = e.next) && (t += "->");
        return n
    }

    function d(e, t) {
        t || (t = "");
        for (var n; e;) {
            t += " [";
            for (var o = 0; o < e.length; o++) t += e[o].index + "." + e[o].vIndex + ",";
            t = t.substr(0, t.length - 1) + "]", n = e, (e = e.next) && (t += "->")
        }
        return n
    }
    var u = {},
        p = [],
        f = [];

    u.reset = function () {
        f = [], p = []
    };

    u.initialize = function (e) {
        u.reset(), e || (e = {});
        for (var t = e.first; t < e.last; t++) u.add({
            index: t
        })
    };

    u.trace = function () {
        for (var e = -1; ;) {
            var t = s(e + 1);
            if (!t) break;
            e = c(t).index
        }
        for (var n = -1; ;) {
            var o = l(n + 1);
            if (!o) break;
            var r = d(o);
            n = r[r.length - 1].index
        }
    };

    u.add = function (e) {
        var n, o, r, i, a = u.getBone(e.index);
        if (a) {
            var s = e.pages && e.pages.length;
            if (a.total && s) return;
            if (!a.total && !s) return;
            u.remove(a)
        }
        if (e.pages && e.pages.length)
            for (var l = 0; l < e.pages.length; l++)(i = {}).index = e.index, i.vIndex = l, i.total = e.pages.length, n && (i.prev = n, n.next = i), f.push(i), n = i, 0 == l && (o = i), l == e.pages.length - 1 && (r = i);
        else (i = {}).index = e.index, i.vIndex = null, f.push(i), o = r = i;
        t(o, r)
    };

    u.remove = function (e) {
        e.prev && e.prev.next && (e.prev.next = null), e.next && e.next.prev && (e.next.prev = null);
        for (var t = 0; t < f.length; t++)
            if (f[t] == e) return void f.splice(t, 1)
    };

    u.getBone = function (e, t) {
        for (var n = 0; n < f.length; n++)
            if (f[n].index == e) {
                if (null == t) return f[n];
                if (f[n].vIndex == t) return f[n]
            }
    };

    u.normalize = function (e) {
        if (null != e.vIndex) {
            var t = u.getJoint(e.index, e.vIndex);
            if (t) {
                var n = t[0].index,
                    o = t[0].vIndex;
                (n != e.index || null != o && o != e.vIndex) && (e.index = n, null != o && (e.vIndex = o))
            }
        }
    };

    u.getJoint = function (e, t) {
        for (var n = 0; n < p.length; n++)
            for (var o = 0; o < p[n].length; o++)
                if (p[n][o].index == e) {
                    if (t <= -1 && p[n][o].total && (t += p[n][o].total), null == t) return p[n];
                    if (null == p[n][o].vIndex) return p[n];
                    if (p[n][o].vIndex == t) return p[n]
                }
    };

    u.nextJoint = function (e, t) {
        var n = u.getBone(e, t);
        if (n) {
            var o = n.joint.next;
            if (o) return {
                index: o.index,
                vIndex: o.vIndex
            }
        }
    };

    u.prevJoint = function (e, t) {
        var n = u.getBone(e, t);
        if (n) {
            var o = n.joint.prev;
            if (o) return {
                index: o.index,
                vIndex: o.vIndex
            }
        }
    };

    u.getNextIndex = function (e, t) {
        var n = {},
            o = u.getJoint(e.index, e.vIndex);
        return o && o.next ? n.leftOb = {
            index: o.next[0].index,
            vIndex: o.next[0].vIndex
        } : n.leftOb = {
            index: o[o.length - 1].index + 1,
            vIndex: 0
        }, t && t(n), n
    };

    u.getPrevIndex = function (e, t) {
        var n = {},
            o = u.getJoint(e.index, e.vIndex);
        return o && o.prev ? n.leftOb = {
            index: o.prev[0].index,
            vIndex: o.prev[0].vIndex
        } : n.leftOb = {
            index: o[0].index - 1,
            vIndex: -1
        }, t && t(n), n
    };

    u.isSingle = function (e, t) {
        var n = u.getJoint(e, t);
        if (n) return 1 == n.length
    };

    u.getCurrentView = function (e, t) {
        var n = u.getJoint(e, t.vIndex);
        if (n) {
            var o = {};
            return o.view = 1 == n.length ? "single" : "double", o.index = e, o
        }
    };

    u.isLastChapter = function (e, t) {
        e || (e = {});
        var n = null == e.index ? currentIndex : e.index,
            o = null == e.vIndex ? currentVIndex : e.vIndex;
        return t(u.getJoint(n, o) == i(e.last).joint)
    };

    u.isFirstChapter = function (e, t) {
        return e || (e = {}), t(u.getJoint(currentIndex, currentVIndex) == r(e.first).joint)
    };

    u.isNeighbor = function (e, t) {
        for (var n = u.getJoint(currentIndex, currentVIndex), o = n.next, r = n.prev, i = 0; i < 2; i++) {
            var a = [o, r][i],
                s = ["next", "prev"][i];
            if (a)
                for (var l = 0; l < a.length; l++)
                    if (a[l].index == e.index) {
                        if (null == a[l].vIndex) return t(s);
                        if (a[l].vIndex == e.vIndex) return t(s)
                    }
        }
        t()
    };

    return u;
}]);

app.factory("spread", ["$injector", "selection", "chapters", "stg", "$filter", function ($injector, selection, chapters, stg, $filter) {
    function l(e, t) {
        function n(e) {
            if (!e) return "";
            t.url;
            for (var n = getElementsByAttribute(e, "*", "src"), o = 0; o < n.length; o++) n[o].getAttribute("src");
            return e.outerHTML
        }

        function r(e, r) {
            r || (r = {}), f = x.top + t.pageHeight - k - w, a && (s.length, s.push({
                content: n(traverseToTop(a, "epubContent")),
                lastLocation: selection.convertElemsToLocation([e], C).left
            })), (a = u(e, {
                markOffset: !0
            })).appendChild(e.cloneNode(!0))
        }

        function i(e) {
            return l.nextSibling ? (l = l.nextSibling, "sibling") : l.parentNode && "body" != l.nodeName.toLowerCase() ? (l = l.parentNode, a = a.parentNode ? a.parentNode : traverseToBottom(a, "epubContent"), i()) : (a && s.push({
                content: n(traverseToTop(a, "epubContent"))
            }), "break")
        }
        t.pages || (t.pages = []), t.index || (t.index = 0), t.lastStop || (t.lastStop = document.createElement("div"));
        var a = t.lastStop,
            s = [];
        e.style.width = t.pageWidth + "px";
        var l = e,
            f = t.pageHeight - 0,
            g = getElemMargin(e, {
                computed: !0
            }),
            h = getElemPadding(e, {
                computed: !0
            }),
            m = g.top,
            v = g.bottom,
            b = h.top,
            y = h.bottom;
        "auto" == m && (m = 0), "auto" == v && (v = 0), "auto" == b && (b = 0), "auto" == y && (y = 0), m || (m = 0), v || (v = 0), b || (b = 0), y || (y = 0), g = Number(g), h = Number(h);
        for (var k = m + b, w = v + y, C = traverseToBottom(e, "epubContent"); ;) {
            if (!l || 8 == l.nodeType) {
                if ("continue" == i()) continue;
                break
            }
            var x = getBoundingBox(l);
            if (x.bottom < f - w) {
                if (t.supportsPageBreakStyles && d(l)) {
                    if (r(l, {
                        src: "breakStyle"
                    }), "break" == i()) break;
                    continue
                }
                I = l.cloneNode(!0);
                if (a.appendChild(I), s.length && p(I, l, "less"), "break" == i()) break
            } else if (c(l)) {
                var I = cloneNodeAttributes(l);
                a.appendChild(I), s.length && p(I, l, "canDigInside"), a = a.childNodes[a.childNodes.length - 1], l = l.childNodes[0]
            } else if (r(l, {
                src: "end"
            }), "break" == i()) break
        }
        if (1 == s.length) {
            l = traverseToBottom(e, "epubContent");
            s[0].content = n(l)
        }
        return s
    }

    function c(e) {
        if (!e.childNodes.length) return !1;
        if (e.className.indexOf && e.className.indexOf("kInteractive") >= 0) {
            if (hasClass(e, "video")) return !1;
            if (hasClass(e, "gallery")) return !1;
            if (hasClass(e, "audio")) return !1;
            if (hasClass(e, "widget")) return !1;
            if (hasClass(e, "widget")) return !1;
            if (hasClass(e, "threed")) return !1
        }
        return !0
    }

    function d(e) {
        return (!e.nodeName || "span" != e.nodeName.toLowerCase()) && (!(!e.style || "always" != e.style.pageBreakBefore && "always" != getComputedStyle(e).pageBreakBefore) || !(!e.previousSibling || !e.previousSibling.style || "always" != e.previousSibling.style.pageBreakAfter && "always" != getComputedStyle(e.previousSibling).pageBreakAfter))
    }

    function u(e, t) {
        t || (t = {});
        for (var n, o = parents(e), r = 0; r < o.length - 1; r++)
            if ("#document" != o[r].nodeName.toLowerCase() && "html" != o[r].nodeName.toLowerCase()) {
                var i = cloneNodeAttributes(o[r]);
                n ? (n.appendChild(i), p(i, o[r], "duplicate"), n = n.childNodes[0]) : n = i
            }
        return n
    }

    function p(e, t, n) {
        var o = getFilteredChildIndex(t);
        if (o > 0) {
            var r = e.parentNode,
                i = getFilteredChildIndex(e);
            o != i && (r.getAttribute("kb-offset") || r.setAttribute("kb-offset", o - i))
        }
    }

    var f = {};

    f.initialize = function () { };

    f.applyMode = function (e) {
        "double" == e ? f.activateDblPage() : "single" == e ? f.activateSinglePage() : "auto" == e && f.activateAutoPage()
    };

    f.activateSinglePage = function () {
        if (chapters.shouldBePaged(stg.data.currentPageOb)) chapters.regenerateSpreads();
        else {
            chapters.resetPages({
                buildChapterMode: !0
            });
            var e = {};
            e.viewMode = "single", e.reload = !0, chapters.loadChapterByIndex(currentIndex, e, function () { })
        }
    };

    f.activateDblPage = function () {
        if (chapters.shouldBePaged(stg.data.currentPageOb)) chapters.regenerateSpreads();
        else {
            chapters.resetPages({
                buildChapterMode: !0
            });
            var e = {};
            e.viewMode = "double", e.reload = !0, chapters.loadChapterByIndex(currentIndex, e, function () { })
        }
    };

    f.activateAutoPage = function () {
        "double" == chapters.getCurrentView(currentIndex, {
            vIndex: currentVIndex
        }).view ? f.activateDblPage() : f.activateSinglePage()
    };

    f.getViewport = function (e) {
        e || (e = {});
        var t = document.getElementById("readerBody"),
            n = document.getElementById("readerHeader"),
            o = document.getElementById("tabMenu"),
            r = t.getBoundingClientRect().width,
            s = t.getBoundingClientRect().height;
        mobile && "alwaysShow" != config.autohideReaderMode && "showOnScroll" != config.autohideReaderMode && "showOnTap" != config.autohideReaderMode || (s -= n.getBoundingClientRect().height + o.getBoundingClientRect().height);
        var l = chapters.getCurrentView(e.index, {
            vIndex: e.vIndex
        });
        l || (l = {
            view: "double"
        });
        var c = {},
            d = config.spreadSpacing ? config.spreadSpacing : 0;
        return "double" == l.view ? c.width = Math.round((r - d) / 2) : (c.width = r, "double" == stg.data.settings.viewMode && (c.width = r / 2), 1.3 * window.innerWidth < window.innerHeight && (c.width = r)), c.height = s, c
    };

    f.parseReflowableChapter = function (e, n) {
        e.raw && !e.pages && (e.content = e.raw), $injector.get("context").simulateChapter(e, function (t) {
            var o = f.getViewport(e),
                r = {};
            r.pages = [], r.pageWidth = o.width, r.pageHeight = o.height, r.url = e.url, r.index = e.index, t.style.width = "100%", t.style.height = "100%", t.style.position = "absolute", t.style.zIndex = -1, t.style.visibility = "hidden", t.setAttribute("data-index", r.index), document.body.appendChild(t), t.addEventListener("error", function (e) { }), t.addEventListener("load", function () {
                var e = t.contentDocument.documentElement,
                    i = e.getElementsByTagName("body")[0];
                i.style.width = o.width + "px", i.style.height = 2 * o.height + "px";
                var a = e.getElementsByTagName("style")[0].innerHTML;
                r.supportsPageBreakStyles = a.indexOf("page-break-after") >= 0 || a.indexOf("page-break-before") >= 0;
                var c = t.contentDocument.getElementById("spreadR");
                c && c.parentNode.removeChild(c), t.contentDocument.getElementById("epubScroller").style.overflowY = "hidden";
                var d = t.contentDocument.getElementById("epubContent"),
                    u = d.outerHTML;
                $filter("contentVirt")("parse", d, function () {
                    var e = l(i, r);
                    t.parentNode.removeChild(t);
                    var a = {};
                    a.pages = e, a.raw = u, a.viewport = o, n(a)
                })
            })
        })
    };

    return f;
}]);

app.factory("status", ["$ionicLoading", "translit", "$q", function ($ionicLoading, translit, $q) {
    function o() {
        i && (clearTimeout(i), i = null)
    }

    function r() {
        o(), s && (i = setTimeout(function () {
            a.update().then(function () {
                a.showAndHide(translit.get("statusTimeoutError"), 3e3)
            })
        }, s))
    }
    var i, a = {}, s = 1e4;

    a.update = function (t, i, l) {
        var c = $q.defer();
        if (l || (l = {}), l.dots && (t += ' <ion-spinner icon="dots" class="loadingSpinner"></ion-spinner>'), l.progress && (t += '<div class="loadingProgress"></div>'), s = l.timeoutDuration ? l.timeoutDuration : 1e4, o(), t) {
            r();
            var d = {
                template: t
            };
            for (var u in l) d[u] = l[u];
            $ionicLoading.show(d), a.updateProgress()
        } else $ionicLoading.hide();
        return timeOut(function () {
            c.resolve()
        }, i || 250), c.promise
    };

    a.updateProgress = function (e) {
        e || (e = 0), e > 1 && (e = 1);
        var t = document.getElementsByClassName("loadingProgress");
        t.length && (t[0].style.width = Math.round(100 * e) + "%"), e > 0 && r()
    };

    a.showAndHide = function (e, t) {
        a.update(e, t).then(function () {
            a.update()
        })
    };

    return a;
}]);

app.factory("stg", ["sync", "$rootScope", "serverapi", "modals", function (sync, $rootScope, serverapi, modal) {
    var n = { data: {}, view: {} };

    n.initialize = function () {
        $rootScope.data = n.data, $rootScope.view = n.view, n.reset(), n.setLanguages(), preview && n.clearSlots()
    };

    n.initializeSettings = function (e) {
        function t() {
            n.applyDefaultSettings();
            try {
                if (config.defaultLanguage && (n.data.settings.language = config.defaultLanguage), n.data.settings.navAnim = config.defaultNavAnim ? config.defaultNavAnim : "navBookBlock", n.data.settings.viewMode = config.defaultViewMode ? config.defaultViewMode : "auto", n.data.settings.pageScroll = null == config.defaultPageScroll || config.defaultPageScroll, !preview) return void n.readSlot("settings" + n.getBookId(!0), function (t) {
                    t && (t.language && (n.data.settings.language = t.language), t.navAnim && (n.data.settings.navAnim = t.navAnim), t.viewMode && (n.data.settings.viewMode = t.viewMode)), e && e()
                })
            } catch (e) { }
            e && e()
        }
        try {
            n.readSlot("settings" + n.getBookId(!0), function (e) {
                n.data.settings = e, 0 == n.data.settings.length && (n.data.settings = {}), t()
            })
        } catch (e) {
            n.data.settings = {}, t()
        }
    };

    n.reset = function () {
        n.resetBook(), n.resetLibrary()
    };

    n.setLanguages = function () {
        n.data.languages = [{
            label: "English",
            val: "en"
        }, {
            label: "Français",
            val: "fr"
        }, {
            label: "Español",
            val: "es"
        }, {
            label: "Português",
            val: "pt"
        }, {
            label: "Nederlands",
            val: "nl"
        }, {
            label: "Deutsch",
            val: "de"
        }, {
            label: "Magyar",
            val: "hu"
        }, {
            label: "Italiano",
            val: "it"
        }, {
            label: "Svenska",
            val: "sw"
        }, {
            label: "Melayu",
            val: "ms"
        }, {
            label: "Norsk",
            val: "no"
        }, {
            label: "Polski",
            val: "pl"
        }, {
            label: "Român",
            val: "ro"
        }, {
            label: "Pусский",
            val: "ru"
        }, {
            label: "Türkçe",
            val: "tr"
        }, {
            label: "العربية",
            val: "ar"
        }, {
            label: "汉语",
            val: "zh"
        }, {
            label: "日本語",
            val: "jp"
        }, {
            label: "한국어",
            val: "ko"
        }]
    };

    n.resetBook = function () {
        n.data.epub = {}, n.data.toc = "", n.data.mediaList = [], n.data.packageURL = "", n.data.book = {}, n.data.book.chapter = {}, n.data.chromeAssets = []
    };

    n.resetLibrary = function () {
        n.data.downloaded = [], n.data.favorites = [], n.data.currentLibrary = {}
    };

    n.applyDefaultSettings = function () {
        n.data.settings && (n.data.settings.styling || (n.data.settings.styling = !0), n.data.settings.textSize || (n.data.settings.textSize = "1em"), n.data.settings.lineAdjust || (n.data.settings.lineAdjust = !0), n.data.settings.language || (n.data.settings.language = "en"), null == config.viewModeOptional && (config.viewModeOptional = !0), null == config.pageScrollOptional && (config.pageScrollOptional = !0), null == config.navAnimOptional && (config.navAnimOptional = !0), null == config.languageOptional && (config.languageOptional = !0))
    };

    n.getBookId = function (e) {
        if (!config.kotobee.cloudid) {
            var t, o = "",
                r = "";
            try {
                t = n.data.book.meta.dc.identifier
            } catch (e) { }
            if (desktop) {
                var i = require("nw.gui");
                r = t || i.App.manifest.name + (i.App.manifest.rand ? i.App.manifest.rand : "")
            } else if (native) r = t || (cordova.rand ? cordova.rand : "");
            else {
                r = (r = (r = (r = location.href.split("#")[0]).replace("https://", "http://")).replace("http://www.", "http://")).replace("http://", "");
                try {
                    o = e ? "" : t || n.data.book.meta.dc.title
                } catch (e) { }
            }
            return "_" + (r + o).replace(/[\/\s:#]/g, "")
        }
        return "book" == config.kotobee.mode ? "_" + config.kotobee.cloudid : "library" == config.kotobee.mode ? "_" + config.kotobee.cloudid + "_" + (e ? "" : n.data.book.id) : void 0
    };

    n.addBookmark = function (t, o) {
        n.readSlot("bookmark" + n.getBookId(), function (r) {
            function i(r) {
                r && (a = r),
                    t.chapter = currentIndex,
                    t.cloudid = config.kotobee.cloudid, a.push(t),
                    serverapi.SaveBookmark({ BookID: n.getBookId(), Chapter: t.chapter, Location: t.location, UserName: "1", Bmid: t.bmid }).then(function (data) {
                        t.BookmarkID = data;

                        n.writeSlot("bookmark" + n.getBookId(), a, function () {
                            return sync.queue("bookmark/add", t), o && o(!0), !0
                        });
                        console.log(t);
                    })
            }
            var a = r;
            null != t.bmid ? n.deleteBookmark(t.bmid, null, i) : n.uniqueId(a, "bmid", function (e) {
                t.bmid = e, i()
            })
        })
    };

    n.getBookmarks = function (e) {
        serverapi.GetBookmark(n.getBookId()).then(function (data) {
            delete localStorage["bookmark" + n.getBookId()];
            localStorage["bookmark" + n.getBookId()] = JSON.stringify(data);

            n.readSlot("bookmark" + n.getBookId(), function (t) {
                e && e(t)
            })
        });
    };

    n.deleteBookmark = function (t, o, r) {
        n.readSlot("bookmark" + n.getBookId(), function (i) {
            for (var a = 0; a < i.length; a++)
                if (i[a].bmid == t) {
                    var s = i[a];
                    serverapi.DeleteBookmark(s.BookmarkID);
                    return i.splice(a, 1), void n.writeSlot("bookmark" + n.getBookId(), i, function () {
                        o || sync.queue("bookmark/delete", s), r && r(i)
                    })
                }
        })
    };

    n.deleteNote = function (t, o, r) {
        n.readSlot("note" + n.getBookId(), function (i) {
            for (var a = 0; a < i.length; a++)
                if (i[a].nid == t) {
                    var s = i[a];
                    serverapi.DeleteNote(s.NoteID);
                    return i.splice(a, 1), void n.writeSlot("note" + n.getBookId(), i, function () {
                        o || sync.queue("note/delete", s), r && r(i)
                    })
                }
        })
    };

    n.addNote = function (t, o) {
        try {
            n.readSlot("note" + n.getBookId(), function (r) {
                function i(i) {
                    i && (r = i), null == t.chapter && (t.chapter = currentIndex), t.cloudid = config.kotobee.cloudid;
                    var a = {};
                    for (var s in t) "event" != s && (a[s] = t[s]);
                    r.push(a),
                        serverapi.SaveNote({ Chapter: t.chapter, Location: t.location, NoteText: t.note, Src: t.src, BookID: n.getBookId(), UserName: 1, Nid: t.nid }).then(function (data) {
                            a.NoteID = data;

                            n.writeSlot("note" + n.getBookId(), r, function () {
                                return sync.queue("note/add", a), o && o(!0), !0
                            });
                            modal.hide();
                        })
                }
                null != t.nid ? n.deleteNote(t.nid, null, i) : n.uniqueId(r, "nid", function (e) {
                    t.nid = e, i()
                })
            })
        } catch (e) { }
        return !0
    };

    n.getNotes = function (e) {
        serverapi.GetNote(n.getBookId()).then(function (data) {
            delete localStorage["note" + n.getBookId()];
            localStorage["note" + n.getBookId()] = JSON.stringify(data);
            n.readSlot("note" + n.getBookId(), e);
        });
    };

    n.deleteHlight = function (t, o, r) {
        n.readSlot("hlight" + n.getBookId(), function (i) {
            for (var a = 0; a < i.length; a++)
                if (i[a].hid == t) {
                    var s = i[a];
                    serverapi.DeleteHighlight(s.HighlightID);
                    return i.splice(a, 1), void n.writeSlot("hlight" + n.getBookId(), i, function () {
                        o || sync.queue("hlight/delete", s), r && r(i)
                    })
                }
        })
    };

    n.addHlight = function (t, o) {
        n.readSlot("hlight" + n.getBookId(), function (r) {
            function i(i) {
                i && (r = i),
                    null == t.chapter && (t.chapter = currentIndex),
                    t.cloudid = config.kotobee.cloudid, r.push(t),
                    serverapi.SaveHighlight({ Chapter: t.chapter, Location: t.location, BookID: n.getBookId(), ColorCode: t.color, Src: t.src, UserName: 1, Hid: t.hid }).then(function (data) {
                        t.HighlightID = data;

                        n.writeSlot("hlight" + n.getBookId(), r, function () {
                            return sync.queue("hlight/add", t), o && o(!0), !0
                        });
                        modal.hide();
                    })

            }
            null != t.hid ? n.deleteHlight(t.hid, null, i) : n.uniqueId(r, "hid", function (e) {
                t.hid = e, i()
            })
        })
    };

    n.getHlightById = function (e, t) {
        n.getHlights(function (n) {
            for (var o = 0; o < n.length; o++)
                if (n[o].hid == e) return t && t(n[o]), n[o]
        })
    };

    n.getHlights = function (e) {
        serverapi.GetHighlight(n.getBookId()).then(function (data) {
            delete localStorage["hlight" + n.getBookId()];
            localStorage["hlight" + n.getBookId()] = JSON.stringify(data);
            n.readSlot("hlight" + n.getBookId(), e);
        });
        //n.readSlot("hlight" + n.getBookId(), e)
    };

    n.addAction = function (e, t) {
        n.readSlot("actions" + n.getBookId(), function (o) {
            o.push(e), n.writeSlot("actions" + n.getBookId(), o, function () {
                return t && t(!0), !0
            })
        })
    };

    n.deleteAction = function (e, t) {
        n.readSlot("actions" + n.getBookId(), function (o) {
            for (var r = 0; r < o.length; r++)
                if (JSON.stringify(e) == JSON.stringify(o[r])) return o.splice(r, 1), void n.writeSlot("actions" + n.getBookId(), o, function () {
                    return t && t(o), o
                });
            t && t()
        })
    };

    n.getActions = function (e) {
        n.readSlot("actions" + n.getBookId(), e)
    };

    n.readSlot = function (e, t) {
        function n() {
            if (!(o = localStorage[e])) return t && t([]), [];
            var n = JSON.parse(o);
            return t && t(n), n
        }
        var o;
        if (chromeApp) chrome.storage.local.get(e, function (n) {
            n = n ? isEmpty(n) ? [] : n[e] : [], t && t(n)
        });
        else if (native) try {
            NativeStorage.getItem(e, function (e) {
                e || t([]);
                var n = JSON.parse(e);
                t && t(n)
            }, function () {
                t && t([])
            })
        } catch (e) {
            n()
        } else n()
    };

    n.writeSlot = function (e, t, n) {
        function o() {
            localStorage[e] = JSON.stringify(t), n && n()
        }
        if (chromeApp) {
            var r = {};
            r[e] = t, chrome.storage.local.set(r, function () {
                n && n()
            })
        } else if (native) try {
            NativeStorage.setItem(e, JSON.stringify(t), function () {
                n && n()
            }, function (e) {
                o()
            })
        } catch (e) {
            o()
        } else o()
    };

    n.clearSlot = function (e, t) {
        function n() {
            delete localStorage[e], t && t()
        }
        if (chromeApp) chrome.storage.local.remove(e, function () {
            t && t()
        });
        else if (native) try {
            NativeStorage.remove(e, function () {
                t && t()
            }, function () {
                n()
            })
        } catch (e) { } else n()
    };

    n.clearSlots = function (e) {
        function t() {
            function t() {
                if (++i >= o.length) e && e();
                else {
                    var r = o[i];
                    0 == r.indexOf("note_") ? n.clearSlot(r, t) : 0 == r.indexOf("bookmark_") ? n.clearSlot(r, t) : 0 == r.indexOf("hlight_") ? n.clearSlot(r, t) : 0 == r.indexOf("settings_") && n.clearSlot(r, t)
                }
            }
            var o = [];
            for (var r in localStorage) o.push(r);
            var i = -1;
            t()
        }
        if (chromeApp) chrome.storage.local.clear(e);
        else if (native) try {
            NativeStorage.clear(e, function () {
                t()
            })
        } catch (e) { } else t()
    };

    n.copyLocalToNativeStorage = function () {
        if (native) try {
            for (var e in localStorage) NativeStorage.setItem(e, localStorage[e]), delete localStorage[e]
        } catch (e) { }
    };

    n.uniqueId = function (e, t, o) {
        var r = 0;
        t || (t = "id");
        for (var i = 0; i < e.length; i++) Number(e[i][t]) > r && (r = Number(e[i][t]));
        n.readSlot("actions" + n.getBookId(), function (e) {
            for (var n = 0; n < e.length; n++) Number(e[n].data[t]) > r && (r = Number(e[n].data[t]));
            return o && o(r + 1), r + 1
        })
    };

    n.getPublicRoot = function (e) {
        var t = "";
        return n.data.paths && ("book" == config.kotobee.mode || "book" == e ? t = n.data.paths.books : "library" != config.kotobee.mode && "library" != e || (t = n.data.paths.libraries)), t || (t = ""), t
    };

    return n;
}]);

app.factory("sync", ["$injector", "$rootScope", "$http", "error", function ($injector, $rootScope, $http, error) {
    function i() {
        $rootScope.data.syncing || ($rootScope.data.syncing = !0, $rootScope.$$phase || $rootScope.$apply())
    }

    function a() {
        $rootScope.data.syncing && ($rootScope.data.syncing = !1, $rootScope.$$phase || $rootScope.$apply())
    }

    var s, l, c, d, u, p, f, g = {};

    g.initialize = function () {
        config.kotobee.cloudid && (l || (l = $injector.get("stg")), c || (c = $injector.get("bookmarks")), d || (d = $injector.get("notes")), u || (u = $injector.get("hlights")), p || (p = $injector.get("chapters")), l.getActions(function (e) {
            s = e, window.addEventListener ? (window.addEventListener("offline", g.onOffline, !1), window.addEventListener("online", g.onOnline, !1)) : (document.addEventListener("offline", g.onOffline, !1), document.addEventListener("online", g.onOnline, !1))
        }))
    };

    g.trigger = function () {
        g.offline() ? g.onOffline() : g.onOnline()
    };

    g.offline = function () {
        if ($injector.get("backend").cloudParam("sync")) try {
            return "NONE" == navigator.network.connection.type.toUpperCase() && "UNKNOWN" == navigator.network.connection.type.toUpperCase()
        } catch (e) {
            return !1
        }
    };

    g.onOffline = function () {
        $injector.get("backend").cloudParam("sync")
    };

    g.onOnline = function (t) {
        $injector.get("backend").cloudParam("sync") && (f || g.applyChanges(function () {
            i();
            arguments.callee;
            var s = getFormData($injector.get("backend").getAuthVars());
            $http.post(config.kotobee.liburl + "library/notebook/all", s, postOptions).success(function (e) {
                if (a(), e.error) return error.update(e), void (t && t());
                var n = e.notes ? e.notes : [],
                    o = e.bookmarks ? e.bookmarks : [],
                    i = e.hlights ? e.hlights : [];
                d.deleteAll(!0, function () {
                    d.addNotesFromSync(n, function () {
                        c.deleteAll(!0, function () {
                            c.addBookmarksFromSync(o, function () {
                                u.deleteAll(!0, function () {
                                    u.addHlightsFromSync(i, function () {
                                        t && t()
                                    })
                                })
                            })
                        })
                    })
                })
            }).error(function (e) {
                $rootScope.data.syncing = !1, error.update({
                    error: "cantLoadNodes"
                }), t && t()
            })
        }))
    };

    g.queue = function (e, t) {
        if (l && config.kotobee.cloudid && !config.kotobee.public) {
            var n = {
                type: e,
                data: clone(t)
            };
            s.push(n), l.addAction(n, function () {
                g.offline() || f || g.applyChanges()
            })
        }
    };

    g.applyChanges = function (t) {
        if (a(), $injector.get("backend").cloudParam("sync") && l && l.data.user && (l.data.user.email || l.data.user.code)) try {
            var n = {
                func: arguments.callee,
                args: arguments
            };
            if (0 == s.length) return f = !1, void (t && t());
            i(), f = !0;
            var c = s[0],
                d = getFormData($injector.get("backend").getAuthVars());
            for (var u in c.data) d.append(u, c.data[u]);
            $http.post(config.kotobee.liburl + "library/" + c.type, d, postOptions).success(function (e) {
                s.splice(0, 1), l.deleteAction(c, function () {
                    g.applyChanges()
                })
            }).error(function (e) {
                f = !1, setTimeout(a, 300), error.update({
                    error: "syncOnConnectionMsg",
                    ref: n
                })
            })
        } catch (e) { }
    };

    return g;
}]);

app.factory("tinCan", ["stg", "backend", function (stg, backend) {
    function n(t) {
        try {
            if (!config.gAnalyticsID) return;
            if (!ga) return
        } catch (e) {
            return
        }
        ga("send", "event", t.activity, t.verb, stg.data.book.meta.dc.title)
    }

    var o = {};

    o.initialize = function () { };

    o.send = function (o) {
        n(o), backend.cloudParam("tincan") && (o.name = stg.data.user.name ? stg.data.user.name : "User", estg.data.user.email ? o.email = estg.data.user.email : estg.data.user.code && (o.email = stg.data.user.code + "@promocode"), o.lrs = backend.cloudParam("tincanlrs"), o.lrsver = backend.cloudParam("tincanlrsver"), o.lrsuser = backend.cloudParam("tincanlrsuser"), o.lrspwd = backend.cloudParam("tincanlrspwd"), backend.tincan(o))
    };

    return o;
}]);

app.factory("translit", ["$interpolate", "$injector", "$rootScope", "cache", function ($interpolate, $injector, $rootScope, cache) {
    function i() {
        for (var e = 0; e < d.length; e++) d[e](a)
    }
    var a, s, l = {},
        c = {},
        d = [];

    l.addListener = function (e) {
        d.push(e)
    };

    l.setLangOld = function (e) {
        a = e
    };

    l.setLang = function (e, t) {
        $injector.get("backend").getServerLang({
            lang: e
        }).success(function (n) {
            s = n, a = e, $rootScope.data && ($rootScope.data.lc || ($rootScope.data.lc = 0), $rootScope.data.lc = Number($rootScope.data.lc) + 1), i(), cache.getOb("viewContainer") || cache.setOb("viewContainer", document.getElementsByClassName("view-container")[0]), "ar" == e ? (cache.getOb("body") && addClass(cache.getOb("body"), "rtl"), cache.getOb("viewContainer") && cache.getOb("viewContainer").setAttribute("dir", "rtl")) : (cache.getOb("body") && removeClass(cache.getOb("body"), "rtl"), cache.getOb("viewContainer") && cache.getOb("viewContainer").setAttribute("dir", "ltr")), t && t()
        }).error(function (e, t) { })
    };

    l.getLang = function () {
        return a
    };

    l.get = function (t, n) {
        if (!s) return t;
        if (!a) return t;
        if (s[t] && (t = s[t]), n)
            for (var o = 0; o < n.length; o++) t = $interpolate(t)(n[o]);
        return t
    };

    l.getOld = function (t, n) {
        if (c[a][t] && (t = c[a][t]), n)
            for (var o = 0; o < n.length; o++) t = $interpolate(t)(n[o]);
        return t
    };

    return l;
}]);

app.factory("view", ["stg", function (stg) {
    var t = {};

    t.generate = function () {
        var t = stg.data.currentLibrary,
            n = stg.data.currentLibrary.cloud,
            o = stg.view;
        o.registration = {}, o.registration.promocode = n && !n.public && t && !t.cloud.promocodesdisabled && t.promocodesettings.inregistration, o.topmenu = {}, o.topmenu.promocode = n && !n.public && t && !t.cloud.promocodesdisabled && t.promocodesettings.intopmenu, o.topmenu = {}, o.topmenu.promocode = n && !n.public && t && !t.cloud.promocodesdisabled && t.promocodesettings.intopmenu, o.bookinfo = {}, o.bookinfo.promocode = n && !n.public && t && !t.cloud.promocodesdisabled && t.promocodesettings.inbookinfo
    };

    return t;
}]);

app.factory("virt", ["$ionicScrollDelegate", "$filter", "cache", "$q", function ($ionicScrollDelegate, $filter, cache, $q) {
    var i, a, s, l, c = {};

    c.lock = function () {
        a = !0
    };

    c.unlock = function () {
        a = !1
    };

    c.enable = function () {
        i || a || (i = !0, l && clearInterval(l), s = setInterval(d, 60), d())
    };

    c.disableAfter = function (e) {
        l && clearInterval(l), l = setInterval(function () {
            c.disable(), clearInterval(l)
        }, e)
    };

    c.disable = function () {
        i && (a || (i = !1, clearInterval(s)))
    };

    var d = function () {
        a || $filter("contentVirt")()
    };

    c.reset = function (e, n) {
        a || (c.reveal(!0), $filter("contentVirt")("reset"), $filter("contentVirt")(null, e, function () {
            c.reveal(!1), n && n()
        }))
    };

    c.checkIfRequiresParse = function (e) {
        hasClass(e, "parsed") || $filter("contentVirt")("parse", e)
    };

    c.checkIfRequiresParseAndAnnotations = function (e) {
        hasClass(e, "parsed") || $filter("contentVirt")("parseAndAnnotations", e)
    };

    c.reveal = function (e) {
        a || $filter("contentVirt")(e ? "reveal" : "undoReveal")
    };

    c.safeAnchorScroll = function (e, t, n) {
        if (!a) {
            var o = $q.defer();
            c.reveal(!0);
            var i = document.getElementById(t);
            return c.safeScrollToElem(e, i, n), timeOut(function () {
                c.reset(), o.resolve()
            }, 1300), o.promise
        }
    };

    c.safeScroll = function (t, n, o, i) {
        if (!a) {
            var s = $q.defer();
            return i || (i = !0), c.reveal(!0), $ionicScrollDelegate.$getByHandle(t).scrollTo(Math.round(n), Math.round(o), i), timeOut(function () {
                c.reset(), s.resolve()
            }, 1300), s.promise
        }
    };

    c.safeScrollToElem = function (t, n, r) {
        if (n && !a) {
            c.reveal(!0);
            var i = cache.getOb("epubContainer"),
                s = 0,
                l = getComputedStyle(i).paddingTop;
            l && (s = Number(l.split("px")[0]));
            var d = $ionicScrollDelegate.$getByHandle(t).getScrollPosition().top - (i.getBoundingClientRect().top + s) - 25;
            return c.safeScroll(t, 0, n.getBoundingClientRect().top + d, r)
        }
    };

    c.forceRepaint = function () {
        try {
            var e = document.createTextNode(" ");
            document.body.appendChild(e), setTimeout(function () { }, 0)
        } catch (e) { }
    };

    return c;
}]);

app.factory("workers", ["stg", function (e) {
    function t(e) {
        p = !0
    }

    function n(e) {
        if (e.length) {
            var t = e[0].args,
                n = e[0].id;
            s(t[0]).postMessage({
                func: t[1],
                params: t[2],
                id: n
            })
        }
    }

    function o(e) {
        var t = i(e.target);
        r(t);
        var o = t[0].args[3];
        o && o(e.data.data), t.shift(), n.apply(this, [t])
    }

    function r(e) {
        for (var t = 0; t < e.length; t++);
    }

    function i(e) {
        return e == c ? g : e == d ? h : e == u ? m : void 0
    }

    function a(e) {
        return "cssParser" == e ? g : "htmlParser" == e ? h : "search" == e ? m : void 0
    }

    function s(e) {
        return "cssParser" == e ? c : "htmlParser" == e ? d : "search" == e ? u : void 0
    }

    function l() {
        return !p && "undefined" != typeof Worker
    }
    var c, d, u, p, f = {},
        g = [],
        h = [],
        m = [];

    f.initialize = function () {
        if (l()) try {
            c = new Worker(rootDir + "js/workers/cssParser.js"), d = new Worker(rootDir + "js/workers/htmlParser.js"), u = new Worker(rootDir + "js/workers/search.js"), c.addEventListener("error", t, !1), d.addEventListener("error", t, !1), u.addEventListener("error", t, !1), c.addEventListener("message", o, !1), d.addEventListener("message", o, !1), u.addEventListener("message", o, !1)
        } catch (e) {
            p = !0
        }
    };

    var v = 0;

    f.call = function (e, t, o, r) {
        if (++v, s(e)) {
            var i = a(e),
                l = [e, t, o, r];
            i.push({
                args: l,
                id: v
            }), 1 == i.length && n(i)
        }
    };

    f.supported = function () {
        return l()
    };

    return f;
}]);

app.factory("serverapi", ["$http", "$q", function ($http, $q) {

    var api = {}; var rootUrl = "http://localhost:54360/api/";

    api.SaveBookmark = function (paramObj) {
        return $http.post(rootUrl + "Bookmarks/SaveBookMark", paramObj).then(function (result) { return result.data; });
    };

    api.GetBookmark = function (BookId) {
        var UserId = 1;
        return $http.get(rootUrl + "Bookmarks/GetBookmarks/" + UserId + "/" + BookId).then(function (result) {
            var Bookmarks = [];
            angular.forEach(result.data, function (val, idx) {
                Bookmarks.push({ BookmarkID: val.BookmarkID, chapter: val.Chapter, location: val.Location, bmid: val.Bmid });
            });
            return Bookmarks;
        });
    };

    api.DeleteBookmark = function (bmId) {
        return $http.delete(rootUrl + "Bookmarks/DeleteBookMark/" + bmId).then(function (result) { return result.data; });
    };

    api.SaveHighlight = function (ParamObj) {
        return $http.post(rootUrl + "Highlight/SaveHighlight", ParamObj).then(function (result) { return result.data; });
    };

    api.GetHighlight = function (BookId) {
        var UserId = 1;
        return $http.get(rootUrl + "Highlight/GetHighlights/" + UserId + "/" + BookId).then(function (result) {
            var Highlights = [];
            angular.forEach(result.data, function (val, idx) {
                Highlights.push({ HighlightID: val.HighlightID, chapter: val.Chapter, location: val.Location, src: val.Src, color: val.ColorCode, hid: val.Hid });
            })
            return Highlights;
        });
    };

    api.DeleteHighlight = function (HId) {
        return $http.delete(rootUrl + "Highlight/DeleteHighlight/" + HId).then(function (result) { return result.data; });
    };

    api.SaveNote = function (ParamObj) {
        return $http.post(rootUrl + "Notes/SaveNotes", ParamObj).then(function (result) { return result.data; });
    }

    api.DeleteNote = function (NoteId) {
        return $http.delete(rootUrl + "Notes/DeleteNote/" + NoteId).then(function (result) { return result.data; });
    }

    api.GetNote = function (BookId) {
        var UserId = 1;
        return $http.get(rootUrl + "Notes/GetNotes/" + UserId + "/" + BookId).then(function (result) {
            var Notes = [];

            angular.forEach(result.data, function (val, idx) {
                Notes.push({ NoteID: val.NoteID, type: "content", chapter: val.Chapter, location: val.Location, note: val.NoteText, src: val.Src, nid: val.Nid });
            })
            return Notes;
        });
    }

    return api;
}]);